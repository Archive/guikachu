/* point.cc
 * 
 * Copyright (C) 1999 The gnomemm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <foocanvasmm/point.h>

namespace FooCanvasmm
{
    
Point::Point (gdouble x_ /* = 0.0 */, gdouble y_ /* = 0.0 */) :
    x (x_),
    y (y_)
{
}

Point::Point(const Point& src)
{
    operator=(src);
}

Point& Point::operator= (const Point& src)
{
    x = src.x;
    y = src.y;

    return *this;
}

Point::~Point()
{
}

gdouble Point::get_x () const
{
  return x; 
}

void Point::set_x (gdouble x_)
{
    x = x_; 
}

gdouble Point::get_y() const
{
    return y; 
}

void Point::set_y (gdouble y)
{
    y = y;
}
  
Point Point::operator+ (const Point& p2)
{
    return Point (x + p2.x, y + p2.y);
}

Point Point::operator- (const Point& p2)
{
    return Point (x - p2.x, y - p2.y);
}

Point const & Point::operator+=(const Point& p2)
{
    x += p2.x;
    y += p2.y;
    
    return *this;
}

Point const & Point::operator-=(const Point& p2)
{
    x -= p2.x;
    y -= p2.y;
    
    return *this;
}

} //namespace FooCanvasmm


std::ostream& operator<<(std::ostream& out, const FooCanvasmm::Point& p)
{
    return out << '(' << p.get_x () << ", " << p.get_y () << ')';
}
