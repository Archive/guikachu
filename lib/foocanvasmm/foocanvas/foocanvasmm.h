/* $Id$ */
/* libgnomecanvasmm - a C++ wrapper for libgnomecanvas
 *
 * Copyright 1999-2001 Free Software Foundation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef FOOCANVASMM_H
#define FOOCANVASMM_H

#include <gtkmm.h>

#include <foocanvasmm/canvas.h>
#include <foocanvasmm/properties.h>
#include <foocanvasmm/group.h>
#include <foocanvasmm/init.h>
#include <foocanvasmm/line.h>
#include <foocanvasmm/pixbuf.h>
#include <foocanvasmm/point.h>
#include <foocanvasmm/rect.h>
#include <foocanvasmm/rect-ellipse.h>

#endif /* #ifndef FOOCANVASMM_H */
