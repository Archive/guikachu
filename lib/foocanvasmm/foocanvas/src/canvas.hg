// -*- C++ -*-
/* $Id$ */

/* canvas.h
 * 
 * Copyright (C) 1998 EMC Capital Management Inc.
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * Copyright (C) 1999 The Gtk-- Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libfoocanvas/foo-canvas.h>
#include <gtkmm/layout.h>
#include <gdkmm/color.h>

_DEFS(foocanvasmm,foocanvas)
_PINCLUDE(gtkmm/private/layout_p.h)

namespace FooCanvasmm
{

class Item;
class Group;

//- Canvas functions usually operate in either World coordinates
//- (units for the entire canvas), or Canvas coordinates (pixels starting 
//- at 0,0 in the top left).  There are functions to transform from 
//- one to the other.
class Canvas : public Gtk::Layout
{
  _CLASS_GTKOBJECT(Canvas, FooCanvas, FOO_CANVAS, Gtk::Layout, GtkLayout);
public:
  _CTOR_DEFAULT;

  //: Get the root canvas item
  _WRAP_METHOD(Group* root() const, foo_canvas_root)
           
  //: Limits of scroll region
  _WRAP_METHOD(void set_scroll_region(double x1, double y1, double x2, double y2), foo_canvas_set_scroll_region)

  //: Get limits of scroll region
  _WRAP_METHOD(void get_scroll_region(double& x1, double& y1, double& x2, double& y2) const, foo_canvas_get_scroll_region)

  _WRAP_METHOD(void set_center_scroll_region(bool center), foo_canvas_set_center_scroll_region)

  //: Set the pixels/world coordinates ratio
  //- With no arguments sets to default of 1.0.
  _WRAP_METHOD(void set_pixels_per_unit(double n = 1.0), foo_canvas_set_pixels_per_unit)

  //: Shift window.
  //- Makes a canvas scroll to the specified offsets, given in canvas pixel
  //- units.
  //- The canvas will adjust the view so that it is not outside the scrolling
  //- region.  This function is typically not used, as it is better to hook
  //- scrollbars to the canvas layout's scrolling adjusments.
  _WRAP_METHOD(void scroll_to(int x, int y), foo_canvas_scroll_to)

  //: Scroll offsets in canvas pixel coordinates.
  _WRAP_METHOD(void get_scroll_offsets(int& cx, int& cy) const, foo_canvas_get_scroll_offsets)

  //: Repaint immediately, don't wait for idle loop
  //- normally the canvas queues repainting and does it in an
  //- idle loop
  _WRAP_METHOD(void update_now(), foo_canvas_update_now)

  //: Find an item at a location.
  //- Looks for the item that is under the specified position, which must be
  //- specified in world coordinates.  Arguments are in world coordinates.
  //- Returns 0 if no item is at that
  //- location.
  _WRAP_METHOD(Item* get_item_at(double x, double y) const, foo_canvas_get_item_at)



  //: Repaint small area (internal)
  //- Used only by item implementations. Request an eventual redraw
  //- of the region, which includes x1,y1 but not x2,y2
  _WRAP_METHOD(void request_redraw(int x1, int y1, int x2, int y2), foo_canvas_request_redraw)

  //: Convert from World to canvas coordinates (units for the entire canvas)
  //: to Canvas coordinates (pixels starting at 0,0 in the top left
  //: of the visible area). The relationship depends on the current
  //: scroll position and the pixels_per_unit ratio (zoom factor)
  _WRAP_METHOD(void w2c(double wx, double wy, int& cx, int& cy) const, foo_canvas_w2c)
  _WRAP_METHOD(void w2c(double wx, double wy, double& cx, double& cy) const, foo_canvas_w2c_d)

  //: From Canvas to World
  _WRAP_METHOD(void c2w(int cx, int cy, double& wx, double& wy) const, foo_canvas_c2w)

  //: Convert from Window coordinates to world coordinates.
  //- Window coordinates are based of the widget's GdkWindow.
  //- This is fairly low-level and not generally useful.
  _WRAP_METHOD(void window_to_world (double winx,double winy, double& worldx,double& worldy) const, foo_canvas_window_to_world)

  //: Convert from world coordinates to Window coordinates.
  //- Window coordinates are based of the widget's GdkWindow.
  //- This is fairly low-level and not generally useful.
  _WRAP_METHOD(void world_to_window (double worldx, double worldy, double& winx, double& winy) const, foo_canvas_world_to_window)

  //: Parse color spec string and allocate it into the GdkColor.
  bool get_color(const Glib::ustring& spec, Gdk::Color& color) const;
  _IGNORE(foo_canvas_get_color)

/* Allocates a color from the RGB value passed into this function. */
  _WRAP_METHOD(gulong get_color_pixel(guint rgba) const, foo_canvas_get_color_pixel)
  _WRAP_METHOD(void set_stipple_origin(const Glib::RefPtr<Gdk::GC>& gc), foo_canvas_set_stipple_origin)


  // The following are simply accessed via the struct in C,
  //  but Federico reports that they are meant to be used.
  //: Get the pixels per unit.
  _MEMBER_GET(pixels_per_unit, pixels_per_unit, double, double);

  //: Draw the background for the area given.
  //- This method is only used for non-antialiased canvases.
  _WRAP_SIGNAL(void draw_background(int x, int y, int width, int height), "draw_background")
  //: Private Virtual methods for groping the canvas inside bonobo.
  _WRAP_VFUNC(void request_update(), "request_update")
};

} /* namespace FooCanvasmm */

