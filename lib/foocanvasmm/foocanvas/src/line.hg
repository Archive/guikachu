// -*- C++ -*-
/* $Id$ */

/* line.h
 * 
 * Copyright (C) 1998 EMC Capital Management Inc.
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * Copyright (C) 1999 The Gtk-- Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <foocanvasmm/item.h>
#include <libfoocanvas/libfoocanvas.h>
#include <libfoocanvas/foo-canvas-line.h>
#include <libfoocanvas/foo-canvas-util.h>
#include <vector>

_DEFS(foocanvasmm,foocanvas)
_PINCLUDE(foocanvasmm/private/item_p.h)

namespace FooCanvasmm
{

class GnomeGroup;

// Sample use of Gnome_CanvasPoints :

//   Gnome_CanvasPoints points;
//  
//   points.push_back(Art::Point(0, 0));
//   points.push_back(Art::Point(100,0));
//   points.push_back(Art::Point(0,100));
//   points.push_back(Art::Point(100,100));
//
//   line = new Gnome_CanvasLine(&m_canvasgroup,points);

//using std::vector;

/** Wrapper for GnomeCanvasPoints.
 * GnomeCanvasPoints is actually a BoxedType,
 * but this acts in a similar way, with the advantage of acting like a std::vector.
 */
class Points : public std::vector<Point>
{
public:
  Points(size_type nbpoints = 0);
  explicit Points(FooCanvasPoints* castitem);
  ~Points();

#ifndef DOXYGEN_SHOULD_SKIP_THIS
  typedef  FooCanvasPoints BaseObjectType;  //So that this works with tempaltes that are intended for normal BoxedTypes.
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

  operator bool() const;
  bool is_null() const;

  const FooCanvasPoints* gobj() const { return _gobj(); }
  FooCanvasPoints* gobj() { return _gobj(); }
  static GType get_type () { return FOO_TYPE_CANVAS_POINTS; }

protected:
  FooCanvasPoints* _gobj() const;
  mutable FooCanvasPoints* points_;
  mutable bool owned_;
};


class Line : public Item
{
  _CLASS_GTKOBJECT(Line, FooCanvasLine, FOO_CANVAS_LINE, Item, FooCanvasItem)
public:
  explicit Line(Group& parent);
  Line(Group& parent, const Points& points);

  _WRAP_PROPERTY("points", Points)
  _WRAP_PROPERTY("fill-color", Glib::ustring)
  _WRAP_PROPERTY("fill-color-gdk", Gdk::Color)
  _WRAP_PROPERTY("fill-color-rgba", guint)
  _WRAP_PROPERTY("fill-stipple", Glib::RefPtr<Gdk::Bitmap>)
  _WRAP_PROPERTY("width-pixels", guint)
  _WRAP_PROPERTY("width-units", double)
  _WRAP_PROPERTY("cap-style", Gdk::CapStyle)
  _WRAP_PROPERTY("join-style", Gdk::JoinStyle)
  _WRAP_PROPERTY("line-style", Gdk::LineStyle)
  _WRAP_PROPERTY("first-arrowhead", bool)
  _WRAP_PROPERTY("last-arrowhead", bool)
  _WRAP_PROPERTY("smooth", bool)
  _WRAP_PROPERTY("spline-steps", guint)
  _WRAP_PROPERTY("arrow-shape-a", double)
  _WRAP_PROPERTY("arrow-shape-b", double)
  _WRAP_PROPERTY("arrow-shape-c", double)
};

} /* namespace FooCanvasmm */


#ifndef DOXYGEN_SHOULD_SKIP_THIS
namespace Glib
{

template <>
class Value<FooCanvasmm::Points> : public Glib::Value_Boxed<FooCanvasmm::Points>
{
public:
  static GType value_type() G_GNUC_CONST;
};

} // namespace Glib
#endif /* DOXYGEN_SHOULD_SKIP_THIS */
