2005-09-18  Stepan Kasal  <kasal@ucw.cz>

	* configure.in: Update.
	* .cvsignore: Add autom4te.cache.

2005-05-12  Jody Goldberg <jody@gnome.org>

	* libfoocanvas/foo-canvas-widget.c (foo_canvas_widget_unmap) : new.
	  umap the widget too.
	(foo_canvas_widget_map) : new.  map the widget if it is visible.
	(foo_canvas_widget_class_init) : connect up the new map/unmap
	  handlers so that show/hide item works.

2005-04-04  Morten Welinder  <terra@gnome.org>

	* */*.c: Use canonical property names.

2005-03-31  Stepan Kasal  <kasal@ucw.cz>

	* demos/Makefile.am: s/INCLUDES/AM_CPPFLAGS/ "automake-1.9 -Wall"
	  has told me to do this; it should be safe even with automake-1.5.
	* libfoocanvas/Makefile.am (INCLUDES, AM_CPPFLAGS): Likewise.
	(non-intermediate): New dummy rule; see
		http://bugzilla.gnome.org/show_bug.cgi?id=172211 and
		http://bugzilla.gnome.org/show_bug.cgi?id=172212
	(.list.c, .list.h): Remove $(GLIB_GENMARSHAL); it didn't work
	  anyway, the prerequisities of an implicit rule are ignored.
	* configure.in (GLIB_GENMARSHAL): Full path is not needed.

2005-03-02  Morten Welinder  <terra@gnome.org>

	* configure.in (HAVE_RENDER): Remove the reference to $EEL_LIBS.
	* libfoocanvas/foo-canvas-rect-ellipse.c (foo_canvas_rect_realize):
	  don't use gdk_display, it can actually point to another display,
	  and it's deprecated.

2005-02-16  Stepan Kasal  <kasal@ucw.cz>

	* libfoocanvas/Makefile.am: Handle generated sources gracefully.
	* configure.in: Add explanation for GLIB_GENMARSHAL, remove dead
	  code and other cleanup.  Up AC_PREREQ to 2.50.
	* acconfig.h: Remove.
	* autogen.sh: Require Automake >= 1.5, the default is = 1.4, which
	  is missing in my museum; other cleanup.
	* demos/Makefile.am (EXTRA_DIST): Add flower.png.
	* README: Typos.

2004-12-04  Jody Goldberg <jody@gnome.org>

	* libfoocanvas/foo-canvas-polygon.c (get_bounds) : return false if
	  there are no points instead of crashing.
	(get_bounds_canvas) : ditto.
	(foo_canvas_polygon_update) : hande situation with no points.

2004-10-20  Alexander Larsson  <alexl@redhat.com>

	* libfoocanvas/foo-canvas.c (foo_canvas_expose):
	Return FALSE from expose handler so other expose handlers are run too.

2004-08-31  Morten Welinder  <terra@gnome.org>

	* libfoocanvas/foo-canvas.c (foo_canvas_group_draw): Use NULL, not
	0, for null pointer.

2004-08-15  Anders Carlsson  <andersca@gnome.org>

	* libfoocanvas/foo-canvas.c (add_idle): Make sure that the idle
	handler has higher priority than the gdk redraw one.
	
2004-07-14  Jody Goldberg <jody@gnome.org>

	* configure.in : remove GNOME_PLATFORM_GNOME_2 and GNOME_GTKDOC_CHECK
	  and replace with GTK_DOC_CHECK([1.0])

2004-07-12  Jody Goldberg <jody@gnome.org>

	* libfoocanvas/foo-canvas.c (foo_canvas_key) : bind to parent on
	  failure

2004-07-02  Jody Goldberg <jody@gnome.org>

	* libfoocanvas/foo-canvas-rect-ellipse.c : move include gdkx.h into
	  HAVE_RENDER

2004-04-23  Jody Goldberg <jody@gnome.org>

	* demos/canvas.c : patch some warnings

	* libfoocanvas/Makefile.am : make clean should remove the generated
	  marshaller code.

	* libfoocanvas/foo-canvas-pixbuf.c (foo_canvas_pixbuf_update) :
	  Cache a prescaled pixbuf here.
	(foo_canvas_pixbuf_draw) : rather than rescaling every time.

2004-03-11  Jody Goldberg <jody@gnome.org>

	* libfoocanvas/foo-canvas-line.c (foo_canvas_line_get_property) :
	  g_value_set_string_take_ownership is deprecated.
	* libfoocanvas/foo-canvas-text.c (foo_canvas_text_get_property) : ditto
	(foo_canvas_text_set_property) : ref the attr list
	(foo_canvas_text_apply_attributes) : respect the zoom

2004-03-02  Alexander Larsson  <alexl@redhat.com>

	* libfoocanvas/foo-canvas.h:
	No comma at end of enum.

2004-01-27  Alexander Larsson  <alexl@redhat.com>

	* libfoocanvas/foo-canvas.c (emit_event):
	Possible fix for nautilus crash.

2003-10-21  Morten Welinder  <terra@gnome.org>

	* libfoocanvas/foo-canvas-util.c (foo_canvas_get_miter_points):
	Clean up and improve portability.

2003-10-01  Padraig O'Briain <padraig.obriain@sun.com>

	* libfoocanvas/foo-canvas.c (foo_canvas_item_accessible_ref_state_set):
	Do not refer to item->canvas if item can be NULL. (bug #123179)

2003-07-16  Jody Goldberg <jody@gnome.org>

	* libfoocanvas/foo-canvas.c (foo_canvas_set_pixels_per_unit) : round
	  when mapping back to integer coordinates to guard against inadverent
	  decrement due to lack of precision.
	  eg zoom of 0.85 == 0.849999999 would lose a pixel

2003-08-04  Alexander Larsson  <alexl@redhat.com>

	* libfoocanvas/foo-canvas.c (do_update):
	Loop do_update if picking caused need_update to be set again.
	(Thanks to George <jirka@5z.com> for noticing this)

2003-07-07  Alexander Larsson  <alexl@redhat.com>

	* libfoocanvas/foo-canvas-rect-ellipse.c (render_rect_alpha):
	Check that format != NULL before using XRender. (#116752)
	Patch from Narayana Pattipati.

2003-06-13  Alexander Larsson  <alexl@redhat.com>

	* po/Makefile.in.in:
	Remove. Not needed.
	
	* libfoocanvas/foo-canvas.c (foo_canvas_item_unrealize) 
	(foo_canvas_group_unrealize):
	Unmap the item if its already mappen when unrealizing.

2003-06-10  Morten Welinder  <terra@gnome.org>

	* libfoocanvas/foo-canvas-rect-ellipse.c
	(set_colors_and_stipples): New function, extracted from
	foo_canvas_re_update_shared.
	(foo_canvas_re_update_shared): Use set_colors_and_stipples.
	(foo_canvas_re_realize): Recalc pixel values and use
	set_colors_and_stipples.

2003-06-10  Morten Welinder  <terra@gnome.org>

	* libfoocanvas/foo-canvas-rect-ellipse.c (get_color_value): Fix --
	was totally bogus.

2003-06-04  Jody Goldberg <jody@gnome.org>

	* libfoocanvas/foo-canvas-pixbuf.c (foo_canvas_pixbuf_draw) : be
	  smarter about how much to redraw.  A nice performance win.

2003-05-22  Morten Welinder <terra@gnome.org>

	* libfoocanvas/foo-canvas.c (foo_canvas_item_dispose): Ungrab the
	right display.
	(foo_canvas_item_ungrab): Ditto.
	(shutdown_transients): Ditto.

2003-05-20  Morten Welinder <terra@gnome.org>

	* libfoocanvas/*.c: Fix compilation warnings per Jody.

2003-05-12  Morten Welinder <terra@gnome.org>

	* libfoocanvas/*.c: Avoid a few deprecated gtk+ functions.

2003-05-12  Jody Goldberg <jody@gnome.org>

	* libfoocanvas/Makefile.am : ensure BUILT_SOURCES get built earlier

2003-05-08  Jody Goldberg <jody@gnome.org>

	* libfoocanvas/foo-canvas-pixbuf.c (foo_canvas_pixbuf_point) :
	  optionally accept events on transparent areas.
	(foo_canvas_pixbuf_class_init) : add 'point_ignores_alpha'.
	(foo_canvas_pixbuf_set_property : handle it here.
	(foo_canvas_pixbuf_get_property) : handle it here.
	(foo_canvas_pixbuf_init) : init to FALSE for compatibility.

2003-05-07  Jody Goldberg <jody@gnome.org>

	* libfoocanvas/foo-canvas.c (foo_canvas_group_map) : call realize if
	  child is unREALIZED, not unMAPPED.

2003-04-11  Jody Goldberg <jody@gnome.org>

	* libfoocanvas/foo-canvas.c (foo_canvas_item_invoke_update) : Add a
	  sanity check to ensure that after an update the item does not still
	  need an update.
	(foo_canvas_item_request_update) : add some checks to ensure that we
	  don't try to queue an update from an update.

2003-04-11  Jody Goldberg <jody@gnome.org>

	* libfoocanvas/foo-canvas-pixbuf.c (foo_canvas_pixbuf_destroy) :
	  gdk_pixbuf_unref is deprecated.
	(foo_canvas_pixbuf_set_property) : ditto.
	(foo_canvas_pixbuf_draw) : gdk_pixbuf_render_to_drawable_alpha is
	  deprecated in favour of gdk_draw_pixbuf.

2003-04-07  Alexander Larsson  <alexl@redhat.com>

	* libfoocanvas/foo-canvas.c:
	Add "visible" property.
	Redo the way REALIZED/MAPPED/VISIBLE works to be the same as Gtk+.
	This allows us to create items that are initially hidden.

2003-03-18  Alexander Larsson  <alexl@redhat.com>

	* libfoocanvas/foo-canvas.c (foo_canvas_set_pixels_per_unit):
	Put up a window with background None when zooming the window, thus
	avoiding nasty tearing effects. We get flashing effects instead, which
	we unfortunately can't fix because gdk doesn't expose the set background
	pixmap on a window.

2003-03-04  Jody Goldberg <jody@gnome.org>

	* libfoocanvas/foo-canvas-text.c : Add wrap_width to connect to the
	  pango get/set width methods.

2003-03-04  Alexander Larsson  <alexl@redhat.com>

	* libfoocanvas/foo-canvas.c (foo_canvas_accessible_initialize) 
	(foo_canvas_accessible_class_init): Only chain up to
	parent if the function != NULL.

2003-03-04  Alexander Larsson  <alexl@redhat.com>

	(foo_canvas_accessible_initialize):
	Fix warning.
	
2003-03-04  Alexander Larsson  <alexl@redhat.com>

	* libfoocanvas/foo-canvas.c:
	(foo_canvas_accessible_adjustment_changed),
	(foo_canvas_accessible_initialize),
	(foo_canvas_accessible_get_n_children),
	(foo_canvas_accessible_ref_child),
	(foo_canvas_accessible_class_init),
	(foo_canvas_accessible_get_type), (foo_canvas_accessible_create),
	(foo_canvas_accessible_factory_get_accessible_type),
	(foo_canvas_accessible_factory_create_accessible),
	(foo_canvas_accessible_factory_class_init),
	(foo_canvas_accessible_factory_get_type), (foo_canvas_class_init),
	(foo_canvas_item_accessible_add_focus_handler),
	(foo_canvas_item_accessible_get_item_extents),
	(foo_canvas_item_accessible_is_item_in_window),
	(foo_canvas_item_accessible_get_extents),
	(foo_canvas_item_accessible_get_mdi_zorder),
	(foo_canvas_item_accessible_grab_focus),
	(foo_canvas_item_accessible_remove_focus_handler),
	(foo_canvas_item_accessible_component_interface_init),
	(foo_canvas_item_accessible_is_item_on_screen),
	(foo_canvas_item_accessible_initialize),
	(foo_canvas_item_accessible_ref_state_set),
	(foo_canvas_item_accessible_class_init),
	(foo_canvas_item_accessible_get_type),
	(foo_canvas_item_accessible_create),
	(foo_canvas_item_accessible_factory_get_accessible_type),
	(foo_canvas_item_accessible_factory_create_accessible),
	(foo_canvas_item_accessible_factory_class_init),
	(foo_canvas_item_accessible_factory_get_type),
	(foo_canvas_item_class_init):
	Accessibility support.
	Patch from padraig.obriain@sun.com.

2003-03-03  Alexander Larsson  <alexl@redhat.com>

	* libfoocanvas/foo-canvas.c (emit_event):
	Remove annoying warning.

	* libfoocanvas/foo-canvas-rect-ellipse.c (render_rect_alpha):
	Don't use deprecated API.

	* libfoocanvas/foo-canvas.c (foo_canvas_group_set_property):
	Correctly update when a group is moved.
	(remove_idle): Don't use deprecated API.
	Remove unused code.

2003-02-15  ERDI Gergo  <cactus@cactus.rulez.org>

	* libfoocanvas/foo-canvas-pixbuf.c: Added new 'interp_type'
	property to choose the GdkPixbuf interpolation method

2002-09-26  Alexander Larsson  <alexl@redhat.com>

	* libfoocanvas/foo-canvas-rect-ellipse.[ch]:
	* libfoocanvas/foo-canvas.[ch]:
	Don't use deprecated APIs.
	
	* libfoocanvas/Makefile.am:
	* libfoocanvas/libfoocanvas.pc.in:
	Fix include paths.

2002-02-16  Alexander Larsson  <alla@lysator.liu.se>

	* libfoocanvas/foo-canvas.c:
	Remove old traces of affines.

Sat Feb 16 23:59:02 2002  Soeren Sandmann  <sandmann@daimi.au.dk>

	* libfoocanvas/foo-canvas-rect-ellipse.c (render_rect_alpha): make
	sure the rectangle is drawn to Gtk+'s backing store, not the
	window, so we don't lose the drawing when Gtk+ updates.

	(foo_canvas_rect_draw): fix a memory leak

Sat Feb 16 20:18:57 2002  Soeren Sandmann  <sandmann@daimi.au.dk>

	* libfoocanvas/foo-canvas-rect-ellipse.c
	(foo_canvas_rect_realize): make it compile.

	* libfoocanvas/foo-canvas.c (foo_canvas_init):
	gtk_widget_set_redraw_on_allocate (GTK_WIDGET (canvas), FALSE);

	* libfoocanvas/foo-canvas.c (foo_canvas_group_draw): only redraw
	children that are inside the exposed region.

2002-02-16  Alexander Larsson  <alla@lysator.liu.se>

        * configure.in:
	* acconfig.h:
	* libfoocanvas/Makefile.am:
	Detect and use Xrender if availible.

	* demos/canvas-primitives.c:
	Add AA rect test and some 8bit text test.
	
	* libfoocanvas/foo-canvas.[ch]:
	Add support for alpha filled rectangles.
	Add update invalidation optimization.

	* libfoocanvas/foo-canvas.c (foo_canvas_set_pixels_per_unit):
	Sete update needed before scrolling, because scrolling may call
	expose. Leading to redraw errors.

2002-02-15  Jody Goldberg <jody@gnome.org>

	* libfoocanvas/foo-canvas-widget.c (foo_canvas_widget_destroy) :
	  minor patch to fix crash on the 2nd call to destroy.

2002-02-10  Alexander Larsson  <alla@lysator.liu.se>

	* README:
	* AUTHORS:
	Add some text here.

2002-02-10  Alexander Larsson  <alla@lysator.liu.se>

	* demos/canvas-primitives.c:
	Add test for centering.
	
	* demos/canvas-scalability.c:
	Cut down on the nr of items for the moment.
	
	* libfoocanvas/foo-canvas-line.c:
	* libfoocanvas/foo-canvas-pixbuf.c:
	* libfoocanvas/foo-canvas-polygon.c:
	* libfoocanvas/foo-canvas-rect-ellipse.c:
	* libfoocanvas/foo-canvas-text.c: 
	* libfoocanvas/foo-canvas-util.c:
	* libfoocanvas/foo-canvas-widget.c:
	Use foo_canvas_item_request_redraw().
	Change g2w -> i2w.
	
	* libfoocanvas/foo-canvas.[ch]:
	Use foo_canvas_item_request_redraw().
	Change g2w -> i2w.
	Added foo_canvas_item_send_behind(),
	foo_canvas_item_request_redraw() and
	foo_canvas_set_center_scroll_region().
	Added comment that window_to/from_world
	are mostly for backwards compat now.
	don't increment scroll_widht/height in scroll_to()
	Update all items in set_scroll_region().

2002-02-09  Alexander Larsson  <alla@lysator.liu.se>

        * */*.[ch]
	Massive sed job gnomecanvas -> foocanvas
	
2002-02-09  Alexander Larsson  <alla@lysator.liu.se>

	* configure.in:
	Remove libart and pangoft2 dependencies.
	
	* libfoocanvas/gnome-canvas-line.c:
	* libfoocanvas/gnome-canvas-polygon.c:
	* libfoocanvas/gnome-canvas-rect-ellipse.c:
	Don't use any affines.
	
	* libfoocanvas/gnome-canvas-text.h:
	Remove affine.
	
	* libfoocanvas/gnome-canvas.[ch]:
	Remove all functions that return affines, make the other
	ones not use affines.

2002-02-09  Alexander Larsson  <alla@lysator.liu.se>

	* libfoocanvas/gnome-canvas-pixbuf.c:
	Update to not use affines.
	
	* libfoocanvas/gnome-canvas.c:
	Get coordinates right when picking and zoom_xofs/yofs != 0
	Fix gnome_canvas_c2w().

2002-02-09  Alexander Larsson  <alla@lysator.liu.se>

	* libfoocanvas/gnome-canvas-line.c: (item_to_canvas),
	(gnome_canvas_line_draw):
	* libfoocanvas/gnome-canvas-pixbuf.c: (gnome_canvas_pixbuf_draw):
	* libfoocanvas/gnome-canvas-polygon.c: (gnome_canvas_polygon_draw):
	* libfoocanvas/gnome-canvas-rect-ellipse.c:
	* libfoocanvas/gnome-canvas-text.c: (gnome_canvas_text_draw):
	* libfoocanvas/gnome-canvas-widget.c: (recalc_bounds),
	(gnome_canvas_widget_draw):
	(gnome_canvas_rect_draw), (gnome_canvas_ellipse_draw):
	Update to the new draw() prototype
	
	* libfoocanvas/gnome-canvas-marshal.list:
	Change the marshaller used for draw.
	
	* libfoocanvas/gnome-canvas.[ch]:
	Use the Gtk+ 2.0 double buffering and invalidation handling.
	Remove all traces of UTAs.

2002-02-09  Alexander Larsson  <alla@lysator.liu.se>

	* libfoocanvas/gnome-canvas-text.c:
	* libfoocanvas/gnome-canvas-util.c:
	Remove some libart includes

2002-02-09  Alexander Larsson  <alla@lysator.liu.se>

	* libfoocanvas/gnome-canvas-line.c:
	* libfoocanvas/gnome-canvas-polygon.c:
	* libfoocanvas/gnome-canvas-rect-ellipse.c:
	* libfoocanvas/gnome-canvas-rect-ellipse.h:
	* libfoocanvas/gnome-canvas-util.c:
	* libfoocanvas/gnome-canvas-util.h:
	* libfoocanvas/gnome-canvas.h:
	Removed all traces of SVPs

2002-02-08  Alexander Larsson  <alla@lysator.liu.se>

	* libfoocanvas/gnome-canvas.c (gnome_canvas_group_point):
	Change xpos -> ypos typo.

