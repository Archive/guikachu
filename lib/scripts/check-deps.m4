dnl **********************
dnl GUIKACHU_DEPS_GUIKACHU
dnl
dnl Check backend and GUI dependancy libraries for Guikachu
dnl **********************
dnl
AC_DEFUN([GUIKACHU_DEPS_GUIKACHU],[
CORE_PACKAGES="glibmm-2.4 gdkmm-2.4 libxml-2.0"
GUI_PACKAGES="gtkmm-2.4 libglademm-2.4"

dnl Check for optional libraries
dnl ----------------------------
AC_MSG_CHECKING([for Gnome-VFS])
PKG_CHECK_EXISTS([gnome-vfsmm-2.6], have_gnomevfs=yes, have_gnomevfs=no)
AC_MSG_RESULT($have_gnomevfs)
if test x$have_gnomevfs = xyes; then
    CORE_PACKAGES="$CORE_PACKAGES gnome-vfsmm-2.6"
    AC_DEFINE(GUIKACHU_HAVE_GNOMEVFS, [], [Is gnome-vfsmm available?])
fi

if test x$GUIKACHU_HOST_OS = xMinGW; then
    AC_DEFINE(GUIKACHU_CONFIG_SYSTEM_WINREG, [], [Configuration system to use])
else
    AC_DEFINE(GUIKACHU_CONFIG_SYSTEM_GCONF, [], [Configuration system to use])
    CORE_PACKAGES="$CORE_PACKAGES gconfmm-2.6"
    have_gconf=yes
fi

AC_MSG_CHECKING([for libgnomeui])
PKG_CHECK_EXISTS(libgnomeuimm-2.6, have_libgnomeui=yes, have_libgnomeui=no)
AC_MSG_RESULT($have_libgnomeui)
if test x$have_libgnomeui = xyes; then
    GUI_PACKAGES="$GUI_PACKAGES libgnomeuimm-2.6"
    AC_DEFINE(GUIKACHU_HAVE_LIBGNOMEUI, [], [Is libgnomeuimm available?])
    LIBGNOME_DATADIR=`"$PKG_CONFIG" --variable=prefix libgnome-2.0`/share
fi

dnl Get compile-time/link-time flags for dependancies
dnl -------------------------------------------------
PKG_CHECK_MODULES(GUIKACHU_DEPS, $CORE_PACKAGES)
PKG_CHECK_MODULES(GUIKACHU_GUI_DEPS, $CORE_PACKAGES $GUI_PACKAGES)

dnl Export variables to Automake
AC_SUBST(GUIKACHU_DEPS_CFLAGS)
AC_SUBST(GUIKACHU_DEPS_LIBS)
AC_SUBST(GUIKACHU_GUI_DEPS_CFLAGS)
AC_SUBST(GUIKACHU_GUI_DEPS_LIBS)
AC_SUBST(LIBGNOME_DATADIR)
])


dnl **********************
dnl GUIKACHU_DEPS_FOOCANVAS
dnl
dnl Check dependancy libraries for FooCanvas
dnl **********************
dnl
AC_DEFUN([GUIKACHU_DEPS_FOOCANVAS],[
PKG_CHECK_MODULES(FOOCANVAS_DEPS, gtk+-2.0 pango)
AC_SUBST(FOOCANVAS_DEPS_CFLAGS)
AC_SUBST(FOOCANVAS_DEPS_LIBS)

AC_PATH_PROG(GLIB_GENMARSHAL, glib-genmarshal)
])


dnl **********************
dnl GUIKACHU_DEPS_FOOCANVASMM
dnl
dnl Check dependancy libraries for FooCanvasmm
dnl **********************
dnl
AC_DEFUN([GUIKACHU_DEPS_FOOCANVASMM],[
PKG_CHECK_MODULES(FOOCANVASMM_DEPS, gtkmm-2.4)
AC_SUBST(FOOCANVASMM_DEPS_CFLAGS)
AC_SUBST(FOOCANVASMM_DEPS_LIBS)

GTKMM_PREFIX=`$PKG_CONFIG --variable=prefix glibmm-2.4`
GTKMM_PROCDIR=$GTKMM_PREFIX/lib/glibmm-2.4/proc
AC_SUBST(GTKMM_PROCDIR)
GTKMM_PROC=$GTKMM_PROCDIR/gmmproc
AC_SUBST(GTKMM_PROC)
])
