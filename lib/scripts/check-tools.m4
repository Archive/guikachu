dnl **********************
dnl GUIKACHU_PROG_GNU_M4(ACTION_NOT_FOUND)
dnl
dnl Check for GNU m4.  (sun won't do.)
dnl **********************
dnl
AC_DEFUN([GUIKACHU_PROG_GNU_M4],[
AC_CHECK_PROGS(M4, gm4 m4, m4)

if test "$M4" = "m4"; then
  AC_MSG_CHECKING(whether m4 is GNU m4)
  if $M4 --version </dev/null 2>/dev/null | grep '^GNU m4 ' >/dev/null ; then
    AC_MSG_RESULT(yes)
  else
    AC_MSG_RESULT(no)
    if test "$host_vendor" = "sun"; then
      $1
    fi
  fi
fi
])

dnl **********************
dnl GUIKACHU_PROG_GNU_MAKE(ACTION_NOT_FOUND)
dnl
dnl Check for GNU make (no sun make)
dnl **********************
dnl
AC_DEFUN([GUIKACHU_PROG_GNU_MAKE],[
AC_MSG_CHECKING(whether make is GNU Make)
if $ac_make --version 2>/dev/null | grep '^GNU Make ' >/dev/null ; then
        AC_MSG_RESULT(yes)
else
        AC_MSG_RESULT(no)
        if test "$host_vendor" = "sun" ; then
           $1
        fi
fi
])


dnl **********************
dnl GUIKACHU_PROG_PERL_XML
dnl
dnl Check for Perl and the required XML module
dnl **********************
dnl
AC_DEFUN([GUIKACHU_PROG_PERL_XML],[
AC_PATH_PROG(PERL, perl)
if test -z "$PERL"; then
	AC_MSG_ERROR([Perl not found in $PATH])
fi

AC_CACHE_CHECK(for XML::Parser Perl module, ac_cv_perl_module_xml, [
if test -z "`$PERL -MXML::Parser -e exit 2>&1`"; then
	ac_cv_perl_module_xml=yes
else
	ac_cv_perl_module_xml=no
fi
])

if test "$ac_cv_perl_module_xml" = "no"; then
	AC_MSG_ERROR([XML::Parser not installed.
Perl module XML::Parser is required by xml-i18n-tools])
fi
])


dnl **********************
dnl GUIKACHU_PROG_MIME
dnl
dnl Check for freedesktop.org MIME database managers
dnl **********************
dnl
AC_DEFUN([GUIKACHU_PROG_MIME],[
AC_PATH_PROG(UPDATE_MIME, update-mime-database, no)

AC_SUBST(UPDATE_MIME)
AM_CONDITIONAL(HAVE_UPDATE_MIME, test "$UPDATE_MIME" != "no")
])


dnl **********************
dnl GUIKACHU_PROG_GCONFTOOL
dnl
dnl Check for GConfTool
dnl **********************
AC_DEFUN([GUIKACHU_PROG_GCONFTOOL],[
AC_PATH_PROG(GCONFTOOL, gconftool-2, no)

if test x"$GCONFTOOL" = xno; then
  AC_MSG_ERROR([gconftool-2 executable not found in your path - should be installed with GConf])
fi

AC_SUBST(GCONFTOOL)
])


dnl **********************
dnl GUIKACHU_CHECK_OPTIONAL_PROG
dnl
dnl usage: GUIKACHU_CHECK_OPTIONAL_PROG(NAME, exe, message_if_not_found)
dnl **********************
AC_DEFUN([GUIKACHU_CHECK_OPTIONAL_PROG],[
AC_PATH_PROG($1, $2, no)

if test "$$1" = no
then
	AC_MSG_WARN([*** $3 ***])
fi

AC_SUBST($1)
AM_CONDITIONAL(HAVE_$1, test "$$1" != "no")
])
