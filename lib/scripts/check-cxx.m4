dnl **********************
dnl GUIKACHU_CXX_VIRTUAL_TREE(ACTION_NOT_FOUND)
dnl
dnl Test the C++ compiler for a tricky multiple virtual inheritance situation
dnl **********************
dnl
AC_DEFUN([GUIKACHU_CXX_VIRTUAL_TREE],[
AC_CACHE_CHECK(
  [for virtual inheritance capabilities of C++ compiler ($CXX_NICE)],
  [guikachu_cv_cxx_virtual_tree],
  [
   AC_LANG_SAVE
   AC_LANG_CPLUSPLUS
   AC_TRY_COMPILE([
    class Base
    {
    public:
        virtual int foo1 () const = 0;
        virtual int foo2 () const = 0;
    };

    class VBase1: public virtual Base
    {
    protected:
       VBase1 (int a) {}
    };

    class VBase2: public virtual Base
    {
    protected:
	VBase2 (int b)
	    {}
    public:
	virtual int foo1 () const;       
	virtual int foo2 () const;       
    };
    
    class VBase2Aux: public VBase2
    {
    protected:
	VBase2Aux (int b): VBase2 (b) {}
    };
    
    class V2Derived1: public virtual VBase2Aux
    {
    protected:
	V2Derived1 (int b): VBase2Aux (b) {}
    public:
	int foo1 () const;
    };
    
    class V2Derived2: public virtual VBase2Aux
    {
    protected:
	V2Derived2 (int b): VBase2Aux (b) {}
    public:
	int foo2 () const;
    };

    class Derived: public VBase1,
                   public V2Derived1,
                   public V2Derived2
    {
    public:
	Derived (int a, int b): VBase1 (a),
				VBase2Aux (b),
				V2Derived1 (b), V2Derived2 (b) {}
    };
   ],[
    Derived *d = new Derived (0, 0);
    d->foo1 ();
    d->foo2 ();
   ],
     [guikachu_cv_cxx_virtual_tree="passed"],
     [guikachu_cv_cxx_virtual_tree="failed"]
   )
   AC_LANG_RESTORE
  ])

if test x$guikachu_cv_cxx_virtual_tree = xfailed; then
	$1
fi
])



dnl **********************
dnl GUIKACHU_CXX_HASH_MAP
dnl
dnl Check for the not-yet-standard STL extension hash_map.
dnl GUIKACHU_HAS_HASH_MAP and GUIKACHU_HASH_MAP_IN_EXT are set to correct values.
dnl **********************
dnl
AC_DEFUN([GUIKACHU_CXX_HASH_MAP],[
AC_MSG_CHECKING([for location of <hash_map>])
AC_LANG_SAVE
AC_LANG_CPLUSPLUS
AC_TRY_COMPILE(
[
#include <hash_map>

using std::hash_map;
],[
;
],[
guikachu_hash_map="in standard include dir"
],[
guikachu_hash_map=no
])

if test "x$guikachu_hash_map" = "xno"; then
	dnl See if it is available in ext/
	AC_TRY_COMPILE(
	[
	#include <ext/hash_map>

	using std::hash_map;
	],[
	;
	],[
	AC_DEFINE(GUIKACHU_HASH_MAP_IN_EXT, [], [Is hash_map inside ext directory?])
	guikachu_hash_map="in ext subdirectory"
	])
fi

if test "x$guikachu_hash_map" = "xno"; then
	AC_MSG_RESULT(not found)
else
	AC_MSG_RESULT("$guikachu_hash_map")
	AC_DEFINE(GUIKACHU_HAVE_HASH_MAP, [], [Is hash_map available?])
fi
AC_LANG_RESTORE
])
