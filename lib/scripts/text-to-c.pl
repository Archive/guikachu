#!/usr/bin/perl

# Converts a text file into a C string literal
# Usage:
#     text-to-c.pl id


$id = $ARGV[0] or die "Usage: text-to-c.pl id";

$id =~ s/-/_/g;

print "static char " . $id . "[] = \n";
$first = 1;
while (<STDIN>)
{
    print "\n" if not $first;
    s/\n$//;
    s/\\/\\\\/g;
    s/\"/\\\"/g;
    print "\t\"" . $_ . "\\n\"";
    $first = 0;
}
print ";\n";
