dnl **********************
dnl GUIKACHU_ACLOCAL_INCLUDE(macrodir)
dnl
dnl Add a directory to macro search (from gnome)
dnl **********************
dnl
AC_DEFUN([GUIKACHU_ACLOCAL_INCLUDE],
[
        test -n "$ACLOCAL_FLAGS" && ACLOCAL="$ACLOCAL $ACLOCAL_FLAGS"
        for k in $1 ; do ACLOCAL="$ACLOCAL -I $k" ; done
])
