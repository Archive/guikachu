/* $Id$
 *
 * You are free to use, modify, and redistribute this file any way you
 * wish, including incorporation into closed source products.
 *
 * Copyright (C) 2001-2002  �RDI Gerg� <cactus@cactus.rulez.org>
 */

#include <System/SystemPublic.h>
#include <UI/UIPublic.h>

#include "sample-app.rcp.h"
#include "main-form.h"

static FormPtr  form       = 0;
static FieldPtr email_fld  = 0;
static FieldPtr notify_fld = 0;
static FieldPtr rating_fld = 0;

static void init_members ()
{
    form       = FrmGetFormPtr (frmResults);
    email_fld  = FrmGetObjectPtr (form, FrmGetObjectIndex (form, results_email));
    notify_fld = FrmGetObjectPtr (form, FrmGetObjectIndex (form, results_notify));
    rating_fld = FrmGetObjectPtr (form, FrmGetObjectIndex (form, results_rating));
}

static void update ()
{
    char     rating[2];
    char    *email;
    Boolean  notify;

    /* Update rating */
    StrPrintF (rating, "%d", frmMain_get_rating ());
    FldSetTextPtr (rating_fld, rating);
    FldDrawField (rating_fld);
    
    /* Update e-mail address */
    email = frmMain_get_email ();
    if (email) {
	FldSetTextPtr (email_fld, email);
	FldDrawField (email_fld);
    }

    /* Update notification request status */
    notify = frmMain_get_notify ();
    if (notify)
	FldSetTextPtr (notify_fld, "Yes");
    else
	FldSetTextPtr (notify_fld, "No");
    FldDrawField (notify_fld);
}

Boolean frmResults_Handler (EventPtr event)
{
    Boolean handled = false;

    switch (event->eType) {

    case frmOpenEvent:
	FrmDrawForm (FrmGetActiveForm ());
	init_members ();
	update ();
	handled = true;
	break;

    case ctlSelectEvent:
	if (event->data.ctlSelect.controlID == results_ok)
	{
	    FrmReturnToForm (frmMain);
	    handled = true;
	}
	
    default:
	break;
    }

    return handled;
}

