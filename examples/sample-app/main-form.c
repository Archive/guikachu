/* $Id$
 *
 * You are free to use, modify, and redistribute this file any way you
 * wish, including incorporation into closed source products.
 *
 * Copyright (C) 2001-2002  �RDI Gerg� <cactus@cactus.rulez.org>
 */

#include <System/SystemPublic.h>
#include <UI/UIPublic.h>

#include "sample-app.rcp.h"

#include "menu.h"

static void not_implemented ()
{
    FrmAlert (dlgNotImpl);
}

static FormPtr    form       = 0;
static FieldPtr   email_fld  = 0;
static ControlPtr notify_ctl = 0;
static ControlPtr ratings[5] = {0, 0, 0, 0, 0};

static void init_members ()
{
    int i;
    
    form       = FrmGetFormPtr (frmMain);
    email_fld  = FrmGetObjectPtr (form, FrmGetObjectIndex (form, main_email));
    notify_ctl = FrmGetObjectPtr (form, FrmGetObjectIndex (form, main_notify));

    /* We use numeric ID's for the rating widgets to automatically set
     * them up here */
    for (i = 0; i < 5; i++)
	ratings[i] = FrmGetObjectPtr (form, FrmGetObjectIndex (form, 9001 + i));
}

/* Accessor for getting the currently selected rating */
int frmMain_get_rating ()
{
    int result = 0;
    int i = 0;

    while (!CtlGetValue (ratings[i]) && i < 5)
	i++;

    if (i < 5)
	result = i + 1;
    
    return result;
}

/* Accessor for getting the entered e-mail address */
char * frmMain_get_email  ()
{
    return FldGetTextPtr (email_fld);
}

/* Accessor for getting notification request status */
Boolean frmMain_get_notify ()
{
    return CtlGetValue (notify_ctl);
}


/* Event handler for frmMain */
Boolean frmMain_Handler (EventPtr event)
{
    Boolean handled = false;
    
    switch (event->eType) {
    case frmOpenEvent:
	FrmDrawForm (FrmGetActiveForm ());
	init_members ();
	handled = true;
	break;

    case menuEvent:
	switch (event->data.menu.itemID) {
	case FileDanger:
	    menu_FileDanger_handler ();
	    handled = true;
	    break;
	case FileSave:
	case FileOpen:
	    /* Not implemented */
	    not_implemented ();
	    break;
	case HelpAbout:
	    menu_HelpAbout_handler ();
	    handled = true;
	    break;	    
	}

    case ctlSelectEvent:
	switch (event->data.ctlSelect.controlID) {
	case main_submit:
	    FrmPopupForm (frmResults);
	    handled = true;
	    break;
	case main_textrating:
	    /* Not implemented */
	    not_implemented ();
	    break;
	}
	
    default:
	break;
    }
    
    return handled;
}

