/* $Id$
 *
 * You are free to use, modify, and redistribute this file any way you
 * wish, including incorporation into closed source products.
 *
 * Copyright (C) 2001-2002  �RDI Gerg� <cactus@cactus.rulez.org>
 */

#include <UI/Event.h>

Boolean frmMain_Handler (EventPtr event);

int     frmMain_get_rating ();
char *  frmMain_get_email  ();
Boolean frmMain_get_notify ();
