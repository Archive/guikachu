/* $Id$
 *
 * You are free to use, modify, and redistribute this file any way you
 * wish, including incorporation into closed source products.
 *
 * Copyright (C) 2001-2002  �RDI Gerg� <cactus@cactus.rulez.org>
 */

#include <System/SystemPublic.h>
#include <UI/UIPublic.h>

#include "sample-app.rcp.h"

#include "main-form.h"
#include "results-form.h"

static int StartApplication(void)
{
    FrmPopupForm (frmMain);
    
    return 0;
}

static void StopApplication ()
{
    FrmCloseAllForms ();
}

static void EventLoop ()
{
    short     err;
    int       formID;
    FormPtr   form;
    EventType event;
    
    do
    {
        
        EvtGetEvent(&event, 200);
        
        if (SysHandleEvent(&event)) continue;
        if (MenuHandleEvent((void *)0, &event, &err)) continue;
        
        if (event.eType == frmLoadEvent) {
            formID = event.data.frmLoad.formID;
            form = FrmInitForm(formID);
            FrmSetActiveForm(form);

            switch (formID) {
            case frmMain:
                FrmSetEventHandler (form, frmMain_Handler);
                break;
	    case frmResults:
		FrmSetEventHandler (form, frmResults_Handler);
		break;
            }
        }
	FrmDispatchEvent(&event);
    } while(event.eType != appStopEvent);
}


UInt32 PilotMain (UInt16 cmd, void *cmdPBP, UInt16 launchFlags)
{
    if (cmd == sysAppLaunchCmdNormalLaunch) {
	StartApplication();
	EventLoop ();
	StopApplication ();
    }

    return 0;
}
