/* $Id$
 *
 * You are free to use, modify, and redistribute this file any way you
 * wish, including incorporation into closed source products.
 *
 * Copyright (C) 2001-2002  �RDI Gerg� <cactus@cactus.rulez.org>
 */

#include <System/SystemPublic.h>
#include <UI/UIPublic.h>

#include "sample-app.rcp.h"

void menu_HelpAbout_handler ()
{
    FrmAlert (dlgAbout);
}

void menu_FileDanger_handler ()
{
    FrmAlert (dlgDanger);
}
