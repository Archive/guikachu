Compiling Guikachu from source, on Windows
==========================================

Compiling Guikachu on Windows is now pretty straightforward. Just
install the packages described below, and it should configure and
build just right.

1. MinGW
--------

MinGW is a POSIX-like, lightweight API and runtime on top of Win32,
and is used by lots of Windows ports of inherently UNIX applications.
To compile Guikachu, you will need GCC (incl. the C++ frontend) and
the MinGW runtime environment.

Download from http://www.mingw.org/download.shtml the following
packages: binutils, gcc-core, gcc-g++, w32api, mingw-runtime,
mingw32-make, msys

To install, just uncompress binutils, gcc-core, gcc-g++, w32api and
mingw-runtime to a common directory (it's customary to use c:\mingw),
then run the setup app of mingw32-make and msys.


2. Required libraries
---------------------
The GTKmm installer available at
http://www.pcpm.ucl.ac.be/~gustin/win32_ports/gtkmm.html
contains most of the libraries needed, including GTK+ itself. Download
the '-devel' installer and run it.

The only missing libraries at this point should be 'gettext' and
'libiconv'. Fortunately, both of these are available as ready-to-use
ports. The ones I use are from
http://www.gimp.org/~tml/gimp/win32/downloads.html, but the ones at
http://gettext.sourceforge.org/ should work as well. The downloaded
zip-files are installable by simply uncompressing them into the same
directory where gcc et al. are installed.


3. Required tools
-----------------
intltool, which is used by Guikachu, requires a Perl
installation. ActiveState's Perl distribution, freely available at
http://www.activestate.com/ should work fine. Just download the zip
file and uncompress it into the same directory as everything else.


4. Configuring the source package
---------------------------------
By now, you should be ready to configure and build Guikachu from
source, just like on a UNIX system. See the included README file for
details.
