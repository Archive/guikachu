//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "bitmap-res.h"

#include "io/bmp-saver.h"
#include <gdkmm/pixbufloader.h>

#include "ui.h"

using namespace Guikachu;

Resources::Bitmap::Bitmap (ResourceManager   *manager,
			   const std::string &id,
			   serial_t           serial):
    Resource (manager, id, serial),
    bitmap_type (changed, TYPE_COLOR_256)
{
}

#include <iostream>

void Resources::Bitmap::load_file (const Glib::ustring &uri)
{
    unsigned char  *data = 0;
    IO::filesize_t  len;
    
    try {
        IO::load_uri (uri, data, len);
        load_data (data, len, uri);
    } catch (Glib::Exception &e) {
        UI::show_error_io_load_resource (uri, id, e.what ());
    }

    delete[] data;
}

void Resources::Bitmap::load_data (const unsigned char *data, size_t len,
                                   const Glib::ustring &src_hint)
{
    Glib::RefPtr<Gdk::PixbufLoader> loader = Gdk::PixbufLoader::create ();
    bool closed = (len == 0);
    
    try {
	loader->write ((unsigned char*)data, len);
	loader->close ();
        closed = true;
	
	ImageData pixbuf = loader->get_pixbuf ();

	set_image (pixbuf);
    } catch (Glib::Exception &e) {
        if (!closed)
            loader->close ();
        
        UI::show_error_io_load_resource (src_hint, id, e.what ());
    }
}

void Resources::Bitmap::save_file_bmp (const Glib::ustring &uri) const
{
    if (!image)
	return;

    unsigned char  *buf = 0;
    IO::filesize_t  len;

    try {
        IO::save_bmp (image, buf, len);
	IO::save_uri (uri, buf, len);
    } catch (Glib::Exception &e) {
        ;
    }

    delete[] buf;
}

void Resources::Bitmap::save_data_png (unsigned char *&data, size_t &len) const
{
    len = 0;
    data = 0;
    
    gchar *pixbuf_buf;
    
    image->save_to_buffer ((gchar*&)pixbuf_buf, (gsize&)len, "png",
                           std::list<Glib::ustring>(), std::list<Glib::ustring>());

    data = new unsigned char[len];
    memcpy (data, pixbuf_buf, len);
    g_free (pixbuf_buf);
}

void Resources::Bitmap::save_file_png (const Glib::ustring &uri) const
{
    if (!image)
	return;

    unsigned char *buf = 0;
    size_t len;

    try {
        save_data_png (buf, len);
	IO::save_uri (uri, buf, len);
    } catch (Glib::Exception &e) {
        UI::show_error_io_save_resource (uri, id, e.what ());
    }

    delete[] buf;    
}

void Resources::Bitmap::set_image (const ImageData &pixbuf)
{
    image = pixbuf;

    changed.emit ();
}
