//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "config.h"

#include "ui.h"

#include <glib.h>
#include <glib/gi18n.h>
#include <glib/gwin32.h>
#include <glibmm/miscutils.h>

#ifdef GUIKACHU_HAVE_GNOMEVFS
#include <libgnomevfsmm/uri.h>
#endif

using namespace Guikachu;

void UI::init_i18n ()
{
#ifdef ENABLE_NLS
#ifdef _WIN32
    char *localedir = g_win32_get_package_installation_subdirectory (0, 0, GUIKACHU_DATADIRNAME "\\locale");
    if (localedir)
        bindtextdomain (PACKAGE, localedir);
    else
        bindtextdomain (PACKAGE, GUIKACHU_LOCALEDIR);
    g_free (localedir);    
#else
    bindtextdomain (PACKAGE, GUIKACHU_LOCALEDIR);
#endif
    bind_textdomain_codeset (PACKAGE, "UTF-8");
    textdomain (PACKAGE);
#endif    
}

Glib::ustring UI::visible_filename (const Glib::ustring &uri)
{
    // FIXME: Ensure UTF-8-safetiness
    
    // Cut off path
    Glib::ustring retval = Glib::path_get_basename (uri);

    // If extension is .guikachu, cut that off as well
    static const char         *pattern = ".guikachu";
    static const unsigned int  pattern_len = strlen (pattern);
    
    if (retval.length () > pattern_len)
    {
        if (retval.substr (retval.length () - pattern_len) == pattern)
            retval.erase (retval.length () - pattern_len);
    }

    return retval;
}

Glib::ustring UI::visible_location (const Glib::ustring &uri)
{
    // FIXME: Ensure UTF-8-safetiness

    Glib::ustring path = uri;
    
#ifdef GUIKACHU_HAVE_GNOMEVFS
    Glib::RefPtr<Gnome::Vfs::Uri> vfs_uri = Gnome::Vfs::Uri::create (uri);
    if (vfs_uri)
        path = vfs_uri->get_path ();
#else
#endif

    return Glib::path_get_dirname (path);
}
