//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "form-res.h"

#include "form-editor/widget.h"
#include "form-editor/widget-util.h"

#include "form-editor/form-editor.h"

#include "form-editor/label.h"
#include "form-editor/button.h"
#include "form-editor/pushbutton.h"
#include "form-editor/graffiti.h"
#include "form-editor/selector-trigger.h"
#include "form-editor/checkbox.h"
#include "form-editor/list.h"
#include "form-editor/popup-trigger.h"
#include "form-editor/scrollbar.h"
#include "form-editor/slider.h"
#include "form-editor/text-field.h"
#include "form-editor/table.h"
#include "form-editor/formbitmap.h"
#include "form-editor/gadget.h"

using namespace Guikachu::Resources;
using namespace Guikachu;

Form::Form (ResourceManager   *manager,
	    const std::string &id,
	    serial_t           serial):
    Resource (manager, id, serial),
    IDManager (manager),
    title (changed, id),
    help_id (changed, manager),
    menu_id (changed, manager),
    modal (changed, false),
    frame (changed, true),
    savebehind (changed, true),
    def_button (changed, this),
    x (changed),
    y (changed),
    width (changed, get_screen_width ()),
    height (changed, get_screen_height ())
{
}

Form::~Form ()
{
    for (widget_map_t::iterator i = widgets.begin (); i != widgets.end (); i++)
    {
	unregister_id (i->second->id);
	delete i->second;
    }
}

namespace
{
    bool compare_serial (const Form::widget_map_t::value_type &a,
			 const Form::widget_map_t::value_type &b)
    {
	return a.first < b.first;
    }
}

Widget* Form::create_widget (Widgets::Type type,
			     std::string   id,
			     bool          try_alternate_names,
			     serial_t      serial)
{
    id = validate_id (id);

    if (serial < 0)
	if (widgets.size ())
	    serial = std::max_element (widgets.begin (), widgets.end (), compare_serial)->first + 1;
	else
	    serial = 0;

    if (serial < 0)
	return 0;
    
    if (id == "")
	id = create_id (Widgets::prefix_from_type (type));
    
    if (!register_id (id))
    {
	if (try_alternate_names)
	{
	    id = create_id (id + "_");
	    register_id (id);
	} else {
	    return 0;
	}
    }
	
    Widget *widget = 0;

    switch (type)
    {
    case Widgets::WIDGET_LABEL:
	widget = new Widgets::Label (this, id, serial);
	break;
    case Widgets::WIDGET_BUTTON:
	widget = new Widgets::Button (this, id, serial);
	break;
    case Widgets::WIDGET_PUSHBUTTON:
	widget = new Widgets::PushButton (this, id, serial);
	break;
    case Widgets::WIDGET_GRAFFITI:
	widget = new Widgets::Graffiti (this, id, serial);
	break;
    case Widgets::WIDGET_SELECTOR_TRIGGER:
	widget = new Widgets::SelectorTrigger (this, id, serial);
	break;
    case Widgets::WIDGET_CHECKBOX:
	widget = new Widgets::Checkbox (this, id, serial);
	break;
    case Widgets::WIDGET_LIST:
	widget = new Widgets::List (this, id, serial);
	break;
    case Widgets::WIDGET_POPUP_TRIGGER:
	widget = new Widgets::PopupTrigger (this, id, serial);
	break;
    case Widgets::WIDGET_SCROLLBAR:
	widget = new Widgets::ScrollBar (this, id, serial);
	break;
    case Widgets::WIDGET_SLIDER:
	widget = new Widgets::Slider (this, id, serial);
	break;
    case Widgets::WIDGET_TEXT_FIELD:
	widget = new Widgets::TextField (this, id, serial);
	break;
    case Widgets::WIDGET_TABLE:
	widget = new Widgets::Table (this, id, serial);
	break;
    case Widgets::WIDGET_FORMBITMAP:
	widget = new Widgets::FormBitmap (this, id, serial);
	break;
    case Widgets::WIDGET_GADGET:
	widget = new Widgets::Gadget (this, id, serial);
	break;
    case Widgets::WIDGET_NONE:
	g_assert_not_reached ();
	return 0;
	break;
    }

    widgets[serial] = widget;
    widget->changed.connect (widget_changed.make_slot());
    
    widget_created (widget);

    changed ();
    
    return widget;
}

void Form::remove_widget (Widget *widget)
{
    widget_removed.emit (widget);

    widgets.erase (widget->get_serial ());
    unregister_id (widget->id);
    
    delete widget;

    changed ();
}

std::set<Widget*> Form::get_widgets () const
{
    std::set<Widget*> ret;

    for (widget_map_t::const_iterator i = widgets.begin (); i != widgets.end (); i++)
    {
	ret.insert (i->second);
    }

    return ret;
}

Widget* Form::get_widget (const std::string &id) const
{
    widget_map_t::const_iterator found;
    for (found = widgets.begin ();
         found != widgets.end () && NoCase ().compare (found->second->id, id);
         ++found);
    
    if (found == widgets.end ())
	return 0;
    else
	return found->second;
}

Widget* Form::get_widget (serial_t serial) const
{
    widget_map_t::const_iterator found = widgets.find (serial);

    if (found == widgets.end ())
	return 0;
    else
	return found->second;
}

int Form::get_screen_width () const
{
    return manager->get_target ()->screen_width;
}

int Form::get_screen_height () const
{
    return manager->get_target ()->screen_height;
}

