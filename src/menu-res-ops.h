//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_MENU_RES_OPS_H
#define GUIKACHU_MENU_RES_OPS_H

#include "undo.h"
#include "menu-res.h"

namespace Guikachu
{
    namespace ResourceOps
    {
	namespace MenuOps
	{
	    class MenuHolder
	    {
	    protected:
		ResourceManager *manager;
		serial_t         menu_serial;

	    public:
		MenuHolder (Resources::Menu *menu);
		Resources::Menu * get_menu () const;
	    };
	    
	    
	    class SubmenuCreateOp: public UndoOp, private MenuHolder
	    {
		unsigned int             index;
		Resources::Menu::Submenu submenu;

		Glib::ustring op_label;

	    public:
		SubmenuCreateOp (Resources::Menu *menu,
				 unsigned int     index);
		virtual ~SubmenuCreateOp () {};
		
		void undo ();
		void redo ();
		Glib::ustring get_label () const { return op_label; };
	    };

	    
	    class SubmenuRemoveOp: public UndoOp, private MenuHolder
	    {
		unsigned int             index;
		Resources::Menu::Submenu submenu;

		Glib::ustring            op_label;
		
	    public:
		SubmenuRemoveOp (Resources::Menu *menu,
				 unsigned int     index);
		virtual ~SubmenuRemoveOp () {};

		void undo ();
		void redo ();
		Glib::ustring get_label () const { return op_label; };
	    };

	    
	    class SubmenuChangeOp: public UndoOp, private MenuHolder
	    {
		unsigned int  index;
		std::string   old_label, new_label;

		Glib::ustring op_label;

	    public:
		SubmenuChangeOp (Resources::Menu   *menu,
				 unsigned int       index,
				 const std::string &new_label);
		virtual ~SubmenuChangeOp () {};

		void     undo ();
		void     redo ();
		UndoOp * combine (UndoOp *other_op) const;

		Glib::ustring get_label () const { return op_label; };
	    };
	    
	    
	    class SubmenuMoveOp: public UndoOp, private MenuHolder
	    {
		typedef unsigned int submenu_idx_t;

                submenu_idx_t old_submenu_idx;
                submenu_idx_t new_submenu_idx;
		
		Glib::ustring  op_label;

	    public:
		SubmenuMoveOp (Resources::Menu *menu,
			       submenu_idx_t    old_index,
			       submenu_idx_t    new_index);
		virtual ~SubmenuMoveOp () {};

		void     undo ();
		void     redo ();
		UndoOp * combine (UndoOp *other_op) const;

		Glib::ustring get_label () const { return op_label; };
	    };



	    
	    class MenuItemCreateOp: public UndoOp, private MenuHolder
	    {
		unsigned int submenu_index, index;
		Resources::Menu::MenuItem item_data;
		
		Glib::ustring op_label;

	    public:
		MenuItemCreateOp (Resources::Menu *menu,
				  unsigned int     submenu_index,
				  unsigned int     index);
		virtual ~MenuItemCreateOp () {};

		void undo ();
		void redo ();
		Glib::ustring get_label () const { return op_label; };
	    };

	    
	    class MenuItemRemoveOp: public UndoOp, private MenuHolder
	    {
		unsigned int submenu_index, index;
		Resources::Menu::MenuItem item_data;
		
		Glib::ustring op_label;

	    public:
		MenuItemRemoveOp (Resources::Menu *menu,
				  unsigned int     submenu_index,
				  unsigned int     index);
		virtual ~MenuItemRemoveOp () {};

		void undo ();
		void redo ();
		Glib::ustring get_label () const { return op_label; };
	    };

	    
	    class MenuItemRenameOp: public UndoOp, private MenuHolder
	    {
		unsigned int submenu_index, index;
		std::string old_id, new_id;
		
		Glib::ustring op_label;

	    public:
		MenuItemRenameOp (Resources::Menu   *menu,
				  unsigned int       submenu_index,
				  unsigned int       index,
				  const std::string &new_id);
		virtual ~MenuItemRenameOp () {};

		void undo ();
		void redo ();
		Glib::ustring get_label () const { return op_label; };
	    };

	    
	    class MenuItemLabelOp: public UndoOp, private MenuHolder
	    {
		unsigned int submenu_index, index;
		std::string old_label, new_label;
		
		Glib::ustring op_label;

	    public:
		MenuItemLabelOp (Resources::Menu   *menu,
				 unsigned int       submenu_index,
				 unsigned int       index,
				 const std::string &new_label);
		virtual ~MenuItemLabelOp () {};

		void     undo ();
		void     redo ();
		UndoOp * combine (UndoOp *other_op) const;

		Glib::ustring get_label () const { return op_label; };
	    };

	    
	    class MenuItemShortcutOp: public UndoOp, private MenuHolder
	    {
		unsigned int submenu_index, index;
		char old_shortcut, new_shortcut;
		
		Glib::ustring op_label;

	    public:
		MenuItemShortcutOp (Resources::Menu *menu,
				    unsigned int     submenu_index,
				    unsigned int     index,
				    char             new_shortcut);
		virtual ~MenuItemShortcutOp () {};
		
		void     undo ();
		void     redo ();
		UndoOp * combine (UndoOp *other_op) const;

		Glib::ustring get_label () const { return op_label; };
	    };
	    
	    
	    class MenuItemMoveOp: public UndoOp, private MenuHolder
	    {
		typedef unsigned int submenu_idx_t;
		typedef unsigned int item_idx_t;

                submenu_idx_t old_submenu_idx;
                item_idx_t    old_item_idx;
                submenu_idx_t new_submenu_idx;
                item_idx_t    new_item_idx;
		
		Glib::ustring  op_label;

	    public:
		MenuItemMoveOp (Resources::Menu *menu,
				submenu_idx_t    old_submenu_idx,
				item_idx_t       old_item_idx,
                                submenu_idx_t    new_submenu_idx,
				item_idx_t       new_item_idx);
		virtual ~MenuItemMoveOp () {};

		void     undo ();
		void     redo ();
		UndoOp * combine (UndoOp *other_op) const;

		Glib::ustring get_label () const { return op_label; };
	    };
	    
	} // namespace MenuOps
    } // namespace ResourceOps
} // namespace Guikachu

#endif /* !GUIKACHU_MENU_RES_H */
