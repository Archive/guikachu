//$Id: cellrenderer-icontext.h,v 1.7 2006/10/06 22:35:40 cactus Exp $ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_CELLRENDERER_LIVETEXT_H
#define GUIKACHU_CELLRENDERER_LIVETEXT_H

#include <gtkmm/cellrenderertext.h>

namespace Guikachu
{
    namespace GUI
    {        
        class CellRendererLiveText: public Gtk::CellRendererText
        {
            sigc::connection conn_cancel;
            sigc::connection conn_done;
            
        public:
            CellRendererLiveText ();
            
        protected:
            Gtk::CellEditable * start_editing_vfunc (GdkEvent               *event,
                                                     Gtk::Widget            &widget,
                                                     const Glib::ustring    &path,
                                                     const Gdk::Rectangle   &background_area,
                                                     const Gdk::Rectangle   &cell_area,
                                                     Gtk::CellRendererState  flags);

            void editing_canceled_cb (Gtk::CellEditable *editable, const Glib::ustring &path);
            void editing_done_cb ();
        };
    }
}

#endif /* !GUIKACHU_CELLRENDERER_LIVETEXT */
