//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "resource-util-gui.h"

using namespace Guikachu;

namespace {
#include "pixmaps/dialog.xpm"
#include "pixmaps/form.xpm"
#include "pixmaps/string.xpm"
#include "pixmaps/stringlist.xpm"
#include "pixmaps/menu.xpm"
#include "pixmaps/bitmap.xpm"
#include "pixmaps/bitmapfamily.xpm"
#include "pixmaps/blob.xpm"
}

const char * const * Resources::get_type_icon (Resources::Type type)
{
    g_return_val_if_fail (type != Resources::RESOURCE_NONE, NULL);

    switch (type)
    {
    case Resources::RESOURCE_FORM:
	return form_xpm;
    case Resources::RESOURCE_DIALOG:
	return dialog_xpm;
    case Resources::RESOURCE_STRING:
	return string_xpm;
    case Resources::RESOURCE_STRINGLIST:
	return stringlist_xpm;
    case Resources::RESOURCE_BITMAP:
	return bitmap_xpm;
    case Resources::RESOURCE_BITMAPFAMILY:
	return bitmapfamily_xpm;
    case Resources::RESOURCE_MENU:
	return menu_xpm;
    case Resources::RESOURCE_BLOB:
        return blob_xpm;
    case Resources::RESOURCE_NONE:
	g_assert_not_reached ();
    }

    return 0;
}

Glib::RefPtr<Gdk::Pixbuf> Resources::get_type_pixbuf (Resources::Type type)
{
    typedef std::map<Resources::Type, Glib::RefPtr<Gdk::Pixbuf> > pixbuf_cache_t;
    static pixbuf_cache_t pixbuf_cache;

    if (pixbuf_cache.find (type) == pixbuf_cache.end ())
        pixbuf_cache[type] = Gdk::Pixbuf::create_from_xpm_data (get_type_icon (type));

    return pixbuf_cache[type];
}
