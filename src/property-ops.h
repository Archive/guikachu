//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_PROPERTY_OPS_H
#define GUIKACHU_PROPERTY_OPS_H

#include "property.h"
#include "undo.h"

/* Since current C++ compilers require all template class methods to
 * be defined at their declarations, this header is messy and reveals
 * lots of implementation detail.
 *
 * Outside code should only deal with the following interfaces:
 *       PropChangeOpFactory (for controllers)
 *       ResourcePropChangeOpFactory, WidgetPropChangeOpFactory (for
 *           controller creators)
 * (controller in the M/V/C sense)
 *
 * Why all the fuss? Because we can't store references to Properties
 * directly, as objects may be destroyed and then re-created by
 * undoing their deletion.
 *
 * FIXME: PropertyHolder is an ugly hack, if you have a better idea,
 * let me know.
 */

namespace Guikachu
{
    template<class OwnerClass, typename MemberType>
    class MemberHolder
    {
	typedef OwnerClass owner_t;
	typedef MemberType member_t;

	size_t member_offset;

    public:
	MemberHolder (const owner_t &owner,
		      member_t      &member) :
	    member_offset (size_t (&member) - size_t (&owner)) {}
	
	member_t & get_member (const owner_t &owner) const {
	    member_t &member = *(member_t*)(size_t (&owner) + member_offset);
	    return member;
	}
	
	bool operator== (const MemberHolder<owner_t, member_t> &other) {
	    return (other.member_offset == member_offset);
	}
	
	bool operator!= (const MemberHolder<owner_t, member_t> &other) {
	    return !(*this == other);
	}
    };
    
    template<typename T>
    class PropChangeOp: public UndoOp
    {
    protected:
	Glib::ustring label;

	typedef T           value_t;
	typedef Property<T> property_t;
	
	value_t old_val, new_val;
	
    protected:
	PropChangeOp (const Glib::ustring &label_,
		      property_t          &prop,
		      const value_t       &new_val_) :
	    label (label_),
	    old_val (prop.get_val ()),
	    new_val (new_val_) {}
	
    public:
	virtual ~PropChangeOp () {};
	
	Glib::ustring get_label () const {
	    return label;
	}
	
	void undo () {
	    property_t &prop = get_prop ();
	    prop = old_val;
	}
	
	void redo () {
	    property_t &prop = get_prop ();
	    prop = new_val;
	}
	
    protected:
	virtual property_t & get_prop () const = 0;
    };
    
    template<typename T>
    class PropChangeOpFactory
    {
    public:
	virtual ~PropChangeOpFactory () {};
	
	virtual void push_change (const T &value) = 0;
    };
}

#endif /* !GUIKACHU_PROPERTY_OPS_H */
