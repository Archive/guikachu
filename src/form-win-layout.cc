//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "form-win.h"
#include "undo.h"

#include <glib/gi18n.h>

#include <list>

using namespace Guikachu;
using namespace Guikachu::GUI;

// TODO: Center vertically/horizontally

namespace
{   
    struct WidgetPosition
    {
	serial_t widget_serial;
	int      old_x, old_y;
	int      new_x, new_y;

	WidgetPosition (Widget *widget,
			int     new_x,
			int     new_y);
    };
    
    typedef std::list<WidgetPosition> widget_positions_t;

    enum AlignType
    {
	ALIGN_LEFT,
	ALIGN_RIGHT,
	ALIGN_TOP,
	ALIGN_BOTTOM
    };
    
    class AlignWidgetsOp: public UndoOp
    {
	ResourceManager    *manager;
	serial_t            form_serial;
	widget_positions_t  positions;

	Glib::ustring       op_label;

    public:
	AlignWidgetsOp (const std::set<Widget*> &widgets,
			AlignType                align_type,
			int                      align_target);

	Glib::ustring get_label () const { return op_label; };
	
	void undo ();
	void redo ();
    };

    enum CenterType
    {
        CENTER_HORIZONTAL,
        CENTER_VERTICAL
    };
    
    class CenterWidgetsOp: public UndoOp
    {
        ResourceManager    *manager;
        serial_t            form_serial;
        widget_positions_t  positions;

        Glib::ustring op_label;

    public:
        CenterWidgetsOp (const std::set<Widget*> &widgets,
                         CenterType               center_type);

        Glib::ustring get_label () const { return op_label; };

        void undo ();
        void redo ();
    };
    
} // anonymous namespace

WidgetPosition::WidgetPosition (Widget *widget,
				int     new_x_,
				int     new_y_) :
    widget_serial (widget->get_serial ()),
    old_x (widget->x),
    old_y (widget->y),
    new_x (new_x_),
    new_y (new_y_)
{
    widget->x = new_x;
    widget->y = new_y;
}

AlignWidgetsOp::AlignWidgetsOp (const std::set<Widget*> &widgets,
				AlignType                align_type,
				int                      align_target) :
    manager ((*widgets.begin ())->get_manager ()),
    form_serial ((*widgets.begin ())->get_form ()->get_serial ())
{
    char *str = g_strdup_printf (_("Align widgets of %s"),
				 (*widgets.begin ())->get_form ()->id ().c_str ());
    op_label = str;
    g_free (str);

    for (std::set<Widget*>::const_iterator i = widgets.begin ();
	 i != widgets.end (); ++i)
    {
	Widget *widget = *i;
	
	int new_x = widget->x;
	int new_y = widget->y;

	switch (align_type)
	{
	case ALIGN_LEFT:
	    new_x = align_target;
	    break;
	case ALIGN_RIGHT:
	    new_x = align_target - widget->get_width ();
	    break;
	case ALIGN_TOP:
	    new_y = align_target;
	    break;
	case ALIGN_BOTTOM:
	    new_y = align_target - widget->get_height ();
	    break;
	}
	
	WidgetPosition pos_entry (widget, new_x, new_y);
	positions.push_back (pos_entry);
    }
}

void AlignWidgetsOp::undo ()
{
    Resources::Form *form = static_cast<Resources::Form*> (manager->get_resource (form_serial));
    for (widget_positions_t::const_iterator i = positions.begin ();
	 i != positions.end (); ++i)
    {
	Widget *widget = form->get_widget (i->widget_serial);
	widget->x = i->old_x;
	widget->y = i->old_y;
    }
}

void AlignWidgetsOp::redo ()
{
    Resources::Form *form = static_cast<Resources::Form*> (manager->get_resource (form_serial));
    for (widget_positions_t::const_iterator i = positions.begin ();
	 i != positions.end (); ++i)
    {
	Widget *widget = form->get_widget (i->widget_serial);
	widget->x = i->new_x;
	widget->y = i->new_y;
    }    
}

CenterWidgetsOp::CenterWidgetsOp (const std::set<Widget*> &widgets,
                                  CenterType               center_type) :
    manager ((*widgets.begin ())->get_manager ()),
    form_serial ((*widgets.begin ())->get_form ()->get_serial ())
{
    Resources::Form *form = (*widgets.begin ())->get_form ();
    
    char *str;

    if (widgets.size () > 1)
        str = g_strdup_printf (
            center_type == CENTER_HORIZONTAL ? _("Center widgets of %s horizontally") : _("Center widgets of %s vertically"),
            form->id ().c_str ());
    else
        str = g_strdup_printf (
            center_type == CENTER_HORIZONTAL ? _("Center %s horizontally") : _("Center %s vertically"),
            (*widgets.begin ())->id ().c_str ());
            
    op_label = str;
    g_free (str);

    for (std::set<Widget*>::const_iterator i = widgets.begin ();
	 i != widgets.end (); ++i)
    {
	Widget *widget = *i;
	
	int new_x = widget->x;
	int new_y = widget->y;

	switch (center_type)
	{
	case CENTER_HORIZONTAL:
	    new_x = (form->width - widget->get_width ()) / 2;
	    break;
	case CENTER_VERTICAL:
	    new_y = (form->height - widget->get_height ()) / 2;
            break;
	}
	
	WidgetPosition pos_entry (widget, new_x, new_y);
	positions.push_back (pos_entry);
    }
}

void CenterWidgetsOp::undo ()
{
    Resources::Form *form = static_cast<Resources::Form*> (manager->get_resource (form_serial));
    for (widget_positions_t::const_iterator i = positions.begin ();
	 i != positions.end (); ++i)
    {
	Widget *widget = form->get_widget (i->widget_serial);
	widget->x = i->old_x;
	widget->y = i->old_y;
    }
}

void CenterWidgetsOp::redo ()
{
    Resources::Form *form = static_cast<Resources::Form*> (manager->get_resource (form_serial));
    for (widget_positions_t::const_iterator i = positions.begin ();
	 i != positions.end (); ++i)
    {
	Widget *widget = form->get_widget (i->widget_serial);
	widget->x = i->new_x;
	widget->y = i->new_y;
    }    
}

namespace
{
    bool compare_left (Guikachu::Widget *p, Guikachu::Widget *q)
    {
	return p->x < q->x;
    }

    bool compare_top (Guikachu::Widget *p, Guikachu::Widget *q)
    {
	return p->y < q->y;
    }

    bool compare_right (Guikachu::Widget *p, Guikachu::Widget *q)
    {
	return (p->x + p->get_width ()) < (q->x + q->get_width ());
    }

    bool compare_bottom (Guikachu::Widget *p, Guikachu::Widget *q)
    {
	return (p->y + p->get_height ()) < (q->y + q->get_height ());
    }
} // anonymous namespace

void FormWindow::align_left ()
{
    selection_t::const_iterator leftmost =
	std::min_element (selection.begin (), selection.end (), compare_left);

    int left = (*leftmost)->x;
    res->get_manager ()->get_undo_manager ().push (
	new AlignWidgetsOp (selection, ALIGN_LEFT, left));
}

void FormWindow::align_right ()
{
    selection_t::const_iterator rightmost =
	std::max_element (selection.begin (), selection.end (), compare_right);

    int right = (*rightmost)->x + (*rightmost)->get_width ();
    res->get_manager ()->get_undo_manager ().push (
	new AlignWidgetsOp (selection, ALIGN_RIGHT, right));
}

void FormWindow::align_top ()
{
    selection_t::const_iterator topmost =
	std::min_element (selection.begin (), selection.end (), compare_top);
    
    int top = (*topmost)->y;
    res->get_manager ()->get_undo_manager ().push (
	new AlignWidgetsOp (selection, ALIGN_TOP, top));
}

void FormWindow::align_bottom ()
{
    selection_t::const_iterator bottommost =
	std::max_element (selection.begin (), selection.end (), compare_bottom);
    
    int bottom = (*bottommost)->y + (*bottommost)->get_height ();
    res->get_manager ()->get_undo_manager ().push (
	new AlignWidgetsOp (selection, ALIGN_BOTTOM, bottom));
}

void FormWindow::center_horizontal ()
{
    res->get_manager ()->get_undo_manager ().push (
        new CenterWidgetsOp (selection, CENTER_HORIZONTAL));
}

void FormWindow::center_vertical ()
{
    res->get_manager ()->get_undo_manager ().push (
        new CenterWidgetsOp (selection, CENTER_VERTICAL));
}
