//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <glib/gi18n.h>

#include "mainwin.h"

#include <gtkmm/stock.h>

#include <gtkmm/action.h>
#include <gtkmm/toggleaction.h>
#include <gtkmm/radioaction.h>
#include <gtkmm/uimanager.h>

#include <list>

#include "preferences.h"
#include "ui-gui.h"

using namespace Guikachu;

namespace {
    Glib::ustring escape_underscores (const Glib::ustring &text);
} // Anonymous namespace

#define GN_(s) dgettext ("gtk20", (s))

void GUI::MainWin::create_menus ()
{
    // File menu
    Glib::RefPtr<Gtk::Action> action;
    main_group->add (Gtk::Action::create ("menu_file", GN_("_File")));
    action = Gtk::Action::create ("new", Gtk::Stock::NEW, _("New project"),
                                  _("Create a new project"));
    action->property_short_label () = _("New");
    main_group->add (action,
                     sigc::mem_fun (*this, &MainWin::new_cb));
    main_group->add (Gtk::Action::create ("open", Gtk::Stock::OPEN, "",
                                          _("Open a file")),
                     sigc::mem_fun (*this, &MainWin::load_cb));
    main_group->add (Gtk::Action::create ("save", Gtk::Stock::SAVE, "",
                                          _("Save the current file")),
                     sigc::mem_fun (*this, &MainWin::save_cb));
    main_group->add (Gtk::Action::create ("save_as", Gtk::Stock::SAVE_AS, "",
                                          _("Save the current file with a different name")),
                     Gtk::AccelKey ("<control><shift>S"),
                     sigc::mem_fun (*this, &MainWin::save_as_cb));
    main_group->add (Gtk::Action::create ("export_rcp", Gtk::Stock::CONVERT,
                                          _("_Export RCP..."),
                                          _("Create an RCP file for compilation")),
                     sigc::mem_fun (*this, &MainWin::export_cb));
    action = Gtk::Action::create ("add_resource", Gtk::Stock::ADD,
                                  _("Add _Resource..."),
                                  _("Add new component to current project"));
    action->property_short_label () = _("Add");
    action->property_is_important () = true;
    main_group->add (action,
                     Gtk::AccelKey ("<control>a"),
                     sigc::mem_fun (*this, &MainWin::add_cb));
    main_group->add (Gtk::Action::create ("exit", Gtk::Stock::QUIT, "",
                                          _("Quit the application")),
                     sigc::mem_fun (*this, &MainWin::exit_cb));
    
    // Edit menu
    main_group->add (Gtk::Action::create ("menu_edit", GN_("_Edit")));
    main_group->add (Gtk::Action::create ("edit_resource", Gtk::Stock::EDIT, ""),
                     sigc::mem_fun (*this, &MainWin::edit_res_cb));
    action = Gtk::Action::create ("undo", Gtk::Stock::UNDO);
    action->property_short_label () = _("Undo");
    main_group->add (action, Gtk::AccelKey ("<control>z"),
                     sigc::mem_fun (*this, &MainWin::undo_cb));
    action = Gtk::Action::create ("redo", Gtk::Stock::REDO);
    action->property_short_label () = _("Redo");
    main_group->add (action, Gtk::AccelKey ("<control><shift>z"),
                     sigc::mem_fun (*this, &MainWin::redo_cb));
    main_group->add (Gtk::Action::create ("cut", Gtk::Stock::CUT, "",
                                          _("Cut the selection")),
                     sigc::mem_fun (*this, &MainWin::cut_cb));
    main_group->add (Gtk::Action::create ("copy", Gtk::Stock::COPY, "",
                                          _("Copy the selection")),
                     sigc::mem_fun (*this, &MainWin::copy_cb));
    main_group->add (Gtk::Action::create ("paste", Gtk::Stock::PASTE, "",
                                          _("Paste the clipboard")),
                     sigc::mem_fun (*this, &MainWin::paste_cb));
    main_group->add (Gtk::Action::create ("duplicate", _("_Duplicate"),
                                          _("Duplicate the selected resource")),
                     Gtk::AccelKey ("<control>u"),
                     sigc::mem_fun (*this, &MainWin::duplicate_cb));
    main_group->add (Gtk::Action::create ("remove", Gtk::Stock::DELETE, _("_Remove"),
                                          _("Remove the selected resource from the project")),
                     Gtk::AccelKey ("Delete"),
                     sigc::mem_fun (*this, &MainWin::remove_cb));
    main_group->add (Gtk::Action::create ("preferences", Gtk::Stock::PREFERENCES, "",
                                          _("Configure the application")),
                     sigc::mem_fun (*this, &MainWin::preferences_cb));

    // View menu
    main_group->add (Gtk::Action::create ("menu_view", _("_View")));
    main_group->add (Gtk::ToggleAction::create ("toggle_toolbar", _("_Toolbar"),
                                                _("Toggles the main window's toolbar"),
                                                true),
                     sigc::mem_fun (*this, &MainWin::toggle_toolbar_cb));
    Gtk::RadioAction::Group view_mode_group;
    main_group->add (Gtk::RadioAction::create (view_mode_group,
                                               "resources_flat", _("Sort by _ID"),
                                               _("Show resources in a sorted list")),
                     sigc::mem_fun (*this, &MainWin::viewmode_cb));
    main_group->add (Gtk::RadioAction::create (view_mode_group,
                                               "resources_tree", _("_Group by type"),
                                               _("Show resources grouped by their type")));
    
    // Help menu
    main_group->add (Gtk::Action::create ("menu_help", GN_("_Help")));
    main_group->add (Gtk::Action::create ("about", Gtk::Stock::ABOUT, "",
                                          _("About this application")),
                     sigc::bind (sigc::ptr_fun (&UI::show_about), this));
    
    static const char *ui_description =
        "<ui>"
        "  <menubar name='MainMenu'>"
        "    <menu action='menu_file'>"
        "      <menuitem action='new'/>"
        "      <menuitem action='open'/>"
        "      <separator/>"        
        "      <menuitem action='save'/>"
        "      <menuitem action='save_as'/>"
        "      <menuitem action='export_rcp'/>"
        "      <separator/>"        
        "      <menuitem action='add_resource'/>"
        "      <separator/>"
        "      <placeholder name='recent_files'/>"
        "      <separator/>"
        "      <menuitem action='exit'/>"
        "    </menu>"
        "    <menu action='menu_edit'>"
        "      <menuitem action='undo'/>"
        "      <menuitem action='redo'/>"
        "      <separator/>"
        "      <menuitem action='cut'/>"
        "      <menuitem action='copy'/>"
        "      <menuitem action='paste'/>"
        "      <separator/>"        
        "      <menuitem action='duplicate'/>"
        "      <menuitem action='remove'/>"
        "      <separator/>"        
        "      <menuitem action='preferences'/>"
        "    </menu>"
        "    <menu action='menu_view'>"
        "      <menuitem action='toggle_toolbar'/>"
        "      <separator/>"
        "      <menuitem action='resources_flat'/>"
        "      <menuitem action='resources_tree'/>"
        "    </menu>"
        "    <menu action='menu_help'>"
        "      <menuitem action='about'/>"
        "    </menu>"
        "  </menubar>"
        "  <toolbar name='MainToolbar'>"
        "    <toolitem action='add_resource'/>"
        "    <separator/>"
        "    <toolitem action='new'/>"
        "    <toolitem action='open'/>"
        "    <toolitem action='save'/>"
        "    <separator/>"
        "    <toolitem action='undo'/>"
        "    <toolitem action='redo'/>"
        "  </toolbar>"
        "  <popup name='ResourceTreePopup'>"
        "    <menuitem action='edit_resource'/>"
        "    <separator/>"        
        "    <menuitem action='cut'/>"
        "    <menuitem action='copy'/>"
        "    <menuitem action='paste'/>"
        "    <separator/>"        
        "    <menuitem action='duplicate'/>"
        "    <menuitem action='remove'/>"
        "  </popup>"
        "</ui>";
    
    ui_manager->signal_connect_proxy ().connect (sigc::mem_fun (*this, &MainWin::setup_statusbar_tooltip));
    
    ui_manager->insert_action_group (main_group);
    ui_manager->add_ui_from_string (ui_description);
    add_accel_group (ui_manager->get_accel_group ());
 
    signal_window_state_event ().connect (
        sigc::bind_return (sigc::mem_fun (*this, &MainWin::update_resize_grip), false));
    
    // Listen for changes in recent file list
    Preferences::Interface::recent_files_changed.connect (
	sigc::mem_fun (*this, &MainWin::update_recent_files));
    update_recent_files ();
}

void GUI::MainWin::update_recent_files ()
{
    // Clear old entries
    if (recent_group)
    {
        ui_manager->remove_action_group (recent_group);
        ui_manager->remove_ui (recent_merge_id);
    }
        
    recent_group = Gtk::ActionGroup::create ();    
    recent_merge_id = ui_manager->new_merge_id ();
    
    Preferences::Interface::RecentFiles recent_files =
	Preferences::Interface::get_recent_files ();
    recent_files_num = recent_files.size ();
    
    if (recent_files_num)
    {
	Preferences::Interface::RecentFiles::const_iterator i;
	int num;
	for (i = recent_files.begin (), num = 0;
	     i != recent_files.end (); i++, num++)
	{
	    Glib::ustring vis_filename = escape_underscores (UI::visible_filename (*i));
	    Glib::ustring menu_label;
            Glib::ustring action_id;
	    
	    if (num < 9)
		menu_label = Glib::ScopedPtr<char> (
		    g_strdup_printf ("_%d %s", num + 1, vis_filename.c_str ())).get ();
	    else
		menu_label = Glib::ScopedPtr<char> (
		    g_strdup_printf ("%d %s", num + 1, vis_filename.c_str ())).get ();
            action_id = Glib::ScopedPtr<char> (
                g_strdup_printf ("recent%d", num)).get ();
            
            recent_group->add (Gtk::Action::create (action_id, menu_label),
                               sigc::bind (sigc::mem_fun (*this, &MainWin::open), *i));
            
            ui_manager->add_ui (recent_merge_id, "/MainMenu/menu_file/recent_files", action_id, action_id,
                                Gtk::UI_MANAGER_AUTO, false);
	}
    }
    
    ui_manager->insert_action_group (recent_group);
    ui_manager->ensure_update ();
}

void GUI::MainWin::update_undo ()
{
    UndoManager &undo_manager = manager->get_undo_manager ();
    
    std::list<Glib::ustring> undo_labels = undo_manager.get_undo_labels ();
    std::list<Glib::ustring> redo_labels = undo_manager.get_redo_labels ();
    
    std::string top_undo_label = undo_labels.size () ?
	escape_underscores (undo_labels.front ()) : _("No operation");
    std::string top_undo_tooltip = undo_labels.size () ? undo_labels.front () : _("No operation");
    
    std::string top_redo_label = redo_labels.size () ?
	escape_underscores (redo_labels.front ()) : _("No operation");
    std::string top_redo_tooltip = redo_labels.size () ? redo_labels.front () : _("No operation");

    Glib::ScopedPtr<char> undo_label   (g_strdup_printf (_("_Undo: %s"), top_undo_label.c_str ()));
    Glib::ScopedPtr<char> undo_tooltip (g_strdup_printf (_("Undo: %s"),  top_undo_tooltip.c_str ()));
    main_group->get_action ("undo")->property_label () = undo_label.get ();
    main_group->get_action ("undo")->property_tooltip () = undo_tooltip.get ();
    main_group->get_action ("undo")->set_sensitive (undo_labels.size ());

    Glib::ScopedPtr<char> redo_label   (g_strdup_printf (_("_Redo: %s"), top_redo_label.c_str ()));
    Glib::ScopedPtr<char> redo_tooltip (g_strdup_printf (_("Redo: %s"),  top_redo_tooltip.c_str ()));    
    main_group->get_action ("redo")->property_label () = redo_label.get ();
    main_group->get_action ("redo")->property_tooltip () = redo_tooltip.get ();
    main_group->get_action ("redo")->set_sensitive (redo_labels.size ());

    ui_manager->ensure_update ();
}

void GUI::MainWin::setup_statusbar_tooltip (const Glib::RefPtr<Gtk::Action> &action,
                                            Gtk::Widget                     *widget)
{
    Gtk::Item *item = dynamic_cast<Gtk::Item*> (widget);
    if (item)
    {
        item->signal_select ().connect (
            sigc::bind (sigc::mem_fun (*this, &MainWin::menuitem_focus_cb), action));
        item->signal_deselect ().connect (
            sigc::bind (sigc::mem_fun (statusbar, &Gtk::Statusbar::pop), guint(STATUSBAR_MENU_TIP)));
    }
}

void GUI::MainWin::menuitem_focus_cb (const Glib::RefPtr<Gtk::Action> &action)
{
    statusbar.push (action->property_tooltip (), STATUSBAR_MENU_TIP);
}
    
void GUI::MainWin::update_resize_grip (GdkEventWindowState *event)
{
    if (event->changed_mask & (GDK_WINDOW_STATE_MAXIMIZED | GDK_WINDOW_STATE_FULLSCREEN))
        statusbar.set_has_resize_grip (
            !(event->new_window_state & (GDK_WINDOW_STATE_MAXIMIZED | GDK_WINDOW_STATE_FULLSCREEN)));
}

void GUI::MainWin::tree_menu_cb (guint button, guint32 time)
{
    Gtk::Widget *popup_menu_widget = ui_manager->get_widget ("/ResourceTreePopup");
    Gtk::Menu   *popup_menu = dynamic_cast<Gtk::Menu*> (popup_menu_widget);
    g_return_if_fail (popup_menu);

    popup_menu->popup (button, time);
}

namespace {

Glib::ustring escape_underscores (const Glib::ustring &text)
{
    // Change _ to __ to avoid underscore accelerators

    Glib::ustring escaped_text;
    escaped_text.reserve (text.length () * 2);
    
    for (Glib::ustring::const_iterator i = text.begin (); i != text.end (); ++i)
	if (*i == '_')
	    escaped_text += "__";
	else
	    escaped_text += *i;
	    
    return escaped_text;
}
    
} // Anonymous namespace
