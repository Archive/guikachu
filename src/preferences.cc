//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "preferences.h"

namespace {
    void init ();

    int get_conf_int   (const std::string &path, const std::string &key, int default_value = 0);
    bool get_conf_bool (const std::string &path, const std::string &key, bool default_value = false);
    
    float get_conf_float (const std::string &path, const std::string &key, float default_value = 0);
    void set_conf_float  (const std::string &path, const std::string &key, float value);

    std::string get_conf_string (const std::string &path, const std::string &key, const std::string &default_value = "");
    void set_conf_string (const std::string &path, const std::string &key, const std::string &value);

    typedef Guikachu::Preferences::Interface::RecentFiles FileList;
    
    FileList get_conf_filelist (const std::string &path, const std::string &key);
    void set_conf_filelist     (const std::string &path, const std::string &key, const FileList &value);
} // anonymous namespace

#include "config.h"

#ifdef GUIKACHU_CONFIG_SYSTEM_GCONF
#include "preferences-gconf.cc"
#else
#ifdef GUIKACHU_CONFIG_SYSTEM_WINREG
#include "preferences-winreg.cc"
#else
#error "Unknown config system"
#endif
#endif


#include <algorithm>

using namespace Guikachu;

sigc::signal0<void> Preferences::Interface::recent_files_changed;

Preferences::Interface::RecentFiles Preferences::Interface::get_recent_files ()
{
    return get_conf_filelist ("Interface", "recent_files");
}

void Preferences::Interface::add_recent_file (const Glib::ustring &uri)
{
    RecentFiles recent_list = get_recent_files ();

    // Remove latest addition from list
    RecentFiles::iterator old_pos =
	std::find (recent_list.begin (), recent_list.end (), uri);
    if (old_pos != recent_list.end ())
	recent_list.erase (old_pos);

    // Prepend latest addition to list
    recent_list.push_front (uri);
    
    // Truncate list
    int recent_list_size = get_conf_int ("Interface", "recent_files_size", 4);
    int pos;
    RecentFiles::iterator truncate_begin;

    for (pos = 0, truncate_begin = recent_list.begin ();
	 pos < recent_list_size && truncate_begin != recent_list.end ();
	 pos++, truncate_begin++)
	;

    if (truncate_begin != recent_list.end ())
	recent_list.erase (truncate_begin, recent_list.end ());

    set_conf_filelist ("Interface", "recent_files", recent_list);
    recent_files_changed.emit ();
}

int Preferences::Interface::get_undo_size ()
{
    return get_conf_int ("Interface", "undo_size", 20);
}

bool Preferences::MainWin::get_default_grouped ()
{
    return get_conf_bool ("MainWin", "default_grouped", true);
}


sigc::signal0<void> Preferences::FormEditor::colors_changed;

#define CONF_COLORS_ROOT "FormEditor/Colors"

void Preferences::FormEditor::set_colors (const std::string &color_fg,
					  const std::string &color_disabled,
					  const std::string &color_bg,
					  const std::string &color_selection)
{
#define SET_COLOR(key,value) set_conf_string (CONF_COLORS_ROOT, key, value)
    
    SET_COLOR("foreground", color_fg);
    SET_COLOR("foreground_disabled", color_disabled);
    SET_COLOR("background", color_bg);
    SET_COLOR("selection", color_selection);

#undef SET_COLOR
    colors_changed.emit ();
}

std::string Preferences::FormEditor::get_color_fg ()
{
    return get_conf_string (CONF_COLORS_ROOT, "foreground", "#000000");
}

std::string Preferences::FormEditor::get_color_disabled ()
{
    return get_conf_string (CONF_COLORS_ROOT, "foreground_disabled", "#757575");
}

std::string Preferences::FormEditor::get_color_bg ()
{
    return get_conf_string (CONF_COLORS_ROOT, "background", "#90b398");
}

std::string Preferences::FormEditor::get_color_selection ()
{
    return get_conf_string (CONF_COLORS_ROOT, "selection", "#ecffa8");
}

float Preferences::FormEditor::get_default_zoom ()
{
    return get_conf_float ("FormEditor", "default_zoom", 1.0);
}

void Preferences::FormEditor::set_default_zoom (float default_zoom)
{
    set_conf_float ("FormEditor", "default_zoom", default_zoom);
}
