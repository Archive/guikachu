//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "dialog-win-helpers.h"

#include <glib/gi18n.h>

#include <gtkmm/stock.h>
#include <gtkmm/button.h>
#include <gtkmm/box.h>
#include <gtkmm/image.h>
#include <gtkmm/label.h>

#include "property-ops-resource.h"

using namespace Guikachu;
using namespace Guikachu::GUI::DialogWindow_Helpers;

ButtonList::ButtonList (Resources::Dialog *res_):
    StringList (res_->buttons, new ButtonChangeOpFactory (res_)),
    res (res_)
{
    // Create "Default" button
    Gtk::Button *button_default = new Gtk::Button;
    button_default->signal_clicked ().connect (sigc::mem_fun (*this, &ButtonList::button_default_cb));
    
    Gtk::Box *box = new Gtk::HBox;
    box->add (*manage (new Gtk::Image (
        render_icon (Gtk::StockID (Gtk::Stock::YES), Gtk::ICON_SIZE_BUTTON))));
    box->add (*manage (new Gtk::Label (_("D_efault"), true)));
    button_default->add (*manage (box));

    add_button (*manage (button_default));
}

void ButtonList::button_default_cb ()
{
    Gtk::TreeModel::iterator iter = treeview.get_selection ()->get_selected ();
    g_return_if_fail (iter);
    Gtk::TreeModel::Path path = treestore->get_path (iter);

    unsigned int index = path[0];

    ResourceOps::PropChangeOpFactory<unsigned int> op_factory (
        _("Change default button of %s"), res, res->default_button, true);    
    op_factory.push_change (index);
}

void ButtonList::create_columns (Gtk::TreeModel::ColumnRecord &columns)
{
    StringList::create_columns (columns);
    columns.add (col_def);
}

void ButtonList::create_view_columns (int &col_text_num)
{
    treeview.append_column ("", col_def);
    
    StringList::create_view_columns (col_text_num);
    ++col_text_num;
}

void ButtonList::update_row (Gtk::TreeRow &row, value_t::size_type i)
{
    StringList::update_row (row, i);
    
    if (i == res->default_button)
        row[col_def] = render_icon (Gtk::StockID (Gtk::Stock::YES), Gtk::ICON_SIZE_MENU);
    else
        row[col_def] = Glib::RefPtr<Gdk::Pixbuf>();
}

std::string ButtonList::new_item ()
{
    return _("New button");
}



ButtonChangeOpFactory::ButtonChangeOpFactory (Resources::Dialog *res_) :
    StringListOpFactory (_("Add \"%s\" button to %s"),
			 _("Remove \"%s\" button from %s"),
			 _("Change \"%s\" button in %s"),
			 _("Move \"%s\" button in %s"),
			 res_,
			 res_->buttons),
    res (res_)
{
}

void ButtonChangeOpFactory::push_add (index_t new_index, const item_t &new_button)
{
    value_t val = res->buttons;
    index_t old_default = res->default_button;
    index_t new_default = old_default;
    
    if (old_default >= new_index)
	new_default += 1;

    char *label_str = g_strdup_printf (add_template.c_str (),
				       new_button.c_str (),
				       res->id ().c_str ());
    UndoOp *op = new AddOp (label_str, res, new_index, new_button, old_default, new_default);
    g_free (label_str);

    if (new_index >= val.size ())
	val.push_back (new_button);
    else
	val.insert (val.begin () + new_index, new_button);
    
    res->buttons = val;
    res->default_button = new_default;
    undo_manager.push (op);    
}

void ButtonChangeOpFactory::push_remove (index_t index)
{
    value_t val = res->buttons;
    index_t old_default = res->default_button;
    index_t new_default = old_default;
    
    if (old_default != 0 && old_default >= index)
	new_default -= 1;

    char *label_str = g_strdup_printf (remove_template.c_str (),
				       val[index].c_str (),
				       res->id ().c_str ());
    UndoOp *op = new RemoveOp (label_str, res, index, old_default, new_default);
    g_free (label_str);

    val.erase (val.begin () + index);
    
    res->buttons = val;
    res->default_button = new_default;
    undo_manager.push (op);
}

void ButtonChangeOpFactory::push_move (index_t old_index, index_t new_index)
{
    value_t val = res->buttons;
    index_t old_default = res->default_button;
    index_t new_default = old_default;
    
    if (old_index == old_default)
	new_default = new_index;
    else
	if (new_index == old_default)
	    new_default = old_index;
    
    char *label_str = g_strdup_printf (move_template.c_str (),
				       val[old_index].c_str (),
				       res->id ().c_str ());
    UndoOp *op = new MoveOp (label_str, res, old_index, new_index, old_default, new_default);
    g_free (label_str);
    
    value_t::iterator old_i = val.begin () + old_index;
    value_t::iterator new_i = val.begin () + new_index;
    iter_swap (old_i, new_i);
    
    res->buttons = val;
    res->default_button = new_default;
    undo_manager.push (op);
}



ButtonChangeOpFactory::ButtonOp::ButtonOp (const Glib::ustring &op_label_,
					   Resources::Dialog   *res,
					   index_t              old_default_,
					   index_t              new_default_) :
    op_label (op_label_),
    manager (res->get_manager ()),
    resource_id (res->id),
    old_default (old_default_),
    new_default (new_default_)
{
}

Resources::Dialog * ButtonChangeOpFactory::ButtonOp::get_dialog () const
{
    Resource *res = manager->get_resource (resource_id);
    Resources::Dialog *dialog = static_cast<Resources::Dialog*> (res);
    g_assert (dialog);

    return dialog;
}


ButtonChangeOpFactory::AddOp::AddOp (const Glib::ustring &op_label,
				     Resources::Dialog   *res,
				     index_t              index_,
				     const item_t        &button_,
				     index_t              old_default,
				     index_t              new_default) :
    ButtonOp (op_label, res, old_default, new_default),
    index (index_),
    button (button_)
{
}

void ButtonChangeOpFactory::AddOp::undo ()
{
    Resources::Dialog *res = get_dialog ();
    value_t buttons = res->buttons;

    buttons.erase (buttons.begin () + index);

    res->buttons = buttons;
    res->default_button = old_default;
}

void ButtonChangeOpFactory::AddOp::redo ()
{
    Resources::Dialog *res = get_dialog ();
    value_t buttons = res->buttons;

    if (index >= buttons.size ())
	buttons.push_back (button);
    else
	buttons.insert (buttons.begin () + index, button);

    res->buttons = buttons;
    res->default_button = new_default;
}


ButtonChangeOpFactory::RemoveOp::RemoveOp (const Glib::ustring &op_label,
					   Resources::Dialog   *res,
					   index_t              index_,
					   index_t              old_default,
					   index_t              new_default) :
    ButtonOp (op_label, res, old_default, new_default),
    index (index_),
    button (res->buttons()[index])
{
}

void ButtonChangeOpFactory::RemoveOp::undo ()
{
    Resources::Dialog *res = get_dialog ();
    value_t buttons = res->buttons;

    buttons.insert (buttons.begin () + index, button);

    res->buttons = buttons;
    res->default_button = old_default;
}

void ButtonChangeOpFactory::RemoveOp::redo ()
{
    Resources::Dialog *res = get_dialog ();
    value_t buttons = res->buttons;

    buttons.erase (buttons.begin () + index);

    res->buttons = buttons;
    res->default_button = new_default;
}


ButtonChangeOpFactory::MoveOp::MoveOp (const Glib::ustring &op_label,
				       Resources::Dialog   *res,
				       index_t              old_index,
				       index_t              new_index,
				       index_t              old_default,
				       index_t              new_default) :
    ButtonOp (op_label, res, old_default, new_default)
{
    index_history.push_back (old_index);
    index_history.push_back (new_index);
}

ButtonChangeOpFactory::MoveOp::MoveOp (const Glib::ustring &op_label,
				       Resources::Dialog   *res,
				       const index_list_t  &index_history_head,
				       const index_list_t  &index_history_tail,
				       index_t              old_default,
				       index_t              new_default) :
    ButtonOp (op_label, res, old_default, new_default),
    index_history (index_history_head)
{
    index_list_t::const_iterator tail_begin = index_history_tail.begin ();
    index_list_t::const_iterator tail_end = index_history_tail.end ();

    g_assert (index_history_tail.front () == index_history_head.back ());
    
    index_history.insert (index_history.end (), ++tail_begin, tail_end);
}



void ButtonChangeOpFactory::MoveOp::undo ()
{
    // Undo/redo may seem patently stupid and over-complicated, but
    // it's the only way to make cascading work

    Resources::Dialog *res = get_dialog ();
    value_t buttons = res->buttons;

    index_list_t::const_reverse_iterator rbegin = index_history.rbegin ();
    index_list_t::const_reverse_iterator rend = index_history.rend ();
    index_list_t::const_reverse_iterator curr, next;
    
    for (curr = rbegin, next = ++rbegin; next != rend; ++curr, ++next)
	std::iter_swap (buttons.begin () + *curr, buttons.begin () + *next);

    res->buttons = buttons;
    res->default_button = old_default;
}

void ButtonChangeOpFactory::MoveOp::redo ()
{
    Resources::Dialog *res = get_dialog ();
    value_t buttons = res->buttons;

    index_list_t::const_iterator begin = index_history.begin ();
    index_list_t::const_iterator end = index_history.end ();
    index_list_t::const_iterator curr, next;
    
    for (curr = begin, next = ++begin; next != end; ++curr, ++next)
	std::iter_swap (buttons.begin () + *curr, buttons.begin () + *next);

    res->buttons = buttons;
    res->default_button = old_default;
}

UndoOp * ButtonChangeOpFactory::MoveOp::combine (UndoOp *other_op) const
{
    ButtonChangeOpFactory::MoveOp *op = dynamic_cast<ButtonChangeOpFactory::MoveOp*> (other_op);
    if (!op)
	return 0;
    
    if (op->resource_id != resource_id)
	return 0;

    if (op->index_history.front () != index_history.back ())
	return 0;
    
    Resources::Dialog *dialog = get_dialog ();
    
    UndoOp *new_op = new MoveOp (get_label (), dialog, index_history, op->index_history,
				 old_default, op->new_default);
    
    return new_op;
}



DialogTypeCombo::DialogTypeCombo (Resources::Dialog *res_):
    res (res_),
    update_block (false)
{
    Gtk::TreeModel::ColumnRecord cols;
    cols.add (col_type);
    cols.add (col_label);
    
    store = Gtk::ListStore::create (cols);

    // Fill the model
    add_type (_("Information"),  Resources::Dialog::TYPE_INFORMATION);
    add_type (_("Confirmation"), Resources::Dialog::TYPE_CONFIRMATION);
    add_type (_("Warning"),      Resources::Dialog::TYPE_WARNING);
    add_type (_("Error"),        Resources::Dialog::TYPE_ERROR);

    // Set the view
    set_model (store);
    pack_start (col_label);

    signal_changed ().connect (sigc::mem_fun (*this, &DialogTypeCombo::changed_cb));
    res->dialog_type.changed.connect (sigc::mem_fun (*this, &DialogTypeCombo::update));
    update ();
}

void DialogTypeCombo::add_type (const Glib::ustring &label, Resources::Dialog::DialogType type)
{
    Gtk::TreeRow row = *(store->append ());
    row[col_type] = type;
    row[col_label] = label;
}

void DialogTypeCombo::update ()
{
    Resources::Dialog::DialogType type = res->dialog_type;
    
    Gtk::TreeModel::iterator iter = store->get_iter ("0");
    while ((*iter)[col_type] != type)
        ++iter;

    update_block = true;
    set_active (iter);
    update_block = false;
}

void DialogTypeCombo::changed_cb ()
{
    if (update_block)
        return;
    
    Gtk::TreeModel::iterator iter = get_active ();
    g_return_if_fail (iter);

    char *label_str = g_strdup_printf (_("Change type of %s"), res->id ().c_str ());
    
    UndoOp *op = new ResourceOps::PropChangeOp<Resources::Dialog::DialogType> (
	label_str, res, res->dialog_type, (*iter)[col_type], false);
    g_free (label_str);
    
    res->dialog_type = (*iter)[col_type];

    res->get_manager ()->get_undo_manager ().push (op);
}
