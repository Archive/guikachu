//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_DIALOG_WIN_HELPERS_H
#define GUIKACHU_DIALOG_WIN_HELPERS_H

#include "dialog-res.h"
#include "widgets/string-list.h"
#include <gtkmm/combobox.h>

namespace Guikachu
{
    namespace GUI
    {
        namespace DialogWindow_Helpers
        {
            class ButtonList: public GUI::PropertyEditors::StringList
            {
                Resources::Dialog *res;
                
            public:
                ButtonList (Resources::Dialog *res);
                
            protected:
                Gtk::TreeModelColumn<Glib::RefPtr<Gdk::Pixbuf> > col_def;
                
                void create_columns (Gtk::TreeModel::ColumnRecord &columns);
                void create_view_columns (int &col_text_num);
                void update_row (Gtk::TreeRow &row, value_t::size_type i);
                std::string new_item ();
                
            private:
                void button_default_cb ();
            };
            
            class ButtonChangeOpFactory: public ResourceOps::StringListOpFactory
            {
                Resources::Dialog *res;
                
                class ButtonOp: public UndoOp
                {
                    Glib::ustring    op_label;
                    
                protected:
                    ResourceManager *manager;
                    std::string      resource_id;
                    
                    index_t          old_default, new_default;
                    
                    ButtonOp (const Glib::ustring &op_label,
                              Resources::Dialog   *res,
                              index_t              old_default,
                              index_t              new_default);
                    
                    Resources::Dialog * get_dialog () const;
                    
                public:
                    Glib::ustring get_label () const { return op_label; };
                };
                
                class AddOp: public ButtonOp
                {
                    index_t index;
                    item_t  button;
                    
                public:
                    AddOp (const Glib::ustring &op_label,
                           Resources::Dialog   *res,
                           index_t              index,
                           const item_t        &button,
                           index_t              old_default,
                           index_t              new_default);
                    
                    void undo ();
                    void redo ();
                };
                
                class RemoveOp: public ButtonOp
                {
                    index_t index;
                    item_t  button;
                    
                public:
                    RemoveOp (const Glib::ustring &op_label,
                              Resources::Dialog   *res,
                              index_t              index,
                              index_t              old_default,
                              index_t              new_default);
                    
                    void undo ();
                    void redo ();
                };
                
                class MoveOp: public ButtonOp
                {
                    typedef std::vector<index_t> index_list_t;
                    
                    index_list_t index_history;
                    
                    MoveOp (const Glib::ustring &op_label,
                            Resources::Dialog   *res,
                            const index_list_t  &index_history_head,
                            const index_list_t  &index_history_tail,
                            index_t              old_default,
                            index_t              new_default);
                    
                public:
                    MoveOp (const Glib::ustring &op_label,
                            Resources::Dialog   *res,
                            index_t              old_index,
                            index_t              new_index,
                            index_t              old_default,
                            index_t              new_default);
                    
                    void     undo ();
                    void     redo ();
                    UndoOp * combine (UndoOp *other_op) const;
                };
                
            public:
                ButtonChangeOpFactory (Resources::Dialog *res);
                
                void push_add    (index_t index, const item_t &item);
                void push_remove (index_t index);
                void push_move   (index_t old_index, index_t new_index);
            };
            
            class DialogTypeCombo: public Gtk::ComboBox
            {
                Resources::Dialog *res;
                
                Gtk::TreeModelColumn<Resources::Dialog::DialogType> col_type;
                Gtk::TreeModelColumn<Glib::ustring>                 col_label;
                
                Glib::RefPtr<Gtk::ListStore> store;
                
            public:
                DialogTypeCombo (Resources::Dialog *res);
                
            private:
                bool update_block;
                
                void update ();
                void changed_cb ();

                void add_type (const Glib::ustring &label, Resources::Dialog::DialogType type);
            };
        }
    }
}

#endif /* !GUIKACHU_DIALOG_WIN_HELPERS_H */
    
