//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "app-win.h"

#include <glib/gi18n.h>

#include "target.h"

#include "widgets/entry.h"
#include "widgets/target-combo.h"
#include "widgets/num-entry.h"
#include "widgets/togglebutton.h"

#include <gtkmm/menu.h>
#include <gtkmm/optionmenu.h>

using namespace Guikachu;

namespace
{
    template<typename T>
    class AppPropChangeOp: public PropChangeOp<T>
    {
	typedef T           value_t;
	typedef Property<T> property_t;

	property_t &prop;
	bool        cascade;

    public:
	AppPropChangeOp (const std::string &label,
			 property_t        &prop_,
			 const value_t     &new_val,
			 bool               cascade_) :
	    PropChangeOp<T> (label, prop_, new_val),
	    prop (prop_),
	    cascade (cascade_)
	    {}

	property_t & get_prop () const {
	    return prop;
	}

	UndoOp * combine (UndoOp *other_op) const {
	    if (!cascade)
		return 0;
	    
	    AppPropChangeOp<value_t> *op =
		dynamic_cast<AppPropChangeOp<value_t>*> (other_op);
	    if (!op)
		return 0;

	    if (&(op->prop) != &prop)
		return 0;

	    AppPropChangeOp<value_t> *new_op =
		new AppPropChangeOp<value_t> (this->label, prop, op->new_val, true);
	    new_op->old_val = this->old_val;

	    return new_op;
	}
    };

    template<typename T>
    class AppPropChangeOpFactory: public PropChangeOpFactory<T>
    {
	typedef T           value_t;
	typedef Property<T> property_t;
	
	std::string  label;
	property_t  &prop;
	bool         cascade;
	
	UndoManager &undo_manager;
	
    public:
	AppPropChangeOpFactory (const std::string      &label_,
				Resources::Application *resource,
				property_t             &prop_,
				bool                    cascade_) :
	    label (label_),
	    prop (prop_),
	    cascade (cascade_),
	    undo_manager (resource->get_manager ()->get_undo_manager ())
	    {}
	
	void push_change (const value_t &value) {
	    UndoOp *op = new AppPropChangeOp<T> (label, prop, value, cascade);
	    std::string old_value = prop;

	    prop = value;
	    if (prop != old_value)
		undo_manager.push (op);
	    else
		delete op;
	}
    };


    
    template<typename T>
    class TargetPropChangeOp: public PropChangeOp<T>
    {
	typedef T           value_t;
	typedef Property<T> property_t;

	property_t &prop;
	bool        cascade;

    public:
	TargetPropChangeOp (const std::string &label,
			    property_t        &prop_,
			    const value_t     &new_val,
			    bool               cascade_) :
	    PropChangeOp<T> (label, prop_, new_val),
	    prop (prop_),
	    cascade (cascade_)
	    {}
	
	property_t & get_prop () const {
	    return prop;
	}
    };
    
    template<typename T>
    class TargetPropChangeOpFactory: public PropChangeOpFactory<T>
    {
	typedef T           value_t;
	typedef Property<T> property_t;
	
	std::string  label;
	property_t  &prop;
	bool         cascade;
	
	UndoManager &undo_manager;
	
    public:
	TargetPropChangeOpFactory (const std::string &label_,
				   ResourceManager   *resource_manager,
				   property_t        &prop_,
				   bool               cascade_) :
	    label (label_),
	    prop (prop_),
	    cascade (cascade_),
	    undo_manager (resource_manager->get_undo_manager ())
	    {}
	
	void push_change (const value_t &value) {
	    UndoOp *op = new TargetPropChangeOp<T> (label, prop, value, cascade);
	    value_t old_value = prop;
	    
	    prop = value;
	    if (prop != old_value)
		undo_manager.push (op);
	    else
		delete op;
	}
    };
    
} // Anonymous namespace

GUI::AppWindow::AppWindow (Resources::Application *res_):
    res (res_),
    window (new Gtk::Window)
{
    using Gtk::manage;
    
    window->property_allow_grow () = false;
    window->property_allow_shrink () = false;
    window->signal_delete_event ().connect (sigc::mem_fun (*this, &AppWindow::delete_event_impl));

    Target *target = res->get_manager ()->get_target ();

    proptable = new GUI::PropertyTable ();
    Gtk::Widget *control;
    
    /* Icon caption */
    control = new GUI::PropertyEditors::Entry (
	true, res->iconname,
	new AppPropChangeOpFactory<std::string> (
	    _("Change icon caption"), res, res->iconname, true));
    proptable->add (_("Icon _caption:"), *manage (control));
    
    /* Version */
    control = new GUI::PropertyEditors::Entry (
	true, res->version,
	new AppPropChangeOpFactory<std::string> (
	    _("Change project version"), res, res->version, true));
    proptable->add (_("_Version:"), *manage (control));

    /* Vendor ID */
    control = new GUI::PropertyEditors::Entry (
	false, res->vendor,
	new AppPropChangeOpFactory<std::string> (
	    _("Change vendor ID"), res, res->vendor, false));
    static_cast<Gtk::Entry*>(control)->set_max_length (4);
    proptable->add (_("Vendor _ID:"), *manage (control));

    /* Target machine */
    GUI::PropertyEditors::TargetCombo *target_combo =
	new GUI::PropertyEditors::TargetCombo (res->get_manager ());
    target_combo->stock_selected.connect (sigc::mem_fun (*this, &AppWindow::stock_target_cb));
    target_combo->custom_selected.connect (sigc::mem_fun (*this, &AppWindow::custom_target_cb));
	
    control = target_combo;
    proptable->add (_("_Target machine:"), *manage (control));

    /* Custom target machine */
    control = custom_target_width = new PropertyEditors::NumEntry (
	0, 2 << 7, target->screen_width,
	new TargetPropChangeOpFactory<int> (_("Change custom screen width"),
					    res->get_manager (), target->screen_width, true));
    proptable->add (_("Screen _width:"), *manage (control));

    control = custom_target_height = new PropertyEditors::NumEntry (
	0, 2 << 7, target->screen_height,
	new TargetPropChangeOpFactory<int> (_("Change custom screen height"),
					    res->get_manager (), target->screen_height, true));
    proptable->add (_("Screen _height:"), *manage (control));

    control = custom_target_color = new PropertyEditors::ToggleButton (
	target->screen_color,
	new TargetPropChangeOpFactory<bool> (_("Change custom screen type"),
					    res->get_manager (), target->screen_color, true));
    proptable->add (_("Color:"), *manage (control));

    proptable->show_all ();
    window->add (*manage (proptable));

    if (target->get_stock_id () != "")
	stock_target_cb ();
    
    res->changed.connect (sigc::mem_fun (*this, &AppWindow::update));
    update ();
}

GUI::AppWindow::~AppWindow ()
{
    delete window;
}

bool GUI::AppWindow::delete_event_impl (GdkEventAny *e)
{
    window->hide ();
    return true;
}

void GUI::AppWindow::show ()
{
    window->show ();
    window->raise ();
}

void GUI::AppWindow::hide ()
{
    window->hide ();
}

void GUI::AppWindow::update ()
{
    Glib::ScopedPtr<char> title_buf (
	g_strdup_printf (_("Application: %s"), res->iconname ().c_str ()));
    window->set_title (title_buf.get ());
}

void GUI::AppWindow::stock_target_cb ()
{
    proptable->set_child_shown (*custom_target_width,  false);
    proptable->set_child_shown (*custom_target_height, false);
    proptable->set_child_shown (*custom_target_color,  false);
}

void GUI::AppWindow::custom_target_cb ()
{
    proptable->set_child_shown (*custom_target_width,  true);
    proptable->set_child_shown (*custom_target_height, true);
    proptable->set_child_shown (*custom_target_color,  true);
}
