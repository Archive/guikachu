//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "edit-cut-and-paste.h"
#include "edit-priv.h"

#include "resource-util.h"
#include "form-editor/widget-util.h"
#include "io/xml-loader.h"
#include "io/xml-saver.h"
#include "storage.h"
#include "edit-ops.h"

#include <libxml/parser.h>
#include <libxml/xmlmemory.h>

#include <gtkmm/invisible.h>

using namespace Guikachu;

// Some awful hacks in the implementation -- but it's the price to pay
// for an interface that is very nice and easy to use and hides the
// asynchronity of the X selection mechanism

namespace
{
    
    const char *TARGET_RES    = "application/x-guikachu/resources";
    const char *TARGET_WIDGET = "application/x-guikachu/widgets";
    
    struct clipboard_contents_
    {
	std::string     target;
	unsigned char  *buffer;
	IO::filesize_t  length;
	
	void reset () {
	    xmlFree (buffer);
	    target = "";
	    buffer = 0;
	    length = 0;
	}

        void set_data (Gtk::SelectionData &data) {
            if (data.get_target () != target)
                return;
            
            data.set (target, 8, (guchar*) buffer, length);
        }
        
    } clipboard_contents = { "", 0, 0 };
    
    union clipboard_state_
    {
	ResourceManager *manager;
	Resources::Form *form;
    } clipboard_state;
    
    void selection_received_cb (const Gtk::SelectionData &data);
    void selection_requested_cb (Gtk::SelectionData &data, guint info);
    void selection_cleared_cb ();
    
    void paste_resources_impl (const Gtk::SelectionData &data);
    void paste_widgets_impl   (const Gtk::SelectionData &data);
    
} // anonymous namespace



void Edit::copy_resource (Resource *resource)
{
    // Create a new XML document in memory
    Storage storage;

    // The root node contains the type of the resource and its ID
    StorageNode root_node = storage.create_root ("guikachu-resources");

    std::string type_id = Resources::type_id_from_type (resource->get_type ());
    StorageNode resource_node = root_node.add_node (type_id);
    resource_node.set_prop ("id", resource->id);

    // Save resource data to document
    IO::XML::ResourceSaver saver (resource_node);
    resource->apply_visitor (saver);

    // Serialize document into a string
    clipboard_contents.reset ();
    storage.save_buffer (clipboard_contents.buffer, clipboard_contents.length);
    
    // Set the selection
    std::list<Gtk::TargetEntry> targets;
    targets.push_back (Gtk::TargetEntry (TARGET_RES));
    clipboard_contents.target = TARGET_RES;
    
    Glib::RefPtr<Gtk::Clipboard> clipboard = Gtk::Clipboard::get ();
    clipboard->set (targets,
                    sigc::ptr_fun (selection_requested_cb),
                    sigc::ptr_fun (selection_cleared_cb));
}

void Edit::duplicate_resource (Resource *resource)
{
    Resource *new_resource = Edit::Priv::duplicate_resource_impl (resource);
    
    resource->get_manager ()->get_undo_manager ().push (
        new Edit::DuplicateResourceOp (resource, new_resource));
}

void Edit::copy_widget (Widget *widget)
{
    Storage storage;

    StorageNode root_node = storage.create_root ("guikachu-widgets");

    std::string type_id = Widgets::type_id_from_type (widget->get_type ());
    StorageNode widget_node = root_node.add_node (type_id);
    widget_node.set_prop ("id", widget->id);

    IO::XML::WidgetSaver saver (widget_node);
    widget->apply_visitor (saver);

    clipboard_contents.reset ();
    storage.save_buffer (clipboard_contents.buffer, clipboard_contents.length);    

    // Set the selection
    std::list<Gtk::TargetEntry> targets;
    targets.push_back (Gtk::TargetEntry (TARGET_WIDGET));
    clipboard_contents.target = TARGET_WIDGET;
    
    Glib::RefPtr<Gtk::Clipboard> clipboard = Gtk::Clipboard::get ();
    clipboard->set (targets,
                    sigc::ptr_fun (selection_requested_cb),
                    sigc::ptr_fun (selection_cleared_cb));
}

void Edit::copy_widgets (const std::set<Widget*> &widgets)
{
    Storage storage;

    StorageNode root_node = storage.create_root ("guikachu-widgets");

    for (std::set<Widget*>::const_iterator i = widgets.begin ();
	 i != widgets.end (); ++i)
    {
	Widget *widget = *i;
	
	std::string type_id = Widgets::type_id_from_type (widget->get_type ());
	StorageNode widget_node = root_node.add_node (type_id);
	widget_node.set_prop ("id", widget->id);

	IO::XML::WidgetSaver saver (widget_node);
	widget->apply_visitor (saver);
    }
    
    clipboard_contents.reset ();
    storage.save_buffer (clipboard_contents.buffer, clipboard_contents.length);
    
    // Set the selection
    std::list<Gtk::TargetEntry> targets;
    targets.push_back (Gtk::TargetEntry (TARGET_WIDGET));
    clipboard_contents.target = TARGET_WIDGET;
    
    Glib::RefPtr<Gtk::Clipboard> clipboard = Gtk::Clipboard::get ();
    clipboard->set (targets,
                    sigc::ptr_fun (selection_requested_cb), 
                    sigc::ptr_fun (selection_cleared_cb));
}

void Edit::duplicate_widget (Widget *widget)
{
    Widget *new_widget = Edit::Priv::duplicate_widget_impl (widget);
    
    widget->get_manager ()->get_undo_manager ().push (
        new Edit::DuplicateWidgetOp (widget, new_widget));
}

void Edit::duplicate_widgets (const std::set<Widget*> &widgets)
{
    if (widgets.size () == 1)
    {
        duplicate_widget (*widgets.begin ());
        return;
    }
    
    std::set<std::pair<Widget*, Widget*> > created_widgets = Edit::Priv::duplicate_widgets_impl (widgets);

    (*widgets.begin ())->get_manager ()->get_undo_manager ().push (
        new Edit::DuplicateWidgetsOp (created_widgets));
}

    
void Edit::paste_resources (ResourceManager *manager)
{
    clipboard_state.manager = manager;
    
    // Request selection
    Glib::RefPtr<Gtk::Clipboard> clipboard = Gtk::Clipboard::get ();
    clipboard->request_contents (TARGET_RES, sigc::ptr_fun (selection_received_cb));
}

void Edit::paste_widgets (Resources::Form *form)
{
    clipboard_state.form = form;

    // Request selection
    Glib::RefPtr<Gtk::Clipboard> clipboard = Gtk::Clipboard::get ();
    clipboard->request_contents (TARGET_WIDGET, sigc::ptr_fun (selection_received_cb));
}


namespace
{
    
void selection_requested_cb (Gtk::SelectionData &data, guint info)
{
    clipboard_contents.set_data (data);
}

void selection_received_cb (const Gtk::SelectionData &data)
{
    if (!data.get_length ())
	return;
    
    if (data.get_target () == TARGET_RES)
	paste_resources_impl (data);
    else if (data.get_target () == TARGET_WIDGET)
	paste_widgets_impl (data);
}

void selection_cleared_cb ()
{
    // Do nothing
}

void paste_resources_impl (const Gtk::SelectionData &data)
{
    Storage storage;
    
    try {
        storage.load_buffer ((unsigned char*)data.get_data (), data.get_length ());
    } catch (...) {
	g_warning ("X clipboard contains unparsable junk");
        return;
    }
    
    StorageNode root_node = storage.get_root ();
    if (root_node.name () != "guikachu-resources")
    {
	g_warning ("X clipboard contains incorrect content");
	return;
    }

    std::set<Resource*> created_resources;
	
    for (StorageNode resource_node = root_node.children (); resource_node; ++resource_node)
    {
	// Create resource
	std::string type_id = resource_node.name ();
	Resources::Type type = Resources::type_from_type_id (type_id);
	std::string id = resource_node.get_prop_string ("id");
	
	Resource *resource = clipboard_state.manager->create_resource (type, id, true);
	g_assert (resource);
	
	// Load resource data from parsed selection
	IO::XML::ResourceLoader loader (resource_node);
	resource->apply_visitor (loader);

	created_resources.insert (resource);
    }

    if (!created_resources.size ())
	return;

    if (created_resources.size () == 1)
    {
	UndoOp *op = new Edit::PasteResourceOp (*(created_resources.begin ()));
	clipboard_state.manager->get_undo_manager ().push (op);
    } else {
	UndoOp *op = new Edit::PasteResourcesOp (created_resources);
	clipboard_state.manager->get_undo_manager ().push (op);
    }
}

void paste_widgets_impl (const Gtk::SelectionData &data)
{
    Storage storage;
    
    try {
        storage.load_buffer ((unsigned char*)data.get_data (), data.get_length ());
    } catch (...) {
	g_warning ("X clipboard contains unparsable junk");
        return;
    }

    StorageNode root_node = storage.get_root ();
    if (root_node.name () != "guikachu-widgets")
    {
	g_warning ("X clipboard contains incorrect content");
	return;
    }

    std::set<Widget*> created_widgets;
	
    for (StorageNode widget_node = root_node.children (); widget_node; ++widget_node)
    {
	std::string type_id = widget_node.name ();
	Widgets::Type type = Widgets::type_from_type_id (type_id);
	std::string id = widget_node.get_prop_string ("id");
	
	Widget *widget = clipboard_state.form->create_widget (type, id, true);
	g_assert (widget);
	
	// Load resource data from parsed selection
	IO::XML::WidgetLoader loader (widget_node);
	widget->apply_visitor (loader);

	created_widgets.insert (widget);
    }

    if (!created_widgets.size ())
	return;

    if (created_widgets.size () == 1)
    {
	UndoOp *op = new Edit::PasteWidgetOp (*(created_widgets.begin ()));
	clipboard_state.form->get_manager ()->get_undo_manager ().push (op);
    } else {
	UndoOp *op = new Edit::PasteWidgetsOp (created_widgets);
	clipboard_state.form->get_manager ()->get_undo_manager ().push (op);
    }
}

} // Anonymous namespace
