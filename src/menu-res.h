//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_MENU_RES_H
#define GUIKACHU_MENU_RES_H

#include "resource.h"

#include <vector>

namespace Guikachu
{
    namespace Resources
    {
	class Menu: public Resource, public IDManager
	{
	public:
	    struct MenuItem
	    {
		bool        separator;
		std::string id;
		std::string label;
		char        shortcut;

                MenuItem ();
                MenuItem (const std::string &id, const std::string &label, char shortcut = 0);
	    };

	    typedef std::vector<MenuItem> MenuItems;
	    
	    struct Submenu
	    {
		std::string label;
		MenuItems   items;
		
		Submenu () {};
		explicit Submenu (const std::string& label);
	    };
	    
	    typedef std::vector<Submenu> MenuTree;
	    
	private:
	    MenuTree submenus;
	public:
	    Menu (ResourceManager *manager, const std::string &id, serial_t serial);
	    ~Menu ();
	    
	    Type get_type () const { return RESOURCE_MENU; };
	    void apply_visitor (ResourceVisitor &visitor) { visitor.visit_resource (this); };

	    MenuTree get_submenus () const { return submenus; };
	    void     set_submenus (const MenuTree &submenus);
	    
	private:
	    void register_menu   (const MenuTree &new_submenus);
	    void deregister_menu ();
	};
    }
}

#endif /* !GUIKACHU_MENU_RES_H */
