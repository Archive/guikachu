//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_TARGET_H
#define GUIKACHU_TARGET_H

#include <string>
#include <glibmm/ustring.h>
#include <map>
#include <sigc++/trackable.h>

#include "property.h"

namespace Guikachu
{
    class Target: public sigc::trackable
    {
    public:
	typedef std::string   stock_id_t;
	typedef Glib::ustring stock_desc_t;
	typedef std::map<stock_id_t, stock_desc_t> stock_list_t;

    private:
	stock_id_t stock_id;
        VSignal changed_internal;

    public:
	Target ();
	
	void       load_stock   (const stock_id_t &stock_id);
	stock_id_t get_stock_id () const;

	static stock_list_t stock_targets ();
	static stock_id_t get_default_stock_id ();
	
	VSignal changed;
	
	// Screen dimensions in pixels (used by the form editor
	Property<int>  screen_width;
	Property<int>  screen_height;
        Property<bool> screen_color;

    private:
	void changed_cb ();
    };
}    

#endif /* !GUIKACHU_TARGET_H */
