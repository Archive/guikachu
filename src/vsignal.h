//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 *
 * This portion of the code is written by Martin Schulze <MHL.Schulze@t-online.de>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_VSIGNAL_H
#define GUIKACHU_VSIGNAL_H

#include <sigc++/signal.h>

namespace Guikachu
{
    /* This is a sigc::signal0<void> wrapper that has only one
       difference from the real McCoy: its emit() member function is
       virtual and thus can be overwritten.
       This is done by QueuedSignal to queue up emission requests and
       schedule real signal emissions to when GTK+ is idle
    */
       
    
    class VSignal
    {
    public:
	typedef sigc::signal0<void>      real_signal_t;
	typedef real_signal_t::slot_type slot_t;
        typedef real_signal_t::iterator  connection_t;

        typedef sigc::bound_const_mem_functor0<void, real_signal_t> signal_slot_t;

    private:
	real_signal_t real_signal;
	
    public:
	VSignal ()          {};
	virtual ~VSignal () {};

	connection_t connect (const slot_t &s);

        virtual signal_slot_t make_slot() const;
	virtual void emit ();
	void operator() () { emit (); };
    };
}

#endif /* !GUIKACHU_QUEUED_SIGNAL_H */
