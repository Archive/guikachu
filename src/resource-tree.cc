//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <glib/gi18n.h>

#include "resource-tree.h"

#include "cellrenderer-icontext.h"
#include "cellrenderer-indent.h"

#include <gdk/gdkkeysyms.h>

#include <algorithm>
#include <functional>

using namespace Guikachu::GUI;
using namespace Guikachu;

namespace
{
    class AppResetOp: public UndoOp
    {
        Resources::Application *res;
        std::string iconname, version, vendor;
        
    public:
        AppResetOp (Resources::Application *res_) :
            res (res_),
            iconname (res->iconname),
            version (res->version),
            vendor (res->vendor)
            {};
        
        Glib::ustring get_label () const { return _("Reset application settings"); };

        void undo () {
            res->iconname = iconname;
            res->version = version;
            res->vendor = vendor;
        };
        
        void redo () { res->reset (); };
    };
} // Anonymous namespace

ResourceTree::ResourceTree ():
    manager (0),
    grouped (false)
{
    Gtk::TreeModel::ColumnRecord cols;
    cols.add (col_data);
    
    treestore = Gtk::TreeStore::create (cols);
    set_model (treestore);

    set_headers_visible (false);
    
    CellRendererIndent<CellRendererIconText>::setup_view (
        *this, "", sigc::mem_fun (*this, &ResourceTree::cell_label_cb));

    // Connect to events
    signal_button_press_event ().connect_notify (sigc::mem_fun (*this, &ResourceTree::button_press_cb));
    signal_key_press_event ().connect (sigc::bind_return (sigc::mem_fun (*this, &ResourceTree::key_press_cb), true));

    add_events (Gdk::BUTTON_PRESS_MASK | Gdk::KEY_PRESS_MASK);

    signal_row_activated ().connect (sigc::mem_fun (*this, &ResourceTree::row_activated_cb));
}

void ResourceTree::cell_label_cb (Gtk::CellRenderer *cell, const Gtk::TreeModel::iterator &iter) const
{
    const TreeData &td = (*iter)[col_data];
    td.apply_to_cell (cell);
}

Guikachu::Resource * ResourceTree::get_selected ()
{
    Glib::RefPtr<Gtk::TreeSelection> tree_selection = get_selection ();
    Gtk::TreeModel::iterator iter = tree_selection->get_selected ();

    if (!iter)
        return 0;

    const TreeData &td = (*iter)[col_data];
    return td.get_resource ();
}

bool ResourceTree::get_app_selected ()
{
    Glib::RefPtr<Gtk::TreeSelection> tree_selection = get_selection ();
    Gtk::TreeModel::iterator iter = tree_selection->get_selected ();

    if (!iter)
        return false;
    
    return iter == treestore->children ().begin ();
}

void ResourceTree::set_manager (ResourceManager *manager_)
{
    manager = manager_;

    treestore->clear ();
    subtrees.clear ();

    root_tree = *(treestore->append ());
    root_tree[col_data] = manager->get_application ();

    manager->get_application ()->changed.connect (
	sigc::mem_fun (*this, &ResourceTree::app_changed_cb));

    manager->resource_created.connect (
	sigc::mem_fun (*this, &ResourceTree::resource_created_cb));
    manager->resource_removed.connect (
	sigc::mem_fun (*this, &ResourceTree::resource_removed_cb));

    reset ();
}

void ResourceTree::reset ()
{
    if (!manager)
        return;
    
    while (!root_tree.children ().empty ())
        treestore->erase (root_tree.children ().begin ());
    
    // Create grouping
    subtrees.clear ();
    if (grouped)
    {
        create_tree_for_type (Resources::RESOURCE_FORM);
        create_tree_for_type (Resources::RESOURCE_MENU);
        create_tree_for_type (Resources::RESOURCE_BITMAP);
        create_tree_for_type (Resources::RESOURCE_STRING);
        create_tree_for_type (Resources::RESOURCE_BLOB);
    }
    
    // Create tree items for existing resources
    const std::set<Resource*> resources = manager->get_resources ();
    std::for_each (resources.begin (), resources.end (),
		   sigc::mem_fun (*this, &ResourceTree::resource_created_cb));

    expand_all ();    
}

void ResourceTree::set_grouped (bool grouped_)
{
    grouped = grouped_;
    reset ();
}

void ResourceTree::create_tree_for_type (Resources::Type type)
{
    if (subtrees.find (type) != subtrees.end ())
        return;

    Gtk::TreeRow row = *(treestore->append (root_tree.children ()));
    row[col_data] = type;

    subtrees[type] = row;

    switch (type)
    {
    case Resources::RESOURCE_FORM:
    case Resources::RESOURCE_DIALOG:
        subtrees[Resources::RESOURCE_FORM] = subtrees[Resources::RESOURCE_DIALOG] = row;
        break;

    case Resources::RESOURCE_STRING:
    case Resources::RESOURCE_STRINGLIST:
        subtrees[Resources::RESOURCE_STRING] = subtrees[Resources::RESOURCE_STRINGLIST] = row;
        break;

    case Resources::RESOURCE_BITMAP:
    case Resources::RESOURCE_BITMAPFAMILY:
        subtrees[Resources::RESOURCE_BITMAP] = subtrees[Resources::RESOURCE_BITMAPFAMILY] = row;
        break;

    default:
        break;
    }
}

void ResourceTree::app_changed_cb ()
{
    treestore->row_changed (treestore->get_path (root_tree), root_tree);
}

void ResourceTree::resource_removed_cb (Resource *res)
{
    row_map_t::iterator row_found = row_map.find (res);
    g_return_if_fail (row_found != row_map.end ());

    treestore->erase (row_found->second);
    row_map.erase (row_found);

    expand_all ();
}

bool ResourceTree::compare_treerow (const Gtk::TreeRow &row, Resource *res) const
{
    const TreeData &td = row[col_data];
    Resource *curr_res = td.get_resource ();
    if (!curr_res)
        return true;
    
    return IDManager::NoCase () (curr_res->id, res->id);
}

Gtk::TreeStore::iterator ResourceTree::get_place (Resource *res)
{
    Gtk::TreeRow parent_row = root_tree;
    
    if (grouped)
    {
        create_tree_for_type (res->get_type ());
        parent_row = subtrees[res->get_type ()];
    }

    return std::lower_bound (parent_row.children ().begin (), parent_row.children ().end (),
                             res, sigc::mem_fun (*this, &ResourceTree::compare_treerow));    
}

void ResourceTree::resource_created_cb (Resource *res)
{
    Gtk::TreeRow row = *(treestore->insert (get_place (res)));
    row[col_data] = res;

    if (row_map.find (res) == row_map.end ())
    {    
        res->changed.connect (sigc::bind (sigc::mem_fun (*this, &ResourceTree::resource_changed_cb), res));
    }    
    row_map[res] = row;

    expand_all ();
}

void ResourceTree::resource_changed_cb (Resource *res)
{
    Gtk::TreeRow row = row_map[res];
    bool selected = get_selection ()->is_selected (row);
    
    treestore->erase (row);
    row = *(treestore->insert (get_place (res)));
    row[col_data] = res;
    row_map[res] = row;

    expand_all ();

    if (selected)
        get_selection ()->select (row);
}

void ResourceTree::row_activated_cb (const Gtk::TreeModel::Path &path,
				     Gtk::TreeView::Column      *col)
{
    Gtk::TreeModel::iterator iter = treestore->get_iter (path);
    if (iter == treestore->children ().begin ())
    {
        app_activated.emit ();
	return;
    }

    const TreeData &td = (*iter)[col_data];
    Resource *res = td.get_resource ();
    if (res)
        resource_activated.emit (res);
}

void ResourceTree::button_press_cb (GdkEventButton *e)
{
    if (e->button != 3)
	return;

    menu.emit (e->button, e->time);
}

void ResourceTree::key_press_cb (GdkEventKey *e)
{
    switch (e->keyval)
    {
    case GDK_Menu:
        menu.emit (0, e->time);
        break;
        
    default:
        break;
    }
}

#if 0
void ResourceTree::app_reset_cb ()
{
    Resources::Application *app = manager->get_application ();
    manager->get_undo_manager ().push (new AppResetOp (app));
    app->reset ();
    if (app_win)
        app_win->hide ();

}
#endif
