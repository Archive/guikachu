//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_BITMAPFAMILY_RES_H
#define GUIKACHU_BITMAPFAMILY_RES_H

#include "resource.h"
#include "bitmap-res.h"

#include <map>

namespace Guikachu
{
    namespace Resources
    {
	class BitmapFamily: public Resource
	{
	    typedef Resources::Bitmap::BitmapType BitmapType;
	    typedef Resources::Bitmap::ImageData  ImageData;
	    typedef std::map<BitmapType, ImageData> image_map_t;
	    
	public:
	    BitmapFamily (ResourceManager *manager, const std::string &id, serial_t serial);

	    Type get_type () const { return RESOURCE_BITMAPFAMILY; };
	    void apply_visitor (ResourceVisitor &visitor) { visitor.visit_resource (this); };
	    ImageData get_image (BitmapType type) const;
	    
	    void load_file     (BitmapType type, const Glib::ustring &uri);
	    void load_data     (BitmapType type, const unsigned char *data, size_t len,
                                const Glib::ustring &src_hint = "");

	    void save_file_bmp (BitmapType type, const Glib::ustring &uri) const;
	    void save_data_png (BitmapType type, unsigned char *&data, size_t &len) const;
	    void save_file_png (BitmapType type, const Glib::ustring &uri) const;
            
            void set_image     (BitmapType type, const ImageData &image);
	    void clear_image   (BitmapType type);
	    
	private:
	    image_map_t images;
	};
    }
}

#endif /* !GUIKACHU_BITMAPFAMILY_RES_H */
