//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 *
 * This portion of the code is written by Martin Schulze <MHL.Schulze@t-online.de>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_QUEUED_SIGNAL_H
#define GUIKACHU_QUEUED_SIGNAL_H

#include "vsignal.h"
#include <sigc++/trackable.h>

namespace Guikachu
{
    class QueuedSignal: public VSignal,
			public sigc::trackable
    {
	bool queued;
	typedef void (*signal_queuer_t) (slot_t);
	static signal_queuer_t signal_queuer;
	
    public:
	QueuedSignal ();
	void emit ();

	static void set_signal_queuer (signal_queuer_t new_signal_queuer);
	
    private:
	void idle_cb ();
    };
}

#endif /* !GUIKACHU_QUEUED_SIGNAL_H */
