//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_WIN_HELPERS_H
#define GUIKACHU_FORM_WIN_HELPERS_H

#include "form-win.h"

#include <foocanvasmm/canvas.h>
#include <foocanvasmm/rect.h>

#include "form-editor/widget-ops.h"

namespace Guikachu
{
    namespace GUI
    {
	namespace FormWindow_Helpers
	{
	    class CanvasSelectionWrapper: public sigc::trackable
	    {
		FooCanvasmm::Canvas &canvas;
		FooCanvasmm::Rect   *rubber_box;
		
	    public:
		CanvasSelectionWrapper (FooCanvasmm::Canvas &canvas);
		~CanvasSelectionWrapper ();
		
		sigc::signal4<void, int, int, int, int> selection;

		void set_active (bool active);
		
	    private:
		bool   dragging;
		bool   active;
		double selection_start_x, selection_start_y;
		
		void selection_begin (GdkEventButton *e);
		void selection_drag  (GdkEventMotion *e);
		void selection_end   (GdkEventButton *e);
	    };
    
	    
	    class FormMoveOp: public UndoOp
	    {
		Glib::ustring label;

		ResourceManager *manager;
		std::string form_id;

		int old_x, old_y;
		int new_x, new_y;

	    public:
		FormMoveOp (Resources::Form *form, int new_x, int new_y);
		virtual ~FormMoveOp () {};

		void undo ();
		void redo ();
		Glib::ustring get_label () const { return label; };
	    };
	}
    }
}

#endif /* !GUIKACHU_FORM_WIN_HELPERS_H */
