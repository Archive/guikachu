//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "preferences.h"

#include <stdarg.h>

#include <windef.h>
#include <winbase.h>
#include <winnt.h>
#include <winreg.h>

void Guikachu::Preferences::init ()
{
}

#define REGISTRY_ROOT "Software\\Guikachu\\"

namespace
{
    std::string create_winreg_key (const std::string &dir)
    {
        std::string winreg_key = dir;
        
        for (std::string::size_type i = winreg_key.find ('/'); i != std::string::npos; i = winreg_key.find ('/', i))
            winreg_key[i] = '\\';

        return REGISTRY_ROOT + winreg_key;
    }

    int get_conf_int (const std::string &path, const std::string &key, int default_value)
    {
        HKEY regkey;
        
        DWORD value;     
        DWORD len = sizeof value;
        DWORD type;
        
        if (RegOpenKeyEx (HKEY_CURRENT_USER, create_winreg_key (path).c_str (), 0, KEY_QUERY_VALUE, &regkey)
            != ERROR_SUCCESS)
            return default_value;
    
        int ret = default_value;
        
        if (RegQueryValueEx (regkey, key.c_str (), 0, &type, (BYTE*)&value, &len) == ERROR_SUCCESS &&
            type == REG_DWORD)
            ret = value;
        
        RegCloseKey (regkey);
    
        return ret;
    }

    bool get_conf_bool (const std::string &path, const std::string &key, bool default_value)
    {
        return get_conf_int (path, key, default_value);
    }
    
    void set_conf_int (const std::string &path, const std::string &key, int value)
    {
        HKEY regkey;

        RegCreateKeyEx (HKEY_CURRENT_USER, create_winreg_key (path).c_str (), 0, 0, 0, KEY_SET_VALUE, 0, &regkey, 0);
        RegSetValueExA (regkey, key.c_str (), 0, REG_DWORD, (BYTE*)value, sizeof (DWORD));
        RegCloseKey (regkey);
    }

    float get_conf_float (const std::string &path, const std::string &key, float default_value)
    {
        return get_conf_int (path, key, default_value * 8) / 8.0;
    }

    void set_conf_float (const std::string &path, const std::string &key, float value)
    {
        set_conf_int (path, key, value * 4);
    }

    std::string get_conf_string (const std::string &path, const std::string &key, const std::string &default_value)
    {   
        HKEY regkey;

        if (RegOpenKeyEx (HKEY_CURRENT_USER, create_winreg_key (path).c_str (), 0, KEY_QUERY_VALUE, &regkey)
            != ERROR_SUCCESS)
            return default_value;
    
        DWORD  len = 32;
        BYTE  *buffer = new BYTE[len];
        DWORD  type;

        DWORD result = RegQueryValueExA (regkey, key.c_str (), 0, &type, buffer, &len);
        if (result == ERROR_MORE_DATA)
        {
            delete buffer;
            buffer = new BYTE[len];
            result = RegQueryValueExA (regkey, key.c_str (), 0, &type, buffer, &len);    
        }

        std::string value = default_value;
        
        if (result == ERROR_SUCCESS && type == REG_SZ)
            value = (char*)buffer;

        delete buffer;
        RegCloseKey (regkey);
        
        return value;
    }
    
    void set_conf_string (const std::string &path, const std::string &key, const std::string &value)
    {   
        HKEY regkey;

        RegCreateKeyEx (HKEY_CURRENT_USER, create_winreg_key (path).c_str (), 0, 0, 0, KEY_SET_VALUE, 0, &regkey, 0);
        RegSetValueExA (regkey, key.c_str (), 0, REG_SZ, (const BYTE*)value.c_str (), value.length ());
        RegCloseKey (regkey);
    }

    FileList get_conf_filelist (const std::string &path, const std::string &key)
    {
        HKEY regkey;
        
        if (RegOpenKeyEx (HKEY_CURRENT_USER, create_winreg_key (path).c_str (), 0, KEY_QUERY_VALUE, &regkey)
            != ERROR_SUCCESS)
            return FileList ();
        
        DWORD  len = 1024;
        BYTE  *buffer = new BYTE[len];
        DWORD  type;

        DWORD result = RegQueryValueExA (regkey, key.c_str (), 0, &type, buffer, &len);
        if (result == ERROR_MORE_DATA)
        {
            delete buffer;
            buffer = new BYTE[len];
            result = RegQueryValueExA (regkey, key.c_str (), 0, &type, buffer, &len);
        }

        FileList ret;
        
        if (result == ERROR_SUCCESS && type == REG_MULTI_SZ)
        {     
            for (BYTE *buf_ptr = buffer; buf_ptr != buffer + len; buf_ptr += strlen ((char*)buf_ptr) + 1)
                ret.push_back ((char*)buf_ptr);
        }

        delete buffer;
        RegCloseKey (regkey);

        return ret;
    }
        
    void set_conf_filelist (const std::string &path, const std::string &key, const FileList &value)
    {
        size_t len = 0;
        for (FileList::const_iterator i = value.begin (); i != value.end (); ++i)
            len += i->length () + 1;
        
        char *buffer = new char[len + 2];
        char *buf_ptr = buffer;
        for (FileList::const_iterator i = value.begin (); i != value.end (); ++i)
        {
            strcpy (buf_ptr, i->c_str ());
            buf_ptr += i->length () + 1;
        }
        
        *buf_ptr = 0;
        *(buf_ptr + 1) = 0;
        
        HKEY regkey;
        RegCreateKeyEx (HKEY_CURRENT_USER, create_winreg_key (path).c_str (), 0, 0, 0, KEY_SET_VALUE, 0, &regkey, 0);
        RegSetValueExA (regkey, key.c_str (), 0, REG_MULTI_SZ, (BYTE*)buffer, len);
        RegCloseKey (regkey);
    }
    
} // anonymous namespace
