//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_STRINGLIST_RES_H
#define GUIKACHU_STRINGLIST_RES_H

#include "resource.h"
#include <vector>

namespace Guikachu
{
    namespace Resources
    {
	class StringList: public Resource
	{
	public:
	    StringList (ResourceManager *manager, const std::string &id, serial_t serial);

	    Type get_type () const { return RESOURCE_STRINGLIST; };
	    void apply_visitor (ResourceVisitor &visitor) { visitor.visit_resource (this); };

	    Property<std::string>                prefix;
	    Property<std::vector <std::string> > strings;
	};
    }
}

#endif /* !GUIKACHU_STRINGLIST_RES_H */
