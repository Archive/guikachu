//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "form-win-helpers.h"

#include <glib/gi18n.h>

#include <sigc++/bind_return.h>

#include <list>
#include <algorithm>


using namespace Guikachu::GUI::FormWindow_Helpers;


/***********************************
 * CanvasSelectionWrapper
 ***********************************/

CanvasSelectionWrapper::CanvasSelectionWrapper (FooCanvasmm::Canvas &canvas_):
    canvas (canvas_),
    rubber_box (0),
    dragging (false),
    active (true)
{
    canvas.signal_button_press_event ().connect_notify (
	sigc::mem_fun (*this, &CanvasSelectionWrapper::selection_begin),
        true); // true == connect_after
    canvas.signal_motion_notify_event ().connect_notify (
	sigc::mem_fun (*this, &CanvasSelectionWrapper::selection_drag),
        true); // true == connect_after
    canvas.signal_button_release_event ().connect_notify (
	sigc::mem_fun (*this, &CanvasSelectionWrapper::selection_end),
        true); // true == connect_after
}

CanvasSelectionWrapper::~CanvasSelectionWrapper ()
{
    delete rubber_box;
}

void CanvasSelectionWrapper::set_active (bool active_)
{
    active = active_;
}

void CanvasSelectionWrapper::selection_begin (GdkEventButton *e)
{
    if (e->button != 1 || dragging)
	return;

    if (!active)
	return;

    double x (0), y (0);
    canvas.window_to_world (e->x, e->y, x, y);
    
    static const char stipple_bits[] = { 0x02, 0x01 };
    static const Glib::RefPtr<Gdk::Bitmap> stipple = Gdk::Bitmap::create (stipple_bits, 2, 2);

    selection_start_x = x;
    selection_start_y = y;
    
    rubber_box = new FooCanvasmm::Rect (*(canvas.root ()), x, y, x, y);
    rubber_box->property_width_pixels () = 1;
    rubber_box->property_outline_color () = "black";
    rubber_box->property_outline_stipple () = stipple;
    
    dragging = true;
}

void CanvasSelectionWrapper::selection_drag (GdkEventMotion *e)
{
    if (!active)
	return;

    if (!dragging)
	return;

    g_return_if_fail (rubber_box != 0);
    
    double x (0), y (0);
    
    canvas.window_to_world (e->x, e->y, x, y);

    double x1 = std::min (x, selection_start_x);
    double x2 = std::max (x, selection_start_x);

    double y1 = std::min (y, selection_start_y);
    double y2 = std::max (y, selection_start_y);

    rubber_box->property_x1 () = x1;
    rubber_box->property_y1 () = y1;

    rubber_box->property_x2 () = x2;
    rubber_box->property_y2 () = y2;
}

void CanvasSelectionWrapper::selection_end (GdkEventButton *e)
{
    if (!active)
	return;
    
    if (e->button != 1 || !dragging)
	return;

    g_return_if_fail (rubber_box != 0);

    double x (0), y (0);
    
    canvas.window_to_world (e->x, e->y, x, y);

    selection.emit (int (selection_start_x), int (selection_start_y),
		    int (x), int (y));

    delete rubber_box;
    rubber_box = 0;

    dragging = false;
}

/***********************************
 * FormMoveOp
 ***********************************/

FormMoveOp::FormMoveOp (Resources::Form *form, int new_x_, int new_y_) :
    manager (form->get_manager ()),
    form_id (form->id),
    old_x (form->x), old_y (form->y),
    new_x (new_x_), new_y (new_y_)
{
    char *label_str = g_strdup_printf (_("Move %s"), form_id.c_str ());
    label = label_str;
    g_free (label_str);
}

void FormMoveOp::undo ()
{
    Resource *resource = manager->get_resource (form_id);
    Resources::Form *form = static_cast<Resources::Form*> (resource);
    g_assert (form);

    form->x = old_x;
    form->y = old_y;
}

void FormMoveOp::redo ()
{
    Resource *resource = manager->get_resource (form_id);
    Resources::Form *form = static_cast<Resources::Form*> (resource);
    g_assert (form);
    
    form->x = new_x;
    form->y = new_y;
}
