//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_EDIT_OPS_H
#define GUIKACHU_EDIT_OPS_H

#include "resource-manager.h"
#include "form-editor/widget.h"
#include "storage-node.h"
#include "undo.h"

namespace Guikachu
{
    namespace Edit
    {
	class PasteResourceOp: public UndoOp
	{
	    ResourceManager *manager;
	    serial_t         serial;

	    Resources::Type type;
	    std::string     id;
	    StorageNode     node;

	    Glib::ustring    op_label;
	    
	public:
	    explicit PasteResourceOp (Resource *pasted_resource);

	    Glib::ustring get_label () const { return op_label; };

	    void undo ();
	    void redo ();
	};

	class DuplicateResourceOp: public UndoOp
	{
            ResourceManager *manager;
            serial_t         original_serial, duplicated_serial;

	    Glib::ustring    op_label;
            
	public:
	    DuplicateResourceOp (Resource *original, Resource *duplicated);

	    Glib::ustring get_label () const { return op_label; };

	    void undo ();
	    void redo ();
	};

	class PasteResourcesOp: public UndoOp
	{
	    typedef std::set<PasteResourceOp*> paste_ops_t;
	    paste_ops_t paste_ops;

	public:
	    PasteResourcesOp (const std::set<Resource*> &pasted_resources);

	    Glib::ustring get_label () const;

	    void undo ();
	    void redo ();
	};


    	class PasteWidgetOp: public UndoOp
	{
	    ResourceManager *manager;
	    serial_t         form_serial, widget_serial;

	    Widgets::Type type;
	    std::string   id;
	    StorageNode   node;

	    Glib::ustring op_label;
	    
	public:
	    explicit PasteWidgetOp (Widget *pasted_resource);

	    Glib::ustring get_label () const { return op_label; };

	    void undo ();
	    void redo ();
	};

	class PasteWidgetsOp: public UndoOp
	{
	    typedef std::set<PasteWidgetOp*> paste_ops_t;
	    paste_ops_t   paste_ops;
	    Glib::ustring op_label;
	    
	public:
	    PasteWidgetsOp (const std::set<Widget*> &pasted_resources);

	    Glib::ustring get_label () const { return op_label; };

	    void undo ();
	    void redo ();
	};

	class DuplicateWidgetOp: public UndoOp
	{
            ResourceManager *manager;
            serial_t         form_serial, original_serial, duplicated_serial;

	    Glib::ustring    op_label;
            
	public:
	    DuplicateWidgetOp (Widget *original, Widget *duplicated);
            
	    Glib::ustring get_label () const { return op_label; };

	    void undo ();
	    void redo ();
	};

	class DuplicateWidgetsOp: public UndoOp
	{
	    typedef std::set<DuplicateWidgetOp*> duplicate_ops_t;
	    duplicate_ops_t duplicate_ops;            
	    Glib::ustring   op_label;
            
	public:
	    DuplicateWidgetsOp (const std::set<std::pair<Widget*, Widget*> > &duplicated_widgets);

	    Glib::ustring get_label () const { return op_label; };

            void undo ();
            void redo ();
	};

    }
}

#endif /* !GUIKACHU_EDIT_OPS_H */
