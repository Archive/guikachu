//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <glib/gi18n.h>

#include "stock.h"

#include <gtkmm/stock.h>
#include <gtkmm/iconfactory.h>

namespace {
#include "pixmaps/menu-align-left.xpm"
#include "pixmaps/menu-align-right.xpm"
#include "pixmaps/menu-align-top.xpm"
#include "pixmaps/menu-align-bottom.xpm"
#include "pixmaps/menu-center-horizontal.xpm"
#include "pixmaps/menu-center-vertical.xpm"
} // anonymous namespace

#define STOCKID(id) Gtk::StockID ("GUIKACHU_STOCK_"#id)

namespace Guikachu
{
    namespace GUI
    {
        namespace Stock
        {
#define CREATE_STOCK(id)                                \
            const Gtk::StockID id = STOCKID(id);
            
            CREATE_STOCK(CENTER_HORIZ);
            CREATE_STOCK(CENTER_VERT);
            CREATE_STOCK(ALIGN_LEFT);
            CREATE_STOCK(ALIGN_RIGHT);
            CREATE_STOCK(ALIGN_TOP);
            CREATE_STOCK(ALIGN_BOTTOM);
        }
    }
}

using namespace Guikachu;

void UI::register_stock ()
{
    static bool registered = false;
    if (registered)
        return;

    Glib::RefPtr<Gtk::IconFactory> icon_factory = Gtk::IconFactory::create ();
    icon_factory->add_default ();

#define ADD_STOCK(id, label, xpm)                                       \
    Gtk::Stock::add (Gtk::StockItem (STOCKID(id), label));              \
    icon_factory->add (STOCKID(id), Gtk::IconSet (Gdk::Pixbuf::create_from_xpm_data (xpm)));

    ADD_STOCK(CENTER_HORIZ, N_("Center _Horizontally"), menu_center_horizontal_xpm);
    ADD_STOCK(CENTER_VERT,  N_("Center _Vertically"),   menu_center_vertical_xpm);
    ADD_STOCK(ALIGN_LEFT,   N_("Align _Left"),          menu_align_left_xpm);
    ADD_STOCK(ALIGN_RIGHT,  N_("Align _Right"),         menu_align_right_xpm);
    ADD_STOCK(ALIGN_TOP,    N_("Align _Top"),           menu_align_top_xpm);
    ADD_STOCK(ALIGN_BOTTOM, N_("Align _Bottom"),        menu_align_bottom_xpm);

    registered = true;
}
