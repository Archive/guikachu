//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_UNDO_H
#define GUIKACHU_UNDO_H

#include <glibmm/ustring.h>
#include <deque>
#include <list>

#include <sigc++/signal.h>

namespace Guikachu
{
    class UndoOp
    {
    public:
	virtual ~UndoOp () {};
	
	virtual Glib::ustring get_label () const = 0;
	
	virtual void undo () = 0;
	virtual void redo () = 0;
	
	virtual UndoOp* combine (UndoOp *other_op) const { return 0; };
    };

    class UndoManager
    {
	class UndoStack
	{
	public:
	    typedef UndoOp*                 value_t;
	    typedef std::deque<value_t>     impl_stack_t;
	    typedef impl_stack_t::size_type size_type;

	private:
	    impl_stack_t impl_stack;
	    
	public:
	    ~UndoStack ();
	    
	    void      clear ();
	    size_type truncate (size_type max_size);
	    
	    value_t   top  () const;
	    void      pop  ();
	    void      push (value_t &value);
	    
	    bool      empty () const;
	    size_type size () const;

	    std::list<Glib::ustring> get_labels () const;
	};
	
	UndoStack undo_stack;
	UndoStack redo_stack;
	
	UndoStack::size_type origin;
	bool                 origin_valid;
	
    private:
	void push_undo (UndoOp *op);
	void push_redo (UndoOp *op);

	void truncate_undo_stack ();
	
    public:
	UndoManager ();
	
	void push (UndoOp *op);
	void undo ();
	void redo ();

	void set_origin ();

	sigc::signal0<void> changed;
	sigc::signal0<void> origin_reached;

	std::list<Glib::ustring> get_undo_labels () const;
	std::list<Glib::ustring> get_redo_labels () const;
    };
}

#endif /* !GUIKACHU_UNDO_H */
