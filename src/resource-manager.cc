//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "resource-manager.h"

#include <glib/gi18n.h>

#include <sigc++/adaptors/bind.h>

#include "ui.h"
#include "resource-util.h"

#include "dialog-res.h"
#include "form-res.h"
#include "menu-res.h"
#include "string-res.h"
#include "stringlist-res.h"
#include "blob-res.h"
#include "bitmap-res.h"
#include "bitmapfamily-res.h"

using namespace Guikachu;

ResourceManager::ResourceManager ():
    app (this),
    dirty_block (0)
{
    app.changed.connect (sigc::mem_fun (*this, &ResourceManager::set_dirty));
    target.changed.connect (sigc::mem_fun (*this, &ResourceManager::set_dirty));

    target.load_stock (Target::get_default_stock_id ());
    
    clear_dirty ();
    undo_manager.origin_reached.connect (sigc::mem_fun (*this, &ResourceManager::clear_dirty));
}

ResourceManager::~ResourceManager ()
{
    for (resource_map_t::iterator i = resource_map.begin ();
	 i != resource_map.end (); ++i)
	delete i->second;
}

namespace
{
    bool compare_serial (const ResourceManager::resource_map_t::value_type &a,
			 const ResourceManager::resource_map_t::value_type &b)
    {
	return a.first < b.first;
    }
}

Resource* ResourceManager::create_resource (Resources::Type type,
					    std::string     id,
					    bool            try_alternate_names,
					    serial_t        serial)
{
    g_return_val_if_fail (type != Resources::RESOURCE_NONE, 0);

    if (serial < 0)
	if (resource_map.size ())
	    serial = std::max_element (resource_map.begin (), resource_map.end (), compare_serial)->first + 1;
	else
	    serial = 0;

    if (serial < 0)
	return 0;
    
    id = validate_id (id);
    
    if (id == "")
	id = create_id (Resources::prefix_from_type (type));
    
    if (!register_id (id))
    {
	if (try_alternate_names) // if FOO is taken, try FOO_1
	{
	    id = create_id (id + "_");
	    register_id (id);
	} else {
	    return 0;
	}
    }

    Resource *res;
    Resources::Form *form = 0;

    switch (type)
    {
    case Resources::RESOURCE_DIALOG:
	res = new Resources::Dialog (this, id, serial);
	break;
    case Resources::RESOURCE_STRING:
	res = new Resources::String (this, id, serial);
	break;
    case Resources::RESOURCE_STRINGLIST:
	res = new Resources::StringList (this, id, serial);
	break;
    case Resources::RESOURCE_BLOB:
	res = new Resources::Blob (this, id, serial);
	break;
    case Resources::RESOURCE_MENU:
	res = new Resources::Menu (this, id, serial);
	break;
    case Resources::RESOURCE_BITMAP:
        res = new Resources::Bitmap (this, id, serial);
        break;
    case Resources::RESOURCE_BITMAPFAMILY:
        res = new Resources::BitmapFamily (this, id, serial);
        break;
    case Resources::RESOURCE_FORM:
	form = new Resources::Form (this, id, serial);
	form->widget_changed.connect (sigc::mem_fun (*this, &ResourceManager::set_dirty));
	res = form;
	break;
    case Resources::RESOURCE_NONE:
	g_assert_not_reached ();
    }
    
    resource_map.insert (std::make_pair (serial, res));

    res->changed.connect (sigc::mem_fun (*this, &ResourceManager::set_dirty));
    
    set_dirty ();
    
    resource_created.emit (res);
    
    return res;
}

void ResourceManager::remove_resource (Resource* resource)
{
    unregister_id (resource->id);
    resource_map.erase (resource->get_serial ());

    resource_removed.emit (resource);
    
    delete resource;

    set_dirty ();
}

std::set<Resource*> ResourceManager::get_resources () const
{
    std::set<Resource*> ret;

    for (resource_map_t::const_iterator i = resource_map.begin ();
	 i != resource_map.end (); i++)
    {
	ret.insert (i->second);
    }

    return ret;
}

Resource* ResourceManager::get_resource (const std::string &id) const
{
    resource_map_t::const_iterator found;
    for (found = resource_map.begin ();
         found != resource_map.end () && NoCase ().compare (found->second->id, id);
         ++found);
    
    if (found == resource_map.end ())
	return 0;
    else
	return found->second;
}

Resource* ResourceManager::get_resource (serial_t serial) const
{
    resource_map_t::const_iterator found = resource_map.find (serial);

    if (found == resource_map.end ())
	return 0;
    else
	return found->second;
}

void ResourceManager::set_dirty ()
{
    if (dirty_block)
	return;
    
    if (!modification_time)
	modification_time = time (0);
    dirty = true;

    dirty_state_changed.emit ();
}

void ResourceManager::clear_dirty ()
{
    UI::flush_events ();
    
    modification_time = 0;
    dirty = false;

    undo_manager.set_origin ();

    dirty_state_changed.emit ();
}

bool ResourceManager::is_dirty () const
{
    return dirty;
}

void ResourceManager::block_dirty ()
{
    ++dirty_block;
}

void ResourceManager::unblock_dirty ()
{
    UI::flush_events ();

    --dirty_block;
}

time_t ResourceManager::get_modification_time ()
{
    return modification_time;
}
