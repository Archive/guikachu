//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_HASH_MAP_H
#define GUIKACHU_HASH_MAP_H

#include "config.h"

#ifdef GUIKACHU_HAVE_HASH_MAP /* We have hash_map */
#ifdef GUIKACHU_HASH_MAP_IN_EXT
#include <ext/hash_map>
#else
#include <hash_map>
#endif

#else /* We have no hash_map */
#include <map>

namespace std
{
    template <class Key, class Value>
    class hash_map: public map<Key, Value> {};
}

#endif

#endif /* !GUIKACHU_HASH_MAP_H */
