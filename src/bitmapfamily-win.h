//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_BITMAPFAMILY_WIN_H
#define GUIKACHU_BITMAPFAMILY_WIN_H

#include "bitmapfamily-res.h"
#include "resource-win.h"

#include <gtkmm/window.h>
#include <gtkmm/image.h>
#include <gtkmm/button.h>

namespace Guikachu
{
    namespace GUI
    {
	class BitmapFamilyWindow: public ResourceWindow,
				  public sigc::trackable
	{
	    Resources::BitmapFamily       *res;
            Resources::Bitmap::BitmapType  type;
	    
	    Gtk::Window  window;
            Gtk::Image   preview_pixmap;
            Gtk::Button *load_button, *export_button, *remove_button;

	public:
	    BitmapFamilyWindow (Resources::BitmapFamily *res);
	    void show ();
            
	private:
	    void delete_event_impl ();
	    void update ();

            void type_changed_cb (Resources::Bitmap::BitmapType type);
            
            void load_cb ();
            void export_cb ();
            void remove_cb ();
	};
    }
}

#endif /* !GUIKACHU_BITMAPFAMILY_WIN_H */
