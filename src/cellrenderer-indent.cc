//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "cellrenderer-indent.h"

using namespace Guikachu::GUI;

CellRendererIndentBase::CellRendererIndentBase ():
    depth (0)
{
}

void CellRendererIndentBase::set_depth (int depth_)
{
    depth = depth_;
}

void CellRendererIndentBase::set_depth (const Gtk::TreeModel::Path &path)
{
    depth = path.size () - 1;
}

void CellRendererIndentBase::real_cell_mode_changed_cb ()
{
    property_mode () = get_real_cell ()->property_mode ().get_value ();
}

void CellRendererIndentBase::get_size_vfunc (Gtk::Widget          &widget,
                                             const Gdk::Rectangle *cell_area,
                                             int                  *x_offset,
                                             int                  *y_offset,
                                             int                  *width,
                                             int                  *height) const
{
    int real_x, real_y, real_w, real_h;    
    get_real_cell ()->get_size (widget, *cell_area, real_x, real_y, real_w, real_h);
    
    if (x_offset)
        *x_offset = real_x;
    
    if (y_offset)
        *y_offset = real_y;

    if (width)
        *width = real_w + depth * 20;

    if (height)
        *height = real_h;
}

void CellRendererIndentBase::render_vfunc (const Glib::RefPtr<Gdk::Drawable> &window,
                                           Gtk::Widget                       &widget,
                                           const Gdk::Rectangle              &background_area,
                                           const Gdk::Rectangle              &cell_area,
                                           const Gdk::Rectangle              &expose_area,
                                           Gtk::CellRendererState             flags)
{
    // This ugly cast is necessary to work around a bug in GTKmm 2.4
    Glib::RefPtr<Gdk::Window> window_win = Glib::RefPtr<Gdk::Window>::cast_dynamic (window);

    int real_x, real_y, real_w, real_h;    
    get_real_cell ()->get_size (widget, cell_area, real_x, real_y, real_w, real_h);
    
    Gdk::Rectangle full_cell_area = cell_area;
    full_cell_area.set_x (cell_area.get_x () + depth * 20);
    full_cell_area.set_width (cell_area.get_width () - depth * 20);

    get_real_cell ()->render (window_win, widget, background_area, full_cell_area, expose_area, flags);
}



bool CellRendererIndentBase::activate_vfunc (GdkEvent               *event,
                                             Gtk::Widget            &widget,
                                             const Glib::ustring    &path,
                                             const Gdk::Rectangle   &background_area,
                                             const Gdk::Rectangle   &cell_area,
                                             Gtk::CellRendererState  flags)
{
    int real_x, real_y, real_w, real_h;    
    get_real_cell ()->get_size (widget, cell_area, real_x, real_y, real_w, real_h);
    
    Gdk::Rectangle full_cell_area = cell_area;
    full_cell_area.set_x (cell_area.get_x () + depth * 20);
    full_cell_area.set_width (cell_area.get_width () - depth * 20);

    return get_real_cell ()->activate (event, widget, path, background_area, full_cell_area, flags);
}

            
Gtk::CellEditable * CellRendererIndentBase::start_editing_vfunc (GdkEvent               *event,
                                                                 Gtk::Widget            &widget,
                                                                 const Glib::ustring    &path,
                                                                 const Gdk::Rectangle   &background_area,
                                                                 const Gdk::Rectangle   &cell_area,
                                                                 Gtk::CellRendererState  flags)
{
    int real_x, real_y, real_w, real_h;    
    get_real_cell ()->get_size (widget, cell_area, real_x, real_y, real_w, real_h);
    
    Gdk::Rectangle full_cell_area = cell_area;
    full_cell_area.set_x (cell_area.get_x () + depth * 20);
    full_cell_area.set_width (cell_area.get_width () - depth * 20);
    
    const_cast<Gdk::Rectangle&> (cell_area) = full_cell_area;
    return get_real_cell ()->start_editing (event, widget, path, background_area, cell_area, flags);
}

template<>
Guikachu::GUI::CellRendererIndent<Gtk::CellRendererText>::CellRendererIndent ()
{
    real_cell.property_mode ().signal_changed ().connect (
        sigc::mem_fun (*this, &CellRendererIndent::real_cell_mode_changed_cb));

    real_cell.property_editable ().signal_changed ().connect (
        sigc::mem_fun (*this, &CellRendererIndent::real_cell_mode_changed_cb));
}
