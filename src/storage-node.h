//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_STORAGE_NODE_H
#define GUIKACHU_STORAGE_NODE_H

#include <string>
#include <libxml/tree.h>
#include <glibmm/ustring.h>
#include "property.h"

namespace Guikachu
{
    class Storage;
    
    class StorageNode
    {
	mutable bool  free_node;
	xmlNodePtr    node_ptr;
	Storage      *parent_doc;

	friend class Storage;

	StorageNode (xmlNodePtr node, Storage *parent_doc);

    public:
	StorageNode ();
        explicit StorageNode (const std::string &name);        
	~StorageNode ();
	
        StorageNode (const StorageNode &orig_node);
        StorageNode & operator= (const StorageNode &orig_node);
        
	std::string name () const;
	inline xmlNodePtr c_node () const { return node_ptr; };
	
	/* Tree management */
	StorageNode add_node (const std::string &name,
			      const std::string &content = "");
        void        add_node (StorageNode node);

	StorageNode  children () const;
	StorageNode  next ()     const;
	StorageNode  operator++ (int);
	StorageNode &operator++ ();

	StorageNode get_translated_child (const std::string &name) const;
	
	inline operator bool () { return node_ptr != 0; }

	/* Content management */
	void          set_content     (const std::string& content);
	std::string   get_content     () const;
	Glib::ustring get_content_raw () const;

	/* Property manipulation */
	template<class T> void set_prop (const std::string &prop_name,
					 const Property<T> &value);
	void set_prop (const std::string &prop_name, const std::string &value);
	void set_prop (const std::string &prop_name, int value);
	void set_prop (const std::string &prop_name, unsigned int value);
	void set_prop (const std::string &prop_name, char value);

        void set_prop_bool (const std::string &prop_name, bool value);
        
	int           get_prop_int        (const std::string &prop_name) const;
	std::string   get_prop_string     (const std::string &prop_name) const;
	Glib::ustring get_prop_string_raw (const std::string &prop_name) const;
	char          get_prop_char       (const std::string &prop_name) const;
        bool          get_prop_bool       (const std::string &prop_name) const;
    };

    template<class T>
    void StorageNode::set_prop (const std::string &prop_name,
				const Property<T> &value)
    {
	set_prop (prop_name, value ());
    }
};


#endif /* !GUIKACHU_STORAGE_NODE_H */
