//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "id-manager.h"

#include <glib/gi18n.h>

#include <glib.h>

#include "ui.h"

using namespace Guikachu;

IDManager::IDManager (IDManager *parent_manager_):
    parent_manager (parent_manager_)
{
}

int IDManager::NoCase::compare (const std::string &x, const std::string &y) const
{
#define ASCII_TOUPPER(c)  (((c) >= 'a' && (c) <= 'z') ? (c) - 32 : (c))
    
    // We can't use strncmp() as that would be locale-dependant
    std::string::const_iterator p = x.begin ();
    std::string::const_iterator q = y.begin ();

    while (p != x.end () && q != y.end () &&
	   ASCII_TOUPPER (*p) == ASCII_TOUPPER (*q))
    {
	p++;
	q++;
    }

    // Return value:  0 if x == y
    //               +1 if x > y
    //               -1 if x < y
    if (p == x.end () && q == y.end ()) return 0;
    if (p == x.end ()) return -1;
    if (q == y.end ()) return 1;

    return ASCII_TOUPPER (*p) < ASCII_TOUPPER (*q) ? -1 : 1;

#undef ASCII_TOUPPER
}

bool IDManager::NoCase::operator () (const std::string &x, const std::string &y) const
{
    // Return value: true  if x <  y
    //               false if x >= y
    return (compare (x, y) < 0);
}


std::string IDManager::create_id (const std::string &prefix)
{
    unsigned int num = 0;
    std::string ret_val;
    gchar *curr_name;
    const char *prefix_str = prefix.c_str ();
    
    for (num = 1; num != 0; num++)
    {
	curr_name = g_strdup_printf ("%s%d", prefix_str, num);

	if (ids.find (curr_name) == ids.end ())
	{
	    ret_val = curr_name;
	    g_free (curr_name);
	    break;
	} else {
	    g_free (curr_name);
	}
    }

    if (!num)
	return "";

    return ret_val;
}

bool IDManager::has_id (const std::string &id) const
{
    if (ids.find (id) != ids.end ())
        return true;
    
    // Recursively check parents
    if (parent_manager)
        return parent_manager->has_id (id);

    return false;
}

bool IDManager::register_id (const std::string &id)
{
    if (has_id (id))
	return false;

    ids.insert (id);
    return true;
}

void IDManager::unregister_id (const std::string &id)
{
    ids.erase (id);
}

std::string IDManager::validate_id (std::string id) const
{
    std::string allowed_characters =
	"abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789";
    
    for (std::string::iterator i = id.begin ();
	 i != id.end (); i++)
	if (allowed_characters.find (*i) == std::string::npos)
	    *i = '_';

    return id;
}

bool IDManager::change_id (const std::string &old_id, const std::string &new_id)
{
    // No-ops: old_id == new_id
    if (old_id == new_id)
	return true;

    // No-ops: new_id and old_id differ only in case
    if (NoCase ().compare (old_id, new_id) == 0)
	return true;

    // Error: empty id
    if (new_id == "")
	return false;

    // Error: old_id does not exist
    if (ids.find (old_id) == ids.end ())
	return false;

    if (has_id (new_id))
    {
	char *error_msg = g_strdup_printf (_("Non-unique identifier `%s'"), new_id.c_str ());
	UI::show_error (error_msg);
	g_free (error_msg);
	return false;
    }
    
    ids.erase (old_id);
    ids.insert (new_id);

    return true;
}
