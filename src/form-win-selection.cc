//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "form-win.h"

using namespace Guikachu::GUI;

void FormWindow::selection_add (Widget *widget)
{
    selection.insert (widget);
    widget->selected (true);

    if (last_widget != widget)
    {
	if (last_widget)
	    last_widget->selected_last (false);
	widget->selected_last (true);
    }
}

void FormWindow::selection_remove (Widget *widget)
{
    selection.erase (widget);
    widget->selected (false);

    if (last_widget == widget)
	last_widget->selected_last (false);
}

void FormWindow::selection_toggle (Widget *widget)
{
    if (selection.find (widget) == selection.end ())
	selection_add (widget);
    else
	selection_remove (widget);
}

void FormWindow::selection_clear ()
{
    for (selection_t::iterator i = selection.begin (); i != selection.end (); i++)
	(*i)->selected (false);

    if (selection.find (last_widget) != selection.end ())
	last_widget->selected_last (false);

    // FIXME: de-select form when clicking on background
//    form_widget->selected_last (false);
    
    selection.clear ();
}

void FormWindow::select_form ()
{
    bool add_to_selection;
    
    {
	Gdk::ModifierType modifiers;
	int dummy;
	
	canvas->get_window ()->get_pointer (dummy, dummy, modifiers);
	add_to_selection = modifiers & Gdk::CONTROL_MASK;
    }

    if (add_to_selection)
	return;

    selection_clear ();
    
    form_widget->selected (true);
    form_widget->selected_last (true);
   
    show_form_property_editor ();
}

void FormWindow::select_all ()
{
    //top_treeitem->deselect ();
    
    form_widget->selected (false);
    form_widget->selected_last (false);

    const std::set<Widget*> &widgets = res->get_widgets ();
    for (std::set<Widget*>::const_iterator i = widgets.begin ();
	 i != widgets.end (); ++i)
	selection_add (*i);
}

void FormWindow::widget_selected_last_cb (bool selected, Widget *widget)
{
    if (selected)
	last_widget = widget;
    else
	last_widget = 0;
}

void FormWindow::tree_widget_selected_cb (Widget *widget, bool selected)
{
    form_widget->selected_last (false);
    form_widget->selected (false);

    widget->request_edit ();
    
    if (selected)
	selection_add (widget);
    else
	selection_remove (widget);
}

void FormWindow::select_widget (Widget *widget)
{
    bool add_to_selection;
    
    {
	Gdk::ModifierType modifiers;
	int dummy;
	
	canvas->get_window ()->get_pointer (dummy, dummy, modifiers);
	add_to_selection = modifiers & Gdk::CONTROL_MASK;
    }

    form_widget->selected (false);
    form_widget->selected_last (false);
    
    widget->request_edit ();
    
    if (!add_to_selection)
    {
	if (selection.find (widget) == selection.end ())
	    selection_clear ();
	selection_add (widget);
    } else {
	selection_toggle (widget);
    }
}

void FormWindow::selection_box_cb (int x1, int y1,
				   int x2, int y2)
{
    x1 -= res->x;
    x2 -= res->x;

    y1 -= res->y;
    y2 -= res->y;
    
    bool add_to_selection;
    
    {
	Gdk::ModifierType modifiers;
	int dummy;
	
	canvas->get_window ()->get_pointer (dummy, dummy, modifiers);
	add_to_selection = modifiers & Gdk::CONTROL_MASK;
    }

    if (!add_to_selection)
	selection_clear ();

    const std::set<Widget*>& widgets = res->get_widgets ();
    for (std::set<Widget*>::const_iterator i = widgets.begin (); i != widgets.end (); i++)
    {
	FormEditor::WidgetCanvasItem *canvas_item = get_canvas_item (*i);
	
	if (canvas_item->is_within (x1, y1, x2, y2))
	    selection_add (*i);
    }
}
