//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_UI_H
#define GUIKACHU_UI_H

#include <glibmm/ustring.h>

namespace Guikachu
{
    namespace UI
    {
        void init_i18n ();
        
	void flush_events ();

	Glib::ustring visible_filename (const Glib::ustring &uri);
	Glib::ustring visible_location (const Glib::ustring &uri);
	
	void show_error   (const Glib::ustring &full_message,
			   const Glib::ustring &short_message = "");
	void show_warning (const Glib::ustring &full_message,
			   const Glib::ustring &short_message = "");


        void show_error_io_open (const Glib::ustring &uri, const Glib::ustring &error_msg);
        void show_error_io_save (const Glib::ustring &uri, const Glib::ustring &error_msg);

        void show_error_io_load_resource (const Glib::ustring &uri, const std::string &id,
                                          const Glib::ustring &error_msg);
        
        void show_error_io_save_resource (const Glib::ustring &uri, const std::string &id,
                                          const Glib::ustring &error_msg);
    }
}

#endif /* !GUIKACHU_UI_H */
