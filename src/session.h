//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_SESSION_H
#define GUIKACHU_SESSION_H

#include <string>
#include <vector>
#include <gtkmm/main.h>

#include "config.h"
#ifdef GUIKACHU_HAVE_LIBGNOMEUI
#include <libgnomeuimm/client.h>
#endif

namespace Guikachu
{
    // Forward declarations
    class ResourceManager;
    
    namespace GUI
    {
	class MainWin;
    }
    
    class Main: public Gtk::Main
    {
	static Main *instance_;

        std::string self_cmd;
        std::string last_uri;
        
	ResourceManager *doc;
	GUI::MainWin    *mainwin;
	
    public:
	Main (int argc, char **argv);
	static inline Main *instance ()    { return instance_; };
	static inline bool  initialized () { return instance_ != 0; };
	~Main ();

        void load_doc (const Glib::ustring &uri);
	void save_doc (const Glib::ustring &uri);
	void new_doc  ();
        
    protected:
	void run_impl ();
	
    private:
	void set_doc (ResourceManager   *doc);
        void set_uri (const Glib::ustring &uri, const std::string &mime_type);

#ifdef GUIKACHU_HAVE_LIBGNOMEUI
	bool session_save_cb (int                      phase,
			      Gnome::UI::SaveStyle     save_style,
			      bool                     is_shutdown,
			      Gnome::UI::InteractStyle interact_style,
			      bool                     is_fast);
	void session_die_cb  ();
	
	void save_open_file ();
#endif
    };
}

#endif /* !GUIKACHU_SESSION_H */
