//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "session.h"

#include <glib/gi18n.h>

#include <foocanvasmm/init.h>

#include <sigc++/adaptors/bind_return.h>
#include "queued-signal.h"

#include "resource-manager.h"
#include "mainwin.h"
#include "ui.h"
#include "stock.h"
#include "preferences.h"

#include "io/xml-loader.h"
#include "io/xml-saver.h"

using namespace Guikachu;

namespace {
    void gtk_queuer (sigc::slot<void> slot)
    {
        Glib::signal_idle ().connect (sigc::bind_return (slot, false), Glib::PRIORITY_DEFAULT_IDLE);
    }   
} // anonymous namespace

namespace {
#include "pixmaps/inline-pixbufs.h"
}

Main* Guikachu::Main::instance_ = 0;

Main::Main (int argc, char **argv):
    Gtk::Main (argc, argv),
    self_cmd (argv[0])
{
    if (instance_)
    {
	g_warning ("Guikachu::Main::Main called twice!\n");
	return;
    }
    instance_ = this;

    // Init subsystems
    FooCanvasmm::init ();
    IO::init ();
    Preferences::init ();
    
    QueuedSignal::set_signal_queuer (gtk_queuer);
    
    UI::register_stock ();
    static Glib::RefPtr<Gdk::Pixbuf> icon = Gdk::Pixbuf::create_from_inline (-1, guikachu_icon_gdkbuf);
    Gtk::Window::set_default_icon (icon);
    

#ifdef GUIKACHU_HAVE_LIBGNOMEUI
    // Set up session managment stuff
    Gnome::UI::Client* session_client = Gnome::UI::Client::master_client ();
    if (session_client)
    {
	session_client->signal_die ().connect (sigc::mem_fun(*this, &Main::session_die_cb));
	session_client->signal_save_yourself ().connect (sigc::mem_fun (*this, &Main::session_save_cb));
    }
#endif

    // Create initial document
    doc = new ResourceManager;
    
    // Create main window
    mainwin = new GUI::MainWin;
    mainwin->set_manager (doc);

    // Load document (last uri) from command line
    if (argc > 1)
    {   
        std::string uri = IO::create_canonical_uri (argv[argc - 1]);
        load_doc (uri);
    }
}


Main::~Main ()
{
}

void Main::run_impl ()
{
    mainwin->show ();
    Gtk::Main::run_impl ();
}

void Main::set_doc (ResourceManager *new_doc)
{
    mainwin->set_manager (new_doc);
    
    delete doc;
    doc = new_doc;
}

void Main::new_doc ()
{
    ResourceManager *new_doc = new ResourceManager;
    mainwin->set_manager (new_doc);

    delete doc;
    doc = new_doc;
    
    mainwin->set_uri ("");
}

void Main::save_doc (const Glib::ustring &uri)
{
    try {
        IO::XMLSaver saver;
        saver.save (doc, uri);
        
        doc->clear_dirty ();
        set_uri (uri, "application/x-guikachu");        
    } catch (Glib::Exception &e) {        
        UI::show_error_io_save (uri, e.what ());
    }
}

void Main::load_doc (const Glib::ustring &uri)
{
    Glib::ustring mime_type;
    IO::Loader *loader = 0;

    try {
        mime_type = IO::get_mime_type (uri);
    } catch (Glib::Exception &e) {
        UI::show_error_io_open (uri, e.what ());
        return;
    }

    loader = IO::IOFactory::instance ()->create_loader (mime_type);

    if (!loader)
    {
	char *error_msg = g_strdup_printf (_("File format \"%s\" unknown"), mime_type.c_str ());
        UI::show_error_io_open (uri, error_msg);
	g_free (error_msg);
        
	return;
    }

    ResourceManager *new_doc = new ResourceManager;
    try {
        loader->load (new_doc, uri);
        set_doc (new_doc);
        set_uri (uri, mime_type);
    } catch (Glib::Exception &e) {
        UI::show_error_io_open (uri, e.what ());
        delete new_doc;
    }
    delete loader;
}

void Guikachu::Main::set_uri (const Glib::ustring &uri,
                              const std::string   &mime_type)
{
    if (uri != "")
	Preferences::Interface::add_recent_file (uri);

    if (mime_type == "application/x-guikachu")
    {
        last_uri = uri;
	mainwin->set_uri (uri);
    } else {
	mainwin->set_uri ("");
    }
}

#ifdef GUIKACHU_HAVE_LIBGNOMEUI
void Guikachu::Main::session_die_cb ()
{
    quit ();
}

bool Guikachu::Main::session_save_cb (int                      phase,
				      Gnome::UI::SaveStyle     save_style,
				      bool                     is_shutdown,
				      Gnome::UI::InteractStyle interact_style,
				      bool                     is_fast)
{
    save_open_file ();
    
    return true;
}

void Guikachu::Main::save_open_file ()
{
    Gnome::UI::Client* session_client = Gnome::UI::Client::master_client ();
    if (session_client->is_connected ())
    {
	std::vector<std::string> argv;
	argv.push_back (self_cmd);
	argv.push_back (last_uri);
	
	session_client->set_clone_command (argv);
	session_client->set_restart_command (argv);
    }
}
#endif
