//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "form-win.h"

#include <glib/gi18n.h>

#include "form-editor/palette.h"

#include <glibmm/main.h>

#include <gdkmm/cursor.h>

#include <foocanvasmm/group.h>
#include <foocanvasmm/polygon.h>
#include "form-editor/screen-canvas.h"

#include <gtkmm/uimanager.h>
#include <gtkmm/stock.h>
#include "stock.h"

#include <sigc++/adaptors/bind.h>
#include <sigc++/adaptors/bind_return.h>

#include "form-win-helpers.h"
#include "form-win-widgettree.h"
#include "form-editor/form-editor.h"
#include "preferences.h"
#include "target.h"
#include "form-editor/widget-ops.h"

#include "form-editor/widget-factory.h"

#include "ui-gui.h"

using namespace Guikachu::GUI;
using namespace Guikachu::Resources;

// Static members of FormWindow
static Guikachu::GUI::Palette *palette = 0;


FormWindow::FormWindow (Form *res_):
    res (res_),
    edited_widget (0),
    last_widget (0),
    canvas_overlay (0),
    palette_selection (Widgets::WIDGET_NONE)
{
    Glib::RefPtr<Gnome::Glade::Xml> gui = UI::glade_gui("res_form");

    gui->get_widget ("res_form", window);

    Gtk::Container *container;
    gui->get_widget ("form_canvas_viewport", container);
    canvas = new ScreenCanvas (res->get_manager ()->get_target ());
    canvas->show ();
    container->add (*manage (canvas));

    window->signal_delete_event ().connect (
        sigc::bind_return (sigc::mem_fun (*this, &FormWindow::delete_event_impl), true));

    // Property editor window
    property_window = new Gtk::Window;
    property_window->set_title (_("Widget properties"));
    property_window->signal_delete_event ().connect (sigc::bind_return (
        sigc::mem_fun (*this, &FormWindow::property_delete_event_impl), true));
    //property_window->set_policy (false, true, true); // FIXME
    
    // Widget tree
    Gtk::TreeView *widget_treeview;
    gui->get_widget ("form_tree", widget_treeview);
    widget_tree = new FormWindow_Helpers::WidgetTreeWrapper (*widget_treeview, res);

    widget_tree->widget_selected.connect (sigc::mem_fun (*this, &FormWindow::tree_widget_selected_cb));
    widget_tree->widget_remove.connect (sigc::mem_fun (*this, &FormWindow::widget_remove_cb));
    widget_tree->widget_menu.connect (sigc::mem_fun (*this, &FormWindow::widget_menu_cb));    

    widget_tree->form_selected.connect (sigc::mem_fun (*this, &FormWindow::form_clicked_cb));
    widget_tree->form_menu.connect (sigc::mem_fun (*this, &FormWindow::form_menu_cb));
    
    // Canvas item for the form itself
    form_widget = new Widgets::Form (res);
    form_canvas_item = form_widget->get_canvas_item (*canvas->root ());
    form_canvas_item->clicked.connect (sigc::mem_fun (*this, &FormWindow::form_clicked_cb));
    form_canvas_item->released.connect (sigc::mem_fun (*this, &FormWindow::form_released_cb));
    form_canvas_item->drag_motion.connect (sigc::mem_fun (*this, &FormWindow::form_drag_motion_cb));
    form_canvas_item->drag_end.connect (sigc::mem_fun (*this, &FormWindow::form_drag_end_cb));
    form_canvas_item->context_menu.connect (sigc::mem_fun (*this, &FormWindow::form_menu_cb));
    
    // Set up initial widgets
    const std::set<Widget*>& widgets = res->get_widgets ();
    std::for_each (widgets.begin (), widgets.end (),
		   sigc::mem_fun (*this, &FormWindow::widget_created_cb));
    
    // Window callbacks
    window->signal_key_press_event ().connect (sigc::bind_return (
	sigc::mem_fun (*this, &FormWindow::key_press_cb), true));

    // Canvas selection callback
    selection_canvas = new FormWindow_Helpers::CanvasSelectionWrapper (*canvas);
    selection_canvas->selection.connect (sigc::mem_fun (*this, &FormWindow::selection_box_cb));
    
    // Form resource callbacks
    res->changed.connect (sigc::mem_fun (*this, &FormWindow::update));
    res->widget_created.connect (sigc::mem_fun (*this, &FormWindow::widget_created_cb));
    res->widget_removed.connect (sigc::mem_fun (*this, &FormWindow::widget_removed_cb));
    update ();

    // Callbacks for global settings
    Preferences::FormEditor::colors_changed.connect (sigc::mem_fun (*this, &FormWindow::colors_changed_cb));
    res->get_manager ()->get_target ()->changed.connect (
	sigc::mem_fun (*this, &FormWindow::target_changed_cb));
    
    if (!palette)
	palette = new Guikachu::GUI::Palette ();
    palette->palette_changed.connect (sigc::mem_fun (*this, &FormWindow::palette_cb));

    // Set up zoom factor entry
    gui->get_widget ("form_scrollwin", scrollwindow);
    gui->get_widget ("form_zoom_entry", zoom_entry);    
    Gtk::Adjustment *zoom_adj = zoom_entry->get_adjustment ();
    zoom_adj->signal_value_changed ().connect (sigc::mem_fun (*this, &FormWindow::zoom_entry_cb));
    zoom_adj->set_value (Preferences::FormEditor::get_default_zoom ());

    recenter_canvas ();
}

FormWindow::~FormWindow ()
{
    delete form_widget;
    delete property_window;
    delete widget_tree;
    delete selection_canvas;

    delete window;
}

void FormWindow::show ()
{
    window->show_all ();
    window->raise ();
    palette->show_all ();
    palette->raise ();

    if (edited_widget)
	show_property_editor (edited_widget);
    else
	show_form_property_editor ();
}

void FormWindow::delete_event_impl (GdkEventAny *e)
{
    window->hide ();
    property_window->hide ();
}

void FormWindow::property_delete_event_impl (GdkEventAny *e)
{
    property_window->hide ();
}

void FormWindow::update ()
{
    gchar *title_buf = g_strdup_printf (_("Form: %s"), res->id ().c_str ());
    window->set_title (title_buf);
    g_free (title_buf);

    if (!edited_widget)
    {
	gchar *prop_buf = g_strdup_printf (_("Properties: %s"), res->id ().c_str ());
	property_window->set_title (prop_buf);
	g_free (prop_buf);
    }

}

void FormWindow::recenter_canvas ()
{   
    int screen_width = res->get_manager ()->get_target ()->screen_width;
    int screen_height = res->get_manager ()->get_target ()->screen_height;
    
    canvas->set_scroll_region (0, 0, screen_width - 1, screen_height - 1);
    scrollwindow->set_size_request (screen_width + 40, screen_height + 40);
}

void FormWindow::force_canvas_update ()
{
    // Update form canvas item
    form_canvas_item->update ();

    // Update widget canvas items
    const std::set<Widget*>& widgets = res->get_widgets ();
    for (std::set<Widget*>::const_iterator i = widgets.begin ();
	 i != widgets.end (); i++)
	get_canvas_item (*i)->update ();
}

void FormWindow::colors_changed_cb ()
{
    force_canvas_update ();
}

void FormWindow::target_changed_cb ()
{
    force_canvas_update ();
    recenter_canvas ();
}

void FormWindow::zoom_entry_cb ()
{
    double zoom_factor = zoom_entry->get_value ();
    if (zoom_factor > FOO_CANVAS_EPSILON)
    {
	canvas->set_pixels_per_unit (zoom_factor);
	force_canvas_update ();
	recenter_canvas ();
    }
}

void FormWindow::widget_created_cb (Widget *widget)
{
    g_return_if_fail (widget != 0);
    
    widget->request_edit.connect (sigc::bind (
	sigc::mem_fun (*this, &FormWindow::show_property_editor),
	widget));

    widget->selected_last.connect (sigc::bind (
	sigc::mem_fun (*this, &FormWindow::widget_selected_last_cb),
	widget));

    // Add canvas item
    FormEditor::WidgetCanvasItem *canvas_item = get_canvas_item (widget);

    canvas_item->update ();
    
    canvas_item->clicked.connect (sigc::bind (sigc::mem_fun (*this, &FormWindow::widget_clicked_cb), widget));
    canvas_item->released.connect (sigc::bind (sigc::mem_fun (*this, &FormWindow::widget_released_cb), widget));
    canvas_item->context_menu.connect (sigc::bind (sigc::mem_fun (*this, &FormWindow::widget_menu_cb), widget));
    
    canvas_item->drag_motion.connect (sigc::mem_fun (*this, &FormWindow::widget_drag_motion_cb));
    canvas_item->drag_end.connect (sigc::mem_fun (*this, &FormWindow::widget_drag_end_cb));
    
}

void FormWindow::widget_removed_cb (Widget *widget)
{
    // Remove from selection
    selection_remove (widget);

    // Remove property editor
    if (widget == edited_widget)
	show_form_property_editor ();
}

bool FormWindow::canvas_overlay_cb (GdkEvent *event)
{
    if (event->type != GDK_BUTTON_PRESS)
	return false;

    if (event->button.button != 1)
	return false;

    add_widget (int (event->button.x - res->x),
		int (event->button.y - res->y));
    
    palette->palette_applied ();

    return true;
}

FormEditor::WidgetCanvasItem * FormWindow::get_canvas_item (Widget *widget)
{
    return FormEditor::get_widget_canvas_item (widget, *form_canvas_item->get_widget_group ());
}

void FormWindow::palette_cb (Widgets::Type type)
{
    if (type == palette_selection)
	return;

    palette_selection = type;
    delete canvas_overlay;
    canvas_overlay = 0;
    
    // Change pointer
    Glib::RefPtr<Gdk::Window> window = canvas->get_window ();
    if (type == Widgets::WIDGET_NONE)
    {
	window->set_cursor ();

	// Un-block rubber band selection from idle handler -- this
	// way, it's done after the _after handler in the
	// SelectionWrapper is run
	Glib::signal_idle ().connect (sigc::bind_return (sigc::bind (
	    sigc::mem_fun (*selection_canvas,
                           &FormWindow_Helpers::CanvasSelectionWrapper::set_active),
	    true), false));
	
	return;

    } else {
	window->set_cursor (Gdk::Cursor (Gdk::CROSSHAIR));
    }
    
			   
    // Create overlay canvas item
    FooCanvasmm::Points points;

    int x1 = res->x;
    int y1 = res->y;
    int x2 = x1 + res->get_manager ()->get_target ()->screen_width;
    int y2 = y1 + res->get_manager ()->get_target ()->screen_height;
    
    points.push_back (FooCanvasmm::Point (x1, y1));
    points.push_back (FooCanvasmm::Point (x2, y1));
    points.push_back (FooCanvasmm::Point (x2, y2));
    points.push_back (FooCanvasmm::Point (x1, y2));
    points.push_back (FooCanvasmm::Point (x1, y1));
    
    canvas_overlay = new FooCanvasmm::Polygon (*canvas->root (), points);
    canvas_overlay->signal_event ().connect (sigc::mem_fun (*this, &FormWindow::canvas_overlay_cb));
    canvas_overlay->raise_to_top ();

    selection_canvas->set_active (false);
}

void FormWindow::add_widget (int x, int y)
{
    g_return_if_fail (palette_selection != Widgets::WIDGET_NONE);
    
    Widget *widget = res->create_widget (palette_selection);

    widget->x = std::max (x - widget->get_width () / 2, 0);
    widget->y = std::max (y - widget->get_height () / 2, 0);

    res->get_manager ()->get_undo_manager ().push (new WidgetOps::CreateOp (widget));
}

void FormWindow::form_clicked_cb ()
{
    select_form ();

    // Block selection rubber box
    selection_canvas->set_active (false);
}

void FormWindow::form_released_cb ()
{
    // Enable selection rubber box
    selection_canvas->set_active (true);
}

#define GN_(s) dgettext ("libgnomeui-2.0", (s))

void FormWindow::form_menu_cb (guint   button,
			       guint32 time)
{
    Glib::RefPtr<Gtk::UIManager> uimanager = Gtk::UIManager::create ();
    Glib::RefPtr<Gtk::ActionGroup> group = Gtk::ActionGroup::create ();
    
    group->add (Gtk::Action::create ("edit_form", Gtk::Stock::PROPERTIES, _("_Edit Form")),
                sigc::mem_fun (*this, &FormWindow::select_form));
    group->add (Gtk::Action::create ("select_all", _("_Select All")),
                sigc::mem_fun (*this, &FormWindow::select_all));
    group->add (Gtk::Action::create ("cut", Gtk::Stock::CUT),
                sigc::bind (sigc::mem_fun (*this, &FormWindow::widget_cut_cb), (Widget*)0));
    group->add (Gtk::Action::create ("copy", Gtk::Stock::COPY),
                sigc::bind (sigc::mem_fun (*this, &FormWindow::widget_copy_cb), (Widget*)0));
    group->add (Gtk::Action::create ("paste", Gtk::Stock::PASTE),
                sigc::mem_fun (*this, &FormWindow::widget_paste_cb));

    static const char *ui_description =
        "<ui>"
        "  <popup>"
        "    <menuitem action='edit_form'/>"
        "    <menuitem action='select_all'/>"
        "    <separator/>"        
        "    <menuitem action='cut'/>"
        "    <menuitem action='copy'/>"
        "    <menuitem action='paste'/>"
        "  </popup>"
        "</ui>";
    
    if (!selection.size ())
    {
        group->get_action ("cut")->set_sensitive (false);
        group->get_action ("copy")->set_sensitive (false);
    }
    
    uimanager->insert_action_group (group);
    uimanager->add_ui_from_string (ui_description);
    
    Gtk::Widget *popup_menu_widget = uimanager->get_widget ("/popup");
    Gtk::Menu   *popup_menu = dynamic_cast<Gtk::Menu*> (popup_menu_widget);

    popup_menu->popup (button, time);
}

void FormWindow::widget_clicked_cb (Widget *widget)
{
    select_widget (widget);

    // Block selection rubber box
    selection_canvas->set_active (false);
}

void FormWindow::widget_released_cb (Widget *widget)
{
    // Enable selection rubber box
    selection_canvas->set_active (true);
}

void FormWindow::widget_menu_cb (guint    button,
				 guint32  time,
				 Widget  *widget)
{
    if (selection.find (widget) == selection.end ())
	select_widget (widget);

    Glib::RefPtr<Gtk::UIManager> uimanager = Gtk::UIManager::create ();
    Glib::RefPtr<Gtk::ActionGroup> group = Gtk::ActionGroup::create ();
    
    group->add (Gtk::Action::create ("edit", Gtk::Stock::PROPERTIES, _("_Edit Form")),
                sigc::bind (sigc::mem_fun (*this, &FormWindow::select_widget), widget));
    group->add (Gtk::Action::create ("select_all", _("_Select All")),
                sigc::mem_fun (*this, &FormWindow::select_all));
    
    group->add (Gtk::Action::create ("cut", Gtk::Stock::CUT),
                sigc::bind (sigc::mem_fun (*this, &FormWindow::widget_cut_cb), widget));
    group->add (Gtk::Action::create ("copy", Gtk::Stock::COPY),
                sigc::bind (sigc::mem_fun (*this, &FormWindow::widget_copy_cb), widget));
    group->add (Gtk::Action::create ("paste", Gtk::Stock::PASTE),
                sigc::mem_fun (*this, &FormWindow::widget_paste_cb));
    
    group->add (Gtk::Action::create ("duplicate", _("_Duplicate")),
                sigc::bind (sigc::mem_fun (*this, &FormWindow::widget_duplicate_cb), widget));
    group->add (Gtk::Action::create ("remove", Gtk::Stock::DELETE),
                sigc::bind (sigc::mem_fun (*this, &FormWindow::widget_remove_cb), widget));

    group->add (Gtk::Action::create ("center-horiz", GUI::Stock::CENTER_HORIZ),
                sigc::mem_fun (*this, &FormWindow::center_horizontal));
    group->add (Gtk::Action::create ("center-vert", GUI::Stock::CENTER_VERT),
                sigc::mem_fun (*this, &FormWindow::center_vertical));

    group->add (Gtk::Action::create ("align-left", GUI::Stock::ALIGN_LEFT),
                sigc::mem_fun (*this, &FormWindow::align_left));
    group->add (Gtk::Action::create ("align-right", GUI::Stock::ALIGN_RIGHT),
                sigc::mem_fun (*this, &FormWindow::align_right));
    group->add (Gtk::Action::create ("align-top", GUI::Stock::ALIGN_TOP),
                sigc::mem_fun (*this, &FormWindow::align_top));
    group->add (Gtk::Action::create ("align-bottom", GUI::Stock::ALIGN_BOTTOM),
                sigc::mem_fun (*this, &FormWindow::align_bottom));
    
    static const char *ui_description =
        "<popup>"
        "  <menuitem action='edit'/>"
        "  <menuitem action='select_all'/>"
        "  <separator/>"        
        "  <menuitem action='cut'/>"
        "  <menuitem action='copy'/>"
        "  <menuitem action='paste'/>"
        "  <separator/>"        
        "  <menuitem action='duplicate'/>"
        "  <menuitem action='remove'/>"
        "  <separator/>"        
        "  <menuitem action='center-horiz'/>"
        "  <menuitem action='center-vert'/>"
        "  <separator/>"
        "  <placeholder name='align'/>"
        "</popup>";

    static const char *align_description =
        "<popup>"
        "  <placeholder name='align'>"
        "    <menuitem action='align-left'/>"
        "    <menuitem action='align-right'/>"
        "    <menuitem action='align-top'/>"
        "    <menuitem action='align-bottom'/>"
        "  </placeholder>"
        "</popup>";
    
    uimanager->insert_action_group (group);
    uimanager->add_ui_from_string (ui_description);
    if (selection.size () > 1 && selection.find (widget) != selection.end ())
        uimanager->add_ui_from_string (align_description);
    
    Gtk::Widget *popup_menu_widget = uimanager->get_widget ("/popup");
    Gtk::Menu   *popup_menu = dynamic_cast<Gtk::Menu*> (popup_menu_widget);

    popup_menu->popup (button, time);
}

void FormWindow::widget_remove_cb (Widget *widget)
{
    if (!widget || selection.find (widget) != selection.end ())
    {
	remove_selection ();
    } else {
	res->get_manager ()->get_undo_manager ().push (new WidgetOps::RemoveOp (widget));
	res->remove_widget (widget);
    }
}

void FormWindow::form_drag_motion_cb (int dx, int dy)
{
    form_canvas_item->move (dx, dy);
}

void FormWindow::form_drag_end_cb (int dx, int dy)
{
    UndoOp *op = new FormWindow_Helpers::FormMoveOp (
	res, res->x + dx, res->y + dy);
    res->get_manager ()->get_undo_manager ().push (op);

    res->x += dx;
    res->y += dy;
}

void FormWindow::widget_drag_motion_cb (int dx, int dy)
{
    for (selection_t::iterator i = selection.begin (); i != selection.end (); i++)
	get_canvas_item (*i)->move (dx, dy);
}

void FormWindow::widget_drag_end_cb (int dx, int dy)
{
    UndoOp *op = new WidgetOps::MultiMoveOp (selection, dx, dy);
    res->get_manager ()->get_undo_manager ().push (op);
	
    for (selection_t::iterator i = selection.begin (); i != selection.end (); i++)
    {
	(*i)->x += dx;
	(*i)->y += dy;
    }
}

void FormWindow::key_press_cb (GdkEventKey *e)
{
    // Del: delete current selection
    if (e->keyval == GDK_Delete || e->keyval == GDK_KP_Delete)
	remove_selection ();
}

void FormWindow::remove_selection ()
{
    if (!selection.size ())
	return;
    
    res->get_manager ()->get_undo_manager ().push (
	new WidgetOps::MultiRemoveOp (selection));
    
    for (selection_t::iterator i = selection.begin (); i != selection.end ();)
    {
	Widget *curr_widget = *(i++);
	
	res->remove_widget (curr_widget);
    }
}
