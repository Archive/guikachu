//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_MENU_WIN_H
#define GUIKACHU_MENU_WIN_H

#include "menu-res.h"
#include "resource-win.h"

#include <gtkmm/window.h>
#include <gtkmm/entry.h>
#include <gtkmm/button.h>
#include <gtkmm/treeview.h>
#include <gtkmm/menu.h>

#include "menu-win-treemodel.h"
#include "menu-win-stock.h"

namespace Guikachu
{
    namespace GUI
    {
	class MenuWindow: public ResourceWindow,
			  public sigc::trackable
	{
	    Resources::Menu *res;

	    Gtk::Window   *window;
	    Gtk::TreeView *treeview;
	    
	    Gtk::Entry  *id_entry;
	    Gtk::Button *btnRemove, *btnUp, *btnDown;
            Gtk::Widget *btnSubmenu, *btnItem;
            Gtk::Menu   *popup_menu, *dropdown_menu;

	    Gtk::TreeModelColumn<bool>          col_is_submenu;
            Gtk::TreeModelColumn<Glib::ustring> col_label;
            Gtk::TreeModelColumn<Glib::ustring> col_id;
            Gtk::TreeModelColumn<char>          col_shortcut;
            Gtk::TreeModelColumn<bool>          col_is_separator;

            Glib::RefPtr<MenuWindow_Helpers::MenuTreeModel> treemodel;
	    
	public:
	    MenuWindow (Guikachu::Resources::Menu *res);
	    ~MenuWindow ();

	    void show ();

	private:
            Gtk::Entry *current_entry;
            int         current_entry_pos;
	    int         update_block;

	    void update ();

            void create_columns ();
            
	    void id_entry_cb ();

            void cell_label_cb    (Gtk::CellRenderer *cell, const Gtk::TreeModel::iterator &iter) const;
            void cell_id_cb       (Gtk::CellRenderer *cell, const Gtk::TreeModel::iterator &iter) const;
            void cell_shortcut_cb (Gtk::CellRenderer *cell, const Gtk::TreeModel::iterator &iter) const;

            void cell_label_edit_cb    (const Glib::ustring &path, const Glib::ustring &new_text);
            void cell_id_edit_cb       (const Glib::ustring &path, const Glib::ustring &new_text);
            void cell_shortcut_edit_cb (const Glib::ustring &path, const Glib::ustring &new_text);
            
	    void delete_event_impl (GdkEventAny *e);

            void button_press_cb   (GdkEventButton *e);
	    void key_press_cb      (GdkEventKey    *e);
            void show_popup_menu   (guint button, guint32 time);
            
	    void reset_controls ();

            void entry_insert_cb (const Glib::ustring &text, int *pos, Gtk::Entry *entry);
            void entry_delete_cb (int start_pos, int end_pos, Gtk::Entry *entry);
            
	    void btn_remove_cb ();
            void btn_up_cb ();
            void btn_down_cb ();
            
	    void create_submenu_cb ();
            void create_stock_submenu_cb (MenuWindow_Helpers::StockMenu stock_id);
	    void create_item_cb (bool separator);

	    void selection_changed_cb ();
	};
    }
}

#endif /* !GUIKACHU_MENU_WIN_H */
