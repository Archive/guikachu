//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <glib/gi18n.h>
#include <glib.h>

#include "edit-ops.h"
#include "edit-priv.h"

#include "io/xml-loader.h"
#include "io/xml-saver.h"

using namespace Guikachu::Edit;

PasteResourceOp::PasteResourceOp (Resource *pasted_resource) :
    manager (pasted_resource->get_manager ()),
    serial (pasted_resource->get_serial ()),
    type (pasted_resource->get_type ()),
    id (pasted_resource->id)
{
    char *op_label_str = g_strdup_printf (_("Paste %s"), id.c_str ());
    op_label = op_label_str;
    g_free (op_label_str);

    IO::XML::ResourceSaver saver (node);
    pasted_resource->apply_visitor (saver);
}

void PasteResourceOp::undo ()
{
    Resource *resource = manager->get_resource (serial);
    g_assert (resource);
    
    manager->remove_resource (resource);
}

void PasteResourceOp::redo ()
{
    Resource *resource = manager->create_resource (type, id, false, serial);

    IO::XML::ResourceLoader loader (node);
    resource->apply_visitor (loader);
}

PasteResourcesOp::PasteResourcesOp (const std::set<Resource*> &pasted_resources)
{
    for (std::set<Resource*>::const_iterator i = pasted_resources.begin ();
	 i != pasted_resources.end (); ++i)
	paste_ops.insert (new PasteResourceOp (*i));
}

Glib::ustring PasteResourcesOp::get_label () const
{
    return _("Paste resources");
}

void PasteResourcesOp::undo ()
{
    for (paste_ops_t::iterator i = paste_ops.begin (); i != paste_ops.end (); ++i)
	(*i)->undo ();
}

void PasteResourcesOp::redo ()
{
    for (paste_ops_t::iterator i = paste_ops.begin (); i != paste_ops.end (); ++i)
	(*i)->redo ();
}



DuplicateResourceOp::DuplicateResourceOp (Resource *original, Resource *duplicated) :
    manager (original->get_manager ()),
    original_serial   (original->get_serial ()),
    duplicated_serial (duplicated->get_serial ())
{
    char *op_label_str = g_strdup_printf (_("Create copy of %s"), original->id ().c_str ());
    op_label = op_label_str;
    g_free (op_label_str);
}

void DuplicateResourceOp::undo ()
{
    Resource *resource = manager->get_resource (duplicated_serial);
    g_assert (resource);
    
    manager->remove_resource (resource);
}

void DuplicateResourceOp::redo ()
{
    Resource *original = manager->get_resource (original_serial);
    g_assert (original);
    
    Resource *duplicated = Priv::duplicate_resource_impl (original);
    g_assert (duplicated);

    duplicated_serial = duplicated->get_serial ();
}



PasteWidgetOp::PasteWidgetOp (Widget *pasted_widget) :
    manager (pasted_widget->get_manager ()),
    form_serial (pasted_widget->get_form ()->get_serial ()),
    widget_serial (pasted_widget->get_serial ()),
    type (pasted_widget->get_type ()),
    id (pasted_widget->id)
{
    char *op_label_str = g_strdup_printf (_("Paste %s into %s"),
					  id.c_str (), pasted_widget->get_form ()->id ().c_str ());
    op_label = op_label_str;
    g_free (op_label_str);

    IO::XML::WidgetSaver saver (node);
    pasted_widget->apply_visitor (saver);
}

void PasteWidgetOp::undo ()
{
    Resources::Form *form = static_cast<Resources::Form*> (manager->get_resource (form_serial));
    Widget *widget = form->get_widget (id);
    g_assert (widget);

    form->remove_widget (widget);
}

void PasteWidgetOp::redo ()
{
    Resources::Form *form = static_cast<Resources::Form*> (manager->get_resource (form_serial));
    Widget *widget = form->create_widget (type, id, false, widget_serial);
    
    IO::XML::WidgetLoader loader (node); 
    widget->apply_visitor (loader);
}



PasteWidgetsOp::PasteWidgetsOp (const std::set<Widget*> &pasted_widgets)
{   
    for (std::set<Widget*>::const_iterator i = pasted_widgets.begin ();
	 i != pasted_widgets.end (); ++i)
	paste_ops.insert (new PasteWidgetOp (*i));

    std::string form_id = (*pasted_widgets.begin ())->get_form ()->id;
    char *op_label_str = g_strdup_printf (_("Paste widgets into %s"), form_id.c_str ());
    op_label = op_label_str;
    g_free (op_label_str);
}

void PasteWidgetsOp::undo ()
{
    for (paste_ops_t::iterator i = paste_ops.begin (); i != paste_ops.end (); ++i)
	(*i)->undo ();
}

void PasteWidgetsOp::redo ()
{
    for (paste_ops_t::iterator i = paste_ops.begin (); i != paste_ops.end (); ++i)
	(*i)->redo ();
}




DuplicateWidgetOp::DuplicateWidgetOp (Widget *original, Widget *duplicated) :
    manager (original->get_manager ()),
    form_serial (original->get_form ()->get_serial ()),
    original_serial   (original->get_serial ()),
    duplicated_serial (duplicated->get_serial ())
{
    char *op_label_str = g_strdup_printf (_("Create copy of %s"), original->id ().c_str ());
    op_label = op_label_str;
    g_free (op_label_str);
}

void DuplicateWidgetOp::undo ()
{
    Resources::Form *form = static_cast<Resources::Form*> (manager->get_resource (form_serial));
    Widget *widget = form->get_widget (duplicated_serial);
    g_assert (widget);

    form->remove_widget (widget);
}

void DuplicateWidgetOp::redo ()
{
    Resources::Form *form = static_cast<Resources::Form*> (manager->get_resource (form_serial));
    Widget *original = form->get_widget (original_serial);
    g_assert (original);
    
    Widget *duplicated = Priv::duplicate_widget_impl (original);
    g_assert (duplicated);

    duplicated_serial = duplicated->get_serial ();
}


DuplicateWidgetsOp::DuplicateWidgetsOp (const std::set<std::pair<Widget*, Widget*> > &duplicated_widgets)
{
    for (std::set<std::pair<Widget*, Widget*> >::const_iterator i = duplicated_widgets.begin ();
	 i != duplicated_widgets.end (); ++i)
	duplicate_ops.insert (new DuplicateWidgetOp (i->first, i->second));

    std::string form_id = duplicated_widgets.begin ()->first->get_form ()->id;
    char *op_label_str = g_strdup_printf (_("Duplicate widgets of %s"), form_id.c_str ());
    op_label = op_label_str;
    g_free (op_label_str);
}

void DuplicateWidgetsOp::undo ()
{
    for (duplicate_ops_t::iterator i = duplicate_ops.begin (); i != duplicate_ops.end (); ++i)
	(*i)->undo ();
}

void DuplicateWidgetsOp::redo ()
{
    for (duplicate_ops_t::iterator i = duplicate_ops.begin (); i != duplicate_ops.end (); ++i)
	(*i)->redo ();
}
