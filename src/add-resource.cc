//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "add-resource.h"

#include <glib/gi18n.h>

#include <gtkmm/optionmenu.h>
#include <gtkmm/menu.h>

#include "resource-manager.h"
#include "resource-manager-ops.h"
#include "resource-util.h"
#include "resource-util-gui.h"

#include "ui.h"
#include "ui-gui.h"

using namespace Guikachu;

Resources::Type GUI::AddResourceWin::last_type = Resources::RESOURCE_FORM;

GUI::AddResourceWin::AddResourceWin (ResourceManager *manager_):
    manager (manager_)
{
    Glib::RefPtr<Gnome::Glade::Xml> gui = UI::glade_gui ("dlg_add");
    
    gui->get_widget ("dlg_add", dialog);
    gui->get_widget ("add_name", id_entry);
    gui->get_widget ("add_type", type_combo);    
    
    Gtk::TreeModel::ColumnRecord cols;
    cols.add (col_type);    

    store = Gtk::ListStore::create (cols);

    Gtk::TreeRow row, selected_row;
#define ADD_TYPE(t)                \
        row = *(store->append ()); \
        row[col_type] = t;         \
        if (t == last_type)        \
            selected_row = row;
    
    ADD_TYPE (Resources::RESOURCE_FORM);
    ADD_TYPE (Resources::RESOURCE_DIALOG);
    ADD_TYPE (Resources::RESOURCE_MENU);
    ADD_TYPE (Resources::RESOURCE_BITMAP);
    ADD_TYPE (Resources::RESOURCE_BITMAPFAMILY);
    ADD_TYPE (Resources::RESOURCE_STRING);
    ADD_TYPE (Resources::RESOURCE_STRINGLIST);
    ADD_TYPE (Resources::RESOURCE_BLOB);

#undef ADD_TYPE
    
    type_combo->set_model (store);
    type_combo->set_active (selected_row);

    CellRendererIconText *cell = new CellRendererIconText;
    type_combo->pack_start (*manage (cell));
    type_combo->set_cell_data_func (*cell, sigc::bind (sigc::mem_fun (*this, &AddResourceWin::cell_cb), cell));
}

void GUI::AddResourceWin::cell_cb (const Gtk::TreeModel::iterator &iter, CellRendererIconText *cell)
{
    Resources::Type type = (*iter)[col_type];
    Glib::ustring label = Resources::display_name_from_type (type);
    Glib::RefPtr<Gdk::Pixbuf> pixbuf = Resources::get_type_pixbuf (type);

    cell->property_pixbuf () = pixbuf;
    cell->property_text () = label;
}

void GUI::AddResourceWin::run (Gtk::Window *parent_win)
{
    if (parent_win)
	dialog->set_transient_for (*parent_win);
    
    int dialog_result;

    dialog_result = dialog->run ();
    dialog->hide ();

    if (dialog_result == Gtk::RESPONSE_OK)
    {
	std::string id = id_entry->get_text ();
        last_type = (*type_combo->get_active ())[col_type];
        
	Resource *res = manager->create_resource (last_type, id);
	if (!res)
	{
	    char *error_msg = g_strdup_printf (_("Non-unique identifier `%s'"), id.c_str ());
	    UI::show_error (error_msg);
	    g_free (error_msg);

	    return;
	}
	
	manager->get_undo_manager ().push (new ResourceOps::CreateOp (res));
    }
}
