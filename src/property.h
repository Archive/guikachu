//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_PROPERTY_H
#define GUIKACHU_PROPERTY_H

#include "vsignal.h"
#include <string>
#include <vector>
#include <iostream>
#include <glibmm/ustring.h>

namespace Guikachu
{
    template <class T>
    class Property
    {
    protected:
	typedef T           value_t;
	typedef Property<T> self_t;
    
	typedef VSignal     notify_signal_t;

    public:
	notify_signal_t &changed;
	
    protected:
	value_t          value;
	
    public:
	Property (notify_signal_t &notify_signal,
		  const value_t   &value_ = value_t ()):
	    changed (notify_signal),
	    value (value_)
	    {
	    };
	    
	virtual ~Property () {};

	virtual inline const value_t & get_val () const { return value; };
	virtual void set_val (const value_t &value_)
	    {
		if (value != value_)
		{
		    value = value_;
		    changed ();
		}
	    };
	
	inline const value_t& operator= (const value_t &value_) { set_val (value_); return value; };
	inline const value_t& operator= (const self_t  &other)  { set_val (other.value); return value; };
	inline operator value_t () const { return get_val (); };
	inline value_t operator() () const { return get_val (); };

	inline bool operator== (const value_t &other) const { return value == other; };
	inline bool operator== (const self_t  &other) const { return value == other.value; };
	
	inline bool operator!= (const value_t &other) const { return value != other; };
	inline bool operator!= (const self_t  &other) const { return value != other.value; };

	inline value_t operator+= (const value_t &other) { set_val (value + other); return value; };
	inline value_t operator+= (const self_t  &other) { set_val (value + other.value); return value; };

	inline value_t operator-= (const value_t &other) { set_val (value - other); return value; };
	inline value_t operator-= (const self_t  &other) { set_val (value - other.value); return value; };
    };
    
    // Specializations
    template<>
    void Property<std::string>::set_val (const std::string &value);

    template<>
    void Property<std::vector<std::string> >::set_val (const std::vector<std::string> &value);

    std::string convert_to_ascii (const Glib::ustring &utf8);
    
    // Special property subclasses
    
    class IDManager; // Forward declaration
    
    class ID: public Property<std::string>
    {
	IDManager *manager;
	
    public:
	ID (notify_signal_t   &notify_signal,
	    IDManager         *manager,
	    const std::string &value);
	
	inline const std::string& operator= (const std::string &value_) {
	    set_val (value_); return value;
	};
	
	void set_val (const std::string &value);
    };
}

#endif /* !GUIKACHU_PROPERTY_H */
