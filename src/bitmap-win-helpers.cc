//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "bitmap-win-helpers.h"

#include <glib/gi18n.h>

#include "property-ops-resource.h"

using namespace Guikachu::GUI::BitmapWindow_Helpers;

DepthCombo::DepthCombo (Resources::Bitmap *res_):
    res (res_),
    update_block (false)
{
    Gtk::TreeModel::ColumnRecord cols;
    cols.add (col_type);
    cols.add (col_label);
    
    store = Gtk::ListStore::create (cols);

    // Fill the model
    const depth_list_t &depth_list = BitmapWindow_Helpers::get_depth_list ();
    for (depth_list_t::const_iterator i = depth_list.begin (); i != depth_list.end (); ++i)
    {
        Gtk::TreeRow row = *(store->append ());
        row[col_type] = i->first;
        row[col_label] = i->second;
    }
    
    // Set the view
    set_model (store);
    pack_start (col_label);

    signal_changed ().connect (sigc::mem_fun (*this, &DepthCombo::changed_cb));
    res->bitmap_type.changed.connect (sigc::mem_fun (*this, &DepthCombo::update));
    update ();
}

void DepthCombo::update ()
{
    Resources::Bitmap::BitmapType type = res->bitmap_type;
    
    Gtk::TreeModel::iterator iter = store->get_iter ("0");
    while ((*iter)[col_type] != type)
        ++iter;
    
    update_block = true;
    set_active (iter);
    update_block = false;
}

void DepthCombo::changed_cb ()
{
    if (update_block)
        return;

    Gtk::TreeModel::iterator iter = get_active ();
    Resources::Bitmap::BitmapType type = (*iter)[col_type];
    
    char *label_str = g_strdup_printf (_("Change type of %s"), res->id ().c_str ());
    UndoOp *op = new ResourceOps::PropChangeOp<Resources::Bitmap::BitmapType> (
	label_str, res, res->bitmap_type, type, false);
    g_free (label_str);
    
    res->bitmap_type = type;

    res->get_manager ()->get_undo_manager ().push (op);
}


const depth_list_t & Guikachu::GUI::BitmapWindow_Helpers::get_depth_list ()
{
    static depth_list_t depth_list;
    static bool depth_list_init = false;
    
    if (!depth_list_init)
    {
        using Guikachu::Resources::Bitmap;
        
        depth_list.push_back (depth_pair_t (Bitmap::TYPE_MONO,      _("Monochrome")));
        depth_list.push_back (depth_pair_t (Bitmap::TYPE_GREY_4,    _("Greyscale (4 shades)")));
        depth_list.push_back (depth_pair_t (Bitmap::TYPE_GREY_16,   _("Greyscale (16 shades)")));
        depth_list.push_back (depth_pair_t (Bitmap::TYPE_COLOR_16,  _("Color (16 colors)")));
        depth_list.push_back (depth_pair_t (Bitmap::TYPE_COLOR_256, _("Color (256 colors)")));
        depth_list.push_back (depth_pair_t (Bitmap::TYPE_COLOR_16K, _("High Color (16k colors)")));
        
        depth_list_init = true;
    }
    
    return depth_list;
}



bool Guikachu::GUI::BitmapWindow_Helpers::is_image_file (const Gtk::FileFilter::Info &filter_info)
{
    g_return_val_if_fail (filter_info.contains && Gtk::FILE_FILTER_MIME_TYPE, false);
    
    return filter_info.mime_type.substr (0, 6) == "image/";
}
