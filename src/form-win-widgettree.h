//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_WIN_WIDGET_TREE_H
#define GUIKACHU_FORM_WIN_WIDGET_TREE_H

#include "form-res.h"
#include "form-editor/widget.h"

#include <map>

#include <gtkmm/treeview.h>
#include <gtkmm/treestore.h>

namespace Guikachu
{
    namespace GUI
    {
	namespace FormWindow_Helpers
	{
	    class WidgetTreeWrapper: public sigc::trackable
	    {
		Gtk::TreeView   &treeview;
		Resources::Form *form;
		
		Gtk::TreeModelColumn<Guikachu::Widget*> col_widget;
		Glib::RefPtr<Gtk::TreeStore>            treestore;
		
		Gtk::TreeRow  root_tree;

		typedef std::map<Guikachu::Widget*, Gtk::TreeRow> row_map_t;
		row_map_t row_map;

		bool selection_block;
                bool menu_block;
		
	    public:
		WidgetTreeWrapper (Gtk::TreeView   &treeview,
				   Resources::Form *form);

		sigc::signal0<void>                 form_selected;
                sigc::signal2<void, guint, guint32> form_menu;
                
		sigc::signal2<void, Guikachu::Widget*, bool>           widget_selected;
                sigc::signal1<void, Guikachu::Widget*>                 widget_remove;
		sigc::signal3<void, guint, guint32, Guikachu::Widget*> widget_menu;
		
	    private:
                void cell_label_cb (Gtk::CellRenderer *cell, const Gtk::TreeModel::iterator &iter) const;
                
		void form_changed_cb ();
		
		void widget_created_cb (Guikachu::Widget *widget);
		void widget_removed_cb (Guikachu::Widget *widget);
		void widget_changed_cb (Guikachu::Widget *widget);

		void widget_selected_cb (bool selected,
					 Guikachu::Widget *widget);

		bool selection_changed_cb (const Glib::RefPtr<Gtk::TreeModel> &model,
					   const Gtk::TreeModel::Path         &path,
					   bool                                previously_selected);

		void button_press_cb (GdkEventButton *e);
                void key_press_cb (GdkEventKey *e);
                
                Gtk::TreeStore::iterator get_place (Guikachu::Widget *widget) const;
                bool compare_treerow (const Gtk::TreeRow &row, Guikachu::Widget *widget) const;
	    };
	}
    }
}

#endif /* !GUIKACHU_FORM_WIN_WIDGET_TREE_H */
