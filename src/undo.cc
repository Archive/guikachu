//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "undo.h"

#include "preferences.h"

using namespace Guikachu;

UndoManager::UndoStack::~UndoStack ()
{
    clear ();
}

void UndoManager::UndoStack::clear ()
{
    for (impl_stack_t::iterator i = impl_stack.begin ();
         i != impl_stack.end (); i++)
        delete *i;
    
    impl_stack.clear ();
}

UndoManager::UndoStack::size_type UndoManager::UndoStack::size () const
{
    return impl_stack.size ();
}

// returns number of removed items
UndoManager::UndoStack::size_type UndoManager::UndoStack::truncate (size_type max_size)
{
    if (size () <= max_size)
	return 0;

    size_type num_truncated = size () - max_size;
    
    impl_stack_t::iterator del_begin = impl_stack.begin ();
    impl_stack_t::iterator del_end = del_begin + num_truncated;
    for (impl_stack_t::iterator i = del_begin; i != del_end; ++i)
	delete *i;
    impl_stack.erase (del_begin, del_end);

    return num_truncated;
}

UndoManager::UndoStack::value_t UndoManager::UndoStack::top () const
{
    return impl_stack.back ();
}

void UndoManager::UndoStack::pop ()
{
    impl_stack.erase (--impl_stack.end ());
}

void UndoManager::UndoStack::push (value_t &value)
{
    impl_stack.push_back (value);
}

bool UndoManager::UndoStack::empty () const
{
    return !impl_stack.size ();
}

std::list<Glib::ustring> UndoManager::UndoStack::get_labels () const
{
    std::list<Glib::ustring> retval;
    
    for (impl_stack_t::const_reverse_iterator i = impl_stack.rbegin ();
	 i != impl_stack.rend (); ++i)
	retval.push_back ((*i)->get_label ());

    return retval;
}


UndoManager::UndoManager () :
    origin (0),
    origin_valid (true)
{
}

void UndoManager::push (UndoOp *op)
{
    // Redo stack is invalidated
    redo_stack.clear ();
    
    push_undo (op);
    changed.emit ();
}

void UndoManager::undo ()
{
    if (!undo_stack.size ())
        return;
    
    UndoOp *top_op = undo_stack.top ();
    undo_stack.pop ();
    
    top_op->undo ();

    push_redo (top_op);
    changed.emit ();

    if (origin_valid && origin == undo_stack.size ())
	origin_reached.emit ();
}


void UndoManager::redo ()
{
    if (!redo_stack.size ())
        return;

    UndoOp *top_op = redo_stack.top ();
    redo_stack.pop ();
    
    top_op->redo ();
    
    push_undo (top_op);
    changed.emit ();
    
    if (origin_valid && origin == undo_stack.size ())
	origin_reached.emit ();
}

void UndoManager::push_undo (UndoOp *op)
{
    // Try to combine new op with topmost op on undo stack
    if (!undo_stack.empty ())
    {
        UndoOp *top_op = undo_stack.top ();
        UndoOp *combined_op = top_op->combine (op);
        if (combined_op)
	{
	    delete top_op;
	    undo_stack.pop ();
            op = combined_op;
        }
    }
    
    undo_stack.push (op);
    truncate_undo_stack ();
}

void UndoManager::push_redo (UndoOp *op)
{
    redo_stack.push (op);
}

void UndoManager::truncate_undo_stack ()
{
    unsigned int undo_max_size = Preferences::Interface::get_undo_size ();

    UndoStack::size_type num_truncated = undo_stack.truncate (undo_max_size);

    if (origin_valid)
	if (num_truncated > origin)
	    origin_valid = false;
	else
	    origin -= num_truncated;
}

void UndoManager::set_origin ()
{
    origin = undo_stack.size ();
    origin_valid = true;
}

std::list<Glib::ustring> UndoManager::get_undo_labels () const
{
    return undo_stack.get_labels ();
}

std::list<Glib::ustring> UndoManager::get_redo_labels () const
{
    return redo_stack.get_labels ();
}
