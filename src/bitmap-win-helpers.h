//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_BITMAP_WIN_HELPERS_H
#define GUIKACHU_BITMAP_WIN_HELPERS_H

#include "bitmap-res.h"

#include <gtkmm/combobox.h>
#include <gtkmm/liststore.h>
#include <gtkmm/filefilter.h>

namespace Guikachu
{
    namespace GUI
    {
        namespace BitmapWindow_Helpers
        {
            class DepthCombo: public Gtk::ComboBox
            {
                Resources::Bitmap *res;

                Gtk::TreeModelColumn<Resources::Bitmap::BitmapType> col_type;
                Gtk::TreeModelColumn<Glib::ustring>                 col_label;
                
                Glib::RefPtr<Gtk::ListStore> store;
                
            public:
                DepthCombo (Resources::Bitmap *res);

            private:
                bool update_block;
                void update ();
                void changed_cb ();
            };
            
            typedef std::pair<Resources::Bitmap::BitmapType, Glib::ustring> depth_pair_t;
            typedef std::list<depth_pair_t> depth_list_t;
            
            const depth_list_t & get_depth_list ();
            
            bool is_image_file (const Gtk::FileFilter::Info &filter_info);
        }
    }
}

#endif /* !GUIKACHU_BITMAP_WIN_HELPERS_H */
