//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 *
 * This portion of the code is written by Martin Schulze <MHL.Schulze@t-online.de>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "vsignal.h"

using namespace Guikachu;

VSignal::connection_t VSignal::connect (const VSignal::slot_t &s)
{
    return real_signal.connect (s);
}

void VSignal::emit ()
{
    real_signal.emit ();
}

VSignal::signal_slot_t VSignal::make_slot() const
{
    return real_signal.make_slot ();
}
