//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "property.h"

#include "resource-manager.h"
#include <algorithm>

using namespace Guikachu;

ID::ID (notify_signal_t   &notify_signal,
	IDManager         *manager_,
	const std::string &value):
    Property<std::string> (notify_signal, value),
    manager (manager_)
{
}

void ID::set_val (const std::string &value_)
{
    std::string real_id = manager->validate_id (value_);

    if (manager->change_id (value, real_id))
	Property<std::string>::set_val (real_id);
}

namespace Guikachu
{

std::string convert_to_ascii (const Glib::ustring &utf8)
{
    std::string ret_val;
    ret_val.reserve (utf8.length ());

#define GET_ASCII_CHAR(c) (c < 128 ? char (c) : '_')
    
    for (Glib::ustring::const_iterator i = utf8.begin (); i != utf8.end ();  ++i)
	ret_val += GET_ASCII_CHAR(*i);

#undef GET_ASCII_CHAR
    
    return ret_val;
}
    
// The following is needed to make sure the object really contains the
// required specializations
namespace
{
    void foo ()
    {
	VSignal s;
	
	Property<std::string>               x1 (s);
	Property<std::vector<std::string> > x2 (s);
    }
    
} // anonymous namespace


    
template<>
void Property<std::string>::set_val (const std::string &value_)
{
    std::string ascii_value = convert_to_ascii (value_);

    if (ascii_value == value)
	return;

    value = ascii_value;
    changed ();
}

template<>
void Property<std::vector<std::string> >::set_val (const std::vector<std::string> &value_)
{
    std::vector<std::string> ascii_values (value_.size ());
    std::transform (value_.begin (), value_.end (),
		    ascii_values.begin (), convert_to_ascii);

    if (ascii_values == value)
	return;

    value = ascii_values;
    changed ();
}
    
} // namespace Guikachu
