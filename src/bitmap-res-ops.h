//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_BITMAP_RES_OPS_H
#define GUIKACHU_BITMAP_RES_OPS_H

#include "undo.h"
#include "bitmap-res.h"

namespace Guikachu
{
    namespace ResourceOps
    {
	namespace BitmapOps
	{
            class ImageChangeOp: public UndoOp
            {
                ResourceManager *manager;
                serial_t         bitmap_serial;
                Glib::ustring    op_label;

                Resources::Bitmap::ImageData old_image, new_image;
                
            public:
		ImageChangeOp (Resources::Bitmap                  *bitmap,
                               const Resources::Bitmap::ImageData &old_image);
		virtual ~ImageChangeOp () {};
		
		void undo ();
		void redo ();
		Glib::ustring get_label () const { return op_label; };
                
                
            private:
                Resources::Bitmap * get_bitmap () const;
            };
	} // namespace BitmapOps
    } // namespace ResourceOps
} // namespace Guikachu

#endif /* !GUIKACHU_BITMAP_RES_H */
