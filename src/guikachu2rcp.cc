//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <glib.h>
#include <glib/gi18n.h>

#include <glibmm/init.h>
#include <gdkmm/wrap_init.h>
#include <memory>

#include "ui.h"

#include "io/xml-loader.h"
#include "io/rcp-saver.h"

using namespace Guikachu;

std::string snip_extension (const std::string &filename, const std::string &extension)
{
    std::string::const_reverse_iterator p = filename.rbegin ();
    std::string::const_reverse_iterator q = extension.rbegin ();
    while (p != filename.rend () && q != extension.rend () && *p == *q)
    {
	++p;
	++q;
    }
    if (q == extension.rend ())
	return std::string (filename, 0, filename.size () - extension.size ());
    else
	return filename;
}

void handle_file (const std::string &filename)
{
    const std::string out_filename = snip_extension (filename, ".guikachu") + ".rcp";

    std::cout << "Converting " << filename << " to " << out_filename
	      << std::endl;

    static IO::Loader *loader = new IO::XMLLoader;
    static IO::Saver  *saver  = new IO::RCPSaver;
	
    std::auto_ptr<ResourceManager> manager (new ResourceManager);

    try {
        loader->load (manager.get (), IO::create_canonical_uri (filename));
    } catch (Glib::Exception &e) {
	char *message = g_strdup_printf (_("Unable to open `%s': %s"),
					 filename.c_str (),
					 e.what ().c_str ());	
	std::cerr << message << std::endl;
	g_free (message);

	return;
    }

    try {
        saver->save (manager.get (), IO::create_canonical_uri (out_filename));
    } catch (Glib::Exception &e) {
	char *message = g_strdup_printf (_("Unable to create `%s': %s"),
					 filename.c_str (),
					 e.what ().c_str ());	
	std::cerr << message << std::endl;
	g_free (message);

	return;
    }
}

void usage (const std::string &self_name)
{
    char *error_msg = g_strdup_printf (_("Usage: %s file1 file2 ..."),
				       self_name.c_str ());
        
    std::cout << error_msg << std::endl;
    g_free (error_msg);
}

int main (int argc, char **argv)
{
    // Init subsystems
    UI::init_i18n ();    
    Glib::init ();
    IO::init ();
    Gdk::wrap_init ();
    
    if (argc < 2)
    {
	usage (argv[0]);
	return -1;
    }
    
    for (int i = 1; i < argc; i++)
    {
	handle_file (argv[i]);
    }

    return 0;
}
