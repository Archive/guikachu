//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "string-win.h"

#include <glib/gi18n.h>

#include "property-ops-resource.h"

#include "widgets/propertytable.h"
#include "widgets/entry.h"
#include "widgets/text.h"

using namespace Guikachu;

GUI::StringWindow::StringWindow (Guikachu::Resources::String *res_):
    res (res_)
{
    window.signal_delete_event ().connect (sigc::mem_fun (*this, &StringWindow::delete_event_impl));
    window.property_allow_grow () = true;
    
    GUI::PropertyTable *proptable = new GUI::PropertyTable ();
    Gtk::Widget *control;

    /* Resource ID */
    control = new GUI::PropertyEditors::Entry (
	false, res->id, new ResourceOps::RenameOpFactory (res));
    proptable->add (_("Resource _ID:"), *manage (control));

    /* String contents */
    control = new GUI::PropertyEditors::TextArea (
	res->text,
	new ResourceOps::PropChangeOpFactory<std::string> (
	    _("Change contents of %s"), res, res->text, true));
    proptable->add (_("_String:"), *manage (control));

    window.add (*manage (proptable));
    
    res->changed.connect (sigc::mem_fun (*this, &StringWindow::update));
    update ();
}

bool GUI::StringWindow::delete_event_impl (GdkEventAny *e)
{
    window.hide ();
    return true;
}

void GUI::StringWindow::show ()
{
    window.show_all ();
    window.raise ();
}

void GUI::StringWindow::update ()
{
    window.set_title (Glib::ScopedPtr<char> (
	g_strdup_printf (_("String: %s"), res->id ().c_str ())).get ());
}
