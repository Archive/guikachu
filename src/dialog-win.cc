//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "dialog-win.h"

#include <glib/gi18n.h>

#include <gtkmm/menu.h>
#include <gtkmm/notebook.h>
#include <sigc++/adaptors/bind_return.h>

#include "property-ops-resource.h"

#include "widgets/propertytable.h"
#include "widgets/resource-combo.h"
#include "widgets/entry.h"
#include "widgets/text.h"
#include "dialog-win-helpers.h"

using namespace Guikachu;

GUI::DialogWindow::DialogWindow (Resources::Dialog *res_):
    res (res_)
{
    using ResourceOps::PropChangeOpFactory;
    using ResourceOps::RenameOpFactory;
    using ResourceOps::StringListOpFactory;
    using namespace DialogWindow_Helpers;
    
    window.signal_delete_event ().connect (sigc::mem_fun (*this, &DialogWindow::delete_event_impl));
    window.property_allow_grow () = true;
    
    Gtk::Notebook *notebook = new Gtk::Notebook;
    
    GUI::PropertyTable *proptable = new GUI::PropertyTable;
    Gtk::Widget *control;

    /* Resource ID */
    control = new GUI::PropertyEditors::Entry (false, res->id, new RenameOpFactory (res));
    proptable->add (_("Resource _ID:"), *manage (control));

    /* Title */
    control = new GUI::PropertyEditors::Entry (
	true, res->title,
	new PropChangeOpFactory<std::string> (
	    _("Change title of %s"), res, res->title, true));
    proptable->add (_("_Title:"), *manage (control));
    
    /* Type */
    proptable->add (_("T_ype:"), *manage (new DialogTypeCombo (res)),
		    _("An icon will be included in the dialog, based on its type"));
    
    /* Help ID */
    control = new GUI::PropertyEditors::ResourceCombo (
	Resources::RESOURCE_STRING, res->help_id,
	new PropChangeOpFactory<std::string> (
	    _("Change help string of %s"), res, res->help_id));
    proptable->add (_("_Help ID:"), *manage (control));

    /* Text */
    control = new GUI::PropertyEditors::TextArea (
	res->text,
	new PropChangeOpFactory<std::string> (
	    _("Change text of %s"), res, res->text, true));
    proptable->add (_("_Text:"), *manage (control));
    proptable->show_all ();
    
    /* Page 1: General properties */
    notebook->pages ().push_back (
	Gtk::Notebook_Helpers::TabElem (*manage (proptable),
					_("General")));

    /* Page 2: Buttons */
    control = new ButtonList (res);
							    
    notebook->pages ().push_back (
	Gtk::Notebook_Helpers::TabElem (*manage (control), _("Buttons")));
					
    window.add (*manage (notebook));
    
    res->changed.connect (sigc::mem_fun (*this, &DialogWindow::update));
    update ();
}

void GUI::DialogWindow::show ()
{
    window.show_all ();
    window.raise ();
}

bool GUI::DialogWindow::delete_event_impl (GdkEventAny *e)
{
    window.hide ();
    return true;
}

void GUI::DialogWindow::update ()
{
    window.set_title (Glib::ScopedPtr<char> (
	g_strdup_printf (_("Dialog: %s"), res->id ().c_str ())).get ());
}
