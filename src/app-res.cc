//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "app-res.h"

#include <glib/gi18n.h>

#include "ui.h"

using namespace Guikachu;
using std::string;
using std::ostream;

Resources::Application::VendorID::VendorID (notify_signal_t &notify_signal,
					    const string    &value):
    Property<string> (notify_signal, value)
{
}

void Resources::Application::VendorID::set_val (const string& value_)
{
    if (value_.length () == 0 || value_.length () == 4)
	Property<string>::set_val (value_);
    else {
	UI::show_error (_("The vendor ID must be exactly "
			  "four characters long, or empty"));
    }
}

Resources::Application::Application (ResourceManager *manager_):
    manager (manager_),
    iconname (changed, ""),
    version (changed, ""),
    vendor (changed, "")
{
}

void Resources::Application::reset ()
{
    iconname = "";
    version = "";
    vendor = "";

    changed ();
}

void Resources::Application::set_tag (const tag_key_t &key,
                                      const tag_val_t &val)
{
    tags[key] = val;
}

Resources::Application::tag_val_t Resources::Application::get_tag (const tag_key_t &key) const
{
    tag_map_t::const_iterator i = tags.find (key);
    
    if (i == tags.end ())
        return "";
    else
        return i->second;
}

Resources::Application::tag_map_t Resources::Application::get_tags () const
{
    return tags;
}

void Resources::Application::add_tags (const tag_map_t &new_tags)
{
    for (tag_map_t::const_iterator i = new_tags.begin (); i != new_tags.end (); ++i)
        if (i->first != "" && i->second != "")
            tags[i->first] = i->second;
}
