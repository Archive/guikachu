//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <glib/gi18n.h>

#include "mainwin.h"
#include "ui-gui.h"

#include <gtkmm/statusbar.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/stock.h>

#include <gtkmm/action.h>
#include <gtkmm/toggleaction.h>
#include <gtkmm/radioaction.h>
#include <gtkmm/uimanager.h>

#include <list>

#include "resource-manager.h"
#include "resource-factory.h"
#include "resource-manager-ops.h"
#include "resource-util.h"

#include "add-resource.h"
#include "preferences-win.h"
#include "preferences.h"

using namespace Guikachu;

GUI::MainWin::MainWin ():
    manager (0),
    uri (""),
    in_exit (false),
    recent_files_num (0),
    main_group (Gtk::ActionGroup::create ()),
    recent_group (0),
    ui_manager (Gtk::UIManager::create ()),
    app_win (0)
{
    set_title ("Guikachu");
    set_role ("mainwin");
    create_menus ();

    // Update resourcetree mode
    set_view_mode (Preferences::MainWin::get_default_grouped ());
    
    // Scrollable viewport
    Gtk::ScrolledWindow *scrollwin = new Gtk::ScrolledWindow ();
    scrollwin->set_policy (Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
    scrollwin->add (resource_tree);
    scrollwin->show_all ();

    // Set up the menu bar, the toolbar, and the statusbar
    Gtk::Widget *menu_widget = ui_manager->get_widget ("/MainMenu");
    toolbar = ui_manager->get_widget ("/MainToolbar");
    //dynamic_cast<Gtk::Toolbar*>(toolbar)->set_toolbar_style (Gtk::TOOLBAR_BOTH_HORIZ);
    
    Gtk::VBox *box = new Gtk::VBox;        
    box->pack_start (*menu_widget, Gtk::PACK_SHRINK);
    box->pack_start (*toolbar, Gtk::PACK_SHRINK);
    box->pack_end (statusbar, Gtk::PACK_SHRINK);
    
    box->pack_start (*manage (scrollwin), Gtk::PACK_EXPAND_WIDGET);

    box->show_all ();
    add (*manage (box));
    
    set_size_request (300, 300);

    resource_tree.get_selection ()->signal_changed ().connect (
        sigc::mem_fun (*this, &MainWin::tree_selection_changed_cb));
    tree_selection_changed_cb ();

    resource_tree.app_activated.connect (sigc::mem_fun (*this, &MainWin::activate_app_cb));
    resource_tree.resource_activated.connect (sigc::mem_fun (*this, &MainWin::activate_res_cb));
    resource_tree.menu.connect (sigc::mem_fun (*this, &MainWin::tree_menu_cb));
}

GUI::MainWin::~MainWin ()
{
    delete app_win;
}

void GUI::MainWin::set_manager (ResourceManager *manager_)
{
    delete app_win;
    app_win = 0;
    
    manager = manager_;

    update_title ();

    resource_tree.set_manager (manager);

    // Is there a resourcetree mode stored in the file?
    std::string resourcetree_mode = manager->get_application ()->get_tag ("guikachu:resourcetree-mode");
    if (resourcetree_mode == "sort")
        set_view_mode (false);
    else if (resourcetree_mode == "group")
        set_view_mode (true);
    
    // Listen for changes in the undo stack
    manager->get_undo_manager ().changed.connect (sigc::mem_fun (*this, &MainWin::update_undo));
    update_undo ();

    // Listen for changes in dirty state
    manager->dirty_state_changed.connect (sigc::mem_fun (*this, &MainWin::update_title));
}

void GUI::MainWin::undo_cb ()
{
    manager->get_undo_manager ().undo ();
}

void GUI::MainWin::redo_cb ()
{
    manager->get_undo_manager ().redo ();
}

void GUI::MainWin::add_cb ()
{
    AddResourceWin add_win (manager);

    add_win.run (this);
}

void GUI::MainWin::remove_cb ()
{
    if (Resource *res = resource_tree.get_selected ())
    {
        manager->get_undo_manager ().push (new ResourceOps::RemoveOp (res));
        manager->remove_resource (res);        
    }
}

void GUI::MainWin::edit_res_cb ()
{
    if (resource_tree.get_app_selected ())
        activate_app_cb ();
    else if (Resource *res = resource_tree.get_selected ())
        activate_res_cb (res);
}

void GUI::MainWin::activate_res_cb (Resource *res)
{
    ResourceWindow *res_win = get_resource_editor (res);
    
    if (res_win)
        res_win->show ();
}

void GUI::MainWin::activate_app_cb ()
{
    if (!app_win)
        app_win = new AppWindow (manager->get_application ());

    app_win->show ();    
}

void GUI::MainWin::preferences_cb ()
{
    static PreferencesWin pref_win;

    pref_win.run ();
}

void GUI::MainWin::set_view_mode (bool grouped)
{
    if (manager)
        manager->get_application ()->set_tag ("guikachu:resourcetree-mode",
                                              grouped ? "group" : "sort");    

    const char *action_name = grouped ? "resources_tree" : "resources_flat";
    Glib::RefPtr<Gtk::Action> grouped_action = main_group->get_action (action_name);
    Glib::RefPtr<Gtk::RadioAction> grouped_radio_action = Glib::RefPtr<Gtk::RadioAction>::cast_dynamic (grouped_action);
    grouped_radio_action->set_active (true);
    
    resource_tree.set_grouped (grouped);
}

void GUI::MainWin::update_title ()
{
    Glib::ustring title_filename;
    if (uri != "")
        title_filename = UI::visible_filename (uri);
    else
        title_filename = _("<unnamed>");
    
    Glib::ScopedPtr<char> title_buf (g_strdup_printf (_("%s%s - Guikachu"),
                                                      manager->is_dirty () ? "*" : "",
                                                      title_filename.c_str ()));
    set_title (title_buf.get ());
}

void GUI::MainWin::toggle_toolbar_cb ()
{
    Glib::RefPtr<Gtk::Action> action = main_group->get_action ("toggle_toolbar");
    Glib::RefPtr<Gtk::ToggleAction> toggleaction = Glib::RefPtr<Gtk::ToggleAction>::cast_dynamic (action);
    
    set_show_toolbar (toggleaction->get_active ());
}

void GUI::MainWin::viewmode_cb ()
{
    Glib::RefPtr<Gtk::Action> grouped_action = main_group->get_action ("resources_tree");
    Glib::RefPtr<Gtk::RadioAction> grouped_radio_action = Glib::RefPtr<Gtk::RadioAction>::cast_dynamic (grouped_action);
    
    set_view_mode (grouped_radio_action->get_active ());
}

void GUI::MainWin::set_show_toolbar (bool show)
{
    Glib::RefPtr<Gtk::Action> action = main_group->get_action ("toggle_toolbar");
    Glib::RefPtr<Gtk::ToggleAction> toggleaction = Glib::RefPtr<Gtk::ToggleAction>::cast_dynamic (action);
    toggleaction->set_active (show);
    
    if (show)
        toolbar->show ();
    else
        toolbar->hide ();
}
    
void GUI::MainWin::tree_selection_changed_cb ()
{
    statusbar.pop (STATUSBAR_TREESELECTION);
    
    if (Resource *res = resource_tree.get_selected ())
    {
        main_group->get_action ("edit_resource")->set_sensitive (true);
        main_group->get_action ("cut")->set_sensitive (true);
        main_group->get_action ("copy")->set_sensitive (true);
        main_group->get_action ("duplicate")->set_sensitive (true);
        main_group->get_action ("remove")->set_sensitive (true);
        
        Glib::ustring s = res->id () + ": " + Resources::display_name_from_type (res->get_type ());
        statusbar.push (s, STATUSBAR_TREESELECTION);
    } else {
        main_group->get_action ("edit_resource")->set_sensitive (false);
        main_group->get_action ("cut")->set_sensitive (false);
        main_group->get_action ("copy")->set_sensitive (false);
        main_group->get_action ("duplicate")->set_sensitive (false);
        main_group->get_action ("remove")->set_sensitive (false);

        if (resource_tree.get_app_selected ())
            main_group->get_action ("edit_resource")->set_sensitive (true);
    }
}
