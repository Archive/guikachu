//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_WIDGETS_RESOURCE_COMBO_H
#define GUIKACHU_WIDGETS_RESOURCE_COMBO_H

namespace Guikachu
{
    namespace GUI
    {
	class ResourceCombo;
    }
}

#include <memory>
#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include "resource-combobox.h"

#include "resource.h"
#include "resource-ref.h"
#include "property-editor.h"
#include "property-ops.h"

#include <set>

namespace Guikachu
{
    namespace GUI
    {
	namespace PropertyEditors
	{
	    class ResourceCombo: public Gtk::HBox,
                                 public PropertyEditor
	    {
		typedef Properties::ResourceRef          property_t;
		typedef PropChangeOpFactory<std::string> op_factory_t;

                ResourceComboBox *combo;
		Gtk::Button       button_edit;
		
		property_t                  &property;
		std::auto_ptr<op_factory_t>  op_factory;
		
	    public:
		ResourceCombo (Resources::Type   type,
			       property_t       &property,
			       op_factory_t     *op_factory);

		ResourceCombo (const std::set<Resources::Type> &types,
			       property_t                      &property,
			       op_factory_t                    *op_factory);

	    private:
                void create_widgets (const std::set<Resources::Type> &types);
                
		bool update_block;
		void update ();
		
		void edit_cb ();
		void combo_cb ();
	    };
	}
    }
}

#endif /* !GUIKACHU_WIDGETS_RESOURCE_COMBO_H */
