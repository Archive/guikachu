//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_WIDGETS_FONT_COMBO_H
#define GUIKACHU_WIDGETS_FONT_COMBO_H

#include "widgets/property-editor.h"

#include "property.h"
#include "property-ops.h"

#include <gtkmm/combobox.h>
#include <gtkmm/liststore.h>
#include <memory>

namespace Guikachu
{
    namespace GUI
    {
	namespace PropertyEditors
	{
	    class FontCombo: public Gtk::ComboBox,
                             public PropertyEditor
	    {
		typedef Property<int>            property_t;
		typedef PropChangeOpFactory<int> op_factory_t;
		
		property_t                  &property;
		std::auto_ptr<op_factory_t>  op_factory;

                Gtk::TreeModelColumn<int>           col_num;
                Gtk::TreeModelColumn<Glib::ustring> col_label;

                Glib::RefPtr<Gtk::ListStore> store;
                
	    public:
		FontCombo (property_t   &property,
			   op_factory_t *op_factory);

	    private:
                void add_font (const Glib::ustring &label, int font_num);

                void cell_cb (const Gtk::TreeModel::iterator &iter, Gtk::CellRendererPixbuf *cell);
                
                bool update_block;
		void update ();

		void changed_cb ();
	    };
	}
    }
}
    
#endif /* !GUIKACHU_WIDGETS_FONT_COMBO_H */
