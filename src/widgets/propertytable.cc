//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "propertytable.h"

#include <gtkmm/separator.h>
#include <gtkmm/label.h>
#include <gtkmm/eventbox.h>
#include <gtkmm/tooltips.h>
#include <gtkmm/alignment.h>
#include <gtkmm/window.h>

#include "property-editor.h"

using namespace Guikachu::GUI;

PropertyTable::PropertyTable ():
    accel_group (Gtk::AccelGroup::create ()),
//    accel_group (0),
    accel_parent (0),
    curr_row (0)
{
    set_border_width (5);
    set_row_spacings (5);
    set_col_spacings (5);

    //signal_parent_changed ().connect (sigc::mem_fun (*this, &PropertyTable::parent_changed_cb));
    signal_map ().connect (sigc::mem_fun (*this, &PropertyTable::map_cb));
}

Gtk::Widget *PropertyTable::create_label (const Glib::ustring &label,
					  Gtk::Widget         &control,
					  const Glib::ustring &tooltip)
{
    Gtk::Widget  *ret_val;
    Gtk::Label   *label_widget;
    guint         accel_key;

    PropertyEditor *prop_editor = dynamic_cast<PropertyEditor*>(&control);

    label_widget = new Gtk::Label (label, Gtk::ALIGN_RIGHT, Gtk::ALIGN_CENTER, true);
    accel_key = label_widget->get_mnemonic_keyval ();

    if (prop_editor)
    {
	double x_align = 1;
	double y_align = 0.5;
	
	switch (prop_editor->get_label_alignment ())
	{
	case LABEL_ALIGN_TOP:
	    y_align = 0;
	    break;
	case LABEL_ALIGN_CENTER:
	    y_align = 0.5;
	    break;
	case LABEL_ALIGN_BOTTOM:
	    y_align = 1;
	    break;
	}
	label_widget->set_alignment (x_align, y_align);
    }

    if (accel_group)
    {
	if (prop_editor)
	    prop_editor->install_underline (accel_group, accel_key);
	else
	    control.add_accelerator ("grab_focus",
				     accel_group, accel_key,
				     Gdk::MOD1_MASK, Gtk::ACCEL_VISIBLE);
    }
    
    if (tooltip != "")
    {
	Gtk::EventBox *e_box = new Gtk::EventBox;
	e_box->add (*manage (label_widget));
	
	Gtk::Tooltips *tips = new Gtk::Tooltips;
	tips->set_tip (*e_box, tooltip);
	manage (tips);

	ret_val = e_box;
    } else {
	ret_val = label_widget;
    }
    
    ret_val->show_all ();

    return ret_val;
}

    
void PropertyTable::add (const Glib::ustring &label,
			 Gtk::Widget         &control,
			 const Glib::ustring &tooltip)
{
    Gtk::Widget *label_widget = create_label (label, control, tooltip);
    control.set_data ("label_widget", label_widget);

    label_widget->show ();
    control.show ();

    attach (*manage (label_widget), 0, 1, curr_row, curr_row + 1,
            Gtk::FILL, Gtk::FILL);

    bool expand = false;
    PropertyEditor *prop_editor = dynamic_cast<PropertyEditor*> (&control);
    if (prop_editor)
        expand = prop_editor->get_resizeable ();
    
    attach (control, 1, 2, curr_row, curr_row + 1,
            Gtk::FILL | Gtk::EXPAND,
            expand ? Gtk::FILL | Gtk::EXPAND : Gtk::FILL);

    curr_row++;
}

void PropertyTable::add (Gtk::Widget &control)
{
    bool expand = false;
    PropertyEditor *prop_editor = dynamic_cast<PropertyEditor*> (&control);
    if (prop_editor)
        expand = prop_editor->get_resizeable ();
    
    attach (control, 0, 2,
            curr_row, curr_row + 1,
            Gtk::FILL | Gtk::EXPAND,
            expand ? Gtk::FILL | Gtk::EXPAND : Gtk::FILL);

    curr_row++;
}

void PropertyTable::add_separator ()
{
    Gtk::HSeparator *separator = new Gtk::HSeparator ();
    separator->show ();
    attach (*manage (separator), 0, 2, curr_row, curr_row + 1, Gtk::FILL, Gtk::FILL);

    curr_row++;
}

void PropertyTable::set_child_sensitive (Gtk::Widget &child, bool sensitive)
{
    Gtk::Widget *label = dynamic_cast<Gtk::Widget*>((Gtk::Widget*)child.get_data ("label_widget"));
    g_return_if_fail (label != 0);

    child.set_sensitive (sensitive);
    label->set_sensitive (sensitive);
}

void PropertyTable::set_child_shown (Gtk::Widget &child, bool shown)
{
    Gtk::Widget *label = dynamic_cast<Gtk::Widget*>((Gtk::Widget*)child.get_data ("label_widget"));
    g_return_if_fail (label != 0);

    if (shown)
    {
	child.show ();
	label->show ();
    } else {
	child.hide ();
	label->hide ();
    }
}


void PropertyTable::on_parent_changed (Gtk::Widget *old_parent)
{
#if 0
    if (old_parent)
    {
	Gtk::Widget *old_toplevel = old_parent->get_toplevel ();
	Gtk::Window *old_window = dynamic_cast<Gtk::Window*> (old_toplevel);
	
	if (old_window && old_window == accel_parent)
	{
	    old_window->remove_accel_group (accel_group);
	    accel_parent = 0;
	}
    }
#else
    map_cb ();
#endif
}

void PropertyTable::map_cb ()
{
    Gtk::Widget *new_parent = get_toplevel ();

    if (new_parent)
    {
	Gtk::Window *new_parent_win = dynamic_cast<Gtk::Window*> (new_parent);
	if (new_parent_win && new_parent_win != accel_parent)
	{
	    accel_parent = new_parent_win;
	    accel_parent->add_accel_group (accel_group);
	}
    }
}
