//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_WIDGETS_PROPERTY_TABLE_H
#define GUIKACHU_WIDGETS_PROPERTY_TABLE_H

#include <gtkmm/table.h>
#include <gtkmm/accelgroup.h>

namespace Guikachu
{
    namespace GUI
    {
	class PropertyTable: public Gtk::Table
	{
	    Glib::RefPtr<Gtk::AccelGroup>  accel_group;
	    Gtk::Window                   *accel_parent;
	    int                            curr_row;

	public:
	    PropertyTable ();
	    
	    void add_separator ();

	    void add (const Glib::ustring &label,
		      Gtk::Widget         &control,
		      const Glib::ustring &tooltip = "");
            
            void add (Gtk::Widget       &control);

	    void set_child_sensitive (Gtk::Widget &child, bool sensitive);
	    void set_child_shown     (Gtk::Widget &child, bool shown);
	    
	protected:
	    Gtk::Widget *create_label (const Glib::ustring &label,
				       Gtk::Widget         &control,
				       const Glib::ustring &tooltip);

	    void on_parent_changed (Gtk::Widget *old_parent);
	    void map_cb ();
	};
    }
}

#endif /* !GUIKACHU_WIDGETS_PROPERTY_TABLE_H */
