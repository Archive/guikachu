//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_WIDGETS_NUM_ENTRY_H
#define GUIKACHU_WIDGETS_NUM_ENTRY_H

#include <memory>
#include <gtkmm/spinbutton.h>
#include "property-editor.h"

#include "property.h"
#include "property-ops.h"


namespace Guikachu
{
    namespace GUI
    {
	namespace PropertyEditors
	{
	    class NumEntry: public Gtk::SpinButton,
                            public PropertyEditor
	    {
	    protected:
		typedef Property<int>            property_t;
		typedef PropChangeOpFactory<int> op_factory_t;

	    private:
		property_t                  &property;
		std::auto_ptr<op_factory_t>  op_factory;

	    public:
		NumEntry (int min, int max,
			  property_t   &property,
			  op_factory_t *op_factory);
		void set_min (int min);
		void set_max (int max);

	    private:
		bool update_block;
		void update ();
		
		void changed_cb ();
	    };
	}
    }
}

#endif /* !GUIKACHU_WIDGETS_NUM_ENTRY_H */
