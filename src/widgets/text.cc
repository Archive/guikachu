//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "text.h"

using namespace Guikachu::GUI::PropertyEditors;

TextArea::TextArea (property_t   &property_,
		    op_factory_t *op_factory_):
    PropertyEditor (this),
    property (property_),
    op_factory (op_factory_),
    update_block (false),
    cursor_pos (-1)
{
    set_shadow_type (Gtk::SHADOW_IN);
    
    text_widget.set_wrap_mode (Gtk::WRAP_WORD);
    text_widget.set_editable (true);
    
    text_widget.get_buffer ()->signal_changed ().connect (sigc::mem_fun (*this, &TextArea::changed_cb));

    set_policy (Gtk::POLICY_NEVER, Gtk::POLICY_ALWAYS);
    add (text_widget);
    
    property.changed.connect (sigc::mem_fun (*this, &TextArea::update));
    update ();
}

void TextArea::changed_cb ()
{
    if (update_block)
        return;

    const Glib::RefPtr<Gtk::TextBuffer> buffer = text_widget.get_buffer ();
    cursor_pos = buffer->get_iter_at_mark (buffer->get_insert ()).get_offset ();
    
    Glib::ustring text_utf = buffer->get_text (buffer->begin (), buffer->end ());
    std::string text = convert_to_ascii (text_utf);

    if (property != text)
	op_factory->push_change (text);
}

void TextArea::update ()
{
    update_block = true;

    Glib::RefPtr<Gtk::TextBuffer> buffer = text_widget.get_buffer ();
    buffer->set_text (property.get_val ());

    if (cursor_pos >= 0)
	buffer->place_cursor (buffer->get_iter_at_offset (cursor_pos));
    if (text_widget.is_mapped ())
        text_widget.place_cursor_onscreen ();
    
    update_block = false;
}
