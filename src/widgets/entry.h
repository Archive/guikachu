//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_WIDGETS_ENTRY_H
#define GUIKACHU_WIDGETS_ENTRY_H

#include <memory>
#include <gtkmm/entry.h>
#include "property.h"
#include "property-ops.h"
#include "property-editor.h"

namespace Guikachu
{
    namespace GUI
    {
	namespace PropertyEditors
	{
	    class Entry: public Gtk::Entry,
                         public PropertyEditor
	    {
		typedef Property<std::string>            property_t;
		typedef PropChangeOpFactory<std::string> op_factory_t;
		
		property_t                  &property;
		std::auto_ptr<op_factory_t>  op_factory;
		
	    public:
		Entry (bool          live_update,
		       property_t   &property,
		       op_factory_t *op_factory);
	    private:
		bool update_block;
		int  cursor_pos;
		void update ();
		
		void changed_cb ();
                void insert_cb (const Glib::ustring &text, int *position);
                void delete_cb (int start_pos, int end_pos);
	    };
	}
    }
}

#endif /* !GUIKACHU_WIDGETS_ENTRY_H */
