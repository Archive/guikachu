//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "property-editor.h"

using namespace Guikachu::GUI;

PropertyEditor::PropertyEditor ():
    underline_widget (0)
{
}

PropertyEditor::PropertyEditor (Gtk::Widget *underline_widget_):
    underline_widget (underline_widget_)
{
}

void PropertyEditor::install_underline (Glib::RefPtr<Gtk::AccelGroup> &accel_group,
					unsigned int                   accel_key)
{
    if (underline_widget)
	underline_widget->add_accelerator ("grab_focus",
					   accel_group, accel_key,
					   Gdk::MOD1_MASK, Gtk::ACCEL_VISIBLE);
}
