//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "font-combo.h"

#include <glib/gi18n.h>

#if 0
#include "form-editor/form-editor.h"
#include "form-editor/form-editor-canvas.h"
#endif

#include <sigc++/adaptors/bind.h>

using namespace Guikachu::GUI::PropertyEditors;

FontCombo::FontCombo (property_t   &property_,
		      op_factory_t *op_factory_):
    PropertyEditor (this),
    property (property_),
    op_factory (op_factory_),
    update_block (false)
{
    Gtk::TreeModel::ColumnRecord cols;
    cols.add (col_num);
    cols.add (col_label);
    
    store = Gtk::ListStore::create (cols);

    // Fill the model
    add_font (_("Regular"),    0);
    add_font (_("Bold"),       1);
    add_font (_("Large"),      2);
    add_font (_("Symbol"),     3);
    add_font (_("Symbol 11"),  4);
    add_font (_("Symbol 7"),   5);
    add_font (_("LED"),        6);
    add_font (_("Large Bold"), 7);

    // Set the view
    set_model (store);

    pack_start (col_label, true);
#if 0
    Gtk::CellRendererPixbuf *cell = new Gtk::CellRendererPixbuf;
    pack_start (*manage (cell), false);
    set_cell_data_func (*cell, sigc::bind (sigc::mem_fun (*this, &FontCombo::cell_cb), cell));
#endif
    
    signal_changed ().connect (sigc::mem_fun (*this, &FontCombo::changed_cb));
    property.changed.connect (sigc::mem_fun (*this, &FontCombo::update));
    update ();
}

void FontCombo::add_font (const Glib::ustring &label, int font_num)
{
    Gtk::TreeRow row = *(store->append ());
    row[col_num] = font_num;
    row[col_label] = label;
}

void FontCombo::cell_cb (const Gtk::TreeModel::iterator &iter, Gtk::CellRendererPixbuf *cell)
{
#if 0
    int font_num = (*iter)[col_num];
    
    const FormEditor::Font    &font = FormEditor::get_font (font_num);
    const Gdk::Color          &color = get_style ()->get_fg (Gtk::STATE_NORMAL);
    Glib::RefPtr<Gdk::Pixbuf>  pixbuf = FormEditor::render_text (font, "Aa", color);

    cell->property_pixbuf () = pixbuf;
#endif
}

void FontCombo::update ()
{
    int new_font_num = property;

    g_return_if_fail (new_font_num < 8);
    
    Gtk::TreeModel::iterator iter = store->get_iter ("0");
    while ((*iter)[col_num] != new_font_num)
        ++iter;
    
    update_block = true;
    set_active (iter);
    update_block = false;
}

void FontCombo::changed_cb ()
{
    if (update_block)
	return;

    Gtk::TreeModel::iterator iter = get_active ();
    g_return_if_fail (iter);

    op_factory->push_change ((*iter)[col_num]);
}
