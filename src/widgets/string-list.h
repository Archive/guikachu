//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_WIDGETS_STRING_LIST_H
#define GUIKACHU_WIDGETS_STRING_LIST_H

#include <gtkmm/box.h>
#include <gtkmm/entry.h>
#include <gtkmm/treeview.h>
#include <gtkmm/liststore.h>
#include <gtkmm/button.h>
#include <gtkmm/buttonbox.h>
#include <memory>

#include "property.h"
#include "property-editor.h"
#include "string-list-ops.h"

#include <vector>

namespace Guikachu
{
    namespace GUI
    {
	namespace PropertyEditors
	{
	    class StringList: public Gtk::HBox,
                              public PropertyEditor
	    {
	    protected:
		typedef std::vector<std::string> value_t;
		typedef Property<value_t>        property_t;
		typedef StringListOps::OpFactory op_factory_t;
		
		property_t                  &property;
		std::auto_ptr<op_factory_t>  op_factory;
		
		Gtk::TreeView                       treeview;
		Glib::RefPtr<Gtk::ListStore>        treestore;
		Gtk::TreeModelColumn<Glib::ustring> col_text;
                int                                 col_text_num;
		
	    private:
		Gtk::Button     btnAdd, btnUp, btnDown, btnRemove;
		Gtk::VButtonBox buttons;
		
	    public:
		StringList (property_t   &property,
			    op_factory_t *op_factory);
		
                LabelAlignment   get_label_alignment () { return LABEL_ALIGN_TOP; };
                bool             get_resizeable      () { return true; };
		
	    protected:
		unsigned int get_selected_row ();
		unsigned int get_row (const Gtk::TreeModel::Path &path);
		
		bool update_block;
		
	    protected:
		void update ();
		void selection_cb ();

		// You can create a customized/specialized list editor
		// by overriding these
		virtual void create_columns (Gtk::TreeModel::ColumnRecord &columns);
		virtual void create_view_columns (int &col_text_num);
		virtual void update_row (Gtk::TreeRow &row, value_t::size_type i);
                virtual std::string new_item ();

		void add_button (Gtk::Widget &button);

		// You can also connect to these
		sigc::signal1<void, unsigned int> item_added;
                sigc::signal1<void, unsigned int> item_removed;
                sigc::signal1<void, unsigned int> up;   /* Argument is the _old_ index */
                sigc::signal1<void, unsigned int> down; /* Argument is the _old_ index */
		
	    private:
		void setup ();
		
		void button_add_cb ();
		void button_up_cb ();
		void button_down_cb ();
		void button_remove_cb ();

		void store_changed_cb (const Gtk::TreeModel::Path     &path,
				       const Gtk::TreeModel::iterator &iter);
	    };
	}
    }
}

#endif /* !GUIKACHU_WIDGETS_STRING_LIST_H */
