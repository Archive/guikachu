//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "string-list-ops-priv-widget.h"

#include <glib.h> // For g_assert

using namespace Guikachu;
using namespace Guikachu::WidgetOps::StringListOps;

OpBase::OpBase (const Glib::ustring &op_label,
		Widget              *widget,
		property_t          &prop) :
    Guikachu::StringListOps::OpBase (op_label),
    manager (widget->get_manager ()),
    prop_holder (*widget, prop),
    form_id (widget->get_form ()->id),
    widget_id (widget->id)
{
}

Widget * OpBase::get_widget () const
{
    Resource *res = manager->get_resource (form_id);
    Resources::Form *form = static_cast<Resources::Form*> (res);
    g_assert (form);

    Widget *widget = form->get_widget (widget_id);
    g_assert (widget);

    return widget;
}

OpBase::property_t & OpBase::get_prop () const
{
    return prop_holder.get_member (*get_widget ());
}
    

RemoveOp::RemoveOp (const Glib::ustring &op_label,
		    Widget              *widget,
		    property_t          &prop,
		    index_t              old_pos) :
    Guikachu::StringListOps::OpBase (op_label),
    OpBase (op_label, widget, prop),
    Guikachu::StringListOps::RemoveOp (op_label, old_pos, prop()[old_pos])
{
}

AddOp::AddOp (const Glib::ustring &op_label,
	      Widget              *widget,
	      property_t          &prop,
	      index_t              index,
	      const std::string   &item) :
    Guikachu::StringListOps::OpBase (op_label),
    OpBase (op_label, widget, prop),
    Guikachu::StringListOps::AddOp (op_label, index, item)
{
}

ChangeOp::ChangeOp (const Glib::ustring &op_label,
		    Widget              *widget,
		    property_t          &prop,
		    index_t              index,
		    const std::string   &item) :
    Guikachu::StringListOps::OpBase (op_label),
    OpBase (op_label, widget, prop),
    Guikachu::StringListOps::ChangeOp (op_label, index, prop()[index], item)
{
}

MoveOp::MoveOp (const Glib::ustring &op_label,
		Widget              *widget,
		property_t          &prop,
		index_t              old_index,
		index_t              new_index) :
    Guikachu::StringListOps::OpBase (op_label),
    OpBase (op_label, widget, prop),
    Guikachu::StringListOps::MoveOp (op_label, old_index, new_index)
{
}

MoveOp::MoveOp (const Glib::ustring  &op_label,
		Widget               *widget,
		property_t           &prop,
		const index_list_t   &index_history_head,
		const index_list_t   &index_history_tail) :
    Guikachu::StringListOps::OpBase (op_label),
    OpBase (op_label, widget, prop),
    Guikachu::StringListOps::MoveOp (op_label, index_history_head, index_history_tail)
{
}

UndoOp * MoveOp::combine (UndoOp *other_op) const
{
    MoveOp *op = dynamic_cast<MoveOp*> (other_op);
    if (!op)
	return 0;

    if (op->widget_id != widget_id)
	return 0;

    if (op->index_history.front () != index_history.back ())
	return 0;
    
    Widget     *widget = get_widget ();
    property_t &prop = get_prop ();
    
    UndoOp *new_op = new MoveOp (op_label, widget, prop, index_history, op->index_history);
    
    return new_op;
}
