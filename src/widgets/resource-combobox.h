//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_WIDGETS_RESOURCE_COMBOBOX_H
#define GUIKACHU_WIDGETS_RESOURCE_COMBOBOX_H

#include <gtkmm/combobox.h>
#include <gtkmm/liststore.h>

#include "resource.h"
#include "element-combobox-base.h"

namespace Guikachu
{
    namespace GUI
    {
        class ResourceComboBox: public ElementComboBoxBase<Resource>
        {
        public:
            ResourceComboBox (const std::set<Resources::Type> &types,
                              ResourceManager                 *manager);
        };
    }
}

#endif /* !GUIKACHU_WIDGETS_RESOURCE_COMBOBOX_H */
