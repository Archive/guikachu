//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "string-list-ops-priv-resource.h"

#include <glib.h> // For g_assert

using namespace Guikachu::ResourceOps::StringListOps;
using namespace Guikachu;

OpBase::OpBase (const Glib::ustring &op_label,
		Resource            *resource,
		property_t          &prop) :
    Guikachu::StringListOps::OpBase (op_label),
    manager (resource->get_manager ()),
    prop_holder (*resource, prop),
    resource_id (resource->id)
{
}

Resource * OpBase::get_resource () const
{
    Resource *res = manager->get_resource (resource_id);
    g_assert (res);

    return res;
}

OpBase::property_t & OpBase::get_prop () const
{
    return prop_holder.get_member (*get_resource ());
}

AddOp::AddOp (const Glib::ustring &op_label,
	      Resource            *resource,
	      property_t          &prop,
	      index_t              index,
	      const std::string   &item) :
    Guikachu::StringListOps::OpBase (op_label),
    OpBase (op_label, resource, prop),
    Guikachu::StringListOps::AddOp (op_label, index, item)
{
}

RemoveOp::RemoveOp (const Glib::ustring &op_label,
		    Resource            *resource,
		    property_t          &prop,
		    index_t              old_pos) :
    Guikachu::StringListOps::OpBase (op_label),
    OpBase (op_label, resource, prop),
    Guikachu::StringListOps::RemoveOp (op_label, old_pos, prop()[old_pos])
{
}

ChangeOp::ChangeOp (const Glib::ustring &op_label,
		    Resource            *resource,
		    property_t          &prop,
		    index_t              index,
		    const std::string   &item) :
    Guikachu::StringListOps::OpBase (op_label),
    OpBase (op_label, resource, prop),
    Guikachu::StringListOps::ChangeOp (op_label, index, prop()[index], item)
{
}

MoveOp::MoveOp (const Glib::ustring &op_label,
		Resource            *resource,
		property_t          &prop,
		index_t              old_index,
		index_t              new_index) :
    Guikachu::StringListOps::OpBase (op_label),
    OpBase (op_label, resource, prop),
    Guikachu::StringListOps::MoveOp (op_label, old_index, new_index)
{
}

MoveOp::MoveOp (const Glib::ustring &op_label,
		Resource            *resource,
		property_t          &prop,
		const index_list_t  &index_history_head,
		const index_list_t  &index_history_tail) :
    Guikachu::StringListOps::OpBase (op_label),
    OpBase (op_label, resource, prop),
    Guikachu::StringListOps::MoveOp (op_label, index_history_head, index_history_tail)
{
}

UndoOp * MoveOp::combine (UndoOp *other_op) const
{
    MoveOp *op = dynamic_cast<MoveOp*> (other_op);
    if (!op)
	return 0;

    if (op->resource_id != resource_id)
	return 0;

    if (op->index_history.front () != index_history.back ())
	return 0;
    
    Resource   *res = get_resource ();
    property_t &prop = get_prop ();
    
    UndoOp *new_op = new MoveOp (op_label, res, prop, index_history, op->index_history);
    
    return new_op;
}
