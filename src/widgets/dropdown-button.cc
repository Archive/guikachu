//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "dropdown-button.h"

#include <gtkmm/box.h>
#include <gtkmm/arrow.h>
#include <gtkmm/separator.h>

#include "ui-gui.h"

using namespace Guikachu::GUI;

DropdownButton::DropdownButton ():
    button (new Gtk::Button)
{
    setup ();
}

DropdownButton::DropdownButton (const Glib::ustring &label):
    button (new Gtk::Button (label))
{
    setup ();
}

DropdownButton::DropdownButton (const Gtk::StockID &stock_id, const Glib::ustring &label):
    button (UI::create_stock_button (stock_id, label))
{
    setup ();
}

void DropdownButton::setup ()
{
    Gtk::Button *arrow_button = new Gtk::Button;
    arrow_button->add (*manage (new Gtk::Arrow (Gtk::ARROW_DOWN, Gtk::SHADOW_NONE)));
    
    button->set_relief (Gtk::RELIEF_NONE);
    arrow_button->set_relief (Gtk::RELIEF_NONE);
    
    Gtk::HBox *box = new Gtk::HBox;
    box->pack_start (*manage (button));
    box->pack_end (*manage (arrow_button), Gtk::PACK_SHRINK);
    box->pack_end (*manage (new Gtk::VSeparator), Gtk::PACK_SHRINK);
    box->show_all ();

    add (*manage (box));
    
    set_shadow_type (Gtk::SHADOW_OUT);
    
    arrow_button->signal_button_press_event ().connect (
        sigc::bind_return (sigc::mem_fun (*this, &DropdownButton::button_event_cb), true), false);
    
}

Glib::SignalProxy0<void> DropdownButton::signal_clicked ()
{
    return button->signal_clicked ();
}

void DropdownButton::append (Gtk::MenuItem &item)
{
    menu.append (item);
}

void DropdownButton::append (const Glib::ustring &label, const sigc::slot0<void> &slot)
{
    Gtk::MenuItem *item = new Gtk::MenuItem (label, true);
    item->signal_activate ().connect (slot);
    item->show ();
    append (*manage (item));
}

void DropdownButton::menu_position_cb (int &x, int &y, bool &push_in)
{
    // Ported from GOffice's GOComboBox
    
    Glib::RefPtr<Gdk::Screen> screen = get_screen ();
    Glib::RefPtr<Gdk::Window> window = get_window ();
    
    window->get_origin (x, y);
    x += get_allocation ().get_x ();
    y += get_allocation ().get_y () + get_allocation ().get_height ();
    
    int popup_height = menu.get_allocation ().get_height ();
    int popup_width = menu.get_allocation ().get_width ();
    
    if (popup_width < get_allocation ().get_width ())
    {
        menu.set_size_request (get_allocation ().get_width (), -1);
        popup_width = get_allocation ().get_width ();
    }
    
    x = std::min (x, screen->get_width () - popup_width);
    y = std::min (y, screen->get_height () - popup_height);
}

void DropdownButton::button_event_cb (GdkEventButton *e)
{
    if (e->button != 1)
        return;

    //btnItemDropdown->set_state (Gtk::STATE_SELECTED);    
    
    menu.popup (sigc::mem_fun (*this, &DropdownButton::menu_position_cb), e->button, e->time);
}

