//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_WIDGETS_WIDGET_COMBO_H
#define GUIKACHU_WIDGETS_WIDGET_COMBO_H

namespace Guikachu
{
    namespace GUI
    {
	class WidgetCombo;
    }
}

#include <memory>
#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include "widget-combobox.h"

#include "property-editor.h"

#include "form-editor/widget.h"
#include "form-editor/widget-ref.h"
#include "property-ops.h"

#include <set>

namespace Guikachu
{
    namespace GUI
    {
	namespace PropertyEditors
	{
	    class WidgetCombo: public Gtk::HBox,
                               public PropertyEditor
	    {
		typedef Properties::WidgetRef            property_t;
		typedef PropChangeOpFactory<std::string> op_factory_t;

		WidgetComboBox *combo;
		Gtk::Button     button_edit;
		
		std::set<Widgets::Type> types;
		
		property_t                  &property;
		std::auto_ptr<op_factory_t>  op_factory;

	    public:
		WidgetCombo (Widgets::Type    type,
			     property_t      &property,
			     op_factory_t    *op_factory);

		WidgetCombo (const std::set<Widgets::Type> &type,
			     property_t                    &property,
			     op_factory_t                  *op_factory);
                
		void install_underline (Gtk::AccelGroup &accel_group,
					guint            accel_key);

	    private:
                void create_widgets (const std::set<Widgets::Type> &types);
                
		bool update_block;
		void update ();

		void edit_cb ();
		void combo_cb ();
	    };
	}
    }
}
    
#endif /* !GUIKACHU_WIDGET_COMBO_H */
