//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_WIDGETS_STRING_LIST_OPS_H
#define GUIKACHU_WIDGETS_STRING_LIST_OPS_H

#include "property.h"
#include "undo.h"

#include "resource.h"
#include "form-editor/widget.h"

#include <string>
#include <vector>

namespace Guikachu
{
    namespace StringListOps
    {
	class OpFactory
	{
	public:
	    typedef std::vector<std::string> value_t;
	    typedef Property<value_t>        property_t;
	    
	    typedef unsigned int index_t;
	    typedef std::string  item_t;
	    
	    virtual ~OpFactory () {};
	    
	    virtual void push_add    (index_t index, const item_t &item);
	    virtual void push_remove (index_t index);
	    virtual void push_change (index_t index, const item_t &new_item);
	    virtual void push_move   (index_t old_index, index_t new_index);

	protected:
	    Glib::ustring  add_template, remove_template, change_template, move_template;
	    property_t    &prop;
	    UndoManager   &undo_manager;

	    OpFactory (const Glib::ustring &add_template,
		       const Glib::ustring &remove_template,
		       const Glib::ustring &change_template,
		       const Glib::ustring &move_template,
		       property_t          &prop,
		       UndoManager         &undo_manager);

	    virtual std::string get_parent_label () = 0;
	    
	    virtual UndoOp * create_add_op    (const Glib::ustring &op_label, index_t index, const item_t &item) = 0;
	    virtual UndoOp * create_remove_op (const Glib::ustring &op_label, index_t index) = 0;
	    virtual UndoOp * create_change_op (const Glib::ustring &op_label, index_t index, const item_t &new_item) = 0;
	    virtual UndoOp * create_move_op   (const Glib::ustring &op_label, index_t old_index, index_t new_index) = 0;
	};
    }

    namespace ResourceOps
    {
	class StringListOpFactory: public Guikachu::StringListOps::OpFactory
	{
	    Resource *resource;

	public:
	    StringListOpFactory (const Glib::ustring &add_template,
				 const Glib::ustring &remove_template,
				 const Glib::ustring &change_template,
				 const Glib::ustring &move_template,
				 Resource            *resource,
				 property_t          &prop);
	    virtual ~StringListOpFactory () {};

	protected:
	    std::string get_parent_label () { return resource->id; };
	    
	    UndoOp * create_add_op    (const Glib::ustring &op_label, index_t index, const item_t &item);
	    UndoOp * create_remove_op (const Glib::ustring &op_label, index_t index);
	    UndoOp * create_change_op (const Glib::ustring &op_label, index_t index, const item_t &new_item);
	    UndoOp * create_move_op   (const Glib::ustring &op_label, index_t old_index, index_t new_index);
	};
    }
    
    namespace WidgetOps
    {
	class StringListOpFactory: public Guikachu::StringListOps::OpFactory
	{
	    Widget *widget;

	public:
	    StringListOpFactory (const Glib::ustring &add_template,
				 const Glib::ustring &remove_template,
				 const Glib::ustring &change_template,
				 const Glib::ustring &move_template,
				 Widget              *widget,
				 property_t          &prop);
	    virtual ~StringListOpFactory () {};

	protected:
	    std::string get_parent_label () { return widget->id; };
	    
	    UndoOp * create_add_op    (const Glib::ustring &op_label, index_t index, const item_t &item);
	    UndoOp * create_remove_op (const Glib::ustring &op_label, index_t index);
	    UndoOp * create_change_op (const Glib::ustring &op_label, index_t index, const item_t &new_item);
	    UndoOp * create_move_op   (const Glib::ustring &op_label, index_t old_index, index_t new_index);
	};
    }
}

#endif /* !GUIKACHU_WIDGETS_STRING_LIST_OPS_H */
