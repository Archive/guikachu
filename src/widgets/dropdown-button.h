//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_WIDGETS_DROPDOWN_BUTTON_H
#define GUIKACHU_WIDGETS_DROPDOWN_BUTTON_H

#include <gtkmm/frame.h>
#include <gtkmm/menu.h>
#include <gtkmm/stockid.h>

namespace Guikachu
{
    namespace GUI
    {
        class DropdownButton: public Gtk::Frame
        {
            Gtk::Button *button;
            Gtk::Menu    menu;
            
        public:
            DropdownButton ();
            explicit DropdownButton (const Glib::ustring &label);
            DropdownButton (const Gtk::StockID &stock_id, const Glib::ustring &label);
            
            Glib::SignalProxy0<void> signal_clicked ();

            void append (Gtk::MenuItem &item);
            void append (const Glib::ustring &label, const sigc::slot0<void> &slot);
            
        private:
            void setup ();

            void button_event_cb (GdkEventButton *e);
            void menu_position_cb (int &x, int &y, bool &push_in);
        };
    }
}
    
#endif /* !GUIKACHU_WIDGET_DROPDOWN_BUTTON_H */
