//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "widgets/pos-entry.h"
#include "form-editor/form-editor.h"

#include "resource-manager.h"
#include "target.h"

using namespace Guikachu::GUI::PropertyEditors;

XPosEntry::XPosEntry (ResourceManager *manager,
		      property_t      &property,
		      op_factory_t    *op_factory) :
    NumEntry (0, (manager->get_target ()->screen_width - 1), property, op_factory),
    target (manager->get_target ())
{
    target->changed.connect (sigc::mem_fun (*this, &XPosEntry::target_changed_cb));
}

void XPosEntry::target_changed_cb ()
{
    set_max (target->screen_width - 1);
}

YPosEntry::YPosEntry (ResourceManager *manager,
		      property_t      &property,
		      op_factory_t    *op_factory) :
    NumEntry (0, (manager->get_target ()->screen_height - 1), property, op_factory),
    target (manager->get_target ())
{
    target->changed.connect (sigc::mem_fun (*this, &YPosEntry::target_changed_cb));
}

void YPosEntry::target_changed_cb ()
{
    set_max (target->screen_height - 1);
}
