//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "widgets/filechooserdialog.h"

#include "config.h"
#include "io/guikachu-io.h"
#include "ui.h"
#include "ui-gui.h"

#include <glib/gi18n.h>
#include <gtkmm/stock.h>
#include <gtkmm/messagedialog.h>

using namespace Guikachu::GUI;

FileChooserDialog::FileChooserDialog (Gtk::Window            &parent_window,
                                      const Glib::ustring    &title,
                                      Gtk::FileChooserAction  action,
                                      const Glib::ustring    &default_ext_) :
    Gtk::FileChooserDialog (parent_window, title, action),
    default_ext (default_ext_)
{
    add_button (Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);

    switch (action)
    {
    case Gtk::FILE_CHOOSER_ACTION_OPEN:
        add_button (Gtk::Stock::OPEN, Gtk::RESPONSE_ACCEPT);
        break;
    case Gtk::FILE_CHOOSER_ACTION_SAVE:
        add_button (Gtk::Stock::SAVE, Gtk::RESPONSE_ACCEPT);
        // We do our own overwrite confirmation because of the support for default extensions
        set_do_overwrite_confirmation (false);
        break;
    default:
        g_assert_not_reached ();
    }    
    set_default_response (Gtk::RESPONSE_ACCEPT);

#ifdef GUIKACHU_HAVE_GNOMEVFS
    set_local_only (false);
#else
    set_local_only (true);
#endif
}

Glib::ustring FileChooserDialog::get_uri () const
{
    return IO::create_canonical_uri (Gtk::FileChooserDialog::get_uri (), default_ext);
}

int FileChooserDialog::run ()
{
    for (;;)
    {
        int response = Gtk::FileChooserDialog::run ();
        if (response != Gtk::RESPONSE_ACCEPT || property_action () != Gtk::FILE_CHOOSER_ACTION_SAVE)
            return response;
    
        Glib::ustring uri = get_uri ();
        if (IO::uri_exists (uri))
        {
            Glib::ScopedPtr<char> message (
                g_strdup_printf (dgettext ("gtk20", "A file named \"%s\" already exists.  Do you want to replace it?"),
                                 UI::visible_filename (uri).c_str ()));
            Glib::ScopedPtr<char> secondary_message (
                g_strdup_printf (dgettext ("gtk20", "The file already exists in \"%s\".  Replacing it will overwrite its contents."),
                                 UI::visible_location (uri).c_str ()));
            
            Gtk::MessageDialog *confirm = new Gtk::MessageDialog (*this,
                                                                  message.get (), false,
                                                                  Gtk::MESSAGE_QUESTION,
                                                                  Gtk::BUTTONS_NONE,
                                                                  true);
            confirm->set_secondary_text (secondary_message.get ());
            
            Gtk::Button *overwrite_btn = UI::create_stock_button (Gtk::Stock::SAVE, _("Overwrite"));
            overwrite_btn->show ();
            
            confirm->add_button (Gtk::Stock::CANCEL, Gtk::RESPONSE_NO);
            confirm->add_action_widget (*manage (overwrite_btn), Gtk::RESPONSE_YES);
            confirm->set_default_response (Gtk::RESPONSE_NO);
            int confirm_response = confirm->run ();
            delete confirm;
            
            if (confirm_response == Gtk::RESPONSE_YES)
                return response;
        } else
        {
            return response;
        }
    }
}
