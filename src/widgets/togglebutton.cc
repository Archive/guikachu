//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "togglebutton.h"

#include <glib/gi18n.h>

using namespace Guikachu::GUI::PropertyEditors;

ToggleButton::ToggleButton (property_t   &property_,
			    op_factory_t *op_factory_):
    property (property_),
    op_factory (op_factory_),
    update_block (false)
{
    signal_clicked ().connect (sigc::mem_fun (*this, &ToggleButton::clicked_cb));
    
    property.changed.connect (sigc::mem_fun (*this, &ToggleButton::update));
    update ();
}

void ToggleButton::update ()
{
    update_block = true;
    
    set_active (property);
    
    remove ();
    if (property)
	set_label (_("Yes"));
    else
	set_label (_("No"));

    update_block = false;
}

void ToggleButton::clicked_cb ()
{
    if (update_block)
	return;

    op_factory->push_change (get_active ());
}

void ToggleButton::install_underline (Glib::RefPtr<Gtk::AccelGroup> &accel_group,
				      unsigned int                   accel_key)
{
    add_accelerator ("clicked",
		     accel_group, accel_key,
		     Gdk::MOD1_MASK, Gtk::ACCEL_VISIBLE);
}
