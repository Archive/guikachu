//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_TOGGLE_BUTTON_H
#define GUIKACHU_TOGGLE_BUTTON_H

#include <memory>
#include <gtkmm/togglebutton.h>
#include "property.h"
#include "property-ops.h"
#include "property-editor.h"

namespace Guikachu
{
    namespace GUI
    {
	namespace PropertyEditors
	{
	    class ToggleButton: public Gtk::ToggleButton,
                                public PropertyEditor
	    {
		typedef Property<bool>            property_t;
		typedef PropChangeOpFactory<bool> op_factory_t;
		
		property_t                  &property;
		std::auto_ptr<op_factory_t>  op_factory;
		
	    public:
		ToggleButton (property_t   &property,
			      op_factory_t *op_factory);

		void install_underline (Glib::RefPtr<Gtk::AccelGroup> &accel_group,
					unsigned int		       accel_key);
	    private:
		bool update_block;
		void update ();
		
		void clicked_cb ();
	    };
	}
    }
}

#endif /* !GUIKACHU_TOGGLE_BUTTON_H */
