//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "num-entry.h"

#include <gtkmm/adjustment.h>

using namespace Guikachu::GUI::PropertyEditors;

NumEntry::NumEntry (int min, int max,
		    property_t   &property_,
		    op_factory_t *op_factory_):
    PropertyEditor (this),
    property (property_),
    op_factory (op_factory_),
    update_block (false)
{
    Gtk::Adjustment *adj = new Gtk::Adjustment (min, min, max);
    adj->signal_value_changed ().connect (sigc::mem_fun (*this, &NumEntry::changed_cb));
    set_adjustment (*manage (adj));
    
    set_update_policy (Gtk::UPDATE_IF_VALID);
    set_digits (0);

    property.changed.connect (sigc::mem_fun (*this, &NumEntry::update));
    update ();
}

void NumEntry::set_min (int min)
{
    get_adjustment ()->set_lower (min);
}

void NumEntry::set_max (int max)
{
    get_adjustment ()->set_upper (max);
}

void NumEntry::update ()
{
    update_block = true;

    set_value (property);
    
    update_block = false;
}

void NumEntry::changed_cb ()
{
    if (update_block)
	return;

    op_factory->push_change (get_value_as_int ());
}
