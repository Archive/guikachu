//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "widgets/target-combo.h"

#include <glib/gi18n.h>

#include <gtkmm/menu.h>

#include "resource-manager.h"
#include "target.h"

using namespace Guikachu::GUI::PropertyEditors;

namespace Guikachu
{
    namespace TargetOps
    {
	class StockOp: public UndoOp
	{
	    Target      *target;
	    std::string  old_stock_id, new_stock_id;
	    int          old_width, old_height;

	public:
	    StockOp (Target            *target,
		     const std::string &new_stock_id);
	    
	    void undo ();
	    void redo ();
	    
	    Glib::ustring get_label () const { return _("Change target machine"); };
	};
    }
}
    
TargetCombo::TargetCombo (ResourceManager *manager_):
    PropertyEditor (this),
    manager (manager_),
    target (manager->get_target ()),
    update_block (false)
{
    Gtk::TreeModel::ColumnRecord cols;
    cols.add (col_id);
    cols.add (col_label);
    
    store = Gtk::ListStore::create (cols);

    Target::stock_list_t targets = Target::stock_targets ();
    for (Target::stock_list_t::const_iterator i = targets.begin (); i != targets.end (); ++i)
    {
        Gtk::TreeRow row = *(store->append ());
        row[col_id] = i->first;
        row[col_label] = i->second;
    }

    Gtk::TreeRow row = *(store->append ());
    row[col_id] = "";
    row[col_label] = _("Custom machine");

    set_model (store);
    pack_start (col_label);

    signal_changed ().connect (sigc::mem_fun (*this, &TargetCombo::changed_cb));
    target->changed.connect (sigc::mem_fun (*this, &TargetCombo::update));
    update ();
}

void TargetCombo::update ()
{
    Target::stock_id_t target_id = target->get_stock_id ();

    Gtk::TreeModel::iterator iter = store->get_iter ("0");
    while (Target::stock_id_t ((*iter)[col_id]) != target_id)
        ++iter;

    update_block = true;
    set_active (iter);
    update_block = false;
}

void TargetCombo::changed_cb ()
{
    if (update_block)
        return;

    Gtk::TreeModel::iterator iter = get_active ();
    Target::stock_id_t stock_id = (*iter)[col_id];

    if (stock_id != "")
    {
        UndoOp *op = new TargetOps::StockOp (target, stock_id);
        target->load_stock (stock_id);
        manager->get_undo_manager ().push (op);
        
        stock_selected.emit ();
    } else {
        custom_selected.emit ();
    }
}

Guikachu::TargetOps::StockOp::StockOp (Target            *target_,
				       const std::string &new_stock_id_) :
    target (target_),
    old_stock_id (target->get_stock_id ()),
    new_stock_id (new_stock_id_),
    old_width (target->screen_width),
    old_height (target->screen_height)
{
}

void Guikachu::TargetOps::StockOp::undo ()
{
    if (old_stock_id != "")
	target->load_stock (old_stock_id);
    else {
	target->screen_width = old_width;
	target->screen_height = old_height;
    }
}

void Guikachu::TargetOps::StockOp::redo ()
{
    target->load_stock (new_stock_id);
}
