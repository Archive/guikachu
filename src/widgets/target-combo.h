//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_WIDGETS_TARGET_COMBO_H
#define GUIKACHU_WIDGETS_TARGET_COMBO_H

#include <gtkmm/combobox.h>
#include <gtkmm/liststore.h>

#include "property-editor.h"
#include "resource-manager.h"
#include <map>

namespace Guikachu
{
    namespace GUI
    {
	namespace PropertyEditors
	{
	    class TargetCombo: public Gtk::ComboBox,
                               public PropertyEditor
	    {
		ResourceManager *manager;
		Target          *target;
		
		std::map<Target::stock_id_t, int> stock_target_to_menuitem;
		int                               num_targets;

                Gtk::TreeModelColumn<Target::stock_id_t>   col_id;
                Gtk::TreeModelColumn<Target::stock_desc_t> col_label;

                Glib::RefPtr<Gtk::ListStore> store;

	    public:
		TargetCombo (ResourceManager *manager);

		sigc::signal0<void> custom_selected;
		sigc::signal0<void> stock_selected;
		
	    private:
                bool update_block;
		void update ();

		void changed_cb ();
	    };
	}
    }
}

#endif /* !GUIKACHU_WIDGETS_TARGET_COMBO_H */
