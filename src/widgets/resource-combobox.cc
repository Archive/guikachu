//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "resource-combobox.h"

#include "resource-manager.h"

using namespace Guikachu;
using namespace Guikachu::GUI;

ResourceComboBox::ResourceComboBox (const std::set<Resources::Type> &types,
                                    ResourceManager                 *manager):
    ElementComboBoxBase<Resource> (types)
{
    manager->resource_created.connect (sigc::mem_fun (*this, &ResourceComboBox::element_created_cb));
    manager->resource_removed.connect (sigc::mem_fun (*this, &ResourceComboBox::element_removed_cb));
    
    // Create list items for existing resources
    const std::set<Resource*> resources = manager->get_resources ();
    std::for_each (resources.begin (), resources.end (),
                   sigc::mem_fun (*this, &ResourceComboBox::element_created_cb));
}
