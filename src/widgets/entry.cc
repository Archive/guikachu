//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "entry.h"

#include <sigc++/adaptors/bind_return.h>
#include <sigc++/adaptors/hide.h>

using namespace Guikachu::GUI::PropertyEditors;

Entry::Entry (bool          live_update,
	      property_t   &property_,
	      op_factory_t *op_factory_):
    PropertyEditor (this),
    property (property_),
    op_factory (op_factory_),
    update_block (false),
    cursor_pos (-1)
{
    if (live_update)
    {
        signal_insert_text ().connect_notify (sigc::mem_fun (*this, &Entry::insert_cb));
        signal_delete_text ().connect_notify (sigc::mem_fun (*this, &Entry::delete_cb));
	signal_changed ().connect (sigc::mem_fun (*this, &Entry::changed_cb));
    } else {
	signal_activate ().connect (sigc::mem_fun (*this, &Entry::changed_cb));
	sigc::slot<bool> activate_slot =
	    sigc::bind_return (sigc::mem_fun (*this, &Entry::changed_cb), false);
	signal_focus_out_event ().connect (sigc::hide (activate_slot));
    }

    property.changed.connect (sigc::mem_fun (*this, &Entry::update));
    update ();
}

void Entry::changed_cb ()
{
    if (update_block)
        return;

    update_block = true;

    const std::string new_val = get_text ();

    if (property != new_val)
	op_factory->push_change (new_val);
    
    property = convert_to_ascii (get_text ());
    update_block = false;
    
    update ();
}

void Entry::insert_cb (const Glib::ustring &text, int *position)
{
    cursor_pos = get_position () + text.length ();
}

void Entry::delete_cb (int start_pos, int end_pos)
{
    cursor_pos = start_pos;
}

void Entry::update ()
{
    update_block = true;

    set_text (Glib::ustring (property));
    if (cursor_pos >= 0)
	set_position (cursor_pos);
    
    update_block = false;
}
