//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_WIDGETS_STRING_LIST_OPS_PRIV_RESOURCE_H
#define GUIKACHU_WIDGETS_STRING_LIST_OPS_PRIV_RESOURCE_H

#include "string-list-ops-priv.h"

#include "resource.h"
#include "property-ops.h"

namespace Guikachu
{
    namespace ResourceOps
    {
	namespace StringListOps
	{
	    class OpBase: public virtual Guikachu::StringListOps::OpBase
	    {
		ResourceManager *manager;

		MemberHolder<Resource, property_t> prop_holder;
		
	    protected:
		std::string resource_id;
		
		OpBase (const Glib::ustring &op_label,
			Resource            *resource,
			property_t          &prop);

		Resource   * get_resource () const;
		property_t & get_prop () const;
	    };

	    
	    class AddOp: public virtual OpBase,
			 public virtual Guikachu::StringListOps::AddOp
	    {
	    public:
		AddOp (const Glib::ustring &op_label,
		       Resource            *resource,
		       property_t          &prop,
		       index_t              index,
		       const std::string   &item);
	    };
	    
	    
	    class RemoveOp: public virtual OpBase,
			    public virtual Guikachu::StringListOps::RemoveOp
	    {
	    public:
		RemoveOp (const Glib::ustring &op_label,
			  Resource            *resource,
			  property_t          &prop,
			  index_t              old_pos);
	    };


	    class ChangeOp: public virtual OpBase,
			    public virtual Guikachu::StringListOps::ChangeOp
	    {
	    public:
		ChangeOp (const Glib::ustring &op_label,
			  Resource            *resource,
			  property_t          &prop,
			  index_t              index,
			  const std::string   &new_item);
	    };


	    class MoveOp: public virtual OpBase,
			  public virtual Guikachu::StringListOps::MoveOp
	    {
	    public:
		MoveOp (const Glib::ustring &op_label,
			Resource            *resource,
			property_t          &prop,
			index_t              old_index,
			index_t              new_index);
		
		MoveOp (const Glib::ustring  &op_label,
			Resource             *resource,
			property_t           &prop,
			const index_list_t   &index_history_head,
			const index_list_t   &index_history_tail);
		
		UndoOp * combine (UndoOp *other_op) const;
	    };
	    
	} // namespace StringListOps
    } // namespace ResourceOps
} // namespace Guikachu

#endif /* !GUIKACHU_WIDGETS_STRING_LIST_OPS_PRIV_RESOURCE_H */

