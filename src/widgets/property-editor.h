//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_WIDGETS_PROPERTY_EDITOR_H
#define GUIKACHU_WIDGETS_PROPERTY_EDITOR_H

#include <gtkmm/widget.h>

namespace Guikachu
{
    namespace GUI
    {
	enum LabelAlignment
	{
	    LABEL_ALIGN_TOP,
	    LABEL_ALIGN_CENTER,
	    LABEL_ALIGN_BOTTOM
	};
	    
	class PropertyEditor
	{
	protected:
	    PropertyEditor ();
	    PropertyEditor (Gtk::Widget *underline_widget);
	    
	    Gtk::Widget *underline_widget;
	public:
            virtual ~PropertyEditor () {};
            
	    virtual LabelAlignment get_label_alignment () { return LABEL_ALIGN_CENTER; };
	    virtual bool           get_resizeable ()      { return false; };
	    virtual void           install_underline (Glib::RefPtr<Gtk::AccelGroup> &accel_group,
						      unsigned int                   accel_key);
	};
    }
}

#endif /* !GUIKACHU_WIDGETS_PROPERTY_EDITOR_H */
