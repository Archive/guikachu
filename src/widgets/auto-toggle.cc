//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "auto-toggle.h"

using namespace Guikachu::GUI::PropertyEditors;

AutoToggle::AutoToggle (Gtk::Widget  &widget_,
			property_t   &property_,
			op_factory_t *op_factory_) :
    PropertyEditor (this),
    widget (widget_),
    property (property_),
    op_factory (op_factory_),
    update_block (false)
{
    togglebutton.show ();
    pack_start (togglebutton, false, false);

    widget.show ();
    pack_start (widget);
    
    togglebutton.signal_clicked ().connect (sigc::mem_fun (*this, &AutoToggle::clicked_cb));
    
    property.changed.connect (sigc::mem_fun (*this, &AutoToggle::update));
    update ();
}

void AutoToggle::update ()
{
    update_block = true;

    togglebutton.set_active (property);
    widget.set_sensitive (property);
    
    update_block = false;
}

void AutoToggle::clicked_cb ()
{
    if (update_block)
	return;

    op_factory->push_change (togglebutton.get_active ());
}
