//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_WIDGETS_AUTO_TOGGLE_H
#define GUIKACHU_WIDGETS_AUTO_TOGGLE_H

#include <memory>
#include <gtkmm/checkbutton.h>
#include <gtkmm/box.h>
#include "property.h"
#include "property-ops.h"
#include "property-editor.h"

namespace Guikachu
{
    namespace GUI
    {
	namespace PropertyEditors
	{
	    class AutoToggle: public Gtk::HBox,
                              public PropertyEditor
	    {
		Gtk::CheckButton  togglebutton;
		Gtk::Widget      &widget;

		typedef Property<bool>            property_t;
		typedef PropChangeOpFactory<bool> op_factory_t;

		property_t                  &property;
		std::auto_ptr<op_factory_t>  op_factory;
		
	    public:
		AutoToggle (Gtk::Widget  &widget,
			    property_t   &property,
			    op_factory_t *op_factory);
		
		//void install_underline;
	    private:
		bool update_block;	    
		void update ();
		
		void clicked_cb ();
	    };
	}
    }
}

#endif /* !GUIKACHU_WIDGETS_AUTO_TOGGLE_H */
