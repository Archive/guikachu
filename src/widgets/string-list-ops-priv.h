//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_WIDGETS_STRING_LIST_OPS_PRIV_H
#define GUIKACHU_WIDGETS_STRING_LIST_OPS_PRIV_H

#include "string-list-ops.h"

namespace Guikachu
{
    namespace StringListOps
    {
	class OpBase: public Guikachu::UndoOp
	{
	protected:
	    typedef std::vector<std::string> value_t;
	    typedef Property<value_t>        property_t;
	    typedef unsigned int             index_t;

	    Glib::ustring op_label;

	    OpBase (const Glib::ustring &op_label_) :
		op_label (op_label_) {};

	    virtual property_t & get_prop () const = 0;
	    
	public:
	    Glib::ustring get_label () const { return op_label; };
	};
	
	class RemoveOp: public virtual OpBase
	{	    
	protected:
	    index_t     old_pos;
	    std::string old_item;
	    
	    RemoveOp (const Glib::ustring &op_label,
		      index_t              old_pos,
		      const std::string   &old_item);
	    
	public:
	    void undo ();
	    void redo ();
	};

	class AddOp: public virtual OpBase
	{
	protected:
	    index_t     index;
	    std::string item;
	    
	    AddOp (const Glib::ustring &op_label,
		   index_t              index,
		   const std::string   &item);
	    
	public:
	    void undo ();
	    void redo ();
	};

	class ChangeOp: public virtual OpBase
	{
	protected:
	    index_t     index;
	    std::string old_item, new_item;

	    ChangeOp (const Glib::ustring &op_label,
		      index_t              index,
		      const std::string   &old_item,
		      const std::string   &new_item);
	    
	public:
	    void undo ();
	    void redo ();
	};
	
	class MoveOp: public virtual OpBase
	{
	protected:
	    typedef std::list<index_t> index_list_t;
	    
	    index_list_t index_history;
	    
	    MoveOp (const Glib::ustring &op_label,
		    const index_list_t  &index_history_head,
		    const index_list_t  &index_history_tail);
	    
	    MoveOp (const Glib::ustring &op_label,
		    index_t              old_index,
		    index_t              new_index);
	    
	public:
	    void undo ();
	    void redo ();
	};
    } // namespace StringListOps
} // namespace Guikachu

#endif /* !GUIKACHU_WIDGETS_STRING_LIST_OPS_PRIV_H */

