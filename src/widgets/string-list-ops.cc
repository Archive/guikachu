//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "string-list-ops.h"
#include "string-list-ops-priv.h"
#include "string-list-ops-priv-resource.h"
#include "string-list-ops-priv-widget.h"

#include <glib.h> // for g_strdup_printf

using namespace Guikachu;

StringListOps::OpFactory::OpFactory (const Glib::ustring &add_template_,
				     const Glib::ustring &remove_template_,
				     const Glib::ustring &change_template_,
				     const Glib::ustring &move_template_,
				     property_t          &prop_,
				     UndoManager         &undo_manager_) :
    add_template (add_template_),
    remove_template (remove_template_),
    change_template (change_template_),
    move_template (move_template_),
    prop (prop_),
    undo_manager (undo_manager_)
{
}

void StringListOps::OpFactory::push_move (index_t old_index, index_t new_index)
{
    char *label_str = g_strdup_printf (move_template.c_str (),
				       prop()[old_index].c_str (),
				       get_parent_label ().c_str ());
    UndoOp *op = create_move_op (label_str, old_index, new_index);
    g_free (label_str);

    value_t val = prop;
    value_t::iterator old_i = val.begin () + old_index;
    value_t::iterator new_i = val.begin () + new_index;
    iter_swap (old_i, new_i);

    prop = val;
    undo_manager.push (op);
}

void StringListOps::OpFactory::push_change (index_t index, const item_t &new_item)
{
    std::string old_item = prop()[index];
    
    char *label_str = g_strdup_printf (change_template.c_str (),
				       old_item.c_str (),
				       get_parent_label ().c_str ());
    UndoOp *op = create_change_op (label_str, index, new_item);
    g_free (label_str);
    
    value_t val = prop;
    val[index] = new_item;
    
    prop = val;
    undo_manager.push (op);
}

void StringListOps::OpFactory::push_remove (index_t index)
{
    char *label_str = g_strdup_printf (remove_template.c_str (),
				       prop()[index].c_str (),
				       get_parent_label ().c_str ());
    UndoOp *op = create_remove_op (label_str, index);
    g_free (label_str);
    
    value_t val = prop;
    val.erase (val.begin () + index);
    
    prop = val;
    undo_manager.push (op);
}

void StringListOps::OpFactory::push_add (index_t index, const item_t &item)
{
    char *label_str = g_strdup_printf (add_template.c_str (),
				       item.c_str (),
				       get_parent_label ().c_str ());
    UndoOp *op = create_add_op (label_str, index, item);
    g_free (label_str);
    
    value_t val = prop;

    if (index >= val.size ())
	val.push_back (item);
    else
	val.insert (val.begin () + index, item);
    
    prop = val;
    undo_manager.push (op);
}
				     

ResourceOps::StringListOpFactory::StringListOpFactory (const Glib::ustring &add_template,
						       const Glib::ustring &remove_template,
						       const Glib::ustring &change_template,
						       const Glib::ustring &move_template,
						       Resource            *resource_,
						       property_t          &prop) :
    Guikachu::StringListOps::OpFactory (add_template, remove_template,
					change_template, move_template,
					prop,
					resource_->get_manager ()->get_undo_manager ()),
    resource (resource_)
{
}

UndoOp * ResourceOps::StringListOpFactory::create_move_op (const Glib::ustring &op_label,
							   index_t old_index, index_t new_index)
{
    return new ResourceOps::StringListOps::MoveOp (op_label, resource, prop, old_index, new_index);
}

UndoOp * ResourceOps::StringListOpFactory::create_change_op (const Glib::ustring &op_label,
							     index_t index, const item_t &new_item)
{
    return new ResourceOps::StringListOps::ChangeOp (op_label, resource, prop, index, new_item);
}

UndoOp * ResourceOps::StringListOpFactory::create_remove_op (const Glib::ustring &op_label,
							     index_t              index)
{
    return new ResourceOps::StringListOps::RemoveOp (op_label, resource, prop, index);
}

UndoOp * ResourceOps::StringListOpFactory::create_add_op (const Glib::ustring &op_label,
							  index_t              index,
							  const std::string   &new_item)
{
    return new ResourceOps::StringListOps::AddOp (op_label, resource, prop, index, new_item);
}




WidgetOps::StringListOpFactory::StringListOpFactory (const Glib::ustring &add_template,
						     const Glib::ustring &remove_template,
						     const Glib::ustring &change_template,
						     const Glib::ustring &move_template,
						     Widget              *widget_,
						     property_t          &prop) :
    Guikachu::StringListOps::OpFactory (add_template, remove_template,
					change_template, move_template,
					prop,
					widget_->get_manager ()->get_undo_manager ()),
    widget (widget_)
{
}

UndoOp * WidgetOps::StringListOpFactory::create_move_op (const Glib::ustring &op_label,
							 index_t old_index, index_t new_index)
{
    return new WidgetOps::StringListOps::MoveOp (op_label, widget, prop, old_index, new_index);
}

UndoOp * WidgetOps::StringListOpFactory::create_change_op (const Glib::ustring &op_label,
							   index_t index, const item_t &new_item)
{
    return new WidgetOps::StringListOps::ChangeOp (op_label, widget, prop, index, new_item);
}

UndoOp * WidgetOps::StringListOpFactory::create_remove_op (const Glib::ustring &op_label,
							   index_t index)
{
    return new WidgetOps::StringListOps::RemoveOp (op_label, widget, prop, index);
}

UndoOp * WidgetOps::StringListOpFactory::create_add_op (const Glib::ustring &op_label,
							index_t              index,
							const std::string   &new_item)
{
    return new WidgetOps::StringListOps::AddOp (op_label, widget, prop, index, new_item);
}
