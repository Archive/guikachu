//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "widget-combo.h"

#include <glib/gi18n.h>

#include <sigc++/adaptors/bind.h>

using namespace Guikachu::GUI::PropertyEditors;

WidgetCombo::WidgetCombo (Widgets::Type    type,
			  property_t      &property_,
			  op_factory_t    *op_factory_):
    Gtk::HBox (false, 5),
    button_edit (_("Edit...")),
    property (property_),
    op_factory (op_factory_)
{
    std::set<Widgets::Type> types;                
    types.insert (type);
    
    create_widgets (types);
}

WidgetCombo::WidgetCombo (const std::set<Widgets::Type> &types,
			  property_t                    &property_,
			  op_factory_t                  *op_factory_):
    Gtk::HBox (false, 5),
    button_edit (_("Edit...")),
    property (property_),
    op_factory (op_factory_)
{
    create_widgets (types);
}

void WidgetCombo::create_widgets (const std::set<Widgets::Type> &types)
{
    Resources::Form *form = property.get_form ();

    combo = new WidgetComboBox (types, form);

    combo->signal_changed ().connect (sigc::mem_fun (*this, &WidgetCombo::combo_cb));
    button_edit.signal_clicked ().connect (sigc::mem_fun (*this, &WidgetCombo::edit_cb));
    
    pack_start (*manage (combo));
    pack_end (button_edit, Gtk::PACK_SHRINK);
    underline_widget = combo;

    property.changed.connect (sigc::mem_fun (*this, &WidgetCombo::update));
    update ();
}

void WidgetCombo::update ()
{
    update_block = true;
    
    combo->set_value (property.resolve ());
    button_edit.set_sensitive (property != "");
    
    update_block = false;
}

void WidgetCombo::edit_cb ()
{
    Guikachu::Widget *widget = property.resolve ();
    g_return_if_fail (widget != 0);

    widget->request_edit ();
}

void WidgetCombo::combo_cb ()
{
    if (update_block)
	return;

    std::string new_val;
    Guikachu::Widget *widget = combo->get_value ();
    if (widget)
        new_val = widget->id;
    else
        new_val = "";

    if (property == new_val)
	return;
    
    op_factory->push_change (new_val);
    property = new_val;
}
