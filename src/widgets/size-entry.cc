//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "widgets/size-entry.h"
#include "form-editor/form-editor.h"
#include "resource-manager.h"
#include "target.h"

using namespace Guikachu::GUI::PropertyEditors;

WidthEntry::WidthEntry (ResourceManager *manager,
			property_t      &property,
			op_factory_t    *op_factory) :
    NumEntry (1, manager->get_target ()->screen_width, property, op_factory),
    target (manager->get_target ())
{
    target->changed.connect (sigc::mem_fun (*this, &WidthEntry::target_changed_cb));
}

void WidthEntry::target_changed_cb ()
{
    set_max (target->screen_width);
}

HeightEntry::HeightEntry (ResourceManager *manager,
			  property_t      &property,
			  op_factory_t    *op_factory) :
    NumEntry (1, manager->get_target ()->screen_height, property, op_factory),
    target (manager->get_target ())
{
    target->changed.connect (sigc::mem_fun (*this, &HeightEntry::target_changed_cb));
}

void HeightEntry::target_changed_cb ()
{
    set_max (target->screen_height);
}

