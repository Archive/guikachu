//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "resource-combo.h"

#include <glib/gi18n.h>

#include "resource-combobox.h"

#include <vector>
#include "resource-manager.h"

#include "resource-factory.h"

using namespace Guikachu::GUI::PropertyEditors;

ResourceCombo::ResourceCombo (Resources::Type  type,
			      property_t      &property_,
			      op_factory_t    *op_factory_):
    Gtk::HBox (false, 5),
    button_edit (_("Edit...")),
    property (property_),
    op_factory (op_factory_)
{
    std::set<Resources::Type> types;                
    types.insert (type);

    create_widgets (types);
}

ResourceCombo::ResourceCombo (const std::set<Resources::Type> &types,
			      property_t                      &property_,
			      op_factory_t                    *op_factory_):
    Gtk::HBox (false, 5),
    button_edit (_("Edit...")),
    property (property_),
    op_factory (op_factory_)
{
    create_widgets (types);
}



void ResourceCombo::create_widgets (const std::set<Resources::Type> &types)
{
    ResourceManager *manager = property.get_manager ();

    combo = new ResourceComboBox (types, manager);

    combo->signal_changed ().connect (sigc::mem_fun (*this, &ResourceCombo::combo_cb));
    button_edit.signal_clicked ().connect (sigc::mem_fun (*this, &ResourceCombo::edit_cb));
    
    pack_start (*manage (combo));
    pack_end (button_edit, Gtk::PACK_SHRINK);
    underline_widget = combo;

    property.changed.connect (sigc::mem_fun (*this, &ResourceCombo::update));
    update ();
}

void ResourceCombo::update ()
{
    update_block = true;

    combo->set_value (property.resolve ());
    button_edit.set_sensitive (property != "");

    update_block = false;
}

void ResourceCombo::edit_cb ()
{
    Resource *res = property.resolve ();
    g_return_if_fail (res != 0);

    Guikachu::GUI::get_resource_editor (res)->show ();
}

void ResourceCombo::combo_cb ()
{
    if (update_block)
	return;

    std::string new_val;
    Resource *res = combo->get_value ();
    if (res)
        new_val = res->id;
    else
        new_val = "";

    if (property == new_val)
	return;
    
    op_factory->push_change (new_val);
    property = new_val;
}
