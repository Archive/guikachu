//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_BITMAP_RES_H
#define GUIKACHU_BITMAP_RES_H

#include "resource.h"

#include <gdkmm/pixbuf.h>
#include "io/guikachu-io.h"

namespace Guikachu
{
    namespace Resources
    {
	class Bitmap: public Resource
	{
	public:
            enum BitmapType
            {
                TYPE_MONO,
                TYPE_GREY_4,
                TYPE_GREY_16,
                TYPE_COLOR_16,
                TYPE_COLOR_256,
		TYPE_COLOR_16K
            };

	    typedef Glib::RefPtr<Gdk::Pixbuf> ImageData;
	    
	    Bitmap (ResourceManager *manager, const std::string &id, serial_t serial);
            
	    Type get_type () const { return RESOURCE_BITMAP; };
	    void apply_visitor (ResourceVisitor &visitor) { visitor.visit_resource (this); };

	    Property<BitmapType>   bitmap_type;

            void load_file     (const Glib::ustring &uri);
	    void load_data     (const unsigned char *data, size_t len, const Glib::ustring &src_hint = "");

	    void save_file_bmp (const Glib::ustring &uri) const;
            void save_data_png (unsigned char *&data, size_t &len) const;
            void save_file_png (const Glib::ustring &uri) const;
            
            void set_image     (const ImageData &image);
	    void clear_image   ();

            ImageData get_image () const { return image; };
            
        private:
            ImageData image;
	};
    }
}

#endif /* !GUIKACHU_BITMAP_RES_H */
