//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "storage.h"

#include <libxml/parser.h>
#include <libxml/xmlmemory.h>

#include <glib.h>

using namespace Guikachu;

Storage::Storage () :
    doc (xmlNewDoc ((xmlChar*)"1.0"))
{
}

Storage::Storage (xmlDocPtr doc_) :
    doc (doc_)
{
}

Storage::~Storage ()
{
    clear ();
}

Storage::Storage (const Storage &other) :
    doc (xmlCopyDoc (other.doc, 1))
{
}

void Storage::clear ()
{
    if (doc)
        xmlFreeDoc (doc);
    doc = 0;
}

Storage & Storage::operator= (const Storage &other)
{
    clear ();

    doc = xmlCopyDoc (other.doc, 1);
    return *this;
}

void Storage::load (const Glib::ustring &filename) throw (Glib::Exception)
{
    clear ();
    
    doc = xmlParseFile (filename.c_str ());

    // FIXME: Use a more appropriate exception
    if (!doc)
    {
#ifdef GUIKACHU_HAVE_GNOMEVFS
        throw IO::Exception (Gnome::Vfs::ERROR_GENERIC);
#else
        throw IO::Exception ();
#endif
    }
}

void Storage::load_uri (const Glib::ustring &uri) throw (Glib::Exception)
{
#ifdef GUIKACHU_HAVE_GNOMEVFS
    clear ();
    
    xmlParserCtxtPtr parser_ctxt = 0;
        
    try {
        Gnome::Vfs::Handle f;
        f.open (uri, Gnome::Vfs::OPEN_READ);
        
        char            buffer[1024];
        IO::filesize_t  bytes_read;
        
        // The first chunk is needed to grab encoding information
        bytes_read = f.read (buffer, sizeof buffer - 1);
        parser_ctxt = xmlCreatePushParserCtxt (0, 0, buffer, bytes_read, uri.c_str ());
        
        while (bytes_read)
        {
            bytes_read = f.read (buffer, sizeof buffer - 1);
            xmlParseChunk (parser_ctxt, buffer, bytes_read, 0);
        }
        
        // Last chunk
        xmlParseChunk (parser_ctxt, buffer, 0, 1);

        if (!parser_ctxt->myDoc || !parser_ctxt->wellFormed)
            throw IO::Exception (Gnome::Vfs::ERROR_WRONG_FORMAT);
        
        xmlFreeDoc (doc);
        doc = parser_ctxt->myDoc;
        
        xmlFreeParserCtxt (parser_ctxt);
    } catch (...) {
        if (parser_ctxt)
        {
            if (parser_ctxt->myDoc)
                xmlFreeDoc (parser_ctxt->myDoc);
            
            xmlFreeParserCtxt (parser_ctxt);
        }

        throw;
    }
#else
    load (uri);
#endif
}

void Storage::load_buffer (unsigned char *buffer, IO::filesize_t buffer_size) throw (Glib::Exception)
{
    clear ();
    
    xmlParserCtxtPtr parser_ctxt = 0;
    
    try {
        parser_ctxt = xmlCreatePushParserCtxt (0, 0, (char*)buffer, buffer_size, "");
        xmlParseChunk (parser_ctxt, (char*)buffer, 0, 1);
        
        if (!parser_ctxt->myDoc || !parser_ctxt->wellFormed)
            throw IO::Exception ();
        
        doc = parser_ctxt->myDoc;
        xmlFreeParserCtxt (parser_ctxt);
    } catch (...) {
        if (parser_ctxt->myDoc)
            xmlFreeDoc (parser_ctxt->myDoc);
        
        xmlFreeParserCtxt (parser_ctxt);

        throw;
    }
}

void Storage::save_uri (const Glib::ustring &uri) const throw (Glib::Exception)
{
    g_assert (doc != 0);

    unsigned char *buffer = 0;
    
    try {
        IO::filesize_t buffer_size;
        
        save_buffer (buffer, buffer_size);    
        IO::save_uri (uri, buffer, buffer_size);
        xmlFree (buffer);
    } catch (...) {
        if (buffer)
            xmlFree (buffer);
        
        throw;
    }
}

void Storage::save_buffer (unsigned char *&buffer, IO::filesize_t &buffer_size) const
{
    int buffer_size_tmp;

    xmlDocDumpMemory (doc, &buffer, &buffer_size_tmp);
    buffer_size = buffer_size_tmp;
}

StorageNode Storage::get_root ()
{
    g_assert (doc != 0);
    
    return StorageNode (xmlDocGetRootElement (doc), this);
}

void Storage::set_root (StorageNode &node)
{
    g_assert (doc != 0);
    g_return_if_fail (node.c_node ());

    xmlDocSetRootElement (doc, node.c_node ());
    node.free_node = false;
}

StorageNode Storage::create_root (const std::string &name)
{
    g_assert (doc != 0);
    
    xmlNodePtr root_node = xmlNewNode (NULL, (xmlChar*) name.c_str ());
    xmlDocSetRootElement (doc, root_node);

    return StorageNode (root_node, this);
}
