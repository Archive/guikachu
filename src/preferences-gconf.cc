//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "preferences.h"

#include <gconfmm/init.h>
#include <gconfmm/client.h>

namespace
{
    Glib::RefPtr<Gnome::Conf::Client> gconf_client;

    const std::string GCONF_ROOT = "/apps/guikachu/";
    
} // anonymous namespace

void Guikachu::Preferences::init ()
{
    Gnome::Conf::init ();
    
    // Set up GConf client
    gconf_client = Gnome::Conf::Client::get_default_client ();
    gconf_client->add_dir ("/apps/guikachu", Gnome::Conf::CLIENT_PRELOAD_RECURSIVE);
    
    // Set up GConf callbacks
    // User interface
    gconf_client->notify_add (GCONF_ROOT + "Interface",
                              sigc::hide (sigc::hide (Interface::recent_files_changed.make_slot ())));
    
    // Colors
    gconf_client->notify_add (GCONF_ROOT + "FormEditor/Colors",
                              sigc::hide (sigc::hide (FormEditor::colors_changed.make_slot ())));
}

#include <iostream>

namespace
{   
    std::string create_key (const std::string &path, const std::string &key)
    {
        return GCONF_ROOT + path + "/" + key;
    }
    
    int get_conf_int (const std::string &path, const std::string &key, int default_value)
    {
        try {
            return gconf_client->get_int (create_key (path, key));
        } catch (Glib::Exception &e) {
            std::cerr << "ERROR: " << e.what () << std::endl;
            return default_value;
        }
    }
    
    bool get_conf_bool (const std::string &path, const std::string &key, bool default_value)
    {
        try {
            return gconf_client->get_bool (create_key (path, key));
        } catch (Glib::Exception &e) {
            std::cerr << "ERROR: " << e.what () << std::endl;
            return default_value;
        }
    }
    
    float get_conf_float (const std::string &path, const std::string &key, float default_value)
    {
        try {
            return gconf_client->get_float (create_key (path, key));
        } catch (Glib::Exception &e) {
            std::cerr << "ERROR: " << e.what () << std::endl;
            return default_value;            
        }
    }

    void set_conf_float (const std::string &path, const std::string &key, float value)
    {
        gconf_client->set (create_key (path, key), value);
    }

    std::string get_conf_string (const std::string &path, const std::string &key, const std::string &default_value)
    {
        try {
            return gconf_client->get_string (create_key (path, key));
        } catch (Glib::Exception &e) {
            std::cerr << "ERROR: " << e.what () << std::endl;
            return default_value;            
        }
    }

    void set_conf_string (const std::string &path, const std::string &key, const std::string &value)
    {
        gconf_client->set (create_key (path, key), value);
    }

    typedef Guikachu::Preferences::Interface::RecentFiles FileList;
    
    FileList get_conf_filelist (const std::string &path, const std::string &key)
    {
        try {
            return gconf_client->get_string_list (create_key (path, key));
        } catch (Glib::Exception &e) {
            std::cerr << "ERROR: " << e.what () << std::endl;
            return FileList ();
        }
    }

    void set_conf_filelist (const std::string &path, const std::string &key, const FileList &value)
    {
        Gnome::Conf::Value gval (Gnome::Conf::VALUE_LIST);
        gval.set_list_type (Gnome::Conf::VALUE_STRING);
        gval.set_string_list (value);

        gconf_client->set (create_key (path, key), gval);
    }
    
} // anonymous namespace
