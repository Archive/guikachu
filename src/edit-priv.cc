//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "edit-priv.h"

#include "io/xml-loader.h"
#include "io/xml-saver.h"
#include "storage.h"

using namespace Guikachu;

Resource * Edit::Priv::duplicate_resource_impl (Resource *resource)
{
    // Create a new XML document in memory
    Storage storage;
    StorageNode root_node = storage.create_root ("dup");
    IO::XML::ResourceSaver saver (root_node);
    resource->apply_visitor (saver);

    // Create new resource
    Resource *new_resource = resource->get_manager ()->create_resource (
        resource->get_type (), resource->id, true);

    // Load resource data from the StorageNode
    IO::XML::ResourceLoader loader (root_node);
    new_resource->apply_visitor (loader);

    return new_resource;
}

Widget * Edit::Priv::duplicate_widget_impl (Widget *widget)
{
    // Create a new XML document in memory
    Storage storage;
    StorageNode root_node = storage.create_root ("dup");
    IO::XML::WidgetSaver saver (root_node);
    widget->apply_visitor (saver);

    // Create new resource
    Widget *new_widget = widget->get_form ()->create_widget (
        widget->get_type (), widget->id, true);

    // Load resource data from the StorageNode
    IO::XML::WidgetLoader loader (root_node);
    new_widget->apply_visitor (loader);

    int new_x = std::min (new_widget->x + 10, new_widget->get_form ()->get_screen_width ());
    int new_y = std::min (new_widget->y + 10, new_widget->get_form ()->get_screen_height ());

    new_widget->x = new_x;
    new_widget->y = new_y;

    return new_widget;
}

std::set<std::pair<Widget*, Widget*> > Edit::Priv::duplicate_widgets_impl (const std::set<Widget*> &widgets)
{
    std::set<std::pair<Widget*, Widget*> > created_widgets;

    for (std::set<Widget*>::const_iterator i = widgets.begin (); i != widgets.end (); ++i)
        created_widgets.insert (std::pair<Widget*, Widget*> (*i, Edit::Priv::duplicate_widget_impl (*i)));

    return created_widgets;
}
