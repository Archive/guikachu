//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_STRINGLIST_WIN_H
#define GUIKACHU_STRINGLIST_WIN_H

#include "stringlist-res.h"
#include "resource-win.h"

#include <gtkmm/window.h>

namespace Guikachu
{
    namespace GUI
    {
	class StringListWindow: public ResourceWindow,
				public sigc::trackable
	{
	    Resources::StringList *res;
	    Gtk::Window            window;

	public:
	    StringListWindow (Resources::StringList *res);

	    void show ();
	private:
	    bool delete_event_impl (GdkEventAny *e);
	    void update ();
	};
    }
}

#endif /* !GUIKACHU_STRING_WIN_H */
