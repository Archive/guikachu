//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_EDIT_CUT_AND_PASTE_H
#define GUIKACHU_EDIT_CUT_AND_PASTE_H

#include "resource-manager.h"
#include "form-editor/widget.h"
#include "form-res.h"

#include <set>

namespace Guikachu
{
    namespace Edit
    {
	void copy_resource      (Resource *resource);
        void duplicate_resource (Resource *resource);
	void paste_resources    (ResourceManager *manager);

	void copy_widget       (Widget *widget);
	void copy_widgets      (const std::set<Widget*> &widgets);
	void duplicate_widget  (Widget *widget);
	void duplicate_widgets (const std::set<Widget*> &widgets);
	void paste_widgets     (Resources::Form *form);
    }
}

#endif /* !GUIKACHU_EDIT_CUT_AND_PASTE_H */
