//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "resource-factory.h"
#include "resource-factory-win.h"

using namespace Guikachu;
using namespace Guikachu::GUI;

namespace
{
    class ResourceEditorManager: public sigc::trackable
    {
    public:
	typedef std::map<Resource*, ResourceWindow*> editor_map_t;
	
    private:
	editor_map_t editors;
	
	void resource_deleted_cb (Resource *res);
	
    public:
	ResourceWindow * get_editor (Resource *res);
    };
	    
    ResourceEditorManager * get_resource_editor_manager ()
    {
	static ResourceEditorManager * instance = 0;
	
	if (!instance)
	    instance = new ResourceEditorManager;
	
	return instance;
    }
    
} // anonymous namespace

void ResourceEditorManager::resource_deleted_cb (Resource *res)
{
    editor_map_t::iterator found = editors.find (res);
    if (found != editors.end ())
    {
	delete found->second;
	editors.erase (found);
    }    
}   

ResourceWindow * ResourceEditorManager::get_editor (Resource *res)
{
    editor_map_t::iterator found = editors.find (res);
    if (found != editors.end ())
	return found->second;
    
    res->deleted.connect (
        sigc::bind (sigc::mem_fun (*this, &ResourceEditorManager::resource_deleted_cb), res));
    
    return editors[res] = ResourceWindowFactoryVisitor (res).get_result ();
}

ResourceWindow * ::Guikachu::GUI::get_resource_editor (Resource *res)
{
    return get_resource_editor_manager ()->get_editor (res);
}
