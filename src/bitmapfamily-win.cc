//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "bitmapfamily-win.h"
#include "bitmapfamily-res-ops.h"

#include <glib/gi18n.h>

#include <sigc++/adaptors/bind_return.h>

#include "property-ops-resource.h"

#include "widgets/propertytable.h"
#include "widgets/entry.h"

#include "bitmapfamily-win-helpers.h"
#include "bitmap-win-helpers.h"

#include "form-editor/form-editor-canvas.h"
#include "preferences.h"

#include <gdk-pixbuf/gdk-pixbuf.h>

#include <gtkmm/alignment.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/frame.h>
#include <gtkmm/button.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/box.h>
#include <gtkmm/main.h>
#include <gtkmm/scrolledwindow.h>

#include "ui-gui.h"
#include "widgets/filechooserdialog.h"

using namespace Guikachu;

GUI::BitmapFamilyWindow::BitmapFamilyWindow (Resources::BitmapFamily *res_):
    res (res_),
    type (Resources::Bitmap::TYPE_MONO)
{
    using ResourceOps::PropChangeOpFactory;
    using ResourceOps::RenameOpFactory;
    using namespace BitmapFamilyWindow_Helpers;
    
    window.signal_delete_event ().connect (
	sigc::bind_return (sigc::hide (sigc::mem_fun (window, &Gtk::Widget::hide)), true));
    window.property_allow_grow () = true;

    Gtk::VBox *main_vbox = new Gtk::VBox (false, 5);
    main_vbox->set_border_width (5);
    
    /* Resource ID */
    // TODO: Underline accelerator for resource entry
    Gtk::Label *id_label = new Gtk::Label (_("Resource ID:"), 1, 0.5);
    Gtk::Widget *id_entry = new GUI::PropertyEditors::Entry (false, res->id, new RenameOpFactory (res));
    Gtk::HBox *id_hbox = new Gtk::HBox (false, 5);
    id_hbox->pack_start (*manage (id_label), false, false);
    id_hbox->pack_start (*manage (id_entry), true, true);
    main_vbox->pack_start (*manage (id_hbox), false, false);
    
    /* Buttons */
    Gtk::Box *buttonbox = new Gtk::HBox (false, 5);
    main_vbox->pack_end (*manage (buttonbox), false, false);
    load_button = UI::create_stock_button (Gtk::Stock::OPEN, _("_Load"));
    load_button->signal_clicked ().connect (sigc::mem_fun (*this, &BitmapFamilyWindow::load_cb));
    buttonbox->add (*manage (load_button));

    export_button = UI::create_stock_button (Gtk::Stock::SAVE_AS, _("_Export"));
    export_button->signal_clicked ().connect (sigc::mem_fun (*this, &BitmapFamilyWindow::export_cb));
    buttonbox->add (*manage (export_button));

    remove_button = UI::create_stock_button (Gtk::Stock::REMOVE, _("_Remove"));
    remove_button->signal_clicked ().connect (sigc::mem_fun (*this, &BitmapFamilyWindow::remove_cb));
    buttonbox->add (*manage (remove_button));

    /* List of color depths */
    Gtk::ScrolledWindow *scroll_win = new Gtk::ScrolledWindow;
    scroll_win->set_shadow_type (Gtk::SHADOW_IN);
    scroll_win->set_policy (Gtk::POLICY_NEVER, Gtk::POLICY_NEVER);
    DepthList *depth_list = new DepthList (res);
    scroll_win->add (*manage (depth_list));
    depth_list->type_changed.connect (sigc::mem_fun (*this, &BitmapFamilyWindow::type_changed_cb));

    /* Preview */
    Gtk::Alignment *align = new Gtk::Alignment (0.5, 0.5, 0, 0);
    Gtk::Frame *frame = new Gtk::Frame;
    frame->add (preview_pixmap);
    align->add (*manage (frame));
    
    Gtk::Box *list_hbox = new Gtk::HBox (false, 5);
    list_hbox->pack_start (*manage (scroll_win), false, false);
    list_hbox->pack_end (*manage (align), true, true, 10);
    main_vbox->pack_start (*manage (list_hbox), true, true);
    
    window.add (*manage (main_vbox));

    res->changed.connect (sigc::mem_fun (*this, &BitmapFamilyWindow::update));
    update ();
}

void GUI::BitmapFamilyWindow::show ()
{
    window.show_all ();
    window.raise ();
}

void GUI::BitmapFamilyWindow::delete_event_impl ()
{
    window.hide ();
}

void GUI::BitmapFamilyWindow::update ()
{
    // Update window title
    gchar *title_buf = g_strdup_printf (_("Bitmap Group: %s"), res->id ().c_str ());
    window.set_title (title_buf);
    g_free (title_buf);

    Resources::Bitmap::ImageData image = res->get_image (type);
    
    Glib::RefPtr<Gdk::Pixbuf> pixbuf = FormEditor::render_bitmap (image, type);

    // Update buttons
    export_button->set_sensitive (true);
    remove_button->set_sensitive (true);    
    
    if (!pixbuf)
    {
        preview_pixmap.set ("");
        export_button->set_sensitive (false);
        remove_button->set_sensitive (false);
    }

    if (type == Resources::Bitmap::TYPE_GREY_16)
    {
        load_button->set_sensitive (!res->get_image (Resources::Bitmap::TYPE_COLOR_16));
    }

    if (type == Resources::Bitmap::TYPE_COLOR_16)
    {
        load_button->set_sensitive (!res->get_image (Resources::Bitmap::TYPE_GREY_16));
    }
    
    // Update the preview
    if (pixbuf)
    {
        guint32 background_color = 0xFFFFFF;

        if (!res->get_manager ()->get_target ()->screen_color &&
	    type < Resources::Bitmap::TYPE_COLOR_16)
        {
            Gdk::Color c (Preferences::FormEditor::get_color_bg ());
            background_color = ((c.get_red () >> 8) << 16) +
		((c.get_green () >> 8) << 8) +
		((c.get_blue ()  >> 8));
        }
        
        Glib::RefPtr<Gdk::Pixbuf> pixbuf_with_background =
	    pixbuf->composite_color_simple (pixbuf->get_width (), pixbuf->get_height (),
					    Gdk::INTERP_NEAREST, 255, 1,
					    background_color, background_color);

        if (pixbuf_with_background)
	    preview_pixmap.set (pixbuf_with_background);
    }
}

void GUI::BitmapFamilyWindow::load_cb ()
{
    char *title = g_strdup_printf (_("Loading image file to %s"), res->id ().c_str ());
    FileChooserDialog file_chooser (window, title, Gtk::FILE_CHOOSER_ACTION_OPEN);
    g_free (title);
    
    Gtk::FileFilter filter;
    filter.set_name (_("Image files"));
    filter.add_custom (Gtk::FILE_FILTER_MIME_TYPE, sigc::ptr_fun (BitmapWindow_Helpers::is_image_file));
    file_chooser.add_filter (filter);
    
    if (file_chooser.run () == Gtk::RESPONSE_ACCEPT)
    {
        UndoOp *op = new ResourceOps::BitmapFamilyOps::ImageChangeOp (res, type, res->get_image (type));
        res->load_file (type, file_chooser.get_uri ());
        res->get_manager ()->get_undo_manager ().push (op);
    }
}

void GUI::BitmapFamilyWindow::export_cb ()
{
    char *title = g_strdup_printf (_("Exporting %s"), res->id ().c_str ());
    FileChooserDialog file_chooser (window, title, Gtk::FILE_CHOOSER_ACTION_SAVE, "png");
    g_free (title);
    
    Gtk::FileFilter filter;
    filter.set_name (_("PNG files"));
    filter.add_mime_type ("image/png");
    file_chooser.add_filter (filter);
    
    if (file_chooser.run () == Gtk::RESPONSE_ACCEPT)
	res->save_file_png (type, file_chooser.get_uri ());
}

void GUI::BitmapFamilyWindow::remove_cb ()
{
    UndoOp *op = new ResourceOps::BitmapFamilyOps::ImageRemoveOp (res, type);
    res->clear_image (type);
    res->get_manager ()->get_undo_manager ().push (op);
}

void GUI::BitmapFamilyWindow::type_changed_cb (Resources::Bitmap::BitmapType type_)
{
    type = type_;
    update ();
}
