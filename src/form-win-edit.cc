//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "form-win.h"

#include "edit-cut-and-paste.h"

using namespace Guikachu::GUI;

void FormWindow::widget_cut_cb (Widget *widget)
{
    widget_copy_cb (widget);
    widget_remove_cb (widget);
}

void FormWindow::widget_copy_cb (Widget *widget)
{
    if (!widget || selection.find (widget) != selection.end ())
    {
	Edit::copy_widgets (selection);
    } else {
	Edit::copy_widget (widget);
    }
}

void FormWindow::widget_paste_cb ()
{
    Edit::paste_widgets (res);
}

void FormWindow::widget_duplicate_cb (Widget *widget)
{
    if (!widget || selection.find (widget) != selection.end ())
    {
	Edit::duplicate_widgets (selection);
    } else {
	Edit::duplicate_widget (widget);
    }    
}
