//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "bitmap-win.h"

#include "bitmap-res-ops.h"

#include <glib/gi18n.h>

#include "bitmap-win-helpers.h"

#include <gtkmm/alignment.h>
#include <gtkmm/menu.h>
#include <gtkmm/main.h>
#include <gtkmm/button.h>
#include <gtkmm/frame.h>
#include <gtkmm/box.h>
#include <gtkmm/messagedialog.h>

#include "ui-gui.h"
#include "widgets/filechooserdialog.h"

#include <sigc++/adaptors/bind_return.h>
#include <sigc++/adaptors/hide.h>

#include "property-ops-resource.h"

#include "widgets/propertytable.h"
#include "widgets/entry.h"

#include "form-editor/form-editor-canvas.h"
#include "preferences.h"

#include <gdk-pixbuf/gdk-pixbuf.h>

using namespace Guikachu;

GUI::BitmapWindow::BitmapWindow (Resources::Bitmap *res_):
    res (res_)
{
    using ResourceOps::PropChangeOpFactory;
    using ResourceOps::RenameOpFactory;
    using namespace BitmapWindow_Helpers;
    
    window.signal_delete_event ().connect (sigc::mem_fun (*this, &BitmapWindow::delete_event_impl));
    window.property_allow_grow () = true;
    
    GUI::PropertyTable *proptable = new GUI::PropertyTable;
    Gtk::Widget *control;

    /* Resource ID */
    control = new GUI::PropertyEditors::Entry (false, res->id, new RenameOpFactory (res));
    proptable->add (_("Resource _ID:"), *manage (control));

    /* Type */
    control = new DepthCombo (res);
    proptable->add (_("T_ype:"), *manage (control));

    /* Preview of the image */
    Gtk::VBox *vbox = new Gtk::VBox (false, 5);

    Gtk::Button *load_button = UI::create_stock_button (Gtk::Stock::OPEN, _("_Load"));
    export_button = UI::create_stock_button (Gtk::Stock::SAVE_AS, _("_Export"));

    Gtk::HBox *button_box = new Gtk::HBox (false, 5);    
    button_box->pack_start (*manage (load_button));
    button_box->pack_start (*manage (export_button));
    Gtk::Frame *preview_frame = new Gtk::Frame;

//    preview_pixmap.set_padding (5, 5);
    preview_frame->add (preview_pixmap);
    
    Gtk::Alignment *alignment = new Gtk::Alignment (0, 0.5, 0, 0);
    alignment->add (*manage (preview_frame));
    vbox->add (*manage (alignment));
    vbox->add (*manage (button_box));
    
    export_button->signal_clicked ().connect (sigc::mem_fun (*this, &BitmapWindow::export_image_cb));
    load_button->signal_clicked ().connect (sigc::mem_fun (*this, &BitmapWindow::load_image_cb));
    proptable->add (_("Image preview:"), *manage (vbox));
    
    window.add (*manage (proptable));

    res->changed.connect (sigc::mem_fun (*this, &BitmapWindow::update));
    res->get_manager ()->get_target ()->changed.connect (
        sigc::mem_fun (*this, &BitmapWindow::update_image));
    update ();
}

void GUI::BitmapWindow::show ()
{
    window.show_all ();
    window.raise ();
}

bool GUI::BitmapWindow::delete_event_impl (GdkEventAny *e)
{
    window.hide ();
    return true;
}

void GUI::BitmapWindow::update_image ()
{
    Resources::Bitmap::ImageData image = res->get_image ();
    if (!image)
    {
        preview_pixmap.set ("");
        export_button->set_sensitive (false);
        return;
    }
    export_button->set_sensitive (true);
    
    Glib::RefPtr<Gdk::Pixbuf> pixbuf = FormEditor::render_bitmap (image, res->bitmap_type);

    if (pixbuf)
    {
        guint32 background_color = 0xFFFFFF;

        if (!res->get_manager ()->get_target ()->screen_color &&
            res->bitmap_type < Resources::Bitmap::TYPE_COLOR_16)
        {
            Gdk::Color c (Preferences::FormEditor::get_color_bg ());
            background_color = ((c.get_red () >> 8) << 16) +
                ((c.get_green () >> 8) << 8) + (c.get_blue () >> 8);
        }

        Glib::RefPtr<Gdk::Pixbuf> pixbuf_with_background =
            pixbuf->composite_color_simple (pixbuf->get_width (),
                                            pixbuf->get_height (),
                                            Gdk::INTERP_NEAREST, 255,
                                            1, background_color, background_color);

        preview_pixmap.set (pixbuf_with_background);
    }
}

void GUI::BitmapWindow::update ()
{
    // Update window title
    gchar *title_buf = g_strdup_printf (_("Bitmap: %s"), res->id ().c_str ());
    window.set_title (title_buf);
    g_free (title_buf);

    update_image ();
}

void GUI::BitmapWindow::load_image_cb ()
{
    char *title = g_strdup_printf (_("Loading image file to %s"), res->id ().c_str ());
    FileChooserDialog file_chooser (window, title, Gtk::FILE_CHOOSER_ACTION_OPEN);
    g_free (title);
    
    Gtk::FileFilter filter;
    filter.set_name (_("Image files"));
    filter.add_custom (Gtk::FILE_FILTER_MIME_TYPE, sigc::ptr_fun (BitmapWindow_Helpers::is_image_file));
    file_chooser.add_filter (filter);
    
    if (file_chooser.run () == Gtk::RESPONSE_ACCEPT)
    {
        Resources::Bitmap::ImageData old_image = res->get_image ();
        res->load_file (file_chooser.get_uri ());
        
        UndoOp *op = new ResourceOps::BitmapOps::ImageChangeOp (res, old_image);
        res->get_manager ()->get_undo_manager ().push (op);
    }
}

void GUI::BitmapWindow::export_image_cb ()
{
    char *title = g_strdup_printf (_("Exporting %s"), res->id ().c_str ());
    FileChooserDialog file_chooser (window, title, Gtk::FILE_CHOOSER_ACTION_SAVE, "png");
    g_free (title);
    
    Gtk::FileFilter filter;
    filter.set_name (_("PNG files"));
    filter.add_mime_type ("image/png");
    file_chooser.add_filter (filter);
    
    if (file_chooser.run () == Gtk::RESPONSE_ACCEPT)
	res->save_file_png (file_chooser.get_uri ());
}
