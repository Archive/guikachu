//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "menu-win.h"
#include "menu-win-treemodel.h"
#include "widgets/dropdown-button.h"

#include <glib/gi18n.h>

#include "property-ops-resource.h"
#include "menu-res-ops.h"

#include <sigc++/adaptors/hide.h>
#include <sigc++/adaptors/bind.h>
#include <sigc++/adaptors/bind_return.h>

#include "ui.h"
#include "ui-gui.h"
#include "property-ops.h"

#include "cellrenderer-indent.h"

#include <gtkmm/stock.h>
#include <gtkmm/box.h>

#include <foocanvasmm/canvas.h>
#include "form-editor/menubar-canvas.h"
#include "form-editor/screen-canvas.h"

using namespace Guikachu;
using namespace Guikachu::ResourceOps::MenuOps;

GUI::MenuWindow::MenuWindow (Guikachu::Resources::Menu *res_):
    res (res_),
    popup_menu (0),
    dropdown_menu (0),
    current_entry (0),
    update_block (0)
{
    Glib::RefPtr<Gnome::Glade::Xml> gui = UI::glade_gui ("res_menu");
    gui->get_widget ("res_menu", window);
    window->signal_delete_event ().connect (sigc::bind_return (
        sigc::mem_fun (*this, &MenuWindow::delete_event_impl), true));    
    
    gui->get_widget ("menu_id", id_entry);
    id_entry->signal_activate ().connect (sigc::mem_fun (*this, &MenuWindow::id_entry_cb));
    id_entry->signal_focus_out_event ().connect (
        sigc::hide (sigc::bind_return (sigc::mem_fun (*this, &MenuWindow::id_entry_cb), false)));

    // Preview
    Gtk::Container *preview_frame;
    gui->get_widget ("menu_preview_frame", preview_frame);
    FooCanvasmm::Canvas *canvas = new ScreenCanvas (res->get_manager ()->get_target ());
    new FormEditor::ScreenCanvasItem (*canvas->root (), res->get_manager ()->get_target ());
    new FormEditor::MenubarCanvasItem (*canvas->root (), res);

    preview_frame->add (*manage (canvas));
    
    // Menu items list
    Gtk::TreeModel::ColumnRecord cols;
    cols.add (col_is_submenu);
    cols.add (col_label);
    cols.add (col_id);
    cols.add (col_shortcut);
    cols.add (col_is_separator);
    treemodel = MenuWindow_Helpers::MenuTreeModel::create (res);

    gui->get_widget ("menu_tree", treeview);
    treeview->set_model (treemodel);
    create_columns ();

    treeview->get_selection ()->set_mode (Gtk::SELECTION_SINGLE);
    treeview->get_selection ()->signal_changed ().connect (
	sigc::mem_fun (*this, &MenuWindow::selection_changed_cb));
    treeview->set_reorderable (true);

    treeview->signal_button_press_event ().connect_notify (sigc::mem_fun (*this, &MenuWindow::button_press_cb));
    treeview->signal_key_press_event ().connect (sigc::bind_return (sigc::mem_fun (*this, &MenuWindow::key_press_cb), true));

    // Buttons
    gui->get_widget ("menu_btn_remove", btnRemove);
    gui->get_widget ("menu_up", btnUp);
    gui->get_widget ("menu_down", btnDown);

    DropdownButton *submenu_dropdown = new DropdownButton (Gtk::Stock::ADD, _("Add _Submenu"));
    btnSubmenu = submenu_dropdown;
    submenu_dropdown->append (_("Add _Submenu"), sigc::mem_fun (*this, &MenuWindow::create_submenu_cb));
    MenuWindow_Helpers::stock_menu_list_t stock_menu_list = MenuWindow_Helpers::get_stock_menus ();
    for (MenuWindow_Helpers::stock_menu_list_t::const_iterator i = stock_menu_list.begin ();
         i != stock_menu_list.end (); ++i)
    {
        submenu_dropdown->append (
            Glib::ScopedPtr<char> (g_strdup_printf (_("Add '%s' menu"), i->second.c_str ())).get (),
            sigc::bind (sigc::mem_fun (*this, &MenuWindow::create_stock_submenu_cb), i->first));
    }

    DropdownButton *item_dropdown = new DropdownButton (Gtk::Stock::ADD, _("Add _Item"));    
    btnItem = item_dropdown;
    item_dropdown->append (_("Add _Item"), sigc::bind (sigc::mem_fun (*this, &MenuWindow::create_item_cb), false));
    item_dropdown->append (_("Add _Separator"), sigc::bind (sigc::mem_fun (*this, &MenuWindow::create_item_cb), true));
    
    Gtk::Box *buttonbox = 0;
    gui->get_widget ("menu_buttonbox", buttonbox);
    buttonbox->pack_start (*manage (submenu_dropdown), Gtk::PACK_SHRINK);
    buttonbox->pack_start (*manage (item_dropdown), Gtk::PACK_SHRINK);
    
    btnRemove->signal_clicked ().connect (sigc::mem_fun (*this, &MenuWindow::btn_remove_cb));
    btnUp->signal_clicked ().connect (sigc::mem_fun (*this, &MenuWindow::btn_up_cb));
    btnDown->signal_clicked ().connect (sigc::mem_fun (*this, &MenuWindow::btn_down_cb));
    submenu_dropdown->signal_clicked ().connect (sigc::mem_fun (*this, &MenuWindow::create_submenu_cb));
    item_dropdown->signal_clicked ().connect (sigc::bind (sigc::mem_fun (*this, &MenuWindow::create_item_cb), false));
    res->changed.connect (sigc::mem_fun (*this, &MenuWindow::update));
    update ();
}

GUI::MenuWindow::~MenuWindow ()
{
    delete popup_menu;
    delete dropdown_menu;
    delete window;
}

void GUI::MenuWindow::cell_label_cb (Gtk::CellRenderer *cell, const Gtk::TreeModel::iterator &iter) const
{
    typedef Gtk::CellRendererText text_cell_t;
    typedef CellRendererIndent<text_cell_t> indent_cell_t;
    
    text_cell_t *cell_text = dynamic_cast<text_cell_t*> (cell);
    g_return_if_fail (cell_text);

    if ((*iter)[col_is_submenu])
    {
        cell_text->property_text () = (*iter)[col_label];
        cell_text->property_weight () = Pango::WEIGHT_BOLD;
        cell_text->property_editable () = true;
    } else {
        cell_text->property_weight () = Pango::WEIGHT_NORMAL;

        if ((*iter)[col_is_separator])
        {
            cell_text->property_text () = "------";
            cell_text->property_editable () = false;
        } else {
            cell_text->property_text () = (*iter)[col_label];
            cell_text->property_editable () = true;
        }
    }
}

void GUI::MenuWindow::cell_id_cb (Gtk::CellRenderer *cell, const Gtk::TreeModel::iterator &iter) const
{
    Gtk::CellRendererText *cell_id = dynamic_cast<Gtk::CellRendererText*> (cell);
    g_return_if_fail (cell_id);

    if ((*iter)[col_is_submenu])
    {
        cell_id->property_text () = "";
        cell_id->property_editable () = false;
    } else {
        cell_id->property_text () = "";
        if (!(*iter)[col_is_separator])
            cell_id->property_text () = (*iter)[col_id];
        cell_id->property_editable () = !(*iter)[col_is_separator];
    }
}

void GUI::MenuWindow::cell_shortcut_cb (Gtk::CellRenderer *cell, const Gtk::TreeModel::iterator &iter) const
{
    Gtk::CellRendererText *cell_shortcut = dynamic_cast<Gtk::CellRendererText*> (cell);
    g_return_if_fail (cell_shortcut);

    cell_shortcut->property_text () = "";
    cell_shortcut->property_editable () = false;
    
    if ((*iter)[col_is_submenu])
        return;

    cell_shortcut->property_editable () = !(*iter)[col_is_separator];
    
    if (!(*iter)[col_is_separator] && (*iter)[col_shortcut])
    {
        cell_shortcut->property_text () =
            Glib::ScopedPtr<char> (g_strdup_printf ("%c", toupper ((*iter)[col_shortcut]))).get ();
    }
}

void GUI::MenuWindow::cell_label_edit_cb (const Glib::ustring &path, const Glib::ustring &new_text)
{
    current_entry = 0;
    
    Gtk::TreeModel::iterator iter = treemodel->get_iter (path);    
    treemodel->set_item_label (iter, new_text);
}

void GUI::MenuWindow::cell_id_edit_cb (const Glib::ustring &path, const Glib::ustring &new_text)
{
    current_entry = 0;
    
    Gtk::TreeModel::iterator iter = treemodel->get_iter (path);
    treemodel->set_item_id (iter, new_text);
}

void GUI::MenuWindow::cell_shortcut_edit_cb (const Glib::ustring &path, const Glib::ustring &new_text)
{
    current_entry = 0;
    
    Gtk::TreeModel::iterator iter = treemodel->get_iter (path);
    char new_shortcut = new_text == "" ? 0 : new_text[0];
    treemodel->set_item_shortcut (iter, new_shortcut);
}

void GUI::MenuWindow::delete_event_impl (GdkEventAny *e)
{
    window->hide ();
}

void GUI::MenuWindow::show ()
{
    window->show_all ();
    window->raise ();
}

void GUI::MenuWindow::update ()
{
    ++update_block;
    
    gchar *title_buf = g_strdup_printf (_("Menu: %s"), res->id ().c_str ());
    window->set_title (title_buf);
    g_free (title_buf);

    id_entry->set_text (Glib::ustring (res->id));

    treeview->expand_all ();
    selection_changed_cb ();

    --update_block;
}

void GUI::MenuWindow::create_columns ()
{
    CellRendererIndent<Gtk::CellRendererText> *indent_cell = 
        CellRendererIndent<Gtk::CellRendererText>::setup_view (*treeview, _("Label"),
            sigc::mem_fun (*this, &MenuWindow::cell_label_cb));

    static_cast<Gtk::CellRendererText*> (indent_cell->get_real_cell ())->
        signal_edited ().connect (sigc::mem_fun (*this, &MenuWindow::cell_label_edit_cb));

    Gtk::CellRendererText *id_renderer = new Gtk::CellRendererText;
    Gtk::TreeView::Column *id_col = new Gtk::TreeView::Column (_("ID"), *manage (id_renderer));
    id_col->set_cell_data_func (*id_renderer, sigc::mem_fun (*this, &MenuWindow::cell_id_cb));
    id_renderer->signal_edited ().connect (sigc::mem_fun (*this, &MenuWindow::cell_id_edit_cb));
    treeview->append_column (*manage (id_col));
    
    Gtk::CellRendererText *shortcut_renderer = new Gtk::CellRendererText;
    Gtk::TreeView::Column *shortcut_col = new Gtk::TreeView::Column (_("Shortcut"), *manage (shortcut_renderer));
    shortcut_col->set_cell_data_func (*shortcut_renderer, sigc::mem_fun (*this, &MenuWindow::cell_shortcut_cb));
    shortcut_renderer->signal_edited ().connect (sigc::mem_fun (*this, &MenuWindow::cell_shortcut_edit_cb));
    treeview->append_column (*manage (shortcut_col));
}

void GUI::MenuWindow::reset_controls ()
{
    ++update_block;
    
    btnRemove->set_sensitive (false);
    btnItem->set_sensitive (false);
    
    --update_block;
}

void GUI::MenuWindow::selection_changed_cb ()
{
    Gtk::TreeModel::iterator iter = treeview->get_selection ()->get_selected ();
	
    reset_controls ();

    btnUp->set_sensitive (treemodel->can_move_row_up (iter));
    btnDown->set_sensitive (treemodel->can_move_row_down (iter));
    if (!iter)
        return;
    
    btnRemove->set_sensitive (true);
    btnItem->set_sensitive (true);
}

void GUI::MenuWindow::id_entry_cb ()
{
    if (update_block)
	return;

    ResourceOps::RenameOpFactory op_factory (res);

    const std::string new_id = id_entry->get_text ();
    op_factory.push_change (new_id);

    id_entry->set_text (Glib::ustring (res->id));
}

void GUI::MenuWindow::btn_remove_cb ()
{
    Gtk::TreeModel::iterator iter = treeview->get_selection ()->get_selected ();
    if (!iter)
        return;

    treemodel->remove_row (iter);
}

void GUI::MenuWindow::btn_up_cb ()
{
    Gtk::TreeModel::iterator iter = treeview->get_selection ()->get_selected ();
    if (!iter)
        return;

    treemodel->move_row_up (iter);
}

void GUI::MenuWindow::btn_down_cb ()
{
    Gtk::TreeModel::iterator iter = treeview->get_selection ()->get_selected ();
    if (!iter)
        return;

    treemodel->move_row_down (iter);
}

void GUI::MenuWindow::create_submenu_cb ()
{
    Gtk::TreePath path = treemodel->insert_submenu (treeview->get_selection ()->get_selected (),
                                                    Resources::Menu::Submenu (_("New submenu")));

    UI::flush_events ();
    treeview->get_selection ()->select (path);
    treeview->set_cursor (path, *treeview->get_column (1), true);
}

void GUI::MenuWindow::create_stock_submenu_cb (MenuWindow_Helpers::StockMenu stock_id)
{
    Resources::Menu::Submenu submenu = MenuWindow_Helpers::create_stock_menu (stock_id);
    
    Gtk::TreePath path = treemodel->insert_submenu (treeview->get_selection ()->get_selected (), submenu);
    UI::flush_events ();
    treeview->get_selection ()->select (path);
    treeview->set_cursor (path, *treeview->get_column (1), false);
}

void GUI::MenuWindow::create_item_cb (bool separator)
{
    Resources::Menu::MenuItem menuitem;
    menuitem.separator = separator;

    if (!separator)
    {
	menuitem.id = res->create_id ("MENUITEM_");
	menuitem.label = menuitem.id;
	menuitem.shortcut = 0;
    }
    
    Gtk::TreePath path = treemodel->insert_menuitem (treeview->get_selection ()->get_selected (), menuitem);

    UI::flush_events ();
    treeview->get_selection ()->select (path);
    treeview->set_cursor (path, *treeview->get_column (1), true);
}

void GUI::MenuWindow::button_press_cb (GdkEventButton *e)
{
    if (e->button != 3)
	return;

    show_popup_menu (e->button, e->time);
}

void GUI::MenuWindow::key_press_cb (GdkEventKey *e)
{
    switch (e->keyval)
    {
    case GDK_Delete:
    case GDK_KP_Delete:
        btn_remove_cb ();
        break;
    case GDK_Menu:
        show_popup_menu (0, e->time);
        break;
        
    default:
        break;
    }
}

void GUI::MenuWindow::show_popup_menu (guint button, guint32 time)
{
}
