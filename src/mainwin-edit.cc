//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <glib/gi18n.h>

#include "mainwin.h"

#include "edit-cut-and-paste.h"
#include "resource-manager-ops.h"

using namespace Guikachu::GUI;

void MainWin::cut_cb ()
{
    Resource *selected_resource = resource_tree.get_selected ();
    if (!selected_resource)
	return;

    Edit::copy_resource (selected_resource);
    
    manager->get_undo_manager ().push (new ResourceOps::RemoveOp (selected_resource));
    manager->remove_resource (selected_resource);
}

void MainWin::copy_cb ()
{
    Resource *selected_resource = resource_tree.get_selected ();
    if (!selected_resource)
	return;

    Edit::copy_resource (selected_resource);
}

void MainWin::paste_cb ()
{
    Edit::paste_resources (manager);
}

void MainWin::duplicate_cb ()
{
    Resource *selected_resource = resource_tree.get_selected ();
    if (!selected_resource)
	return;

    Edit::duplicate_resource (selected_resource);
}

