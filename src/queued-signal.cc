//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 *
 * This portion of the code is written by Martin Schulze <MHL.Schulze@t-online.de>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "queued-signal.h"

#include <glibmm/main.h>
#include <sigc++/bind_return.h>

using namespace Guikachu;

namespace
{
    void default_queuer (VSignal::slot_t slot)
    {
	slot ();
    }
    
} // anonymous namespace

QueuedSignal::signal_queuer_t QueuedSignal::signal_queuer = default_queuer;

QueuedSignal::QueuedSignal () :
    queued (false)
{
}

void QueuedSignal::set_signal_queuer (signal_queuer_t new_signal_queuer)
{
    signal_queuer = new_signal_queuer;
}

void QueuedSignal::emit ()
{
    if (!queued)
    {
        queued = true;
	signal_queuer (sigc::mem_fun (*this, &QueuedSignal::idle_cb));
    }

}

void QueuedSignal::idle_cb ()
{
    if (queued)
    {
	VSignal::emit ();
	queued = false;
    }
}
