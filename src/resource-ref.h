//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_RESOURCE_REF_H
#define GUIKACHU_RESOURCE_REF_H

#include "property.h"
#include "resource.h"
#include <sigc++/trackable.h>
#include <sigc++/connection.h>

namespace Guikachu
{
    namespace Properties
    {
	class ResourceRef: public Property<std::string>,
			   public sigc::trackable
	{
	    ResourceManager  *manager;
	    Resource         *current_resource;
	    sigc::connection  current_resource_changed;
	    
	public:
	    ResourceRef (notify_signal_t   &notify_signal,
			 ResourceManager   *manager,
			 const std::string &value = "");
	    
	    virtual void set_val (const std::string &value);
	    inline const std::string& operator= (const std::string &value_) { set_val (value_); return value; };

            ResourceManager * get_manager () const { return manager; };
            Resource *        resolve () const { return current_resource; };

	    sigc::signal0<void> resource_changed;
	    
	private:
	    void resource_created_cb (Resource *res);
	    void resource_removed_cb (Resource *res);
	    void resource_changed_cb ();
	};
    }
}

#endif /* !GUIKACHU_RESOURCE_REF_H */
