//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_ADD_RESOURCE_H
#define GUIKACHU_ADD_RESOURCE_H

#include <gtkmm/entry.h>
#include <gtkmm/dialog.h>
#include <gtkmm/combobox.h>
#include <gtkmm/liststore.h>
#include "cellrenderer-icontext.h"

#include "resource.h"
#include "resource-manager.h"

namespace Guikachu
{
    namespace GUI
    {
	class AddResourceWin: public sigc::trackable
	{
	    ResourceManager *manager;

	    Gtk::Dialog   *dialog;
	    Gtk::Entry    *id_entry;
            Gtk::ComboBox *type_combo;

            Gtk::TreeModelColumn<Resources::Type> col_type;
            
            Glib::RefPtr<Gtk::ListStore> store;

            static Resources::Type last_type;
	    
	public:
	    AddResourceWin (ResourceManager *manager);

	    void run (Gtk::Window *parent_win = 0);

        private:
            void cell_cb (const Gtk::TreeModel::iterator &iter, CellRendererIconText *cell);
	};
    }
}

#endif /* !GUIKACHU_ADD_RESOURCE_H */
