//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_STORAGE_H
#define GUIKACHU_STORAGE_H

#include <string>
#include <sigc++/signal.h>
#include <glibmm/exception.h>

#include "io/guikachu-io.h"

#include "storage-node.h"

namespace Guikachu
{
    class Storage
    {
	xmlDocPtr doc;
	
    public:
	Storage ();
	Storage (xmlDocPtr doc);
	~Storage ();

	Storage (const Storage &other);
	Storage & operator= (const Storage &other);
	
	void load        (const Glib::ustring &filename) throw (Glib::Exception); 
	void load_uri    (const Glib::ustring &uri) throw (Glib::Exception);
	void save_uri    (const Glib::ustring &uri) const throw (Glib::Exception);

        void load_buffer (unsigned char *buffer, IO::filesize_t buffer_size) throw (Glib::Exception);
	void save_buffer (unsigned char *&buffer, IO::filesize_t &buffer_size) const;
	
	StorageNode get_root    ();
	void        set_root    (StorageNode &root);
	StorageNode create_root (const std::string &name);

	sigc::signal0<void> encoding_notify;

    private:
        void clear ();
    };
    
}; // namespace Guikachu


#endif /* !GUIKACHU_STORAGE_H */
