//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_RESOURCE_TREE_H
#define GUIKACHU_RESOURCE_TREE_H

namespace Guikachu
{
    namespace GUI
    {
	class ResourceTree;
    }
}

#include "resource.h"
#include "app-res.h"
#include "resource-manager.h"

#include <gtkmm/treeview.h>
#include <gtkmm/treestore.h>

#include "hash-map.h"

namespace Guikachu
{
    namespace GUI
    {
	class ResourceTree: public Gtk::TreeView
	{
	    ResourceManager *manager;

            class TreeData
            {
                enum {
                    TREEDATA_APP,
                    TREEDATA_CATEGORY,
                    TREEDATA_RESOURCE,
                    TREEDATA_NULL
                } type;

                union {
                    Resources::Application *app;
                    Resources::Type         category;
                    Resource               *res;
                };
                
            public:
                TreeData ();
                TreeData (Resources::Application *app);
                TreeData (Resources::Type         category);
                TreeData (Resource               *res);

                Glib::ustring get_label () const;
                Resource *    get_resource () const;

                void apply_to_cell (Gtk::CellRenderer *cell) const;
            };
            
	    Gtk::TreeModelColumn<TreeData> col_data;
	    Glib::RefPtr<Gtk::TreeStore>   treestore;

            typedef std::map<Guikachu::Resource*, Gtk::TreeRow> row_map_t;
            row_map_t row_map;

	    typedef std::hash_map<Resources::Type, Gtk::TreeRow> subtree_map_t;

	    Gtk::TreeRow  root_tree;
	    subtree_map_t subtrees;

            bool grouped;
	    
	public:
	    ResourceTree ();

	    void set_manager (ResourceManager *manager);
            void set_grouped (bool grouped);

            Resource * get_selected ();
            bool       get_app_selected ();

            sigc::signal1<void, Resource*> resource_activated;
            sigc::signal0<void>            app_activated;

            sigc::signal2<void, guint, guint32> menu;

	private:
            void cell_label_cb (Gtk::CellRenderer *cell, const Gtk::TreeModel::iterator &iter) const;
            
	    void create_tree_for_type (Resources::Type type);

	    void row_activated_cb (const Gtk::TreeModel::Path &path,
				   Gtk::TreeView::Column      *col);

            void button_press_cb (GdkEventButton *e);
	    void key_press_cb    (GdkEventKey    *e);

	    void app_changed_cb ();

            void reset ();
	    void resource_created_cb (Resource *res);
	    void resource_removed_cb (Resource *res);
	    void resource_changed_cb (Resource *res);

            Gtk::TreeStore::iterator get_place (Resource *res);
            bool compare_treerow (const Gtk::TreeRow &row, Resource *res) const;
	};
    }
}

#endif /* !GUIKACHU_RESOURCE_TREE_H */
