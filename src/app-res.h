//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_APP_RES_H
#define GUIKACHU_APP_RES_H

#include "property.h"
#include "queued-signal.h"
#include <map>

namespace Guikachu
{
    class ResourceManager;
    
    namespace Resources
    {
	class Application: public sigc::trackable
	{
	    ResourceManager *manager;
	    
	    class VendorID: public Property<std::string>
	    {
	    public:
		VendorID (notify_signal_t   &notify_signal,
			  const std::string &value);
		
		inline const std::string& operator= (const std::string &value_) { set_val (value_); return value; };
		virtual void set_val (const std::string &value);
	    };

        public:
            typedef std::string tag_key_t;
            typedef std::string tag_val_t;
            typedef std::map<tag_key_t, tag_val_t> tag_map_t;
        private:
            tag_map_t tags;
            
	public:
	    Application (ResourceManager *manager);
	    QueuedSignal changed;

	    ResourceManager *get_manager () { return manager; };
	    
	    Property<std::string> iconname;
	    Property<std::string> version;
	    VendorID              vendor;
	    
	    void reset ();

            void set_tag (const tag_key_t &key, const tag_val_t &val);
            tag_val_t get_tag (const tag_key_t &key) const;
            tag_map_t get_tags () const;
            void add_tags (const tag_map_t &tags);
	};
    };
};

#endif /* !GUIKACHU_APP_RES_H */
