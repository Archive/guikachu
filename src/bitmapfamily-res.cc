//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "bitmapfamily-res.h"

#include "io/bmp-saver.h"
#include <gdkmm/pixbufloader.h>

#include "ui.h"

using namespace Guikachu;

Resources::BitmapFamily::BitmapFamily (ResourceManager   *manager,
				       const std::string &id,
				       serial_t           serial):
    Resource (manager, id, serial)
{
}

Resources::BitmapFamily::ImageData Resources::BitmapFamily::get_image (BitmapType type) const
{
    image_map_t::const_iterator i = images.find (type);

    if (i != images.end ())
	return i->second;
    else
	return ImageData ();
}

void Resources::BitmapFamily::load_file (BitmapType type, const Glib::ustring &uri)
{
    unsigned char  *data = 0;
    IO::filesize_t  len;

    try {
        
        IO::load_uri (uri, data, len);
        load_data (type, data, len);
        
    } catch (Glib::Exception &e) {
        UI::show_error_io_load_resource (uri, id, e.what ());
    }
    
    delete[] data;
}

void Resources::BitmapFamily::load_data (BitmapType type, const unsigned char *data, size_t len,
                                         const Glib::ustring &src_hint)
{
    Glib::RefPtr<Gdk::PixbufLoader> loader = Gdk::PixbufLoader::create ();
    bool closed = (len == 0);
    
    try {
	loader->write (data, len);
	loader->close ();
        closed = true;
	
	ImageData pixbuf = loader->get_pixbuf ();
	
	set_image (type, pixbuf);
    } catch (Glib::Exception &e) {
        if (!closed)
            loader->close ();
        
        UI::show_error_io_load_resource (src_hint, id, e.what ());
    }
}

void Resources::BitmapFamily::save_file_bmp (BitmapType type, const Glib::ustring &uri) const
{
    ImageData image = get_image (type);
    
    if (!image)
	return;
    
    unsigned char *buf = 0;
    IO::filesize_t len;

    try {
        IO::save_bmp (image, buf, len);
	IO::save_uri (uri, buf, len);
    } catch (Glib::Exception &e) {
        UI::show_error_io_save_resource (uri, id, e.what ());
    }

    delete[] buf;
}

void Resources::BitmapFamily::save_data_png (BitmapType type, unsigned char *&data, size_t &len) const
{
    len = 0;
    data = 0;
    
    ImageData image = get_image (type);
    if (!image)
        return;
    
    gchar *pixbuf_buf;

    image->save_to_buffer ((gchar*&)pixbuf_buf, (gsize&)len, "png",
                           std::list<Glib::ustring>(), std::list<Glib::ustring>());
    
    data = new unsigned char[len];
    memcpy (data, pixbuf_buf, len);
    g_free (pixbuf_buf);
}    

void Resources::BitmapFamily::save_file_png (BitmapType type, const Glib::ustring &uri) const
{
    unsigned char *buf = 0;
    size_t len;
    
    try {
        save_data_png (type, buf, len);
	IO::save_uri (uri, buf, len);
    } catch (Glib::Exception &e) {
        UI::show_error_io_save_resource (uri, id, e.what ());
    }

    delete[] buf;
}

void Resources::BitmapFamily::set_image (BitmapType type, const ImageData &image)
{
    // 4bpp color and greyscale bitmaps are mutually exclusive
    if (image)
    {
        if (type == Bitmap::TYPE_GREY_16)
            images[Bitmap::TYPE_COLOR_16] = ImageData ();
        else if (type == Bitmap::TYPE_COLOR_16)
            images[Bitmap::TYPE_GREY_16] = ImageData ();
    }
    
    images[type] = image;

    changed.emit ();
}

void Resources::BitmapFamily::clear_image (BitmapType type)
{
    set_image (type, ImageData ());
}
