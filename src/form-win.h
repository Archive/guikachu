//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_WIN_H
#define GUIKACHU_FORM_WIN_H

#include "form-res.h"
#include "resource-win.h"

#include "form-editor/form.h"
#include "form-editor/form-canvas.h"
#include "form-editor/widget-canvas.h"
#include "form-editor/widget.h"

#include <gtkmm/window.h>
#include <gtkmm/spinbutton.h>
#include <gtkmm/scrolledwindow.h>
#include <foocanvasmm/canvas.h>
#include <set>

namespace Guikachu
{
    namespace GUI
    {
	namespace FormWindow_Helpers
	{
	    class CanvasSelectionWrapper;
	    class WidgetTreeWrapper;
	}
	
	class FormWindow: public ResourceWindow,
			  public sigc::trackable
	{
	    Resources::Form *res;
	    
	    Gtk::Window *window;
	    Gtk::Window *property_window;
	    
	    FooCanvasmm::Canvas *canvas;
	    Gtk::SpinButton     *zoom_entry;
            Gtk::ScrolledWindow *scrollwindow;

	    Widgets::Form              *form_widget;
	    FormEditor::FormCanvasItem *form_canvas_item;

	    FormWindow_Helpers::CanvasSelectionWrapper *selection_canvas;
	    FormWindow_Helpers::WidgetTreeWrapper      *widget_tree;

	    Widget *edited_widget;
	    Widget *last_widget;

	    typedef std::set<Widget*> selection_t;
	    selection_t               selection;
	    
	    sigc::connection   edited_widget_changed_conn;
	    
	    FooCanvasmm::Item *canvas_overlay;
	    Widgets::Type      palette_selection;

	public:
	    FormWindow (Resources::Form *res);
	    ~FormWindow ();
	    void show ();
	    
	private:
	    void show_property_editor (Widget *widget);
	    void show_form_property_editor ();

	    void delete_event_impl (GdkEventAny *e);
	    void property_delete_event_impl (GdkEventAny *e);

	    void zoom_entry_cb ();
	    
	    void update ();
	    void edited_widget_changed ();

	    void force_canvas_update ();
	    void recenter_canvas ();
	    void colors_changed_cb ();
	    void target_changed_cb ();

	    void palette_cb (Widgets::Type type);
	    bool canvas_overlay_cb (GdkEvent *event);
	    void add_widget (int x, int y);

	    void key_press_cb (GdkEventKey *e);

	    void widget_created_cb (Widget *widget);
	    void widget_removed_cb (Widget *widget);

	    void widget_remove_cb (Widget *widget);

	    FormEditor::WidgetCanvasItem * get_canvas_item (Widget *widget);
	    
	    // Layouting
	    void align_left   ();
	    void align_right  ();
	    void align_top    ();
	    void align_bottom ();
            void center_horizontal ();
            void center_vertical   ();
	    
	    // Selection managment
	    void selection_add    (Widget *widget);
	    void selection_remove (Widget *widget);
	    void selection_toggle (Widget *widget);
	    void selection_clear  ();

	    void remove_selection ();
	    
	    void select_form   ();
	    void select_all    ();
	    void select_widget (Widget *widget);

	    void tree_widget_selected_cb (Widget *widget, bool selected);
	    
	    void form_clicked_cb  ();
	    void form_released_cb ();
	    void form_menu_cb     (guint   button,
				   guint32 time);

	    void widget_selected_last_cb (bool selected, Widget *widget);
	    void widget_clicked_cb       (Widget  *widget);
	    void widget_released_cb      (Widget  *widget);
	    void widget_menu_cb          (guint    button,
					  guint32  time,
					  Widget  *widget);
	    void widget_cut_cb           (Widget  *widget);
	    void widget_copy_cb          (Widget  *widget);
	    void widget_paste_cb         ();
            void widget_duplicate_cb     (Widget  *widget);
	    
	    void selection_box_cb (int x1, int y1,
				   int x2, int y2);

	    // Resizing by drag & drop
	    void form_drag_motion_cb   (int dx, int dy);
	    void form_drag_end_cb      (int dx, int dy);
	    
	    void widget_drag_motion_cb (int dx, int dy);
	    void widget_drag_end_cb    (int dx, int dy);
	};
    }
}

#endif /* !GUIKACHU_FORM_WIN_H */
