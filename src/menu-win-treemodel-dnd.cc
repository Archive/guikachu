//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "menu-win-treemodel.h"

#include <gtkmm/treepath.h>

#include "storage.h"
#include "storage-node.h"

#include "menu-res-ops.h"

using namespace Guikachu;
using namespace Guikachu::GUI::MenuWindow_Helpers;
using namespace Guikachu::ResourceOps::MenuOps;

bool MenuTreeModel::row_draggable_vfunc (const Path &path) const
{
    return true;
}

namespace {
    const char *TARGET_SUBMENU  = "application/x-guikachu/menu/submenu";
    const char *TARGET_MENUITEM = "application/x-guikachu/menu/menuitem";
} // anonymous namespace

void MenuTreeModel::move_submenu (submenu_idx_t old_submenu_idx, submenu_idx_t new_submenu_idx)
{
    Resources::Menu::Submenu submenu = submenus[old_submenu_idx];
    submenus.erase  (submenus.begin () + old_submenu_idx);
    submenus.insert (submenus.begin () + new_submenu_idx, submenu);
        
    res->get_manager ()->get_undo_manager ().push (
        new SubmenuMoveOp (res, old_submenu_idx, new_submenu_idx));
    res->set_submenus (submenus);        
    submenus_old = submenus;
    
    std::vector<submenu_idx_t> new_order (submenus.size ());
    for (submenu_idx_t i = 0; i != submenus.size (); ++i)
        new_order[i] = i;
    new_order.erase (new_order.begin () + old_submenu_idx);
    new_order.insert (new_order.begin () + new_submenu_idx, old_submenu_idx);
    
#if 0
    rows_reordered (Path (), iterator (), new_order);
#else
    GtkTreePath *reorder_path = gtk_tree_path_new ();
    int *new_order_c = new int[new_order.size ()];
    for (unsigned int i = 0; i != new_order.size (); ++i)
        new_order_c[i] = new_order[i];
    gtk_tree_model_rows_reordered (Gtk::TreeModel::gobj (), reorder_path, 0, new_order_c);
    gtk_tree_path_free (reorder_path);
#endif  
}

void MenuTreeModel::move_item (submenu_idx_t old_submenu_idx, item_idx_t old_item_idx,
                               submenu_idx_t new_submenu_idx, item_idx_t new_item_idx)
{
    Resources::Menu::MenuItems &old_items = submenus[old_submenu_idx].items;
    Resources::Menu::MenuItems &new_items = submenus[new_submenu_idx].items;
    
    Resources::Menu::MenuItem item = old_items[old_item_idx];    
    old_items.erase  (old_items.begin () + old_item_idx);
    new_items.insert (new_items.begin () + new_item_idx, item);
    
    res->get_manager ()->get_undo_manager ().push (
        new MenuItemMoveOp (res, old_submenu_idx, old_item_idx,
                            new_submenu_idx, new_item_idx));
    res->set_submenus (submenus);
    submenus_old = submenus;
    
    if (old_submenu_idx != new_submenu_idx)
    {
        row_deleted (drag_path);
        row_inserted (drop_path, get_iter (drop_path));
    } else {
        Path parent_path (1);
        parent_path[0] = old_submenu_idx;
        
        std::vector<item_idx_t> new_order (old_items.size ());
        for (item_idx_t i = 0; i != old_items.size (); ++i)
            new_order[i] = i;
        new_order.erase (new_order.begin () + old_item_idx);
        new_order.insert (new_order.begin () + new_item_idx, old_item_idx);
        
        rows_reordered (parent_path, get_iter (parent_path), new_order);
    }
}
    
bool MenuTreeModel::drag_data_get_vfunc (const Path &path, Gtk::SelectionData &selection_data) const
{
    g_return_val_if_fail (path.size () <= 2, false);

    drag_dragged = true;
    drag_path = path;
    
    selection_data.set (path.size () == 1 ? TARGET_SUBMENU : TARGET_MENUITEM, 8, 0, 0);    
    return true;
}

bool MenuTreeModel::drag_data_delete_vfunc (const Path &dragged_from_path)
{
    if (!drag_dropped)
        return true;
    
    drag_dropped = false;
    drag_dragged = false;

    g_return_val_if_fail (dragged_from_path.size () <= 2, false);
    
    if (dragged_from_path.size () == 1)
        move_submenu (drag_path[0], drop_path[0]);
    else
        move_item (drag_path[0], drag_path[1], drop_path[0], drop_path[1]);

    return true;
}

bool MenuTreeModel::drag_data_received_vfunc (const Path &dest, const Gtk::SelectionData &selection_data)
{
    g_return_val_if_fail (dest.size () < 4, false);
    g_return_val_if_fail (drag_dragged, false);

    drag_dropped = false;

    if (selection_data.get_data_type () == TARGET_SUBMENU)
    {
        if (dest[0] == drag_path[0])
            return true;
        
        submenu_idx_t target_submenu_idx;
        
        if ((dest.size () > 1 && drag_path[0] > dest[0]) || dest.size () == 1)
            target_submenu_idx = dest[0];
        else
            target_submenu_idx = dest[0] + 1;
        
        target_submenu_idx = std::min<submenu_idx_t> (target_submenu_idx, submenus.size ());
        if (target_submenu_idx > submenu_idx_t (drag_path[0]))
            --target_submenu_idx;
        
        if (submenu_idx_t (drag_path[0]) == target_submenu_idx)
            return true;
        
        drag_dropped = true;
        drop_path.clear ();
        drop_path.push_back (target_submenu_idx);

    } else if (selection_data.get_data_type () == TARGET_MENUITEM) {
        
        if (dest[0] == drag_path[0] && dest[1] == drag_path[1])
            return true;

        submenu_idx_t target_submenu_idx = dest[0];
        item_idx_t    target_item_idx = 0;

        if (dest.size () > 1)
            target_item_idx = dest[1];

        if (submenu_idx_t (drag_path[0]) == target_submenu_idx &&
            item_idx_t (drag_path[1]) < target_item_idx &&
            target_item_idx)
            --target_item_idx;
        
        if (target_submenu_idx == submenu_idx_t (drag_path[0]) &&
            target_item_idx == item_idx_t (drag_path[1]))
            return true;
        
        drag_dropped = true;
        drop_path.clear ();
        drop_path.push_back (target_submenu_idx);
        drop_path.push_back (target_item_idx);
        
    } else {
        g_warning ("Unknown Drag & Drop data '%s'", selection_data.get_data_type ().c_str ());
        return false;
    }

    return true;
}

bool MenuTreeModel::row_drop_possible_vfunc (const Path &dest, const Gtk::SelectionData &selection_data) const
{
    if (!drag_dragged)
        return false;
    
    if (selection_data.get_data_type () == TARGET_MENUITEM)
        return dest.size () > 1;

    if (selection_data.get_data_type () == TARGET_SUBMENU)
        return true;
    
    return false;
}
