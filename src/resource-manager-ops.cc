//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <glib/gi18n.h>
#include <glib.h>

#include "resource-manager-ops.h"
#include "io/xml-loader.h"
#include "io/xml-saver.h"

using namespace Guikachu::ResourceOps;

RemoveOp::RemoveOp (Resource *resource_):
    resource (resource_),
    manager (resource->get_manager ()),
    type (resource->get_type ()),
    id (resource->id),
    serial (resource->get_serial ())
{
    char *tmp = g_strdup_printf (_("Remove %s"), id.c_str ());
    label = tmp;
    g_free (tmp);
    
    IO::XML::ResourceSaver saver (node);
    resource->apply_visitor (saver);
}

Glib::ustring RemoveOp::get_label () const
{
    return label;
}

void RemoveOp::undo ()
{
    resource = manager->create_resource (type, id, false, serial);
    IO::XML::ResourceLoader loader (node);
    
    resource->apply_visitor (loader);
}

void RemoveOp::redo ()
{
    manager->remove_resource (resource);
}


CreateOp::CreateOp (Resource *resource):
    manager (resource->get_manager ()),
    type (resource->get_type ()),
    id (resource->id),
    serial (resource->get_serial ())
{
    char *tmp = g_strdup_printf (_("Create %s"), id.c_str ());
    label = tmp;
    g_free (tmp);
}

Glib::ustring CreateOp::get_label () const
{
    return label;
}

void CreateOp::undo ()
{
    manager->remove_resource (manager->get_resource (serial));
}

void CreateOp::redo ()
{
    manager->create_resource (type, id, false, serial);
}
