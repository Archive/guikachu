//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_MAINWIN_H
#define GUIKACHU_MAINWIN_H

#include <gtkmm/window.h>
#include <gtkmm/fileselection.h>
#include <gtkmm/statusbar.h>
#include <gtkmm/actiongroup.h>
#include <gtkmm/uimanager.h>

#include "app-win.h"

#include "resource-tree.h"
#include "resource.h"
#include "resource-manager.h"
#include "resource-tree.h"

namespace Guikachu
{
    namespace GUI
    {
	class MainWin: public Gtk::Window
	{
	    ResourceManager *manager;
	    
	    Glib::ustring uri;
	    bool          in_exit;

	    int recent_files_num;

	    ResourceTree resource_tree;

            Glib::RefPtr<Gtk::ActionGroup> main_group, recent_group;
            Gtk::UIManager::ui_merge_id recent_merge_id;
            
            Glib::RefPtr<Gtk::UIManager> ui_manager;
            Gtk::Statusbar  statusbar;            
            Gtk::Widget    *toolbar;
            
            enum {
                STATUSBAR_DEFAULT,
                STATUSBAR_MENU_TIP,
                STATUSBAR_TREESELECTION
            };

            AppWindow *app_win;

	public:
	    MainWin ();
            ~MainWin ();
	    void set_uri     (const Glib::ustring& uri);
	    void set_manager (ResourceManager *manager);
	    
	private:
            void create_menus ();
            
	    void update_recent_files ();
	    void update_undo ();
	    
	    void update_title ();
            void update_resize_grip (GdkEventWindowState *event);
            
            void tree_selection_changed_cb ();

	    bool on_delete_event (GdkEventAny *e);
            
            void tree_menu_cb (guint button, guint32 time);
            
	    void exit_cb ();
	    void add_cb ();
	    void new_cb ();
	    void load_cb ();
	    void save_cb ();
	    void save_as_cb ();
	    void export_cb ();

            void edit_res_cb ();
	    void undo_cb ();
	    void redo_cb ();
	    void cut_cb ();
	    void copy_cb ();
	    void paste_cb ();
            void duplicate_cb ();
            void remove_cb ();

            void activate_res_cb (Resource *res);
            void activate_app_cb ();

            void toggle_toolbar_cb ();
            void viewmode_cb ();
	    
	    void preferences_cb ();

	    void open (Glib::ustring uri);
	    
	    bool check_save ();

            void set_view_mode (bool grouped);
            void set_show_toolbar (bool show);
            
            void setup_statusbar_tooltip (const Glib::RefPtr<Gtk::Action> &action,
                                          Gtk::Widget                     *widget);
            void menuitem_focus_cb (const Glib::RefPtr<Gtk::Action> &action);
	};
    }
}

#endif /* !GUIKACHU_MAINWIN_H */
