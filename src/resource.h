//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_RESOURCE_H
#define GUIKACHU_RESOURCE_H

#include <string>

namespace Guikachu
{
    namespace Resources
    {
	enum Type
	{
	    RESOURCE_NONE,
	    RESOURCE_DIALOG,
	    RESOURCE_STRING,
	    RESOURCE_STRINGLIST,
	    RESOURCE_MENU,
	    RESOURCE_FORM,
	    RESOURCE_BLOB,
            RESOURCE_BITMAP,
	    RESOURCE_BITMAPFAMILY
	};
    }
    
    class Resource;

    typedef int serial_t;
}

#include "queued-signal.h"
#include "resource-manager.h"
#include "resource-visitor.h"
#include "property.h"

namespace Guikachu
{
    class Resource: public sigc::trackable
    {
    protected:
	Resource (ResourceManager   *manager,
		  const std::string &id,
		  serial_t           serial);

	ResourceManager *manager;
	serial_t         serial;
	
    public:
        typedef Resources::Type type_t;
        
	virtual ~Resource ();

	ResourceManager * get_manager () { return manager; };
	
	// Resource managment functions
	virtual Resources::Type get_type () const = 0;

	// Generic visitor hook
	virtual void apply_visitor (ResourceVisitor &visitor) = 0;
	
	// Notification
	sigc::signal0<void> deleted;
	QueuedSignal        changed;
	
	// Common properties
	ID id;
	serial_t get_serial () const { return serial; };
    };
}

#endif /* !GUIKACHU_RESOURCE_H */
