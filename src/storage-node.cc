//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "storage-node.h"

#include <libxml/xmlmemory.h>
#include <glib.h>
#include <string.h>
#include "gnome-i18n-cnp.h" // for get_language_list

#include "storage.h"

#include <glibmm/utility.h>

using namespace Guikachu;

StorageNode::StorageNode ():
    free_node (true),
    node_ptr (xmlNewNode (0, (xmlChar*) "")),
    parent_doc (0)
{
}

StorageNode::StorageNode (const std::string &name):
    free_node (true),
    node_ptr (xmlNewNode (0, (xmlChar*) name.c_str ())),
    parent_doc (0)
{
}

StorageNode::StorageNode (const StorageNode &orig_node):
    free_node (orig_node.free_node),
    node_ptr (orig_node.node_ptr),
    parent_doc (orig_node.parent_doc)
{
    orig_node.free_node = false;
}

StorageNode & StorageNode::operator= (const StorageNode &orig_node)
{
    free_node = orig_node.free_node;
    node_ptr = orig_node.node_ptr;
    parent_doc = orig_node.parent_doc;
    
    orig_node.free_node = false;
    return *this;
}

StorageNode::~StorageNode ()
{
    if (free_node)
	xmlFree (node_ptr);
}

StorageNode::StorageNode (xmlNodePtr node, Storage *parent_doc_):
    free_node (false),
    node_ptr (node),
    parent_doc (parent_doc_)
{   
}

StorageNode StorageNode::children () const
{
    xmlNodePtr child_node;
    for (child_node = node_ptr->xmlChildrenNode; child_node && child_node->type != XML_ELEMENT_NODE;
	 child_node = child_node->next);
    
    return StorageNode (child_node, parent_doc);
}

StorageNode StorageNode::operator++ (int)
{
    StorageNode tmp = *this;
    node_ptr = next ().c_node ();
    return tmp;
}

StorageNode& StorageNode::operator++ ()
{
    node_ptr = next ().c_node ();
    return *this;
}

std::string StorageNode::name () const
{
    if (!node_ptr->name)
	return "";
    
    return (char*)(node_ptr->name);
}

StorageNode StorageNode::add_node (const std::string &name,
				   const std::string &content)
{
    g_assert (node_ptr != 0);
    
    xmlNodePtr new_node_ptr = 0;

    if (content != "")
	new_node_ptr = xmlNewChild (node_ptr, 0,
				    (xmlChar*)name.c_str (),
				    (xmlChar*)content.c_str ());
    else
	new_node_ptr = xmlNewChild (node_ptr, 0,
				    (xmlChar*)name.c_str (), 0);

    return StorageNode (new_node_ptr, parent_doc);
}

void StorageNode::add_node (StorageNode node)
{
    g_assert (node_ptr != 0);
    g_assert (node.node_ptr != 0);

    xmlAddChild (node_ptr, node.node_ptr);
    node.free_node = false;
}

void StorageNode::set_content (const std::string &content)
{
    g_assert (node_ptr != 0);

    xmlNodeSetContent (node_ptr, (xmlChar*) content.c_str ());
}

Glib::ustring StorageNode::get_content_raw () const
{
    g_assert (node_ptr != 0);
    
    std::string ret;
    char *tmp = (char*)xmlNodeGetContent (node_ptr);
    if (tmp)
    {
	ret = tmp;
	g_free (tmp);
    } else {
	return "";
    }

    return ret;
}

namespace
{
    std::pair<bool, std::string> escape_string (const Glib::ustring &raw_str)
    {
	std::string converted_str;
	bool converted = false;

	converted_str.reserve (raw_str.length ());
	
	for (Glib::ustring::const_iterator i = raw_str.begin (); i != raw_str.end (); ++i)
	{
	    if ((signed char)*i < 0)
	    {
		converted = true;
		
		Glib::ScopedPtr<char> tmp (g_strdup_printf ("\\x%X", *i));
		converted_str += tmp.get ();
	    } else {
		converted_str += char (*i);
	    }
	}

	return std::make_pair (converted, converted_str);
    }
}

std::string StorageNode::get_content () const
{
    Glib::ustring raw = get_content_raw ();

    std::pair<bool, std::string> conversion_status = escape_string (raw);
    
    if (conversion_status.first && parent_doc)
	parent_doc->encoding_notify.emit ();
    
    return conversion_status.second;
}


void StorageNode::set_prop (const std::string &prop_name,
			    const std::string &value)
{
    g_assert (node_ptr != 0);

    xmlSetProp (node_ptr,
		(xmlChar*) prop_name.c_str (),
		(xmlChar*) value.c_str ());
}

void StorageNode::set_prop (const std::string &prop_name,
			    unsigned int       value)
{
    g_assert (node_ptr != 0);

    Glib::ScopedPtr<char> tmp (g_strdup_printf ("%d", value));
    set_prop (prop_name, tmp.get ());
}

void StorageNode::set_prop (const std::string &prop_name,
			    int                value)
{
    g_assert (node_ptr != 0);
    
    Glib::ScopedPtr<char> tmp (g_strdup_printf ("%d", value));
    set_prop (prop_name, tmp.get ());
}

void StorageNode::set_prop (const std::string &prop_name,
			    char               value)
{
    g_assert (node_ptr != 0);

    if (!value)
	return;
    
    static char *buffer = g_new0 (char, 2);

    buffer[0] = value;
    set_prop (prop_name, buffer);
}

void StorageNode::set_prop_bool (const std::string &prop_name,
                                 bool               value)
{
    set_prop (prop_name, value ? "yes" : "no");
}

Glib::ustring StorageNode::get_prop_string_raw (const std::string &prop_name) const
{
    g_assert (node_ptr != 0);

    Glib::ustring ret;
    
    xmlChar *xml_tmp = xmlGetProp (node_ptr, (xmlChar*) prop_name.c_str ());

    if (xml_tmp)
    {
	ret = (char*) xml_tmp;
	xmlFree (xml_tmp);
    }
    
    return ret;
}

std::string StorageNode::get_prop_string (const std::string &prop_name) const
{
    Glib::ustring raw = get_prop_string_raw (prop_name);

    std::pair<bool, std::string> convert_status = escape_string (raw);

    if (convert_status.first && parent_doc)
	parent_doc->encoding_notify.emit ();
    
    return convert_status.second;
}

int StorageNode::get_prop_int (const std::string &prop_name) const
{
    g_assert (node_ptr != 0);

    int ret;

    xmlChar *xml_tmp = xmlGetProp (node_ptr, (xmlChar*) prop_name.c_str ());
    if (xml_tmp)
    {
	ret = atoi ((char*) xml_tmp);
	xmlFree (xml_tmp);
    } else {
	ret = 0;
    }

    return ret;
}

char StorageNode::get_prop_char (const std::string &prop_name) const
{
    g_assert (node_ptr != 0);

    char ret = 0;

    xmlChar *xml_tmp = xmlGetProp (node_ptr, (xmlChar*) prop_name.c_str ());
    if (xml_tmp)
    {
	if (strlen ((char*) xml_tmp))
	    ret = (char) xml_tmp[0];
	
	xmlFree (xml_tmp);
    }

    return ret;
}

bool StorageNode::get_prop_bool (const std::string &prop_name) const
{
    std::string value = get_prop_string (prop_name);
    return value == "yes";
}

StorageNode StorageNode::next () const
{
    xmlNodePtr next_node;
    for (next_node = node_ptr->next; next_node && next_node->type != XML_ELEMENT_NODE;
	 next_node = next_node->next);
    
    return StorageNode (next_node, parent_doc);
}

StorageNode StorageNode::get_translated_child (const std::string &name) const
{
    // This one is lifted from GAL. It's pretty smart, actually.
    
    StorageNode best_node (0, parent_doc);

    const std::list<std::string> lang_list = get_language_list ("LC_MESSAGES");
    
    int best_score = INT_MAX;
    
    for (StorageNode node = children (); node; node++)
    {
	if (node.name () != name)
	    continue;

	char *lang = (char*)xmlNodeGetLang (node.c_node ());

	if (lang != 0)
	{
	    int score;

	    std::list<std::string>::const_iterator i;
	    for (i = lang_list.begin (), score = 0;
		 i != lang_list.end () && score < best_score;
		 ++i, ++best_score)
	    {
		if (*i == lang)
		{
		    best_node = node;
		    best_score = score;
		}
	    }
	} else {
	    if (!best_node)
		best_node = node;
	}

	xmlFree (lang);

	if (best_score == 0) // It won't get better than that
	    return best_node;
    }

    return best_node;
}
