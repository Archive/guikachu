//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "resource-util.h"

#include <glib/gi18n.h>

using namespace Guikachu;

std::string Resources::prefix_from_type (Resources::Type type)
{
    std::string type_id;
    
    switch (type)
    {
    case Resources::RESOURCE_STRING:
	type_id = "STRING";
	break;
    case Resources::RESOURCE_STRINGLIST:
	type_id = "STRINGLIST";
	break;
    case Resources::RESOURCE_DIALOG:
	type_id = "DIALOG";
	break;
    case Resources::RESOURCE_MENU:
	type_id = "MENU";
	break;
    case Resources::RESOURCE_FORM:
	type_id = "FORM";
	break;
    case Resources::RESOURCE_BITMAP:
        type_id = "BITMAP";
        break;
    case Resources::RESOURCE_BITMAPFAMILY:
        type_id = "BITMAPGROUP";
        break;
    case Resources::RESOURCE_BLOB:
	type_id = "BLOB";
	break;
    case Resources::RESOURCE_NONE:
	g_assert_not_reached ();
	break;
    }

    return type_id;
}

std::string Resources::type_id_from_type (Resources::Type type)
{
    std::string type_id;
    
    switch (type)
    {
    case Resources::RESOURCE_STRING:
	type_id = "string";
	break;
    case Resources::RESOURCE_STRINGLIST:
	type_id = "stringlist";
	break;
    case Resources::RESOURCE_DIALOG:
	type_id = "dialog";
	break;
    case Resources::RESOURCE_MENU:
	type_id = "menu";
	break;
    case Resources::RESOURCE_FORM:
	type_id = "form";
	break;
    case Resources::RESOURCE_BITMAP:
        type_id = "bitmap";
        break;
    case Resources::RESOURCE_BITMAPFAMILY:
        type_id = "bitmapfamily";
        break;
    case Resources::RESOURCE_BLOB:
	type_id = "blob";
	break;
    case Resources::RESOURCE_NONE:
	g_assert_not_reached ();
	break;
    }

    return type_id;
}

Resources::Type Resources::type_from_type_id (const std::string &type_id)
{
    Resources::Type type;

    if (type_id == "blob")
	type = Resources::RESOURCE_BLOB;
    else if (type_id == "dialog")
	type = Resources::RESOURCE_DIALOG;
    else if (type_id == "form")
	type = Resources::RESOURCE_FORM;
    else if (type_id == "menu")
	type = Resources::RESOURCE_MENU;
    else if (type_id == "string")
	type = Resources::RESOURCE_STRING;
    else if (type_id == "stringlist")
	type = Resources::RESOURCE_STRINGLIST;
    else if (type_id == "bitmap")
        type = Resources::RESOURCE_BITMAP;
    else if (type_id == "bitmapfamily")
        type = Resources::RESOURCE_BITMAPFAMILY;
    else
	type = Resources::RESOURCE_NONE;

    return type;
}

Glib::ustring Resources::display_name_from_type (Resources::Type type)
{
    Glib::ustring label;
    
    switch (type)
    {
    case Resources::RESOURCE_STRING:
	label = _("String");
	break;
    case Resources::RESOURCE_STRINGLIST:
	label = _("String list");
	break;
    case Resources::RESOURCE_DIALOG:
	label = _("Dialog");
	break;
    case Resources::RESOURCE_MENU:
	label = _("Menu");
	break;
    case Resources::RESOURCE_FORM:
	label = _("Form");
	break;
    case Resources::RESOURCE_BITMAP:
        label = _("Bitmap");
        break;
    case Resources::RESOURCE_BITMAPFAMILY:
        label = _("Bitmap Group");
        break;
    case Resources::RESOURCE_BLOB:
	label = _("RCP Code Fragment");
	break;
    case Resources::RESOURCE_NONE:
	g_assert_not_reached ();
	break;
    }

    return label;
}
