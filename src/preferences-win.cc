//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "preferences-win.h"
#include "preferences.h"

#include "ui-gui.h"

#include <glib/gi18n.h>

#include <gtkmm/adjustment.h>

#include <sigc++/adaptors/bind.h>
#include <sigc++/adaptors/hide.h>

namespace {
    std::string color_to_string (const Gdk::Color &color);
}

using namespace Guikachu;

GUI::PreferencesWin::PreferencesWin ()
{
    Glib::RefPtr<Gnome::Glade::Xml> gui = UI::glade_gui ("preferences");
    Gtk::Button *ok_btn, *cancel_btn;
    
    gui->get_widget ("preferences",         prop_box);
    gui->get_widget ("pref_color_fg",       color_fg);
    gui->get_widget ("pref_color_disabled", color_disabled);
    gui->get_widget ("pref_color_bg",       color_bg);
    gui->get_widget ("pref_color_sel",      color_selection);
    gui->get_widget ("pref_zoom",           spin_zoom);
    gui->get_widget ("pref_apply",          apply_btn);
    gui->get_widget ("pref_ok",             ok_btn);
    gui->get_widget ("pref_cancel",         cancel_btn);
    
    prop_box->set_title (_("Preferences"));

    // Set up 'changed' signals so that the 'Apply' button is made sensitive
    color_fg->signal_color_set ().connect (
	sigc::mem_fun (*this, &PreferencesWin::color_changed_cb));
    color_disabled->signal_color_set ().connect (
	sigc::mem_fun (*this, &PreferencesWin::color_changed_cb));
    color_bg->signal_color_set ().connect (
	sigc::mem_fun (*this, &PreferencesWin::color_changed_cb));
    color_selection->signal_color_set ().connect (
	sigc::mem_fun (*this, &PreferencesWin::color_changed_cb));
    
    spin_zoom->get_adjustment ()->signal_value_changed ().connect (
	bind (sigc::mem_fun (*this, &PreferencesWin::set_modified), true));
    
    // Update controls when window's shown
    prop_box->signal_show ().connect (sigc::mem_fun (*this, &PreferencesWin::update));
    
    // Set up buttons
    apply_btn->signal_clicked ().connect (sigc::mem_fun (*this, &PreferencesWin::apply_cb));
    ok_btn->signal_clicked ().connect (sigc::mem_fun (*this, &PreferencesWin::apply_cb));
    ok_btn->signal_clicked ().connect (sigc::mem_fun (*prop_box, &Gtk::Dialog::hide));
    cancel_btn->signal_clicked ().connect (sigc::mem_fun (*prop_box, &Gtk::Dialog::hide));
}

void GUI::PreferencesWin::color_changed_cb ()
{
    set_modified (true);
}

void GUI::PreferencesWin::set_modified (bool modified)
{
    apply_btn->set_sensitive (modified);
}

void GUI::PreferencesWin::run ()
{
    prop_box->show ();
}

void GUI::PreferencesWin::update ()
{
    // Update color pickers
    color_fg->set_color (Gdk::Color (Preferences::FormEditor::get_color_fg ()));
    color_disabled->set_color (Gdk::Color (Preferences::FormEditor::get_color_disabled ()));
    color_bg->set_color (Gdk::Color (Preferences::FormEditor::get_color_bg ()));
    color_selection->set_color (Gdk::Color (Preferences::FormEditor::get_color_selection ()));

    // Update default zoom entry
    spin_zoom->get_adjustment ()->set_value (Preferences::FormEditor::get_default_zoom ());

    // Reset property box state
    set_modified (false);
}

void GUI::PreferencesWin::apply_cb ()
{
    // Set form editor colors
    Preferences::FormEditor::set_colors (color_to_string (color_fg->get_color ()),
                                         color_to_string (color_disabled->get_color ()),
                                         color_to_string (color_bg->get_color ()),
                                         color_to_string (color_selection->get_color ()));
    
    // Set default form editor zoom factor
    Preferences::FormEditor::set_default_zoom (spin_zoom->get_value ());

    set_modified (false);
}

namespace {
    
    // This converts to "#rrggbb" format
    std::string color_to_string (const Gdk::Color &color)
    {
        unsigned char r = 0xff & (color.get_red () >> 8);
        unsigned char g = 0xff & (color.get_green () >> 8);
        unsigned char b = 0xff & (color.get_blue () >> 8);

        char buf[8];
        snprintf (buf, 8, "#%02x%02x%02x", r, g, b);

        return std::string (buf);
    }

} // Anonymous namespace
