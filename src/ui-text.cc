//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <glib/gi18n.h>

#include "ui.h"

#include <iostream>

using namespace Guikachu;

void UI::flush_events ()
{
    // Do nothing
}

void UI::show_error (const Glib::ustring &full_message,
		     const Glib::ustring &short_message)
{
    if (short_message != "")
	std::cerr << _("Guikachu: Error: ") << short_message << std::endl;
}

void UI::show_warning (const Glib::ustring &full_message,
		       const Glib::ustring &short_message)
{
    if (short_message != "")
	std::cerr << _("Guikachu: Warning: ") << short_message << std::endl;
}

void UI::show_error_io_open (const Glib::ustring &uri, const Glib::ustring &error_msg)
{
    show_error (uri + ":" + error_msg);
}

void UI::show_error_io_save (const Glib::ustring &uri, const Glib::ustring &error_msg)
{
    show_error (uri + ":" + error_msg);
}

void UI::show_error_io_load_resource (const Glib::ustring &uri, const std::string &id,
                                      const Glib::ustring &error_msg)
{
    show_error (id + ":" + uri + ":" + error_msg);
}

void UI::show_error_io_save_resource (const Glib::ustring &uri, const std::string &id,
                                      const Glib::ustring &error_msg)
{
    show_error (id + ":" + uri + ":" + error_msg);
}
