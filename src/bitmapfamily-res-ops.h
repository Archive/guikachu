//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_BITMAPFAMILY_RES_OPS_H
#define GUIKACHU_BITMAPFAMILY_RES_OPS_H

#include "undo.h"
#include "bitmapfamily-res.h"

namespace Guikachu
{
    namespace ResourceOps
    {
	namespace BitmapFamilyOps
	{
            class BitmapFamilyHolder
            {
                ResourceManager *manager;
                serial_t         serial;

            protected:
                BitmapFamilyHolder (Resources::BitmapFamily *res);

                Resources::BitmapFamily * get_resource () const;
            };
            
            class ImageChangeOp: private BitmapFamilyHolder,
                                 public UndoOp
            {
                Glib::ustring op_label;

                Resources::Bitmap::BitmapType type;
                Resources::Bitmap::ImageData old_image, new_image;
                
            public:
		ImageChangeOp (Resources::BitmapFamily            *res,
                               Resources::Bitmap::BitmapType       type,
                               const Resources::Bitmap::ImageData &old_image);
		virtual ~ImageChangeOp () {};
		
		void undo ();
		void redo ();
		Glib::ustring get_label () const { return op_label; };
            };

            class ImageRemoveOp: private BitmapFamilyHolder,
                                 public UndoOp
            {
                Glib::ustring op_label;

                Resources::Bitmap::BitmapType type;
                Resources::Bitmap::ImageData  old_image;
                
            public:
		ImageRemoveOp (Resources::BitmapFamily       *res,
                               Resources::Bitmap::BitmapType  type);
		virtual ~ImageRemoveOp () {};
		
		void undo ();
		void redo ();
		Glib::ustring get_label () const { return op_label; };
            };
            
	} // namespace BitmapFamilyOps
    } // namespace ResourceOps
} // namespace Guikachu

#endif /* !GUIKACHU_BITMAPFAMILY_RES_H */
