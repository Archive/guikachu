//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "stringlist-win.h"

#include <glib/gi18n.h>

#include <gtkmm/box.h>

#include "property-ops-resource.h"

#include "widgets/propertytable.h"
#include "widgets/entry.h"
#include "widgets/string-list.h"
#include "widgets/string-list-ops.h"

using namespace Guikachu;

GUI::StringListWindow::StringListWindow (Resources::StringList *res_):
    res (res_),
    window (Gtk::WINDOW_TOPLEVEL)
{
    using ResourceOps::PropChangeOpFactory;
    using ResourceOps::RenameOpFactory;
    using ResourceOps::StringListOpFactory;
    
    window.signal_delete_event ().connect (sigc::mem_fun (*this, &StringListWindow::delete_event_impl));
    window.property_allow_grow () = true;

    Gtk::Box *vbox = new Gtk::VBox;
    
    GUI::PropertyTable *proptable = new GUI::PropertyTable ();
    Gtk::Widget *control;

    /* Resource ID */
    control = new GUI::PropertyEditors::Entry (false, res->id, new RenameOpFactory (res));
    proptable->add (_("Resource _ID:"), *manage (control));

    /* Prefix */
    control = new GUI::PropertyEditors::Entry (
	true, res->prefix,
	new PropChangeOpFactory<std::string> (_("Change prefix of %s"), res, res->prefix, true));
    proptable->add (_("_Prefix:"), *manage (control));

    proptable->add_separator ();
    vbox->pack_start (*manage (proptable), false, false);
    
    /* String list */
    control = new GUI::PropertyEditors::StringList (
	res->strings,
	new StringListOpFactory (_("Add \"%s\" to %s"),
				 _("Remove \"%s\" from %s"),
				 _("Change \"%s\" in %s"),
				 _("Move item \"%s\" in %s"),
				 res,
				 res->strings));
    vbox->add (*manage (control));
    
    window.add (*manage (vbox));
    
    res->changed.connect (sigc::mem_fun (*this, &StringListWindow::update));
    update ();
}

bool GUI::StringListWindow::delete_event_impl (GdkEventAny *e)
{
    window.hide ();
    return true;
}

void GUI::StringListWindow::show ()
{
    window.show_all ();
    window.raise ();
}

void GUI::StringListWindow::update ()
{
    window.set_title (Glib::ScopedPtr<char> (
	g_strdup_printf (_("String list: %s"), res->id ().c_str ())).get ());
}
