//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "pushbutton-canvas.h"

#include "form-editor.h"
#include "form-editor-canvas.h"
#include "resizeable-ops.h"

using namespace Guikachu::GUI::FormEditor;

PushButtonCanvasItem::PushButtonCanvasItem (Widgets::PushButton *widget_,
					    FooCanvasmm::Group  &parent_group):
    CanvasItem (widget_, parent_group),
    WidgetCanvasItem (widget_, parent_group),
    ResizeableCanvasItem (widget_, parent_group, new WidgetOps::ResizeOpFactory (widget_)),
    widget (widget_)
{
    widget->bitmap_id.resource_changed.connect (sigc::mem_fun (*this, &PushButtonCanvasItem::update));
}

void PushButtonCanvasItem::draw (FooCanvasmm::Group &group) const
{
    /* Don't ask about the +1, I have no idea... */
    int x1 = widget->x - 1;
    int x2 = x1 + widget->get_width () + 1;
    int y1 = widget->y - 1;
    int y2 = y1 + widget->get_height () + 1;

    /* A Polygon is used instead of a Rectangle to get mouse events
     * inside the "hollow" area
     */
    using namespace FooCanvasmm::Properties;

    *(FormEditor::draw_rectangle (group, x1, y1, x2, y2))
	<< width_units (1)
	<< outline_color (get_foreground_color (widget->usable));

    int text_x = x1 + 1;
    int text_y = y1 + 1;

    if (widget->bitmap_id == "" && widget->selected_bitmap_id == "")
	draw_text (group,
		   widget->text, get_font (widget->font),
		   get_foreground_color (widget->usable),
		   text_x, text_y,
                   widget->get_width (), widget->get_height ());
    else
        draw_bitmapref (group,
                        widget->bitmap_id, widget->get_manager ()->get_target ()->screen_color,
                        get_background_color (),
                        widget->x , widget->y,
                        widget->get_width (), widget->get_height (),
                        Gtk::ANCHOR_CENTER);
}

void PushButtonCanvasItem::get_bounds (int &x1, int &y1,
				       int &x2, int &y2)
{
    x1 = widget->x + 1;
    x2 = x1 + widget->get_width () - 3;
    y1 = widget->y + 1;
    y2 = y1 + widget->get_height () - 1;
}
