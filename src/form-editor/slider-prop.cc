//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "slider-prop.h"

#include <glib/gi18n.h>

#include "widgets/num-entry.h"
#include "widgets/togglebutton.h"
#include "widgets/propertytable.h"
#include "widgets/resource-combo.h"
#include <gtkmm/menu.h>

#include "auto-resizeable-prop.h"
#include "usable-prop.h"
#include "resizeable-prop.h"
#include "disabled-prop.h"

#include "property-ops-widget.h"

using namespace Guikachu::GUI::FormEditor;
using namespace Guikachu::Widgets;

SliderProperties::SliderProperties (Slider *res_):
    WidgetProperties (res_),
    res (res_)
{
    using Gtk::manage;
    
    using WidgetOps::PropChangeOpFactory;
    
    GUI::PropertyTable *proptable = new GUI::PropertyTable ();
    Gtk::Widget *control;

    /* Resource ID */
    add_general_controls (*proptable);

    /* Current value */
    control = value = new GUI::PropertyEditors::NumEntry (
	0, 32767, res->value,
	new PropChangeOpFactory<int> (_("Change value of %s"), res, res->value, true));
    proptable->add (_("_Value:"), *manage (control),
		    _("Default value of slider"));

    /* Max and min value */
    control = min_value = new GUI::PropertyEditors::NumEntry (
	0, 32767, res->min_value,
	new PropChangeOpFactory<int> (_("Change minimum value of %s"), res, res->min_value, true));
    proptable->add (_("Mi_nimum:"), *manage (control),
		    _("Minimum value allowed"));

    control = max_value = new GUI::PropertyEditors::NumEntry (
	0, 32767, res->max_value,
	new PropChangeOpFactory<int> (_("Change maximum value of %s"), res, res->max_value, true));
    proptable->add (_("Ma_ximum:"), *manage (control),
		    _("Maximum value allowed"));

    /* Page size */
    control = new GUI::PropertyEditors::NumEntry (
	0, 32767, res->page_size,
	new PropChangeOpFactory<int> (_("Change page size of %s"), res, res->page_size, true));
    proptable->add (_("_Page size:"), *manage (control),
		    _("The amount of slider movement when the "
		      "slider itself is clicked by the user"));

    /* Bitmaps */
    std::set<Resources::Type> bitmap_types;
    bitmap_types.insert (Resources::RESOURCE_BITMAP);
    bitmap_types.insert (Resources::RESOURCE_BITMAPFAMILY);

    control = new GUI::PropertyEditors::ResourceCombo (
	bitmap_types, res->thumb_id,
	new PropChangeOpFactory<std::string> (
	    _("Change thumb bitmap of %s"), res, res->thumb_id));
    proptable->add (_("_Thumb bitmap:"), *manage (control),
                   _("Select the image to use for the slider thumb"));

    control = new GUI::PropertyEditors::ResourceCombo (
	bitmap_types, res->background_id,
	new PropChangeOpFactory<std::string> (
	    _("Change background bitmap of %s"), res, res->background_id));
    proptable->add (_("_Background bitmap:"), *manage (control),
                   _("Select the image to use for the slider background"));

    /* Feedback */
    control = new GUI::PropertyEditors::ToggleButton (
	res->feedback,
	new PropChangeOpFactory<bool> (_("Toggle feedback behavior of %s"), res, res->feedback, false));
    proptable->add (_("_Feedback:"), *manage (control),
		    _("Feedback sliders will report values to the program while the user is still changing the value"));

    /* Usable */
    UsableProperties::add_controls (res, *proptable);
    DisabledProperties::add_controls (res, *proptable);
    
    /* Separator */
    proptable->add_separator ();

    /* Position */
    add_position_controls (*proptable);
    
    /* Size */
    ResizeableProperties::add_width_controls (res, *proptable);
    AutoHeightProperties::add_controls (res, *proptable);
    
    proptable->show ();
    property_editor = proptable;

    res->changed.connect (sigc::mem_fun (*this, &SliderProperties::update));
    update ();
}

SliderProperties::~SliderProperties ()
{
    delete property_editor;
}

void SliderProperties::update ()
{
    value->set_min (res->min_value);
    value->set_max (res->max_value);
}

Gtk::Widget* SliderProperties::get_editor ()
{
    return property_editor;
}
