//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_WIDGET_OPS_H
#define GUIKACHU_FORM_EDITOR_WIDGET_OPS_H

#include "resource-manager.h"
#include "widget.h"
#include "storage-node.h"

namespace Guikachu
{
    namespace WidgetOps
    {
	class RemoveOp: public UndoOp
	{
	    ResourceManager *manager;
	
	    serial_t      form_serial, widget_serial;

	    Widgets::Type type;
	    std::string   widget_id;
	    StorageNode   node;
	
	    Glib::ustring   label;
	
	public:
	    RemoveOp (Widget *widget);
	    virtual ~RemoveOp () {};
	
	    Glib::ustring get_label () const;
	
	    void undo ();
	    void redo ();
	};

	class CreateOp: public UndoOp
	{
	    ResourceManager *manager;

	    serial_t         form_serial, widget_serial;
	
	    Widgets::Type    type;
	    std::string      widget_id;
	    int              x, y;

	    Glib::ustring    label;
	
	public:
	    CreateOp (Widget *widget);
	    virtual ~CreateOp () {};
	
	    Glib::ustring get_label () const;
	
	    void undo ();
	    void redo ();	
	};

	class MultiMoveOp: public UndoOp
	{
	    Glib::ustring label;
	    
	    ResourceManager *manager;
	    
	    serial_t           form_serial;
	    std::set<serial_t> widget_serials;
	    
	    int dx, dy;
	    
	public:
	    MultiMoveOp (const std::set<Widget*> &widgets, int dx, int dy);
	    virtual ~MultiMoveOp () {};
	    
	    void undo ();
	    void redo ();
	    Glib::ustring get_label () const { return label; };
	};

	
	class MultiRemoveOp: public UndoOp
	{
	    typedef RemoveOp             real_op_t;
	    typedef std::set<real_op_t*> real_op_list_t;
		
	    real_op_list_t real_ops;
	    Glib::ustring label;
	    
	public:
	    MultiRemoveOp (const std::set<Widget*> &widgets);
	    virtual ~MultiRemoveOp ();
	    
	    void undo ();
	    void redo ();
	    Glib::ustring get_label () const { return label; };
	};

    } // namespace WidgetOps
} // namespace Guikachu

#endif /* !GUIKACHU_FORM_EDITOR_WIDGET_OPS_H */
