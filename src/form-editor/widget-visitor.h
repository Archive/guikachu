//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_WIDGET_VISITOR_H
#define GUIKACHU_WIDGET_VISITOR_H

namespace Guikachu
{
    namespace Widgets
    {
	class Label;
	class Button;
	class PushButton;
	class Graffiti;
	class SelectorTrigger;
	class Checkbox;
	class List;
	class PopupTrigger;
	class ScrollBar;
	class Slider;
	class TextField;
	class Table;
        class FormBitmap;
	class Gadget;
    }
    
    class WidgetVisitor
    {
    public:
	virtual ~WidgetVisitor () {};
	
	virtual void visit_widget (Widgets::Label           *widget) = 0;
	virtual void visit_widget (Widgets::Button          *widget) = 0;
	virtual void visit_widget (Widgets::PushButton      *widget) = 0;
	virtual void visit_widget (Widgets::Graffiti        *widget) = 0;
	virtual void visit_widget (Widgets::SelectorTrigger *widget) = 0;
	virtual void visit_widget (Widgets::Checkbox        *widget) = 0;
	virtual void visit_widget (Widgets::List            *widget) = 0;
	virtual void visit_widget (Widgets::PopupTrigger    *widget) = 0;
	virtual void visit_widget (Widgets::ScrollBar       *widget) = 0;
	virtual void visit_widget (Widgets::Slider          *widget) = 0;
	virtual void visit_widget (Widgets::TextField       *widget) = 0;
	virtual void visit_widget (Widgets::Table           *widget) = 0;
	virtual void visit_widget (Widgets::FormBitmap      *widget) = 0;
	virtual void visit_widget (Widgets::Gadget          *widget) = 0;
    };
}

#endif /* !GUIKACHU_WIDGET_VISITOR_H */
