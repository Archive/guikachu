//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_FORM_H
#define GUIKACHU_FORM_EDITOR_FORM_H

namespace Guikachu
{
    namespace Widgets
    {
	class Form;
    }
}

#include <string>
#include <sigc++/trackable.h>

#include "form-res.h"
#include "form-editor/form-element.h"
#include "form-editor/resizeable.h"
#include "form-editor/form-canvas.h"
#include "form-editor/form-prop.h"

namespace Guikachu
{
    namespace Widgets
    {
	class Form: public Resizeable,
		    public virtual sigc::trackable
	{		
	    typedef std::map<FooCanvasmm::Group *,
		GUI::FormEditor::FormCanvasItem*> canvas_item_map_t;
	    
	    Resources::Form *res;
	    
	    mutable GUI::FormEditor::FormProperties *editor;
	    mutable canvas_item_map_t                canvas_items;
	    
	public:
	    Form (Resources::Form *res);
	    ~Form ();
	    
	    // FormElement methods
	    int get_x () const { return res->x; };
	    int get_y () const { return res->y; };
	    void set_x (int x) { res->x = x; };
	    void set_y (int y) { res->y = y; };

	    int get_width  () const { return res->width;  };
	    int get_height () const { return res->height; };    

	    ResourceManager* get_manager () const { return res->get_manager (); };

	    Gtk::Widget                     *get_editor      ();
	    GUI::FormEditor::FormCanvasItem *get_canvas_item (FooCanvasmm::Group &parent_group);

	    Resources::Form *get_resource () const { return res; };
	};
    }
}

#endif /* !GUIKACHU_FORM_EDITOR_FORM_H */
