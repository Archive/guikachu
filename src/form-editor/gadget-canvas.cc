//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "gadget-canvas.h"

#include "form-editor.h"
#include "form-editor-canvas.h"
#include "resizeable-ops.h"

using namespace Guikachu::GUI::FormEditor;

GadgetCanvasItem::GadgetCanvasItem (Widgets::Gadget    *widget_,
				    FooCanvasmm::Group &parent_group):
    CanvasItem (widget_, parent_group),
    WidgetCanvasItem (widget_, parent_group),
    ResizeableCanvasItem (widget_, parent_group, new WidgetOps::ResizeOpFactory (widget_)),
    widget (widget_)
{
}

void GadgetCanvasItem::draw (FooCanvasmm::Group &group) const
{
    int x1 = widget->x;
    int x2 = x1 + widget->width;
    int y1 = widget->y;
    int y2 = y1 + widget->height;
    
    static const char stipple_bits[] = { 0x04, 0x02, 0x01, };
    static const Glib::RefPtr<Gdk::Bitmap> stipple =
	Gdk::Bitmap::create (stipple_bits, 3, 3);
    
    using namespace FooCanvasmm::Properties;
    *(draw_rectangle (group, x1, y1, x2, y2))
	<< fill_stipple (stipple)
	<< fill_color (get_foreground_color (widget->usable));
}
