//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "scrollbar-prop.h"

#include <glib/gi18n.h>

#include "widgets/num-entry.h"
#include "widgets/propertytable.h"

#include "usable-prop.h"
#include "resizeable-prop.h"
#include "auto-resizeable-prop.h"

#include "property-ops-widget.h"

using namespace Guikachu::GUI::FormEditor;
using namespace Guikachu::Widgets;

ScrollBarProperties::ScrollBarProperties (ScrollBar *res_):
    WidgetProperties (res_),
    res (res_)
{
    using WidgetOps::PropChangeOpFactory;
    
    GUI::PropertyTable *proptable = new GUI::PropertyTable ();
    Gtk::Widget *control;
    
    /* Resource ID */
    add_general_controls (*proptable);

    /* Current value */
    control = value = new GUI::PropertyEditors::NumEntry (
	0, 32767, res->value,
	new PropChangeOpFactory<int> (_("Change value of %s"), res, res->value, true));
    proptable->add (_("_Value:"), *manage (control),
		    _("Default value of scrollbar"));

    /* Max and min value */
    control = min_value = new GUI::PropertyEditors::NumEntry (
	0, 32767, res->min_value,
	new PropChangeOpFactory<int> (_("Change minimum value of %s"), res, res->min_value, true));
    proptable->add (_("Mi_nimum:"), *manage (control),
		    _("Minimum value allowed"));

    control = max_value = new GUI::PropertyEditors::NumEntry (
	0, 32767, res->max_value,
	new PropChangeOpFactory<int> (_("Change maximum value of %s"), res, res->max_value, true));
    proptable->add (_("Ma_ximum:"), *manage (control),
		    _("Maximum value allowed"));    

    /* Page size */
    control = new GUI::PropertyEditors::NumEntry (
	0, 32767, res->page_size,
	new PropChangeOpFactory<int> (_("Change page size of %s"), res, res->page_size, true));
    proptable->add (_("_Page size:"), *manage (control),
		    _("The amount of slider movement when the "
		      "scrollbar itself is clicked by the user"));
    
    /* Usable */
    UsableProperties::add_controls (res, *proptable);

    /* Separator */
    proptable->add_separator ();

    /* Position */
    add_position_controls (*proptable);
    
    /* Size */
    AutoWidthProperties::add_controls (res, *proptable);
    ResizeableProperties::add_height_controls (res, *proptable);
    
    proptable->show ();
    property_editor = proptable;

    res->changed.connect (sigc::mem_fun (*this, &ScrollBarProperties::update));
}

ScrollBarProperties::~ScrollBarProperties ()
{
    delete property_editor;
}

Gtk::Widget* ScrollBarProperties::get_editor ()
{
    return property_editor;
}

void ScrollBarProperties::update ()
{
    value->set_min (res->min_value);
    value->set_max (res->max_value);
}
