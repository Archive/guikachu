//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_RESIZEABLE_CANVAS_H
#define GUIKACHU_FORM_EDITOR_RESIZEABLE_CANVAS_H

#include <memory>
#include "canvasitem.h"
#include "resizeable.h"
#include "canvas-grip.h"

namespace Guikachu
{
    class ResizeOp;
    class ResizeOpFactory;
    
    namespace GUI
    {
	namespace FormEditor
	{
	    class ResizeableCanvasItem: public virtual CanvasItem
	    {
		Widgets::Resizeable *resizeable;
		FooCanvasmm::Group  *grip_group;
		CanvasGrip          *grip[4]; 
		
		enum Corner
		{
		    CORNER_NW,
		    CORNER_SW,
		    CORNER_NE,
		    CORNER_SE
		};

		struct {
		    int start_width;
		    int start_height;

		    int start_x;
		    int start_y;

		    int dx;
		    int dy;
		} drag_context;

		std::auto_ptr<ResizeOpFactory>  op_factory;
		ResizeOp                       *current_op;
		
	    protected:
		ResizeableCanvasItem (Widgets::Resizeable *resizeable,
				      FooCanvasmm::Group  &parent_group,
				      ResizeOpFactory     *op_factory);

	    public:
		virtual ~ResizeableCanvasItem ();

	    private:
		void create_resize_grips ();
		CanvasGrip *create_grip (Corner corner);
		
		void selected_last_cb (bool selected);
		void canvas_item_changed_cb ();
		void grip_drag_begin_cb (Corner corner);
		void grip_drag_motion_cb (int dx, int dy, Corner corner);
		void grip_drag_end_cb (Corner corner);

		void get_delta_constraints (int &dx_min, int &dx_max,
					    int &dy_min, int &dy_max,
					    Corner corner);
		
		void clip_delta (int &dx, int &dy, Corner corner);
	    };
	}
    }
}

#endif /* !GUIKACHU_FORM_EDITOR_RESIZEABLE_CANVAS_H */
