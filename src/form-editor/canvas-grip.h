//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_CANVAS_GRIP_H
#define GUIKACHU_FORM_EDITOR_CANVAS_GRIP_H

#include <foocanvasmm/rect.h>

namespace Guikachu
{
    namespace GUI
    {
	namespace FormEditor
	{
	    class CanvasGrip: public FooCanvasmm::Rect
	    {
		struct
                {
                    bool dragging;

                    double last_x, last_y;
                    double delta_remainder_x, delta_remainder_y;
                } drag_context;

                Gdk::CursorType cursor;
                
	    public:
		CanvasGrip (FooCanvasmm::Group &parent_group,
			    int                 x,
			    int                 y,
                            Gdk::CursorType     cursor = Gdk::TOP_LEFT_ARROW);

		void set_position (gdouble x, gdouble y);
		
		sigc::signal0<void> drag_begin;
		sigc::signal2<void, int, int> drag_motion;
		sigc::signal0<void> drag_end;

	    private:
		void canvas_event_cb  (GdkEvent *event);

		void drag_begin_impl  (GdkEvent *event);
		void drag_motion_impl (GdkEvent *event);
		void drag_end_impl    (GdkEvent *event);
	    };
	}
    }
}

#endif /* !GUIKACHU_FORM_EDITOR_CANVAS_GRIP_H */
