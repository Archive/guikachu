//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "button-canvas.h"

#include "form-editor.h"
#include "form-editor-canvas.h"
#include "resizeable-ops.h"

#include <foocanvasmm/pixbuf.h>

using namespace Guikachu::GUI::FormEditor;

ButtonCanvasItem::ButtonCanvasItem (Widgets::Button    *widget_,
				    FooCanvasmm::Group &parent_group):
    CanvasItem (widget_, parent_group),
    WidgetCanvasItem (widget_, parent_group),
    ResizeableCanvasItem (widget_, parent_group, new WidgetOps::ResizeOpFactory (widget_)),
    widget (widget_)
{
    widget->bitmap_id.resource_changed.connect (sigc::mem_fun (*this, &ButtonCanvasItem::update));
}

void ButtonCanvasItem::create_simple_border (FooCanvasmm::Group &group) const
{
    int x1 = widget->x;
    int y1 = widget->y;

    int width = widget->get_width ();
    int height = widget->get_height () + 1;

    int pix_height = height + 2;
    int pix_width = width + 2;

    Glib::RefPtr<Gdk::Pixbuf> pixbuf = Gdk::Pixbuf::create (
	Gdk::COLORSPACE_RGB, true, 8, pix_width, pix_height);
    
    guint8     *pixdata = pixbuf->get_pixels ();
    int         rowstride = pixbuf->get_rowstride ();
    Gdk::Color  color (get_foreground_color (widget->usable));

    memset (pixdata, 0, pix_height * rowstride);
    
#define GUIKACHU_OFFSET(x,y)				\
    (MIN (MAX (0, (y)), (pix_height - 1)) * rowstride +	\
     MIN (MAX (0, (x)), (pix_width - 1)) * 4)

#define GUIKACHU_PLOT(x,y)						\
    {									\
	pixdata[GUIKACHU_OFFSET(x,y) + 0] = color.get_red ()   >> 8;	\
	pixdata[GUIKACHU_OFFSET(x,y) + 1] = color.get_green () >> 8;	\
	pixdata[GUIKACHU_OFFSET(x,y) + 2] = color.get_blue ()  >> 8;	\
	pixdata[GUIKACHU_OFFSET(x,y) + 3] = 255;			\
    }

    // N and S border
    for (int x = 3; x < width - 1; x++)
    {
	GUIKACHU_PLOT (x, 0);
	GUIKACHU_PLOT (x, height);
    }
    
    // W and E border
    for (int y = 3; y < height - 2; y++)
    {
	GUIKACHU_PLOT (0, y);
	GUIKACHU_PLOT (width + 1, y);
    }

    // NW corner
    GUIKACHU_PLOT (1, 1);
    GUIKACHU_PLOT (2, 1);
    GUIKACHU_PLOT (1, 2);

    // NE corner
    GUIKACHU_PLOT (width, 1);
    GUIKACHU_PLOT (width - 1, 1);
    GUIKACHU_PLOT (width, 2);

    // SE corner
    GUIKACHU_PLOT (width - 1, height - 1);
    GUIKACHU_PLOT (width, height - 1);
    GUIKACHU_PLOT (width, height - 2);

    // SW corner
    GUIKACHU_PLOT (1, height - 1);
    GUIKACHU_PLOT (2, height - 1);
    GUIKACHU_PLOT (1, height - 2);

#undef GUIKACHU_PLOT
    
    FooCanvasmm::Pixbuf *pixbuf_item = new FooCanvasmm::Pixbuf (group, x1 - 1, y1 - 1, pixbuf);
    pixbuf_item->property_interp_type () = Gdk::INTERP_NEAREST;

    FormEditor::draw_rectangle (group,
				x1 - 1, y1 - 1,
				x1 + width + 1, y1 + height + 2);
}

void ButtonCanvasItem::create_bold_border (FooCanvasmm::Group &group) const
{
    int x1 = widget->x;
    int y1 = widget->y;

    int width = widget->get_width ();
    int height = widget->get_height ();

    const int pix_height = height + 4;
    const int pix_width = width + 4;

    Glib::RefPtr<Gdk::Pixbuf> pixbuf = Gdk::Pixbuf::create (
	Gdk::COLORSPACE_RGB, true, 8, pix_width, pix_height);
    
    guint8     *pixdata = pixbuf->get_pixels ();
    int         rowstride = pixbuf->get_rowstride ();
    Gdk::Color  color (get_foreground_color (widget->usable));

    memset (pixdata, 0, pix_height * rowstride);

#define GUIKACHU_PLOT(x,y)						\
    {									\
	pixdata[GUIKACHU_OFFSET(x, y) + 0] = color.get_red ()   >> 8;	\
	pixdata[GUIKACHU_OFFSET(x, y) + 1] = color.get_green () >> 8;	\
	pixdata[GUIKACHU_OFFSET(x, y) + 2] = color.get_blue ()  >> 8;	\
	pixdata[GUIKACHU_OFFSET(x, y) + 3] = 255;			\
    }

    // N and S border
    for (int x = 6; x < width - 2; x++)
    {
	GUIKACHU_PLOT (x, 0);
	GUIKACHU_PLOT (x, 1);
	
	GUIKACHU_PLOT (x, height + 2);
	GUIKACHU_PLOT (x, height + 3);
    }

    // W and E border
    for (int y = 5; y < height - 1; y++)
    {
	GUIKACHU_PLOT (0, y);
	GUIKACHU_PLOT (1, y);
	
	GUIKACHU_PLOT (width + 2, y);
	GUIKACHU_PLOT (width + 3, y);
    }

    // NW corner
    const int corner_width = 5;
    const int corner_height = 6;
    const char *corner_nw[] = {
	"    X",
	"  XXX",
	" XXXX",
	"XXX  ",
	"XX   ",
	" X   " };

    const char *corner_ne[] = {
	"X    ",
	"XXX  ",
	"XXXX ",
	"  XXX",
	"   XX",
	"   X " };

    const char *corner_se[] = {
	"   X ",
	"   XX",
	"  XXX",
	"XXXX ",
	"XXX  ",
	"X    " };

    const char *corner_sw[] = {
	" X   ",
	"XX   ",
	"XXX  ",
	" XXXX",
	"  XXX",
	"    X" };

    for (int parse_y = 0; parse_y < corner_height; parse_y++)
	for (int parse_x = 0; parse_x < corner_width; parse_x++)
	{
	    if (corner_nw[parse_y][parse_x] != ' ')
		GUIKACHU_PLOT (1 + parse_x, parse_y);
	    if (corner_ne[parse_y][parse_x] != ' ')
		GUIKACHU_PLOT (width - 2 + parse_x, parse_y);
	    if (corner_se[parse_y][parse_x] != ' ')
		GUIKACHU_PLOT (width - 2 + parse_x, height - 2 + parse_y);
	    if (corner_sw[parse_y][parse_x] != ' ')
		GUIKACHU_PLOT (1 + parse_x, height - 2 + parse_y);
	}
    
#undef GUIKACHU_PLOT
    
    FooCanvasmm::Pixbuf *pixbuf_item = new FooCanvasmm::Pixbuf (group, x1 - 2, y1 - 2, pixbuf);
    pixbuf_item->property_interp_type () = Gdk::INTERP_NEAREST;
    
    FormEditor::draw_rectangle (group,
				x1 - 2, y1 - 2,
				x1 + width + 3, y1 + height + 3);
}

void ButtonCanvasItem::draw (FooCanvasmm::Group &group) const
{
    if (widget->frame == Widgets::Button::FRAME_SIMPLE)
	create_simple_border (group);

    if (widget->frame == Widgets::Button::FRAME_BOLD)
	create_bold_border (group);
	
    if (widget->bitmap_id == "" && widget->selected_bitmap_id == "")
	FormEditor::draw_text (group,
			       widget->text, get_font (widget->font),
			       get_foreground_color (widget->usable),
			       widget->x, widget->y,
                               widget->get_width (), widget->get_height ());
    else
        draw_bitmapref (group,
                        widget->bitmap_id, widget->get_manager ()->get_target ()->screen_color,
                        get_background_color (),
                        widget->x, widget->y,
                        widget->get_width (), widget->get_height (),
                        Gtk::ANCHOR_CENTER);
}

void ButtonCanvasItem::get_bounds (int &x1, int &y1,
				   int &x2, int &y2)
{
    x1 = widget->x;
    y1 = widget->y;
    x2 = x1 + widget->get_width ();
    y2 = y1 + widget->get_height ();
}
