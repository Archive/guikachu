//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "label.h"

#include "form-editor.h"

using Guikachu::Widgets::Label;
using namespace Guikachu::GUI::FormEditor;

Label::Label (Resources::Form   *owner,
	      const std::string &id,
	      serial_t           serial):
    Widget (owner, id, serial),
    Usable (changed),
    Textual (changed, id)
{
}

int Label::get_width  () const
{
    int width = 0, height = 0;
    GUI::FormEditor::get_text_extents (font, text, width, height);

    return width;
}

int Label::get_height () const
{
    int width = 0, height = 0;
    GUI::FormEditor::get_text_extents (font, text, width, height);

    return height;
}
