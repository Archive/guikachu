//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "formbitmap-canvas.h"

#include "form-editor.h"
#include "form-editor-canvas.h"

#include <foocanvasmm/pixbuf.h>

using namespace Guikachu::GUI::FormEditor;

FormBitmapCanvasItem::FormBitmapCanvasItem (Widgets::FormBitmap *widget_,
                                            FooCanvasmm::Group  &parent_group):
    CanvasItem (widget_, parent_group),
    WidgetCanvasItem (widget_, parent_group),
    widget (widget_)
{
    widget->bitmap_id.resource_changed.connect (sigc::mem_fun (*this, &FormBitmapCanvasItem::update));
}

void FormBitmapCanvasItem::draw_placeholder (FooCanvasmm::Group &group) const
{
    int x = widget->x;
    int y = widget->y;
    
    static const char stipple_bits[] = { 0x04, 0x02, 0x01, };
    static const Glib::RefPtr<Gdk::Bitmap> stipple = Gdk::Bitmap::create (stipple_bits, 3, 3);
    
    using namespace FooCanvasmm::Properties;
    *(draw_rectangle (group, x, y,
                      x + widget->get_width (), y + widget->get_height ()))
        << fill_stipple (stipple)
        << fill_color (get_foreground_color (widget->usable));    
}

void FormBitmapCanvasItem::draw (FooCanvasmm::Group &group) const
{
    if (!draw_bitmapref (group, widget->bitmap_id,
                         widget->get_manager ()->get_target ()->screen_color,
                         get_background_color (),
                         widget->x, widget->y))
        draw_placeholder (group);
}
