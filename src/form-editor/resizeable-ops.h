//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_RESIZEABLE_OPS_H
#define GUIKACHU_FORM_EDITOR_RESIZEABLE_OPS_H

#include "resource-manager.h"
#include "widget.h"
#include "resizeable.h"
#include "form.h"

namespace Guikachu
{   
    class ResizeOp: public UndoOp
    {
	int  old_x, old_y, old_width, old_height;
	bool old_manual_width, old_manual_height;
	int  new_x, new_y, new_width, new_height;

	bool committed;

    protected:
	virtual Widgets::Resizeable * get_resizeable () = 0;
	
    public:
	ResizeOp (Widgets::Resizeable *resizeable);
	virtual ~ResizeOp () {};
	void commit ();
	
	void undo ();
	void redo ();	
    };

    class ResizeOpFactory
    {
	UndoManager &undo_manager;

    protected:
	ResizeOpFactory (UndoManager &undo_manager);
	
    public:
	virtual ~ResizeOpFactory () {};
	
	virtual ResizeOp * create_op () = 0;
	void push_op (ResizeOp *op);
    };

    
    namespace WidgetOps
    {
	class ResizeOp: public Guikachu::ResizeOp
	{
	    ResourceManager *manager;
	    serial_t form_serial, widget_serial;

	    Glib::ustring label;
	
	protected:
	    Widgets::Resizeable * get_resizeable ();

	public:
	    ResizeOp (Widget *widget);
	    virtual ~ResizeOp () {};

	    Glib::ustring get_label () const;
	};

	class ResizeOpFactory: public Guikachu::ResizeOpFactory
	{
	    Widget *widget;

	public:
	    ResizeOpFactory (Widget *widget);
	    virtual ~ResizeOpFactory () {};

	    Guikachu::ResizeOp * create_op ();
	};
	
    } // namespace WidgetOps

    
    namespace ResourceOps
    {
	class FormResizeOp: public ResizeOp
	{
	    ResourceManager *manager;
	    serial_t form_serial;
	    Widgets::Form *last_adapter;

	    Glib::ustring label;
	    
	protected:
	    Widgets::Resizeable * get_resizeable ();
	    
	public:
	    FormResizeOp (Widgets::Form *form);
	    virtual ~FormResizeOp ();
	    
	Glib::ustring get_label () const;
	};
	
	class FormResizeOpFactory: public ResizeOpFactory
	{
	    Widgets::Form *form;
	    
	public:
	    FormResizeOpFactory (Widgets::Form *form);
	    virtual ~FormResizeOpFactory () {};
	    
	    Guikachu::ResizeOp * create_op ();
	};
	
    } // namespace ResourceOps
}

#endif /* !GUIKACHU_FORM_EDITOR_WIDGET_OPS_H */
