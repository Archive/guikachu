//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_WIDGET_REF_H
#define GUIKACHU_WIDGET_REF_H

#include "property.h"
#include <sigc++/trackable.h>
#include <sigc++/connection.h>

namespace Guikachu
{
    /* Don't you hate circular dependencies? */
    
    class Widget;

    class Resource;
    namespace Resources
    {
	class Form;
    }
    
    namespace Properties
    {
	class WidgetRef: public Property<std::string>,
			 public sigc::trackable
	{
	    Resources::Form  *form;
	    Widget           *current_widget;
	    sigc::connection  current_widget_changed;
	    
	public:
	    WidgetRef (notify_signal_t   &notify_signal,
		       Resources::Form   *form,
		       const std::string &value = "");
	    
	    virtual void set_val (const std::string &value);
	    inline const std::string& operator= (const std::string &value_) { set_val (value_); return value; };

            Resources::Form * get_form () const { return form; };
            Widget *          resolve () const { return current_widget; };

	    sigc::signal0<void> widget_changed;
	    
	private:
	    void widget_created_cb (Widget *widget);
	    void widget_removed_cb (Widget *widget);
	    void widget_changed_cb ();
	};
    }
}

#endif /* !GUIKACHU_WIDGET_REF_H */
