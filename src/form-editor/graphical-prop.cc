//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "graphical-prop.h"

#include <glib/gi18n.h>

#include "property-ops-widget.h"

#include "widgets/resource-combo.h"

using namespace Guikachu::GUI::FormEditor;

void GraphicalProperties::add_controls (Widgets::Graphical *res,
                                        GUI::PropertyTable &proptable)
{
    Gtk::Widget *control;

    std::set<Resources::Type> bitmap_types;
    bitmap_types.insert (Resources::RESOURCE_BITMAP);
    bitmap_types.insert (Resources::RESOURCE_BITMAPFAMILY);
    
    control = new GUI::PropertyEditors::ResourceCombo (
	bitmap_types, res->bitmap_id,
        new WidgetOps::PropChangeOpFactory<std::string> (
            _("Change icon of %s"), dynamic_cast<Widget*>(res), res->bitmap_id));
    proptable.add (_("_Bitmap:"), *manage (control),
                   _("Select the optional icon to show"));

    control = new GUI::PropertyEditors::ResourceCombo (
	bitmap_types, res->selected_bitmap_id,
        new WidgetOps::PropChangeOpFactory<std::string> (
            _("Change selected icon of %s"), dynamic_cast<Widget*>(res), res->selected_bitmap_id));
    proptable.add (_("_Selected bitmap:"), *manage (control),
                   _("Select the icon to show while the button is tapped"));
}
