//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "graffiti-prop.h"

#include "widgets/propertytable.h"

using namespace Guikachu::GUI::FormEditor;
using namespace Guikachu::Widgets;

GraffitiProperties::GraffitiProperties (Graffiti *res):
    WidgetProperties (res)
{
    GUI::PropertyTable *proptable = new GUI::PropertyTable ();
    
    /* Resource ID */
    add_general_controls (*proptable);

    /* Separator */
    proptable->add_separator ();

    /* Position */
    add_position_controls (*proptable);
    
    property_editor = proptable;
}

GraffitiProperties::~GraffitiProperties ()
{
    delete property_editor;
}

Gtk::Widget* GraffitiProperties::get_editor ()
{
    return property_editor;
}
