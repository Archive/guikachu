//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_AUTO_RESIZEABLE_H
#define GUIKACHU_FORM_EDITOR_AUTO_RESIZEABLE_H

#include "resizeable-widget.h"

namespace Guikachu
{
    namespace Widgets
    {
	class AutoWidth: public virtual ResizeableWidget
	{
	    VSignal &changed_propagate;
	    VSignal  manual_width_changed;

	protected:
	    AutoWidth (VSignal &changed);

	    virtual int get_auto_width () const = 0;

	public:
	    virtual ~AutoWidth () {};

	    int get_width ()  const;

	    Property<bool> manual_width;

	private:
	    void manual_width_cb  ();
	    void width_changed_cb ();
	    bool manual_sync_block;
	};
	
	class AutoHeight: public virtual ResizeableWidget
	{
	    VSignal &changed_propagate;
	    VSignal  manual_height_changed;

	protected:
	    AutoHeight (VSignal &changed);

	    virtual int get_auto_height () const = 0;

	public:
	    virtual ~AutoHeight () {};

	    int get_height () const;

	    Property<bool> manual_height;

	private:
	    void manual_height_cb  ();
	    void height_changed_cb ();
	    bool manual_sync_block;
	};
    }
}

#endif /* !GUIKACHU_FORM_EDITOR_AUTO_RESIZEABLE_H */
