//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "resizeable-widget.h"

#include <sigc++/sigc++.h>

using namespace Guikachu::Widgets;

ResizeableWidget::ResizeableWidget (VSignal &changed):
    Resizeable (_width, _height),
    real_changed (changed),
    _width (width_changed, 0),
    _height (height_changed, 0)
{
    width_changed.connect (sigc::mem_fun (*this, &ResizeableWidget::emit_changed));
    height_changed.connect (sigc::mem_fun (*this, &ResizeableWidget::emit_changed));
}


void ResizeableWidget::emit_changed ()
{
    real_changed.emit ();
}
