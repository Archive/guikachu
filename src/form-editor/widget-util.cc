//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "form-editor/widget-util.h"

#include <glib/gi18n.h>

using namespace Guikachu;

std::string Widgets::prefix_from_type (Widgets::Type type)
{
    g_return_val_if_fail (type != Widgets::WIDGET_NONE, "");
    
    std::string type_id;
    
    switch (type)
    {
    case Widgets::WIDGET_LABEL:
	type_id = "LABEL";
	break;
    case Widgets::WIDGET_BUTTON:
	type_id = "BUTTON";
	break;
    case Widgets::WIDGET_PUSHBUTTON:
	type_id = "PUSHBUTTON";
	break;
    case Widgets::WIDGET_GRAFFITI:
	type_id = "GRAFFITI";
	break;
    case Widgets::WIDGET_SELECTOR_TRIGGER:
	type_id = "SELECTOR_TRIGGER";
	break;
    case Widgets::WIDGET_CHECKBOX:
	type_id = "CHECKBOX";
	break;
    case Widgets::WIDGET_LIST:
	type_id = "LIST";
	break;
    case Widgets::WIDGET_POPUP_TRIGGER:
	type_id = "POPUP_TRIGGER";
	break;
    case Widgets::WIDGET_SCROLLBAR:
	type_id = "SCROLLBAR";
	break;
    case Widgets::WIDGET_SLIDER:
	type_id = "SLIDER";
	break;
    case Widgets::WIDGET_TEXT_FIELD:
	type_id = "TEXT_FIELD";
	break;
    case Widgets::WIDGET_TABLE:
	type_id = "TABLE";
	break;
    case Widgets::WIDGET_FORMBITMAP:
        type_id = "FORMBITMAP";
        break;
    case Widgets::WIDGET_GADGET:
	type_id = "GADGET";
	break;
    case Widgets::WIDGET_NONE:
	g_assert_not_reached ();
	break;
    }
    
    return type_id;
}

std::string Widgets::type_id_from_type (Widgets::Type type)
{
    g_return_val_if_fail (type != Widgets::WIDGET_NONE, "");
    
    std::string type_id;
    
    switch (type)
    {
    case Widgets::WIDGET_LABEL:
	type_id = "label";
	break;
    case Widgets::WIDGET_BUTTON:
	type_id = "button";
	break;
    case Widgets::WIDGET_PUSHBUTTON:
	type_id = "pushbutton";
	break;
    case Widgets::WIDGET_GRAFFITI:
	type_id = "graffiti";
	break;
    case Widgets::WIDGET_SELECTOR_TRIGGER:
	type_id = "selector_trigger";
	break;
    case Widgets::WIDGET_CHECKBOX:
	type_id = "checkbox";
	break;
    case Widgets::WIDGET_LIST:
	type_id = "list";
	break;
    case Widgets::WIDGET_POPUP_TRIGGER:
	type_id = "popup_trigger";
	break;
    case Widgets::WIDGET_SCROLLBAR:
	type_id = "scrollbar";
	break;
    case Widgets::WIDGET_SLIDER:
	type_id = "slider";
	break;
    case Widgets::WIDGET_TEXT_FIELD:
	type_id = "text_field";
	break;
    case Widgets::WIDGET_TABLE:
	type_id = "table";
	break;
    case Widgets::WIDGET_FORMBITMAP:
        type_id = "formbitmap";
        break;
    case Widgets::WIDGET_GADGET:
	type_id = "gadget";
	break;
    case Widgets::WIDGET_NONE:
	g_assert_not_reached ();
	break;
    }
    
    return type_id;
}

Widgets::Type Widgets::type_from_type_id (const std::string &type_id)
{
    Widgets::Type type;

    g_return_val_if_fail (type_id != "", Widgets::WIDGET_NONE);
    
    if (type_id == "label")
	type = Widgets::WIDGET_LABEL;
    else if (type_id == "button")
	type = Widgets::WIDGET_BUTTON;
    else if (type_id ==  "pushbutton")
	type = Widgets::WIDGET_PUSHBUTTON;
    else if (type_id == "graffiti")
	type = Widgets::WIDGET_GRAFFITI;
    else if (type_id == "selector_trigger")
	type = Widgets::WIDGET_SELECTOR_TRIGGER;
    else if (type_id == "checkbox")
	type = Widgets::WIDGET_CHECKBOX;
    else if (type_id == "list")
	type = Widgets::WIDGET_LIST;
    else if (type_id == "popup_trigger")
	type = Widgets::WIDGET_POPUP_TRIGGER;
    else if (type_id == "scrollbar")
	type = Widgets::WIDGET_SCROLLBAR;
    else if (type_id == "slider")
	type = Widgets::WIDGET_SLIDER;
    else if (type_id == "text_field")
	type = Widgets::WIDGET_TEXT_FIELD;
    else if (type_id == "table")
	type = Widgets::WIDGET_TABLE;
    else if (type_id == "formbitmap")
        type = Widgets::WIDGET_FORMBITMAP;
    else if (type_id == "gadget")
	type = Widgets::WIDGET_GADGET;
    else
	type = Widgets::WIDGET_NONE;
    
    return type;
}

Glib::ustring Widgets::display_name_from_type (Widgets::Type type)
{
    std::string disp_name;
    
    switch (type)
    {
    case Widgets::WIDGET_NONE:
	disp_name = _("Selector");
	break;
    case Widgets::WIDGET_LABEL:
	disp_name = _("Label");
	break;
    case Widgets::WIDGET_BUTTON:
	disp_name = _("Button");
	break;
    case Widgets::WIDGET_PUSHBUTTON:
	disp_name = _("Pushbutton");
	break;
    case Widgets::WIDGET_GRAFFITI:
	disp_name = _("Graffiti");
	break;
    case Widgets::WIDGET_SELECTOR_TRIGGER:
	disp_name = _("Selector trigger");
	break;
    case Widgets::WIDGET_CHECKBOX:
	disp_name = _("Checkbox");
	break;
    case Widgets::WIDGET_LIST:
	disp_name = _("List");
	break;
    case Widgets::WIDGET_POPUP_TRIGGER:
	disp_name = _("Popup trigger");
	break;
    case Widgets::WIDGET_SCROLLBAR:
	disp_name = _("Scrollbar");
	break;
    case Widgets::WIDGET_SLIDER:
	disp_name = _("Slider");
	break;
    case Widgets::WIDGET_TEXT_FIELD:
	disp_name = _("Text field");
	break;
    case Widgets::WIDGET_TABLE:
	disp_name = _("Table");
	break;
    case Widgets::WIDGET_FORMBITMAP:
        disp_name = _("Bitmap");
        break;
    case Widgets::WIDGET_GADGET:
	disp_name = _("Gadget");
	break;
    }
    
    return disp_name;
}
