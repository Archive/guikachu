//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_FORM_ELEMENT_H
#define GUIKACHU_FORM_EDITOR_FORM_ELEMENT_H

#include "resource-manager.h"

namespace Guikachu
{
    // A FormElement is something that can be displayed in the Form
    // Preview, i.e. a Form or a Widget
    class FormElement
    {
    protected:
	virtual ~FormElement () {};

    public:
	// Position
	virtual int get_x () const = 0;
	virtual int get_y () const = 0;

	// Geometry
	virtual int get_width  () const = 0;
	virtual int get_height () const = 0;

	virtual ResourceManager *get_manager () const = 0;
	
	sigc::signal1<void, bool> selected;
	sigc::signal1<void, bool> selected_last;
	sigc::signal0<void>       request_edit;
    };
}

#endif /* !GUIKACHU_FORM_EDITOR_FORM_ELEMENT_H */
