//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_MENUBAR_CANVAS_H
#define GUIKACHU_FORM_EDITOR_MENUBAR_CANVAS_H

#include <foocanvasmm/group.h>
#include "menu-res.h"

namespace Guikachu
{
    namespace GUI
    {
	namespace FormEditor
	{
            class MenubarCanvasItem: public FooCanvasmm::Group
            {
                Resources::Menu *res;

                FooCanvasmm::Group *item_group;
                
                class SubmenuCanvasItem: public FooCanvasmm::Group
                {
                    Resources::Menu::Submenu  submenu;
                    Guikachu::Target         *target;
                    FooCanvasmm::Group        title_group, selection_group, items_group;

                    bool rolled_down;

                public:
                    SubmenuCanvasItem (FooCanvasmm::Group             &parent_group,
                                       const Resources::Menu::Submenu &submenu,
                                       Guikachu::Target               *target,
                                       int                            &x);

                    sigc::signal0<void> clicked;
                    
                    bool is_rolled_down () { return rolled_down; };
                    void roll_down ();
                    void roll_up ();
                    
                private:
                    void event_cb (GdkEvent *event);
                };

                std::vector<SubmenuCanvasItem*> submenu_items;
                
            public:
                MenubarCanvasItem (FooCanvasmm::Group &parent_group,
                                   Resources::Menu    *res);

            private:
                void update ();

                int get_screen_width () const;
                int get_screen_height () const;

                void submenu_clicked_cb (SubmenuCanvasItem *item);
	    };
	}
    }
}

#endif /* !GUIKACHU_FORM_EDITOR_MENUBAR_CANVAS_H */
