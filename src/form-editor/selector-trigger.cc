//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "selector-trigger.h"

#include "form-editor.h"

using Guikachu::Widgets::SelectorTrigger;
using namespace Guikachu::GUI::FormEditor;

SelectorTrigger::SelectorTrigger (Resources::Form   *owner,
				  const std::string &id,
				  serial_t           serial):
    ResizeableWidget (changed),
    Widget (owner, id, serial),
    AutoWidth (changed),
    AutoHeight (changed),
    Usable (changed),
    Disabled (changed),
    Textual (changed, id),
    Graphical (changed, owner->get_manager ()),
    
    anchor_right (changed, false)
{
}

int SelectorTrigger::get_auto_width () const
{
    int text_width = get_line_width (font, text);
    return 6 + text_width;
}

int SelectorTrigger::get_auto_height () const
{
    int text_height = get_font_height (font);
    return text_height + 1;
}
