//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "pushbutton.h"

#include "form-editor.h"

using Guikachu::Widgets::PushButton;
using namespace Guikachu::GUI::FormEditor;

PushButton::PushButton (Resources::Form   *owner,
			const std::string &id,
			serial_t           serial):
    ResizeableWidget (changed),
    Widget (owner, id, serial),
    AutoWidth (changed),
    AutoHeight (changed),
    Usable (changed),
    Disabled (changed),
    Textual (changed, id),
    Graphical (changed, owner->get_manager ()),
    
    anchor_right (changed, false),
    group (changed)
{
}

int PushButton::get_auto_width () const
{
    return get_line_width (font, text) + 6;
}

int PushButton::get_auto_height () const
{
    return get_font_height (font) + 1;
}
