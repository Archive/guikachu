//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "list-canvas.h"

#include "form-editor.h"
#include "form-editor-canvas.h"
#include "resizeable-ops.h"

using namespace Guikachu::GUI::FormEditor;

ListCanvasItem::ListCanvasItem (Widgets::List      *widget_,
				FooCanvasmm::Group &parent_group):
    CanvasItem (widget_, parent_group),
    WidgetCanvasItem (widget_, parent_group),
    ResizeableCanvasItem (widget_, parent_group, new WidgetOps::ResizeOpFactory (widget_)),
    widget (widget_)
{
}

void ListCanvasItem::draw (FooCanvasmm::Group &group) const
{
    int font_num = widget->font;    
    int height = get_font_height (font_num) * widget->visible_items;
    int width = widget->get_width ();

    /* Render text items */
    const std::vector<std::string> &listitems = widget->items;

    // FIXME: size() is unsigned, visible_items is signed
    int visible_strings = std::min<int> (widget->visible_items, listitems.size ());
    
    int line_spacing = get_font_height (font_num);
    int text_y = widget->y;
    int text_x = widget->x + 2;
    
    for (int i = 0; i < visible_strings; i++)
    {
	if (width > 4)
	    draw_text (group, listitems[i], get_font (font_num),
		       get_foreground_color (widget->usable),
		       text_x, text_y, width - 4, 0,
		       Gtk::ANCHOR_NW);
	
	text_y += line_spacing;
    }

    /* Render frame */
    int x1 = widget->x - 1;
    int y1 = widget->y - 1;
    int x2 = x1 + width + 1;
    int y2 = y1 + height + 1;
    
    using namespace FooCanvasmm::Properties;
    *(FormEditor::draw_rectangle (group, x1, y1, x2, y2))
	<< width_units (1)
	<< outline_color (get_foreground_color (widget->usable));
}
