//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "form-editor/form-editor.h"

#include "resource-manager.h"
#include "target.h"

using namespace Guikachu::GUI;
using Guikachu::GUI::FormEditor::Font;

namespace Fonts
{
#include "stdfont-fontdef.c"
#include "boldfont-fontdef.c"
#include "bigfont-fontdef.c"
#include "symbol-fontdef.c"
#include "symbol11-fontdef.c"
#include "symbol7-fontdef.c"
#include "ledfont-fontdef.c"
#include "bigbold-fontdef.c"
}

#include <sstream>

#define GUIKACHU_TITLE_FONT 1

const Font &FormEditor::get_font (int font_num)
{
    static Font standard_fonts[8];
    static bool init[8] = {false, false, false, false, false, false, false, false};
    static const char* const font_filenames[] = { Fonts::stdfont, Fonts::boldfont, Fonts::bigfont,
                                                  Fonts::symbol, Fonts::symbol11, Fonts::symbol7,
                                                  Fonts::ledfont, Fonts::bigbold };

    if (font_num > 7)
    {
        g_warning ("Font number %d out of range", font_num);
        font_num = 0;
    }
    
    if (!init[font_num])
    {
        std::istringstream buf (font_filenames[font_num]);
        standard_fonts[font_num].parse (buf);
        init[font_num] = true;
    }
    
    return standard_fonts[font_num];
}


const Font &FormEditor::get_title_font ()
{
    return get_font (GUIKACHU_TITLE_FONT);
}

int FormEditor::get_font_height (int font_num)
{
    const Font &font = get_font (font_num);
    return font.get_line_height ();
}

int FormEditor::get_line_width (int font_num, const std::string &text)
{
    const Font &font = get_font (font_num);
    return font.get_line_width (text);
}

void FormEditor::get_text_extents (int font_num, const std::string &text,
				   int &width, int &height)
{
    const Font &font = get_font (font_num);
    font.get_text_extents (text, width, height);
}

