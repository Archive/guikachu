//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "popup-trigger-canvas.h"

#include "form-editor.h"
#include "form-editor-canvas.h"
#include "resizeable-ops.h"

#include <gdkmm/pixbuf.h>
#include <foocanvasmm/pixbuf.h>

#include "pixmaps/popup-trigger-canvas.xpm"
#include "pixmaps/popup-trigger-canvas-disabled.xpm"

using namespace Guikachu::GUI::FormEditor;

PopupTriggerCanvasItem::PopupTriggerCanvasItem (Widgets::PopupTrigger *widget_,
						FooCanvasmm::Group    &parent_group):
    CanvasItem (widget_, parent_group),
    WidgetCanvasItem (widget_, parent_group),
    ResizeableCanvasItem (widget_, parent_group, new WidgetOps::ResizeOpFactory (widget_)),
    widget (widget_)
{
    widget->bitmap_id.resource_changed.connect (sigc::mem_fun (*this, &PopupTriggerCanvasItem::update));
}

void PopupTriggerCanvasItem::draw (FooCanvasmm::Group &group) const
{
    if (widget->bitmap_id == "" && widget->selected_bitmap_id == "")
    {
        Glib::RefPtr<Gdk::Pixbuf> pixbuf = Gdk::Pixbuf::create_from_xpm_data (
            widget->usable ? popup_trigger_canvas_xpm : popup_trigger_canvas_disabled_xpm);
        
        new FooCanvasmm::Pixbuf (group, widget->x, widget->y, pixbuf);

        int text_x = widget->x + 15;
        int text_y = widget->y;
        
        if (widget->get_width () > 15)
            draw_text (group,
                       widget->text, get_font (widget->font),
                       get_foreground_color (widget->usable),
                       text_x, text_y,
                       widget->get_width () - 15, widget->get_height (),
                       Gtk::ANCHOR_NW);
    } else {
        if (widget->get_width () > 3)
            draw_bitmapref (group, widget->bitmap_id,
                            widget->get_manager ()->get_target ()->screen_color,
                            get_background_color (),
                            widget->x + 3, widget->y,
                            widget->get_width () - 3, widget->get_height (),
                            Gtk::ANCHOR_CENTER);
    }
}

void PopupTriggerCanvasItem::get_bounds (int &x1, int &y1,
					 int &x2, int &y2)
{
    x1 = widget->x;
    x2 = x1 + widget->get_width ();
    y1 = widget->y;
    y2 = y1 + widget->get_height ();
}
