//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "list.h"

#include "form-editor.h"

using Guikachu::Widgets::List;
using namespace Guikachu::GUI::FormEditor;

List::List (Resources::Form   *owner,
	    const std::string &id,
	    serial_t           serial):
    ResizeableWidget (changed),
    Widget (owner, id, serial),
    AutoWidth (changed),
    Usable (changed),
    font (changed, 0),
    visible_items (changed, 4),
    items (changed, std::vector<std::string> ())
{
}

int List::get_height () const
{
    return get_font_height (font) * visible_items;
}

int List::get_auto_width () const
{
    int width = 18;
    if (items ().size ())
	width = get_line_width (font, items ()[0]) + 3;
    
    return width;
}
