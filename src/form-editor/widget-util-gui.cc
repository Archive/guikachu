//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "form-editor/widget-util-gui.h"

using namespace Guikachu;

namespace {
#include "pixmaps/label.xpm"
#include "pixmaps/button.xpm"
#include "pixmaps/pushbutton.xpm"
#include "pixmaps/graffiti.xpm"
#include "pixmaps/selector-trigger.xpm"
#include "pixmaps/checkbox.xpm"
#include "pixmaps/list.xpm"
#include "pixmaps/popup-trigger.xpm"
#include "pixmaps/scrollbar.xpm"
#include "pixmaps/slider.xpm"
#include "pixmaps/text-field.xpm"
#include "pixmaps/table.xpm"
#include "pixmaps/formbitmap.xpm"
#include "pixmaps/gadget.xpm"
}

const char * const * Widgets::get_type_icon (Widgets::Type type)
{
    g_return_val_if_fail (type != Widgets::WIDGET_NONE, NULL);

    switch (type)
    {
    case Widgets::WIDGET_BUTTON:
	return button_xpm;
    case Widgets::WIDGET_CHECKBOX:
	return checkbox_xpm;
    case Widgets::WIDGET_GRAFFITI:
	return graffiti_xpm;
    case Widgets::WIDGET_LABEL:
	return label_xpm;
    case Widgets::WIDGET_LIST:
	return list_xpm;
    case Widgets::WIDGET_POPUP_TRIGGER:
	return popup_trigger_xpm;
    case Widgets::WIDGET_PUSHBUTTON:
	return pushbutton_xpm;
    case Widgets::WIDGET_SCROLLBAR:
	return scrollbar_xpm;
    case Widgets::WIDGET_SLIDER:
	return slider_xpm;
    case Widgets::WIDGET_SELECTOR_TRIGGER:
	return selector_trigger_xpm;
    case Widgets::WIDGET_TEXT_FIELD:
	return text_field_xpm;
    case Widgets::WIDGET_TABLE:
	return table_xpm;
    case Widgets::WIDGET_FORMBITMAP:
        return formbitmap_xpm;
    case Widgets::WIDGET_GADGET:
	return gadget_xpm;
    case Widgets::WIDGET_NONE:
	g_assert_not_reached ();
    }

    g_assert_not_reached ();
    return NULL;
}

Glib::RefPtr<Gdk::Pixbuf> Widgets::get_type_pixbuf (Widgets::Type type)
{
    typedef std::map<Widgets::Type, Glib::RefPtr<Gdk::Pixbuf> > pixbuf_cache_t;
    static pixbuf_cache_t pixbuf_cache;

    if (pixbuf_cache.find (type) == pixbuf_cache.end ())
        pixbuf_cache[type] = Gdk::Pixbuf::create_from_xpm_data (get_type_icon (type));

    return pixbuf_cache[type];
}
