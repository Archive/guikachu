//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "text-field-canvas.h"
#include "form-editor.h"
#include "form-editor-canvas.h"

#include "resizeable-ops.h"

#include <foocanvasmm/line.h>

using namespace Guikachu::GUI::FormEditor;

TextFieldCanvasItem::TextFieldCanvasItem (Widgets::TextField *widget_,
					  FooCanvasmm::Group &parent_group):
    CanvasItem (widget_, parent_group),
    WidgetCanvasItem (widget_, parent_group),
    ResizeableCanvasItem (widget_, parent_group, new WidgetOps::ResizeOpFactory (widget_)),
    widget (widget_)
{
}

void TextFieldCanvasItem::draw (FooCanvasmm::Group &group) const
{   
    const Font &font = get_font (widget->font);
    int height = widget->get_height ();
    int width = widget->get_width ();

    int line_height = font.get_line_height ();
    int num_lines = 1;
    if (height > line_height && widget->multi_line)
	num_lines = height / line_height;

    int x = widget->x;
    int y = widget->y + 1;
    
    /* Draw background */
    draw_rectangle (group, x, y, x + width, y + height);

    /* Draw underlines if enabled */
    if (widget->underline)
    {
	/* Data for the GdkBitmap to use as an outline stipple */
	static const char stipple_bits[] = { 0x01, };
	static const Glib::RefPtr<Gdk::Bitmap> stipple =
	    Gdk::Bitmap::create (stipple_bits, 2, 1);

	int underline_y = y + font.get_ascent ();
	for (int i = 0;
	     i < num_lines;
	     i++, underline_y +=  line_height)
	{
	    FooCanvasmm::Points ul_points;
	    
	    ul_points.push_back (FooCanvasmm::Point (x, underline_y));
	    ul_points.push_back (FooCanvasmm::Point (x + width, underline_y));

	    using namespace FooCanvasmm::Properties;
	    *(new FooCanvasmm::Line (group, ul_points))
		<< fill_stipple (stipple)
		<< width_units (1.0)
		<< fill_color (get_foreground_color (widget->usable));
	}
    }
}

void TextFieldCanvasItem::get_bounds (int &x1, int &y1,
				      int &x2, int &y2)
{
    x1 = widget->x;
    x2 = x1 + widget->get_width ();
    y1 = widget->y;
    y2 = y1 + widget->get_height ();
}
