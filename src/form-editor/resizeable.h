//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_RESIZEABLE_H
#define GUIKACHU_FORM_EDITOR_RESIZEABLE_H

#include <sigc++/signal.h>
#include <sigc++/object.h>
#include "property.h"
#include "form-element.h"
#include "moveable.h"

namespace Guikachu
{
    namespace Widgets
    {
	class Resizeable: public virtual FormElement,
                          public virtual Moveable
	{
	protected:
	    Resizeable (Property<int> &real_width,
			Property<int> &real_height);
	    
	public:
	    virtual ~Resizeable () {};

	    virtual int get_width  () const;
	    virtual int get_height () const;

	    Property<int> &width;
	    Property<int> &height;
	};
    }
}

#endif /* !GUIKACHU_FORM_EDITOR_RESIZEABLE_H */
