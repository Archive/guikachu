//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "resizeable-prop.h"

#include <glib/gi18n.h>

#include "widgets/size-entry.h"
#include "property-ops-widget.h"

using namespace Guikachu::GUI::FormEditor;

void ResizeableProperties::add_width_controls (Widgets::Resizeable *res,
					       GUI::PropertyTable  &proptable)
{
    Gtk::Widget *control;
    
    /* Width */
    control = new GUI::PropertyEditors::WidthEntry (
	res->get_manager (), res->width,
	new WidgetOps::PropChangeOpFactory<int> (
	    _("Resize %s"), dynamic_cast<Widget*> (res), res->width, true));
				
    proptable.add (_("_Width:"), *manage (control),
		   _("Horizontal size of the widget"));    
}

void ResizeableProperties::add_height_controls (Widgets::Resizeable *res,
					       GUI::PropertyTable  &proptable)
{
    Gtk::Widget *control;
    
    /* Height */
    control = new GUI::PropertyEditors::HeightEntry (
	res->get_manager (), res->height,
	new WidgetOps::PropChangeOpFactory<int> (
	    _("Resize %s"), dynamic_cast<Widget*> (res), res->height, true));

    proptable.add (_("_Height:"), *manage (control),
		   _("Vertical size of the widget"));
    
}

void ResizeableProperties::add_controls (Widgets::Resizeable *res,
					 GUI::PropertyTable  &proptable)
{
    add_width_controls  (res, proptable);
    add_height_controls (res, proptable);
}
