//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_WIDGET_PROP_H
#define GUIKACHU_FORM_EDITOR_WIDGET_PROP_H

#include "widget.h"
#include "widgets/propertytable.h"

namespace Guikachu
{
    namespace GUI
    {
	namespace FormEditor
	{
	    class WidgetProperties
	    {
		Widget *widget;

	    protected:
		WidgetProperties (Widget *widget);

		void add_position_controls (GUI::PropertyTable &proptable);
		void add_general_controls  (GUI::PropertyTable &proptable);

	    public:
		virtual ~WidgetProperties () {};
		
		virtual Gtk::Widget * get_editor () = 0;
	    };
	};
    }
}

#endif /* !GUIKACHU_FORM_EDITOR_WIDGET_PROP_H */
