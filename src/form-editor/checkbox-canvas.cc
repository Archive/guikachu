//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "checkbox-canvas.h"

#include "form-editor.h"
#include "form-editor-canvas.h"
#include "resizeable-ops.h"

using namespace Guikachu::GUI::FormEditor;

CheckboxCanvasItem::CheckboxCanvasItem (Widgets::Checkbox  *widget_,
					FooCanvasmm::Group &parent_group):
    CanvasItem (widget_, parent_group),
    WidgetCanvasItem (widget_, parent_group),
    ResizeableCanvasItem (widget_, parent_group, new WidgetOps::ResizeOpFactory (widget_)),
    widget (widget_)
{
}

void CheckboxCanvasItem::draw (FooCanvasmm::Group &group) const
{
    int width = widget->get_width ();
    int height = widget->get_height ();

    // Don't ask about the +1, I have no idea...
    int x1 = widget->x;
    int x2 = x1 + width + 1;
    int y1 = widget->y;
    int y2 = y1 + height + 1;

    // Background transparent rectangle to get mouse events inside
    // "hollow" areas
    FormEditor::draw_rectangle (group, x1, y1, x2, y2);

    int text_x = x1 + 17;
    int text_y = y1;

    // Render checkbox mark
    // You can't include a \0 in a C string literal.
    std::string null_string;
    null_string += '\0';
    
    // Font #4 is Symbol11, and \001 and \000 are the checked/unchecked boxes
    draw_text (group, widget->toggled ? "\001" : null_string, get_font (4),
	       get_foreground_color (widget->usable),
	       x1, text_y, 0, height,
	       Gtk::ANCHOR_WEST);

    // Render text label
    if (width > 18)
    {
	draw_text (group,
		   widget->text, get_font (widget->font),
		   get_foreground_color (widget->usable),
		   text_x, text_y, width - 16, height,
		   Gtk::ANCHOR_WEST);
    }
}

void CheckboxCanvasItem::get_bounds (int &x1, int &y1,
				     int &x2, int &y2)
{
    int width = widget->get_width ();
    int height = widget->get_height ();

    width  = std::max (18, width);
    height = std::max (10, height);
    
    x1 = widget->x + 1;
    x2 = x1 + width;
    y1 = widget->y + 1;
    y2 = y1 + height - 3;
}
