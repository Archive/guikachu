//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_SLIDER_H
#define GUIKACHU_FORM_EDITOR_SLIDER_H

#include "widget.h"
#include "auto-resizeable.h"
#include "usable.h"
#include "disabled.h"

#include "resource-ref.h"

namespace Guikachu
{
    namespace Widgets
    {
	class Slider: public Widget,
		      public AutoHeight,
                      public Usable,
                      public Disabled
	{
	public:
	    Slider (Resources::Form *owner, const std::string &id, serial_t serial);
	    
	    Type get_type () const { return WIDGET_SLIDER; };
	    void apply_visitor (WidgetVisitor &visitor) { visitor.visit_widget (this); };

	    Property<bool> feedback;
	    Properties::ResourceRef thumb_id, background_id;
	    Property<int>  min_value, max_value, value, page_size;

	protected:
	    int get_auto_height () const;
	};
    }
}

#endif /* !GUIKACHU_FORM_EDITOR_SLIDER_H */
