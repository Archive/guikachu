//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "table-canvas.h"

#include "form-editor.h"
#include "form-editor-canvas.h"
#include "resizeable-ops.h"

#include <foocanvasmm/line.h>
#include <gdkmm/bitmap.h>

using namespace Guikachu::GUI::FormEditor;

namespace
{
void draw_table_line (FooCanvasmm::Group &group,
                      const Gdk::Color   &color,
		      int x1, int y1,
		      int x2, int y2)
{
    FooCanvasmm::Points line_points;
    
    static const char                stipple_bits[] = { 0x02, 0x01, };
    static Glib::RefPtr<Gdk::Bitmap> stipple = Gdk::Bitmap::create (stipple_bits, 2, 2);
    
    line_points.push_back (FooCanvasmm::Point (x1, y1));
    line_points.push_back (FooCanvasmm::Point (x2, y2));
    
    using namespace FooCanvasmm::Properties;
    *(new FooCanvasmm::Line (group, line_points))
	<< fill_color (color)
	<< fill_stipple (stipple)
	<< width_units (1.0);
}
    
} // Anonymous namespace

TableCanvasItem::TableCanvasItem (Widgets::Table     *widget_,
				  FooCanvasmm::Group &parent_group):
    CanvasItem (widget_, parent_group),
    WidgetCanvasItem (widget_, parent_group),
    ResizeableCanvasItem (widget_, parent_group, new WidgetOps::ResizeOpFactory (widget_)),
    widget (widget_)
{
}

void TableCanvasItem::draw (FooCanvasmm::Group &group) const
{
    const std::vector<int> &columns = widget->column_width;

    /* Column separators */
    int width  = 0;
    int height = widget->num_rows * 11;
    Gdk::Color color (get_foreground_color (true));
    
    int x1 = widget->x;
    int y1 = widget->y;
    int y2 = y1 + std::min (height, widget->get_height ());
    
    draw_table_line (group, color, x1, y1, x1, y2);

    int num_columns = widget->num_columns;
    for (int i = 0; i < num_columns; i++)
    {
	width += columns[i] + 1;
	int line_x = x1 + width;

	if (width <= widget->get_width ())
	    draw_table_line (group, color, line_x, y1, line_x, y2);
    }
    
    int x2 = x1 + std::min (width, widget->get_width ());
    
    /* Row separators */
    draw_table_line (group, color, x1, y1, x2, y1);
    const int row_step = 11;
    int row_y = 0;
    for (int i = 0; i < widget->num_rows; i++)
    {
	row_y += row_step;
	if (row_y > widget->get_height ())
	    break;
	
	draw_table_line (group, color, x1, y1 + row_y, x2, y1 + row_y);
    }

    // Draw background (to make it clickable)
    draw_rectangle (group, x1, y1, x1 + widget->get_width (), y1 + widget->get_height ());
}
