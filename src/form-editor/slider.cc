//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "slider.h"

#include "form-editor.h"

using Guikachu::Widgets::Slider;
using namespace Guikachu::GUI::FormEditor;

Slider::Slider (Resources::Form   *owner,
		const std::string &id,
		serial_t           serial):
    ResizeableWidget (changed),
    Widget (owner, id, serial),
    AutoHeight (changed),
    Usable (changed),
    Disabled (changed),
    
    feedback (changed, false),
    thumb_id (changed, owner->get_manager ()),
    background_id (changed, owner->get_manager ()),
    min_value (changed, 0),
    max_value (changed, 0),
    value (changed, 0),
    page_size (changed, 0)
{
    width = 114; /* Just some value where the default picture looks good */
}

#if 0
int Slider::get_auto_width () const
{
    return 114; /* Just some value where the default picture looks good */
}
#endif

int Slider::get_auto_height () const
{
    return 15; /* Height of the default picture */
}
