//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_H
#define GUIKACHU_FORM_EDITOR_H

#include "form-editor/font.h"

namespace Guikachu
{
    namespace GUI
    {
	namespace FormEditor
	{
	    const Font &get_font (int font_num);
	    const Font &get_title_font ();

	    int  get_font_height  (int font_num);

	    int  get_line_width   (int                font_num,
				   const std::string &text);
	    
	    void get_text_extents (int                font_num,
				   const std::string &text,
				   int               &width,
				   int               &height);
	}
    }
}
    
#endif /* !GUIKACHU_FORM_EDITOR_H */
