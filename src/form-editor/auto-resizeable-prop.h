//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_AUTO_RESIZEABLE_PROP_H
#define GUIKACHU_FORM_EDITOR_AUTO_RESIZEABLE_PROP_H

#include "auto-resizeable.h"
#include "widgets/propertytable.h"

namespace Guikachu
{
    namespace GUI
    {
	namespace FormEditor
	{
	    class AutoWidthProperties
	    {
	    public:
		static void add_controls (Widgets::AutoWidth *res,
					  GUI::PropertyTable &proptable);
	    };

	    class AutoHeightProperties
	    {
	    public:
		static void add_controls (Widgets::AutoHeight *res,
					  GUI::PropertyTable  &proptable);
	    };
	}
    }
}

#endif /* !GUIKACHU_FORM_EDITOR_AUTO_RESIZEABLE_PROP_H */
