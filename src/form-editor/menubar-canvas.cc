//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "menubar-canvas.h"

#include <foocanvasmm/pixbuf.h>
#include "form-editor.h"
#include "form-editor-canvas.h"

#include "preferences.h"

using namespace Guikachu::GUI::FormEditor;

MenubarCanvasItem::MenubarCanvasItem (FooCanvasmm::Group &parent_group,
                                      Resources::Menu    *res_):
    FooCanvasmm::Group (parent_group, 0, 0),
    res (res_),
    item_group (0) 
{
    Preferences::FormEditor::colors_changed.connect (sigc::mem_fun (*this, &MenubarCanvasItem::update));
    res->changed.connect (sigc::mem_fun (*this, &MenubarCanvasItem::update));
    res->get_manager ()->get_target ()->changed.connect (sigc::mem_fun (*this, &MenubarCanvasItem::update));
    
    update ();
}

void MenubarCanvasItem::update ()
{
    int rolled_down_submenu_idx = -1;
    for (unsigned int i = 0; i != submenu_items.size (); ++i)
        if (submenu_items[i]->is_rolled_down ())
            rolled_down_submenu_idx = i;
    
    delete item_group;
    item_group = new FooCanvasmm::Group (*this, 0, 0);
    submenu_items.clear ();
    
    // Background bar
    int bar_width = get_screen_width ();
    int bar_height = 15;

    Glib::RefPtr<Gdk::Pixbuf> bar_pixbuf = Gdk::Pixbuf::create (
	Gdk::COLORSPACE_RGB, true, 8, bar_width, bar_height);
    
    guint8     *pixdata = bar_pixbuf->get_pixels ();
    int         rowstride = bar_pixbuf->get_rowstride ();
    Gdk::Color  color (get_foreground_color (res->get_manager ()->get_target (), true));
    
    memset (pixdata, 0, bar_height * rowstride);
    
#define GUIKACHU_OFFSET(x,y)				\
    (MIN (MAX (0, (y)), (bar_height - 1)) * rowstride +	\
     MIN (MAX (0, (x)), (bar_width - 1)) * 4)

#define GUIKACHU_PLOT(x,y)						\
    {									\
	pixdata[GUIKACHU_OFFSET(x,y) + 0] = color.get_red ()   >> 8;	\
	pixdata[GUIKACHU_OFFSET(x,y) + 1] = color.get_green () >> 8;	\
	pixdata[GUIKACHU_OFFSET(x,y) + 2] = color.get_blue ()  >> 8;	\
	pixdata[GUIKACHU_OFFSET(x,y) + 3] = 255;			\
    }

    // Horizontal lines
    for (int x = 1; x != bar_width - 2; ++x)
    {
        GUIKACHU_PLOT (x, 0);
        GUIKACHU_PLOT (x, bar_height - 2);
    }
    GUIKACHU_PLOT (bar_width - 2, bar_height - 2);
    
    for (int x = 2; x != bar_width - 2; ++x)
        GUIKACHU_PLOT (x, bar_height - 1);

    // Vertical lines
    for (int y = 1; y != bar_height - 2; ++y)
    {
        GUIKACHU_PLOT (0, y);
        GUIKACHU_PLOT (bar_width - 2, y);
    }
    for (int y = 2; y != bar_height - 2; ++y)
        GUIKACHU_PLOT (bar_width - 1, y);

    new FooCanvasmm::Pixbuf (*item_group, 0, 0, bar_pixbuf);

    // Submenus
    
    const Resources::Menu::MenuTree &submenus = res->get_submenus ();
    int x = 9;
    submenu_items.reserve (submenus.size ());
    int submenu_idx = 0;
    for (Resources::Menu::MenuTree::const_iterator i = submenus.begin (); i != submenus.end (); ++i, ++submenu_idx)
    {
        SubmenuCanvasItem *submenu_item = new SubmenuCanvasItem (*item_group, *i, res->get_manager ()->get_target (), x);
        submenu_item->clicked.connect (
            sigc::bind (sigc::mem_fun (*this, &MenubarCanvasItem::submenu_clicked_cb), submenu_item));
        
        submenu_items.push_back (submenu_item);
        if (submenu_idx == rolled_down_submenu_idx)
            submenu_item->roll_down ();
    }
}

void MenubarCanvasItem::submenu_clicked_cb (SubmenuCanvasItem *item)
{
    if (item->is_rolled_down ())
    {
        item->roll_up ();
    } else {
        for (std::vector<SubmenuCanvasItem*>::const_iterator i = submenu_items.begin ();
             i != submenu_items.end (); ++i)
            (*i)->roll_up ();
        item->roll_down ();
    }   
}

int MenubarCanvasItem::get_screen_width () const
{
    return res->get_manager ()->get_target ()->screen_width;
}

int MenubarCanvasItem::get_screen_height () const
{
    return res->get_manager ()->get_target ()->screen_height;
}
