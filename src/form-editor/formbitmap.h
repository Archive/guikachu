//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_FORMBITMAP_H
#define GUIKACHU_FORM_EDITOR_FORMBITMAP_H

#include "widget.h"
#include "widget-ref.h"
#include "usable.h"

#include <sigc++/connection.h>

namespace Guikachu
{
    namespace Widgets
    {
	class FormBitmap: public Widget,
                          public Usable
	{
	    VSignal          bitmap_id_changed;
	    sigc::connection bitmap_changed_conn;
	    
	public:
	    FormBitmap (Resources::Form *owner, const std::string &id, serial_t serial);
	    
	    Type get_type () const { return WIDGET_FORMBITMAP; };
	    void apply_visitor (WidgetVisitor &visitor) { visitor.visit_widget (this); };
	    
	    Properties::ResourceRef bitmap_id;

            // Virtual methods from FormElement parent class
            int get_width  () const;
            int get_height () const;

	private:
	    void bitmap_id_changed_cb ();
	};
    }
}

#endif /* !GUIKACHU_FORM_EDITOR_FORMBITMAP_H */
