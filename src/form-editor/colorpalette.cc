//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "colorpalette.h"

using namespace Guikachu::GUI;

namespace
{
    struct Color
    {
        guint8 r, g, b;
    };
    
    int operator- (const Color &lhs, const Color &rhs)
    {
        return (lhs.r - rhs.r) * (lhs.r - rhs.r) +
            (lhs.g - rhs.g) * (lhs.g - rhs.g) +
            (lhs.b - rhs.b) * (lhs.b - rhs.b);
    }
    
    class ColorPalette
    {
        const Color *palette;
        size_t       palette_size;
        
    public:
        ColorPalette (const Color *colors, size_t color_num):
            palette (colors),
            palette_size (color_num)
            {
            }
        
        Color get_nearest_color (const Color &color) const;
        void  dither_pixels (const Guikachu::Resources::Bitmap::ImageData &image, 
			     const Glib::RefPtr<Gdk::Pixbuf>              &pixbuf) const;
    };
} // Anonymous namespace
    
Color ColorPalette::get_nearest_color (const Color &color) const
{
    int    min_diff = color - palette[0];
    size_t min_i = 0;
    
    for (size_t i = 0; i < palette_size; ++i)
    {
        int diff = color - palette[i];
        if (diff == 0)
            return color;
        
        if (diff < min_diff)
        {
            min_diff = diff;
            min_i = i;
        }
    }
    
    return palette[min_i];
}

void ColorPalette::dither_pixels (const Guikachu::Resources::Bitmap::ImageData &image, 
				  const Glib::RefPtr<Gdk::Pixbuf>              &pixbuf) const
{
    guint8 *dest_pixels = pixbuf->get_pixels ();
    int width = pixbuf->get_width ();
    int height = pixbuf->get_height ();
    int rowstride = pixbuf->get_rowstride ();

    guint8 *src_pixels = image->get_pixels ();
    int src_rowstride = image->get_rowstride ();
    int src_bpp = image->get_has_alpha () ? 4 : 3;

    for (int y = 0; y < height; ++y)
        for (int x = 0; x < width; ++x)
        {
            guint8 *start = src_pixels + (y * src_rowstride) + (x * src_bpp);
            Color c = { start[0], start[1], start[2] };

            c = get_nearest_color (c);
            guint8 *dest_start = dest_pixels + (y * rowstride) + (x * 3);
            dest_start[0] = c.r;
            dest_start[1] = c.g;
            dest_start[2] = c.b;
        }
}

    
// Color palettes -- the actual numbers are lifted from PilRC
namespace
{
    const Color palette_mono_data[] = {
        { 255, 255, 255}, {   0,   0,   0 }
    };
    const ColorPalette palette_mono (palette_mono_data, 2);
    
    const Color palette_grey_4_data[] = {
        { 255, 255, 255}, { 192, 192, 192}, { 128, 128, 128 }, {   0,   0,   0 }
    };
    const ColorPalette palette_grey_4 (palette_grey_4_data, 4);

    const Color palette_grey_16_data[] = {
        { 255, 255, 255}, { 238, 238, 238 }, { 221, 221, 221 }, { 204, 204, 204 },
        { 187, 187, 187}, { 170, 170, 170 }, { 153, 153, 153 }, { 136, 136, 136 },
        { 119, 119, 119}, { 102, 102, 102 }, {  85,  85,  85 }, {  68,  68,  68 },
        {  51,  51,  51}, {  34,  34,  34 }, {  17,  17,  17 }, {   0,   0,   0 }
    };
    const ColorPalette palette_grey_16 (palette_grey_16_data, 16);

    const Color palette_color_16_data[] = {
        { 255, 255, 255}, { 128, 128, 128 }, { 128,   0,   0 }, { 128, 128,   0 },
        {   0, 128,   0}, {   0, 128, 128 }, {   0,   0, 128 }, { 128,   0, 128 },
        { 255,   0, 255}, { 192, 192, 192 }, { 255,   0,   0 }, { 255, 255,   0 },
        {   0, 255,   0}, {   0, 255, 255 }, {   0,   0, 255 }, {   0,   0,   0 }
    };
    const ColorPalette palette_color_16 (palette_color_16_data, 16);

    // Funny how it's only 232 colors
    const Color palette_color_256_data[] = {
        { 255, 255, 255 }, { 255, 204, 255 }, { 255, 153, 255 }, { 255, 102, 255 }, 
        { 255,  51, 255 }, { 255,   0, 255 }, { 255, 255, 204 }, { 255, 204, 204 }, 
        { 255, 153, 204 }, { 255, 102, 204 }, { 255,  51, 204 }, { 255,   0, 204 }, 
        { 255, 255, 153 }, { 255, 204, 153 }, { 255, 153, 153 }, { 255, 102, 153 }, 
        { 255,  51, 153 }, { 255,   0, 153 }, { 204, 255, 255 }, { 204, 204, 255 },
        { 204, 153, 255 }, { 204, 102, 255 }, { 204,  51, 255 }, { 204,   0, 255 },
        { 204, 255, 204 }, { 204, 204, 204 }, { 204, 153, 204 }, { 204, 102, 204 },
        { 204,  51, 204 }, { 204,   0, 204 }, { 204, 255, 153 }, { 204, 204, 153 },
        { 204, 153, 153 }, { 204, 102, 153 }, { 204,  51, 153 }, { 204,   0, 153 },
        { 153, 255, 255 }, { 153, 204, 255 }, { 153, 153, 255 }, { 153, 102, 255 },
        { 153,  51, 255 }, { 153,   0, 255 }, { 153, 255, 204 }, { 153, 204, 204 },
        { 153, 153, 204 }, { 153, 102, 204 }, { 153,  51, 204 }, { 153,   0, 204 },
        { 153, 255, 153 }, { 153, 204, 153 }, { 153, 153, 153 }, { 153, 102, 153 },
        { 153,  51, 153 }, { 153,   0, 153 }, { 102, 255, 255 }, { 102, 204, 255 },
        { 102, 153, 255 }, { 102, 102, 255 }, { 102,  51, 255 }, { 102,   0, 255 },
        { 102, 255, 204 }, { 102, 204, 204 }, { 102, 153, 204 }, { 102, 102, 204 },
        { 102,  51, 204 }, { 102,   0, 204 }, { 102, 255, 153 }, { 102, 204, 153 },
        { 102, 153, 153 }, { 102, 102, 153 }, { 102,  51, 153 }, { 102,   0, 153 },
        {  51, 255, 255 }, {  51, 204, 255 }, {  51, 153, 255 }, {  51, 102, 255 },
        {  51,  51, 255 }, {  51,   0, 255 }, {  51, 255, 204 }, {  51, 204, 204 },
        {  51, 153, 204 }, {  51, 102, 204 }, {  51,  51, 204 }, {  51,   0, 204 },
        {  51, 255, 153 }, {  51, 204, 153 }, {  51, 153, 153 }, {  51, 102, 153 },
        {  51,  51, 153 }, {  51,   0, 153 }, {   0, 255, 255 }, {   0, 204, 255 },
        {   0, 153, 255 }, {   0, 102, 255 }, {   0,  51, 255 }, {   0,   0, 255 },
        {   0, 255, 204 }, {   0, 204, 204 }, {   0, 153, 204 }, {   0, 102, 204 },
        {   0,  51, 204 }, {   0,   0, 204 }, {   0, 255, 153 }, {   0, 204, 153 },
        {   0, 153, 153 }, {   0, 102, 153 }, {   0,  51, 153 }, {   0,   0, 153 },
        { 255, 255, 102 }, { 255, 204, 102 }, { 255, 153, 102 }, { 255, 102, 102 },
        { 255,  51, 102 }, { 255,   0, 102 }, { 255, 255,  51 }, { 255, 204,  51 },
        { 255, 153,  51 }, { 255, 102,  51 }, { 255,  51,  51 }, { 255,   0,  51 },
        { 255, 255,   0 }, { 255, 204,   0 }, { 255, 153,   0 }, { 255, 102,   0 },
        { 255,  51,   0 }, { 255,   0,   0 }, { 204, 255, 102 }, { 204, 204, 102 },
        { 204, 153, 102 }, { 204, 102, 102 }, { 204,  51, 102 }, { 204,   0, 102 },
        { 204, 255,  51 }, { 204, 204,  51 }, { 204, 153,  51 }, { 204, 102,  51 },
        { 204,  51,  51 }, { 204,   0,  51 }, { 204, 255,   0 }, { 204, 204,   0 },
        { 204, 153,   0 }, { 204, 102,   0 }, { 204,  51,   0 }, { 204,   0,   0 },
        { 153, 255, 102 }, { 153, 204, 102 }, { 153, 153, 102 }, { 153, 102, 102 },
        { 153,  51, 102 }, { 153,   0, 102 }, { 153, 255,  51 }, { 153, 204,  51 },
        { 153, 153,  51 }, { 153, 102,  51 }, { 153,  51,  51 }, { 153,   0,  51 },
        { 153, 255,   0 }, { 153, 204,   0 }, { 153, 153,   0 }, { 153, 102,   0 },
        { 153,  51,   0 }, { 153,   0,   0 }, { 102, 255, 102 }, { 102, 204, 102 },
        { 102, 153, 102 }, { 102, 102, 102 }, { 102,  51, 102 }, { 102,   0, 102 },
        { 102, 255,  51 }, { 102, 204,  51 }, { 102, 153,  51 }, { 102, 102,  51 },
        { 102,  51,  51 }, { 102,   0,  51 }, { 102, 255,   0 }, { 102, 204,   0 },
        { 102, 153,   0 }, { 102, 102,   0 }, { 102,  51,   0 }, { 102,   0,   0 },
        {  51, 255, 102 }, {  51, 204, 102 }, {  51, 153, 102 }, {  51, 102, 102 },
        {  51,  51, 102 }, {  51,   0, 102 }, {  51, 255,  51 }, {  51, 204,  51 },
        {  51, 153,  51 }, {  51, 102,  51 }, {  51,  51,  51 }, {  51,   0,  51 },
        {  51, 255,   0 }, {  51, 204,   0 }, {  51, 153,   0 }, {  51, 102,   0 },
        {  51,  51,   0 }, {  51,   0,   0 }, {   0, 255, 102 }, {   0, 204, 102 },
        {   0, 153, 102 }, {   0, 102, 102 }, {   0,  51, 102 }, {   0,   0, 102 },
        {   0, 255,  51 }, {   0, 204,  51 }, {   0, 153,  51 }, {   0, 102,  51 },
        {   0,  51,  51 }, {   0,   0,  51 }, {   0, 255,   0 }, {   0, 204,   0 },
        {   0, 153,   0 }, {   0, 102,   0 }, {   0,  51,   0 }, {  17,  17,  17 },
        {  34,  34,  34 }, {  68,  68,  68 }, {  85,  85,  85 }, { 119, 119, 119 },
        { 136, 136, 136 }, { 170, 170, 170 }, { 187, 187, 187 }, { 221, 221, 221 },
        { 238, 238, 238 }, { 192, 192, 192 }, { 128,   0,   0 }, { 128,   0, 128 },
        {   0, 128,   0 }, {   0, 128, 128 }, {   0,   0,   0 }, {   0,   0,   0 }
    };
    const ColorPalette palette_color_256 (palette_color_256_data, 232);
    
} // Anonymous namespace

void FormEditor::dither_pixels (Resources::Bitmap::BitmapType       type,
				const Resources::Bitmap::ImageData &image,
				Glib::RefPtr<Gdk::Pixbuf>          &pixbuf)
{
    const ColorPalette *palette = 0;
    
    switch (type)
    {
    case Resources::Bitmap::TYPE_MONO:
        palette = &palette_mono;
        break;
        
    case Resources::Bitmap::TYPE_GREY_4:
        palette = &palette_grey_4;
        break;
    case Resources::Bitmap::TYPE_GREY_16:
        palette = &palette_grey_16;
        break;

    case Resources::Bitmap::TYPE_COLOR_16:
        palette = &palette_color_16;
        break;
    case Resources::Bitmap::TYPE_COLOR_256:
        palette = &palette_color_256;
        break;

    case Resources::Bitmap::TYPE_COLOR_16K:
	// No dithering is done for high-color images
	pixbuf = image;
	break;
    }

    if (palette)
	palette->dither_pixels (image, pixbuf);
}
