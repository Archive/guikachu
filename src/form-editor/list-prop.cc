//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "list-prop.h"

#include <glib/gi18n.h>

#include "widgets/entry.h"
#include "widgets/num-entry.h"
#include "widgets/togglebutton.h"
#include "widgets/propertytable.h"
#include "widgets/string-list.h"
#include "widgets/font-combo.h"

#include <gtkmm/notebook.h>

#include "auto-resizeable-prop.h"
#include "usable-prop.h"

#include "property-ops-widget.h"

using namespace Guikachu::GUI::FormEditor;
using namespace Guikachu::Widgets;

ListProperties::ListProperties (List *res):
    WidgetProperties (res)
{
    using WidgetOps::PropChangeOpFactory;
    using WidgetOps::StringListOpFactory;
    
    Gtk::Notebook *notebook = new Gtk::Notebook;

    /* Page 1: General properties */
    GUI::PropertyTable *proptable = new GUI::PropertyTable ();
    Gtk::Widget *control;

    /* Resource ID */
    add_general_controls (*proptable);

    /* Font */
    control = new GUI::PropertyEditors::FontCombo (
	res->font,
	new PropChangeOpFactory<int> (_("Change font of %s"), res, res->font, false));
    proptable->add (_("_Font:"), *manage (control));

    /* Number of visible items */
    control = new GUI::PropertyEditors::NumEntry (
	1, 20, res->visible_items,
	new PropChangeOpFactory<int> (
	    _("Change number of visible items in %s"), res, res->visible_items, true));
    proptable->add (_("_Visible items:"), *manage (control));

    /* Usable */
    UsableProperties::add_controls (res, *proptable);

    /* Separator */
    proptable->add_separator ();

    /* Position */
    add_position_controls (*proptable);

    /* Width */
    AutoWidthProperties::add_controls (res, *proptable);

    // FIXME: Add uline accelerators to the tabs
    notebook->pages ().push_back (
	Gtk::Notebook_Helpers::TabElem (*manage (proptable),
					_("General")));

    /* Page 2: Item list */
    control = new GUI::PropertyEditors::StringList (res->items, new StringListOpFactory (
	_("Add \"%s\" to %s"),
	_("Remove \"%s\" from %s"),
	_("Change \"%s\" in %s"),
	_("Move item \"%s\" in %s"),
	res, res->items));

    notebook->pages ().push_back (
	Gtk::Notebook_Helpers::TabElem (*manage (control), _("Items")));
    
    notebook->show_all ();
    
    property_editor = notebook;
}

ListProperties::~ListProperties ()
{
    delete property_editor;
}

Gtk::Widget* ListProperties::get_editor ()
{
    return property_editor;
}
