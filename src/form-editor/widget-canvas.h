//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_WIDGET_CANVAS_H
#define GUIKACHU_FORM_EDITOR_WIDGET_CANVAS_H

namespace Guikachu
{
    namespace GUI
    {
	namespace FormEditor
	{
	    class WidgetCanvasItem;
	}
    }
}

#include <sigc++/signal.h>

#include "canvasitem.h"
#include "widget.h"

namespace Guikachu
{
    namespace GUI
    {
	namespace FormEditor
	{
	    class WidgetCanvasItem: public virtual CanvasItem
	    {
		Widget *widget;
		
	    protected:
		WidgetCanvasItem (Widget             *widget,
				  FooCanvasmm::Group &parent_group);		
	    public:
		virtual ~WidgetCanvasItem ();

                void update ();
                virtual void draw (FooCanvasmm::Group &group) const = 0;
                
	    protected:
		// Selection
		bool selected;
		FooCanvasmm::Item *selection_box;
		virtual FooCanvasmm::Item * create_selection_box ();

                // Colours
                Gdk::Color get_foreground_color (bool enabled) const;
                Gdk::Color get_background_color () const;
                Gdk::Color get_selection_color  () const;

	    public:
		sigc::signal2<void, guint, guint32> context_menu;
		
	    private:
		void update_hook ();
		
		void canvas_item_changed_cb ();
		
		void widget_selected_cb (bool      selected);
		void canvas_event_cb    (GdkEvent *event);
	    };
	}
    }
}

#endif /* !GUIKACHU_FORM_EDITOR_WIDGET_CANVAS_H */
