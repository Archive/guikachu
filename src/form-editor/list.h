//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_LIST_H
#define GUIKACHU_FORM_EDITOR_LIST_H

#include "widget.h"
#include "auto-resizeable.h"
#include "usable.h"

#include <vector>

namespace Guikachu
{
    namespace Widgets
    {
	class List: public Widget,
		    public AutoWidth,
                    public Usable
	{
	    sigc::signal0<void> manual_width_changed;
	    
	public:
	    List (Resources::Form *owner, const std::string &id, serial_t serial);
	    
	    Type get_type () const { return WIDGET_LIST; };
	    void apply_visitor (WidgetVisitor &visitor) { visitor.visit_widget (this); };
	    
	    Property<int>                       font;
	    Property<int>                       visible_items;
	    Property<std::vector<std::string> > items;

	    int get_height () const;

	protected:
	    int get_auto_width () const;
	};
    }
}

#endif /* !GUIKACHU_FORM_EDITOR_LIST_H */
