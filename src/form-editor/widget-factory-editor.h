//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_WIDGET_FACTORY_EDITOR_H
#define GUIKACHU_FORM_EDITOR_WIDGET_FACTORY_EDITOR_H

#include "form-editor/widget-visitor.h"

#include "form-editor/widget.h"
#include "form-editor/widget-prop.h"

#include "form-editor/button-prop.h"
#include "form-editor/checkbox-prop.h"
#include "form-editor/formbitmap-prop.h"
#include "form-editor/gadget-prop.h"
#include "form-editor/graffiti-prop.h"
#include "form-editor/label-prop.h"
#include "form-editor/label-prop.h"
#include "form-editor/list-prop.h"
#include "form-editor/popup-trigger-prop.h"
#include "form-editor/pushbutton-prop.h"
#include "form-editor/scrollbar-prop.h"
#include "form-editor/slider-prop.h"
#include "form-editor/selector-trigger-prop.h"
#include "form-editor/table-prop.h"
#include "form-editor/text-field-prop.h"

namespace Guikachu
{
    namespace GUI
    {
	namespace FormEditor
	{
	    class EditorFactoryVisitor: public WidgetVisitor
	    {
		WidgetProperties *result;
	
	    public:
		EditorFactoryVisitor (Widget *widget) :
		    result (0)
		    {
			widget->apply_visitor (*this);
		    }
		
		WidgetProperties * get_result () const
		    {
			return result;
		    }
		
#define GUIKACHU_WIDGET_EDITOR_VISIT(Type)				\
		void visit_widget (Widgets::Type *widget)		\
		{							\
		    result = new Type##Properties (widget);		\
		}

		GUIKACHU_WIDGET_EDITOR_VISIT(Label);
		GUIKACHU_WIDGET_EDITOR_VISIT(Button);
		GUIKACHU_WIDGET_EDITOR_VISIT(PushButton);
		GUIKACHU_WIDGET_EDITOR_VISIT(Graffiti);
		GUIKACHU_WIDGET_EDITOR_VISIT(SelectorTrigger);
		GUIKACHU_WIDGET_EDITOR_VISIT(Checkbox);
		GUIKACHU_WIDGET_EDITOR_VISIT(List);
		GUIKACHU_WIDGET_EDITOR_VISIT(PopupTrigger);
		GUIKACHU_WIDGET_EDITOR_VISIT(ScrollBar);
		GUIKACHU_WIDGET_EDITOR_VISIT(Slider);
		GUIKACHU_WIDGET_EDITOR_VISIT(TextField);
		GUIKACHU_WIDGET_EDITOR_VISIT(Table);
		GUIKACHU_WIDGET_EDITOR_VISIT(FormBitmap);
		GUIKACHU_WIDGET_EDITOR_VISIT(Gadget);
	    };
	    
	} // namespace FormEditor
    } // namespace GUI
} // namespace Guikachu

#endif /* !GUIKACHU_FORM_EDITOR_WIDGET_FACTORY_EDITOR_H */
