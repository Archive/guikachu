//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "canvas-grip.h"

#include <foocanvasmm/canvas.h>
#include <sigc++/adaptors/bind_return.h>
#include <math.h>

using namespace Guikachu::GUI::FormEditor;

CanvasGrip::CanvasGrip (FooCanvasmm::Group &parent_group,
			int                 x,
			int                 y,
                        Gdk::CursorType     cursor_):
    FooCanvasmm::Rect (parent_group),
    cursor (cursor_)
{   
    property_width_pixels () = 2;
    property_outline_color () = "black";
    property_fill_color () = "white";
    
    set_position (x, y);
    
    drag_context.dragging = false;
    
    signal_event ().connect (sigc::bind_return (
	sigc::mem_fun (*this, &CanvasGrip::canvas_event_cb), true));
}

void CanvasGrip::set_position (gdouble x, gdouble y)
{
    gdouble size = 3 / get_canvas ()->get_pixels_per_unit ();
    
    property_x1 () = x - size;
    property_x2 () = x + size;

    property_y1 () = y - size;
    property_y2 () = y + size;
}

void CanvasGrip::canvas_event_cb (GdkEvent *e)
{
    // Begin drag
    if (e->type == GDK_BUTTON_PRESS &&
        e->button.button == 1 &&
        !drag_context.dragging)
        drag_begin_impl (e);
    
    // Dragging
    if (e->type == GDK_MOTION_NOTIFY &&
        drag_context.dragging)
        drag_motion_impl (e);

    // Ending drag
    if (e->type == GDK_BUTTON_RELEASE &&
        e->button.button == 1 &&
        drag_context.dragging)
        drag_end_impl (e);
}

void CanvasGrip::drag_begin_impl (GdkEvent *e)
{
    double x = e->button.x;
    double y = e->button.y;

    drag_context.last_x = x;
    drag_context.last_y = y;

    drag_context.delta_remainder_x = 0;
    drag_context.delta_remainder_y = 0;
    
    grab (GDK_POINTER_MOTION_MASK | GDK_BUTTON_MOTION_MASK |
	  GDK_BUTTON_RELEASE_MASK | GDK_BUTTON_PRESS_MASK,
          Gdk::Cursor (cursor),
	  e->button.time);

    drag_context.dragging = true;

    drag_begin.emit ();
}

void CanvasGrip::drag_motion_impl (GdkEvent *e)
{
    double x = e->motion.x;
    double y = e->motion.y;
    
    double dx = x - drag_context.last_x + drag_context.delta_remainder_x;
    double dy = y - drag_context.last_y + drag_context.delta_remainder_y;
    
    // Store fractions of one pixel
    drag_context.delta_remainder_x = fmod (dx, 1.0);
    drag_context.delta_remainder_y = fmod (dy, 1.0);
    
    // Emit motion signal
    drag_motion.emit (int (dx), int (dy));
    
    drag_context.last_x = x;
    drag_context.last_y = y;
}

void CanvasGrip::drag_end_impl (GdkEvent *e)
{
    ungrab (e->button.time);

    drag_context.dragging = false;

    drag_end.emit ();
}
