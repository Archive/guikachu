//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "font.h"

#include <sstream>
#include <fstream>
#include <stdio.h> /* For sscanf */
#include <list>

namespace {
// Get next character from a string, with escape codes resolved
static char next_char (const std::string &text,
		       std::string::const_iterator &i)
{
    // Not an escape sequence
    if (*i != '\\')
	return *(i++);

    // Trailing '\'
    if (++i == text.end ())
	return '\\';

    // Check escape type
    char escape_type = *i;

    // Simple escape sequences
    switch (escape_type)
    {
    case 'a':
	i++;
	return '\x07';
    case 'b':
	i++;
	return '\x08';
    case 'e':
	i++;
	// FIXME: Figure out the value of '\e'
	return 'e';
    case 'f':
	i++;
	return '\x0c';
    case 'n':
	i++;
	return '\x0a';
    case 'r':
	i++;
	return '\n';
    case 't':
	i++;
	return '\x09';
    case 'v':
	i++;
	return '\x0b';
    case 'z':
	i++;
	// FIXME: Figure out the value of '\z'
	return 'z';
    }

#define IS_OCTAL(c)  ((c) >= '0' && (c) <= '7')
#define GET_OCTAL(c) ((c) - '0')

#define IS_HEX(c)  (((c) >= '0' && (c) <= '9') ||	\
		    ((c) >= 'a' && (c) <= 'f') ||	\
		    ((c) >= 'A' && (c) <= 'F'))

#define GET_HEX(c) (((c) >= '0' && (c) <= '9') ?	\
		     (c) - '0' :			\
		     ((c) >= 'a' && (c) <= 'f') ?	\
		      (c) - 'a' + 10 : (c) - 'A' + 10)
    
    
    // Octal numbers
    if (IS_OCTAL (escape_type))
    {
	char octal_value = 0;
	
	// Get next at most three octal numbers
	for (int j = 0; j < 3 && i != text.end (); i++, j++)
	{
	    if (!IS_OCTAL (*i))
		// Not an octal number
		break;

	    octal_value = octal_value * 8 + GET_OCTAL (*i);
	}

	return octal_value;
    }

    // Hexadecimal numbers
    if (escape_type == 'x')
    {
	char hex_value = 0;

	// Skip leading 0's
	while (*(++i) == '0' && i != text.end ());
	
	// Read at most two hexadecimal characters
	for (int j = 0; j < 2 && i != text.end (); i++, j++)
	{
	    if (!IS_HEX (*i))
		break;

	    hex_value = hex_value * 16 + GET_HEX (*i);
	}

	return hex_value;
    }

    // Unknown escape sequence: return escaped character
    return *(i++);
}

} // Anonymous namespace
    
using namespace Guikachu::GUI::FormEditor;

Font::Font ():
    line_height (0),
    empty_glyph (0)
{
}

void Font::parse (const std::string &filename)
{
    std::ifstream stream ((std::string (GUIKACHU_FONTDIR) + "/" + filename).c_str ());

    parse (stream);
}

void Font::parse (std::istream &stream)
{
    /* FIXME: Error handling, robustness issues regarding garbage
     * input */
    
    std::string curr_line;
    bool in_header = true;
    
    while (in_header)
    {
	getline (stream, curr_line);

	if (curr_line == "")
	    continue;
	
	std::stringstream curr_line_stream;
	std::string       param_name;
	int               param_val;
	
	curr_line_stream << curr_line;
	curr_line_stream >> param_name;
	if (!(curr_line_stream >> param_val))
	    param_val = 0;

	if (param_name == "ascent")
	    ascent = param_val;

	else if (param_name == "descent")
	    descent = param_val;
	
	else if (param_name == "GLYPH")
	{
	    in_header = false;
	    continue;
	}
    }

    while (!stream.eof ())
    {
	std::stringstream curr_line_stream;
	std::string       param_name;
	
	curr_line_stream << curr_line;
	curr_line_stream >> param_name;

	if (param_name == "GLYPH")
	{
	    std::string glyph_index;
	    char        glyph_char;

	    if (!(curr_line_stream >> glyph_index))
		glyph_index = "\\0";

	    /* Un-escape char index */
            if (glyph_index[0] == '\'')
            {
                sscanf (glyph_index.c_str (), "'%c'", &glyph_char);
            } else {
                glyph_char = atoi (glyph_index.c_str ());
            }

	    /* Parse current glyph */
	    Glyph curr_glyph (stream, line_height);

	    glyphs.insert (std::pair <char, Glyph> (glyph_char, curr_glyph));
	}
	
	getline (stream, curr_line);
    }

    empty_glyph = new Glyph (line_height, ascent);
}

Font::~Font ()
{
    delete empty_glyph;
}

const Glyph& Font::get_glyph (char c) const
{
    std::hash_map<char, Glyph>::const_iterator found_glyph = glyphs.find (c);
    if (found_glyph != glyphs.end ())
	return found_glyph->second;

    return *empty_glyph;
}

int Font::get_line_width (const std::string &text) const
{
    int width = 0;

    std::string::const_iterator i = text.begin ();
    while (i != text.end ())
    {
	char c = next_char (text, i);
	const Glyph &g = get_glyph (c);
	
	width += g.get_width ();
    }
    
    return width;
}

int Font::get_raw_line_width (const std::string &text) const
{
    int width = 0;
    
    for (std::string::const_iterator i = text.begin ();
	 i != text.end (); i++)
    {
	const Glyph &g = get_glyph (*i);
	
	width += g.get_width ();
    }
    
    return width;
}

void Font::get_text_extents (const std::string &text,
			     int               &width,
			     int               &height) const
{
    width = 0;
    height = line_height; // Even if the string is empty, a height of
                          // at least one line is returned
    
    std::string::const_iterator i = text.begin ();
    std::string current_line;
    while (i != text.end ())
    {
	char c = next_char (text, i);

	if (c != '\n')
	    current_line += c;
	else
	{
	    width = std::max<int> (width, get_raw_line_width (current_line));
	    height += line_height;
	    current_line.clear ();
	}
    }

    // Add last line
    width = std::max<int> (width, get_raw_line_width (current_line));
}
