//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_CANVAS_H
#define GUIKACHU_FORM_EDITOR_CANVAS_H

#include <foocanvasmm/canvas.h>

#include "form-editor/font.h"
#include "bitmap-res.h"
#include "resource-ref.h"

namespace Guikachu
{
    namespace GUI
    {
	namespace FormEditor
	{
            FooCanvasmm::Item * draw_text (FooCanvasmm::Group &group,
                                           const std::string  &text,
                                           const Font         &font,
                                           const Gdk::Color   &color,
                                           int                 x,
                                           int                 y,
                                           int                 clip_width,
                                           int                 clip_height,
                                           Gtk::AnchorType     anchor = Gtk::ANCHOR_CENTER,
                                           bool                multi_line = false);
            
            Glib::RefPtr<Gdk::Pixbuf> render_bitmap (const Resources::Bitmap::ImageData &image,
                                                     Resources::Bitmap::BitmapType       bitmap_type);

	    Glib::RefPtr<Gdk::Pixbuf> render_bitmapref (const Properties::ResourceRef &ref_prop,
                                                        bool                           color);

            Glib::RefPtr<Gdk::Pixbuf> render_text (const Font        &font,
                                                   const std::string &text,
                                                   const Gdk::Color  &color,
                                                   bool               multi_line = false);
            
	    // Use this instead of Rectangle canvas items because
	    // those are shoddy
	    FooCanvasmm::Item * draw_rectangle (FooCanvasmm::Group &group,
                                                int x1, int y1,
                                                int x2, int y2);
	    
            FooCanvasmm::Item * draw_bitmap (FooCanvasmm::Group                 &group,
                                             const Resources::Bitmap::ImageData &image,
                                             Resources::Bitmap::BitmapType       bitmap_type,
                                             const Gdk::Color                   &background_color,
                                             int                                 x,
                                             int                                 y,
                                             int                                 clip_width = 0,
                                             int                                 clip_height = 0,
                                             Gtk::AnchorType                     anchor = Gtk::ANCHOR_NW);

            FooCanvasmm::Item * draw_bitmapref (FooCanvasmm::Group            &group,
                                                const Properties::ResourceRef &ref_prop,
                                                bool                           color,
                                                const Gdk::Color              &background_color,
                                                int                            x,
                                                int                            y,
                                                int                            clip_width = 0,
                                                int                            clip_height = 0,
                                                Gtk::AnchorType                anchor = Gtk::ANCHOR_NW);

            Gdk::Color get_foreground_color (Target *target, bool enabled);
            Gdk::Color get_background_color (Target *target);
            Gdk::Color get_selection_color  (Target *target);

            guint32 get_pixel_value (const Gdk::Color &color);
	}
    }
}
    
#endif /* !GUIKACHU_FORM_EDITOR_CANVAS_H */
