//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <glib.h>
#include <glib/gi18n.h>

#include "widget-ops.h"
#include "io/xml-loader.h"
#include "io/xml-saver.h"

using namespace Guikachu::WidgetOps;

RemoveOp::RemoveOp (Widget *widget):
    manager (widget->get_manager ()),
    form_serial (widget->get_form ()->get_serial ()),
    widget_serial (widget->get_serial ()),
    type (widget->get_type ()),
    widget_id (widget->id)
{
    char *tmp = g_strdup_printf (_("Remove %s"), widget_id.c_str ());
    label = tmp;
    g_free (tmp);
    
    IO::XML::WidgetSaver saver (node);
    widget->apply_visitor (saver);
}

Glib::ustring RemoveOp::get_label () const
{
    return label;
}

void RemoveOp::undo ()
{
    Resources::Form *form = static_cast<Resources::Form*> (manager->get_resource (form_serial));

    Widget *widget = form->create_widget (type, widget_id, false, widget_serial);
    IO::XML::WidgetLoader loader (node);
    
    widget->apply_visitor (loader);
}

void RemoveOp::redo ()
{
    Resources::Form *form = static_cast<Resources::Form*> (manager->get_resource (form_serial));
    g_assert (form);
    Widget *widget = form->get_widget (widget_serial);
    g_assert (widget);
    
    form->remove_widget (widget);
}




CreateOp::CreateOp (Widget *widget):
    manager (widget->get_manager ()),
    form_serial (widget->get_form ()->get_serial ()),
    widget_serial (widget->get_serial ()),
    type (widget->get_type ()),
    widget_id (widget->id),
    x (widget->x), y (widget->y)
{
    char *tmp = g_strdup_printf (_("Create %s"), widget_id.c_str ());
    label = tmp;
    g_free (tmp);
}

Glib::ustring CreateOp::get_label () const
{
    return label;
}

void CreateOp::undo ()
{
    Resources::Form *form = static_cast<Resources::Form*> (manager->get_resource (form_serial));

    form->remove_widget (form->get_widget (widget_serial));
}

void CreateOp::redo ()
{
    Resources::Form *form = static_cast<Resources::Form*> (manager->get_resource (form_serial));

    Widget *widget = form->create_widget (type, widget_id, false, widget_serial);
    widget->x = x;
    widget->y = y;
}




MultiMoveOp::MultiMoveOp (const std::set<Widget*> &widgets, int dx_, int dy_) :
    dx (dx_), dy (dy_)
{
    g_assert (widgets.size ());
    
    Resources::Form *form = (*widgets.begin ())->get_form ();
    
    form_serial = form->get_serial ();
    manager = form->get_manager ();

    char *label_str = 0;

    if (widgets.size () == 1)
    {
	std::string widget_id = (*widgets.begin ())->id;
	
	label_str = g_strdup_printf (_("Move %s"), widget_id.c_str ());
    } else {
	std::string form_id = form->id;
	label_str = g_strdup_printf (_("Move widgets of %s"), form->id ().c_str ());
    }

    label = label_str;
    g_free (label_str);

    for (std::set<Widget*>::const_iterator i = widgets.begin ();
	 i != widgets.end (); ++i)
	widget_serials.insert ((*i)->get_serial ());
}

void MultiMoveOp::undo ()
{
    Resource *resource = manager->get_resource (form_serial);
    Resources::Form *form = static_cast<Resources::Form*> (resource);
    g_assert (form);

    for (std::set<serial_t>::const_iterator i = widget_serials.begin ();
	 i != widget_serials.end (); ++i)
    {
	Widget *widget = form->get_widget (*i);
	g_assert (widget);
	
	widget->x -= dx;
	widget->y -= dy;
    }
}

void MultiMoveOp::redo ()
{
    Resource *resource = manager->get_resource (form_serial);
    Resources::Form *form = static_cast<Resources::Form*> (resource);
    g_assert (form);

    for (std::set<serial_t>::const_iterator i = widget_serials.begin ();
	 i != widget_serials.end (); ++i)
    {
	Widget *widget = form->get_widget (*i);
	g_assert (widget);
	
	widget->x += dx;
	widget->y += dy;
    }
}
    



MultiRemoveOp::MultiRemoveOp (const std::set<Widget*> &widgets)
{
    g_assert (widgets.size ());
    
    Resources::Form *form = (*widgets.begin ())->get_form ();
    std::string form_id = form->id;
    
    for (std::set<Widget*>::const_iterator i = widgets.begin ();
	 i != widgets.end (); i++)
	real_ops.insert (new real_op_t (*i));

    if (widgets.size () == 1)
	label = (*real_ops.begin ())->get_label ();
    else {
	char *label_str = g_strdup_printf (_("Remove widgets from %s"), form_id.c_str ());
	label = label_str;
	g_free (label_str);
    }

}

MultiRemoveOp::~MultiRemoveOp ()
{
    for (real_op_list_t::iterator i = real_ops.begin (); i != real_ops.end (); i++)
	delete *i;
}

void MultiRemoveOp::undo ()
{
    for (real_op_list_t::iterator i = real_ops.begin (); i != real_ops.end (); i++)
    {
	(*i)->undo ();
    }
}

void MultiRemoveOp::redo ()
{
    for (real_op_list_t::iterator i = real_ops.begin (); i != real_ops.end (); i++)
	(*i)->redo ();
}
