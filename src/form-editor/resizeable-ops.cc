//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <glib/gi18n.h>

#include "resizeable-ops.h"
#include "auto-resizeable.h"

using namespace Guikachu;

ResizeOp::ResizeOp (Widgets::Resizeable *resizeable) :
    old_x (resizeable->get_x ()),
    old_y (resizeable->get_y ()),
    old_width (resizeable->width),
    old_height (resizeable->height),
    committed (false)
{
    Widgets::AutoWidth *auto_w = dynamic_cast<Widgets::AutoWidth*> (resizeable);
    if (auto_w)
	old_manual_width = auto_w->manual_width;

    Widgets::AutoHeight *auto_h = dynamic_cast<Widgets::AutoHeight*> (resizeable);
    if (auto_h)
	old_manual_height = auto_h->manual_height;
}

void ResizeOp::commit ()
{
    g_assert (!committed);

    Widgets::Resizeable *resizeable = get_resizeable ();
    
    new_x = resizeable->get_x ();
    new_y = resizeable->get_y ();
    new_width  = resizeable->width;
    new_height = resizeable->height;

    committed = true;
}

void ResizeOp::undo ()
{
    g_assert (committed);

    Widgets::Resizeable *resizeable = get_resizeable ();
    g_assert (resizeable);

    resizeable->set_x (old_x);
    resizeable->set_y (old_y);
    resizeable->width  = old_width;
    resizeable->height = old_height;

    Widgets::AutoWidth *auto_w = dynamic_cast<Widgets::AutoWidth*> (resizeable);
    if (auto_w)
	auto_w->manual_width = old_manual_width;

    Widgets::AutoHeight *auto_h = dynamic_cast<Widgets::AutoHeight*> (resizeable);
    if (auto_h)
	auto_h->manual_height = old_manual_height;
}

void ResizeOp::redo ()
{
    g_assert (committed);

    Widgets::Resizeable *resizeable = get_resizeable ();
    g_assert (resizeable);

    resizeable->set_x (new_x);
    resizeable->set_y (new_y);
    resizeable->width  = new_width;
    resizeable->height = new_height;
}


ResizeOpFactory::ResizeOpFactory (UndoManager &undo_manager_):
    undo_manager (undo_manager_)
{
}

void ResizeOpFactory::push_op (ResizeOp *op)
{
    g_assert (op);
    
    op->commit ();
    undo_manager.push (op);
}




WidgetOps::ResizeOp::ResizeOp (Widget *widget):
    Guikachu::ResizeOp (dynamic_cast<Widgets::Resizeable*> (widget)),
    manager (widget->get_manager ()),
    form_serial (widget->get_form ()->get_serial ()),
    widget_serial (widget->get_serial ())
{
    char *label_str = g_strdup_printf (_("Resize %s"), widget->id ().c_str ());
    label = label_str;
    g_free (label_str);
}

Glib::ustring WidgetOps::ResizeOp::get_label () const
{
    return label;
}

Widgets::Resizeable * WidgetOps::ResizeOp::get_resizeable ()
{
    Resource *res = manager->get_resource (form_serial);
    Resources::Form *form = static_cast<Resources::Form*> (res);
    g_assert (form);

    Widget *widget = form->get_widget (widget_serial);
    Widgets::Resizeable *resizeable = dynamic_cast<Widgets::Resizeable*> (widget);

    return resizeable;
}


WidgetOps::ResizeOpFactory::ResizeOpFactory (Widget *widget_) :
    Guikachu::ResizeOpFactory (widget_->get_manager ()->get_undo_manager ()),
    widget (widget_)
{
}

Guikachu::ResizeOp * WidgetOps::ResizeOpFactory::create_op ()
{
    return new WidgetOps::ResizeOp (widget);
}




ResourceOps::FormResizeOp::FormResizeOp (Widgets::Form *form):
    Guikachu::ResizeOp (form),
    manager (form->get_manager ()),
    form_serial (form->get_resource ()->get_serial ()),
    last_adapter (0)
{
    char *label_str = g_strdup_printf (_("Resize %s"), form->get_resource ()->id ().c_str ());
    label = label_str;
    g_free (label_str);
}

ResourceOps::FormResizeOp::~FormResizeOp ()
{
    delete last_adapter;
}

Glib::ustring ResourceOps::FormResizeOp::get_label () const
{
    return label;
}

Widgets::Resizeable * ResourceOps::FormResizeOp::get_resizeable ()
{
    Resource *res = manager->get_resource (form_serial);
    Resources::Form *form = static_cast<Resources::Form*> (res);
    g_assert (form);

    if (last_adapter)
	delete last_adapter;

    return last_adapter = new Widgets::Form (form);
}


ResourceOps::FormResizeOpFactory::FormResizeOpFactory (Widgets::Form *form_) :
    ResizeOpFactory (form_->get_manager ()->get_undo_manager ()),
    form (form_)
{
}

ResizeOp * ResourceOps::FormResizeOpFactory::create_op ()
{
    return new ResourceOps::FormResizeOp (form);
}
