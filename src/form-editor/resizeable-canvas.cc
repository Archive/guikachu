//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "resizeable-canvas.h"

#include <foocanvasmm/canvas.h>

#include "form-editor.h"
#include <algorithm>

#include "resizeable-ops.h"

using namespace Guikachu::GUI::FormEditor;

ResizeableCanvasItem::ResizeableCanvasItem (Widgets::Resizeable *resizeable_,
					    FooCanvasmm::Group  &parent_group_,
					    ResizeOpFactory     *op_factory_) :
    CanvasItem (resizeable_, parent_group_),
    resizeable (resizeable_),
    grip_group (0),
    op_factory (op_factory_),
    current_op (0)
{
    canvas_item_changed.connect (
	sigc::mem_fun (*this, &ResizeableCanvasItem::canvas_item_changed_cb));
    resizeable->selected.connect (
	sigc::mem_fun (*this, &ResizeableCanvasItem::selected_last_cb));
}

ResizeableCanvasItem::~ResizeableCanvasItem ()
{
}

void ResizeableCanvasItem::canvas_item_changed_cb ()
{
    if (grip_group)
    {
	int x1, y1, x2, y2;
	get_bounds (x1, y1, x2, y2);
	
	gdouble box_size = 2 / overlay_group->get_canvas ()->get_pixels_per_unit ();
	
	gdouble box_x1 = x1 - box_size;
	gdouble box_x2 = x2 + box_size;
	gdouble box_y1 = y1 - box_size;
	gdouble box_y2 = y2 + box_size;

	grip[0]->set_position (box_x1, box_y1);
	grip[1]->set_position (box_x1, box_y2);
	grip[2]->set_position (box_x2, box_y1);
	grip[3]->set_position (box_x2, box_y2);
    }
}

void ResizeableCanvasItem::create_resize_grips ()
{
    grip_group = new FooCanvasmm::Group (*overlay_group, 0, 0);

    grip[0] = create_grip (CORNER_NW);
    grip[1] = create_grip (CORNER_SW);
    grip[2] = create_grip (CORNER_NE);
    grip[3] = create_grip (CORNER_SE);

    // Update grip positions
    canvas_item_changed_cb ();
}

CanvasGrip *ResizeableCanvasItem::create_grip (Corner corner)
{
    Gdk::CursorType cursor = Gdk::ARROW;
    switch (corner)
    {
    case CORNER_NW:
        cursor = Gdk::TOP_LEFT_CORNER;
        break;
    case CORNER_SW:
        cursor = Gdk::BOTTOM_LEFT_CORNER;
        break;
    case CORNER_NE:
        cursor = Gdk::TOP_RIGHT_CORNER;
        break;
    case CORNER_SE:
        cursor = Gdk::BOTTOM_RIGHT_CORNER;
        break;
    }

    // TODO: Is it a good idea to use these custom mouse cursors?
    CanvasGrip *grip = new CanvasGrip (*grip_group, 0, 0);
    grip->drag_begin.connect (sigc::bind (
	sigc::mem_fun (*this, &ResizeableCanvasItem::grip_drag_begin_cb), corner));
    grip->drag_motion.connect (sigc::bind (
	sigc::mem_fun (*this, &ResizeableCanvasItem::grip_drag_motion_cb), corner));
    grip->drag_end.connect (sigc::bind (
	sigc::mem_fun (*this, &ResizeableCanvasItem::grip_drag_end_cb), corner));

    return grip;
}

void ResizeableCanvasItem::selected_last_cb (bool selected)
{
    if (selected)
    {
	if (!grip_group)
	    create_resize_grips ();
    } else {
	delete grip_group;
	grip_group = 0;
    }
}

void ResizeableCanvasItem::grip_drag_begin_cb (Corner corner)
{
    drag_context.start_width = resizeable->get_width ();
    drag_context.start_x = resizeable->get_x ();
    drag_context.dx = 0;
    
    drag_context.start_height = resizeable->get_height ();
    drag_context.start_y = resizeable->get_y ();
    drag_context.dy = 0;

    current_op = op_factory->create_op ();
    
    clicked.emit ();
}

void ResizeableCanvasItem::grip_drag_motion_cb (int dx, int dy,
						Corner corner)
{
    switch (corner)
    {
    case CORNER_NW:
    case CORNER_SW:
	drag_context.dx -= dx;
	break;

    case CORNER_NE:
    case CORNER_SE:
	drag_context.dx += dx;
	break;
    }
    
    switch (corner)
    {
    case CORNER_NW:
    case CORNER_NE:
	drag_context.dy -= dy;
	break;

    case CORNER_SW:
    case CORNER_SE:
	drag_context.dy += dy;
	break;
    }

    int real_dx = drag_context.dx;
    int real_dy = drag_context.dy;

    clip_delta (real_dx, real_dy, corner);

    // Keep aspect ratio if SHIFT is pressed
    bool keep_aspect_ratio;
    {
	Gdk::ModifierType modifiers;
	int dummy;
	
	parent_group.get_canvas ()->get_window ()->get_pointer (dummy, dummy, modifiers);
	keep_aspect_ratio = modifiers & Gdk::SHIFT_MASK;
    }

    if (keep_aspect_ratio)
    {
	/*
	 * 
	 * The algorithm used here looks for the (dwidth,dheight) tuple
	 * that meets two criterion:
	 * 
	 *   - dwidth/dheight = start_width/start_height
	 *   - distance((mouse_x, mouse_y), (dwidth,dheight)) is
	 *     minimized
	 *
	 * So basically we are looking for the intersection of two
	 * straight lines:
	 *
	 * e: dH = r * dW
	 * f: dH = -(1/r) * dW + (mouse_y + 1/r) * mouse_x
	 *
	 * where r := start_height / start_width
	 *
	 * Solving for the crossing of e and f gives:
	 *
	 * 	dW = (k * r) / (r * r + 1)
	 *
	 * where k := mouse_y + 1/r * mouse_x
	 *
	 */

	double ratio = double (drag_context.start_height) / double (drag_context.start_width);	
	double k = real_dy + real_dx / ratio;
	
	real_dx = int ((k * ratio) / (ratio * ratio + 1));

	int dx_min, dx_max;
	int dy_min, dy_max;

	get_delta_constraints (dx_min, dx_max, dy_min, dy_max, corner);

	if (dx_min < int (dy_min / ratio))
	    dx_min = int (dy_min / ratio);

	if (dx_max > int (dy_max / ratio))
	    dx_max = int (dy_max / ratio);
	
	if (real_dx < dx_min)
	    real_dx = dx_min;
	if (real_dx > dx_max)
	    real_dx = dx_max;

	real_dy = int (ratio * real_dx);
    }
    

    int new_width = drag_context.start_width + real_dx;
    int new_height = drag_context.start_height + real_dy;

    resizeable->width = new_width;
    resizeable->height = new_height;

    // Position delta is recalculated to work with widgets that
    // override the width and/or height property from get_display_*
    int pos_dx = resizeable->get_width  () - drag_context.start_width;
    int pos_dy = resizeable->get_height () - drag_context.start_height;

    if (corner == CORNER_NW || corner == CORNER_SW)
	resizeable->set_x (drag_context.start_x - pos_dx);

    if (corner == CORNER_NW || corner == CORNER_NE)
	resizeable->set_y (drag_context.start_y - pos_dy);
}

void ResizeableCanvasItem::get_delta_constraints (int &dx_min, int &dx_max,
						  int &dy_min, int &dy_max,
						  Corner corner)
{
    int screen_width = resizeable->get_manager ()->get_target ()->screen_width;
    int screen_height = resizeable->get_manager ()->get_target ()->screen_height;
    
    // New size should be at least 1x1
    dx_min = 1 - drag_context.start_width;
    dy_min = 1 - drag_context.start_height;

    // New size should not be larger than the screen
    dx_max = screen_width - drag_context.start_width;
    dy_max = screen_height - drag_context.start_height;

    // New position should be on screen
    if (corner == CORNER_NW || corner == CORNER_SW)
    {
	if (drag_context.start_x - (screen_width - 1) > dx_min)
	    dx_min = drag_context.start_x - (screen_width - 1);
	    
	if (drag_context.start_x < dx_max)
	    dx_max = drag_context.start_x;
    }

    if (corner == CORNER_NW || corner == CORNER_NE)
    {
	if (drag_context.start_y - (screen_height - 1) > dy_min)
	    dy_min = drag_context.start_y - (screen_height - 1);
	    
	if (drag_context.start_y < dy_max)
	    dy_max = drag_context.start_y;
    }
}


void ResizeableCanvasItem::clip_delta (int &dx, int &dy, Corner corner)
{
    int dx_min, dx_max;
    int dy_min, dy_max;

    get_delta_constraints (dx_min, dx_max, dy_min, dy_max, corner);

    if (dx < dx_min)
	dx = dx_min;
    if (dx > dx_max)
	dx = dx_max;

    if (dy < dy_min)
	dy = dy_min;
    if (dy > dy_max)
	dy = dy_max;
}

void ResizeableCanvasItem::grip_drag_end_cb (Corner corner)
{
    op_factory->push_op (current_op);
    
    released.emit ();
}
