//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_FORM_CANVAS_H
#define GUIKACHU_FORM_EDITOR_FORM_CANVAS_H

namespace Guikachu
{
    namespace GUI
    {
	namespace FormEditor
	{
	    class FormCanvasItem;
	}
    }
}

#include "form-res.h"
#include "form-editor/canvasitem.h"
#include "form-editor/resizeable-canvas.h"
#include "form-editor/form.h"

namespace Guikachu
{
    namespace GUI
    {
	namespace FormEditor
	{
	    class FormCanvasItem: public ResizeableCanvasItem
	    {		
		Resources::Form    *res;

		FooCanvasmm::Group *frame_group;
		FooCanvasmm::Group *title_group;
		FooCanvasmm::Group *widget_group;
		FooCanvasmm::Item  *bounding_box;

	    public:
		FormCanvasItem (Widgets::Form      *form_widget,
				FooCanvasmm::Group &parent_group);

		/* CanvasItem implementations */
		void update ();
		void move (int dx, int dy);

		FooCanvasmm::Group *get_widget_group ();

		sigc::signal2<void, guint, guint32> context_menu;
		sigc::signal1<void, bool> selected;
		
	    private:
		/* Drawing methods */
		void draw_frame ();
		void draw_title ();
		
		void draw_frame_modal    ();
		void draw_frame_nonmodal ();
		void draw_title_modal    ();
		void draw_title_nonmodal ();
		
		/* Callbacks */
		void canvas_event_cb (GdkEvent *e);
		void selected_cb     (bool selected);

	    protected:
		void get_bounds (int &x1, int &y1,
				 int &x2, int &y2);
                
                // Colours
                Gdk::Color get_foreground_color () const;
                Gdk::Color get_background_color () const;
                Gdk::Color get_selection_color  () const;
	    };
	}
    }
}

#endif /* !GUIKACHU_FORM_EDITOR_FORM_CANVAS_H */
