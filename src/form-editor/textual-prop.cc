//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "textual-prop.h"

#include <glib/gi18n.h>

#include "property-ops-widget.h"

#include "widgets/entry.h"
#include "widgets/font-combo.h"

using namespace Guikachu::GUI::FormEditor;

void TextualProperties::add_controls (Widgets::Textual   *res,
				      GUI::PropertyTable &proptable)
{
    Gtk::Widget *control;
    
    /* Label */
    control = new GUI::PropertyEditors::Entry (
	true, res->text, new WidgetOps::PropChangeOpFactory<std::string> (
	    _("Change text of %s"), dynamic_cast<Widget*>(res), res->text, true));
    proptable.add (_("_Text:"), *manage (control));

    /* Font */
    control = new GUI::PropertyEditors::FontCombo (
	res->font, new WidgetOps::PropChangeOpFactory<int> (
	    _("Change font of %s"), dynamic_cast<Widget*>(res), res->font, false));
    proptable.add (_("_Font:"), *manage (control));
}
