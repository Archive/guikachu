//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "formbitmap.h"

#include "form-editor.h"
#include "bitmap-res.h"

using Guikachu::Widgets::FormBitmap;
using namespace Guikachu::GUI::FormEditor;

FormBitmap::FormBitmap (Resources::Form   *owner,
                        const std::string &id,
                        serial_t           serial):
    Widget (owner, id, serial),
    Usable (changed),
    bitmap_id (bitmap_id_changed, owner->get_manager ())
{
    bitmap_id_changed.connect (sigc::mem_fun (*this, &FormBitmap::bitmap_id_changed_cb));
    bitmap_id_changed_cb ();
}

int FormBitmap::get_width () const
{
    Resource *bitmap_res = bitmap_id.resolve ();
    Resources::Bitmap *bitmap = dynamic_cast<Resources::Bitmap*> (bitmap_res);

    if (!bitmap || !bitmap->get_image ())
    {
        return 16;
    }
    
    return bitmap->get_image ()->get_width ();
}

int FormBitmap::get_height () const
{
    Resource *bitmap_res = bitmap_id.resolve ();
    Resources::Bitmap *bitmap = dynamic_cast<Resources::Bitmap*> (bitmap_res);

    if (!bitmap || !bitmap->get_image ())
    {
        return 16;
    }
    
    return bitmap->get_image ()->get_height ();
}

void FormBitmap::bitmap_id_changed_cb ()
{
    if (bitmap_changed_conn.connected ())
	bitmap_changed_conn.disconnect ();

    Resource *bitmap_res = bitmap_id.resolve ();
    if (bitmap_res)
	bitmap_changed_conn = bitmap_res->changed.connect (changed.make_slot ());
    
    changed.emit ();
}
