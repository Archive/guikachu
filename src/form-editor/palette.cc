//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "palette.h"

#include <glib/gi18n.h>

#include <gtkmm/table.h>
#include <gtkmm/image.h>
#include <gtkmm/box.h>
#include <gtkmm/separator.h>

#include "form-editor/widget-util.h"
#include "form-editor/widget-util-gui.h"

using namespace Guikachu::GUI;

namespace {
#include "pixmaps/none.xpm"
}
    
Palette::Palette ():
    sticky (false),
    inside_button_cb (false)
{
    set_title (_("Widget Palette"));
    property_allow_grow () = false;
    set_type_hint (Gdk::WINDOW_TYPE_HINT_UTILITY);
    set_role ("palette");

    Gtk::VBox *vbox = new Gtk::VBox;
    Gtk::HBox *hbox = new Gtk::HBox (false, 5);

    Gtk::ToggleButton *button_none;

    button_none = create_button (Widgets::WIDGET_NONE, _("Selector"), none_xpm);
    current_widget_name.set_alignment (0, 0.5);
    current_widget_name.set_size_request (10, -1);
    
    hbox->pack_start (*manage (button_none), false, false);
    hbox->pack_start (current_widget_name, true, true);
    vbox->pack_start (*manage (hbox), false, false);
    vbox->pack_start (*manage (new Gtk::HSeparator), false, false);
    
    Gtk::Table *table = new Gtk::Table (3, 5);

#define PALETTE_ADD_BUTTON(type,tip,x,y)				\
    table->attach (*manage (create_button (type, tip)),                 \
		   x, (x + 1), y, (y + 1),				\
		   Gtk::AttachOptions (), Gtk::AttachOptions ());	\
    
    PALETTE_ADD_BUTTON(Widgets::WIDGET_LABEL,
		       _("Static text label"),
		       0, 0);
    PALETTE_ADD_BUTTON(Widgets::WIDGET_TEXT_FIELD,
		       _("Editable, dynamic text field"),
		       1, 0);
    PALETTE_ADD_BUTTON(Widgets::WIDGET_GRAFFITI,
		       _("Graffiti state indicator"),
		       2, 0);    

    PALETTE_ADD_BUTTON(Widgets::WIDGET_BUTTON,
		       _("Clickable button"),
		       0, 1);
    PALETTE_ADD_BUTTON(Widgets::WIDGET_PUSHBUTTON,
		       _("Togglable button"),
		       1, 1);
    PALETTE_ADD_BUTTON(Widgets::WIDGET_SELECTOR_TRIGGER,
		       _("Selector trigger"),
		       2, 1);
    
    PALETTE_ADD_BUTTON(Widgets::WIDGET_CHECKBOX,
		       _("Check box"),
		       0, 2);

    PALETTE_ADD_BUTTON(Widgets::WIDGET_LIST,
		       _("List"),
		       2, 2);

    PALETTE_ADD_BUTTON(Widgets::WIDGET_POPUP_TRIGGER,
		       _("Popup trigger"),
		       1, 2);

    PALETTE_ADD_BUTTON(Widgets::WIDGET_SCROLLBAR,
		       _("Vertical scroll bar"),
		       0, 3);

    PALETTE_ADD_BUTTON(Widgets::WIDGET_TABLE,
		       _("Multi-column table"),
		       1, 3);

    PALETTE_ADD_BUTTON(Widgets::WIDGET_FORMBITMAP,
		       _("Bitmap"),
		       2, 3);

    PALETTE_ADD_BUTTON(Widgets::WIDGET_SLIDER,
		       _("Slider"),
		       0, 4);

    PALETTE_ADD_BUTTON(Widgets::WIDGET_GADGET,
		       _("Custom widget"),
		       1, 4);

#undef PALETTE_ADD_BUTTON

    vbox->add (*manage (table));

    add (*manage (vbox));

    set_selection (Widgets::WIDGET_NONE);
}

Gtk::ToggleButton * Palette::create_button (Widgets::Type       type,
					    const std::string  &tooltip,
                                            const char * const *icon_xpm)
{
    Gtk::ToggleButton *button = new Gtk::ToggleButton;		
    
    Glib::RefPtr<Gdk::Pixbuf> pixbuf = icon_xpm ?
        Gdk::Pixbuf::create_from_xpm_data (icon_xpm) : Widgets::get_type_pixbuf (type);
    
    button->set_relief (Gtk::RELIEF_NONE);
    button->add (*manage (new Gtk::Image (pixbuf)));
    button->property_user_data () = GINT_TO_POINTER (type);
    
    button->signal_clicked ().connect (
        sigc::bind (sigc::mem_fun (*this, &Palette::widget_button_cb), type));
    
    tips.set_tip (*button, tooltip);

    buttons.push_front (button);

    return button;
}

void Palette::palette_applied ()
{
    if (!sticky)
	set_selection (Widgets::WIDGET_NONE);
}

int Palette::delete_event_impl (GdkEventAny *e)
{
    hide ();
    return true;
}

void Palette::widget_button_cb (Widgets::Type type)
{
    if (inside_button_cb)
	return;
    
    Gdk::ModifierType modifiers;
    int dummy;
    
    get_window ()->get_pointer (dummy, dummy, modifiers);

    sticky = (modifiers & Gdk::CONTROL_MASK);
    set_selection (type);
}

void Palette::set_selection (Widgets::Type type)
{
    if (inside_button_cb)
	return;

    inside_button_cb = true;
    
    for (std::list<Gtk::ToggleButton*>::iterator i = buttons.begin ();
	 i != buttons.end (); i++)
    {
	int i_type = GPOINTER_TO_INT ((*i)->property_user_data ().get_value ());
	
	if (i_type == type)
	    (*i)->set_active (true);
	else
	    (*i)->set_active (false);
    }

    current_widget_name.set_markup ("<small>" + Widgets::display_name_from_type (type) + "</small>");
    
    inside_button_cb = false;
    
    palette_changed (type);
}
