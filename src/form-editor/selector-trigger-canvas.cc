//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "selector-trigger-canvas.h"

#include "form-editor.h"
#include "form-editor-canvas.h"
#include "resizeable-ops.h"

#include <gdkmm/bitmap.h>

using namespace Guikachu::GUI::FormEditor;

SelectorTriggerCanvasItem::SelectorTriggerCanvasItem (Widgets::SelectorTrigger *widget_,
						      FooCanvasmm::Group       &parent_group):
    CanvasItem (widget_, parent_group),
    WidgetCanvasItem (widget_, parent_group),
    ResizeableCanvasItem (widget_, parent_group, new WidgetOps::ResizeOpFactory (widget_)),
    widget (widget_)
{
    widget->bitmap_id.resource_changed.connect (sigc::mem_fun (*this, &SelectorTriggerCanvasItem::update));
}

void SelectorTriggerCanvasItem::draw (FooCanvasmm::Group &group) const
{
    int width = widget->get_width ();
    int height = widget->get_height ();

    /* Don't ask about the -1, I have no idea... */
    int x1 = widget->x;
    int x2 = x1 + width;
    int y1 = widget->y;
    int y2 = y1 + height;

    /* Data for the GdkBitmap to use as an outline stipple */
    static const char stipple_bits[] = { 0x01, 0x02, };
    static const Glib::RefPtr<Gdk::Bitmap> stipple =
	Gdk::Bitmap::create (stipple_bits, 2, 2);
    
    using namespace FooCanvasmm::Properties;
    *(FormEditor::draw_rectangle (group,
				  x1 - 1, y1 - 1,
				  x2, y2))
	<< width_units (1)
	<< outline_stipple (stipple)
	<< outline_color (get_foreground_color (widget->usable));

    if (widget->get_width () > 3)
    {
        int text_x = x1 + 3;
        int text_y = y1;
        
        if (widget->bitmap_id == "" && widget->selected_bitmap_id == "")
            draw_text (group,
                       widget->text, get_font (widget->font),
                       get_foreground_color (widget->usable),
                       text_x, text_y, width - 3, height,
                       Gtk::ANCHOR_WEST);
        else
            draw_bitmapref (group,
                            widget->bitmap_id, widget->get_manager ()->get_target ()->screen_color,
                            get_background_color (),
                            text_x, text_y, width - 3, height,
                            Gtk::ANCHOR_WEST);
    }
}

void SelectorTriggerCanvasItem::get_bounds (int &x1, int &y1,
					    int &x2, int &y2)
{
    int width = widget->get_width ();
    int height = widget->get_height ();
    
    x1 = widget->x - 1;
    x2 = x1 + width + 1;
    y1 = widget->y - 1;
    y2 = y1 + height + 1;
}
