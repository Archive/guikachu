//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "form-editor/form-editor-canvas.h"
#include "form-editor/form-editor.h"
#include "colorpalette.h"

#include <foocanvasmm/canvas.h>
#include <foocanvasmm/group.h>
#include <foocanvasmm/polygon.h>
#include <foocanvasmm/pixbuf.h>

#include "preferences.h"

#include "bitmapfamily-res.h"

#include <list>

using namespace Guikachu;
using namespace Guikachu::GUI;
using Guikachu::GUI::FormEditor::Font;
using Guikachu::GUI::FormEditor::Glyph;

namespace
{
    Glib::RefPtr<Gdk::Pixbuf> clip_pos_pixbuf (Glib::RefPtr<Gdk::Pixbuf>  pixbuf,
                                               bool                       text,
                                               int                       &x,
                                               int                       &y,
                                               int                        clip_width,
                                               int                        clip_height,
                                               Gtk::AnchorType            anchor);
            
    typedef std::list<const Glyph*> typeset_t;
    
    Glib::RefPtr<Gdk::Pixbuf> render_text_impl (const Font        &font,
                                                const std::string &text,
                                                const Gdk::Color  &color);
    
    Glib::RefPtr<Gdk::Pixbuf> render_text_multi_line_impl (const Font        &font,
                                                           const std::string &text,
                                                           const Gdk::Color  &color);

    void render_raw_line (const Font       &font,
			  const typeset_t  &typeset,
			  const Gdk::Color &color,
			  guint8           *target,
			  int               target_rowstride);

    Resources::Bitmap::ImageData bitmapref_to_image (const Properties::ResourceRef &ref_prop,
						     bool                           color,
						     Resources::Bitmap::BitmapType &bitmap_type);
}

Glib::RefPtr<Gdk::Pixbuf> FormEditor::render_text (const Font        &font,
                                                   const std::string &text,
                                                   const Gdk::Color  &color,
                                                   bool               multi_line)
{
    return multi_line ?
        render_text_multi_line_impl (font, text, color) :
        render_text_impl (font, text, color);
}

FooCanvasmm::Item * FormEditor::draw_text (FooCanvasmm::Group &group,
                                           const std::string  &text,
                                           const Font         &font,
                                           const Gdk::Color   &color,
                                           int                 x,
                                           int                 y,
                                           int                 clip_width,
                                           int                 clip_height,
                                           Gtk::AnchorType     anchor,
                                           bool                multi_line)
{
    FooCanvasmm::Group *text_group = new FooCanvasmm::Group (group, 0, 0);
    
    Glib::RefPtr<Gdk::Pixbuf> pixbuf = render_text (font, text, color, multi_line);
    if (!pixbuf)
	return 0;
    
    Glib::RefPtr<Gdk::Pixbuf> clipped_pixbuf =
        clip_pos_pixbuf (pixbuf, true, x, y, clip_width, clip_height, anchor);

    if (!clipped_pixbuf)
        return 0;
    
    int width = clipped_pixbuf->get_width ();
    int height = clipped_pixbuf->get_height ();

    // Draw the text
    FooCanvasmm::Pixbuf *pixbuf_item =
        new FooCanvasmm::Pixbuf (*text_group, x, y, clipped_pixbuf);
    pixbuf_item->property_interp_type () = Gdk::INTERP_NEAREST;

    // Draw a transparent background block (to get mouse events for
    // the whole area)
    FormEditor::draw_rectangle (*text_group,
				x, y,
				x + width, y + height);
    
    return text_group;
}

namespace // Text rendering utility functions
{

// Thar Be Dragons Here
 Glib::RefPtr<Gdk::Pixbuf> clip_pos_pixbuf (Glib::RefPtr<Gdk::Pixbuf>  pixbuf,
                                            bool                       text,
                                            int                       &x,
                                            int                       &y,
                                            int                        clip_width,
                                            int                        clip_height,
                                            Gtk::AnchorType            anchor)
{
    int clip_x = x;
    int clip_y = y;
    
    int width = pixbuf->get_width ();
    int height = pixbuf->get_height ();

    // Make sure (x,y) is at the upper-left corner
    
    // Horizontal offset
    switch (anchor)
    {
    case Gtk::ANCHOR_NORTH_WEST:
    case Gtk::ANCHOR_WEST:
    case Gtk::ANCHOR_SOUTH_WEST:
	break;
    case Gtk::ANCHOR_NORTH:
    case Gtk::ANCHOR_CENTER:
    case Gtk::ANCHOR_SOUTH:
	x += (clip_width - width + 1) / 2;
	break;
    case Gtk::ANCHOR_NORTH_EAST:
    case Gtk::ANCHOR_EAST:
    case Gtk::ANCHOR_SOUTH_EAST:
	x += (clip_width - width);
    }
    
    // Vertical offset
    int text_dy;
    switch (anchor)
    {
    case Gtk::ANCHOR_NORTH_WEST:
    case Gtk::ANCHOR_NORTH:
    case Gtk::ANCHOR_NORTH_EAST:
	break;
    case Gtk::ANCHOR_WEST:
    case Gtk::ANCHOR_CENTER:
    case Gtk::ANCHOR_EAST:
        // Ugly code, but this is how PalmOS works -- don't ask me why
        text_dy = clip_height - height;
        if (text && text_dy < 0 && (clip_height > (height + 1) / 2))
            --text_dy;
	if (!text)
	    ++text_dy;
        
        y += text_dy / 2;
	break;
    case Gtk::ANCHOR_SOUTH_WEST:
    case Gtk::ANCHOR_SOUTH:
    case Gtk::ANCHOR_SOUTH_EAST:
	y += (clip_height - height);
	break;
    }
    
    int clip_offset_x = std::max (0, clip_x - x);
    int clip_offset_y = std::max (0, clip_y - y);
    
    int clip_size_x = width - clip_offset_x;
    if (clip_width)
        clip_size_x -= std::max (0, (x + width - (clip_x + clip_width)));

    int clip_size_y = height - clip_offset_y;
    if (clip_height)
        clip_size_y -= std::max (0, (y + height - (clip_y + clip_height)));

    if (!(clip_size_x > 0 && clip_size_y > 0))
        return Glib::RefPtr<Gdk::Pixbuf> (0);
    
    Glib::RefPtr<Gdk::Pixbuf> dest = Gdk::Pixbuf::create (Gdk::COLORSPACE_RGB, true, 8, clip_size_x, clip_size_y);

    x += clip_offset_x;
    y += clip_offset_y;
    
    pixbuf->copy_area (clip_offset_x, clip_offset_y, clip_size_x, clip_size_y, dest, 0, 0);
    
    return dest;
}

// Get next character from a string, with escape codes resolved
static char next_char (const std::string &text,
		       std::string::const_iterator &i)
{
    // Not an escape sequence
    if (*i != '\\')
	return *(i++);

    // Trailing '\'
    if (++i == text.end ())
	return '\\';

    // Check escape type
    char escape_type = *i;

    // Simple escape sequences
    switch (escape_type)
    {
    case 'a':
	i++;
	return '\x07';
    case 'b':
	i++;
	return '\x08';
    case 'e':
	i++;
	// FIXME: Figure out the value of '\e'
	return 'e';
    case 'f':
	i++;
	return '\x0c';
    case 'n':
	i++;
	return '\x0a';
    case 'r':
	i++;
	return '\n';
    case 't':
	i++;
	return '\x09';
    case 'v':
	i++;
	return '\x0b';
    case 'z':
	i++;
	// FIXME: Figure out the value of '\z'
	return 'z';
    }

#define IS_OCTAL(c)  ((c) >= '0' && (c) <= '7')
#define GET_OCTAL(c) ((c) - '0')

#define IS_HEX(c)  (((c) >= '0' && (c) <= '9') ||	\
		    ((c) >= 'a' && (c) <= 'f') ||	\
		    ((c) >= 'A' && (c) <= 'F'))

#define GET_HEX(c) (((c) >= '0' && (c) <= '9') ?	\
		     (c) - '0' :			\
		     ((c) >= 'a' && (c) <= 'f') ?	\
		      (c) - 'a' + 10 : (c) - 'A' + 10)
    
    
    // Octal numbers
    if (IS_OCTAL (escape_type))
    {
	char octal_value = 0;
	
	// Get next at most three octal numbers
	for (int j = 0; j < 3 && i != text.end (); i++, j++)
	{
	    if (!IS_OCTAL (*i))
		// Not an octal number
		break;

	    octal_value = octal_value * 8 + GET_OCTAL (*i);
	}

	return octal_value;
    }

    // Hexadecimal numbers
    if (escape_type == 'x')
    {
	char hex_value = 0;

	// Skip leading 0's
	while (*(++i) == '0' && i != text.end ());
	
	// Read at most two hexadecimal characters
	for (int j = 0; j < 2 && i != text.end (); i++, j++)
	{
	    if (!IS_HEX (*i))
		break;

	    hex_value = hex_value * 16 + GET_HEX (*i);
	}

	return hex_value;
    }

    // Unknown escape sequence: return escaped character
    return *(i++);
}

void set_pixel (guint8           *pixel,
		const Gdk::Color &color)
{
    pixel[0] = color.get_red ()   >> 8;
    pixel[1] = color.get_green () >> 8;
    pixel[2] = color.get_blue ()  >> 8;
    pixel[3] = 255;
}

void render_raw_line (const Font       &font,
		      const typeset_t  &typeset,
		      const Gdk::Color &color,
		      guint8           *target,
		      int               target_rowstride)
{
    int line_height = font.get_line_height ();
    
    for (typeset_t::const_iterator i = typeset.begin (); i != typeset.end (); i++)
    {
	const Glyph *glyph = *i;	
	guint8 *curr_row = target;

	for (int y = 0; y < line_height; y++)
	{
	    guint8 *curr_pixel = curr_row;

	    for (int x = 0; x < glyph->get_width (); x++)
		{
		    if (glyph->get_pixel (x, y))
			set_pixel (curr_pixel, color);
		    
		    curr_pixel += 4;
		}
            
            curr_row += target_rowstride;
        }
	
        target += glyph->get_width () * 4;
    }
}

Glib::RefPtr<Gdk::Pixbuf> render_text_impl (const Font        &font,
					    const std::string &text,
					    const Gdk::Color  &color)
{
    if (text == "")
	return Glib::RefPtr<Gdk::Pixbuf> (0);

    int width = 0;
    int height = font.get_line_height ();
    typeset_t typeset;
    
    std::string::const_iterator i = text.begin ();
    while (i != text.end ())
    {
	char c = next_char (text, i);
	const Glyph &g = font.get_glyph (c);
	
	typeset.push_back (&g);
	width += g.get_width ();
    }

    Glib::RefPtr<Gdk::Pixbuf> pixbuf =
        Gdk::Pixbuf::create (Gdk::COLORSPACE_RGB, true, 8, width, height);
    guint8 *pixbuf_data = pixbuf->get_pixels ();
    int rowstride = pixbuf->get_rowstride ();
    
    memset (pixbuf_data, 0, height * rowstride);

    render_raw_line (font, typeset, color, pixbuf_data, rowstride);
    
    return pixbuf;
}

Glib::RefPtr<Gdk::Pixbuf> render_text_multi_line_impl (const Font        &font,
						       const std::string &text,
						       const Gdk::Color  &color)
{
    if (text == "")
	return Glib::RefPtr<Gdk::Pixbuf> (0);

    std::list <typeset_t> typesets;
    int width = 0;
    int height = 0;

    int current_width = 0;
    std::list <const Glyph*> current_typeset;
    int line_height = font.get_line_height ();
    
    std::string::const_iterator i = text.begin ();
    while (i != text.end ())
    {
	char c = next_char (text, i);
	if (c != '\n')
	{
	    const Glyph &g = font.get_glyph (c);
	    
	    current_typeset.push_back (&g);
	    current_width += g.get_width ();
	} else {
	    typesets.push_back (current_typeset);
	    current_typeset.clear ();

	    width = std::max<int> (width, current_width);
	    current_width = 0;
	    height += line_height;
	}
    }
    
    // Add last line
    typesets.push_back (current_typeset);
    width = std::max<int> (width, current_width);
    height += font.get_line_height ();
    
    Glib::RefPtr<Gdk::Pixbuf> pixbuf =
        Gdk::Pixbuf::create (Gdk::COLORSPACE_RGB, true, 8, width, height);
    guint8 *pixbuf_data = pixbuf->get_pixels ();
    memset (pixbuf_data, 0, height * pixbuf->get_rowstride ());
    
    int offset = 0;
    for (std::list <typeset_t>::const_iterator i = typesets.begin ();
         i != typesets.end (); i++)
    {
        render_raw_line (font, *i, color, pixbuf_data + offset, pixbuf->get_rowstride ());

        offset += (line_height * pixbuf->get_rowstride ());
    }

    return pixbuf;
}

} // anonymous namespace

namespace // Image rendering utility functions
{
    typedef std::pair<Guikachu::Resources::Bitmap::ImageData,
		      Guikachu::Resources::Bitmap::BitmapType> image_pair_t;
    
#define TRY_IMAGETYPE(t)                                                \
    image = res->get_image (Guikachu::Resources::Bitmap::TYPE_##t);     \
    if (image)                                                          \
        return image_pair_t (image, Guikachu::Resources::Bitmap::TYPE_##t);

    image_pair_t get_grey_image (Guikachu::Resources::BitmapFamily *res)
    {
        Guikachu::Resources::Bitmap::ImageData image;

        TRY_IMAGETYPE(GREY_16);
        TRY_IMAGETYPE(GREY_4);
        TRY_IMAGETYPE(MONO);

        return image_pair_t (image, Guikachu::Resources::Bitmap::TYPE_MONO);
    }
                                   
    image_pair_t get_color_image (Guikachu::Resources::BitmapFamily *res)
    {
        Guikachu::Resources::Bitmap::ImageData image;

        TRY_IMAGETYPE(COLOR_16K);
        TRY_IMAGETYPE(COLOR_256);
        TRY_IMAGETYPE(COLOR_16);

        return get_grey_image (res);
    }

    Resources::Bitmap::ImageData bitmapref_to_image (const Properties::ResourceRef &ref_prop,
						     bool                           color,
						     Resources::Bitmap::BitmapType &bitmap_type)
    {
        Resource *res = ref_prop.resolve ();
        Resources::Bitmap::ImageData image;
        
        if (Resources::Bitmap *bitmap = dynamic_cast<Resources::Bitmap*> (res))
        {
            bitmap_type = bitmap->bitmap_type;
            if (!color)
                bitmap_type = std::min (bitmap_type, Resources::Bitmap::TYPE_GREY_16);
            image = bitmap->get_image ();
        } else if (Resources::BitmapFamily *bitmapfamily = dynamic_cast<Resources::BitmapFamily*> (res)) {
            image_pair_t image_pair = color ?
                get_color_image (bitmapfamily) : get_grey_image (bitmapfamily);
            image = image_pair.first;
            bitmap_type = image_pair.second;
        }
        
        return image;
    }
    
} // anonymous namespace
    
FooCanvasmm::Item * FormEditor::draw_rectangle (FooCanvasmm::Group &group,
                                                int x1, int y1,
                                                int x2, int y2)
{
    FooCanvasmm::Points points;
    using FooCanvasmm::Point;

    points.push_back (Point (x1, y1));
    points.push_back (Point (x2, y1));
    points.push_back (Point (x2, y2));
    points.push_back (Point (x1, y2));
    
    return new FooCanvasmm::Polygon (group, points);
}

Glib::RefPtr<Gdk::Pixbuf> FormEditor::render_bitmap (const Resources::Bitmap::ImageData &image,
                                                     Resources::Bitmap::BitmapType       bitmap_type)
{
    if (!image)
        return Glib::RefPtr<Gdk::Pixbuf> ();

    unsigned int width = image->get_width ();
    unsigned int height = image->get_height ();

    Glib::RefPtr<Gdk::Pixbuf> pixbuf = Gdk::Pixbuf::create (
        Gdk::COLORSPACE_RGB, false, 8, width, height);

    dither_pixels (bitmap_type, image, pixbuf);
    
    return pixbuf->add_alpha (true, 0xFF, 0xFF, 0xFF);
    // return pixbuf;
}

FooCanvasmm::Item * FormEditor::draw_bitmap (FooCanvasmm::Group                 &group,
                                             const Resources::Bitmap::ImageData &image,
                                             Resources::Bitmap::BitmapType       bitmap_type,
                                             const Gdk::Color                   &background_color,
                                             int                                 x,
                                             int                                 y,
                                             int                                 clip_width,
                                             int                                 clip_height,
                                             Gtk::AnchorType                     anchor)
{
    Glib::RefPtr<Gdk::Pixbuf> pixbuf;
    Glib::RefPtr<Gdk::Pixbuf> pixbuf_img = render_bitmap (image, bitmap_type);
    if (!pixbuf_img)
        return 0;

    // Clip pixbuf to desired size
    pixbuf = clip_pos_pixbuf (pixbuf_img, false, x, y, clip_width, clip_height, anchor);

    if (!pixbuf)
        return 0;
    
    // Create parent group to store background + pixbuf
    FooCanvasmm::Group *ret_val = new FooCanvasmm::Group (group, x, y);

    // Create clickable background
    using namespace FooCanvasmm::Properties;
    *(draw_rectangle (*ret_val, 0, 0, pixbuf->get_width (), pixbuf->get_height ()))
        << fill_color (background_color);

    // Create pixbuf item
    new FooCanvasmm::Pixbuf (*ret_val, 0, 0, pixbuf);

    return ret_val;
}

FooCanvasmm::Item * FormEditor::draw_bitmapref (FooCanvasmm::Group            &group,
                                                const Properties::ResourceRef &ref_prop,
                                                bool                           color,
                                                const Gdk::Color              &background_color,
                                                int                            x,
                                                int                            y,
                                                int                            clip_width,
                                                int                            clip_height,
                                                Gtk::AnchorType                anchor)
{
    Resources::Bitmap::BitmapType bitmap_type;
    Resources::Bitmap::ImageData image = bitmapref_to_image (ref_prop, color, bitmap_type);
    
    if (!image)
        return 0;
    
    return draw_bitmap (group, image, bitmap_type, background_color,
                        x, y, clip_width, clip_height, anchor);
}

Glib::RefPtr<Gdk::Pixbuf> FormEditor::render_bitmapref (const Properties::ResourceRef &ref_prop,
                                                        bool                           color)
{
    Resources::Bitmap::BitmapType bitmap_type;
    Resources::Bitmap::ImageData image = bitmapref_to_image (ref_prop, color, bitmap_type);

    return render_bitmap (image, bitmap_type);
}


Gdk::Color FormEditor::get_foreground_color (Target *target, bool enabled)
{
    if (target->screen_color)
        return enabled ? Gdk::Color ("#000000") : Gdk::Color ("#757575");
    else
        return enabled ?
            Gdk::Color (Preferences::FormEditor::get_color_fg ()) :
            Gdk::Color (Preferences::FormEditor::get_color_disabled ());
}

Gdk::Color FormEditor::get_background_color (Target *target)
{
    if (target->screen_color)
        return Gdk::Color ("#ffffff");
    else
        return Gdk::Color (Preferences::FormEditor::get_color_bg ());
}

Gdk::Color FormEditor::get_selection_color (Target *target)
{
    if (target->screen_color)
	return Gdk::Color ("#7d94ca");
    else
	return Gdk::Color (Preferences::FormEditor::get_color_selection ());
}

guint32 FormEditor::get_pixel_value (const Gdk::Color &color)
{
    guchar r = color.get_red ()   >> 8;
    guchar g = color.get_green () >> 8;
    guchar b = color.get_blue ()  >> 8;
    guchar a = 255;

    return (r << 24) | (g << 16) | (b << 8) | a;

}
