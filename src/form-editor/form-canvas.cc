//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "form-editor/form-canvas.h"

#include <glib/gi18n.h>

#include <gdkmm/pixbuf.h>
#include <foocanvasmm/rect.h>
#include <foocanvasmm/pixbuf.h>
#include <foocanvasmm/polygon.h>
#include "screen-canvas.h"

#include <sigc++/adaptors/bind_return.h>

#include <list>
#include <math.h>

#include "preferences.h"

#include "form-editor/form-editor.h"
#include "form-editor/form-editor-canvas.h"
#include "form-editor/resizeable-ops.h"

using namespace Guikachu::GUI::FormEditor;

#define GUIKACHU_WINDOW_TITLE_HEIGHT 13

FormCanvasItem::FormCanvasItem (Widgets::Form      *form_widget,
				FooCanvasmm::Group &parent_group):
    CanvasItem (form_widget, parent_group),
    ResizeableCanvasItem (form_widget, parent_group,
			  new ResourceOps::FormResizeOpFactory (form_widget)),
    res (form_widget->get_resource ()),
    frame_group (0),
    title_group (0),
    widget_group (new FooCanvasmm::Group (*item_group, 0, 0))
{
    FooCanvasmm::Item *background_item = new ScreenCanvasItem (parent_group, res->get_manager ()->get_target ());
    background_item->signal_event ().connect (
	sigc::bind_return (sigc::mem_fun (*this, &FormCanvasItem::canvas_event_cb), false));
    background_item->lower_to_bottom ();
    
    res->changed.connect (sigc::mem_fun (*this, &FormCanvasItem::update));
    update ();
}

void FormCanvasItem::update ()
{
    draw_frame ();
    draw_title ();

    item_group->property_x () = 0;
    item_group->property_y () = 0;
    item_group->move (res->x, res->y);

    widget_group->raise_to_top ();
    overlay_group->raise_to_top ();
}

void FormCanvasItem::move (int dx, int dy)
{
    item_group->move (dx, dy);
}

FooCanvasmm::Group *FormCanvasItem::get_widget_group ()
{
    return widget_group;
}

void FormCanvasItem::draw_frame ()
{
    delete frame_group;

    frame_group = new FooCanvasmm::Group (*draw_group, 0, 0);

    if (res->modal)
	draw_frame_modal ();
    else
	if (res->frame)
	    draw_frame_nonmodal ();
}

void FormCanvasItem::draw_frame_nonmodal ()
{
    FooCanvasmm::Rect *frame_rect = new FooCanvasmm::Rect (*frame_group, -1, -1, res->width, res->height);
    
    frame_rect->property_width_units () = 1;
    frame_rect->property_outline_color_gdk () = get_foreground_color ();
    // frame_rect->property_fill_color () = "";
}

static Glib::RefPtr<Gdk::Pixbuf> draw_solid (const Gdk::Color &color, int width, int height)
{
    guint8 red   = color.get_red ()   >> 8;
    guint8 green = color.get_green () >> 8;
    guint8 blue  = color.get_blue ()  >> 8;
    
    Glib::RefPtr<Gdk::Pixbuf> pixbuf =
	Gdk::Pixbuf::create (Gdk::COLORSPACE_RGB, true, 8, width, height);
    pixbuf->fill (red << 24 | green << 16 | blue << 8 | 255);
    
    return pixbuf;
}

void FormCanvasItem::draw_frame_modal ()
{
    int width = res->width - 1;
    int height = res->height - 1;

    Glib::RefPtr<Gdk::Pixbuf> pixbuf;
    Gdk::Color color (get_foreground_color ());
    
    pixbuf = draw_solid (color, width + 1, 2);
    new FooCanvasmm::Pixbuf (*frame_group, 0, -2,         pixbuf);
    new FooCanvasmm::Pixbuf (*frame_group, 0, height + 1, pixbuf);

    pixbuf = draw_solid (color, 2, height + 1);
    new FooCanvasmm::Pixbuf (*frame_group, -2,        0, pixbuf);
    new FooCanvasmm::Pixbuf (*frame_group, width + 1, 0, pixbuf);

    pixbuf = draw_solid (color, 2, 2);
    new FooCanvasmm::Pixbuf (*frame_group, -1,    -1,     pixbuf);
    new FooCanvasmm::Pixbuf (*frame_group, width, -1,     pixbuf);
    new FooCanvasmm::Pixbuf (*frame_group, width, height, pixbuf);
    new FooCanvasmm::Pixbuf (*frame_group, -1,    height, pixbuf);
}

void FormCanvasItem::draw_title ()
{
    delete title_group;
    title_group = new FooCanvasmm::Group (*draw_group, 0, 0);

    set_canvas_item (title_group);
    title_group->signal_event ().connect (sigc::bind_return (
	sigc::mem_fun (*this, &FormCanvasItem::canvas_event_cb), true));
    
    if (res->modal)
	draw_title_modal ();
    else
	draw_title_nonmodal ();
}

void FormCanvasItem::draw_title_nonmodal ()
{
    FooCanvasmm::Points points;
    int title_width;
    const Font &font = get_title_font ();

    title_width = get_line_width (1, res->title); /* Title is font#1 (FIXME) */
    title_width = std::min<int> (title_width + 4, res->width);

    int max_width = res->width;
    int max_height = std::min<int> (res->height, GUIKACHU_WINDOW_TITLE_HEIGHT);
    
    // NW corner
    points.push_back (FooCanvasmm::Point (0, 1));
    points.push_back (FooCanvasmm::Point (1, 0));

    // NE corner
    points.push_back (FooCanvasmm::Point (title_width,     0));
    points.push_back (FooCanvasmm::Point (title_width + 1, 1));

    // SE corner
    points.push_back (FooCanvasmm::Point ((title_width + 1), (GUIKACHU_WINDOW_TITLE_HEIGHT + 1)));
    // SW corner
    points.push_back (FooCanvasmm::Point (0, (GUIKACHU_WINDOW_TITLE_HEIGHT + 1)));
    // Back to NW
    points.push_back (FooCanvasmm::Point (0, 1));

    {
	using namespace FooCanvasmm::Properties;

	*(new FooCanvasmm::Polygon (*title_group, points))
	    << width_units (1)
	    << fill_color (get_foreground_color ());

	draw_text (*title_group, res->title, font,
                   get_background_color (),
                   3, 2, max_width, max_height,
                   Gtk::ANCHOR_NW);
	
	*(new FooCanvasmm::Rect (*title_group,
                                 0, GUIKACHU_WINDOW_TITLE_HEIGHT,
                                 res->width - 1, GUIKACHU_WINDOW_TITLE_HEIGHT + 1))
	    << width_units (1)
	    << fill_color (get_foreground_color ());
    }
}

void FormCanvasItem::draw_title_modal ()
{
    const Font &font = get_title_font ();

    int title_height = std::min<int> (res->height, 11);

    int max_width = res->width;
    int max_height = std::min<int> (res->height, 11);

    {
	using namespace FooCanvasmm::Properties;
    
	*(new FooCanvasmm::Rect (*title_group, 0, 0, res->width, title_height))
	    << width_units (1)
	    << fill_color (get_foreground_color ());

	draw_text (*title_group, res->title, font,
                   get_background_color (),
                   0, 0, max_width, max_height, Gtk::ANCHOR_N);
    }
}

void FormCanvasItem::selected_cb (bool selected)
{
}

void FormCanvasItem::canvas_event_cb (GdkEvent *e)
{
    // Context menu
    if ((e->type == GDK_BUTTON_PRESS) &&
	(e->button.button == 3))
	context_menu.emit (e->button.button, e->button.time);
}

void FormCanvasItem::get_bounds (int &x1, int &y1,
				 int &x2, int &y2)
{
    x1 = 0;
    y1 = 0;

    x2 = res->width;
    y2 = res->height;
}

Gdk::Color FormCanvasItem::get_foreground_color () const
{
    if (res->get_manager ()->get_target ()->screen_color)
        return Gdk::Color ("#330099");
    else
        return Gdk::Color (Preferences::FormEditor::get_color_fg ());
}

Gdk::Color FormCanvasItem::get_background_color () const
{
    if (res->get_manager ()->get_target ()->screen_color)
        return Gdk::Color ("#ffffff");
    else
        return Gdk::Color (Preferences::FormEditor::get_color_bg ());
}

Gdk::Color FormCanvasItem::get_selection_color () const
{
    return Gdk::Color (Preferences::FormEditor::get_color_selection ());
}
