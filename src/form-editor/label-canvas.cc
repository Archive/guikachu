//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "label-canvas.h"

#include "form-editor.h"
#include "form-editor-canvas.h"

using namespace Guikachu::GUI::FormEditor;

LabelCanvasItem::LabelCanvasItem (Widgets::Label     *widget_,
				  FooCanvasmm::Group &parent_group):
    CanvasItem (widget_, parent_group),
    WidgetCanvasItem (widget_, parent_group),
    widget (widget_)
{
}

void LabelCanvasItem::draw (FooCanvasmm::Group &group) const
{
    // Avoid 0x0 rendering, as that would make it impossible to select
    // the widget
    std::string text = widget->text;
    if (text == "")
	text = " ";

    draw_text (group,
	       text, get_font (widget->font),
	       get_foreground_color (widget->usable),
	       widget->x, widget->y, 0, 0,
               Gtk::ANCHOR_NW,
	       true /* Labels can have multiple lines of text */);
}
