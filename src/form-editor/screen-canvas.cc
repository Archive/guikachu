//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "screen-canvas.h"
#include "form-editor-canvas.h"
#include <foocanvasmm/rect.h>

#include "preferences.h"

using namespace Guikachu::GUI;
using namespace Guikachu::GUI::FormEditor;

ScreenCanvas::ScreenCanvas (Guikachu::Target *target_):
    target (target_)
{
    set_center_scroll_region (true);
        
    target->changed.connect (sigc::mem_fun (*this, &ScreenCanvas::update));    
    update ();
}

void ScreenCanvas::update ()
{
    float zoom_factor = get_pixels_per_unit ();
    int screen_width = target->screen_width;
    int screen_height = target->screen_height;
    
    set_scroll_region (0, 0, screen_width - 1, screen_height - 1);
    
    
    set_size_request (int (screen_width * zoom_factor),
                      int (screen_height * zoom_factor));
    
}

ScreenCanvasItem::ScreenCanvasItem (FooCanvasmm::Group &parent_group,
                                    Guikachu::Target   *target_):
    FooCanvasmm::Group (parent_group, 0, 0),
    target (target_),
    item (0)
{
    Preferences::FormEditor::colors_changed.connect (sigc::mem_fun (*this, &ScreenCanvasItem::update));
    target->changed.connect (sigc::mem_fun (*this, &ScreenCanvasItem::update));    
    update ();
}

void ScreenCanvasItem::update ()
{
    delete item;

    int screen_width  = target->screen_width;
    int screen_height = target->screen_height;
    
    item = new FooCanvasmm::Rect (*this, 0, 0, screen_width - 1, screen_height - 1);

    using namespace FooCanvasmm::Properties;
    *item << width_pixels (0)
          << fill_color (get_background_color (target));
}
