//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "widget-prop.h"

#include <glib/gi18n.h>

#include "property-ops-widget.h"

#include "widgets/entry.h"
#include "widgets/pos-entry.h"

using namespace Guikachu::GUI::FormEditor;

WidgetProperties::WidgetProperties (Widget *widget_) :
    widget (widget_)
{
}

void WidgetProperties::add_position_controls (GUI::PropertyTable &proptable)
{
    using WidgetOps::PropChangeOpFactory;
    
    Gtk::Widget *control;

    /* X */
    control = new GUI::PropertyEditors::XPosEntry (
	widget->get_manager (), widget->x,
	new PropChangeOpFactory<int> (_("Move %s"), widget, widget->x, true));
    proptable.add (_("_X:"), *manage (control),
		   _("Horizontal position of the widget, relative "
		     "to the parent form"));

    /* Y */
    control = new GUI::PropertyEditors::YPosEntry (
	widget->get_manager (), widget->y,
	new PropChangeOpFactory<int> (_("Move %s"), widget, widget->y, true));
    proptable.add (_("_Y:"), *manage (control),
		   _("Vertical position of the widget, relative "
		     "to the parent form"));

}

void WidgetProperties::add_general_controls (GUI::PropertyTable &proptable)
{
    Gtk::Widget *control;

    /* Resource ID */
    control = new GUI::PropertyEditors::Entry (
	false, widget->id, new WidgetOps::RenameOpFactory (widget));
    proptable.add (_("Resource _ID:"), *manage (control));
}
