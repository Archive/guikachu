//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "font.h"

#include <sstream>

using namespace Guikachu::GUI::FormEditor;

#define GLYPH_ON  '#'
#define GLYPH_OFF '-'

Glyph::Glyph (int height, int ascent):
    width (5)
{
    bitmap = g_new0 (bool, height * width);
    
    for (int i = 0; i < height; i++)
    {
	bitmap[i] = g_new0 (bool, width);
    }

    int y1 = std::min (abs (ascent - 8), abs (ascent - 1));
    int y2 = std::max (abs (ascent - 8), abs (ascent - 1));

    for (int x = 0; x < width; x++)
    {
	bitmap[y1 * width + x] = true;
	bitmap[y2 * width + x] = true;
    }

    for (int y = y1; y < y2; y++)
    {
	bitmap[y * width + 0] = true;
	bitmap[y * width + 4] = true;
    }
}

Glyph::Glyph (std::istream &stream,
	      int          &height): 
    width (0),
    offset (0),
    bitmap (0)
{    
    std::string curr_line;
    bool        in_bitmap = true;
    bool        in_header = true;

    while (in_header)
    {
	getline (stream, curr_line);

	if (curr_line[0] == GLYPH_ON ||
	    curr_line[0] == GLYPH_OFF)
	{
	    in_header = false;
	    continue;
	}

	std::stringstream curr_line_stream;
	std::string       param_name;
	int               param_val;
	
	curr_line_stream << curr_line;
	curr_line_stream >> param_name;
	if (!(curr_line_stream >> param_val))
	    param_val = 0;

	if (param_name == "OFFSET")
	    offset = param_val;
    }

    int row = 0;
    
    while (in_bitmap)
    {
	if (curr_line == "")
	{
	    in_bitmap = false;
	    continue;
	}

	if (!width)
	    width = curr_line.length ();
	
	bitmap = (bool*) g_realloc (bitmap, ((row + 1) * width) * sizeof (bool));
	
	for (int i = 0; i < width; i++)
	    bitmap[row * width  + i] = (curr_line[i] == GLYPH_ON);
	
	getline (stream, curr_line);
	row++;
    }

    if (!height)
	height = row;
}

Glyph::~Glyph ()
{
#if 0
    g_free (bitmap);
#endif
}
