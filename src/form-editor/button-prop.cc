//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "button-prop.h"

#include <glib/gi18n.h>

#include "widgets/num-entry.h"
#include "widgets/togglebutton.h"
#include "widgets/propertytable.h"

#include <gtkmm/combobox.h>
#include <gtkmm/liststore.h>

#include "auto-resizeable-prop.h"
#include "usable-prop.h"
#include "disabled-prop.h"
#include "textual-prop.h"
#include "graphical-prop.h"

#include "property-ops-widget.h"

using namespace Guikachu;
using namespace Guikachu::GUI::FormEditor;

namespace
{
    class ButtonFrameCombo: public Gtk::ComboBox
    {
        Widgets::Button *res;

        Gtk::TreeModelColumn<Widgets::Button::FrameType> col_type;
        Gtk::TreeModelColumn<Glib::ustring>              col_label;
        
        Glib::RefPtr<Gtk::ListStore> store;
        
    public:
        ButtonFrameCombo (Widgets::Button *res);
                
    private:
        bool update_block;
        
        void update ();
        void changed_cb ();

        void add_type (const Glib::ustring &label, Widgets::Button::FrameType type);
    };
} // anonymous namespace

ButtonProperties::ButtonProperties (Widgets::Button *res):
    WidgetProperties (res)
{
    using Gtk::manage;
    
    using WidgetOps::PropChangeOpFactory;
    
    GUI::PropertyTable *proptable = new GUI::PropertyTable ();
    Gtk::Widget *control;

    /* Resource ID */
    add_general_controls (*proptable);

    /* Label */
    TextualProperties::add_controls (res, *proptable);
    
    /* Bitmaps */
    GraphicalProperties::add_controls (res, *proptable);
    
    /* Frame type */
    control = new ButtonFrameCombo (res);
    proptable->add (_("_Frame type:"), *manage (control));

    /* Repeat */
    control = new GUI::PropertyEditors::ToggleButton (
	res->repeat,
	new PropChangeOpFactory<bool> (_("Toggle repeating of %s"), res, res->repeat, false));
    proptable->add (_("_Repeating:"), *manage (control),
		    _("Repeating buttons fire multiple ButtonPress "
		      "signals when kept pressed"));

    /* Anchor right */
    control = new GUI::PropertyEditors::ToggleButton (
	res->anchor_right,
	new PropChangeOpFactory<bool> (_("Change alignment of %s"), res, res->anchor_right, false));
    proptable->add (_("_Anchor right:"), *manage (control),
		    _("Keep right aligned when changing label "
		      "text at run-time"));
    
    /* Usable */
    UsableProperties::add_controls (res, *proptable);
    DisabledProperties::add_controls (res, *proptable);
    
    /* Separator */
    proptable->add_separator ();

    /* Position */
    add_position_controls (*proptable);
    
    /* Size */
    AutoWidthProperties::add_controls  (res, *proptable);
    AutoHeightProperties::add_controls (res, *proptable);
    
    proptable->show ();
    property_editor = proptable;
}

ButtonProperties::~ButtonProperties ()
{
    delete property_editor;
}

Gtk::Widget* ButtonProperties::get_editor ()
{
    return property_editor;
}



ButtonFrameCombo::ButtonFrameCombo (Widgets::Button *res_):
    res (res_)
{
    Gtk::TreeModel::ColumnRecord cols;
    cols.add (col_type);
    cols.add (col_label);
    
    store = Gtk::ListStore::create (cols);

    // Fill the model
    add_type (_("None"),   Widgets::Button::FRAME_NONE);
    add_type (_("Simple"), Widgets::Button::FRAME_SIMPLE);
    add_type (_("Bold"),   Widgets::Button::FRAME_BOLD);

    // Set the view
    set_model (store);
    pack_start (col_label);

    signal_changed ().connect (sigc::mem_fun (*this, &ButtonFrameCombo::changed_cb));
    res->frame.changed.connect (sigc::mem_fun (*this, &ButtonFrameCombo::update));
    update ();
}

void ButtonFrameCombo::add_type (const Glib::ustring &label, Widgets::Button::FrameType type)
{
    Gtk::TreeRow row = *(store->append ());
    row[col_type] = type;
    row[col_label] = label;
}

void ButtonFrameCombo::update ()
{
    Widgets::Button::FrameType type = res->frame;
    
    Gtk::TreeModel::iterator iter = store->get_iter ("0");
    while ((*iter)[col_type] != type)
        ++iter;

    update_block = true;
    set_active (iter);
    update_block = false;
}

void ButtonFrameCombo::changed_cb ()
{
    if (update_block)
        return;
    
    Gtk::TreeModel::iterator iter = get_active ();
    g_return_if_fail (iter);

    char *label_str = g_strdup_printf (_("Change frame of %s"), res->id ().c_str ());
    
    UndoOp *op = new WidgetOps::PropChangeOp<Widgets::Button::FrameType> (
	label_str, res, res->frame, (*iter)[col_type], false);
    g_free (label_str);
    
    res->frame = (*iter)[col_type];

    res->get_manager ()->get_undo_manager ().push (op);
}


