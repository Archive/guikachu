//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "form.h"

#include <glib/gi18n.h>

#include "form-editor.h"

using namespace Guikachu;
using namespace Guikachu::Widgets;

#define GUIKACHU_WINDOW_TITLE_HEIGHT 13

Form::Form (Resources::Form *res_):
    Resizeable (res_->width, res_->height),
    res (res_),
    editor (0)
{
}

Form::~Form ()
{
    if (editor)
	delete editor;
    
    for (canvas_item_map_t::iterator i = canvas_items.begin ();
	 i != canvas_items.end (); i++)
	delete i->second;
}

Gtk::Widget* Form::get_editor ()
{
    if (!editor)
	editor = new GUI::FormEditor::FormProperties (res);
	
    return editor->get_editor ();
}

GUI::FormEditor::FormCanvasItem* Form::get_canvas_item (FooCanvasmm::Group &parent_group)
{
    GUI::FormEditor::FormCanvasItem *ret;

    canvas_item_map_t::iterator found = canvas_items.find (&parent_group);
    if (found == canvas_items.end ())
	ret = canvas_items[&parent_group] = new GUI::FormEditor::FormCanvasItem (this, parent_group);
    else
	ret = found->second;
    
    return ret;
}
