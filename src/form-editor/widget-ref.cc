//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "widget-ref.h"
#include "form-res.h"
#include "form-editor/widget.h"

using namespace Guikachu::Properties;
using std::string;

WidgetRef::WidgetRef (notify_signal_t &notify_signal,
		      Resources::Form *form_,
		      const string    &value_):
    Property<string> (notify_signal, value_),
    form (form_)
{
    current_widget = form->get_widget (value);

    if (current_widget)
	current_widget_changed = current_widget->changed.connect (
	    sigc::mem_fun (*this, &WidgetRef::widget_changed_cb));

    form->widget_created.connect (sigc::mem_fun (*this, &WidgetRef::widget_created_cb));
    form->widget_removed.connect (sigc::mem_fun (*this, &WidgetRef::widget_removed_cb));
}

void WidgetRef::set_val (const string &value_)
{
    if (value_ == "")
	Property<string>::set_val ("");
    
    Widget *new_widget = form->get_widget (value_);

    if (new_widget != current_widget)
    {
	current_widget_changed.disconnect ();
	current_widget = new_widget;

	if (current_widget)
	    current_widget_changed = current_widget->changed.connect (
		sigc::mem_fun (*this, &WidgetRef::widget_changed_cb));
    }

    Property<string>::set_val (value_);
}

void WidgetRef::widget_created_cb (Widget *widget)
{
    if (widget->id == value && !current_widget)
    {
	current_widget = widget;
	current_widget_changed.disconnect ();
	current_widget_changed = current_widget->changed.connect (
	    sigc::mem_fun (*this, &WidgetRef::widget_changed_cb));
    }
}

void WidgetRef::widget_removed_cb (Widget *widget)
{
    if (widget == current_widget)
    {
	current_widget = 0;
	Property<string>::set_val ("");
    }
}

void WidgetRef::widget_changed_cb ()
{
    Property<string>::set_val (current_widget->id);
    widget_changed.emit ();
}
