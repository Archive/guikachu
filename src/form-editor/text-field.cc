//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "text-field.h"

#include "form-editor.h"

using Guikachu::Widgets::TextField;
using namespace Guikachu::GUI::FormEditor;

TextField::TextField (Resources::Form   *owner,
		      const std::string &id,
		      serial_t           serial):
    ResizeableWidget (changed),
    Widget (owner, id, serial),
    AutoHeight (changed),
    Usable (changed),
    Disabled (changed),
    
    font (changed, 0),
    editable (changed, true),
    multi_line (changed, false),
    auto_shift (changed, false),
    max_length (changed, 0),
    numeric (changed, false),
    
    underline (changed, true),
    dynamic_size (changed, false),
    justify_right (changed, false),
    has_scrollbar (changed, false)
{
    width = 30;
}

int TextField::get_height () const
{
    int min_height = get_auto_height ();
    
    if (manual_height && height > min_height)
	return height;
    
    return min_height;
}

int TextField::get_auto_height () const
{
    return get_font_height (font);
}
