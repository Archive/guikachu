//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_TEXT_FIELD_H
#define GUIKACHU_FORM_EDITOR_TEXT_FIELD_H

#include "widget.h"
#include "auto-resizeable.h"
#include "usable.h"
#include "disabled.h"

namespace Guikachu
{
    namespace Widgets
    {
	class TextField: public Widget,
			 public AutoHeight,
                         public Usable,
                         public Disabled
	{
	public:
	    TextField (Resources::Form *owner, const std::string &id, serial_t serial);
	    
	    Type get_type () const { return WIDGET_TEXT_FIELD; };
	    void apply_visitor (WidgetVisitor &visitor) { visitor.visit_widget (this); };
	    
	    Property<int>  font;
	    Property<bool> editable;
	    Property<bool> multi_line;
	    Property<bool> auto_shift;
	    Property<int>  max_length;
	    Property<bool> numeric;
	    
	    Property<bool> underline;
	    Property<bool> dynamic_size;
	    Property<bool> justify_right;
	    Property<bool> has_scrollbar;

	protected:
	    int get_auto_height () const;

	public:
	    int get_height () const;
	};
    }
}

#endif /* !GUIKACHU_FORM_EDITOR_TEXT_FIELD_H */
