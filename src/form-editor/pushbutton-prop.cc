//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "pushbutton-prop.h"

#include <glib/gi18n.h>

#include "auto-resizeable-prop.h"
#include "usable-prop.h"
#include "disabled-prop.h"
#include "textual-prop.h"
#include "graphical-prop.h"

#include "widgets/entry.h"
#include "widgets/num-entry.h"
#include "widgets/togglebutton.h"
#include "widgets/propertytable.h"

#include "property-ops-widget.h"

using namespace Guikachu::GUI::FormEditor;
using namespace Guikachu::Widgets;

PushButtonProperties::PushButtonProperties (PushButton *res):
    WidgetProperties (res)
{
    using WidgetOps::PropChangeOpFactory;
    
    GUI::PropertyTable *proptable = new GUI::PropertyTable ();
    Gtk::Widget *control;
    
    /* Resource ID */
    add_general_controls (*proptable);

    /* Label */
    TextualProperties::add_controls (res, *proptable);

    /* Bitmaps */
    GraphicalProperties::add_controls (res, *proptable);
    
    /* Group ID */
    control = new GUI::PropertyEditors::NumEntry(
	0, 65535, res->group,
	new PropChangeOpFactory<int> (_("Change group ID of %s"), res, res->group, true));
    proptable->add (_("_Group ID:"), *manage (control),
		    _("Only one push button can be selected "
		      "per group (select 0 for non-exclusive "
		      "buttons)"));
    
    /* Anchor right */
    control = new GUI::PropertyEditors::ToggleButton (
	res->anchor_right,
	new PropChangeOpFactory<bool> (_("Change alignment of %s"), res, res->anchor_right, false));
    proptable->add (_("_Anchor right:"), *manage (control),
		    _("Keep right aligned when changing label "
		      "text at run-time"));

    /* Usable */
    UsableProperties::add_controls (res, *proptable);
    DisabledProperties::add_controls (res, *proptable);
    
    /* Separator */
    proptable->add_separator ();

    /* Position */
    add_position_controls (*proptable);

    /* Size */
    AutoWidthProperties::add_controls  (res, *proptable);
    AutoHeightProperties::add_controls (res, *proptable);
    
    proptable->show ();
    property_editor = proptable;
}

PushButtonProperties::~PushButtonProperties ()
{
    delete property_editor;
}

Gtk::Widget* PushButtonProperties::get_editor ()
{
    return property_editor;
}
