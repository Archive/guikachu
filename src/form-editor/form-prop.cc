//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "form.h"

#include <glib/gi18n.h>

#include "property-ops-resource.h"

#include "widgets/entry.h"
#include "widgets/num-entry.h"
#include "widgets/pos-entry.h"
#include "widgets/size-entry.h"
#include "widgets/togglebutton.h"
#include "widgets/widget-combo.h"
#include "widgets/resource-combo.h"
#include "widgets/propertytable.h"

#include "property-ops-resource.h"

using namespace Guikachu::GUI::FormEditor;

FormProperties::FormProperties (Resources::Form *res_):
    res (res_)
{
    using ResourceOps::PropChangeOpFactory;
    using ResourceOps::RenameOpFactory;

    GUI::PropertyTable *proptable = new GUI::PropertyTable ();
    Gtk::Widget *control;
    
    /* Resource ID */
    control = new GUI::PropertyEditors::Entry (false, res->id, new RenameOpFactory (res));
    proptable->add (_("Resource _ID:"), *manage (control));

    /* Title */
    control = new GUI::PropertyEditors::Entry (
	true, res->title,
	new PropChangeOpFactory<std::string> (_("Change title of %s"), res, res->title, true));
    proptable->add (_("_Title:"), *manage (control));

    /* Help ID */
    control = new GUI::PropertyEditors::ResourceCombo (
	Resources::RESOURCE_STRING, res->help_id,
	new PropChangeOpFactory<std::string> (_("Change help string of %s"), res, res->help_id));
    proptable->add (_("_Help ID:"), *manage (control));
    
    /* Menu ID */
    control = new GUI::PropertyEditors::ResourceCombo (
	Resources::RESOURCE_MENU, res->menu_id,
	new PropChangeOpFactory<std::string> (_("Change menu of %s"), res, res->menu_id));
    proptable->add (_("_Menu ID:"), *manage (control));
    
    /* Modal */
    control = new GUI::PropertyEditors::ToggleButton (
	res->modal,
	new PropChangeOpFactory<bool> (_("Change modalness of %s"), res, res->modal, false));
    proptable->add (_("Mo_dal:"), *manage (control));

    /* Frame */
    control = new GUI::PropertyEditors::ToggleButton (
	res->frame,
	new PropChangeOpFactory<bool> (_("Toggle frame of %s"), res, res->frame, false));
    proptable->add (_("_Frame:"), *manage (control));

    /* Save behind */
    control = new GUI::PropertyEditors::ToggleButton (
	res->savebehind,
	new PropChangeOpFactory<bool> (_("Change drawing mode of %s"), res, res->savebehind, false));
    proptable->add (_("_Save behind:"), *manage (control));

    /* Default button */
    control = new GUI::PropertyEditors::WidgetCombo (
	Widgets::WIDGET_BUTTON, res->def_button,
	new PropChangeOpFactory<std::string> (_("Change default button of %s"), res, res->def_button));
    proptable->add (_("Default _button:"), *manage (control));
    
    /* Separator */
    proptable->add_separator ();

    /* X */
    control = new GUI::PropertyEditors::XPosEntry (
	res->get_manager (), res->x,
	new PropChangeOpFactory<int> (_("Move %s"), res, res->x, true));
    proptable->add (_("_X:"), *manage (control),
		    _("Horizontal position of the form, relative "
		      "to the screen"));
    
    /* Y */
    control = new GUI::PropertyEditors::YPosEntry (
	res->get_manager (), res->y,
	new PropChangeOpFactory<int> (_("Move %s"), res, res->y, true));
    proptable->add (_("_Y:"), *manage (control),
		    _("Vertical position of the form, relative "
		      "to the screen"));

    /* Width */
    control = new GUI::PropertyEditors::WidthEntry (
	res->get_manager (), res->width,
	new PropChangeOpFactory<int> (_("Resize %s"), res, res->width, true));
    proptable->add (_("_Width:"), *manage (control),
		    _("Horizontal size of the form"));

    /* Height */
    control = new GUI::PropertyEditors::HeightEntry (
	res->get_manager (), res->height,
	new PropChangeOpFactory<int> (_("Resize %s"), res, res->height, true));
    proptable->add (_("_Height:"), *manage (control),
		    _("Vertical size of the form"));
    
    proptable->show ();
    property_editor = proptable;
}

FormProperties::~FormProperties ()
{
    delete property_editor;
}

Gtk::Widget* FormProperties::get_editor ()
{
    return property_editor;
}
