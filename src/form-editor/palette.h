//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_PALETTE_H
#define GUIKACHU_FORM_EDITOR_PALETTE_H

namespace Guikachu
{
    namespace GUI
    {
	class Palette;
    }
}

#include "form-editor/widget.h"

#include <gtkmm/window.h>
#include <gtkmm/tooltips.h>
#include <gtkmm/togglebutton.h>
#include <gtkmm/label.h>

#include <list>

namespace Guikachu
{
    namespace GUI
    {
	class Palette: public Gtk::Window
	{
	    Gtk::Tooltips                 tips;
	    Gtk::Label                    current_widget_name;
	    std::list<Gtk::ToggleButton*> buttons;
	    bool                          sticky;
	public:
	    Palette ();

	    sigc::signal1<void, Widgets::Type> palette_changed;
	    void palette_applied ();
	    
	private:
	    Gtk::ToggleButton* create_button (Widgets::Type       type,
					      const std::string  &tooltip,
					      const char * const *icon_xpm = 0);

	    int  delete_event_impl (GdkEventAny *e);

	    bool inside_button_cb;
	    
	    void widget_button_cb  (Widgets::Type type);	    
	    void set_selection     (Widgets::Type type);
	};
    };
};

#endif /* !GUIKACHU_FORM_EDITOR_PALETTE_H */
