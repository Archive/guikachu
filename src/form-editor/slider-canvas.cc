//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "slider-canvas.h"

#include "form-editor.h"
#include "form-editor-canvas.h"
#include "resizeable-ops.h"

#include <foocanvasmm/pixbuf.h>

#include "pixmaps/slider-background-canvas.xpm"
#include "pixmaps/slider-thumb-canvas.xpm"

using namespace Guikachu::GUI::FormEditor;

SliderCanvasItem::SliderCanvasItem (Widgets::Slider    *widget_,
				    FooCanvasmm::Group &parent_group):
    CanvasItem (widget_, parent_group),
    WidgetCanvasItem (widget_, parent_group),
    ResizeableCanvasItem (widget_, parent_group, new WidgetOps::ResizeOpFactory (widget_)),
    widget (widget_)
{
    widget->thumb_id.resource_changed.connect (sigc::mem_fun (*this, &SliderCanvasItem::update));
    widget->background_id.resource_changed.connect (sigc::mem_fun (*this, &SliderCanvasItem::update));
}

void SliderCanvasItem::draw_back (FooCanvasmm::Group &group, Glib::RefPtr<Gdk::Pixbuf> pixbuf) const
{
    Glib::RefPtr<Gdk::Pixbuf> pixbuf_sub;
    FooCanvasmm::Pixbuf *pixbuf_item;
    int back_cut_x1, back_cut_x2, back_cut_y, back_cut_width,
	back_cut_height, background_x1, background_x2, background_y;

    /* Be consistent, so we have x1 and x2 */
    back_cut_x1 = 0;

    if (widget->get_width () / 2 < pixbuf->get_width ())
    {
	back_cut_x2 = pixbuf->get_width () - widget->get_width () / 2;
	back_cut_width = widget->get_width () / 2;
	if (back_cut_width == 0) /* Sanity check */
	{
	    back_cut_width++;
	    back_cut_x2--;
	}
    } else {
	back_cut_x2 = 0;
	back_cut_width = pixbuf->get_width ();
    }

    if (pixbuf->get_height () > widget->get_height ())
    {
	back_cut_y = (pixbuf->get_height () - widget->get_height ()) / 2;
	back_cut_height = widget->get_height ();
    } else {
	back_cut_y = 0;
	back_cut_height = pixbuf->get_height ();
    }

    background_x1 = widget->x;
    background_x2 = widget->x + widget->get_width () / 2;
    background_y = widget->y + widget->get_height () / 2 -
	    back_cut_height / 2;

    /* One half */
    pixbuf_sub = Gdk::Pixbuf::create_subpixbuf(pixbuf, back_cut_x1,
		    back_cut_y, back_cut_width, back_cut_height);
    pixbuf_item = new FooCanvasmm::Pixbuf (group, background_x1,
		    background_y, pixbuf_sub);
    pixbuf_item->property_interp_type () = Gdk::INTERP_NEAREST;

    /* Other half */
    pixbuf_sub = Gdk::Pixbuf::create_subpixbuf(pixbuf, back_cut_x2,
		    back_cut_y, back_cut_width, back_cut_height);
    pixbuf_item = new FooCanvasmm::Pixbuf (group, background_x2,
		    background_y, pixbuf_sub);
    pixbuf_item->property_interp_type () = Gdk::INTERP_NEAREST;
}

void SliderCanvasItem::draw_thumb (FooCanvasmm::Group &group, Glib::RefPtr<Gdk::Pixbuf> pixbuf) const
{
    Glib::RefPtr<Gdk::Pixbuf> pixbuf_sub;
    FooCanvasmm::Pixbuf *pixbuf_item;
    int value_range_size, thumb_x, thumb_y, thumb_height, thumb_width;
    double value_ratio = 0;

    value_range_size = widget->max_value - widget->min_value;

    if (value_range_size)
    {
	int value_abs = widget->value - widget->min_value;
	value_ratio = (double)value_abs / (double)value_range_size;

	if (value_ratio < 0)
	    value_ratio = 0;
	
	if (value_ratio > 1)
	    value_ratio = 1;
    }

    thumb_width = pixbuf->get_width ();
    thumb_height = std::min(widget->get_height (), pixbuf->get_height ());
    thumb_x = (int) (value_ratio * (widget->get_width () - thumb_width));
    thumb_y = (widget->get_height () - thumb_height) / 2;

    pixbuf_sub = Gdk::Pixbuf::create_subpixbuf(pixbuf, 
	    (pixbuf->get_width () - thumb_width) / 2,
	    (pixbuf->get_height () - thumb_height) / 2,
	    thumb_width, thumb_height);

    pixbuf_item = new FooCanvasmm::Pixbuf (group,
	    widget->x + thumb_x,  widget->y + thumb_y, pixbuf_sub);
    pixbuf_item->property_interp_type () = Gdk::INTERP_NEAREST;
}

void SliderCanvasItem::draw (FooCanvasmm::Group &group) const
{
    // TODO: Draw disabled (un-usable) sliders in different color
    
    /* Begin with the background image */
    if (widget->background_id != "")
	draw_back (group, FormEditor::render_bitmapref (widget->background_id,
                                                        widget->get_manager ()->get_target ()->screen_color));
    else
	draw_back (group, Gdk::Pixbuf::create_from_xpm_data (slider_background_canvas_xpm));

    /* Now draw the thumb */
    if (widget->thumb_id != "")
	draw_thumb(group, FormEditor::render_bitmapref (widget->thumb_id,
                                                        widget->get_manager ()->get_target ()->screen_color));
    else
	draw_thumb(group, Gdk::Pixbuf::create_from_xpm_data (slider_thumb_canvas_xpm));
}

void SliderCanvasItem::get_bounds (int &x1, int &y1,
				   int &x2, int &y2)
{
    x1 = widget->x;
    y1 = widget->y;
    x2 = x1 + widget->get_width ();
    y2 = y1 + widget->get_height ();
}
