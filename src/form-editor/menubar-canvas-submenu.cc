//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "menubar-canvas.h"

#include <foocanvasmm/pixbuf.h>
#include "form-editor.h"
#include "form-editor-canvas.h"

using namespace Guikachu::GUI::FormEditor;

MenubarCanvasItem::SubmenuCanvasItem::SubmenuCanvasItem (FooCanvasmm::Group             &parent_group,
                                                         const Resources::Menu::Submenu &submenu_,
                                                         Guikachu::Target               *target_,
                                                         int                            &x):
    FooCanvasmm::Group (parent_group, x, 0),
    submenu (submenu_),
    target (target_),
    title_group (*this, 0, 0),
    selection_group (title_group, 0, 0),
    items_group (*this, 0, 14),
    rolled_down (false)
{
    using namespace FooCanvasmm::Properties;

    Gdk::Color selected_color = FormEditor::get_foreground_color (target, true);
    if (target->screen_color)
        selected_color = Gdk::Color ("#330099");

    // Create submenu title (with clickable background)
    Glib::RefPtr<Gdk::Pixbuf> submenu_pixbuf = FormEditor::render_text (
        get_title_font (), submenu.label,
        get_foreground_color (target, true),
        false);
    
    int background_start = -4;
    int background_end = submenu_pixbuf->get_width () + 3;
    
    FormEditor::draw_rectangle (title_group, background_start, 1, background_end, 13);
    new FooCanvasmm::Pixbuf (title_group, 0, 1, submenu_pixbuf);

    // Create submenu title in selected state
    Glib::RefPtr<Gdk::Pixbuf> submenu_pixbuf_selected = FormEditor::render_text (
        get_title_font (), submenu.label,
        get_background_color (target),
        false);
    
    (*FormEditor::draw_rectangle (selection_group, background_start, 1, background_end, 13))
        << fill_color (selected_color);
    new FooCanvasmm::Pixbuf (selection_group, 0, 1, submenu_pixbuf_selected);
    selection_group.raise_to_top ();

    // Create menu items
    int item_y = 0;
    int max_width = 0;
    // Pass 1: Draw labels and calculate menu width
    for (Resources::Menu::MenuItems::const_iterator i = submenu.items.begin ();
         i != submenu.items.end (); ++i)
    {
        if (i->separator)
        {
            item_y += 5;
            continue;
        }
        
        Glib::RefPtr<Gdk::Pixbuf> item_pixbuf = FormEditor::render_text (
            get_title_font (), i->label, get_foreground_color (target, true), false);
        int item_width = item_pixbuf->get_width ();
        if (i->shortcut)
            item_width += 28;
                
        new FooCanvasmm::Pixbuf (items_group, 0, item_y, item_pixbuf);
        max_width = std::max (max_width, item_width);
        item_y += 11;
    }
    
    int menu_height = item_y + 2;
    int menu_width = max_width + 9;
    menu_width = std::min (menu_width, target->screen_width ());
    int shortcut_x = menu_width - 26;

    // Pass 2: Draw shortcuts
    item_y = 0;
    char shortcut_label[3];
    shortcut_label[0] = 157;
    shortcut_label[2] = 0;
    for (Resources::Menu::MenuItems::const_iterator i = submenu.items.begin ();
         i != submenu.items.end (); ++i)
    {
        if (i->separator)
        {
            Glib::RefPtr<Gdk::Pixbuf> pixbuf = Gdk::Pixbuf::create (
                Gdk::COLORSPACE_RGB, true, 8, menu_width - 3, 1);
            pixbuf->fill (get_pixel_value (get_foreground_color (target, true)));
            guint8 *pixdata = pixbuf->get_pixels ();
            for (int separator_x = 0; separator_x < menu_width - 3; separator_x += 2)
                pixdata[(separator_x * 4) + 3] = 0;

            new FooCanvasmm::Pixbuf (items_group, -3, item_y + 2, pixbuf);
            
            item_y += 5;
            continue;
        }

        if (i->shortcut)
        {
            shortcut_label[1] = toupper (i->shortcut);
            Glib::RefPtr<Gdk::Pixbuf> shortcut_pixbuf = FormEditor::render_text (
                get_title_font (), shortcut_label, get_foreground_color (target, true), false);
            new FooCanvasmm::Pixbuf (items_group, shortcut_x, item_y, shortcut_pixbuf);
        }
        
        item_y += 11;
    }
    
#define GUIKACHU_OFFSET(x,y)                                    \
    (MIN (MAX (0, (y)), (menu_height - 1)) * rowstride +	\
     MIN (MAX (0, (x)), (menu_width - 1)) * 4)

#define GUIKACHU_PLOT(x,y)						\
    {									\
	pixdata[GUIKACHU_OFFSET(x,y) + 0] = color.get_red ()   >> 8;	\
	pixdata[GUIKACHU_OFFSET(x,y) + 1] = color.get_green () >> 8;	\
	pixdata[GUIKACHU_OFFSET(x,y) + 2] = color.get_blue ()  >> 8;	\
	pixdata[GUIKACHU_OFFSET(x,y) + 3] = 255;			\
    }

    Glib::RefPtr<Gdk::Pixbuf> menu_pixbuf = Gdk::Pixbuf::create (
	Gdk::COLORSPACE_RGB, true, 8, menu_width, menu_height);
    menu_pixbuf->fill (get_pixel_value (get_background_color (target)));
    
    guint8     *pixdata = menu_pixbuf->get_pixels ();
    int         rowstride = menu_pixbuf->get_rowstride ();
    Gdk::Color  color = get_foreground_color (target, true);

    for (int draw_x = 1; draw_x != menu_width - 1; ++draw_x)
        GUIKACHU_PLOT (draw_x, menu_height - 2);
    for (int draw_x = 2; draw_x != menu_width - 2; ++draw_x)
        GUIKACHU_PLOT (draw_x, menu_height - 1);
    for (int draw_y = 0; draw_y != menu_height - 2; ++draw_y)
    {
        GUIKACHU_PLOT (0, draw_y);
        GUIKACHU_PLOT (menu_width - 2, draw_y);
    }
    for (int draw_y = 0; draw_y != menu_height - 2; ++draw_y)
        GUIKACHU_PLOT (menu_width - 1, draw_y);        

    (new FooCanvasmm::Pixbuf (items_group, background_start, 0, menu_pixbuf))->lower_to_bottom ();
    if (menu_width + x + background_start > target->screen_width ())
    {
        items_group.move (target->screen_width () - (menu_width + x + background_start), 0);
    }
    
    // Start menu in rolled-up state
    roll_up ();
    
    title_group.signal_event ().connect (
        sigc::bind_return (sigc::mem_fun (*this, &SubmenuCanvasItem::event_cb), false));
    x += submenu_pixbuf->get_width () + 7;
}

void MenubarCanvasItem::SubmenuCanvasItem::event_cb (GdkEvent *e)
{
    if ((e->type == GDK_BUTTON_PRESS) &&
	(e->button.button == 1))
        clicked.emit ();
}

void MenubarCanvasItem::SubmenuCanvasItem::roll_up ()
{
    items_group.hide ();
    selection_group.hide ();
    rolled_down = false;
}

void MenubarCanvasItem::SubmenuCanvasItem::roll_down ()
{
    items_group.show ();
    selection_group.show ();
    rolled_down = true;
}
