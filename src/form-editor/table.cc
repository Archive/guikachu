//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "table.h"

#include "form-editor.h"
#include <numeric> // for std::accumulate

using Guikachu::Widgets::Table;
using namespace Guikachu::GUI::FormEditor;

Table::Table (Resources::Form   *owner,
	      const std::string &id,
	      serial_t           serial):
    ResizeableWidget (changed),
    Widget           (owner, id, serial),
    AutoWidth        (changed),
    AutoHeight       (changed),
    num_rows         (changed, 1),
    num_columns      (columns_changed, 1),
    column_width     (changed, std::vector<int>(1, 1))
{
    columns_changed.connect (sigc::mem_fun (*this, &Table::update));
}

int Table::get_auto_width  () const
{
    const std::vector<int> &columns = column_width;
    int columns_size = num_columns;

    // We add columns_size because each separator is one pixel
    return std::accumulate (columns.begin (),
			    columns.begin () + columns_size,
			    columns_size);
}

int Table::get_auto_height () const
{
    return num_rows * 11;
}


void Table::update ()
{
    /* Make sure column_width contains at least num_columns elements */
    
    const std::vector<int> &columns = column_width;
    
    int num_columns_in_vec = columns.size ();
    int num_columns_target = num_columns;

    if (num_columns_in_vec < num_columns_target)
    {
	std::vector<int> columns_copy = columns;
	
	for (int i = num_columns_in_vec; i < num_columns_target; i++)
	    columns_copy.push_back (1);
	
	column_width = columns_copy;
    }

    changed.emit ();
}
