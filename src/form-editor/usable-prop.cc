//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "usable-prop.h"

#include <glib/gi18n.h>

#include "property-ops-widget.h"

#include "widgets/togglebutton.h"

using namespace Guikachu::GUI::FormEditor;

void UsableProperties::add_controls (Widgets::Usable    *res,
                                     GUI::PropertyTable &proptable)
{
    Gtk::Widget *control;
    
    /* Usable */
    control = new GUI::PropertyEditors::ToggleButton (
	res->usable, new WidgetOps::PropChangeOpFactory<bool> (
            _("Toggle usable state of %s"), dynamic_cast<Widget*> (res), res->usable, false));
    proptable.add (_("_Usable:"), *manage (control),
                   _("Non-usable widgets are not rendered by default"));
}
