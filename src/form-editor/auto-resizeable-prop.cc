//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "auto-resizeable-prop.h"

#include <glib/gi18n.h>

#include "widgets/size-entry.h"
#include "widgets/auto-toggle.h"
#include "property-ops-widget.h"

using namespace Guikachu::GUI::FormEditor;

void AutoWidthProperties::add_controls (Widgets::AutoWidth *res,
					GUI::PropertyTable &proptable)
{
    Gtk::Widget *control;
    
    /* Width */
    control = new GUI::PropertyEditors::WidthEntry (
	res->get_manager (), res->width,
	new WidgetOps::PropChangeOpFactory<int> (
	    _("Resize %s"), dynamic_cast<Widget*> (res), res->width, true));
    control = new GUI::PropertyEditors::AutoToggle (
	*manage (control), res->manual_width,
	new WidgetOps::PropChangeOpFactory<bool> (
	    _("Change manual resizing of %s"),
	    dynamic_cast<Widget*> (res), res->manual_width));
    proptable.add (_("_Width:"), *manage (control),
		   _("Horizontal size of the widget "
		     "(check to set manually)"));
}

void AutoHeightProperties::add_controls (Widgets::AutoHeight *res,
					 GUI::PropertyTable
					 &proptable)
{
    Gtk::Widget *control;
    
    /* Height */
    control = new GUI::PropertyEditors::HeightEntry (
	res->get_manager (), res->height,
	new WidgetOps::PropChangeOpFactory<int> (
	    _("Resize %s"),
	    dynamic_cast<Widget*> (res), res->height, true));
    control = new GUI::PropertyEditors::AutoToggle (
	*manage (control), res->manual_height,
	new WidgetOps::PropChangeOpFactory<bool> (
	    _("Change manual resizing of %s"),
	    dynamic_cast<Widget*> (res), res->manual_height));
    proptable.add (_("_Height:"), *manage (control),
		   _("Vertical size of the widget "
		     "(check to set manually)"));
}
