//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "auto-resizeable.h"

#include <sigc++/sigc++.h>

using namespace Guikachu::Widgets;

AutoWidth::AutoWidth (VSignal &changed):
    ResizeableWidget (changed),
    changed_propagate (changed),
    manual_width (manual_width_changed, false),
    manual_sync_block (false)
{
    manual_width.changed.connect (sigc::mem_fun (*this, &AutoWidth::manual_width_cb));
    width.changed.connect (sigc::mem_fun (*this, &AutoWidth::width_changed_cb));
}

int AutoWidth::get_width () const
{
    if (manual_width && width != 0)
	return width;
    else
	return get_auto_width ();
}

void AutoWidth::manual_width_cb ()
{
    manual_sync_block = true;

    if (manual_width && width == 0)
	width = get_auto_width ();
    else
	changed_propagate.emit ();

    manual_sync_block = false;
}

void AutoWidth::width_changed_cb ()
{
    if (!manual_width && width != 0 && !manual_sync_block)
	manual_width = true;
}

AutoHeight::AutoHeight (VSignal &changed):
    ResizeableWidget (changed),
    changed_propagate (changed),
    manual_height (manual_height_changed, false),
    manual_sync_block(false)
{
    manual_height.changed.connect (sigc::mem_fun (*this, &AutoHeight::manual_height_cb));
    height.changed.connect (sigc::mem_fun (*this, &AutoHeight::height_changed_cb));
}

int AutoHeight::get_height () const
{
    if (manual_height && height != 0)
	return height;
    else
	return get_auto_height ();
}

void AutoHeight::manual_height_cb ()
{
    manual_sync_block = true;

    if (manual_height && height == 0)
	height = get_auto_height ();
    else
	changed_propagate.emit ();

    manual_sync_block = false;
}

void AutoHeight::height_changed_cb ()
{
    if (!manual_height && height != 0 && !manual_sync_block)
	manual_height = true;
}
