//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "table-prop.h"

#include <glib/gi18n.h>

#include "widgets/num-entry.h"
#include "widgets/propertytable.h"

#include <gtkmm/adjustment.h>
#include <gtkmm/notebook.h>
#include <gtkmm/box.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/table.h>
#include <gtkmm/label.h>
#include <gtkmm/spinbutton.h>
#include "form-editor.h"

#include "property-ops-widget.h"
#include "auto-resizeable-prop.h"
#include <vector>

using namespace Guikachu::GUI::FormEditor;
using namespace Guikachu::Widgets;

namespace Guikachu
{
    namespace
    {
	class TableColOp: public UndoOp
	{
	    ResourceManager *manager;
	    serial_t         form_serial, table_serial;

	    int old_num_columns, new_num_columns;
	    std::vector<int> old_column_width, new_column_width;
	    
	    Glib::ustring op_label;

	    TableColOp (Table                  *table,
			int                     old_num_columns,
			int                     new_num_columns,
			const std::vector<int> &old_column_width,
			const std::vector<int> &new_column_width);
	    
	public:
	    TableColOp (Table *table, int new_num_columns);
	    TableColOp (Table *table, const std::vector<int> &new_colum_width);
	    virtual ~TableColOp () {};

	    void undo ();
	    void redo ();
	    UndoOp * combine (UndoOp *other_op) const;
	    Glib::ustring get_label () const { return op_label; };

	private:
	    Table * get_table () const;
	};

	class TableColOpFactory: public PropChangeOpFactory<int>
	{
	    Table *table;
	    
	public:
	    TableColOpFactory (Table *table);
	    virtual ~TableColOpFactory () {};

	    void push_change (const int &value);
	};
	
	class TableColEditor: public Gtk::VBox
	{
	    struct spinbutton_pair {
		Gtk::Widget     *label;
		Gtk::SpinButton *spinbutton;
		
		spinbutton_pair (Gtk::Widget     *label_,
				 Gtk::SpinButton *spinbutton_):
		    label (label_),
		    spinbutton (spinbutton_)
		    {
		    };
	    };
	    
	    Table                        *table;
	    Gtk::Table                    col_list;
	    std::vector<spinbutton_pair>  spinbuttons;
	public:
	    TableColEditor (Table *table);
	    ~TableColEditor ();
	private:
	    bool update_block;
	    void update ();
	    void col_width_cb (unsigned int column);
	};
	
    } // Anonymous namespace
} // namespace Guikachu

using Guikachu::TableColOp;
using Guikachu::TableColOpFactory;
using Guikachu::TableColEditor;

TableProperties::TableProperties (Table *res):
    WidgetProperties (res)
{
    using WidgetOps::PropChangeOpFactory;
    
    Gtk::Notebook *notebook = new Gtk::Notebook;

    /* Page 1: General properties */    
    GUI::PropertyTable *proptable = new GUI::PropertyTable ();
    Gtk::Widget *control;
    
    /* Resource ID */
    add_general_controls (*proptable);

    /* Number of rows */
    control = new GUI::PropertyEditors::NumEntry (
	1, 160, res->num_rows,
	new PropChangeOpFactory<int> (_("Change number of rows in %s"), res, res->num_rows, true));
    proptable->add (_("Number of rows:"), *manage (control));
    
    /* Separator */
    proptable->add_separator ();

    /* Position */
    add_position_controls (*proptable);
    
    /* Size */
    AutoWidthProperties::add_controls  (res, *proptable);
    AutoHeightProperties::add_controls (res, *proptable);

    // FIXME: Add uline accelerators to the tabs
    notebook->pages ().push_back (
	Gtk::Notebook_Helpers::TabElem (*manage (proptable),
					_("General")));

    /* Page 2: Columns */
    control = manage (new TableColEditor (res));
    notebook->pages ().push_back (
	Gtk::Notebook_Helpers::TabElem (*manage (control),
					_("Columns")));

    notebook->show_all ();
    property_editor = notebook;
}

TableProperties::~TableProperties ()
{
    delete property_editor;
}

Gtk::Widget* TableProperties::get_editor ()
{
    return property_editor;
}




TableColOp::TableColOp (Table *table, int new_num_columns_) :
    manager (table->get_manager ()),
    form_serial (table->get_form ()->get_serial ()),
    table_serial (table->get_serial ()),
    old_num_columns (table->num_columns),
    new_num_columns (new_num_columns_),
    old_column_width (table->column_width),
    new_column_width (old_column_width)
{
    char *label_str = g_strdup_printf (_("Change columns in %s"), table->id ().c_str ());
    op_label = label_str;
    g_free (label_str);
}

TableColOp::TableColOp (Table *table, const std::vector<int> &new_column_width_) :
    manager (table->get_manager ()),
    form_serial (table->get_form ()->get_serial ()),
    table_serial (table->get_serial ()),
    old_num_columns (table->num_columns),
    new_num_columns (old_num_columns),
    old_column_width (table->column_width),
    new_column_width (new_column_width_)
{
    char *label_str = g_strdup_printf (_("Change columns in %s"), table->id ().c_str ());
    op_label = label_str;
    g_free (label_str);
}

TableColOp::TableColOp (Table                  *table,
			int                     old_num_columns_,
			int                     new_num_columns_,
			const std::vector<int> &old_column_width_,
			const std::vector<int> &new_column_width_) :
    manager (table->get_manager ()),
    form_serial (table->get_form ()->get_serial ()),
    table_serial (table->get_serial ()),
    old_num_columns (old_num_columns_),
    new_num_columns (new_num_columns_),
    old_column_width (old_column_width_),
    new_column_width (new_column_width_)
{
    char *label_str = g_strdup_printf (_("Change columns in %s"), table->id ().c_str ());
    op_label = label_str;
    g_free (label_str);
}
    
	    

Table * TableColOp::get_table () const
{
    Resource *res = manager->get_resource (form_serial);
    Resources::Form *form = static_cast<Resources::Form*> (res);
    g_assert (form);

    Widget *widget = form->get_widget (table_serial);
    Table *table = static_cast<Table*> (widget);
    g_assert (table);

    return table;
}

void TableColOp::undo ()
{
    Table *table = get_table ();

    table->num_columns = old_num_columns;
    table->column_width = old_column_width;
}

void TableColOp::redo ()
{
    Table *table = get_table ();

    table->num_columns = new_num_columns;
    table->column_width = new_column_width;
}

Guikachu::UndoOp * TableColOp::combine (UndoOp *other_op) const
{
    TableColOp *op = dynamic_cast<TableColOp*> (other_op);
    if (!op)
	return 0;

    if (op->form_serial != form_serial || op->table_serial != table_serial)
	return 0;

    TableColOp *new_op = new TableColOp (
	get_table (),
	old_num_columns, op->new_num_columns,
	old_column_width, op->new_column_width);
    
    return new_op;
}




TableColOpFactory::TableColOpFactory (Table *table_):
    table (table_)
{
}

void TableColOpFactory::push_change (const int &value)
{
    table->get_manager ()->get_undo_manager ().push (new TableColOp (table, value));
    table->num_columns = value;
}




TableColEditor::TableColEditor (Table *table_):
    Gtk::VBox (false, 5),
    table (table_),
    update_block (false)
{
    col_list.set_row_spacings (5);
    col_list.set_col_spacings (5);
    
    Gtk::ScrolledWindow *scrolled_win = new Gtk::ScrolledWindow;
    scrolled_win->set_policy (Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS);
    scrolled_win->add (col_list);
    add (*manage (scrolled_win));
    
    Gtk::Widget *num_columns = new Guikachu::GUI::PropertyEditors::NumEntry (
	1, 160, table->num_columns,
	new TableColOpFactory (table));
    Gtk::HBox *hbox = new Gtk::HBox (false, 5);
    hbox->pack_start (*manage (
	new Gtk::Label (_("Number of columns:"), 0, 0.5)),
	false, false);
    hbox->pack_start (*manage (num_columns), false, false);

    hbox->show_all ();
    pack_start (*manage (hbox), false, false);
    set_border_width (5);

    table->changed.connect (sigc::mem_fun (*this, &TableColEditor::update));
    update ();
}

TableColEditor::~TableColEditor ()
{
    for (std::vector<spinbutton_pair>::iterator i = spinbuttons.begin ();
	 i != spinbuttons.end (); i++)
    {
	delete i->label;
	delete i->spinbutton;
    }
}

void TableColEditor::col_width_cb (unsigned int index)
{
    if (update_block)
	return;
    
    Gtk::SpinButton *spinbutton = spinbuttons[index].spinbutton;
    unsigned int new_val = spinbutton->get_value_as_int ();
    std::vector<int> columns = table->column_width;

    g_return_if_fail (index <= columns.size ());
    
    columns[index] = new_val;
    table->get_manager ()->get_undo_manager ().push (new TableColOp (table, columns));
    table->column_width = columns;
}

void TableColEditor::update ()
{
    if (update_block)
	return;
    
    update_block = true;
    
    const std::vector<int> &columns = table->column_width;
    unsigned int displayed_num_columns = spinbuttons.size ();
    unsigned int num_columns = table->num_columns;
    
    /* Update visible spin buttons */
    for (unsigned int i = 0; i < MIN (num_columns, displayed_num_columns); i++)
	spinbuttons[i].spinbutton->set_value (columns[i]);
    
    /* Remove spin buttons if not needed */
    if (displayed_num_columns > num_columns)
    {
	for (unsigned int i = num_columns; i < displayed_num_columns; i++)
	{
	    delete spinbuttons.back ().label;
	    delete spinbuttons.back ().spinbutton;

	    spinbuttons.pop_back ();
	}
    }
    
    /* Add new spin buttons if necessary */
    if (displayed_num_columns < num_columns)
    {
	Gtk::SpinButton *spinbutton;
	Gtk::Widget     *label;
	Gtk::Adjustment *adj;
	
	for (unsigned int i = displayed_num_columns; i < num_columns; i++)
	{
	    Glib::ScopedPtr<char> buffer (g_strdup_printf (_("Column %d:"), i + 1));
	    label = new Gtk::Label (buffer.get ());
	    label->show_all ();
	    col_list.attach (*label,
			     0, 1, i, i + 1,
			     Gtk::AttachOptions (), Gtk::AttachOptions ());
	    
	    adj = new Gtk::Adjustment (columns[i],
				       1, table->get_manager()->get_target ()->screen_width);
	    adj->signal_value_changed ().connect (
		sigc::bind (sigc::mem_fun (*this, &TableColEditor::col_width_cb), i));
	    spinbutton = new Gtk::SpinButton (*manage (adj));
	    spinbutton->set_digits (0);
	    spinbutton->set_update_policy (Gtk::UPDATE_IF_VALID);	    
	    spinbutton->show_all ();
	    col_list.attach (*spinbutton,
			     1, 2, i, i + 1,
			     Gtk::FILL | Gtk::EXPAND, Gtk::AttachOptions ());

	    spinbuttons.push_back (spinbutton_pair (label, spinbutton));
	}
    }

    update_block = false;
}

