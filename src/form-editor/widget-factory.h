//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_WIDGET_FACTORY_H
#define GUIKACHU_FORM_EDITOR_WIDGET_FACTORY_H

#include "form-editor/widget.h"
#include "form-editor/widget-canvas.h"
#include <gtkmm/widget.h>

namespace Guikachu
{
    namespace GUI
    {
	namespace FormEditor
	{
	    WidgetCanvasItem * get_widget_canvas_item (Widget *widget, FooCanvasmm::Group &group);
	    Gtk::Widget *      get_widget_editor (Widget *widget);
	}
    }
}

#endif /* !GUIKACHU_FORM_EDITOR_WIDGET_FACTORY_H */
