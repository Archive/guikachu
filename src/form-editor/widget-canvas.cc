//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "widget-canvas.h"

#include <glib/gi18n.h>

#include <foocanvasmm/rect.h>

#include <list>
#include <algorithm>
#include <math.h>

#include <sigc++/adaptors/bind_return.h>

#include "preferences.h"

#include "form-editor-canvas.h"

using namespace Guikachu::GUI::FormEditor;

WidgetCanvasItem::WidgetCanvasItem (Widget             *widget_,
				    FooCanvasmm::Group &parent_group_):
    CanvasItem (widget_, parent_group_),
    widget (widget_),
    selected (false),
    selection_box (0)
{
    // Chain signal callbacks
    canvas_item_changed.connect (sigc::mem_fun (*this, &WidgetCanvasItem::canvas_item_changed_cb));

    widget->changed.connect  (sigc::mem_fun (*this, &WidgetCanvasItem::update_hook));
    widget->selected.connect (sigc::mem_fun (*this, &WidgetCanvasItem::widget_selected_cb));
}

WidgetCanvasItem::~WidgetCanvasItem ()
{
    if (selection_box)
	delete selection_box;
}

void WidgetCanvasItem::update_hook ()
{
    update ();
}

void WidgetCanvasItem::update ()
{
    if (item)
	delete item;
    
    FooCanvasmm::Group *new_item = new FooCanvasmm::Group (*draw_group, 0, 0);
    draw (*new_item);
    set_canvas_item (new_item);
}

void WidgetCanvasItem::canvas_item_changed_cb ()
{
    item->signal_event ().connect (sigc::bind_return (
	sigc::mem_fun (*this, &WidgetCanvasItem::canvas_event_cb), false));
    widget_selected_cb (selected);
}

void WidgetCanvasItem::canvas_event_cb (GdkEvent *e)
{
    // Context menu
    if ((e->type == GDK_BUTTON_PRESS) &&
	(e->button.button == 3))
    {
	context_menu.emit (e->button.button, e->button.time);
    }
}

FooCanvasmm::Item * WidgetCanvasItem::create_selection_box ()
{
    int x1, y1, x2, y2;
    get_bounds (x1, y1, x2, y2);

    gdouble box_size = 1.5 / overlay_group->get_canvas ()->get_pixels_per_unit ();
    
    gdouble box_x1 = x1 - 0.5 - box_size;
    gdouble box_x2 = x2 + 0.5 + box_size;
    gdouble box_y1 = y1 - 0.5 - box_size;
    gdouble box_y2 = y2 + 0.5 + box_size;

    using namespace FooCanvasmm::Properties;
    FooCanvasmm::Rect *bounding_box = new FooCanvasmm::Rect (
	*overlay_group, box_x1, box_y1, box_x2, box_y2);
    *bounding_box
	<< width_pixels (2)
	<< outline_color (get_selection_color ());

    return bounding_box;
}

void WidgetCanvasItem::widget_selected_cb (bool selected_)
{
    selected = selected_;
    
    if (!selected)
    {
	if (selection_box)
	{
	    delete selection_box;
	    selection_box = 0;
	}
	return;
    }

    delete selection_box;
    selection_box = create_selection_box ();
    selection_box->lower_to_bottom ();
    
    item_group->raise_to_top ();
}

Gdk::Color WidgetCanvasItem::get_foreground_color (bool enabled) const
{
    return FormEditor::get_foreground_color (widget->get_manager ()->get_target (), enabled);
}

Gdk::Color WidgetCanvasItem::get_background_color () const
{
    return FormEditor::get_background_color (widget->get_manager ()->get_target ());
}

Gdk::Color WidgetCanvasItem::get_selection_color () const
{
    return FormEditor::get_selection_color (widget->get_manager ()->get_target ());
}
