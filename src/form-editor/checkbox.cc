//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "checkbox.h"

#include "form-editor.h"

using Guikachu::Widgets::Checkbox;
using namespace Guikachu::GUI::FormEditor;

Checkbox::Checkbox (Resources::Form   *owner,
		    const std::string &id,
		    serial_t           serial):
    ResizeableWidget (changed),
    Widget (owner, id, serial),
    AutoWidth (changed),
    AutoHeight (changed),
    Usable (changed),
    Disabled (changed),
    Textual (changed, id),
    anchor_right (changed, false),
    group (this),
    toggled (this)
{
}

Checkbox::GroupProp::GroupProp (Checkbox *owner_):
    Property<int> (owner_->changed),
    owner (owner_)
{
}

Checkbox::ToggledProp::ToggledProp (Checkbox *owner_):
    Property<bool> (owner_->changed),
    owner (owner_)
{
}

void Checkbox::ToggledProp::set_val (const bool& value_)
{
    if (value_ == value)
	return;
    
    value = value_;
    if (value_ && owner->group)
    {
	std::set<Widget*> widgets = owner->get_form ()->get_widgets ();
	for (std::set<Widget*>::iterator i = widgets.begin ();
	     i != widgets.end (); i++)
	{
	    if ((*i)->get_type () == WIDGET_CHECKBOX &&
		(*i) != owner)
	    {
		Checkbox *checkbox = static_cast <Checkbox*> (*i);
		if (checkbox->group == owner->group && checkbox->toggled)
		    checkbox->toggled = false;
	    }
	}
    }
    
    changed ();
}

void Checkbox::GroupProp::set_val (const int& value_)
{   
    if (value_ == value)
	return;

    value = value_;
    
    if (value_)
    {
	std::set<Widget*> widgets = owner->get_form ()->get_widgets ();
	std::set<Widget*>::iterator i = widgets.begin ();
	bool found = false;

	// See if we there is another toggled widget in the new group
	while (!found && i != widgets.end ())
	{
	    if ((*i)->get_type () == WIDGET_CHECKBOX &&
		(*i) != owner)
	    {
		Checkbox *checkbox = static_cast <Checkbox*> (*i);
		if (checkbox->group == owner->group && checkbox->toggled)
		    found = true;
	    }
	    
	    i++;
	}

	// If there is, untoggle ourselves
	if (found && owner->toggled)
	    owner->toggled = false;
    }
    
    changed ();
}

int Checkbox::get_auto_width () const
{
    return 18 + get_line_width (font, text);
}

int Checkbox::get_auto_height () const
{
    return 1 + get_font_height (font);
}
