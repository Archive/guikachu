//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_CANVASITEM_H
#define GUIKACHU_FORM_EDITOR_CANVASITEM_H

namespace Guikachu
{
    namespace GUI
    {
	namespace FormEditor
	{
	    class CanvasItem;
	}
    }
}

#include <sigc++/signal.h>
#include <foocanvasmm/group.h>

#include "form-element.h"

namespace Guikachu
{
    namespace GUI
    {
	namespace FormEditor
	{
	    class CanvasItem: public sigc::trackable
	    {
		FormElement *form_element;
		
		struct
		{
		    bool dragging;
		    bool drag_occured;

		    double start_x, start_y;
		    double last_x, last_y;
		    double offset_x, offset_y;
		    double delta_remainder_x, delta_remainder_y;
		} drag_context;
		
	    protected:
		CanvasItem (FormElement        *form_element,
			    FooCanvasmm::Group &parent_group);
		
	    public:
		virtual ~CanvasItem ();

	    protected:
		FooCanvasmm::Group &parent_group;
		FooCanvasmm::Group *item_group;
		FooCanvasmm::Group *draw_group;
		FooCanvasmm::Group *overlay_group;
		FooCanvasmm::Group *item;

		sigc::signal0<void> canvas_item_changed;

	    public:
		virtual void update () = 0;

		sigc::signal0<void> deleted;
		sigc::signal0<void> clicked;
		sigc::signal0<void> released;

		// Position and drag & drop
		virtual void move (int dx, int dy);

		sigc::signal0<void>           drag_begin;
		sigc::signal2<void, int, int> drag_motion; // arguments are dx and dy
		sigc::signal2<void, int, int> drag_end;    // arguments are dx and dy
		sigc::signal0<void>           drag_cancel;

	    protected:
                void set_canvas_item (FooCanvasmm::Group *item);
                
		// Screen (physical) dimensions
		virtual void get_bounds (int &x1, int &y1,
					 int &x2, int &y2);

	    public:
		virtual bool is_within  (int x1, int y1,
					 int x2, int y2);


	    private:
		void canvas_event_cb  (GdkEvent *event);

		void drag_begin_impl  (GdkEvent *event);
		void drag_motion_impl (GdkEvent *event);
		void drag_end_impl    (GdkEvent *event);
	    };
	}
    }
}

#endif /* !GUIKACHU_FORM_EDITOR_CANVASITEM_H */
