//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_FONT_H
#define GUIKACHU_FORM_EDITOR_FONT_H

#include <string>
#include <iostream>
#include "hash-map.h"
#include <list>

#include <gdkmm/pixbuf.h>
#include <gdkmm/color.h>

namespace Guikachu
{
    namespace GUI
    {
	namespace FormEditor
	{
	    class Glyph
	    {
		int   width;
		int   offset;
		bool *bitmap;

	    public:
		Glyph (std::istream &stream, int &height);
		Glyph (int height, int ascent); // Create placeholder glyph
		~Glyph ();

		int get_width  () const { return width; }
		int get_offset () const { return offset; }
		bool get_pixel (int x, int y) const { return bitmap[y * width + x]; }
	    };
	    
	    class Font
	    {
		std::hash_map <char, Glyph> glyphs;
		int                         line_height, ascent, descent;
		
	    public:
		Font ();
                ~Font ();

		void parse (const std::string &filename);
		void parse (std::istream      &stream);
		
		void get_text_extents (const std::string &text,
				       int               &width,
				       int               &height) const;


		int get_line_width   (const std::string &text) const;
		
		int get_line_height  () const { return line_height; };
		int get_ascent       () const { return ascent;      };
		int get_descent      () const { return descent;     };

		const Glyph& get_glyph (char c) const;

	    private:
                Glyph *empty_glyph;
		int get_raw_line_width (const std::string &text) const;
	    };
	}
    }
}

#endif /* !GUIKACHU_FORM_EDITOR_FONT_H */
