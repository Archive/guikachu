//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "label-prop.h"

#include <glib/gi18n.h>

#include "usable-prop.h"
#include "textual-prop.h"

#include "widgets/propertytable.h"

#include "property-ops-widget.h"

using namespace Guikachu::GUI::FormEditor;

LabelProperties::LabelProperties (Widgets::Label *res):
    WidgetProperties (res)
{
    using WidgetOps::PropChangeOpFactory;
    
    GUI::PropertyTable *proptable = new GUI::PropertyTable ();
    
    /* Resource ID */
    add_general_controls (*proptable);

    /* Text */
    TextualProperties::add_controls (res, *proptable);
    
    /* Usable */
    UsableProperties::add_controls (res, *proptable);

    /* Separator */
    proptable->add_separator ();

    /* Position */
    add_position_controls (*proptable);
    
    proptable->show ();
    property_editor = proptable;
}

LabelProperties::~LabelProperties ()
{
    delete property_editor;
}

Gtk::Widget* LabelProperties::get_editor ()
{
    return property_editor;
}
