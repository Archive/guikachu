//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_WIDGET_FACTORY_CANVAS_H
#define GUIKACHU_FORM_EDITOR_WIDGET_FACTORY_CANVAS_H

#include "form-editor/widget-visitor.h"

#include "form-editor/widget.h"
#include "form-editor/widget-canvas.h"

#include "form-editor/button-canvas.h"
#include "form-editor/checkbox-canvas.h"
#include "form-editor/formbitmap-canvas.h"
#include "form-editor/gadget-canvas.h"
#include "form-editor/graffiti-canvas.h"
#include "form-editor/label-canvas.h"
#include "form-editor/list-canvas.h"
#include "form-editor/popup-trigger-canvas.h"
#include "form-editor/pushbutton-canvas.h"
#include "form-editor/scrollbar-canvas.h"
#include "form-editor/slider-canvas.h"
#include "form-editor/selector-trigger-canvas.h"
#include "form-editor/table-canvas.h"
#include "form-editor/text-field-canvas.h"

namespace Guikachu
{
    namespace GUI
    {
	namespace FormEditor
	{
	    class CanvasItemManager: public sigc::trackable
	    {
	    public:
		typedef std::map<FooCanvasmm::Group*, WidgetCanvasItem*> item_map_t;
		
	    private:
		Widget *widget;
		item_map_t items;
		
		void item_deleted_cb (WidgetCanvasItem *item);
		
	    public:
		~CanvasItemManager ();
		
		CanvasItemManager (Widget *widget);
		WidgetCanvasItem * get_canvas_item (FooCanvasmm::Group &group);
	    };
	    
	    class CanvasItemFactoryVisitor: public WidgetVisitor
	    {
		WidgetCanvasItem      *result;
		FooCanvasmm::Group &group;
	
	    public:
		CanvasItemFactoryVisitor (Widget *widget, FooCanvasmm::Group &group_) :
		    result (0),
		    group (group_)
		    {
			widget->apply_visitor (*this);
		    }
		
		WidgetCanvasItem * get_result () const
		    {
			return result;
		    }
		
#define GUIKACHU_WIDGET_CANVAS_VISIT(Type)				\
		void visit_widget (Widgets::Type *widget)		\
		{							\
		    result = new Type##CanvasItem (widget, group);	\
		}

		GUIKACHU_WIDGET_CANVAS_VISIT(Label);
		GUIKACHU_WIDGET_CANVAS_VISIT(Button);
		GUIKACHU_WIDGET_CANVAS_VISIT(PushButton);
		GUIKACHU_WIDGET_CANVAS_VISIT(Graffiti);
		GUIKACHU_WIDGET_CANVAS_VISIT(SelectorTrigger);
		GUIKACHU_WIDGET_CANVAS_VISIT(Checkbox);
		GUIKACHU_WIDGET_CANVAS_VISIT(List);
		GUIKACHU_WIDGET_CANVAS_VISIT(PopupTrigger);
		GUIKACHU_WIDGET_CANVAS_VISIT(ScrollBar);
		GUIKACHU_WIDGET_CANVAS_VISIT(Slider);
		GUIKACHU_WIDGET_CANVAS_VISIT(TextField);
		GUIKACHU_WIDGET_CANVAS_VISIT(Table);
		GUIKACHU_WIDGET_CANVAS_VISIT(FormBitmap);
		GUIKACHU_WIDGET_CANVAS_VISIT(Gadget);
	    };
	    
	} // namespace FormEditor
    } // namespace GUI
} // namespace Guikachu

#endif /* !GUIKACHU_FORM_EDITOR_WIDGET_FACTORY_EDITOR_H */
