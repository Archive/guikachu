//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

/* This code may need a clean-up, but it Works(tm) at the moment */

#include "scrollbar-canvas.h"

#include "form-editor.h"
#include "resizeable-ops.h"

#include <gdkmm/pixbuf.h>
#include <foocanvasmm/pixbuf.h>

#include "form-editor-canvas.h"

using namespace Guikachu::GUI::FormEditor;

ScrollBarCanvasItem::ScrollBarCanvasItem (Widgets::ScrollBar *widget_,
					  FooCanvasmm::Group &parent_group):
    CanvasItem (widget_, parent_group),
    WidgetCanvasItem (widget_, parent_group),
    ResizeableCanvasItem (widget_, parent_group, new WidgetOps::ResizeOpFactory (widget_)),
    widget (widget_)
{
}

void ScrollBarCanvasItem::draw (FooCanvasmm::Group &group) const
{
    int width = widget->get_width ();
    int height = widget->height;

    int x1 = widget->x;
    int y1 = widget->y;
    int y2 = y1 + height;


    // Step 1: the arrows
    int arrow_width  = width;
    int arrow_height = (arrow_width + 1) / 2;
    // FIXME: sanity checks to ensure width/height things

    Glib::RefPtr<Gdk::Pixbuf> up_pixbuf =
	Gdk::Pixbuf::create (Gdk::COLORSPACE_RGB, true, 8, arrow_width, arrow_height);
    Glib::RefPtr<Gdk::Pixbuf> dn_pixbuf =
	Gdk::Pixbuf::create (Gdk::COLORSPACE_RGB, true, 8, arrow_width, arrow_height);
    
    guint8     *up_pixbuf_data = up_pixbuf->get_pixels ();
    guint8     *dn_pixbuf_data = dn_pixbuf->get_pixels ();
    Gdk::Color  color (get_foreground_color (widget->usable));
    int         up_rowstride = up_pixbuf->get_rowstride ();
    int         dn_rowstride = dn_pixbuf->get_rowstride ();

    memset (up_pixbuf_data, 0, arrow_height * up_rowstride);
    memset (dn_pixbuf_data, 0, arrow_height * dn_rowstride);
    
    int row_indent = 0;
    for (int row = arrow_height; row > 0; row--, row_indent++)
    {
	int y_up_offset = up_rowstride * (row - 1);
	int y_dn_offset = dn_rowstride * (arrow_height - row);
	
	int row_max_x = arrow_width - row_indent;
	
	for (int x = row_indent; x < row_max_x; x++)
	{
	    up_pixbuf_data[(y_up_offset + x * 4) + 0] = color.get_red ()   >> 8;
	    up_pixbuf_data[(y_up_offset + x * 4) + 1] = color.get_green () >> 8;
	    up_pixbuf_data[(y_up_offset + x * 4) + 2] = color.get_blue ()  >> 8;
	    up_pixbuf_data[(y_up_offset + x * 4) + 3] = 255;

	    dn_pixbuf_data[(y_dn_offset + x * 4) + 0] = color.get_red ()   >> 8;
	    dn_pixbuf_data[(y_dn_offset + x * 4) + 1] = color.get_green () >> 8;
	    dn_pixbuf_data[(y_dn_offset + x * 4) + 2] = color.get_blue ()  >> 8;
	    dn_pixbuf_data[(y_dn_offset + x * 4) + 3] = 255;
	}
    }
    
    new FooCanvasmm::Pixbuf (group, x1, y1 + 1, up_pixbuf);
    new FooCanvasmm::Pixbuf (group, x1, y2 - arrow_height - 1, dn_pixbuf);
    
    // Step 2: the scrolling range

    /* FIXME: the scrolling range should have a minimum width of 1 */
    int range_height = height - 2 * (arrow_height + 2);
    int range_width = MAX (width - 4, 0);
    int range_x1 = x1 + 2;
    int range_x2 = range_x1 + range_width;
    int range_y1 = y1 + arrow_height + 2;
    int range_y2 = range_y1 + range_height;
    
    /* Data for the GdkBitmap to use as a fill stipple */
    static const char stipple_bits[] = { 0x02, 0x01 };
    static const Glib::RefPtr<Gdk::Bitmap> stipple =
	Gdk::Bitmap::create (stipple_bits, 2, 2);

    using namespace FooCanvasmm::Properties;
    
    *(FormEditor::draw_rectangle (group,
				  range_x1, range_y1,
				  range_x2, range_y2))
	    << fill_stipple (stipple)
	    << fill_color (get_foreground_color (widget->usable));

    // Step 3: The value marker

    int value_range_size = widget->max_value - widget->min_value;

    int slider_size = 10;
    if (widget->page_size)
    {
	double page_ratio = (double) value_range_size / (double) widget->page_size;
	double page_size = 1.0 / (page_ratio + 1.0) * range_height;

	if (page_size > 10)
	    slider_size = (int) page_size;
    }
    
    int slider_y = 0;
    if (value_range_size)
    {
	int value_abs = widget->value - widget->min_value;
	double value_ratio = (double)value_abs / (double)value_range_size;

	if (value_ratio < 0)
	    value_ratio = 0;
	
	if (value_ratio > 1)
	    value_ratio = 1;

	slider_y = (int) (value_ratio * (range_height - slider_size));
    }
    
    int slider_top = range_y1 + slider_y;
    int slider_bottom = slider_top + slider_size;
    
    // Space around the mark
    *(FormEditor::draw_rectangle (group,
				  range_x1, slider_top - 1,
				  range_x2, slider_bottom + 1))
        << fill_color (get_background_color ());

    // The mark itself
    *(FormEditor::draw_rectangle (group,
				  range_x1, slider_top,
				  range_x2, slider_bottom))
      << fill_color (get_foreground_color (widget->usable));
}
