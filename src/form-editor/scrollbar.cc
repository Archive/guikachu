//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "scrollbar.h"
#include <sigc++/object_slot.h>

using Guikachu::Widgets::ScrollBar;

ScrollBar::ScrollBar (Resources::Form   *owner,
		      const std::string &id,
		      serial_t           serial):
    ResizeableWidget (changed),
    Widget (owner, id, serial),
    AutoWidth (changed),
    Usable (changed),
    
    min_value (changed, 0),
    max_value (changed, 0),
    value (changed, 0),
    page_size (changed, 0)
{
    height = 40;
    
    changed.connect (sigc::mem_fun (*this, &ScrollBar::update));
}

void ScrollBar::update ()
{
    static bool update_block = false;

    if (update_block)
	return;

    update_block = true;
    
    if (max_value < min_value)
	max_value = min_value;

    if (value < min_value)
	value = min_value;

    if (value > max_value)
	value = max_value;

    update_block = false;
}

int ScrollBar::get_auto_width () const
{
    return 7; /* recommended width for scroll bars */
}
