//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "form-editor/widget-factory.h"
#include "form-editor/widget-factory-canvas.h"
#include "form-editor/widget-factory-editor.h"

using namespace Guikachu;
using namespace Guikachu::GUI::FormEditor;

namespace
{	    
    class WidgetManager: public sigc::trackable
    {
    public:
	typedef std::map<Widget*, CanvasItemManager*> item_manager_map_t;
	typedef std::map<Widget*, WidgetProperties*>  editor_map_t;
	
    private:
	item_manager_map_t item_managers;
	editor_map_t       editors;
	
	void widget_deleted_cb (Widget *widget);
	
    public:
	WidgetCanvasItem * get_canvas_item (Widget *widget, FooCanvasmm::Group &group);
	Gtk::Widget *      get_editor (Widget *widget);
    };
	    
    WidgetManager * get_widget_manager ()
    {
	static WidgetManager * instance = 0;
	
	if (!instance)
	    instance = new WidgetManager;
	
	return instance;
    }
}

void WidgetManager::widget_deleted_cb (Widget *widget)
{
    item_manager_map_t::iterator manager_found = item_managers.find (widget);
    if (manager_found != item_managers.end ())
    {
	delete manager_found->second;
	item_managers.erase (manager_found);
    }

    editor_map_t::iterator editor_found = editors.find (widget);
    if (editor_found != editors.end ())
    {
	delete editor_found->second;
	editors.erase (editor_found);
    }    
}   

WidgetCanvasItem * WidgetManager::get_canvas_item (Widget             *widget,
						   FooCanvasmm::Group &group)
{
    item_manager_map_t::iterator found = item_managers.find (widget);
    if (found != item_managers.end ())
	return found->second->get_canvas_item (group);
    
    if (editors.find (widget) == editors.end ())
	widget->deleted.connect (
            sigc::bind (sigc::mem_fun (*this, &WidgetManager::widget_deleted_cb), widget));
    
    CanvasItemManager *item_manager = item_managers[widget] = new CanvasItemManager (widget);
    return item_manager->get_canvas_item (group);
}

Gtk::Widget * WidgetManager::get_editor (Widget *widget)
{
    editor_map_t::iterator found = editors.find (widget);
    if (found != editors.end ())
	return found->second->get_editor ();
    
    if (item_managers.find (widget) == item_managers.end ())
	widget->deleted.connect (
            sigc::bind (sigc::mem_fun (*this, &WidgetManager::widget_deleted_cb), widget));
    
    WidgetProperties *editor = editors[widget] = EditorFactoryVisitor (widget).get_result ();
    return editor->get_editor ();
}


WidgetCanvasItem * ::Guikachu::GUI::FormEditor::get_widget_canvas_item (Widget *widget, FooCanvasmm::Group &group)
{
    return get_widget_manager ()->get_canvas_item (widget, group);
}

Gtk::Widget * ::Guikachu::GUI::FormEditor::get_widget_editor (Widget *widget)
{
    return get_widget_manager ()->get_editor (widget);
}
