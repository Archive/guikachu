//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "widget.h"

#include "resource-manager.h"

using namespace Guikachu;

Widget::Widget (Resources::Form   *form_,
		const std::string &id_,
		serial_t           serial_):
    form (form_),
    serial (serial_),
    id (changed, form, id_),
    x (changed, 0),
    y (changed, 0)
{
}

Widget::~Widget ()
{
    deleted.emit ();
}

ResourceManager* Widget::get_manager () const
{
    return form->get_manager ();
}

Resources::Form* Widget::get_form () const
{
    return form;
}
