//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_TEXTUAL_H
#define GUIKACHU_FORM_EDITOR_TEXTUAL_H

#include <sigc++/signal.h>
#include "property.h"

namespace Guikachu
{
    namespace Widgets
    {
	class Textual
	{
	protected:
	    Textual (VSignal           &changed,
		     const std::string &default_text = "");

	public:
	    virtual ~Textual () {};

	    Property<std::string> text;
	    Property<int>         font;
	};
    }
}

#endif /* !GUIKACHU_FORM_EDITOR_TEXTUAL_H */
