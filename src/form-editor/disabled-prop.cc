//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "disabled-prop.h"

#include <glib/gi18n.h>

#include "property-ops-widget.h"

#include "widgets/togglebutton.h"

using namespace Guikachu::GUI::FormEditor;

void DisabledProperties::add_controls (Widgets::Disabled    *res,
                                     GUI::PropertyTable &proptable)
{
    Gtk::Widget *control;
    
    /* Disabled */
    control = new GUI::PropertyEditors::ToggleButton (
	res->disabled, new WidgetOps::PropChangeOpFactory<bool> (
            _("Toggle disabled state of %s"), dynamic_cast<Widget*> (res), res->disabled, false));
    proptable.add (_("_Disabled:"), *manage (control),
                   _("Disabled widgets cannot be changed by the user"));
}
