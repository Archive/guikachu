//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "form-editor/widget-factory-canvas.h"

using namespace Guikachu::GUI::FormEditor;

CanvasItemManager::CanvasItemManager (Widget *widget_):
    widget (widget_)
{
}

CanvasItemManager::~CanvasItemManager ()
{
    item_map_t items_cp = items;
    
    for (item_map_t::iterator i = items_cp.begin (); i != items_cp.end (); ++i)
	delete i->second;
}

namespace
{
    struct item_finder
    {
	WidgetCanvasItem *search_item;

	bool operator () (const std::pair<FooCanvasmm::Group*, WidgetCanvasItem*> &i) const
	    {
		return i.second == search_item;
	    }

	item_finder (WidgetCanvasItem *search_item_):
	    search_item (search_item_)
	    {}
    };
}

void CanvasItemManager::item_deleted_cb (WidgetCanvasItem *canvas_item)
{
    item_map_t::iterator found = std::find_if (
	items.begin (), items.end (), item_finder (canvas_item));
    
    if (found != items.end ())
    {
	items.erase (found);
    }
	
}

WidgetCanvasItem * CanvasItemManager::get_canvas_item (FooCanvasmm::Group &group)
{
    item_map_t::iterator found = items.find (&group);
    if (found != items.end ())
	return found->second;

    WidgetCanvasItem *canvas_item = CanvasItemFactoryVisitor (widget, group).get_result ();
    canvas_item->deleted.connect (
	sigc::bind (sigc::mem_fun (*this, &CanvasItemManager::item_deleted_cb), canvas_item));

    items[&group] = canvas_item;
    
    return canvas_item;
}
