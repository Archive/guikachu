//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_LIST_PROP_H
#define GUIKACHU_FORM_EDITOR_LIST_PROP_H

#include "list.h"
#include "widget-prop.h"

#include <sigc++/trackable.h>

namespace Guikachu
{
    namespace GUI
    {
	namespace FormEditor
	{
	    class ListProperties: public WidgetProperties,
				  public sigc::trackable
	    {
		Gtk::Widget *property_editor;
	    public:
		ListProperties (Widgets::List *res);
		~ListProperties ();
		Gtk::Widget *get_editor ();
	    };
	}
    }
}

#endif /* !GUIKACHU_FORM_EDITOR_LIST_PROP_H */
