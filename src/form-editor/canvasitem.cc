//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "canvasitem.h"

#include <algorithm>
#include <math.h>
#include <sigc++/adaptors/bind_return.h>

#include "form-editor.h"

using namespace Guikachu::GUI::FormEditor;

CanvasItem::CanvasItem (FormElement        *form_element_,
			FooCanvasmm::Group &parent_group_):
    form_element (form_element_),
    parent_group (parent_group_),
    item_group (new FooCanvasmm::Group (parent_group, 0, 0)),
    draw_group (new FooCanvasmm::Group (*item_group, 0, 0)),
    overlay_group (new FooCanvasmm::Group (*item_group, 0, 0)),
    item (0)
{
    overlay_group->raise_to_top ();    
    
    drag_context.dragging = false;
    drag_context.drag_occured = false;

}

CanvasItem::~CanvasItem ()
{
    deleted.emit ();
    
    delete item_group;
}

void CanvasItem::set_canvas_item (FooCanvasmm::Group *item_)
{
    item = item_;
    
    item->signal_event ().connect (sigc::bind_return (
	sigc::mem_fun (*this, &CanvasItem::canvas_event_cb), false));

    item_group->property_x () = 0;
    item_group->property_y () = 0;
    item_group->move (0, 0);

    canvas_item_changed.emit ();
}

void CanvasItem::canvas_event_cb (GdkEvent *e)
{
    // Single click
    if (e->type == GDK_BUTTON_PRESS &&
	e->button.button == 1)
	clicked.emit ();

    // Single click -- release
    if (e->type == GDK_BUTTON_RELEASE &&
	e->button.button == 1)
	released.emit ();

    // Begin drag
    if (e->type == GDK_BUTTON_PRESS &&
	e->button.button == 1 &&
	!drag_context.dragging)
	drag_begin_impl (e);
    
    // Dragging
    if (e->type == GDK_MOTION_NOTIFY &&
	drag_context.dragging)
	drag_motion_impl (e);

    // Ending drag
    if (e->type == GDK_BUTTON_RELEASE &&
	e->button.button == 1 &&
	drag_context.dragging)
	drag_end_impl (e);
}

void CanvasItem::drag_begin_impl (GdkEvent *e)
{
    drag_context.dragging = true;
    drag_context.drag_occured = false;

    drag_context.last_x = drag_context.start_x = e->button.x;
    drag_context.last_y = drag_context.start_y = e->button.y;
    
    drag_context.offset_x = drag_context.last_x - form_element->get_x ();
    drag_context.offset_y = drag_context.last_y - form_element->get_y ();

    drag_context.delta_remainder_x = 0;
    drag_context.delta_remainder_y = 0;

    Gdk::Cursor cursor (Gdk::FLEUR);
    item->grab (GDK_POINTER_MOTION_MASK | GDK_BUTTON_MOTION_MASK |
		GDK_BUTTON_RELEASE_MASK | GDK_BUTTON_PRESS_MASK,
		cursor, e->button.time);

    drag_begin.emit ();
}

void CanvasItem::drag_motion_impl (GdkEvent *e)
{
    int screen_width = form_element->get_manager ()->get_target ()->screen_width;
    int screen_height = form_element->get_manager ()->get_target ()->screen_height;
    
    drag_context.drag_occured = true;
    
    double curr_x = e->motion.x;
    double curr_y = e->motion.y;
    
    // East bound
    if ((curr_x - drag_context.offset_x) > (screen_width - 1))
	    curr_x = (screen_width - 1) + drag_context.offset_x;
    
    // South bound
    if ((curr_y - drag_context.offset_y) > (screen_height - 1))
	curr_y = (screen_height - 1) + drag_context.offset_y;
    
    // West bound
    if ((curr_x - drag_context.offset_x) < 0)
	curr_x = drag_context.offset_x;
    
    // North bound
    if ((curr_y - drag_context.offset_y) < 0)
	curr_y = drag_context.offset_y;

    // Remainders need to be collected
    double dx = (curr_x - drag_context.last_x) + drag_context.delta_remainder_x;
    double dy = (curr_y - drag_context.last_y) + drag_context.delta_remainder_y;

    drag_context.delta_remainder_x = fmod (dx, 1.0);
    drag_context.delta_remainder_y = fmod (dy, 1.0);
    
    drag_context.last_x = curr_x;
    drag_context.last_y = curr_y;     

    drag_motion.emit (int (dx), int (dy));
}

void CanvasItem::drag_end_impl (GdkEvent *e)
{
    item->ungrab (e->button.time);
    drag_context.dragging = false;
    
    if (drag_context.drag_occured)
    {
	int dx = (int)(drag_context.last_x - drag_context.start_x - drag_context.delta_remainder_x);
	int dy = (int)(drag_context.last_y - drag_context.start_y - drag_context.delta_remainder_y);

	drag_end.emit (dx, dy);
    }
}

void CanvasItem::move (int dx, int dy)
{
    item_group->move (dx, dy);
}

void CanvasItem::get_bounds (int &x1, int &y1,
			     int &x2, int &y2)
{
    double x1_ (0), y1_ (0);
    double x2_ (0), y2_ (0);
    
    item->get_bounds (x1_, y1_, x2_, y2_);

    x1 = int (x1_);
    y1 = int (y1_);
    x2 = int (x2_);
    y2 = int (y2_);
}

bool CanvasItem::is_within (int x1, int y1,
			    int x2, int y2)
{
    int real_x1 = std::min (x1, x2);
    int real_x2 = std::max (x1, x2);

    int real_y1 = std::min (y1, y2);
    int real_y2 = std::max (y1, y2);

    int own_x1, own_y1, own_x2, own_y2;
    get_bounds (own_x1, own_y1, own_x2, own_y2);
    
    return (real_x1 <= own_x1 && real_y1 <= own_y1 &&
	    real_x2 >= own_x2 && real_y2 >= own_y2);
}
