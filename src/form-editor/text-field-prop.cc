//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "text-field-prop.h"

#include <glib/gi18n.h>

#include "resizeable-prop.h"
#include "auto-resizeable-prop.h"
#include "usable-prop.h"
#include "disabled-prop.h"

#include "widgets/num-entry.h"
#include "widgets/togglebutton.h"
#include "widgets/propertytable.h"
#include "widgets/font-combo.h"

#include "property-ops-widget.h"

using namespace Guikachu::GUI::FormEditor;
using namespace Guikachu::Widgets;

TextFieldProperties::TextFieldProperties (TextField *res):
    WidgetProperties (res)
{
    using WidgetOps::PropChangeOpFactory;
    
    GUI::PropertyTable *proptable = new GUI::PropertyTable ();
    Gtk::Widget *control;
    
    /* Resource ID */
    add_general_controls (*proptable);

    /* Font */
    control = new GUI::PropertyEditors::FontCombo (
	res->font,
	new PropChangeOpFactory<int> (_("Change font of %s"), res, res->font, false));
    proptable->add (_("_Font:"), *manage (control));
    
    /* Editable */
    control = new GUI::PropertyEditors::ToggleButton (
	res->editable,
	new PropChangeOpFactory<bool> (_("Toggle editable state of %s"), res, res->editable, false));
    proptable->add (_("_Editable:"), *manage (control));

    /* Multi-line */
    control = new GUI::PropertyEditors::ToggleButton (
	res->multi_line,
	new PropChangeOpFactory<bool> (
	    _("Toggle multi-line state of %s"), res, res->multi_line, false));
    proptable->add (_("_Multi-line:"), *manage (control));

    /* Numeric */
    control = new GUI::PropertyEditors::ToggleButton (
	res->numeric,
	new PropChangeOpFactory<bool> (_("Toggle numeric state of %s"), res, res->numeric, false));
    proptable->add (_("Allow _numeric only:"), *manage (control),
		    _("Numeric text fields only accept numbers as input"));

    /* max_length */
    control = new GUI::PropertyEditors::NumEntry (
	0, 32767, res->max_length,
	new PropChangeOpFactory<int> (_("Change maximum length of %s"), res, res->max_length, true));
    proptable->add (_("Maximum length:"), *manage (control),
		    _("Maximum allowed text length, in bytes. "
		      "0 means maximum length allowed by PalmOS"));

    proptable->add_separator ();
    
    /* Underline */
    control = new GUI::PropertyEditors::ToggleButton (
	res->underline,
	new PropChangeOpFactory<bool> (_("Toggle underlining of %s"), res, res->underline, false));
    proptable->add (_("U_nderlined:"), *manage (control));
    
    /* Auto-shift */
    control = new GUI::PropertyEditors::ToggleButton (
	res->auto_shift,
	new PropChangeOpFactory<bool> (_("Toggle auto-shift of %s"), res, res->auto_shift, false));
    proptable->add (_("_Auto-shift:"), *manage (control),
		    _("Toggle intelligent shift state managment "
		      "(e.g. turning shift on after a dot)"));

    /* Dynamic size */
    control = new GUI::PropertyEditors::ToggleButton (
	res->dynamic_size,
	new PropChangeOpFactory<bool> (
	    _("Toggle dynamic resizing of %s"), res, res->dynamic_size, false));
    proptable->add (_("_Dynamic size:"), *manage (control),
		    _("Resize text field based on content's size"));
    
    /* Justify right */
    control = new GUI::PropertyEditors::ToggleButton (
	res->justify_right,
	new PropChangeOpFactory<bool> (_("Change alignment of %s"), res, res->justify_right, false));
    proptable->add (_("_Right-aligned:"), *manage (control),
		    _("Toggle left/right justification. "
		      "Right-justified fields don't accept "
		      "TAB characters"));
    
    /* has_scrollbar */
    control = new GUI::PropertyEditors::ToggleButton (
	res->has_scrollbar,
	new PropChangeOpFactory<bool> (_("Toggle scrollbar of %s"), res, res->has_scrollbar, false));
    proptable->add (_("_Scrollbar:"), *manage (control),
		    _("Turn this on to display a vertical scrollbar "
		      "if the text entered is longer than the field's "
		      "visible size"));
    
    /* Usable */
    UsableProperties::add_controls (res, *proptable);
    DisabledProperties::add_controls (res, *proptable);
    
    /* Separator */
    proptable->add_separator ();

    /* Position */
    add_position_controls (*proptable);

    /* Size */
    ResizeableProperties::add_width_controls (res, *proptable);
    AutoHeightProperties::add_controls (res, *proptable);
    
    proptable->show ();
    property_editor = proptable;
}

TextFieldProperties::~TextFieldProperties ()
{
    delete property_editor;
}

Gtk::Widget* TextFieldProperties::get_editor ()
{
    return property_editor;
}
