//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_EDITOR_WIDGET_H
#define GUIKACHU_FORM_EDITOR_WIDGET_H

namespace Guikachu
{
    namespace Widgets
    {
	enum Type
	{
	    WIDGET_NONE,
	    WIDGET_LABEL,
	    WIDGET_BUTTON,
	    WIDGET_PUSHBUTTON,
	    WIDGET_GRAFFITI,
	    WIDGET_SELECTOR_TRIGGER,
	    WIDGET_CHECKBOX,
	    WIDGET_LIST,
	    WIDGET_POPUP_TRIGGER,
	    WIDGET_SCROLLBAR,
	    WIDGET_SLIDER,
	    WIDGET_TEXT_FIELD,
	    WIDGET_TABLE,
            WIDGET_FORMBITMAP,
	    WIDGET_GADGET,
	};
    }
    
    class Widget;
}

#include <string>
#include <map>
#include <sigc++/trackable.h>
#include <sigc++/signal.h>

#include "form-res.h"
#include "form-editor/form-element.h"
#include "form-editor/moveable.h"
#include "form-editor/widget-visitor.h"

#include "property.h"

namespace Guikachu
{
    class Widget: public virtual FormElement,
                  public virtual Moveable,
		  public virtual sigc::trackable
    {
    protected:
	Resources::Form *form;
	serial_t         serial;
	
	Widget (Resources::Form   *form,
		const std::string &id,
		serial_t           serial);
	
    public:
        typedef Widgets::Type type_t;
        
	virtual ~Widget ();
	
	virtual Widgets::Type get_type () const = 0;

	// Generic visitor hook
	virtual void apply_visitor (WidgetVisitor &visitor) = 0;
	
	// Notification
	QueuedSignal        changed;
	sigc::signal0<void> deleted; 

        // Common properties
	ID            id;
        Property<int> x;
        Property<int> y;
        
        // FormElement implementations
        int get_x () const { return x; };
        int get_y () const { return y; };

        // Moveable implementations
        void set_x (int x_) { x = x_; };
        void set_y (int y_) { y = y_; };        
        
        // Managing
	serial_t get_serial () const { return serial; };
        
	Resources::Form *get_form    () const;
	ResourceManager *get_manager () const;
    };
}

#endif /* !GUIKACHU_FORM_EDITOR_WIDGET_H */
