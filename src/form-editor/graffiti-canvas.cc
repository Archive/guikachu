//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "graffiti-canvas.h"

#include "form-editor.h"

#include <gdkmm/pixbuf.h>
#include <foocanvasmm/pixbuf.h>

#include "pixmaps/graffiti-canvas.xpm"

using namespace Guikachu::GUI::FormEditor;

GraffitiCanvasItem::GraffitiCanvasItem (Widgets::Graffiti  *widget_,
					FooCanvasmm::Group &parent_group):
    CanvasItem (widget_, parent_group),
    WidgetCanvasItem (widget_, parent_group),
    widget (widget_)
{
}

void GraffitiCanvasItem::draw (FooCanvasmm::Group &group) const
{
    Glib::RefPtr<Gdk::Pixbuf> pixbuf =
        Gdk::Pixbuf::create_from_xpm_data (graffiti_canvas_xpm);
    
    FooCanvasmm::Pixbuf *pixbuf_item = new FooCanvasmm::Pixbuf (group, widget->x, widget->y, pixbuf);
    pixbuf_item->property_interp_type () = Gdk::INTERP_NEAREST;
}
