//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "bitmapfamily-win-helpers.h"
#include "bitmap-win-helpers.h"

#include <glib/gi18n.h>

using namespace Guikachu::GUI::BitmapFamilyWindow_Helpers;
using namespace Guikachu::GUI::BitmapWindow_Helpers;;

DepthList::DepthList (Resources::BitmapFamily *res_):
    res (res_),
    update_block (false)
{
    Gtk::TreeModel::ColumnRecord cols;
    cols.add (col_label);
    cols.add (col_type);
    
    liststore = Gtk::ListStore::create (cols);
    set_model (liststore);
    
    append_column ("", col_label);
    set_headers_visible (false);

    get_selection ()->set_mode (Gtk::SELECTION_BROWSE);
    get_selection ()->signal_changed ().connect (
	sigc::mem_fun (*this, &DepthList::selection_changed_cb));
    
    res->changed.connect (sigc::mem_fun (*this, &DepthList::update));

    const depth_list_t &depth_list = BitmapWindow_Helpers::get_depth_list ();
    for (depth_list_t::const_iterator i = depth_list.begin ();
         i != depth_list.end (); ++i)
        liststore->append ();
    get_selection ()->select (Gtk::TreeModel::Path ("0"));
    update ();
}

void DepthList::update ()
{
    update_block = true;

    const depth_list_t &depth_list = get_depth_list ();
    Gtk::ListStore::iterator store_iter = liststore->get_iter ("0");
    for (depth_list_t::const_iterator i = depth_list.begin ();
         i != depth_list.end (); ++i, ++store_iter)
    {
        char *label_str;
        if (res->get_image (i->first))
            label_str = g_strdup (i->second.c_str ());
        else
            label_str = g_strdup_printf (_("%s (unset)"), i->second.c_str ());
        
        (*store_iter)[col_type] = i->first;
        (*store_iter)[col_label] = label_str;

        g_free (label_str);
    }
        
    update_block = false;
}

void DepthList::selection_changed_cb ()
{
    if (update_block)
        return;

    Gtk::TreeModel::iterator iter = get_selection ()->get_selected ();
    g_return_if_fail (iter);

    type_changed.emit ((*iter)[col_type]);
}
