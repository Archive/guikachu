//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_PROPERTY_OPS_WIDGET_H
#define GUIKACHU_PROPERTY_OPS_WIDGET_H

#include "property-ops.h"

#include "form-editor/widget.h"

/* Since current C++ compilers require all template class methods to
 * be defined at their declarations, this header is messy and reveals
 * lots of implementation detail.
 */

namespace Guikachu
{
    namespace WidgetOps
    {
	template<typename T>
	class PropChangeOp: public Guikachu::PropChangeOp<T>
	{
	protected:
	    typedef T           value_t;
	    typedef Property<T> property_t;
	
	    typedef Guikachu::MemberHolder<Widget, property_t> holder_t;
	
	    ResourceManager *manager;
	    serial_t         form_serial, widget_serial;
	    holder_t         prop_holder;
	    bool             cascade;
	
	public:
	    PropChangeOp (const Glib::ustring &label,
			  Widget              *widget,
			  property_t          &prop,
			  const value_t       &new_val,
			  bool                 cascade_) :
		Guikachu::PropChangeOp<T> (label, prop, new_val),
		manager (widget->get_manager ()),
		form_serial (widget->get_form ()->get_serial ()),
		widget_serial (widget->get_serial ()),
		prop_holder (*widget, prop),
		cascade (cascade_)
		{}
	    
	    UndoOp * combine (UndoOp *other_op) const {
		if (!cascade)
		    return 0;

		PropChangeOp<value_t> *op = dynamic_cast<PropChangeOp<value_t>*> (other_op);
		if (!op)
		    return 0;

		if (!op->cascade ||
		    op->form_serial != form_serial ||
		    op->widget_serial != widget_serial ||
		    op->prop_holder != prop_holder)
		    return 0;

		Widget *widget = get_widget ();
		g_assert (widget);
	    
		property_t &prop = prop_holder.get_member (*widget);
	    
		PropChangeOp<value_t> *new_op = new PropChangeOp<value_t> (
		    this->label, widget, prop, op->new_val, true);
		new_op->old_val = this->old_val;

		return new_op;
	    }
	
	protected:
	    property_t & get_prop () const {
		Widget *widget = get_widget ();
		g_assert (widget);
	    
		return prop_holder.get_member (*widget);
	    }

	private:
	    Widget * get_widget () const {
		Resource *resource = manager->get_resource (form_serial);
		Resources::Form *form = static_cast<Resources::Form*> (resource);
		g_assert (form);

		Widget *widget = form->get_widget (widget_serial);
		return widget;
	    }
	};
    
	template<typename T>
	class PropChangeOpFactory: public Guikachu::PropChangeOpFactory<T>
	{
	    typedef T           value_t;
	    typedef Property<T> property_t;
	
	    Glib::ustring  label_template;
	    Widget        *widget;
	    property_t    &prop;
	    bool           cascade;
	
	    UndoManager &undo_manager;
	
	public:
	    PropChangeOpFactory (const std::string &label_template_,
				 Widget            *widget_,
				 property_t        &prop_,
				 bool               cascade_ = false) :
		label_template (label_template_),
		widget (widget_),
		prop (prop_),
		cascade (cascade_),
		undo_manager (widget->get_manager ()->get_undo_manager ())
		{}
	
	    void push_change (const value_t &value) {
		char *label = g_strdup_printf (label_template.c_str (), widget->id ().c_str ());
	    
		UndoOp *op = new PropChangeOp<value_t> (label, widget, prop, value, cascade);
		value_t old_value = prop;

		prop = value;
		if (prop != old_value)
		    undo_manager.push (op);
		else
		    delete op;

		g_free (label);
	    }
	};
    
	class RenameOpFactory: public Guikachu::PropChangeOpFactory<std::string>
	{
	    Widget      *widget;
	    UndoManager &undo_manager;
	
	public:
	    RenameOpFactory (Widget *widget_):
		widget (widget_),
		undo_manager (widget->get_manager ()->get_undo_manager ())
		{}
	
	    void push_change (const std::string &value) {
		char *label = g_strdup_printf (_("Rename %s to %s"),
					       widget->id ().c_str (),
					       value.c_str ());

		UndoOp *op = new PropChangeOp<std::string> (label, widget, widget->id, value, false);
		std::string old_value = widget->id;

		widget->id = value;
		if (widget->id != old_value)
		    undo_manager.push (op);
		else
		    delete op;
	    
		g_free (label);
	    }
	};

    } // namespace WidgetOps
} // namespace Guikachu

#endif /* !GUIKACHU_PROPERTY_OPS_WIDGET_H */
