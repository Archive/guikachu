//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_FORM_RES_H
#define GUIKACHU_FORM_RES_H

#include "resource.h"
#include "resource-ref.h"
#include "form-editor/widget.h"
#include "form-editor/widget-ref.h"

#include <set>

namespace Guikachu
{
    namespace Resources
    {
	class Form: public Resource, public IDManager
	{
	public:
	    typedef std::map<serial_t, Widget*> widget_map_t;

	private:
	    widget_map_t widgets;

	public:
	    Form (ResourceManager *manager, const std::string &name, serial_t serial);
	    ~Form ();
	    
	    Type get_type () const { return RESOURCE_FORM; };
	    void apply_visitor (ResourceVisitor &visitor) { visitor.visit_resource (this); };

	    // Widget managment
	    std::set<Widget*> get_widgets () const;
	    Widget*           get_widget  (const std::string &id) const;
	    Widget*           get_widget  (serial_t serial) const;
	    
	    Widget* create_widget (Widgets::Type  type,
				   std::string    id = "",
				   bool           try_alternate_names = false,
				   serial_t       serial = -1);
	    void    remove_widget (Widget        *widget);

	    sigc::signal1<void, Widget*> widget_created;
	    sigc::signal1<void, Widget*> widget_removed;
	    sigc::signal0<void> widget_changed;

	    // Properties
	    Property<std::string>   title;
	    Properties::ResourceRef help_id;
	    Properties::ResourceRef menu_id;

	    Property<bool>          modal;
	    Property<bool>          frame;
	    Property<bool>          savebehind;
	    
	    Properties::WidgetRef   def_button;

	    Property<int>           x;
	    Property<int>           y;
	    Property<int>           width;
	    Property<int>           height;

	    // Utility functions for widgets
	    int get_screen_width () const;
	    int get_screen_height () const;
	};
    }
}

#endif /* !GUIKACHU_FORM_RES_H */
