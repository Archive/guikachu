//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_RESOURCE_MANAGER_H
#define GUIKACHU_RESOURCE_MANAGER_H

namespace Guikachu
{
    class ResourceManager;
}

#include "id-manager.h"

#include "resource.h"
#include "app-res.h"
#include "target.h"

#include "undo.h"

#include <set>
#include <sigc++/signal.h>
#include <sigc++/trackable.h>
#include <time.h>

namespace Guikachu
{
    class ResourceManager: public IDManager, public sigc::trackable
    {
    public:
	typedef std::map<serial_t, Resource*> resource_map_t;

    private:
	resource_map_t         resource_map;
	Resources::Application app;
	Target                 target;

	UndoManager            undo_manager;
	
	bool                   dirty;
	int                    dirty_block;
	time_t                 modification_time;

    public:
	ResourceManager  ();
	~ResourceManager ();
	
	Resource* create_resource (Resources::Type type,
				   std::string     id = "",
				   bool            try_alternate_names = false,
				   serial_t        serial = -1);
	void remove_resource (Resource *resource);

	std::set<Resource*>     get_resources   () const;
	Resource*               get_resource    (const std::string &id) const;
	Resource*               get_resource    (serial_t serial) const;
	Resources::Application* get_application () { return &app; };
	Target*                 get_target      () { return &target; };

	UndoManager& get_undo_manager () { return undo_manager; };
	
	// Signals
	sigc::signal1<void, Resource*> resource_created;
	sigc::signal1<void, Resource*> resource_removed;
	sigc::signal0<void> dirty_state_changed;
	
	bool   is_dirty () const;
	time_t get_modification_time ();
	
	void clear_dirty ();
	void set_dirty ();
	void block_dirty ();
	void unblock_dirty ();
    };
}

#endif /* !GUIKACHU_RESOURCE_MANAGER_H */
