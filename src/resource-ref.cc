//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "resource-ref.h"

using namespace Guikachu::Properties;
using std::string;

ResourceRef::ResourceRef (notify_signal_t &notify_signal,
			  ResourceManager *manager_,
			  const string    &value_):
    Property<string> (notify_signal, value_),
    manager (manager_)
{
    current_resource = manager->get_resource (value);

    if (current_resource)
	current_resource_changed = current_resource->changed.connect (
	    sigc::mem_fun (*this, &ResourceRef::resource_changed_cb));

    manager->resource_created.connect (sigc::mem_fun (*this, &ResourceRef::resource_created_cb));
    manager->resource_removed.connect (sigc::mem_fun (*this, &ResourceRef::resource_removed_cb));
}

void ResourceRef::set_val (const string &value_)
{
    Resource *new_resource = manager->get_resource (value_);

    if (new_resource != current_resource)
    {
	current_resource_changed.disconnect ();
	current_resource = new_resource;

	if (current_resource)
	    current_resource_changed = current_resource->changed.connect (
		sigc::mem_fun (*this, &ResourceRef::resource_changed_cb));
    }

    Property<string>::set_val (value_);
}

void ResourceRef::resource_created_cb (Resource *res)
{
    if (res->id == value && !current_resource)
    {
	current_resource = res;
	current_resource_changed.disconnect ();
	current_resource_changed = current_resource->changed.connect (
	    sigc::mem_fun (*this, &ResourceRef::resource_changed_cb));
    }
}

void ResourceRef::resource_removed_cb (Resource *res)
{
    if (res == current_resource)
    {
	current_resource = 0;
	Property<string>::set_val ("");
    }
}

void ResourceRef::resource_changed_cb ()
{
    Property<string>::set_val (current_resource->id);
    resource_changed.emit ();
}
