//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 *
 * The contents of this file is either copied verbatim or ported to
 * C++, from libgnome-2.0, which in turn takes most of it from GNU
 * Gettext.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "gnome-i18n-cnp.h"

#include "config.h"
#include <glib/gi18n.h>

#include <stdio.h>
#include <glib.h>
#include <locale.h>
#include <string.h>

#include <map>

namespace
{
    GHashTable *alias_table = NULL;
    
    // read an alias file for the locales
    static void read_aliases (const char *file)
    {
	FILE *fp;
	char buf[256];
	if (!alias_table)
	    alias_table = g_hash_table_new (g_str_hash, g_str_equal);
	fp = fopen (file,"r");
	if (!fp)
	    return;
	while (fgets (buf,256,fp))
	{
	    char *p;
	    g_strstrip(buf);
	    if (buf[0]=='#' || buf[0]=='\0')
		continue;
	    p = strtok (buf,"\t ");
	    if (!p)
		continue;
	    p = strtok (NULL,"\t ");
	    if(!p)
		continue;
	    if (!g_hash_table_lookup (alias_table, buf))
		g_hash_table_insert (alias_table, g_strdup(buf), g_strdup(p));
	}
	fclose (fp);
    }
    
    // return the un-aliased language as a newly allocated string
    static char *
    unalias_lang (char *lang)
    {
	char *p;
	int i;
	if (!alias_table)
	{
#ifdef GUIKACHU_HAVE_LIBGNOMEUI
	    read_aliases (LIBGNOME_DATADIR "/locale/locale.alias");
#endif
	    read_aliases ("/usr/share/locale/locale.alias");
	    read_aliases ("/usr/local/share/locale/locale.alias");
	    read_aliases ("/usr/lib/X11/locale/locale.alias");
	    read_aliases ("/usr/openwin/lib/locale/locale.alias");
	}
	i = 0;
	while ((p=(char*)g_hash_table_lookup(alias_table,lang)) && strcmp(p, lang))
	{
	    lang = p;
	    if (i++ == 30)
	    {
		static gboolean said_before = FALSE;
		if (!said_before)
		    g_warning ("Too many alias levels for a locale, may indicate a loop");
		said_before = TRUE;
		return lang;
	    }
	}
	return lang;
    }
    
    std::string guess_category_value (const std::string &categoryname)
    {
	const gchar *retval;
	
	// The highest priority value is the `LANGUAGE' environment
	// variable.  This is a GNU extension.
	retval = g_getenv ("LANGUAGE");
	if (retval && retval[0] != '\0')
	    return retval;
	
	// `LANGUAGE' is not set.  So we have to proceed with the POSIX
	// methods of looking to `LC_ALL', `LC_xxx', and `LANG'.  On some
	// systems this can be done by the `setlocale' function itself.
	
	// Setting of LC_ALL overwrites all other.
	retval = g_getenv ("LC_ALL");  
	if (retval && retval[0] != '\0')
	    return retval;
	
	// Next comes the name of the desired category.
	retval = g_getenv (categoryname.c_str ());
	if (retval && retval[0] != '\0')
	    return retval;
	
	// Last possibility is the LANG environment variable.
	retval = g_getenv ("LANG");
	if (retval && retval[0] != '\0')
	    return retval;
	
	return "";
    }
}


std::list<std::string> Guikachu::get_language_list (std::string category_name)
{
    typedef std::list<std::string> language_list_t;
    typedef std::map<std::string, language_list_t> category_table_t;
    static category_table_t category_table;
    
    if (category_name == "")
	category_name = "LC_ALL";
    
    category_table_t::const_iterator found = category_table.find (category_name);
    if (found != category_table.end ())
	return found->second;
    
    language_list_t languages;
    std::string category_value;
    
    category_value = guess_category_value (category_name);
    if (category_value == "")
	category_value = "C";
    
    bool c_locale_defined = false;
    
    gchar **locales = g_strsplit (category_value.c_str (), ":", 0);
	
    for (gchar **locale = locales; *locale != 0; ++locale)
    {
	std::string language = unalias_lang (*locale);
	if (language == "C")
	    c_locale_defined = true;
	
	languages.push_back (language);
    }
    
    g_strfreev (locales);

    if (!c_locale_defined)
	languages.push_back ("C");
	
    category_table[category_name] = languages;
    
    return languages;
}
