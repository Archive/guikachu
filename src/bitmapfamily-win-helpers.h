//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_BITMAPFAMILY_WIN_HELPERS_H
#define GUIKACHU_BITMAPFAMILY_WIN_HELPERS_H

#include "bitmap-res.h"
#include "bitmapfamily-res.h"

#include <gtkmm/treeview.h>
#include <gtkmm/liststore.h>

namespace Guikachu
{
    namespace GUI
    {
        namespace BitmapFamilyWindow_Helpers
        {
            class DepthList: public Gtk::TreeView
            {
                Resources::BitmapFamily *res;
                
                Gtk::TreeModelColumn<Glib::ustring>                 col_label;
                Gtk::TreeModelColumn<Resources::Bitmap::BitmapType> col_type;
                Glib::RefPtr<Gtk::ListStore>                        liststore;
                
            public:
                DepthList (Resources::BitmapFamily *res);
                sigc::signal1<void, Resources::Bitmap::BitmapType> type_changed;
                
            private:
                bool update_block;
                void update ();
                void selection_changed_cb ();
            };
        }
    }
}

#endif /* !GUIKACHU_BITMAPFAMILY_WIN_HELPERS_H */
