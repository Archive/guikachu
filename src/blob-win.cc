//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "blob-win.h"

#include <glib/gi18n.h>

#include "property-ops-resource.h"
#include "widgets/propertytable.h"
#include "widgets/entry.h"
#include "widgets/text.h"

using namespace Guikachu;

GUI::BlobWindow::BlobWindow (Guikachu::Resources::Blob *res_):
    res (res_),
    window (Gtk::WINDOW_TOPLEVEL)
{
    using ResourceOps::RenameOpFactory;
    using ResourceOps::PropChangeOpFactory;
    
    window.signal_delete_event ().connect (sigc::mem_fun (*this, &BlobWindow::delete_event_impl));
    window.property_allow_grow () = true;

    GUI::PropertyTable *proptable = new GUI::PropertyTable ();
    Gtk::Widget *control;

    /* Resource ID */
    control = new GUI::PropertyEditors::Entry (false, res->id, new RenameOpFactory (res));
    proptable->add (_("Resource _ID:"), *manage (control));

    /* Blob contents */
    control = new GUI::PropertyEditors::TextArea (
	res->contents,
	new PropChangeOpFactory<std::string> (
	    _("Change contents of %s"), res, res->contents, true));
    proptable->add (_("_Blob:"), *manage (control));
    
    window.add (*manage (proptable));
    
    res->changed.connect (sigc::mem_fun (*this, &BlobWindow::update));
    update ();
}

bool GUI::BlobWindow::delete_event_impl (GdkEventAny *e)
{
    window.hide ();
    return true;
}

void GUI::BlobWindow::show ()
{
    window.show_all ();
    window.raise ();
}

void GUI::BlobWindow::update ()
{
    window.set_title (Glib::ScopedPtr<char> (
	g_strdup_printf (_("Blob: %s"), res->id ().c_str ())).get ());
}
