//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_UI_GUI_H
#define GUIKACHU_UI_GUI_H

#include "ui.h"

#include <gtkmm/button.h>
#include <gtkmm/stock.h>
#include <gtkmm/filechooserdialog.h>
#include <libglademm/xml.h>

namespace Guikachu
{
    namespace UI
    {
	void show_about (Gtk::Window *parent = 0);

        Gtk::Button * create_stock_button (const Gtk::StockID  &stock_id,
                                           const Glib::ustring &title);

        Glib::RefPtr<Gnome::Glade::Xml> glade_gui (const Glib::ustring &root = Glib::ustring ());
    }
}

#endif /* !GUIKACHU_UI_GUI_H */
