//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "menu-win-stock.h"

#include <glib.h>

using namespace Guikachu;
using namespace Guikachu::GUI;

MenuWindow_Helpers::stock_menu_list_t MenuWindow_Helpers::get_stock_menus ()
{
    stock_menu_list_t stock_menu_list;

    stock_menu_list.push_back (stock_menu_def_t (STOCK_MENU_EDIT, "Edit"));
    stock_menu_list.push_back (stock_menu_def_t (STOCK_MENU_OPTIONS, "Options"));

    return stock_menu_list;
}

    Resources::Menu::Submenu MenuWindow_Helpers::create_stock_menu (StockMenu stock_id)
{
    Resources::Menu::Submenu submenu;
    
    switch (stock_id)
    {
    case STOCK_MENU_EDIT:
        submenu.label = "Edit";
        submenu.items.push_back (Resources::Menu::MenuItem ("UNDO",       "Undo",             'u'));
        submenu.items.push_back (Resources::Menu::MenuItem ("CUT",        "Cut",              'x'));
        submenu.items.push_back (Resources::Menu::MenuItem ("COPY",       "Copy",             'c'));
        submenu.items.push_back (Resources::Menu::MenuItem ("PASTE",      "Paste",            'p'));
        submenu.items.push_back (Resources::Menu::MenuItem ("SELECT_ALL", "Select All",       's'));
        submenu.items.push_back (Resources::Menu::MenuItem ());
        submenu.items.push_back (Resources::Menu::MenuItem ("KEYBOARD",   "Keyboard",         'k'));
        submenu.items.push_back (Resources::Menu::MenuItem ("GRAFFITI",   "Graffiti Help",    'g'));
        break;
    case STOCK_MENU_OPTIONS:
        submenu.label = "Options";
        submenu.items.push_back (Resources::Menu::MenuItem ("FONT",       "Font\\x85",        'f'));
        submenu.items.push_back (Resources::Menu::MenuItem ("PREF",       "Preferences\\x85", 'r'));
        submenu.items.push_back (Resources::Menu::MenuItem ("ABOUT",      "About"));
        break;
    default:
        g_warning ("Unknown stock menu: %d", stock_id);
        break;
    }

    return submenu;
}
