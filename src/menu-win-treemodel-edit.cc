//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "menu-win-treemodel.h"

#include "menu-res-ops.h"

#include <gtkmm/treepath.h>

using namespace Guikachu::GUI::MenuWindow_Helpers;
using namespace Guikachu::ResourceOps::MenuOps;

bool MenuTreeModel::can_move_row_up (const iterator &iter) const
{
    if (!iter)
        return false;

    submenu_idx_t submenu_idx;
    item_idx_t    item_idx;
    bool          is_submenu;
    read_iter (iter, submenu_idx, item_idx, is_submenu);
    
    if (is_submenu && submenu_idx == 0)
        return false;
    if (!is_submenu && item_idx == 0)
        return false;

    return true;
}

bool MenuTreeModel::can_move_row_down (const iterator &iter) const
{
    if (!iter)
        return false;

    submenu_idx_t submenu_idx;
    item_idx_t    item_idx;
    bool          is_submenu;
    read_iter (iter, submenu_idx, item_idx, is_submenu);
    
    if (is_submenu && submenu_idx == submenus.size () - 1)
        return false;
    if (!is_submenu && item_idx == submenus[submenu_idx].items.size () - 1)
        return false;
    
    return true;
}

void MenuTreeModel::remove_row (const iterator &iter)
{
    submenu_idx_t submenu_idx;
    item_idx_t    item_idx;
    bool          is_submenu;
    read_iter (iter, submenu_idx, item_idx, is_submenu);

    UndoOp *op = 0;
    
    if (is_submenu)
    {
        g_return_if_fail (submenu_idx < submenus.size ());
    
        submenus.erase (submenus.begin () + submenu_idx);
        op = new SubmenuRemoveOp (res, submenu_idx);

    } else {        
        g_return_if_fail (submenu_idx < submenus.size ());
        g_return_if_fail (item_idx < submenus[submenu_idx].items.size ());
        
        submenus[submenu_idx].items.erase (submenus[submenu_idx].items.begin () + item_idx);
        op = new MenuItemRemoveOp (res, submenu_idx, item_idx);
    }
    
    res->set_submenus (submenus);
    res->get_manager ()->get_undo_manager ().push (op);
}

void MenuTreeModel::move_row_up (const iterator &iter)
{
    if (!can_move_row_up (iter))
        return;
    
    submenu_idx_t submenu_idx;
    item_idx_t    item_idx;
    bool          is_submenu;
    read_iter (iter, submenu_idx, item_idx, is_submenu);

    if (is_submenu)
    {
        move_submenu (submenu_idx, submenu_idx - 1);
    } else {
        move_item (submenu_idx, item_idx, submenu_idx, item_idx - 1);
    }
}

void MenuTreeModel::move_row_down (const iterator &iter)
{
    if (!can_move_row_down (iter))
        return;
    
    submenu_idx_t submenu_idx;
    item_idx_t    item_idx;
    bool          is_submenu;
    read_iter (iter, submenu_idx, item_idx, is_submenu);
    
    if (is_submenu)
    {
        move_submenu (submenu_idx, submenu_idx + 1);
    } else {
        move_item (submenu_idx, item_idx, submenu_idx, item_idx + 1);
    }
}

Gtk::TreePath MenuTreeModel::insert_submenu (const iterator &iter, const submenu_t &submenu)
{
    submenu_idx_t submenu_idx = submenus.size ();
    item_idx_t    item_idx;
    bool          is_submenu;
    if (iter)
        read_iter (iter, submenu_idx, item_idx, is_submenu);
    
    submenus.insert (submenus.begin () + submenu_idx, submenu);

    res->set_submenus (submenus);
    res->get_manager ()->get_undo_manager ().push (new SubmenuCreateOp (res, submenu_idx));

    iterator new_submenu_iter;
    fill_iter (new_submenu_iter, submenu_idx);
    return get_path (new_submenu_iter);
}

Gtk::TreePath MenuTreeModel::insert_menuitem (const iterator &iter, const item_t &item)
{
    if (!iter_is_valid (iter))
        return Path ();
    
    submenu_idx_t submenu_idx;
    item_idx_t    item_idx;
    bool          is_submenu;
    read_iter (iter, submenu_idx, item_idx, is_submenu);

    submenu_t &submenu = submenus[submenu_idx];
    if (is_submenu)
    {
        submenu.items.push_back (item);
        item_idx = submenu.items.size () - 1;
    } else {
        submenu.items.insert (submenu.items.begin () + item_idx, item);
    }
    
    res->set_submenus (submenus);
    res->get_manager ()->get_undo_manager ().push (new MenuItemCreateOp (res, submenu_idx, item_idx));

    iterator new_item_iter;
    fill_iter (new_item_iter, submenu_idx, item_idx);
    return get_path (new_item_iter);
}

void MenuTreeModel::set_item_label (const iterator &iter, const std::string &new_label)
{
    submenu_idx_t submenu_idx;
    item_idx_t    item_idx;
    bool          is_submenu;
    read_iter (iter, submenu_idx, item_idx, is_submenu);

    UndoOp *op = 0;
    
    if (is_submenu)
    {
        g_return_if_fail (submenu_idx < submenus.size ());

        if (submenus[submenu_idx].label == new_label)
            return;
        
        submenus[submenu_idx].label = new_label;
	op = new SubmenuChangeOp (res, submenu_idx, new_label);
        
    } else {
        g_return_if_fail (submenu_idx < submenus.size ());
        g_return_if_fail (item_idx < submenus[submenu_idx].items.size ());

        if (submenus[submenu_idx].items[item_idx].label == new_label)
            return;
        
        submenus[submenu_idx].items[item_idx].label = new_label;
	op = new MenuItemLabelOp (res, submenu_idx, item_idx, new_label);
    }
    
    res->set_submenus (submenus);
    res->get_manager ()->get_undo_manager ().push (op);
}

void MenuTreeModel::set_item_id (const iterator &iter, const std::string &new_id)
{
    submenu_idx_t submenu_idx;
    item_idx_t    item_idx;
    bool          is_submenu;
    read_iter (iter, submenu_idx, item_idx, is_submenu);

    if (is_submenu)
        return;
    
    g_return_if_fail (submenu_idx < submenus.size ());
    g_return_if_fail (item_idx < submenus[submenu_idx].items.size ());
    
    if (submenus[submenu_idx].items[item_idx].id == new_id)
        return;
    
    submenus[submenu_idx].items[item_idx].id = new_id;
    
    UndoOp *op = new MenuItemRenameOp (res, submenu_idx, item_idx, new_id);
    res->set_submenus (submenus);
    res->get_manager ()->get_undo_manager ().push (op);
}

void MenuTreeModel::set_item_shortcut (const iterator &iter, char new_shortcut)
{
    submenu_idx_t submenu_idx;
    item_idx_t    item_idx;
    bool          is_submenu;
    read_iter (iter, submenu_idx, item_idx, is_submenu);

    if (is_submenu)
        return;
    
    g_return_if_fail (submenu_idx < submenus.size ());
    g_return_if_fail (item_idx < submenus[submenu_idx].items.size ());
    
    if (toupper (submenus[submenu_idx].items[item_idx].shortcut) == toupper (new_shortcut))
        return;
    
    submenus[submenu_idx].items[item_idx].shortcut = new_shortcut;
    
    UndoOp *op = new MenuItemShortcutOp (res, submenu_idx, item_idx, new_shortcut);
    res->set_submenus (submenus);
    res->get_manager ()->get_undo_manager ().push (op);
}
