//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_MENU_WIN_TREEMODEL_H
#define GUIKACHU_MENU_WIN_TREEMODEL_H

#include <gtkmm/treemodel.h>
#include <gtkmm/treedragsource.h>
#include <gtkmm/treedragdest.h>
#include <gtkmm/treepath.h>

#include "menu-res.h"

namespace Guikachu
{
    namespace GUI
    {
        namespace MenuWindow_Helpers
        {
            class MenuTreeModel: public Glib::Object,
                                 public Gtk::TreeModel,
                                 public Gtk::TreeDragSource,
                                 public Gtk::TreeDragDest
            {
                Resources::Menu *res;
                
                typedef Resources::Menu::MenuTree menutree_t;
                typedef Resources::Menu::Submenu  submenu_t;
                typedef Resources::Menu::MenuItem item_t;

                menutree_t submenus, submenus_old;
                
                typedef unsigned int submenu_idx_t;
                typedef unsigned int item_idx_t;
                
                int stamp;

                mutable bool drag_dragged;
                mutable Path drag_path;
                mutable bool drag_dropped;
                mutable Path drop_path;
                
                void fill_iter (iterator &iter, submenu_idx_t submenu_idx, item_idx_t item_idx) const;
                void fill_iter (iterator &iter, submenu_idx_t submenu_idx) const;
                void read_iter (const iterator &iter,
                                submenu_idx_t  &submenu_idx,
                                item_idx_t     &item_idx,
                                bool           &is_submenu) const;
                
            public:
                static Glib::RefPtr<MenuTreeModel> create (Resources::Menu *res);
                
            protected:
                MenuTreeModel (Resources::Menu *res);
                
                virtual Gtk::TreeModelFlags get_flags_vfunc() const;
                virtual int get_n_columns_vfunc() const;
                virtual GType get_column_type_vfunc(int index) const;
                
                virtual void get_value_vfunc (const iterator &iter, int column, Glib::ValueBase &value) const;
                
                virtual bool iter_is_valid (const iterator &iter) const;

                // Linear traversal
                virtual bool iter_next_vfunc (const iterator &iter, iterator &iter_next) const;
                virtual int  iter_n_root_children_vfunc () const;

                // Tree traversal
                virtual bool iter_has_child_vfunc  (const iterator &iter) const;
                virtual int  iter_n_children_vfunc (const iterator &iter) const;
                virtual bool iter_nth_child_vfunc  (const iterator &iter, int n, iterator &child) const;
                virtual bool iter_children_vfunc   (const iterator &iter, iterator &child) const;
                virtual bool iter_parent_vfunc     (const iterator &iter, iterator &parent) const;

                
                // Path <-> iter mapping
                virtual Path get_path_vfunc (const iterator &iter) const;
                virtual bool get_iter_vfunc (const Path &path, iterator &iter) const;

                // TreeDragSource interface
                virtual bool row_draggable_vfunc    (const Path &path) const;
                virtual bool drag_data_get_vfunc    (const Path &path, Gtk::SelectionData &selection_data) const;
                virtual bool drag_data_delete_vfunc (const Path &path);
                
                // TreeDragDest interface
                virtual bool drag_data_received_vfunc (const Path &dest, const Gtk::SelectionData &selection_data);
                virtual bool row_drop_possible_vfunc  (const Path &dest, const Gtk::SelectionData &selection_data) const;
            public:
                bool can_move_row_up   (const iterator &iter) const;
                bool can_move_row_down (const iterator &iter) const;
                
                void remove_row        (const iterator &iter);
                void move_row_up       (const iterator &iter);
                void move_row_down     (const iterator &iter);

                Path insert_submenu    (const iterator &iter, const submenu_t &submenu);
                Path insert_menuitem   (const iterator &iter, const item_t &item);
                
                void set_item_label    (const iterator &iter, const std::string &new_label);
                void set_item_id       (const iterator &iter, const std::string &new_id);
                void set_item_shortcut (const iterator &iter, char               new_shortcut);
                
            private:
                void move_submenu (submenu_idx_t old_submenu_idx, submenu_idx_t new_submenu_idx);
                void move_item    (submenu_idx_t old_submenu_idx, item_idx_t old_item_idx,
                                   submenu_idx_t new_submenu_idx, item_idx_t new_item_idx);
                
                void update ();
                void invalidate ();
            };
        }
    }
}

#endif /* !GUIKACHU_MENU_WIN_TREEMODEL_H */

