//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_BITMAP_WIN_H
#define GUIKACHU_BITMAP_WIN_H

#include "bitmap-res.h"
#include "resource-win.h"

#include <gtkmm/window.h>
#include <gtkmm/image.h>
#include <gtkmm/button.h>

namespace Guikachu
{
    namespace GUI
    {
	class BitmapWindow: public ResourceWindow,
			    public sigc::trackable
	{
	    Resources::Bitmap *res;
	    
	    Gtk::Window  window;	    
            Gtk::Image   preview_pixmap;
            Gtk::Button *export_button;

	public:
	    BitmapWindow (Resources::Bitmap *res);
	    void show ();
            
	private:
	    bool delete_event_impl (GdkEventAny *e);
	    void update ();
            void update_image ();

            void load_image_cb ();
	    void export_image_cb ();
	};
    }
}

#endif /* !GUIKACHU_BITMAP_WIN_H */
