//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "menu-res.h"

using namespace Guikachu;
using namespace Guikachu::Resources;

Menu::MenuItem::MenuItem () :
    separator (true),
    shortcut (0)
{
}

Menu::MenuItem::MenuItem (const std::string &id_, const std::string &label_, char shortcut_) :
    separator (false),
    id (convert_to_ascii (id_)),
    label (convert_to_ascii (label_)),
    shortcut (shortcut_)
{
}

Menu::Submenu::Submenu (const std::string &label_) :
    label (convert_to_ascii (label_))
{
}

Menu::Menu (ResourceManager   *manager,
	    const std::string &id,
	    serial_t           serial):
    Resource (manager, id, serial),
    IDManager (manager)
{
}

Menu::~Menu ()
{
}

void Menu::register_menu (const MenuTree &new_submenus)
{
    submenus = new_submenus;
    
    for (MenuTree::iterator i = submenus.begin (); i != submenus.end (); ++i)
    {
	for (MenuItems::iterator j = i->items.begin (); j != i->items.end (); ++j)
	{
	    if (!j->separator)
	    {
		j->id = convert_to_ascii (j->id);
		j->label = convert_to_ascii (j->label);
		
		if (!register_id (j->id))
		{
		    j->id = create_id (j->id + "_");
		    register_id (j->id);
		}
	    }
	}
    }
}

void Menu::deregister_menu ()
{
    for (MenuTree::const_iterator i = submenus.begin (); i != submenus.end (); ++i)
    {
	for (MenuItems::const_iterator j = i->items.begin ();
	     j != i->items.end (); ++j)
	{
	    if (!j->separator)
		unregister_id (j->id);
	}
    }
}

void Menu::set_submenus (const Menu::MenuTree& new_menu)
{
    deregister_menu ();
    register_menu (new_menu);
    
    changed ();
}
