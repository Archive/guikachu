//$Id: cellrenderer-icontext.cc,v 1.8 2006/10/06 22:35:40 cactus Exp $ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "cellrenderer-livetext.h"
#include <gtkmm/entry.h>

using namespace Guikachu::GUI;

CellRendererLiveText::CellRendererLiveText ()
{
}

Gtk::CellEditable * CellRendererLiveText::start_editing_vfunc (GdkEvent               *event,
                                                               Gtk::Widget            &widget,
                                                               const Glib::ustring    &path,
                                                               const Gdk::Rectangle   &background_area,
                                                               const Gdk::Rectangle   &cell_area,
                                                               Gtk::CellRendererState  flags)
{
    g_assert (!conn_cancel && !conn_done);
    
    Gtk::CellEditable *editable = Gtk::CellRendererText::start_editing_vfunc (event, widget, path, background_area, cell_area, flags);

    conn_cancel = signal_editing_canceled ().connect (sigc::bind (sigc::mem_fun (this, &CellRendererLiveText::editing_canceled_cb), editable, path));
    conn_done = signal_edited ().connect (sigc::hide (sigc::hide (sigc::mem_fun (this, &CellRendererLiveText::editing_done_cb))));
    
    return editable;
}

void CellRendererLiveText::editing_canceled_cb (Gtk::CellEditable *editable, const Glib::ustring &path)
{
    conn_cancel.disconnect ();
    conn_done.disconnect ();

    Gtk::Entry *entry = dynamic_cast<Gtk::Entry*>(editable);

    if (entry)
        edited (path, entry->get_text ());

}

void CellRendererLiveText::editing_done_cb ()
{
    conn_cancel.disconnect ();
    conn_done.disconnect ();
}
