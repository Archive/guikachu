//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "target.h"

#include <glib/gi18n.h>

#include "storage.h"
#include <glib.h> // For g_warning

#include <sys/types.h>
#include <dirent.h>

using namespace Guikachu;

// Cache of available stock targets
namespace
{
    struct StockTargetData
    {
	Target::stock_desc_t description;
	int  screen_width;
	int  screen_height;
        bool screen_color;

	StockTargetData (const Target::stock_desc_t &description_,
			 int                         screen_width_,
			 int                         screen_height_,
                         bool                        screen_color_):
	    description (description_),
	    screen_width  (screen_width_),
	    screen_height (screen_height_),
            screen_color  (screen_color_)
	    {}
    };

    typedef std::map<Target::stock_id_t, StockTargetData> StockTargetMap;
    StockTargetMap stock_target_map;
    
    void static_stock_map_init ();
    const StockTargetData& get_stock_data (const Target::stock_id_t &stock_id);
}


Target::Target ():
    screen_width  (changed_internal),
    screen_height (changed_internal),
    screen_color  (changed_internal)
{
    load_stock (get_default_stock_id ());

    changed_internal.connect (sigc::mem_fun (*this, &Target::changed_cb));
}

void Target::changed_cb ()
{
    // Invalidate stock ID
    stock_id = "";

    changed.emit ();
}

std::string Target::get_default_stock_id ()
{
    return "palm-color";
}

void Target::load_stock (const Target::stock_id_t &stock_id_)
{
    g_return_if_fail (stock_id_ != "");
    
    if (stock_id_ == stock_id)
	return;
    
    const StockTargetData &stock_data = get_stock_data (stock_id_);

    screen_width  = stock_data.screen_width;
    screen_height = stock_data.screen_height;
    screen_color  = stock_data.screen_color;

    stock_id = stock_id_;
    changed ();
    stock_id = stock_id_;
}

Target::stock_id_t Target::get_stock_id () const
{
    return stock_id;
}

Target::stock_list_t Target::stock_targets ()
{    
    Target::stock_list_t ret_val;

    static_stock_map_init ();
    for (StockTargetMap::const_iterator i = stock_target_map.begin ();
         i != stock_target_map.end (); ++i)
        ret_val[i->first] = i->second.description;

    return ret_val;
}

namespace {
void static_stock_map_init ()
{
    static bool done = false;
    if (done)
        return;

    stock_target_map.insert (std::make_pair ("palm",
        StockTargetData (_("Palm and compatible (Greyscale)"), 160, 160, false)));
    stock_target_map.insert (std::make_pair ("palm-color",
        StockTargetData (_("Palm and compatible (Color)"), 160, 160, true)));
    stock_target_map.insert (std::make_pair ("ebook-l",
        StockTargetData (_("eBookMan (portrait orientation)"), 240, 200, false)));
    stock_target_map.insert (std::make_pair ("ebook-p",
        StockTargetData (_("eBookMan (landscape orientation)"), 200, 240, false)));

    done = true;
}
    
const StockTargetData& get_stock_data (const Target::stock_id_t &stock_id)
{
    static_stock_map_init ();

    StockTargetMap::const_iterator i = stock_target_map.find (stock_id);    
    g_return_val_if_fail (i != stock_target_map.end (), get_stock_data (Target::get_default_stock_id ()));
    
    return i->second;
}

} // Anonymous namespace
