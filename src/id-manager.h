//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_ID_MANAGER_H
#define GUIKACHU_ID_MANAGER_H

#include <set>
#include <string>

namespace Guikachu
{
    class IDManager
    {
    public:
        class NoCase
        {
        public:
            int  compare     (const std::string &x, const std::string &y) const;
            bool operator () (const std::string &x, const std::string &y) const;
        };
        
    private:
        IDManager *parent_manager;
        
	typedef std::set<std::string, NoCase> id_set_t;        
	id_set_t ids;
        
        bool has_id (const std::string &id) const;
        
    public:
        IDManager (IDManager *parent = 0);
        
	bool register_id   (const std::string &id);
	void unregister_id (const std::string &id);
        bool change_id     (const std::string &old_id, const std::string &new_id);

	std::string create_id   (const std::string &prefix);
	std::string validate_id (std::string id) const;
    };
}

#endif /* !GUIKACHU_ID_MANAGER_H */
