//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <glib/gi18n.h>

#include "mainwin.h"
#include "ui.h"
#include "ui-gui.h"

#include "resource-manager.h"
#include "session.h"

#include <gtkmm/main.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/stock.h>
#include <gtkmm/image.h>
#include <gtkmm/button.h>
#include <gtkmm/box.h>
#include "widgets/filechooserdialog.h"

#include "io/rcp-saver.h"

#include <sigc++/adaptors/hide.h>
#include <sigc++/adaptors/bind.h>
#include <sigc++/adaptors/bind_return.h>

using namespace Guikachu;

namespace
{
    Glib::ustring time_ago_string (time_t ago)
    {
        Glib::ustring  ret_val;
        gchar         *buf = 0;
        
        int minutes = ago / 60;
        int hours = minutes / 60;
        int days = hours / 24;
        
        // Although we discriminate by pluralness ourselves, ngettext() is
        // still used, to help languages where there are multiple plurals
        // based on the actual number
        if (days > 1)
            buf = g_strdup_printf (
                ngettext ("If you don't save, your work from the last %d day will be lost.",
                          "If you don't save, your work from the last %d days will be lost.",
                          days), days);
        else if (hours > 1)
            buf = g_strdup_printf (
                ngettext ("If you don't save, your work from the last %d hour will be lost.",
                          "If you don't save, your work from the last %d hours will be lost.",
                          hours), hours);
        else if (minutes > 1)
            buf = g_strdup_printf (
                ngettext ("If you don't save, your work from the last %d minute will be lost.",
                          "If you don't save, your work from the last %d minutes will be lost.",
                          minutes), minutes);
        else
            buf = g_strdup_printf (_("If you don't save, your work from "
                                     "the last minute will be lost."));
        
        ret_val = buf;
        g_free (buf);
        return ret_val;
    }
} // anonymous namespace
    
bool GUI::MainWin::check_save ()
{
    if (!manager->is_dirty ())
    {
	if (in_exit)
	    Gtk::Main::quit ();
	return true;
    }
    
    int   response;
    int   time_since_save = time (0) - manager->get_modification_time ();
    char *filename_message;

    if (uri != "")
	filename_message = g_strdup_printf (
	    _("Save changes to document \"%s\"?"),
	    UI::visible_filename (uri).c_str ());
    else
	filename_message = g_strdup (
	    _("Save changes to the current document?"));

    Glib::ustring message = "<big><b>" + Glib::ustring (filename_message) + "</b></big>";
    message += "\n\n" + time_ago_string (time_since_save);

    g_free (filename_message);

    Gtk::MessageDialog *dialog = new Gtk::MessageDialog (
        *this, message, true, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_NONE, true);

    static const int discard_btn_num = 0;
    static const int cancel_btn_num = 1;
    static const int save_btn_num = 2;

    Gtk::Button *discard_btn = UI::create_stock_button (Gtk::Stock::DELETE,
                                                        _("_Don't Save"));
    discard_btn->show ();
    
    dialog->add_action_widget (*manage (discard_btn), discard_btn_num);
    dialog->add_button (Gtk::Stock::CANCEL, cancel_btn_num);
    dialog->add_button (Gtk::Stock::SAVE, save_btn_num);
    dialog->set_default_response (save_btn_num);

    dialog->set_position (Gtk::WIN_POS_CENTER_ON_PARENT);
    response = dialog->run ();
    dialog->hide ();

    if (response == cancel_btn_num || response == Gtk::RESPONSE_NONE) // Cancel
	return false;

    if (response == discard_btn_num)
	if (in_exit)
	    Gtk::Main::quit ();
    
    if (response == save_btn_num)
    {
	save_cb ();
	//if (manager->is_dirty ())
	    return check_save ();
    }
	    
    return true;
}

void GUI::MainWin::new_cb ()
{
    // Check if file has changed
    if (!check_save ())
	return;

    Guikachu::Main::instance ()->new_doc ();
}

void GUI::MainWin::set_uri (const Glib::ustring& uri_)
{
    uri = uri_;
    update_title ();
}

void GUI::MainWin::save_cb ()
{
    if (uri == "")
	save_as_cb ();
    else
	Guikachu::Main::instance ()->save_doc (uri);
}

void GUI::MainWin::save_as_cb ()
{
    FileChooserDialog file_chooser (*this, _("Save Guikachu file"), Gtk::FILE_CHOOSER_ACTION_SAVE, "guikachu");

    Gtk::FileFilter filter_guikachu;
    filter_guikachu.set_name (_("Guikachu documents"));
#ifdef GUIKACHU_HAVE_GNOMEVFS
    filter_guikachu.add_mime_type ("application/x-guikachu");
#else
    filter_guikachu.add_pattern ("*.guikachu");
#endif
    file_chooser.add_filter (filter_guikachu);
    
    Gtk::FileFilter filter_all;
    filter_all.set_name (_("All files"));
    filter_all.add_pattern ("*");
    file_chooser.add_filter (filter_all);

    if (file_chooser.run () == Gtk::RESPONSE_ACCEPT)
        Guikachu::Main::instance ()->save_doc (file_chooser.get_uri ());
}
    
void GUI::MainWin::load_cb ()
{
    FileChooserDialog file_chooser (*this, _("Select a Guikachu file to open"), Gtk::FILE_CHOOSER_ACTION_OPEN);
    
    Gtk::FileFilter filter_guikachu;
    filter_guikachu.set_name (_("Guikachu documents"));
#ifdef GUIKACHU_HAVE_GNOMEVFS
    filter_guikachu.add_mime_type ("application/x-guikachu");
#else
    filter_guikachu.add_pattern ("*.guikachu");
#endif
    file_chooser.add_filter (filter_guikachu);
    
    Gtk::FileFilter filter_rcp;
    filter_rcp.set_name (_("PilRC documents"));
#ifdef GUIKACHU_HAVE_GNOMEVFS
    filter_rcp.add_mime_type ("text/x-rcp");
#else
    filter_rcp.add_pattern ("*.rcp");
#endif
    file_chooser.add_filter (filter_rcp);

    file_chooser.set_default_response (Gtk::RESPONSE_ACCEPT);
    if (file_chooser.run () == Gtk::RESPONSE_ACCEPT)
        open (file_chooser.get_uri ());
}
    
void GUI::MainWin::export_cb ()
{
    FileChooserDialog file_chooser (*this, _("Export to RCP"), Gtk::FILE_CHOOSER_ACTION_SAVE);

    Gtk::FileFilter filter_rcp;
    filter_rcp.set_name (_("PilRC documents"));
#ifdef GUIKACHU_HAVE_GNOMEVFS
    filter_rcp.add_mime_type ("text/x-rcp");
#else
    filter_rcp.add_pattern ("*.rcp");
#endif
    file_chooser.add_filter (filter_rcp);

    Gtk::FileFilter filter_all;
    filter_all.set_name (_("All files"));
    filter_all.add_pattern ("*");
    file_chooser.add_filter (filter_all);
    
    file_chooser.set_default_response (Gtk::RESPONSE_ACCEPT);    
    if (file_chooser.run () != Gtk::RESPONSE_ACCEPT)
        return;
    
    IO::Saver *rcp_saver = new IO::RCPSaver;
    try {
        rcp_saver->save (manager, file_chooser.get_uri ());
    } catch (Glib::Exception &e) {
	char *message = g_strdup_printf (
	    _("Error exporting to `%s':\n%s"),
	    g_basename (UI::visible_filename (uri).c_str ()), e.what ().c_str ());

	UI::show_error (message);
	g_free (message);        
    }    
    delete rcp_saver;
}

void GUI::MainWin::open (Glib::ustring uri)
{
    // Prompt the user about saving the current doc if it's changed
    if (!check_save ())
	return;

    Guikachu::Main::instance ()->load_doc (uri);
}

void GUI::MainWin::exit_cb ()
{
    in_exit = true;

    if (!check_save ())
	in_exit = false;
}

bool GUI::MainWin::on_delete_event (GdkEventAny *e)
{
    exit_cb ();

    return true;
}
