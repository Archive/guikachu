//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <glib/gi18n.h>

#include "menu-res-ops.h"
#include <glib.h> // for g_assert

using namespace Guikachu;
using namespace Guikachu::ResourceOps::MenuOps;

MenuHolder::MenuHolder (Resources::Menu   *menu) :
    manager (menu->get_manager ()),
    menu_serial (menu->get_serial ())
{
}

Resources::Menu * MenuHolder::get_menu () const
{
    Resource *res = manager->get_resource (menu_serial);
    g_assert (res);

    return static_cast<Resources::Menu*> (res);
}




SubmenuCreateOp::SubmenuCreateOp (Resources::Menu *menu,
				  unsigned int     index_) :
    MenuHolder (menu),
    index (index_)
{
    const std::string &menu_id = menu->id;
    
    char *label_str = g_strdup_printf (_("Create submenu of %s"), menu_id.c_str ());
    op_label = label_str;
    g_free (label_str);
}

void SubmenuCreateOp::undo ()
{
    Resources::Menu           *res = get_menu ();
    Resources::Menu::MenuTree  submenus = res->get_submenus ();

    submenu = submenus[index];
    submenus.erase (submenus.begin () + index);

    res->set_submenus (submenus);
}

void SubmenuCreateOp::redo ()
{
    Resources::Menu           *res = get_menu ();
    Resources::Menu::MenuTree  submenus = res->get_submenus ();

    submenus.insert (submenus.begin () + index, submenu);
    
    res->set_submenus (submenus);
    
}




SubmenuRemoveOp::SubmenuRemoveOp (Resources::Menu *menu,
				  unsigned int     index_) :
    MenuHolder (menu),
    index (index_),
    submenu (menu->get_submenus ()[index])
{
    const std::string &menu_id = menu->id;
    const std::string &submenu_id = submenu.label;
    
    char *label_str = g_strdup_printf (_("Remove \"%s\" from %s"), submenu_id.c_str (), menu_id.c_str ());
    op_label = label_str;
    g_free (label_str);
}

void SubmenuRemoveOp::undo ()
{
    Resources::Menu           *res = get_menu ();
    Resources::Menu::MenuTree  submenus = res->get_submenus ();

    submenus.insert (submenus.begin () + index, submenu);

    res->set_submenus (submenus);    
}

void SubmenuRemoveOp::redo ()
{
    Resources::Menu           *res = get_menu ();
    Resources::Menu::MenuTree  submenus = res->get_submenus ();

    submenus.erase (submenus.begin () + index);

    res->set_submenus (submenus);
}



SubmenuChangeOp::SubmenuChangeOp (Resources::Menu   *menu,
				  unsigned int       index_,
				  const std::string &new_label_) :
    MenuHolder (menu),
    index (index_),
    old_label (menu->get_submenus ()[index].label),
    new_label (new_label_)
{
    const std::string &menu_id = menu->id;
    
    char *label_str = g_strdup_printf (_("Rename \"%s\" in %s"), old_label.c_str (), menu_id.c_str ());
    op_label = label_str;
    g_free (label_str);
}

void SubmenuChangeOp::undo ()
{
    Resources::Menu           *res = get_menu ();
    Resources::Menu::MenuTree  submenus = res->get_submenus ();

    submenus[index].label = old_label;

    res->set_submenus (submenus);
}

void SubmenuChangeOp::redo ()
{
    Resources::Menu           *res = get_menu ();
    Resources::Menu::MenuTree  submenus = res->get_submenus ();

    submenus[index].label = new_label;

    res->set_submenus (submenus);
}

UndoOp * SubmenuChangeOp::combine (UndoOp *other_op) const
{
    SubmenuChangeOp *op = dynamic_cast<SubmenuChangeOp*> (other_op);
    if (!op)
	return 0;

    if (op->menu_serial != menu_serial || op->index != index)
	return 0;

    Resources::Menu *menu = get_menu ();
    SubmenuChangeOp *new_op = new SubmenuChangeOp (menu, index, op->new_label);
    new_op->old_label = old_label;

    char *label_str = g_strdup_printf (_("Rename \"%s\" in %s"),
				       old_label.c_str (), menu->id ().c_str ());
    new_op->op_label = label_str;
    g_free (label_str);

    return new_op;
}




SubmenuMoveOp::SubmenuMoveOp (Resources::Menu *menu,
			      submenu_idx_t    old_index,
			      submenu_idx_t    new_index) :
    MenuHolder (menu),
    old_submenu_idx (old_index),
    new_submenu_idx (new_index)
{
    Resources::Menu::MenuTree submenus = menu->get_submenus ();
    const std::string  submenu_label = submenus[old_index].label;
    const std::string &menu_id = menu->id;

    char *label_str = g_strdup_printf (_("Move \"%s\" in %s"),
				       submenu_label.c_str (), menu_id.c_str ());
    op_label = label_str;
    g_free (label_str);
}

void SubmenuMoveOp::undo ()
{
    Resources::Menu *res = get_menu ();
    Resources::Menu::MenuTree submenus = res->get_submenus ();
    
    Resources::Menu::Submenu submenu = submenus[new_submenu_idx];
    submenus.erase  (submenus.begin () + new_submenu_idx);
    submenus.insert (submenus.begin () + old_submenu_idx, submenu);

    res->set_submenus (submenus);
}

void SubmenuMoveOp::redo ()
{
    Resources::Menu *res = get_menu ();
    Resources::Menu::MenuTree submenus = res->get_submenus ();

    Resources::Menu::Submenu submenu = submenus[old_submenu_idx];
    submenus.erase  (submenus.begin () + old_submenu_idx);
    submenus.insert (submenus.begin () + new_submenu_idx, submenu);
    
    res->set_submenus (submenus);
}

UndoOp * SubmenuMoveOp::combine (UndoOp *other_op) const
{
    // I'm not sure combining SubmenuMoves is a good idea.
#if 0
    SubmenuMoveOp *op = dynamic_cast<SubmenuMoveOp*> (other_op);
    if (!op)
	return 0;

    if (op->menu_serial != menu_serial)
	return 0;

    if (op->old_submenu_idx != new_submenu_idx)
        return 0;

    SubmenuMoveOp *new_op = new SubmenuMoveOp (get_menu (), old_submenu_idx, op->new_submenu_idx);
    new_op->op_label = op_label;
    
    return new_op;
#endif
    
    return 0;
}




MenuItemCreateOp::MenuItemCreateOp (Resources::Menu *menu,
				    unsigned int     submenu_index_,
				    unsigned int     index_) :
    MenuHolder (menu),
    submenu_index (submenu_index_),
    index (index_)
{
    item_data = menu->get_submenus ()[submenu_index].items[index];

    const std::string &menu_id = menu->id;
    const std::string  menuitem_id = item_data.separator ? _("separator") : item_data.id;

    char *label_str = g_strdup_printf (_("Create %s in %s"), menuitem_id.c_str (), menu_id.c_str ());
    op_label = label_str;
    g_free (label_str);
}

void MenuItemCreateOp::undo ()
{
    Resources::Menu *res = get_menu ();
    Resources::Menu::MenuTree submenus = res->get_submenus ();

    std::vector<Resources::Menu::MenuItem> &items = submenus[submenu_index].items;
    items.erase (items.begin () + index);

    res->set_submenus (submenus);
}
    
void MenuItemCreateOp::redo ()
{
    Resources::Menu *res = get_menu ();
    Resources::Menu::MenuTree submenus = res->get_submenus ();

    std::vector<Resources::Menu::MenuItem> &items = submenus[submenu_index].items;
    items.insert (items.begin () + index, item_data);

    res->set_submenus (submenus);
}
    
MenuItemRemoveOp::MenuItemRemoveOp (Resources::Menu *menu,
				    unsigned int     submenu_index_,
				    unsigned int     index_) :
    MenuHolder (menu),
    submenu_index (submenu_index_),
    index (index_)
{
    item_data = menu->get_submenus ()[submenu_index].items[index];

    const std::string &menu_id = menu->id;
    const std::string  menuitem_id = item_data.separator ? _("separator") : item_data.id;

    char *label_str = g_strdup_printf (_("Remove %s from %s"), menuitem_id.c_str (), menu_id.c_str ());
    op_label = label_str;
    g_free (label_str);
}




void MenuItemRemoveOp::undo ()
{
    Resources::Menu *res = get_menu ();
    Resources::Menu::MenuTree submenus = res->get_submenus ();

    std::vector<Resources::Menu::MenuItem> &items = submenus[submenu_index].items;
    items.insert (items.begin () + index, item_data);

    res->set_submenus (submenus);
}
    
void MenuItemRemoveOp::redo ()
{
    Resources::Menu *res = get_menu ();
    Resources::Menu::MenuTree submenus = res->get_submenus ();

    std::vector<Resources::Menu::MenuItem> &items = submenus[submenu_index].items;
    items.erase (items.begin () + index);

    res->set_submenus (submenus);
}
    



MenuItemRenameOp::MenuItemRenameOp (Resources::Menu   *menu,
				    unsigned int       submenu_index_,
				    unsigned int       index_,
				    const std::string &new_id_) :
    MenuHolder (menu),
    submenu_index (submenu_index_),
    index (index_),
    old_id (menu->get_submenus ()[submenu_index].items[index].id),
    new_id (new_id_)
{
    char *label_str = g_strdup_printf (_("Rename %s to %s"), old_id.c_str (), new_id.c_str ());
    op_label = label_str;
    g_free (label_str);
}

void MenuItemRenameOp::undo ()
{
    Resources::Menu *res = get_menu ();
    Resources::Menu::MenuTree submenus = res->get_submenus ();

    submenus[submenu_index].items[index].id = old_id;

    res->set_submenus (submenus);
}

void MenuItemRenameOp::redo ()
{
    Resources::Menu *res = get_menu ();
    Resources::Menu::MenuTree submenus = res->get_submenus ();

    submenus[submenu_index].items[index].id = new_id;

    res->set_submenus (submenus);
}




MenuItemLabelOp::MenuItemLabelOp (Resources::Menu   *menu,
				  unsigned int       submenu_index_,
				  unsigned int       index_,
				  const std::string &new_label_) :
    MenuHolder (menu),
    submenu_index (submenu_index_),
    index (index_),
    old_label (menu->get_submenus ()[submenu_index].items[index].label),
    new_label (new_label_)
{
    std::string id = menu->get_submenus ()[submenu_index].items[index].id;
    
    char *label_str = g_strdup_printf (_("Change label of %s"), id.c_str ());
    op_label = label_str;
    g_free (label_str);
}

void MenuItemLabelOp::undo ()
{
    Resources::Menu *res = get_menu ();
    Resources::Menu::MenuTree submenus = res->get_submenus ();

    submenus[submenu_index].items[index].label = old_label;

    res->set_submenus (submenus);
}

void MenuItemLabelOp::redo ()
{
    Resources::Menu *res = get_menu ();
    Resources::Menu::MenuTree submenus = res->get_submenus ();

    submenus[submenu_index].items[index].label = new_label;

    res->set_submenus (submenus);
}

UndoOp * MenuItemLabelOp::combine (UndoOp *other_op) const
{
    MenuItemLabelOp *op = dynamic_cast<MenuItemLabelOp*> (other_op);
    if (!op)
	return 0;

    if (op->menu_serial != menu_serial ||
	op->submenu_index != submenu_index || op->index != index)
	return 0;

    MenuItemLabelOp *new_op = new MenuItemLabelOp (get_menu (), submenu_index, index, op->new_label);
    new_op->old_label = old_label;
    
    return new_op;
}




MenuItemShortcutOp::MenuItemShortcutOp (Resources::Menu *menu,
					unsigned int     submenu_index_,
					unsigned int     index_,
					char             new_shortcut_) :
    MenuHolder (menu),
    submenu_index (submenu_index_),
    index (index_),
    old_shortcut (menu->get_submenus ()[submenu_index].items[index].shortcut),
    new_shortcut (new_shortcut_)
{
    std::string id = menu->get_submenus ()[submenu_index].items[index].id;
    
    char *label_str = g_strdup_printf (_("Change shortcut of %s"), id.c_str ());
    op_label = label_str;
    g_free (label_str);
}

void MenuItemShortcutOp::undo ()
{
    Resources::Menu *res = get_menu ();
    Resources::Menu::MenuTree submenus = res->get_submenus ();

    submenus[submenu_index].items[index].shortcut = old_shortcut;

    res->set_submenus (submenus);
}

void MenuItemShortcutOp::redo ()
{
    Resources::Menu *res = get_menu ();
    Resources::Menu::MenuTree submenus = res->get_submenus ();

    submenus[submenu_index].items[index].shortcut = new_shortcut;

    res->set_submenus (submenus);
}

UndoOp * MenuItemShortcutOp::combine (UndoOp *other_op) const
{
    MenuItemShortcutOp *op = dynamic_cast<MenuItemShortcutOp*> (other_op);
    if (!op)
	return 0;

    if (op->menu_serial != menu_serial ||
	op->submenu_index != submenu_index || op->index != index)
	return 0;

    // Combine shortcut changes only if it's an erase-type sequence
    // (since otherwise every shortcut change would result in two undo ops)
    if (op->old_shortcut != 0)
	return 0;

    MenuItemShortcutOp *new_op = new MenuItemShortcutOp (
	get_menu (), submenu_index, index, op->new_shortcut);
    new_op->old_shortcut = old_shortcut;
    
    return new_op;
}




MenuItemMoveOp::MenuItemMoveOp (Resources::Menu *menu,
				submenu_idx_t    old_submenu_idx_,
				item_idx_t       old_item_idx_,
				submenu_idx_t    new_submenu_idx_,
				item_idx_t       new_item_idx_) :
    MenuHolder (menu),
    old_submenu_idx (old_submenu_idx_),
    old_item_idx    (old_item_idx_),
    new_submenu_idx (new_submenu_idx_),
    new_item_idx    (new_item_idx_)
{
    Resources::Menu::MenuTree submenus = menu->get_submenus ();
    const std::string  item_id = submenus[old_submenu_idx].items[old_item_idx].id;
    const std::string &menu_id = menu->id;

    char *label_str = g_strdup_printf (_("Move %s in %s"), item_id.c_str (), menu_id.c_str ());
    op_label = label_str;
    g_free (label_str);
}

void MenuItemMoveOp::undo ()
{
    Resources::Menu *res = get_menu ();
    Resources::Menu::MenuTree submenus = res->get_submenus ();

    Resources::Menu::MenuItem item = submenus[new_submenu_idx].items[new_item_idx];    
    submenus[new_submenu_idx].items.erase  (submenus[new_submenu_idx].items.begin () + new_item_idx);
    submenus[old_submenu_idx].items.insert (submenus[old_submenu_idx].items.begin () + old_item_idx, item);

    res->set_submenus (submenus);
}

void MenuItemMoveOp::redo ()
{
    Resources::Menu *res = get_menu ();
    Resources::Menu::MenuTree submenus = res->get_submenus ();

    Resources::Menu::MenuItem item = submenus[old_submenu_idx].items[old_item_idx];    
    submenus[old_submenu_idx].items.erase  (submenus[old_submenu_idx].items.begin () + old_item_idx);
    submenus[new_submenu_idx].items.insert (submenus[new_submenu_idx].items.begin () + new_item_idx, item);

    res->set_submenus (submenus);
}

UndoOp * MenuItemMoveOp::combine (UndoOp *other_op) const
{
    MenuItemMoveOp *op = dynamic_cast<MenuItemMoveOp*> (other_op);
    if (!op)
	return 0;

    if (op->menu_serial != menu_serial)
	return 0;

    if ((op->old_submenu_idx != new_submenu_idx) || (op->old_item_idx != new_item_idx))
        return 0;
    
    MenuItemMoveOp *new_op = new MenuItemMoveOp (get_menu (), old_submenu_idx, old_item_idx,
                                                 op->new_submenu_idx, op->new_item_idx);
    new_op->op_label = op_label;

    return new_op;
}
