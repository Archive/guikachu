//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_RESOURCE_FACTORY_WIN_H
#define GUIKACHU_RESOURCE_FACTORY_WIN_H

#include "resource-visitor.h"

#include "resource.h"
#include "resource-win.h"

#include "dialog-win.h"
#include "form-win.h"
#include "menu-win.h"
#include "string-win.h"
#include "stringlist-win.h"
#include "blob-win.h"
#include "bitmap-win.h"
#include "bitmapfamily-win.h"

namespace Guikachu
{
    namespace GUI
    {
	class ResourceWindowFactoryVisitor: public ResourceVisitor
	{
	    ResourceWindow *result;
	    
	public:
	    ResourceWindowFactoryVisitor (Resource *resource) :
		result (0) {
		resource->apply_visitor (*this);
	    }
	    
	    ResourceWindow * get_result () const {
		return result;
	    }
	    
#define GUIKACHU_RESOURCE_WINDOW_VISIT(Type)			\
            void visit_resource (Resources::Type *resource)	\
            {							\
                result = new Type##Window (resource);		\
            }

	    GUIKACHU_RESOURCE_WINDOW_VISIT(Dialog);
	    GUIKACHU_RESOURCE_WINDOW_VISIT(Form);
	    GUIKACHU_RESOURCE_WINDOW_VISIT(Menu);
	    GUIKACHU_RESOURCE_WINDOW_VISIT(String);
	    GUIKACHU_RESOURCE_WINDOW_VISIT(StringList);
	    GUIKACHU_RESOURCE_WINDOW_VISIT(Blob);
	    GUIKACHU_RESOURCE_WINDOW_VISIT(Bitmap);
	    GUIKACHU_RESOURCE_WINDOW_VISIT(BitmapFamily);
	};
	
    } // namespace GUI
} // namespace Guikachu

#endif /* !GUIKACHU_RESOURCE_FACTORY_WIN_H */
