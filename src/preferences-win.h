//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_PREFERENCES_WIN_H
#define GUIKACHU_PREFERENCES_WIN_H

#include <gtkmm/dialog.h>
#include <gtkmm/colorbutton.h>
#include <gtkmm/spinbutton.h>

namespace Guikachu
{
    namespace GUI
    {
	class PreferencesWin: public sigc::trackable
	{
	    Gtk::Dialog      *prop_box;
	    Gtk::ColorButton *color_fg, *color_disabled, *color_bg, *color_selection;
	    Gtk::SpinButton  *spin_zoom;
	    Gtk::Button      *apply_btn;
	    
	public:
	    PreferencesWin ();

	    void run ();
	    void update ();
	    void set_modified (bool modified);
	    void apply_cb ();

	    void color_changed_cb ();
	};
    }
}

#endif /* !GUIKACHU_PREFERENCES_WIN_H */
