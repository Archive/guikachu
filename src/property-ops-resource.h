//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_PROPERTY_OPS_RESOURCE_H
#define GUIKACHU_PROPERTY_OPS_RESOURCE_H

#include <glib/gi18n.h>

#include "property-ops.h"

#include "resource.h"

/* Since current C++ compilers require all template class methods to
 * be defined at their declarations, this header is messy and reveals
 * lots of implementation detail.
 */

namespace Guikachu
{
    namespace ResourceOps
    {    
	template<typename T>
	class PropChangeOp: public Guikachu::PropChangeOp<T>
	{
	protected:
	    typedef T           value_t;
	    typedef Property<T> property_t;
	    
	    typedef Guikachu::MemberHolder<Resource, property_t> holder_t;
	    
	    ResourceManager *manager;
	    serial_t         resource_serial;
	    holder_t         prop_holder;
	    bool             cascade;
	    
	public:
	    PropChangeOp (const Glib::ustring &label,
			  Resource            *resource,
			  property_t          &prop,
			  const value_t       &new_val,
			  bool                 cascade_) :
		Guikachu::PropChangeOp<T> (label, prop, new_val),
		manager (resource->get_manager ()),
		resource_serial (resource->get_serial ()),
		prop_holder (*resource, prop),
		cascade (cascade_)
		{}

	    UndoOp * combine (UndoOp *other_op) const {
		if (!cascade)
		    return 0;
		
		PropChangeOp<value_t> *op = dynamic_cast<PropChangeOp<value_t>*> (other_op);
		if (!op)
		    return 0;
		
		if (!op->cascade ||
		    op->resource_serial != resource_serial ||
		    op->prop_holder != prop_holder)
		    return 0;
		
		Resource *resource = get_resource ();
		g_assert (resource);
		property_t &prop = prop_holder.get_member (*resource);
		
		PropChangeOp<value_t> *new_op = new PropChangeOp<value_t> (
		    this->label, resource, prop, op->new_val, true);
		new_op->old_val = this->old_val;
		
		return new_op;
	    }
	
	protected:
	    property_t & get_prop () const {
		Resource *resource = get_resource ();
		g_assert (resource);
	    
		return prop_holder.get_member (*resource);
	    }

	private:
	    Resource * get_resource () const {
		return manager->get_resource (resource_serial);
	    }
	};

	template<typename T>
	class PropChangeOpFactory: public Guikachu::PropChangeOpFactory<T>
	{
	    typedef T           value_t;
	    typedef Property<T> property_t;
	    
	    Glib::ustring  label_template;
	    Resource      *resource;
	    property_t    &prop;
	    bool           cascade;
	    
	    UndoManager &undo_manager;
	    
	public:
	    PropChangeOpFactory (const Glib::ustring &label_template_,
				 Resource            *resource_,
				 property_t          &prop_,
				 bool                 cascade_ = false) :
		label_template (label_template_),
		resource (resource_),
		prop (prop_),
		cascade (cascade_),
		undo_manager (resource->get_manager ()->get_undo_manager ())
		{}
	    
	    void push_change (const value_t &value) {
		char *label = g_strdup_printf (label_template.c_str (), resource->id ().c_str ());
		
		UndoOp *op = new PropChangeOp<value_t> (label, resource, prop, value, cascade);
		value_t old_value = prop;
		
		prop = value;
		if (prop != old_value)
		    undo_manager.push (op);
		else
		    delete op;
		
		g_free (label);
	    }
	};

	class RenameOpFactory: public Guikachu::PropChangeOpFactory<std::string>
	{
	    Resource    *resource;
	    UndoManager &undo_manager;
	    
	public:
	    RenameOpFactory (Resource *resource_):
		resource (resource_),
		undo_manager (resource->get_manager ()->get_undo_manager ())
		{}
	    
	    void push_change (const std::string &value) {
		char *label = g_strdup_printf (_("Rename %s to %s"),
					       resource->id ().c_str (),
					       value.c_str ());
		
		UndoOp *op = new PropChangeOp<std::string> (label, resource, resource->id, value, false);
		std::string old_value = resource->id;
		
		resource->id = value;
		if (resource->id != old_value)
		    undo_manager.push (op);
		else
		    delete op;
		
		g_free (label);
	    }
	};
	
    } // namespace ResourceOps
} // namespace Guikachu

#endif /* !GUIKACHU_PROPERTY_OPS_RESOURCE_H */
