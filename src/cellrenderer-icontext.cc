//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "cellrenderer-icontext.h"

using namespace Guikachu::GUI;

CellRendererIconText::CellRendererIconText ()
{
    property_editable ().signal_changed ().connect (
        sigc::mem_fun (*this, &CellRendererIconText::editable_cb));
}

void CellRendererIconText::editable_cb ()
{
    property_mode () = property_editable () ? Gtk::CELL_RENDERER_MODE_EDITABLE : Gtk::CELL_RENDERER_MODE_INERT;
}

Glib::PropertyProxy<Glib::RefPtr<Gdk::Pixbuf> > CellRendererIconText::property_pixbuf ()
{
    return cell_pixbuf.property_pixbuf ();
}

Gtk::CellRendererPixbuf & CellRendererIconText::get_pixbuf_cell ()
{
    return cell_pixbuf;
}

void CellRendererIconText::get_size_vfunc (Gtk::Widget          &widget,
                                           const Gdk::Rectangle *cell_area,
                                           int                  *x_offset,
                                           int                  *y_offset,
                                           int                  *width,
                                           int                  *height) const
{
    int pix_x, pix_y, pix_w, pix_h;    
    cell_pixbuf.get_size (widget, *cell_area, pix_x, pix_y, pix_w, pix_h);

    int text_x, text_y, text_w, text_h;
    Gtk::CellRendererText::get_size_vfunc (widget, cell_area, &text_x, &text_y, &text_w, &text_h);

    if (text_w && pix_w)
        pix_w += property_xpad ();
    
    if (x_offset)
        *x_offset = 0;
    
    if (y_offset)
        *y_offset = 0;

    if (width)
        *width = text_w + pix_w;

    if (height)
        *height = std::max (text_h, pix_h);
}

void CellRendererIconText::render_vfunc (const Glib::RefPtr<Gdk::Drawable> &window,
                                         Gtk::Widget                       &widget,
                                         const Gdk::Rectangle              &background_area,
                                         const Gdk::Rectangle              &cell_area,
                                         const Gdk::Rectangle              &expose_area,
                                         Gtk::CellRendererState             flags)
{
    // This ugly cast is necessary to work around a bug in GTKmm 2.4
    Glib::RefPtr<Gdk::Window> window_win = Glib::RefPtr<Gdk::Window>::cast_dynamic (window);
    
    int pix_x, pix_y, pix_w, pix_h;    
    cell_pixbuf.get_size (widget, cell_area, pix_x, pix_y, pix_w, pix_h);
    
    Gdk::Rectangle pix_cell_area = cell_area;
    pix_cell_area.set_width (pix_w);
    
    cell_pixbuf.render (window_win, widget, background_area, pix_cell_area, expose_area, flags);
    
    Gdk::Rectangle text_cell_area = cell_area;

    if (pix_w)
    {
        text_cell_area.set_x (text_cell_area.get_x () + pix_w + property_xpad ());
        text_cell_area.set_width (text_cell_area.get_width () - pix_w - property_xpad ());
    }

    Gtk::CellRendererText::render_vfunc (window, widget, background_area, text_cell_area, expose_area, flags);
}

Gtk::CellEditable * CellRendererIconText::start_editing_vfunc (GdkEvent               *event,
                                                               Gtk::Widget            &widget,
                                                               const Glib::ustring    &path,
                                                               const Gdk::Rectangle   &background_area,
                                                               const Gdk::Rectangle   &cell_area,
                                                               Gtk::CellRendererState  flags)
{
    int pix_x, pix_y, pix_w, pix_h;    
    cell_pixbuf.get_size (widget, cell_area, pix_x, pix_y, pix_w, pix_h);

    Gdk::Rectangle text_cell_area = cell_area;

    if (pix_w)
    {
        text_cell_area.set_x (cell_area.get_x () + pix_w + property_xpad ());
        text_cell_area.set_width (cell_area.get_width () - pix_w - property_xpad ());
    }

    // If the editing is started via a mouse click, check if it
    // occured on the text
    if (event && event->type == GDK_BUTTON_PRESS)
    {
        if (event->button.x < text_cell_area.get_x ())
            return 0;
    }
    
    const_cast<Gdk::Rectangle&> (cell_area) = text_cell_area;
    return Gtk::CellRendererText::start_editing_vfunc (event, widget, path, background_area, cell_area, flags);
}
