//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/xml/res-form.h"

#include "io/xml-saver.h"
#include "io/xml-loader.h"

#include "io/storage-processor.h"
#include "io/storage-node-handlers.h"

#include "form-editor/widget-util.h"

using namespace Guikachu::IO::XML;

FormIOAdaptor::FormIOAdaptor (Resources::Form *res_):
    res (res_)
{
}

void FormIOAdaptor::load (const StorageNode &node)
{
    // TODO: use StorageProcessor
    
    // Defaults for binary tags
    res->frame = false;
    res->modal = false;
    res->savebehind = false;
    
    for (StorageNode curr_node = node.children ();
         curr_node; curr_node++)
    {
        if (curr_node.name () == "title")
            res->title = curr_node.get_content ();
        else if (curr_node.name () == "help")
            res->help_id = curr_node.get_prop_string ("id");
        else if (curr_node.name () == "menu")
            res->menu_id = curr_node.get_prop_string ("id");
        else if (curr_node.name () == "modal")
            res->modal = true;
        else if (curr_node.name () == "frame")
            res->frame = true;
        else if (curr_node.name () == "savebehind")
            res->savebehind = true;
	else if (curr_node.name () == "pos")
        {
	    res->x = curr_node.get_prop_int ("x");
	    res->y = curr_node.get_prop_int ("y");
	    
	    res->width  = curr_node.get_prop_int ("width");
	    res->height = curr_node.get_prop_int ("height");
        }
        else if (curr_node.name () == "widgets")
        {
            res->def_button = curr_node.get_prop_string ("default");
            
            for (StorageNode widget_node = curr_node.children (); widget_node; widget_node++)
                load_widget (widget_node);
        }
        else
            g_warning ("Error parsing `form' resource: "
                       "unexpected element `%s'",
                       curr_node.name ().c_str ());
    }
}

void FormIOAdaptor::save (StorageNode &node)
{
    /* Title */
    node.add_node ("title", res->title);
    
    /* Position */
    StorageNode pos_node = node.add_node ("pos");
    pos_node.set_prop ("x", res->x);
    pos_node.set_prop ("y", res->y);
    pos_node.set_prop ("width",  res->width);
    pos_node.set_prop ("height", res->height);
    
    /* Help ID */
    if (res->help_id != "")
	node.add_node ("help").set_prop ("id", res->help_id);

    /* Menu ID */
    if (res->menu_id != "")
	node.add_node ("menu").set_prop ("id", res->menu_id);

    if (res->modal)
	node.add_node ("modal");
    if (res->frame)
	node.add_node ("frame");
    if (res->savebehind)
	node.add_node ("savebehind");

    /* Widgets */
    const std::set<Widget*> &widgets = res->get_widgets ();
    if (widgets.size ())
    {
	StorageNode widgetlist_node = node.add_node ("widgets");

	if (res->def_button != "")
	    widgetlist_node.set_prop ("default", res->def_button);
	
	for (std::set<Widget*>::const_iterator i = widgets.begin ();
	     i != widgets.end (); i++)
	    save_widget (*i, widgetlist_node);
    }
}

void FormIOAdaptor::save_widget (Widget *widget, StorageNode &node)
{
    std::string curr_node_name = Widgets::type_id_from_type (widget->get_type ());
    
    StorageNode widget_node = node.add_node (curr_node_name);
    widget_node.set_prop ("id", widget->id);

    WidgetSaver saver (widget_node);
    widget->apply_visitor (saver);
}

void FormIOAdaptor::load_widget (const StorageNode &node)
{
    std::string node_name = node.name ();
    std::string id = node.get_prop_string ("id");

    if (id == "")
    {
        g_warning ("`%s': Missing ID field", node_name.c_str ());
        return;
    }
    
    Widgets::Type type = Widgets::type_from_type_id (node.name ());
    
    if (type == Widgets::WIDGET_NONE)
    {
	g_warning ("Unknown widget type `%s'", node.name ().c_str ());
	return;
    }
    
    // try_alternate_names is TRUE to make cut & paste implementable
    // through the IO system
    Widget *widget = res->create_widget (type, id, true);
    WidgetLoader loader (node);
    widget->apply_visitor (loader);
}
