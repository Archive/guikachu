//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_IO_XML_RES_FORM_H
#define GUIKACHU_IO_XML_RES_FORM_H

#include "io/xml/io-adaptor.h"
#include "form-res.h"

namespace Guikachu
{
    namespace IO
    {
	namespace XML
	{
	    class FormIOAdaptor: public IOAdaptor
	    {
		Resources::Form *res;
		
	    public:
		explicit FormIOAdaptor (Resources::Form *res);
		
		void save (StorageNode       &node);
		void load (const StorageNode &node);
		
	    private:
		void save_widget (Widget *widget, StorageNode &node);
		void load_widget (const StorageNode &node);
	    };
	}
    }
}

#endif /* ! GUIKACHU_IO_GUIKACHU_RES_FORM_H */
