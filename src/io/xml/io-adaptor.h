//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_IO_XML_IO_ADAPTOR_H
#define GUIKACHU_IO_XML_IO_ADAPTOR_H

#include "storage-node.h"

namespace Guikachu
{
    namespace IO
    {
	namespace XML
	{
	    class IOAdaptor
	    {
            protected:
                Glib::ustring src_hint;
                
	    public:
		virtual ~IOAdaptor () {};
		
		virtual void save (StorageNode       &node) = 0;
		virtual void load (const StorageNode &node) = 0;

                void set_src_hint (const Glib::ustring &src_hint_) { src_hint = src_hint_; };
	    };
	}
    }
}

#endif /* ! GUIKACHU_IO_XML_IO_ADAPTOR_H */
