//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "widget-io-util.h"

#include "form-editor/auto-resizeable.h"

namespace Guikachu
{
namespace IO
{
namespace XML
{

TextHandler::TextHandler (Widgets::Textual *widget_) :
    widget (widget_)
{
}

void TextHandler::operator() (const StorageNode &node)
{
    widget->font = node.get_prop_int ("font");
    widget->text = node.get_content ();
}

PositionHandler::PositionHandler (Widget *widget_) :
    widget (widget_)
{
}

void PositionHandler::operator() (const StorageNode &node)
{
    widget->x = node.get_prop_int ("x");
    widget->y = node.get_prop_int ("y");
}

SizePosHandler::SizePosHandler (Widgets::Resizeable *element_) :
    element (element_)
{
}

void SizePosHandler::operator() (const StorageNode &node)
{
    element->set_x (node.get_prop_int ("x"));
    element->set_y (node.get_prop_int ("y"));

    load_width (node);
    load_height (node);
}

void SizePosHandler::load_width (const StorageNode &node)
{
    std::string width_str = node.get_prop_string ("width");

    element->width = atoi (width_str.c_str ());
    
    Widgets::AutoWidth *auto_width = dynamic_cast<Widgets::AutoWidth*> (element);
    if (auto_width)
	auto_width->manual_width = (width_str != "auto");
}

void SizePosHandler::load_height (const StorageNode &node)
{
    std::string height_str = node.get_prop_string ("height");

    element->height = atoi (height_str.c_str ());
    
    Widgets::AutoHeight *auto_height = dynamic_cast<Widgets::AutoHeight*> (element);
    if (auto_height)
	auto_height->manual_height = (height_str != "auto");
}

void save_widget_pos (Widget *widget, StorageNode &root_node)
{
    StorageNode node = root_node.add_node ("pos");
    
    node.set_prop ("x", widget->x);
    node.set_prop ("y", widget->y);
}

void save_widget_pos_size (Widgets::Resizeable *widget, StorageNode &root_node)
{
    // Save position
    Widget *pos_widget = dynamic_cast<Widget*> (widget);
    StorageNode pos_node = root_node.add_node ("pos");

    pos_node.set_prop ("x", pos_widget->x);
    pos_node.set_prop ("y", pos_widget->y);

    // Save width
    Widgets::AutoWidth *auto_width = dynamic_cast<Widgets::AutoWidth*> (widget);
    if (auto_width)
    {
	if (auto_width->manual_width)
	    pos_node.set_prop ("width", widget->width);
	else
	    pos_node.set_prop ("width", "auto");
    } else {
	pos_node.set_prop ("width", widget->width);
    }

    // Save height
    Widgets::AutoHeight *auto_height = dynamic_cast<Widgets::AutoHeight*> (widget);
    if (auto_height)
    {
	if (auto_height->manual_height)
	    pos_node.set_prop ("height", widget->height);
	else
	    pos_node.set_prop ("height", "auto");
    } else {
	pos_node.set_prop ("height", widget->height);
    }
}
    
void save_widget_text (Widgets::Textual *widget, StorageNode &root_node)
{
    StorageNode node = root_node.add_node ("text", widget->text);
    node.set_prop ("font", widget->font);
}

    
} // namespace XML
} // namespace IO
} // namespace Guikachu
