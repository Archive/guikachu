//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/xml/widget-checkbox.h"

#include "io/storage-processor.h"
#include "io/storage-node-handlers.h"

#include "io/xml/widget-io-util.h"

using namespace Guikachu::IO::XML;

CheckboxIOAdaptor::CheckboxIOAdaptor (Widgets::Checkbox *widget_):
    widget (widget_)
{
}

void CheckboxIOAdaptor::load (const StorageNode &node)
{
    StorageProcessor processor;
    processor.add_handler ("pos",          new SizePosHandler (widget));
    processor.add_handler ("text",         new TextHandler (widget));
    processor.add_handler ("usable",       new StorageNodeHandlers::Flag (widget->usable));
    processor.add_handler ("disabled",     new StorageNodeHandlers::Flag (widget->disabled));
    processor.add_handler ("anchor_right", new StorageNodeHandlers::Flag (widget->anchor_right));
    processor.add_handler ("group",        new StorageNodeHandlers::IntProp ("id", widget->group));
    processor.add_handler ("toggled",      new StorageNodeHandlers::Flag (widget->toggled));
    
    processor.run (node);
}

void CheckboxIOAdaptor::save (StorageNode &node)
{
    save_widget_pos_size (widget, node);
    
    /* Usable */
    if (widget->usable)
	node.add_node ("usable");
    if (widget->disabled)
	node.add_node ("disabled");
    
    /* Group ID */
    node.add_node ("group").set_prop ("id", widget->group);
    
    /* Toggled */
    if (widget->toggled)
	node.add_node ("toggled");
    
    /* Anchor_right */
    if (widget->anchor_right)
	node.add_node ("anchor_right");
    
    /* Label and font */
    save_widget_text (widget, node);
}
