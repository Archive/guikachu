//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/xml/widget-list.h"

#include "io/storage-processor.h"
#include "io/storage-node-handlers.h"

#include "io/xml/widget-io-util.h"

using namespace Guikachu::IO::XML;

ListIOAdaptor::ListIOAdaptor (Widgets::List *widget_):
    widget (widget_)
{
}

void ListIOAdaptor::load (const StorageNode &node)
{
    class ItemsHandler: public StorageNodeHandlers::StringListContents
    {
	Widgets::List *widget;
    public:
	ItemsHandler (Widgets::List *widget_):
	    StorageNodeHandlers::StringListContents ("item", widget_->items),
	    widget (widget_)
	{}

	void operator() (const StorageNode &node)
	{
	    widget->font = node.get_prop_int ("font");
	    widget->visible_items = node.get_prop_int ("visible");
	    StorageNodeHandlers::StringListContents::operator() (node);
	}
    };

    StorageProcessor processor;
    processor.add_handler ("pos",    new SizePosHandler (widget));
    processor.add_handler ("usable", new StorageNodeHandlers::Flag (widget->usable));
    processor.add_handler ("items",  new ItemsHandler (widget));
    
    processor.run (node);
}

void ListIOAdaptor::save (StorageNode &node)
{
    /* Position */
    StorageNode pos_node = node.add_node ("pos");
    pos_node.set_prop ("x", widget->x);
    pos_node.set_prop ("y", widget->y);

    /* Size */
    if (widget->manual_width)
	pos_node.set_prop ("width", widget->width);
    else
	pos_node.set_prop ("width", "auto");

    /* Usable */
    if (widget->usable)
	node.add_node ("usable");

    /* List items */
    StorageNode item_node = node.add_node ("items");
    item_node.set_prop ("font", widget->font);
    item_node.set_prop ("visible", widget->visible_items);

    const std::vector<std::string> &items = widget->items ();
    for (std::vector<std::string>::const_iterator i = items.begin ();
	 i != items.end (); i++)
	item_node.add_node ("item", *i);
}
