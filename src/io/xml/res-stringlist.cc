//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/xml/res-stringlist.h"

#include "io/storage-processor.h"
#include "io/storage-node-handlers.h"

#include <glib.h> // for g_warning, FIXME: remove when converting
                  // everything to StorageProcessor

using namespace Guikachu::IO::XML;

StringListIOAdaptor::StringListIOAdaptor (Resources::StringList *res_):
    res (res_)
{
}

void StringListIOAdaptor::load (const StorageNode &node)
{
    std::vector<std::string> strings_tmp;
    
    for (StorageNode curr_node = node.children (); curr_node; ++curr_node)
    {
        if (curr_node.name () == "text")
            strings_tmp.push_back (curr_node.get_content ());
        else if (curr_node.name () == "prefix")
            res->prefix = curr_node.get_content ();
        else
            g_warning ("Error parsing `stringlist' resource: "
                       "unexpected element `%s'", curr_node.name ().c_str ());
    }

    res->strings = strings_tmp;
}

void StringListIOAdaptor::save (StorageNode &node)
{
    node.add_node ("prefix", res->prefix);
    
    const std::vector<std::string> &strings = res->strings;
    for (std::vector<std::string>::const_iterator i = strings.begin ();
         i != strings.end (); i++)
        node.add_node ("text", *i);    
}
