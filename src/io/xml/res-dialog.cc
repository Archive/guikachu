//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/xml/res-dialog.h"

#include "io/storage-processor.h"
#include "io/storage-node-handlers.h"

using namespace Guikachu::IO::XML;

DialogIOAdaptor::DialogIOAdaptor (Resources::Dialog *res_):
    res (res_)
{
}

void DialogIOAdaptor::load (const StorageNode &node)
{
    // Create a bunch of 'lambda functors'
    class DialogTypeHandler: public StorageNodeHandlers::HandlerBase
    {
	Property<Resources::Dialog::DialogType> &target;
    public:
	explicit DialogTypeHandler (Property<Resources::Dialog::DialogType> &target_) :
	    target (target_)
	    {};
	
	void operator() (const StorageNode &node)
	    {
		const std::string &xml_type = node.get_prop_string ("value");
		
		if (xml_type == "information")
		    target = Resources::Dialog::TYPE_INFORMATION;
		else if (xml_type == "confirmation")
		    target = Resources::Dialog::TYPE_CONFIRMATION;
		else if (xml_type == "warning")
		    target = Resources::Dialog::TYPE_WARNING;
		else if (xml_type == "error")
		    target = Resources::Dialog::TYPE_ERROR;
	    };
    };

    class ButtonsHandler: public StorageNodeHandlers::StringListProp
    {
	Resources::Dialog *res;
    public:
	explicit ButtonsHandler (Resources::Dialog *res_):
	    StorageNodeHandlers::StringListProp ("button", "label", res_->buttons),
	    res (res_)
	    {};

	void operator() (const StorageNode &node)
	    {
		/* Set default button */
		res->default_button = node.get_prop_int ("default");
		
		/* Iterate buttons */
		StorageNodeHandlers::StringListProp::operator() (node);
	    };
    };
    
    StorageProcessor processor;
    processor.add_handler ("text",    new StorageNodeHandlers::StringContents (res->text));
    processor.add_handler ("title",   new StorageNodeHandlers::StringContents (res->title));
    processor.add_handler ("help",    new StorageNodeHandlers::StringProp ("id", res->help_id));
    processor.add_handler ("type",    new DialogTypeHandler (res->dialog_type));
    processor.add_handler ("buttons", new ButtonsHandler (res));

    processor.run (node);
}

void DialogIOAdaptor::save (StorageNode &node)
{
    std::string type_str;

    /* Dialog type */
    switch (res->dialog_type)
    {
    case Resources::Dialog::TYPE_INFORMATION:
        type_str = "information";
        break;
    case Resources::Dialog::TYPE_CONFIRMATION:
        type_str = "confirmation";
        break;
    case Resources::Dialog::TYPE_WARNING:
        type_str = "warning";
        break;
    case Resources::Dialog::TYPE_ERROR:
        type_str = "error";
    }

    StorageNode my_node = node.add_node ("type");
    my_node.set_prop ("value", type_str);

    /* Title */
    node.add_node ("title", res->title);
    /* Text */
    node.add_node ("text", res->text);

    /* Help ID */
    if (res->help_id != "")
    {
        my_node = node.add_node ("help");
        my_node.set_prop ("id", res->help_id);
    }

    /* Buttons */
    const std::vector<std::string> &buttons = res->buttons;
    if (buttons.size ())
    {
        my_node = node.add_node ("buttons");
        my_node.set_prop ("default", res->default_button);
        for (std::vector <std::string>::const_iterator i = buttons.begin ();
             i != buttons.end (); i++)
        {
            StorageNode button_node = my_node.add_node ("button");
            button_node.set_prop ("label", *i);
        }
    }
}
