//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/xml/widget-table.h"

#include "io/storage-processor.h"
#include "io/storage-node-handlers.h"

#include "io/xml/widget-io-util.h"

using namespace Guikachu::IO::XML;

TableIOAdaptor::TableIOAdaptor (Widgets::Table *widget_):
    widget (widget_)
{
}

void TableIOAdaptor::load (const StorageNode &node)
{
    class TableDataHandler: public StorageNodeHandlers::HandlerBase
    {
	Widgets::Table *widget;
	std::vector<int> columns_tmp;
	
    public:
	TableDataHandler (Widgets::Table *widget_):
	    widget (widget_)
	{
	    // Reset to empty default values
	    widget->column_width = std::vector<int>(0);
	    widget->num_columns  = 0;
	}

	~TableDataHandler ()
	{
	    widget->column_width = columns_tmp;
	    widget->num_columns = columns_tmp.size ();
	}

	void operator() (const StorageNode &node)
	{
	    class TableColumnHandler: public StorageNodeHandlers::HandlerBase
	    {
		std::vector<int> &columns;
	    public:
		TableColumnHandler (std::vector<int> &columns_) :
		    columns (columns_)
		{}
		
		void operator() (const StorageNode &node)
		{
		    columns.push_back (node.get_prop_int ("width"));
		}
	    };
	    
	    widget->num_rows = node.get_prop_int ("rows");
	    StorageProcessor columns_processor;
	    columns_processor.add_handler ("column", new TableColumnHandler (columns_tmp));
	    columns_processor.run (node);
	};
    };

    StorageProcessor processor;
    processor.add_handler ("pos",        new PositionHandler (widget));
    processor.add_handler ("table_data", new TableDataHandler (widget));
    processor.run (node);
}

void TableIOAdaptor::save (StorageNode &node)
{
    save_widget_pos (widget, node);

    StorageNode data_node = node.add_node ("table_data");

    /* Number of rows and columns */
    data_node.set_prop ("rows", widget->num_rows);

    const std::vector<int> &columns = widget->column_width;
    for (int i = 0; i < widget->num_columns; i++)
	data_node.add_node ("column").set_prop ("width", columns[i]);
}
