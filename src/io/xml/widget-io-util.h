//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_IO_XML_WIDGET_WIDGET_IO_UTIL_H
#define GUIKACHU_IO_XML_WIDGET_WIDGET_IO_UTIL_H

#include "storage-node.h"
#include "form-editor/widget.h"
#include "form-editor/resizeable.h"
#include "form-editor/textual.h"

#include "io/storage-node-handlers.h"

namespace Guikachu
{
    namespace IO
    {
	namespace XML
	{
	    void save_widget_pos      (Widget              *widget, StorageNode &root_node);
	    void save_widget_pos_size (Widgets::Resizeable *widget, StorageNode &root_node);
	    
	    void save_widget_text     (Widgets::Textual    *widget, StorageNode &root_node);

	    // StorageNode handlers used by several widgets
	    class TextHandler: public StorageNodeHandlers::HandlerBase
	    {
		Widgets::Textual *widget;
	    public:
		TextHandler (Widgets::Textual *widget);
		void operator() (const StorageNode &node);
	    };
	    
	    class PositionHandler: public StorageNodeHandlers::HandlerBase
	    {
		Widget *widget;
	    public:
		PositionHandler (Widget *widget);		
		void operator() (const StorageNode &node);
	    };

	    class SizePosHandler: public StorageNodeHandlers::HandlerBase
	    {
	    protected:
		Widgets::Resizeable *element;
		
	    public:
		SizePosHandler (Widgets::Resizeable *element);

	    protected:
		virtual void load_width (const StorageNode &node);
		virtual void load_height (const StorageNode &node);

	    public:
		void operator() (const StorageNode &node);
	    };
	}
    }
}

#endif /* ! GUIKACHU_IO_XML_WIDGET_WIDGETS_IO_UTIL_H */
