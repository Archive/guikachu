//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/xml/widget-label.h"

#include "io/storage-processor.h"
#include "io/storage-node-handlers.h"

#include "io/xml/widget-io-util.h"

using namespace Guikachu::IO::XML;

LabelIOAdaptor::LabelIOAdaptor (Widgets::Label *widget_):
    widget (widget_)
{
}

void LabelIOAdaptor::load (const StorageNode &node)
{
    StorageProcessor processor;
    processor.add_handler ("pos",    new PositionHandler (widget));
    processor.add_handler ("text",   new TextHandler (widget));
    processor.add_handler ("usable", new StorageNodeHandlers::Flag (widget->usable));

    processor.run (node);
}

void LabelIOAdaptor::save (StorageNode &node)
{
    /* Position */
    save_widget_pos (widget, node);

    /* Usable */
    if (widget->usable)
	node.add_node ("usable");
    
    /* Text and font */
    save_widget_text (widget, node);
}
