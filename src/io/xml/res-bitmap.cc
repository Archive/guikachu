//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/xml/res-bitmap.h"

#include "io/storage-processor.h"
#include "io/storage-node-handlers.h"
#include "io/base64.h"

#include <gdkmm/pixbufloader.h>

using namespace Guikachu::IO::XML;

BitmapIOAdaptor::BitmapIOAdaptor (Resources::Bitmap *res_):
    res (res_)
{
}

void BitmapIOAdaptor::load (const StorageNode &node)
{
    std::string type_str = node.get_prop_string ("type");
    Resources::Bitmap::BitmapType type = Resources::Bitmap::TYPE_GREY_16;
    
    if (type_str == "mono")
        type = Resources::Bitmap::TYPE_MONO;
    else if (type_str == "grey_4")
        type = Resources::Bitmap::TYPE_GREY_4;
    else if (type_str == "grey_16")
        type = Resources::Bitmap::TYPE_GREY_16;
    else if (type_str == "color_16")
        type = Resources::Bitmap::TYPE_COLOR_16;
    else if (type_str == "color_256")
        type = Resources::Bitmap::TYPE_COLOR_256;
    else if (type_str == "color_16k")
	type = Resources::Bitmap::TYPE_COLOR_16K;

    res->bitmap_type = type;
    
    // Create dummy property
    VSignal sig;
    Property<std::string> image_data_base64_prop (sig);
    
    StorageProcessor processor;
    processor.add_handler ("image_data", new StorageNodeHandlers::StringContents (image_data_base64_prop));

    processor.run (node);

    // Load image data from base64 encoded string
    unsigned char *data = 0;
    size_t len;
    
    if (image_data_base64_prop == "")
	return;
    
    IO::Base64::decode (image_data_base64_prop, data, len);
    res->load_data (data, len, src_hint);
    delete[] data;
}

void BitmapIOAdaptor::save (StorageNode &node)
{
    std::string type_str;
    
    switch (res->bitmap_type ())
    {
    case Resources::Bitmap::TYPE_MONO:
        type_str = "mono";
        break;    
    case Resources::Bitmap::TYPE_GREY_4:
        type_str = "grey_4";
        break;
    case Resources::Bitmap::TYPE_GREY_16:
        type_str = "grey_16";
        break;
    case Resources::Bitmap::TYPE_COLOR_16:
        type_str = "color_16";
        break;
    case Resources::Bitmap::TYPE_COLOR_256:
        type_str = "color_256";
        break;
    case Resources::Bitmap::TYPE_COLOR_16K:
	type_str = "color_16k";
	break;
    }

    node.set_prop ("type", type_str);

    unsigned char *pixbuf_buf = 0;
    size_t         pixbuf_len;
    
    try {
        res->save_data_png (pixbuf_buf, pixbuf_len);
        
        StorageNode image_node = node.add_node ("image_data", Base64::encode (pixbuf_buf, pixbuf_len));
        image_node.set_prop ("encoding", "base64");
        image_node.set_prop ("content-type", "image/png");

        delete[] pixbuf_buf;
    } catch (Glib::Exception &e) {
        delete[] pixbuf_buf;
    }
}
