//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/xml-saver.h"
#include "io/xml-loader.h"

#include "io/xml/res-dialog.h"
#include "io/xml/res-string.h"
#include "io/xml/res-stringlist.h"
#include "io/xml/res-menu.h"
#include "io/xml/res-form.h"
#include "io/xml/res-blob.h"
#include "io/xml/res-bitmap.h"
#include "io/xml/res-bitmapfamily.h"

namespace Guikachu
{
    
namespace IO
{

namespace XML
{

ResourceSaver::ResourceSaver (StorageNode &node_, const Glib::ustring &src_hint_):
    node (node_),
    src_hint (src_hint_)
{
}
    
ResourceLoader::ResourceLoader (const StorageNode &node_, const Glib::ustring &src_hint_):
    node (node_),
    src_hint (src_hint_)
{
}

#define CREATE_RESOURCE_IO_VISITOR(T) 				\
    void ResourceSaver::visit_resource (Resources::T *res)	\
    {								\
	T##IOAdaptor adaptor (res);                             \
        adaptor.set_src_hint (src_hint);                        \
        adaptor.save (node);                                    \
    }								\
								\
    void ResourceLoader::visit_resource (Resources::T *res)	\
    {								\
	T##IOAdaptor adaptor (res);                             \
        adaptor.set_src_hint (src_hint);                        \
        adaptor.load (node);                                    \
    }

CREATE_RESOURCE_IO_VISITOR(Dialog);
CREATE_RESOURCE_IO_VISITOR(String);
CREATE_RESOURCE_IO_VISITOR(StringList);
CREATE_RESOURCE_IO_VISITOR(Menu);
CREATE_RESOURCE_IO_VISITOR(Form);
CREATE_RESOURCE_IO_VISITOR(Blob);
CREATE_RESOURCE_IO_VISITOR(Bitmap);
CREATE_RESOURCE_IO_VISITOR(BitmapFamily);

} // namespace XML
} // namespace IO
} // namespace Guikachu
