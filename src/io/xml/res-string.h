//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_IO_XML_RES_STRING_H
#define GUIKACHU_IO_XML_RES_STRING_H

#include "io-adaptor.h"
#include "string-res.h"

namespace Guikachu
{
    namespace IO
    {
	namespace XML
	{
	    class StringIOAdaptor: public IOAdaptor
	    {
		Resources::String *res;
		
	    public:
		explicit StringIOAdaptor (Resources::String *res);
		
		void save (StorageNode       &node);
		void load (const StorageNode &node);
	    };
	}
    }
}

#endif /* ! GUIKACHU_IO_XML_RES_STRING_H */
