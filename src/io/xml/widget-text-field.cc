//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/xml/widget-text-field.h"

#include "io/storage-processor.h"
#include "io/storage-node-handlers.h"
#include "io/xml/widget-io-util.h"

using namespace Guikachu::IO::XML;

TextFieldIOAdaptor::TextFieldIOAdaptor (Widgets::TextField *widget_):
    widget (widget_)
{
}

void TextFieldIOAdaptor::load (const StorageNode &node)
{
    class TextHandler: public StorageNodeHandlers::HandlerBase
    {
	Widgets::TextField *widget;
    public:
	explicit TextHandler (Widgets::TextField *widget_):
	    widget (widget_)
	{}

	void operator() (const StorageNode &node)
	{
	    widget->font = node.get_prop_int ("font");
	    widget->max_length = node.get_prop_int ("max_length");
	}
    };

    /*
     * Evil hack: some files created by Guikachu 1.0 contain 'auto'
     * when width was 30
     */
    class PosHandler: public SizePosHandler
    {
    public:
	PosHandler (Widgets::TextField *widget):
	    SizePosHandler (widget)
	{}
	
    protected:
	void load_width (const StorageNode &node)
	{
	    std::string width_str = node.get_prop_string ("width");
	    
	    if (width_str == "auto")
		element->width = 30;
	    else
		element->width = atoi (width_str.c_str ());
	}
    };
    
    StorageProcessor processor;
    processor.add_handler ("pos",           new PosHandler (widget));
    processor.add_handler ("text",          new TextHandler (widget));
    processor.add_handler ("usable",        new StorageNodeHandlers::Flag (widget->usable));
    processor.add_handler ("editable",      new StorageNodeHandlers::Flag (widget->editable));
    processor.add_handler ("multi_line",    new StorageNodeHandlers::Flag (widget->multi_line));
    processor.add_handler ("auto_shift",    new StorageNodeHandlers::Flag (widget->auto_shift));
    processor.add_handler ("numeric",       new StorageNodeHandlers::Flag (widget->numeric));
    processor.add_handler ("underline",     new StorageNodeHandlers::Flag (widget->underline));
    processor.add_handler ("dynamic_size",  new StorageNodeHandlers::Flag (widget->dynamic_size));
    processor.add_handler ("justify_right", new StorageNodeHandlers::Flag (widget->justify_right));
    processor.add_handler ("has_scrollbar", new StorageNodeHandlers::Flag (widget->has_scrollbar));
    
    processor.run (node);
}

void TextFieldIOAdaptor::save (StorageNode &node)
{
    /* Position */
    StorageNode pos_node = node.add_node ("pos");
    pos_node.set_prop ("x", widget->x);
    pos_node.set_prop ("y", widget->y);

    /* Size */
    pos_node.set_prop ("width", widget->width);

    if (widget->manual_height)
	pos_node.set_prop ("height", widget->height);
    else
	pos_node.set_prop ("height", "auto");
    
    /* Flags */
    if (widget->usable)
	node.add_node ("usable");
    if (widget->editable)
	node.add_node ("editable");
    if (widget->multi_line)
	node.add_node ("multi_line");
    if (widget->auto_shift)
	node.add_node ("auto_shift");
    if (widget->numeric)
	node.add_node ("numeric");

    if (widget->underline)
	node.add_node ("underline");
    if (widget->dynamic_size)
	node.add_node ("dynamic_size");
    if (widget->justify_right)
	node.add_node ("justify_right");
    if (widget->has_scrollbar)
	node.add_node ("has_scrollbar");

    /* Font and length */
    StorageNode text_node = node.add_node ("text");
    text_node.set_prop ("font", widget->font);
    text_node.set_prop ("max_length", widget->max_length);
}
