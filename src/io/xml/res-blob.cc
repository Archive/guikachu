//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/xml/res-blob.h"

using namespace Guikachu::IO::XML;

BlobIOAdaptor::BlobIOAdaptor (Resources::Blob *res_):
    res (res_)
{
}

void BlobIOAdaptor::load (const StorageNode &node)
{
    res->contents = node.get_content ();
}

void BlobIOAdaptor::save (StorageNode &node)
{
    node.set_content (res->contents);
}
