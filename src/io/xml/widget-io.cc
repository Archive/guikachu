//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/xml-saver.h"
#include "io/xml-loader.h"

#include "io/xml/widget-formbitmap.h"
#include "io/xml/widget-button.h"
#include "io/xml/widget-checkbox.h"
#include "io/xml/widget-gadget.h"
#include "io/xml/widget-graffiti.h"
#include "io/xml/widget-label.h"
#include "io/xml/widget-list.h"
#include "io/xml/widget-popup-trigger.h"
#include "io/xml/widget-pushbutton.h"
#include "io/xml/widget-scrollbar.h"
#include "io/xml/widget-slider.h"
#include "io/xml/widget-selector-trigger.h"
#include "io/xml/widget-table.h"
#include "io/xml/widget-text-field.h"

namespace Guikachu
{
    
namespace IO
{

namespace XML
{

WidgetSaver::WidgetSaver (StorageNode &node_, const Glib::ustring &src_hint_):
    node (node_),
    src_hint (src_hint_)
{
}
    
WidgetLoader::WidgetLoader (const StorageNode &node_, const Glib::ustring &src_hint_):
    node (node_),
    src_hint (src_hint_)
{
}

#define CREATE_WIDGET_IO_VISITOR(T) 				\
    void WidgetSaver::visit_widget (Widgets::T *widget)		\
    {								\
	T##IOAdaptor adaptor (widget);                          \
        adaptor.set_src_hint (src_hint);                        \
        adaptor.save (node);                                    \
    }								\
								\
    void WidgetLoader::visit_widget (Widgets::T *widget)	\
    {								\
	T##IOAdaptor (widget).load (node);			\
    }

CREATE_WIDGET_IO_VISITOR(Label);
CREATE_WIDGET_IO_VISITOR(List);
CREATE_WIDGET_IO_VISITOR(Button);
CREATE_WIDGET_IO_VISITOR(PushButton);
CREATE_WIDGET_IO_VISITOR(Graffiti);
CREATE_WIDGET_IO_VISITOR(SelectorTrigger);
CREATE_WIDGET_IO_VISITOR(Checkbox);
CREATE_WIDGET_IO_VISITOR(PopupTrigger);
CREATE_WIDGET_IO_VISITOR(ScrollBar);
CREATE_WIDGET_IO_VISITOR(Slider);
CREATE_WIDGET_IO_VISITOR(TextField);
CREATE_WIDGET_IO_VISITOR(Table);
CREATE_WIDGET_IO_VISITOR(FormBitmap);
CREATE_WIDGET_IO_VISITOR(Gadget);

} // namespace XML
} // namespace IO
} // namespace Guikachu
