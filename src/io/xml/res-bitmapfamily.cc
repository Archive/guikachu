//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/xml/res-bitmapfamily.h"

#include "io/storage-processor.h"
#include "io/storage-node-handlers.h"
#include "io/base64.h"

#include <glib.h> // for g_warning

using namespace Guikachu;
using namespace Guikachu::IO::XML;

namespace
{
    std::string bitmap_type_to_string (Resources::Bitmap::BitmapType type);
    signed int  bitmap_type_from_string (const std::string &type_string);
}

BitmapFamilyIOAdaptor::BitmapFamilyIOAdaptor (Resources::BitmapFamily *res_):
    res (res_)
{
}

namespace
{
} // anonymous namespace

void BitmapFamilyIOAdaptor::load (const StorageNode &node)
{
    for (StorageNode curr_node = node.children (); curr_node; ++curr_node)
    {
        if (curr_node.name () != "images")
        {
            g_warning ("Error parsing `%s' node: unexpected element `%s'",
                       node.name ().c_str (), curr_node.name ().c_str ());
            continue;
        }

        for (StorageNode image_node = curr_node.children (); image_node; ++image_node)
        {
            if (image_node.name () != "image_data")
            {
                g_warning ("Error parsing `%s' node: unexpected element `%s'",
                           node.name ().c_str (), image_node.name ().c_str ());
                continue;
            }

            std::string bitmap_type_str = image_node.get_prop_string ("type");
            int bitmap_type = bitmap_type_from_string (bitmap_type_str);
            if (bitmap_type < 0)
            {
                g_warning ("Error parsing `%s' node: unknown bitmap type `%s'",
                           node.name ().c_str (), bitmap_type_str.c_str ());
                continue;
            }

	    unsigned char *data;
	    size_t len;
            std::string image_data = image_node.get_content_raw ();
	    IO::Base64::decode (image_data, data, len);
	    res->load_data (static_cast<Resources::Bitmap::BitmapType> (bitmap_type), data, len, src_hint);
	    delete[] data;
        }
    }
}

void BitmapFamilyIOAdaptor::save (StorageNode &node)
{
    StorageNode images_node = node.add_node ("images");
    
    save_bitmap (images_node, Resources::Bitmap::TYPE_MONO);
    save_bitmap (images_node, Resources::Bitmap::TYPE_GREY_4);
    save_bitmap (images_node, Resources::Bitmap::TYPE_GREY_16);
    save_bitmap (images_node, Resources::Bitmap::TYPE_COLOR_16);
    save_bitmap (images_node, Resources::Bitmap::TYPE_COLOR_256);
    save_bitmap (images_node, Resources::Bitmap::TYPE_COLOR_16K);    
}

void BitmapFamilyIOAdaptor::save_bitmap (StorageNode                   &parent_node,
                                         Resources::Bitmap::BitmapType  type)
{
    std::string type_name = bitmap_type_to_string (type);
    
    unsigned char *pixbuf_buf;
    size_t pixbuf_len;

    try {
        res->save_data_png (type, pixbuf_buf, pixbuf_len);

        if (!pixbuf_buf)
            return;
        
        StorageNode image_node = parent_node.add_node ("image_data", Base64::encode (pixbuf_buf, pixbuf_len));
        image_node.set_prop ("encoding", "base64");
        image_node.set_prop ("content-type", "image/png");
        image_node.set_prop ("type", type_name);

        delete[] pixbuf_buf;
    } catch (Glib::Exception &e) {
        delete[] pixbuf_buf;
    }
}

namespace
{
    
std::string bitmap_type_to_string (Resources::Bitmap::BitmapType type)
{
    std::string ret;

    switch (type)
    {
    case Resources::Bitmap::TYPE_MONO:
        ret = "mono";
        break;
    case Resources::Bitmap::TYPE_GREY_4:
        ret = "grey_4";
        break;
    case Resources::Bitmap::TYPE_GREY_16:
        ret = "grey_16";
        break;
    case Resources::Bitmap::TYPE_COLOR_16:
        ret = "color_16";
        break;
    case Resources::Bitmap::TYPE_COLOR_256:
        ret = "color_256";
        break;
    case Resources::Bitmap::TYPE_COLOR_16K:
        ret = "color_16k";
        break;
    }
    
    return ret;
}

signed int bitmap_type_from_string (const std::string &type_string)
{
    signed int ret = -1;

    if (type_string == "mono")
        ret = Resources::Bitmap::TYPE_MONO;
    else if (type_string == "grey_4")
        ret = Resources::Bitmap::TYPE_GREY_4;
    else if (type_string == "grey_16")
        ret = Resources::Bitmap::TYPE_GREY_16;
    else if (type_string == "color_16")
        ret = Resources::Bitmap::TYPE_COLOR_16;
    else if (type_string == "color_256")
        ret = Resources::Bitmap::TYPE_COLOR_256;
    else if (type_string == "color_16k")
        ret = Resources::Bitmap::TYPE_COLOR_16K;

    return ret;
}

} // anonymous namespace
