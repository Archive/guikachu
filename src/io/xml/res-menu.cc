//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/xml/res-menu.h"

#include <glib.h> // for g_warning, FIXME: remove when converting
                  // everything to StorageProcessor

using namespace Guikachu::IO::XML;

MenuIOAdaptor::MenuIOAdaptor (Resources::Menu *res_):
    res (res_)
{
}

void MenuIOAdaptor::load (const StorageNode &node)
{
    Resources::Menu::MenuTree submenus;
    
    for (StorageNode curr_node = node.children (); curr_node; ++curr_node)
    {
        if (curr_node.name () == "submenu")
        {
            Resources::Menu::Submenu submenu;
            std::string label = curr_node.get_prop_string ("label");
            
            if (label == "")
                break;
            
            submenu.label = label;
            
            for (StorageNode sub_node = curr_node.children (); sub_node; ++sub_node)
            {
                Resources::Menu::MenuItem menu_item;
                if (sub_node.name () == "menuitem")
                {
                    std::string id = sub_node.get_prop_string ("id");
                    std::string label = sub_node.get_prop_string ("label");
                    char        shortcut = sub_node.get_prop_char ("shortcut");
		    
                    if (id == "" || label == "")
                        break;

                    menu_item.separator = false;

                    menu_item.id = id;
                    menu_item.label = label;                
                    menu_item.shortcut = shortcut;

                    submenu.items.push_back (menu_item);
                }
                else if (sub_node.name () == "separator")
                {
                    menu_item.separator = true;

                    submenu.items.push_back (menu_item);
                }
                else
                    g_warning ("Error parsing `menu' resource: "
                               "unexpected element `%s/%s'",
                               curr_node.name ().c_str (),
                               sub_node.name ().c_str ());
            }
	    
            submenus.push_back (submenu);
	}
	else
	    g_warning ("Error parsing `menu' resource: "
		       "unexpected element `%s'", curr_node.name ().c_str ());
    }

    res->set_submenus (submenus);
}

void MenuIOAdaptor::save (StorageNode &node)
{
    const Resources::Menu::MenuTree& submenus = res->get_submenus ();
    
    for (Resources::Menu::MenuTree::const_iterator i = submenus.begin ();
         i != submenus.end (); i++)
    {
        StorageNode submenu_node = node.add_node ("submenu");
        submenu_node.set_prop ("label", i->label);

        for (std::vector<Resources::Menu::MenuItem>::const_iterator j = i->items.begin ();
             j != i->items.end (); j++)
        {
            if (j->separator)
            {
                submenu_node.add_node ("separator");
            } else {
                StorageNode item_node = submenu_node.add_node ("menuitem");
                item_node.set_prop ("id",    j->id);
                item_node.set_prop ("label", j->label);

                if (j->shortcut)
                    item_node.set_prop ("shortcut", j->shortcut);
            }
        }
    }
}
