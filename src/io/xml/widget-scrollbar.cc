//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/xml/widget-scrollbar.h"

#include "io/storage-processor.h"
#include "io/storage-node-handlers.h"

#include "io/xml/widget-io-util.h"

using namespace Guikachu::IO::XML;

ScrollBarIOAdaptor::ScrollBarIOAdaptor (Widgets::ScrollBar *widget_):
    widget (widget_)
{
}

void ScrollBarIOAdaptor::load (const StorageNode &node)
{
    class ValuesHandler: public StorageNodeHandlers::HandlerBase
    {
	Widgets::ScrollBar *widget;
    public:
	explicit ValuesHandler (Widgets::ScrollBar *widget_):
	    widget (widget_)
	{}

	void operator() (const StorageNode &node)
	{
	widget->min_value = node.get_prop_int ("min");
	widget->max_value = node.get_prop_int ("max");
	widget->page_size = node.get_prop_int ("page_size");
	widget->value     = node.get_prop_int ("value");
	}
    };
    
    StorageProcessor processor;
    processor.add_handler ("pos",    new SizePosHandler (widget));
    processor.add_handler ("usable", new StorageNodeHandlers::Flag (widget->usable));
    processor.add_handler ("values", new ValuesHandler (widget));

    processor.run (node);
}

void ScrollBarIOAdaptor::save (StorageNode &node)
{
    save_widget_pos_size (widget, node);
    
    /* Usable */
    if (widget->usable)
	node.add_node ("usable");

    /* Values */
    StorageNode val_node = node.add_node ("values");
    val_node.set_prop ("min",       widget->min_value);
    val_node.set_prop ("max",       widget->max_value);
    val_node.set_prop ("page_size", widget->page_size);
    val_node.set_prop ("value",     widget->value);
}
