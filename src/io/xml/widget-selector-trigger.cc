//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/xml/widget-selector-trigger.h"

#include "io/storage-processor.h"
#include "io/storage-node-handlers.h"

#include "io/xml/widget-io-util.h"

using namespace Guikachu::IO::XML;

SelectorTriggerIOAdaptor::SelectorTriggerIOAdaptor (Widgets::SelectorTrigger *widget_):
    widget (widget_)
{
}

void SelectorTriggerIOAdaptor::load (const StorageNode &node)
{
    StorageProcessor processor;
    processor.add_handler ("pos",            new SizePosHandler (widget));
    processor.add_handler ("text",           new TextHandler (widget));
    processor.add_handler ("image",          new StorageNodeHandlers::StringProp ("id", widget->bitmap_id));
    processor.add_handler ("selected_image", new StorageNodeHandlers::StringProp ("id", widget->selected_bitmap_id));
    processor.add_handler ("usable",         new StorageNodeHandlers::Flag (widget->usable));
    processor.add_handler ("disabled",       new StorageNodeHandlers::Flag (widget->disabled));
    processor.add_handler ("anchor_right",   new StorageNodeHandlers::Flag (widget->anchor_right));

    processor.run (node);
}

void SelectorTriggerIOAdaptor::save (StorageNode &node)
{
    save_widget_pos_size (widget, node);
    
    /* Usable */
    if (widget->usable)
	node.add_node ("usable");
    if (widget->disabled)
	node.add_node ("disabled");

    /* Anchor_right */
    if (widget->anchor_right)
	node.add_node ("anchor_right");
    
    /* Label and font */
    save_widget_text (widget, node);
    
    /* Pictures */
    if (widget->bitmap_id != "")
    {
	StorageNode bitmap_node = node.add_node ("image");
	bitmap_node.set_prop ("id", widget->bitmap_id);
    }

    if (widget->selected_bitmap_id != "")
    {
	StorageNode bitmap_node = node.add_node ("selected_image");
	bitmap_node.set_prop ("id", widget->bitmap_id);
    }	
}
