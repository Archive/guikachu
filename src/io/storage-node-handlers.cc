//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "storage-node-handlers.h"

namespace Guikachu
{
namespace StorageNodeHandlers
{

Flag::Flag (Property<bool> &target_) :
    target (target_)
{
    target = false;
}
    
void Flag::operator() (const StorageNode &node)
{
    target = true;
}



StringContents::StringContents (Property<std::string> &target_) :
    target (target_)
{
}

void StringContents::operator() (const StorageNode &node)
{
    target = node.get_content ();
}


IntProp::IntProp (const std::string &prop_name_, Property<int> &target_) :
    prop_name (prop_name_),
    target (target_)
{
}
	
void IntProp::operator() (const StorageNode &node)
{
    target = node.get_prop_int (prop_name);
}



StringProp::StringProp (const std::string &prop_name_, Property<std::string> &target_) :
    prop_name (prop_name_),
    target (target_)
{
}
	
void StringProp::operator() (const StorageNode &node)
{
    target = node.get_prop_string (prop_name);
}




StringListProp::StringListProp (const std::string &child_name_,
				const std::string &prop_name_,
				Property<list_t>  &target_) :
    child_name (child_name_),
    prop_name (prop_name_),
    target (target_)
{
}

StringListProp::~StringListProp ()
{
    target = temp_list;
}

void StringListProp::operator () (const StorageNode &node)
{
    class ListItemHandler: public HandlerBase
    {
	const std::string prop_name;
	list_t &container;
    public:
	ListItemHandler (const std::string &prop_name_, list_t &container_) :
	    prop_name (prop_name_),
	    container (container_)
	    {};

	void operator() (const StorageNode &node)
	    {
		container.push_back (node.get_prop_string (prop_name));
	    };
    };

    StorageProcessor children_processor;
    children_processor.add_handler (child_name, new ListItemHandler (prop_name, temp_list));
    children_processor.run (node);
}



StringListContents::StringListContents (const std::string &child_name_,
					Property<list_t>  &target_) :
    child_name (child_name_),
    target (target_)
{
}

StringListContents::~StringListContents ()
{
    target = temp_list;
}

void StringListContents::operator () (const StorageNode &node)
{
    class ListItemHandler: public HandlerBase
    {
	list_t &container;
    public:
	ListItemHandler (list_t &container_) :
	    container (container_)
	    {};
	
	void operator() (const StorageNode &node)
	    {
		container.push_back (node.get_content ());
	    };
    };
    
    StorageProcessor children_processor;
    children_processor.add_handler (child_name, new ListItemHandler (temp_list));
    children_processor.run (node);
}

    
} // namespace StorageNodeHandlers
} // namespace Guikachu
