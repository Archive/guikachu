//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "config.h"

#ifdef GUIKACHU_HAVE_GNOMEVFS
#include "io-vfs.cc"
#else
#include "io-posix.cc"
#endif


IO::IOFactory * IO::IOFactory::instance_ = 0;

namespace
{
    template<class T>
    IO::Loader * loader_factory ()
    {
	return new T;
    }

    template<class T>
    IO::Saver * saver_factory ()
    {
	return new T;
    }
    
} // anonymous namespace

IO::IOFactory::IOFactory ()
{
    loader_map["application/x-guikachu"] = loader_factory<IO::XMLLoader>;
    loader_map["text/x-rcp"] = loader_factory<IO::RCPLoader>;

    saver_map["application/x-guikachu"] = saver_factory<IO::XMLSaver>;
    saver_map["text/x-rcp"] = saver_factory<IO::RCPSaver>;
}

IO::IOFactory * IO::IOFactory::instance ()
{
    if (!instance_)
	instance_ = new IOFactory;
	
    return instance_;
}

IO::Loader * IO::IOFactory::create_loader (const std::string &mime_type) const
{
    loader_map_t::const_iterator found = loader_map.find (mime_type);
    if (found == loader_map.end ())
	return 0;

    return found->second ();
}

IO::Saver * IO::IOFactory::create_saver (const std::string &mime_type) const
{
    saver_map_t::const_iterator found = saver_map.find (mime_type);
    if (found == saver_map.end ())
	return 0;

    return found->second ();
}
