//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_IO_XML_LOADER_H
#define GUIKACHU_IO_XML_LOADER_H

#include "io/guikachu-io.h"
#include "storage-node.h"
#include "resource-visitor.h"
#include "form-editor/widget-visitor.h"

#include <sigc++/trackable.h>

namespace Guikachu
{
    namespace IO
    {
	class XMLLoader: public sigc::trackable,
			 public Loader
	{
	    bool encoding_fixed;
	    void encoding_fixed_cb ();
	    void encoding_warning ();
	    
	public:
	    void load (ResourceManager *manager, const Glib::ustring &uri) throw (Glib::Exception);
	};

	namespace XML
	{
	    class ResourceLoader: public ResourceVisitor
	    {
		const StorageNode &node;
                Glib::ustring      src_hint;
		
	    public:
		explicit ResourceLoader (const StorageNode &node, const Glib::ustring &src_hint = Glib::ustring ());
	    
		void visit_resource (Resources::Dialog       *res);
		void visit_resource (Resources::String       *res);
		void visit_resource (Resources::StringList   *res);
		void visit_resource (Resources::Menu         *res);
		void visit_resource (Resources::Form         *res);
		void visit_resource (Resources::Blob         *res);
                void visit_resource (Resources::Bitmap       *res);
                void visit_resource (Resources::BitmapFamily *res);
	    };
	    
	    class WidgetLoader: public WidgetVisitor
	    {
		const StorageNode &node;
                Glib::ustring      src_hint;
                
	    public:
		WidgetLoader (const StorageNode &node, const Glib::ustring &src_hint = Glib::ustring ());
		
		void visit_widget (Widgets::Label           *widget);
		void visit_widget (Widgets::Button          *widget);
		void visit_widget (Widgets::PushButton      *widget);
		void visit_widget (Widgets::Graffiti        *widget);
		void visit_widget (Widgets::SelectorTrigger *widget);
		void visit_widget (Widgets::Checkbox        *widget);
		void visit_widget (Widgets::List            *widget);
		void visit_widget (Widgets::PopupTrigger    *widget);
		void visit_widget (Widgets::ScrollBar       *widget);
		void visit_widget (Widgets::Slider          *widget);
		void visit_widget (Widgets::TextField       *widget);
		void visit_widget (Widgets::Table           *widget);
		void visit_widget (Widgets::FormBitmap      *widget);
		void visit_widget (Widgets::Gadget          *widget);
	    };
	}	
    }
}

#endif /* !GUIKACHU_IO_GUIKACHU_LOAD_H */
