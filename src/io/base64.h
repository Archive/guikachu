//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_IO_BASE64_H
#define GUIKACHU_IO_BASE64_H

#include <string>

namespace Guikachu
{
    namespace IO
    {
	namespace Base64
	{
	    typedef char          encoded_char_t;
	    typedef signed char   encoded_byte_t;
	    typedef unsigned char decoded_byte_t;

	    typedef std::basic_string<encoded_char_t>  encoded_string_t;
	    typedef decoded_byte_t                    *decoded_stream_t;
	    
	    void decode (const encoded_string_t &encoded,
			 decoded_stream_t &decoded_data, size_t &decoded_size);
	    
	    encoded_string_t encode (decoded_stream_t orig_data, size_t orig_size);
	}
    }
}

#endif /* !GUIKACHU_IO_BASE64_H */
