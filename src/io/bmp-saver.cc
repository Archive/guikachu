//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/bmp-saver.h"

using namespace Guikachu;

namespace
{
    void write_short (unsigned char *data, int val)
    {
        data[0] = val & 0xFF;
        data[1] = (val & 0xFF00) >> 8;
    }

    void write_int (unsigned char *data, int val)
    {
        write_short (data, val);
        write_short (data + 2, (val &0xFFFF0000) >> 16);
    }
    
} // anonymous namespace

void IO::save_bmp (const Glib::RefPtr<Gdk::Pixbuf> &pixbuf,
                   unsigned char *&data, filesize_t &len) throw (Glib::Exception)
{
    static const int filehdr_size = 14;
    static const int infohdr_size = 40;

    const int h = pixbuf->get_height ();
    const int w = pixbuf->get_width ();
    const int rowstride = ((((w * 3) + 3) / 4) * 4);
    const int sz = h * rowstride;

    len = filehdr_size + infohdr_size + sz;
    data = new unsigned char[len];

    // Fill FILE header
    data[0] = 'B';
    data[1] = 'M';
    write_int (data + 2, len);
    write_int (data + 6, 0);
    write_int (data + 10, filehdr_size + infohdr_size);

    // Fill INFO header
    write_int (data + 14, infohdr_size);
    write_int (data + 18, w);
    write_int (data + 22, h);
    write_short (data + 26, 1);
    write_short (data + 28, 24);
    write_int (data + 30, 0);
    write_int (data + 34, sz);
    write_int (data + 38, 0x1000);
    write_int (data + 42, 0x1000);
    write_int (data + 46, 0x0000);
    write_int (data + 50, 0xFFFF);

    // Fill pixels
    int bpp = pixbuf->get_has_alpha () ? 4 : 3;
    guint8 *pixels = pixbuf->get_pixels ();
    
    for (int y = 0; y < h; ++y)
    {
        guint8 *pixels_x = pixels;
        
        for (int x = 0; x < w; ++x)
        {
            data[filehdr_size + infohdr_size + (h - y - 1) * rowstride + x * 3 + 0] = pixels_x[2];
            data[filehdr_size + infohdr_size + (h - y - 1) * rowstride + x * 3 + 1] = pixels_x[1];
            data[filehdr_size + infohdr_size + (h - y - 1) * rowstride + x * 3 + 2] = pixels_x[0];
            
            pixels_x += bpp;
        }

        pixels += pixbuf->get_rowstride ();
    }
}
