//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/guikachu-io.h"

#include "io/xml-loader.h"
#include "io/xml-saver.h"
#include "io/rcp-loader.h"
#include "io/rcp-saver.h"

#include <glib.h>
#include <glib/gstdio.h>
#include <glibmm/miscutils.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

using namespace Guikachu;

IO::Exception::Exception ():
    stored_errno (EIO)
{
}

IO::Exception::Exception (int stored_errno_):
    stored_errno (stored_errno_)
{
}

Glib::ustring IO::Exception::what() const
{
    return strerror (stored_errno);
}


namespace {
    class FDHolder
    {
    public:
        const int fd;

        explicit FDHolder (int fd);
        ~FDHolder ();
    };
} // anonymous namespace

FDHolder::FDHolder (int fd_):
    fd (fd_)
{
    if (fd < 0)
        throw IO::Exception (errno);
}

FDHolder::~FDHolder ()
{
    if (fd > 0)
        close (fd);
}

void IO::init ()
{
    // Do nothing
}

Glib::ustring IO::create_canonical_uri (const Glib::ustring &filename, const Glib::ustring &default_ext)
{
    if (filename == "")
	return "";

    Glib::ustring full_filename = filename;
    
    if (filename.substr (0, strlen ("file://localhost/")) == "file://localhost/")
    {
#ifdef _WIN32
        full_filename = filename.substr (strlen ("file://localhost/"), std::string::npos);
#else
        full_filename = filename.substr (strlen ("file://localhost/") - 1, std::string::npos);
#endif
    } else if (filename.substr (0, strlen ("file:///")) == "file:///") {
#ifdef _WIN32
        full_filename = filename.substr (strlen ("file:///"), std::string::npos);
#else
        full_filename = filename.substr (strlen ("file:///") - 1, std::string::npos);
#endif
    }

    std::string::size_type dot_idx = full_filename.rfind (".");
    if ((dot_idx == std::string::npos || dot_idx < full_filename.rfind ("/")) && default_ext != "") // No extension found
        full_filename += "." + default_ext;
    
    if (!Glib::path_is_absolute (full_filename))
	full_filename = Glib::build_filename (Glib::get_current_dir (), full_filename);

    return full_filename;
}

Glib::ustring IO::get_mime_type (const Glib::ustring &uri) throw (Glib::Exception)
{
#define HAS_EXT(s,ext) (s).rfind (ext) == (s).length () - strlen (ext)

    if (HAS_EXT (uri, ".guikachu"))
        return "application/x-guikachu";
    
    if (HAS_EXT (uri, ".rcp"))
        return "text/x-rcp";

    return "unknown/unknown";
}

void IO::load_uri (const Glib::ustring &uri, unsigned char *&data, filesize_t &len) throw (Glib::Exception)
{
    FDHolder f (g_open (uri.c_str (), O_RDONLY, 0));

    struct stat fs_info;
    if (fstat (f.fd, &fs_info) < 0)
        throw IO::Exception (errno);
    
    data = new unsigned char[fs_info.st_size];
    len = 0;
    unsigned char buffer[10240];
    filesize_t    bytes_read;
    
    do {
        bytes_read = read (f.fd, buffer, sizeof buffer - 1);
        if (bytes_read < 0)
            throw IO::Exception (errno);            
        
        if (bytes_read)            
            memcpy (data + len, buffer, bytes_read);
	len += bytes_read;
    } while (bytes_read);
}

void IO::save_uri (const Glib::ustring &uri, const unsigned char *data, filesize_t len) throw (Glib::Exception)
{
    // Create file
    FDHolder f (g_creat (uri.c_str (), 0644));
    
    // Write data to file
    const filesize_t chunk_size = len;
    
    for (filesize_t bytes_written = 0; len; data += bytes_written)
    {
        bytes_written = write (f.fd, data, std::min (chunk_size, len));
        if (bytes_written < 0)
            throw IO::Exception (errno);

        len -= bytes_written;
    }
}

bool IO::uri_exists (const Glib::ustring &uri)
{
    struct stat fs_info;
    if (g_stat (uri.c_str (), &fs_info) < 0)
        return false;

    return true;
}
