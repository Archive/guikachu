//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <glib/gi18n.h>

#include <glibmm/utility.h>

#include "rcp-loader.h"
#include "rcp/parser-interface.h"

#include "ui.h"

using namespace Guikachu;
using namespace Guikachu::IO;

namespace
{
    void show_error (const Glib::ustring &uri, const Glib::ustring &error_msg)
    {
        Glib::ScopedPtr<char> buffer (g_strdup_printf (_("<big><b>Error while loading \"%s\"</b></big>\n"
                                                         "%s"),
                                                       UI::visible_filename (uri).c_str (),
                                                       error_msg.c_str ()));
	UI::show_error (buffer.get ());
    }
}

void RCPLoader::load (ResourceManager *manager, const Glib::ustring &uri) throw (Glib::Exception)
{
    // Load data from URI
    unsigned char  *data = 0;
    IO::filesize_t  len;

    try {
        IO::load_uri (uri, data, len);
    } catch (...)
    {
        delete[] data;
        throw;
    }

    // Start the lexer
    yy_buffer_state* lex_buffer = yy_scan_bytes ((char*)data, len);
    delete[] data;

    // ...and the parser
    IO::RCP::setup (manager, uri);
    yyparse ();
    yy_delete_buffer (lex_buffer);
    
    if (IO::RCP::error != "")
	show_error (uri, IO::RCP::error);
}
