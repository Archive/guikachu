//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_STORAGE_PROCESSOR_H
#define GUIKACHU_STORAGE_PROCESSOR_H

#include "storage-node.h"

#include <map>

namespace Guikachu
{
    class StorageProcessor
    {
    public:
	class NodeHandler
	{
	public:
	    virtual ~NodeHandler () {};
	    virtual void operator() (const StorageNode &node) = 0;
	};

    private:
	typedef std::map<std::string, NodeHandler*> handler_map_t;
	handler_map_t handler_map;
	
    public:
	~StorageProcessor ();
	
	void add_handler (const std::string &node_name, NodeHandler *handler);
	void run (const StorageNode &root);

    private:
	NodeHandler* get_handler (const StorageNode &node);
    };
}

#endif /* !GUIKACHU_STORAGE_PROCESSOR_H */
