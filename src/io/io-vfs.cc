//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/guikachu-io.h"

#include "io/xml-loader.h"
#include "io/xml-saver.h"
#include "io/rcp-loader.h"
#include "io/rcp-saver.h"

#include <libgnomevfsmm/init.h>
#include <libgnomevfsmm/handle.h>
#include <libgnomevfsmm/uri.h>
#include <libgnomevfsmm/utils.h>

using namespace Guikachu;

void IO::init ()
{
    Gnome::Vfs::init ();
}

Glib::ustring IO::create_canonical_uri (const Glib::ustring &filename, const Glib::ustring &default_ext)
{
    if (filename == "")
	return "";

    Glib::ustring full_filename = filename;
    std::string::size_type dot_idx = full_filename.rfind (".");
    if ((dot_idx == std::string::npos || dot_idx < full_filename.rfind ("/")) && default_ext != "") // No extension found
        full_filename += "." + default_ext;

    if (full_filename.find ("://") < full_filename.find ("/")) // Don't touch URI's
	return full_filename;
    
    if (!Glib::path_is_absolute (filename))
	full_filename = Glib::build_filename (Glib::get_current_dir (), full_filename);
    
    return Gnome::Vfs::get_uri_from_local_path (Gnome::Vfs::make_uri_canonical (full_filename));
}

Glib::ustring IO::get_mime_type (const Glib::ustring &uri) throw (Glib::Exception)
{
    Glib::RefPtr<Gnome::Vfs::Uri> vfs_uri = Gnome::Vfs::Uri::create (uri);
    Glib::RefPtr<Gnome::Vfs::FileInfo> file_info = vfs_uri->get_file_info (Gnome::Vfs::FILE_INFO_GET_MIME_TYPE);
    return file_info->get_mime_type ();
}

void IO::load_uri (const Glib::ustring &uri, unsigned char *&data, filesize_t &len) throw (Glib::Exception)
{
    Gnome::Vfs::Handle f;
    f.open (uri, Gnome::Vfs::OPEN_READ);

    Glib::RefPtr<Gnome::Vfs::FileInfo> vfs_info = f.get_file_info (Gnome::Vfs::FILE_INFO_FOLLOW_LINKS);
        
    data = new unsigned char[vfs_info->get_size ()];
    len = 0;        
    unsigned char buffer[10240];
    filesize_t    bytes_read;

    do {
        bytes_read = f.read (buffer, sizeof buffer - 1);
        if (bytes_read)
            memcpy (data + len, buffer, bytes_read);
	len += bytes_read;
    } while (bytes_read);
}

void IO::save_uri (const Glib::ustring &uri, const unsigned char *data, filesize_t len) throw (Glib::Exception)
{
    // Create file
    Gnome::Vfs::Handle f;
    f.create (uri, Gnome::Vfs::OPEN_WRITE, 0, 0644);
	
    // Write data to VFS stream
    const filesize_t chunk_size = len;
    
    for (filesize_t bytes_written = 0; len; data += bytes_written)
    {
        bytes_written = f.write (data, std::min (chunk_size, len));
        len -= bytes_written;
    }
}

bool IO::uri_exists (const Glib::ustring &uri)
{
    try {
	Glib::RefPtr<Gnome::Vfs::FileInfo> fileinfo = Gnome::Vfs::Handle::get_file_info (uri);
	return true;
    } catch (...) {
    }

    return false;
}
