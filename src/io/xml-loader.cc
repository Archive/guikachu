//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/xml-loader.h"

#include <glib/gi18n.h>

#include <libxml/parser.h>

#include "storage.h"
#include "storage-node.h"
#include "storage-processor.h"
#include "storage-node-handlers.h"

#include "ui.h"

#include "resource-util.h"

using namespace Guikachu;

namespace Guikachu
{
    
namespace IO
{
    
namespace XML
{
    namespace
    {
        void load_app      (Resources::Application *app,     const Glib::ustring &uri, const StorageNode &node);
        void load_target   (Target                 *target,  const Glib::ustring &uri, const StorageNode &node);
        void load_resource (ResourceManager        *manager, const Glib::ustring &uri, StorageNode &node);
    }
}


void XMLLoader::encoding_fixed_cb ()
{
    encoding_fixed = true;
}


void XMLLoader::load (ResourceManager *manager, const Glib::ustring &uri) throw (Glib::Exception)
{
    Storage storage;
    
    storage.load_uri (uri);
    
    StorageNode root_node = storage.get_root ();
    if (root_node.name () != "guikachu")
    {
#ifdef GUIKACHU_HAVE_GNOMEVFS
        throw IO::Exception (Gnome::Vfs::ERROR_WRONG_FORMAT);
#else
        throw IO::Exception ();
#endif
    }

    // Reset context
    encoding_fixed = false;

    // Set up callbacks
    storage.encoding_notify.connect (sigc::mem_fun (*this, &XMLLoader::encoding_fixed_cb));

    manager->block_dirty ();
    
    // Walk the tree
    for (StorageNode curr_node = root_node.children ();
         curr_node; curr_node++)
    {
        if (curr_node.name () == "application")
            XML::load_app (manager->get_application (), uri, curr_node);
        
        else if (curr_node.name () == "target")
            XML::load_target (manager->get_target (), uri, curr_node);
        
        else
            XML::load_resource (manager, uri, curr_node);
    }

    manager->unblock_dirty ();
    
    // Show warning if 8-bit input was encountered
    if (encoding_fixed)
    {
        encoding_warning ();
	manager->set_dirty ();
    }
}

void XMLLoader::encoding_warning ()
{
    UI::show_warning (
	_("Previous releases of Guikachu always stored\n"
	  "strings in the local encoding, while PalmOS\n"
	  "uses the Latin-1 character  set. The encoding\n"
	  "of old Guikachu files can not be determined,\n"
	  "so all non-ASCII characters have been\n"
	  "automatically converted to escape sequences.\n"
	  "\n"
	  "Please review the results of the automatic\n"
	  "conversion"),
	_("Input file contains non-ASCII characters"));
}




namespace XML
{

namespace
{
    
void load_app (Resources::Application *app, const Glib::ustring &uri, const StorageNode &node)
{
    app->iconname = "";
    app->version =  "";
    app->vendor =   "";

    class TagHandler: public StorageProcessor::NodeHandler
    {
        Resources::Application::tag_map_t &tags;
    public:
	TagHandler (Resources::Application::tag_map_t &tags_) :
	    tags (tags_)
	    {};
	
	void operator() (const StorageNode &node)
	    {
                for (StorageNode i = node.children (); i; ++i)
                    if (i.name () == "tag")
                        tags[i.get_prop_string ("name")] = i.get_prop_string ("val");
	    };
    };
    Resources::Application::tag_map_t tags;
    
    StorageProcessor processor;
    processor.add_handler ("iconname", new StorageNodeHandlers::StringContents (app->iconname));
    processor.add_handler ("version",  new StorageNodeHandlers::StringContents (app->version));
    processor.add_handler ("vendor",   new StorageNodeHandlers::StringContents (app->vendor));
    processor.add_handler ("tags",     new TagHandler (tags));

    processor.run (node);
    app->add_tags (tags);
}

void load_target (Target *target, const Glib::ustring &uri, const StorageNode &node)
{
    // Is this a stock target?
    std::string stock_id = node.get_prop_string ("stock_id");
    if (stock_id != "")
    {
        target->load_stock (stock_id);
        return;
    }
    
    for (StorageNode curr_node = node.children (); curr_node; curr_node++)
    {
        if (curr_node.name () == "screen")
        {
            target->screen_width  = curr_node.get_prop_int ("width");
            target->screen_height = curr_node.get_prop_int ("height");
            target->screen_color  = curr_node.get_prop_bool ("color");
        } else {
            g_warning ("Error parsing custom target definition: "
                       "unexpected element `%s'",
                       curr_node.name ().c_str ());
        }
    }
}

void load_resource (ResourceManager *manager, const Glib::ustring &uri, StorageNode &node)
{
    std::string node_name = node.name ();
    std::string id = node.get_prop_string ("id");

    if (id == "")
    {
        g_warning ("`%s': Missing ID field", node_name.c_str ());
        return;
    }

    Resources::Type type = Resources::type_from_type_id (node_name);
    
    // try_alternate_names is TRUE to make cut & paste implementable
    // through the IO system
    Resource *res = manager->create_resource (type, id, true);
    if (!res)
    {
        g_warning ("Unknown resource type `%s'", node_name.c_str ());
        return;
    }
    
    ResourceLoader loader (node, uri);
    res->apply_visitor (loader);
}

} // anonymous namespace    
} // namespace XML
} // namespace IO
} // namespace Guikachu
