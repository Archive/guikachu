//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_IO_H
#define GUIKACHU_IO_H

#include "resource-manager.h"

#ifdef GUIKACHU_HAVE_GNOMEVFS
#include <libgnomevfsmm/types.h>
#endif

#include <glibmm/exception.h>

#include "config.h"
#include "io/io-types.h"

namespace Guikachu
{
    namespace IO
    {
        void init ();
        
	Glib::ustring create_canonical_uri (const Glib::ustring &filename, const Glib::ustring &default_ext = "");
        Glib::ustring get_mime_type (const Glib::ustring &uri) throw (Glib::Exception);
        
	void load_uri (const Glib::ustring &uri, unsigned char *&data, filesize_t &len) throw (Glib::Exception);
	void save_uri (const Glib::ustring &uri, const unsigned char *data, filesize_t len) throw (Glib::Exception);

	bool uri_exists (const Glib::ustring &uri);
	
	class Loader
	{
	public:
	    virtual ~Loader () {};
	    
	    virtual void load (ResourceManager *manager, const Glib::ustring &uri) throw (Glib::Exception) = 0;
	};
	
	class Saver
	{
	public:
	    virtual ~Saver () {};
	    
	    virtual void save (ResourceManager *manager, const Glib::ustring &uri) throw (Glib::Exception) = 0;
	};

	class IOFactory
	{
	    typedef Loader * (*loader_factory_fn) ();
	    typedef Saver *  (*saver_factory_fn) ();

	    typedef std::map<std::string, loader_factory_fn> loader_map_t;
	    typedef std::map<std::string, saver_factory_fn> saver_map_t;

	    loader_map_t loader_map;
	    saver_map_t  saver_map;
	    
	private:
	    IOFactory ();
	    static IOFactory *instance_;
	    
	public:
	    static IOFactory *instance ();
	    
	    Loader * create_loader (const std::string &mime_type) const;
	    Saver  * create_saver  (const std::string &mime_type) const;
	};
    }
}

#endif /* !GUIKACHU_IO_H */
