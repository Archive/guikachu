//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_IO_RCP_SAVER_PRIV_H
#define GUIKACHU_IO_RCP_SAVER_PRIV_H

#include "property.h"

namespace Guikachu
{
    namespace IO
    {
        namespace RCP
        {
            std::string string_to_rcp (const std::string &src);
            
            template<class T>
            inline std::ostream & operator<< (std::ostream &ostr, const Guikachu::Property<T> &prop)
            {
                ostr << prop.get_val ();
                return ostr;
            }
            
            template<>
            inline std::ostream & operator<< (std::ostream &ostr, const Guikachu::Property<std::string> &prop)
            {
                ostr << string_to_rcp (prop.get_val ());
                return ostr;
            }
	}
    }
}

#endif /* !GUIKACHU_IO_RCP_SAVER_PRIV_H */
