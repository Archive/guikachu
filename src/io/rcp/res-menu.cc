//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "parser-callbacks.h"
#include "parser.h"

#include <iostream>

#include "menu-res.h"

using namespace Guikachu;
using namespace Guikachu::IO;

namespace {
    Resources::Menu *menu;
    
    Resources::Menu::MenuTree submenus;
}

void RCP::Menu::import (ResourceManager   *manager,
			const std::string &id)
{
    Resource *res = manager->create_resource (Resources::RESOURCE_MENU, id);
    menu = dynamic_cast<Resources::Menu*> (res);
}

void RCP::Menu::import_end ()
{
    menu->set_submenus (submenus);
}

void RCP::Menu::import_menu (const std::string &menu_label)
{
    Resources::Menu::Submenu submenu;
    submenu.label = menu_label;

    submenus.push_back (submenu);
}

void RCP::Menu::import_menuitem (const std::string &id, const std::string &label,
				 const std::string &shortcut)
{
    Resources::Menu::Submenu &submenu = submenus.back ();
    Resources::Menu::MenuItem item;

    item.separator = false;
    item.id = id;
    item.label = label;

    if (shortcut != "")
	item.shortcut = shortcut[0];

    submenu.items.push_back (item);
}

void RCP::Menu::import_separator ()
{
    Resources::Menu::Submenu &submenu = submenus.back ();
    Resources::Menu::MenuItem item;

    item.separator = true;
    submenu.items.push_back (item);
}
