//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "expr.h"

using namespace Guikachu::IO::RCP::Expr;

Literal::Literal (value_t value_):
    value (value_)
{
}

value_t Literal::eval (Widget *widget, Widget *previous_widget) const
{
    return value;
}
                
value_t Functions::add (value_t left, value_t right) { return left + right; };
value_t Functions::sub (value_t left, value_t right) { return left - right; };
value_t Functions::mul (value_t left, value_t right) { return left * right; };
value_t Functions::div (value_t left, value_t right) { return left / right; };

BinFunction::BinFunction (fun_t fun_, Expr *left_, Expr *right_):
    fun (fun_), left (left_), right (right_)
{
}
                    
value_t BinFunction::eval (Widget *widget, Widget *previous_widget) const
{
    return fun (left->eval (widget, previous_widget),
                right->eval (widget, previous_widget));
}



value_t Relative::PrevLeft::eval (Widget *widget, Widget *previous_widget) const
{
    if (!previous_widget) return 0;
    
    return previous_widget->x;
}

value_t Relative::PrevRight::eval (Widget *widget, Widget *previous_widget) const
{
    if (!previous_widget) return 0;
    
    return previous_widget->x + previous_widget->get_width ();
}

value_t Relative::PrevWidth::eval (Widget *widget, Widget *previous_widget) const
{
    if (!previous_widget) return 0;
    
    return previous_widget->get_width ();
}

value_t Relative::PrevTop::eval (Widget *widget, Widget *previous_widget) const
{
    if (!previous_widget) return 0;
    
    return previous_widget->y;
}

value_t Relative::PrevBottom::eval (Widget *widget, Widget *previous_widget) const
{
    if (!previous_widget) return 0;
    
    return previous_widget->y + previous_widget->get_height ();
}

value_t Relative::PrevHeight::eval (Widget *widget, Widget *previous_widget) const
{
    if (!previous_widget) return 0;
    
    return previous_widget->get_height ();
}


CenterHoriz::CenterHoriz (Expr *center_):
    center (center_)
{
}

value_t CenterHoriz::eval (Widget *widget, Widget *previous_widget) const
{
    value_t center_pos = center.get () ? center->eval (widget, previous_widget) : (widget->get_form ()->width / 2);

    return center_pos - (widget->get_width () / 2);
}

CenterVert::CenterVert (Expr *center_):
    center (center_)
{
}
        
value_t CenterVert::eval (Widget *widget, Widget *previous_widget) const
{
    value_t center_pos = center.get () ? center->eval (widget, previous_widget) : widget->get_form ()->height / 2;
    
    return center_pos - widget->get_height () / 2;
}

Bottom::Bottom (Expr *bottom_):
    bottom (bottom_)
{
}
        
value_t Bottom::eval (Widget *widget, Widget *previous_widget) const
{
    value_t bottom_pos = bottom.get () ? bottom->eval (widget, previous_widget) : widget->get_form ()->height;
    
    return bottom_pos - widget->get_height ();
}

Right::Right (Expr *right_):
    right (right_)
{
}
        
value_t Right::eval (Widget *widget, Widget *previous_widget) const
{
    value_t right_pos = right.get () ? right->eval (widget, previous_widget) : widget->get_form ()->width;
    
    return right_pos - widget->get_width ();
}
