//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_IO_RCP_EXPR_H
#define GUIKACHU_IO_RCP_EXPR_H

#include <memory>
#include "form-editor/widget.h"

namespace Guikachu
{
    namespace IO
    {
        namespace RCP
        {
            namespace Expr
            {
                typedef int value_t;
                
                class Expr
                {
                public:
                    virtual ~Expr () {};
                    virtual value_t eval (Widget *widget, Widget *previous_widget) const = 0;
                };
                
                class Literal: public Expr
                {
                    value_t value;
                public:
                    Literal (value_t value);
                    value_t eval (Widget *widget, Widget *previous_widget) const;
                };
                
                namespace Functions
                {
                    value_t add (value_t left, value_t right);
                    value_t sub (value_t left, value_t right);
                    value_t mul (value_t left, value_t right);
                    value_t div (value_t left, value_t right);
                }
                
                class BinFunction: public Expr
                {
                public:
                    typedef value_t (*fun_t) (value_t, value_t);
                    
                private:
                    fun_t               fun;
                    std::auto_ptr<Expr> left;
                    std::auto_ptr<Expr> right;
                    
                public:
                    BinFunction (fun_t fun, Expr *left, Expr *right);
                    
                    value_t eval (Widget *widget, Widget *previous_widget) const;
                };

                namespace Relative
                {
                    class PrevLeft: public Expr
                    {
                    public:
                        PrevLeft () {};

                        value_t eval (Widget *widget, Widget *previous_widget) const;
                    };

                    class PrevRight: public Expr
                    {
                    public:
                        PrevRight () {};

                        value_t eval (Widget *widget, Widget *previous_widget) const;
                    };

                    class PrevWidth: public Expr
                    {
                    public:
                        PrevWidth () {};

                        value_t eval (Widget *widget, Widget *previous_widget) const;
                    };

                    class PrevTop: public Expr
                    {
                    public:
                        PrevTop () {};

                        value_t eval (Widget *widget, Widget *previous_widget) const;
                    };

                    class PrevBottom: public Expr
                    {
                    public:
                        PrevBottom () {};

                        value_t eval (Widget *widget, Widget *previous_widget) const;
                    };

                    class PrevHeight: public Expr
                    {
                    public:
                        PrevHeight () {};

                        value_t eval (Widget *widget, Widget *previous_widget) const;
                    };
                }

                class CenterHoriz: public Expr
                {
                    std::auto_ptr<Expr> center;
                public:
                    CenterHoriz () {};
                    CenterHoriz (Expr *center);

                    value_t eval (Widget *widget, Widget *previous_widget) const;
                };

                class CenterVert: public Expr
                {
                    std::auto_ptr<Expr> center;
                public:
                    CenterVert () {};
                    CenterVert (Expr *center);

                    value_t eval (Widget *widget, Widget *previous_widget) const;
                };

                class Bottom: public Expr
                {
                    std::auto_ptr<Expr> bottom;
                public:
                    Bottom () {};
                    Bottom (Expr *bottom);

                    value_t eval (Widget *widget, Widget *previous_widget) const;
                };

                class Right: public Expr
                {
                    std::auto_ptr<Expr> right;
                public:
                    Right () {};
                    Right (Expr *right);

                    value_t eval (Widget *widget, Widget *previous_widget) const;
                };
            }
        }
    }
}

#endif /* !GUIKACHU_IO_RCP_EXPR_H */
