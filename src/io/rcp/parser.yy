//$Id: parser.yy,v 1.18 2006/11/04 14:17:59 cactus Exp $ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

%{

#define YYERROR_VERBOSE

#include "parser-interface.h"
#include "expr.h"
    
#include <glib/gi18n.h>
    
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#include "io/guikachu-io.h"
    
#include "resource-manager.h"

#include "string-res.h"
#include "stringlist-res.h"
#include "dialog-res.h"
#include "form-res.h"
#include "menu-res.h"
#include "bitmap-res.h"
#include "bitmapfamily-res.h"

#include "form-editor/label.h"
#include "form-editor/checkbox.h"
#include "form-editor/button.h"    
#include "form-editor/pushbutton.h"
#include "form-editor/selector-trigger.h"
#include "form-editor/popup-trigger.h"
#include "form-editor/graffiti.h"
#include "form-editor/text-field.h"
#include "form-editor/list.h"
#include "form-editor/table.h"
#include "form-editor/scrollbar.h"
#include "form-editor/slider.h"
#include "form-editor/formbitmap.h"

#ifdef GUIKACHU_HAVE_GNOMEVFS
#include <libgnomevfsmm/uri.h>
#endif
    
    extern int yylex();
    extern int lexer_lineno;
    extern char *yytext;

    extern struct yy_buffer_state* yy_scan_bytes (const char *bytes, int len);
    extern void yypop_buffer_state ();
    
    using namespace Guikachu;
    using namespace Guikachu::IO;

    namespace Guikachu
    {
	namespace IO
	{
	    namespace RCP
	    {
		std::string error;
		
		void setup (ResourceManager *manager, const std::string &uri);
		void finish ();
	    }
	}
    }
    
    namespace
    {
	ResourceManager  *manager;
        IO::uri_holder_t  base_uri;
	
	union current_
	{
	    Resources::Dialog       *dialog;
	    Resources::Menu         *menu;
	    Resources::StringList   *stringlist;
	    Resources::Form         *form;
	    Resources::BitmapFamily *bitmap_family;
	} current;

	union current_widget_
	{
	    Widgets::Button          *button;
	    Widgets::Checkbox        *checkbox;
	    Widgets::Label           *label;
	    Widgets::PushButton      *pushbutton;
	    Widgets::SelectorTrigger *strigger;
	    Widgets::PopupTrigger    *ptrigger;
	    Widgets::TextField       *field;
	    Widgets::List            *lst;
	    Widgets::Table           *table;
	    Widgets::ScrollBar       *scrollbar;
            Widgets::FormBitmap      *bitmap;
            Widgets::Slider          *slider;
	} current_widget;

	Widget *previous_widget;
	
	struct current_pos_size_
	{
	    RCP::Expr::Expr *x;
	    RCP::Expr::Expr *y;
	    RCP::Expr::Expr *width;
            bool             width_auto;
	    RCP::Expr::Expr *height;
            bool             height_auto;
	} current_pos_size;

        Resources::Bitmap::BitmapType current_bitmap_type;

	Glib::ustring create_child_uri (const Glib::ustring child)
	{
#ifdef GUIKACHU_HAVE_GNOMEVFS
            IO::uri_holder_t child_uri = base_uri->append_path (child);
            return child_uri->to_string ();
#else
            return Glib::build_filename (base_uri, child);
#endif
	}
	
	namespace StringList
	{
	    std::vector<std::string> strings;

	    void import_string (const std::string &button)
	    {
		strings.push_back (button);
	    }
	    
	    void import_end ()
	    {
		current.stringlist->strings = strings;
		strings.clear ();
	    }
	}

	namespace Dialog
	{
	    void import_button (const std::string &button)
	    {
		std::vector<std::string> buttons = current.dialog->buttons;
		buttons.push_back (button);
		current.dialog->buttons = buttons;
	    }
	}

	namespace BitmapFamily
	{
	    void import_bitmap (const std::string &bitmap_filename,
				int                bitmap_bpp)
	    {
		Glib::ustring bitmap_uri = create_child_uri (bitmap_filename);
		
		Resources::Bitmap::BitmapType type = Resources::Bitmap::TYPE_COLOR_256;
		switch (bitmap_bpp)
		{
		case 1:
		    type = Resources::Bitmap::TYPE_MONO;
		    break;
		case 2:
		    type = Resources::Bitmap::TYPE_GREY_4;
		    break;
		case 4:
		    type = Resources::Bitmap::TYPE_GREY_16;
		    break;
		case 5:
		    type = Resources::Bitmap::TYPE_COLOR_16;
		    break;
		case 8:
		    type = Resources::Bitmap::TYPE_COLOR_256;
		    break;
		case 16:
		    type = Resources::Bitmap::TYPE_COLOR_16K;
		    break;
		default:
		    /* WRITE ME */
		    break;
		}
		
		current.bitmap_family->load_file (type, bitmap_uri);
	    }
	}
	
	namespace Menu
	{
	    Resources::Menu::MenuTree submenus;
	    
	    void import_end ()
	    {
		current.menu->set_submenus (submenus);
		submenus.clear ();
	    }

	    void import_menu (const std::string &menu_label)
	    {
		submenus.push_back (Resources::Menu::Submenu (menu_label));
	    }
	    
	    void import_menuitem (const std::string &id, const std::string &label,
				  const std::string &shortcut = "")
	    {
		Resources::Menu::Submenu &submenu = submenus.back ();
		Resources::Menu::MenuItem item (id, label, shortcut[0]);
		submenu.items.push_back (item);
	    }

	    void import_separator ()
	    {
		Resources::Menu::Submenu &submenu = submenus.back ();
		Resources::Menu::MenuItem item;
		submenu.items.push_back (item);
	    }
	}

	namespace Form
	{
	    typedef std::map<std::string, std::string> popuplist_map_t;
	    popuplist_map_t popuplist_map;
	    
	    void import_popuplist (const std::string &popup_id,
				   const std::string &list_id)
	    {
		popuplist_map[popup_id] = list_id;
	    }
	    
	    void import_end ()
	    {
		for (popuplist_map_t::const_iterator i = popuplist_map.begin ();
		     i != popuplist_map.end (); ++i)
		{
		    Widget *widget = current.form->get_widget (i->first);
		    Widgets::PopupTrigger *ptrigger = dynamic_cast<Widgets::PopupTrigger*> (widget);
		    if (ptrigger)
			ptrigger->list_id = i->second;
		}		
		
		popuplist_map.clear ();
	    }
	}

	namespace List
	{
	    std::vector<std::string> items;

	    void import_item (const std::string &item)
	    {
		items.push_back (item);
	    }

	    void apply_items ()
	    {
		current_widget.lst->items = items;
		items.clear ();
	    }
	}

	namespace Table
	{
	    std::vector<int> cols;

	    void import_col (int col)
	    {
		cols.push_back (col);
	    }

	    void import_end ()
	    {
		current_widget.table->column_width = cols;
		cols.clear ();
	    }
	}
	
	void widget_apply_pos (Widget *widget)
	{
	    widget->x = current_pos_size.x->eval (widget, previous_widget);
	    widget->y = current_pos_size.y->eval (widget, previous_widget);

            delete current_pos_size.x;
            delete current_pos_size.y;
	}
	    
	void widget_apply_pos_size (Widget *widget)
	{
	    Widgets::ResizeableWidget *resizeable =
		dynamic_cast<Widgets::ResizeableWidget*> (widget);

            if (!current_pos_size.width_auto)
		resizeable->width = current_pos_size.width->eval (widget, previous_widget);
            if (!current_pos_size.height_auto)
		resizeable->height = current_pos_size.height->eval (widget, previous_widget);

            delete current_pos_size.width;
            delete current_pos_size.height;

	    widget_apply_pos (widget);
        }
    }
    
    void RCP::setup (ResourceManager *manager_, const Glib::ustring &uri)
    {
	manager = manager_;
	error = "";

	// Store base URI
#ifdef GUIKACHU_HAVE_GNOMEVFS
        IO::uri_holder_t orig_uri = Gnome::Vfs::Uri::create (uri);
	base_uri = orig_uri->get_parent ();
#else
        base_uri = Glib::path_get_dirname (uri);
#endif
    }

    void RCP::finish ()
    {
	yypop_buffer_state ();
    }
    
    /* parse errors */
    void yyerror(const char *s) {
	char *buffer = g_strdup_printf (_("%s at line %d near `%s'"),
					s, lexer_lineno, yytext);
	IO::RCP::error = buffer;
	g_free (buffer);
    }
%}

%union {
    char                          *str;
    int                            num;
    Guikachu::IO::RCP::Expr::Expr *expr;
}


/*---------------------------------------------------------------------------*/
/* bison struct definition */
/*---------------------------------------------------------------------------*/

/* tokens for all RCP reserved words */

%token ID_TOKEN /* NOTE: ID is a class name in Guikachu itself */
%token AUTOID
%token BEG /* NOTE: flex already uses BEGIN, hence BEG which matches 'begin' */
%token END
%token LBRACE
%token RBRACE
%token ATSIGN /* NOTE: AT is a token for "AT" */
%token BACKSLASH

%token HELPID
%token MENUID
%token DEFAULTBTNID

/* Resource types */
%token APPLICATIONICONNAME
%token ALERT
%token MENU
%token STRING
%token STRINGTABLE
%token BITMAP
%token BITMAPFAMILY
%token CATEGORIES

/* Dialog types */
%token DIALOG_CONFIRMATION
%token DIALOG_INFORMATION
%token DIALOG_WARNING
%token DIALOG_ERROR

 /* Widget types */
%token BUTTON
%token POPUPTRIGGER
%token REPEATBUTTON
%token CHECKBOX
%token SELECTORTRIGGER
%token LABEL
%token PUSHBUTTON
%token GRAFFITISTATEINDICATOR
%token NOGRAFFITISTATEINDICATOR
%token FIELD
%token LIST
%token FORMBITMAP
%token GADGET
%token POPUPLIST
%token TABLE
%token SCROLLBAR
%token SLIDER

/* Widget properties */
%token AUTO
%token USABLE NONUSABLE DISABLED
%token LEFTANCHOR RIGHTANCHOR
%token FRAME NOFRAME BOLDFRAME
%token LEFTALIGN RIGHTALIGN
%token NUMERIC
%token AUTOSHIFT
%token HASSCROLLBAR
%token VERTICAL
%token FEEDBACK
%token VISIBLEITEMS
%token SEARCH
%token ROWS
%token COLUMNS
%token COLUMNWIDTHS
%token VALUE MIN_VAL MAX_VAL PAGESIZE
%token GRAPHICAL BITMAPID SELECTEDBITMAPID
%token THUMBID BACKGROUNDID

/* Bitmap types */
%token BITMAPGREY
%token BITMAPGREY16
%token BITMAPCOLOR16
%token BITMAPCOLOR
%token BITMAPCOLOR16K
%token BITMAPFAMILYSPECIAL
%token BITMAPFAMILYEX

/* Bitmap properties */
%token COMPRESS NOCOMPRESS FORCECOMPRESS
%token COLORTABLE NOCOLORTABLE
%token TRANSPARENT TRANSPARENTINDEX
%token DENSITY
%token BPP

%token AT
%token BUTTONS
%token CHECKED
%token DEFAULTBUTTON
%token DYNAMICSIZE
%token EDITABLE
%token FONT
%token FORM
%token GROUP
%token MAXCHARS
%token MENUITEM
%token MESSAGE
%token MODAL
%token MULTIPLELINES
%token NONEDITABLE 
%token NOSAVEBEHIND
%token PULLDOWN
%token SAVEBEHIND
%token SEPARATOR
%token SINGLELINE
%token TITLE
%token UNDERLINED
%token VERSION_TOKEN
%token LOCALE

/* Relative positioning */
%token CENTER RIGHT BOTTOM
%token PREVLEFT PREVRIGHT PREVTOP PREVBOTTOM
%token PREVWIDTH PREVHEIGHT

/* synthetic tokens defined by this parser */
%token <str>  QUOTESTRING
%token <str>  WORD
%type  <str>  stringlit

/* Expression value types */
%type <str> identifier
%type <num> hsize vsize;
%type <num> widget_prop_usable;
%type <num> widget_prop_disabled;
%type <num> widget_prop_font;
%type <num> widget_prop_anchor;
%token <num> NUMBER

/* Infix algebraic expressions */
%token PLUS MINUS TIMES DIV
%left PLUS MINUS
%left TIMES DIV
%left ATSIGN
%type <expr> hexpr hexpr_atom vexpr vexpr_atom
%type <expr> NUMLIT

/* Strings are g_strdup()'d */
%destructor { g_free ($$); } stringlit QUOTESTRING WORD

/* %destructor { delete $$; } NUMLIT */

%start rcpfile

%%

/************************************************************
 * Grammar starts here
 ************************************************************/

/* an rcp file contains zero or more resources */
rcpfile: rcpfile resource | /* empty */;

/* resources appear at the top level of an rcp file */
resource: res_form
        | res_string
        | res_stringlist
        | res_app_ver
        | res_app_name
        | res_dialog
        | res_bitmap
        | res_bitmapfamily
        | res_menu;

/* FUTURE: is use of implied 'ID' deprecated? */
/* identifier matches a bareword or the "ID bareword" */
identifier: WORD | ID_TOKEN WORD { $$ = $2; } | AUTOID { $$ = "_AUTO_"; };

/* FUTURE: localization */
opt_localization: localization | /* empty */;
localization: LOCALE stringlit;

/************************************************************
 * Common widget properties
 ************************************************************/
widget_prop_font: FONT NUMBER { $$ = $2; };
widget_prop_usable: USABLE { $$ = true; } | NONUSABLE { $$ = false; };
widget_prop_disabled: DISABLED { $$ = true; };
widget_prop_anchor: LEFTANCHOR { $$ = false; } | RIGHTANCHOR { $$ = true; };

POSITION: LBRACE hexpr vexpr RBRACE {
    current_pos_size.x = $2;
    current_pos_size.y = $3;
};

POS_SIZE: LBRACE hexpr vexpr hsize vsize RBRACE {
    current_pos_size.x = $2;
    current_pos_size.y = $3;
};

FORM_POS_SIZE: LBRACE hexpr vexpr hexpr vexpr RBRACE {
    current_pos_size.x = $2;
    current_pos_size.y = $3;
    current_pos_size.width_auto = false;
    
    current_pos_size.width = $4;
    current_pos_size.height_auto = false;
    current_pos_size.height = $5;
};

/* This one sucks but will have to do for now */
hsize: hexpr { current_pos_size.width_auto = false; current_pos_size.width = $1; }
     | AUTO { current_pos_size.width_auto = true; current_pos_size.width = 0; };

vsize: vexpr { current_pos_size.height_auto = false; current_pos_size.height = $1; }
     | AUTO { current_pos_size.height_auto = true; current_pos_size.height = 0; };
    

/************************************************************
 * Popup trigger
 ************************************************************/

widget_ptrigger: POPUPTRIGGER stringlit identifier AT POS_SIZE {
    current_widget.ptrigger = dynamic_cast<Widgets::PopupTrigger*> (
	current.form->create_widget (Widgets::WIDGET_POPUP_TRIGGER, $3, true));
    current_widget.ptrigger->text = $2;
} widget_ptrigger_options {
    widget_apply_pos_size (current_widget.ptrigger);
    previous_widget = current_widget.ptrigger;
};

widget_ptrigger_options: widget_ptrigger_options widget_ptrigger_option | /* empty */;
widget_ptrigger_option: widget_prop_font      { current_widget.ptrigger->font = $1; }
                      | widget_prop_usable    { current_widget.ptrigger->usable = $1; }
                      | widget_prop_disabled  { current_widget.ptrigger->disabled = $1; }
                      | widget_prop_anchor    { current_widget.ptrigger->anchor_right = $1; }
                      | GRAPHICAL             /* Ignored */
		      | BITMAPID WORD	      { current_widget.ptrigger->bitmap_id = $2; }
		      | SELECTEDBITMAPID WORD { current_widget.ptrigger->selected_bitmap_id = $2; };


/************************************************************
 * Checkbox
 ************************************************************/

widget_checkbox: CHECKBOX stringlit identifier AT POS_SIZE {
    current_widget.checkbox = dynamic_cast<Widgets::Checkbox*> (
	current.form->create_widget (Widgets::WIDGET_CHECKBOX, $3, true));
    current_widget.checkbox->text = $2;
} widget_checkbox_options {
    widget_apply_pos_size (current_widget.checkbox);
    previous_widget = current_widget.checkbox;
}

widget_checkbox_options: widget_checkbox_options widget_checkbox_option | /* empty */;
widget_checkbox_option: widget_prop_font     { current_widget.checkbox->font = $1; }
                      | widget_prop_usable   { current_widget.checkbox->usable = $1; }
                      | widget_prop_disabled { current_widget.checkbox->disabled = $1; }
                      | widget_prop_anchor   { current_widget.checkbox->anchor_right = $1; }
                      | GROUP NUMBER         { current_widget.checkbox->group = $2; }
                      | CHECKED              { current_widget.checkbox->toggled = true; };



/************************************************************
 * SelectorTrigger
 ************************************************************/

widget_strigger: SELECTORTRIGGER stringlit identifier AT POS_SIZE {
    current_widget.strigger = dynamic_cast<Widgets::SelectorTrigger*> (
	current.form->create_widget (Widgets::WIDGET_SELECTOR_TRIGGER, $3, true));
    current_widget.strigger->text = $2;
} widget_strigger_options {
    widget_apply_pos_size (current_widget.strigger);
    previous_widget = current_widget.strigger;
};

widget_strigger_options: widget_strigger_options widget_strigger_option | /* empty */;
widget_strigger_option: widget_prop_font      { current_widget.strigger->font = $1; }
                      | widget_prop_anchor    { current_widget.strigger->anchor_right = $1; }
                      | widget_prop_usable    { current_widget.strigger->usable = $1; }
                      | widget_prop_disabled  { current_widget.strigger->disabled = $1; }
                      | GRAPHICAL             /* Ignored */
		      | BITMAPID WORD	      { current_widget.strigger->bitmap_id = $2; }
		      | SELECTEDBITMAPID WORD { current_widget.strigger->selected_bitmap_id = $2; };


/************************************************************
 * Label
 ************************************************************/

widget_label: LABEL stringlit identifier AT POSITION {
    current_widget.label = dynamic_cast<Widgets::Label*> (
	current.form->create_widget (Widgets::WIDGET_LABEL, $3, true));
    current_widget.label->text = $2;
} widget_label_options {
    widget_apply_pos (current_widget.label);
    previous_widget = current_widget.label;
};

widget_label_options: widget_label_options widget_label_option | /* empty */;
widget_label_option: widget_prop_font   { current_widget.label->font = $1; }
                   | widget_prop_usable { current_widget.label->usable = $1; };



/************************************************************
 * Pushbutton
 ************************************************************/

widget_pushbutton: PUSHBUTTON stringlit identifier AT POS_SIZE {
    current_widget.pushbutton = dynamic_cast<Widgets::PushButton*> (
	current.form->create_widget (Widgets::WIDGET_PUSHBUTTON, $3, true));
    current_widget.pushbutton->text = $2;
} widget_pushbutton_options {
    widget_apply_pos_size (current_widget.pushbutton);
    previous_widget = current_widget.pushbutton;
};

widget_pushbutton_options: widget_pushbutton_options widget_pushbutton_option | /* empty */;
widget_pushbutton_option: widget_prop_font      { current_widget.pushbutton->font = $1; }
                        | widget_prop_usable    { current_widget.pushbutton->usable = $1; }
                        | widget_prop_disabled  { current_widget.pushbutton->disabled = $1; }
                        | widget_prop_anchor    { current_widget.pushbutton->anchor_right = $1; }
                        | GROUP NUMBER          { current_widget.pushbutton->group = $2; }
                        | GRAPHICAL             /* Ignored */
			| BITMAPID WORD		{ current_widget.pushbutton->bitmap_id = $2; }
			| SELECTEDBITMAPID WORD { current_widget.pushbutton->selected_bitmap_id = $2; };


/************************************************************
 * Button
 ************************************************************/

widget_button: widget_button_begin AT POS_SIZE widget_button_options {
    widget_apply_pos_size (current_widget.button);
    previous_widget = current_widget.button;
};

widget_button_begin: BUTTON stringlit identifier {
    current_widget.button = dynamic_cast<Widgets::Button*> (
	current.form->create_widget (Widgets::WIDGET_BUTTON, $3, true));
    current_widget.button->text = $2;
} | REPEATBUTTON stringlit identifier {
    current_widget.button = dynamic_cast<Widgets::Button*> (
	current.form->create_widget (Widgets::WIDGET_BUTTON, $3, true));
    current_widget.button->text = $2;
    current_widget.button->repeat = true;
};

widget_button_options: widget_button_options widget_button_option | /* empty */;
widget_button_option : widget_prop_font      { current_widget.button->font = $1; }
                     | widget_prop_usable    { current_widget.button->usable = $1; }
                     | widget_prop_disabled  { current_widget.button->disabled = $1; }
                     | NOFRAME               { current_widget.button->frame = Widgets::Button::FRAME_NONE; }
                     | FRAME                 { current_widget.button->frame = Widgets::Button::FRAME_SIMPLE; }
                     | BOLDFRAME             { current_widget.button->frame = Widgets::Button::FRAME_BOLD; }
                     | widget_prop_anchor    { current_widget.button->anchor_right = $1; }
                     | GRAPHICAL             /* Ignored */
		     | BITMAPID WORD	     { current_widget.button->bitmap_id = $2; }
		     | SELECTEDBITMAPID WORD { current_widget.button->selected_bitmap_id = $2; };
		     

/************************************************************
 * Bitmap widget
 ************************************************************/

widget_bitmap: FORMBITMAP AT POSITION {
    current_widget.bitmap = dynamic_cast<Widgets::FormBitmap*> (
	current.form->create_widget (Widgets::WIDGET_FORMBITMAP));
} widget_bitmap_options {
    widget_apply_pos (current_widget.bitmap);
    previous_widget = current_widget.bitmap;
};

widget_bitmap_options: widget_bitmap_option widget_bitmap_options | /* Empty */;
widget_bitmap_option: BITMAP WORD        { current_widget.bitmap->bitmap_id = $2; }
                    | widget_prop_usable { current_widget.bitmap->usable = $1; };


/************************************************************
 * Graffiti state indicator
 ************************************************************/

widget_graffiti: GRAFFITISTATEINDICATOR AT POSITION {
    Widgets::Graffiti *graffiti = dynamic_cast<Widgets::Graffiti*> (
	current.form->create_widget (Widgets::WIDGET_GRAFFITI));
    widget_apply_pos (graffiti);
    previous_widget = graffiti;
} | NOGRAFFITISTATEINDICATOR;


/************************************************************
 * Table
 ************************************************************/

widget_table: TABLE identifier AT POS_SIZE {
    current_widget.table = dynamic_cast<Widgets::Table*> (
	current.form->create_widget (Widgets::WIDGET_TABLE, $2, true));
} widget_table_options {
    Table::import_end ();
    widget_apply_pos_size (current_widget.table);
    previous_widget = current_widget.table;
};

widget_table_options: widget_table_option widget_table_options | /* empty */;
widget_table_option: ROWS NUMBER    { current_widget.table->num_rows = $2; }
                   | COLUMNS NUMBER { current_widget.table->num_columns = $2; }
                   | COLUMNWIDTHS widget_table_cols;

widget_table_cols: widget_table_col widget_table_cols | /* empty */;
widget_table_col: NUMBER { Table::import_col ($1); };


/************************************************************
 * List
 ************************************************************/

widget_list: LIST widget_list_items identifier AT POS_SIZE {
    current_widget.lst = dynamic_cast<Widgets::List*> (
	current.form->create_widget (Widgets::WIDGET_LIST, $3, true));
    List::apply_items ();
} widget_list_options {
    widget_apply_pos_size (current_widget.lst);
    previous_widget = current_widget.lst;
};

widget_list_items: widget_list_item widget_list_items | /* empty */;
widget_list_item: stringlit {
    List::import_item ($1);
};

widget_list_options: widget_list_option widget_list_options | /* empty */;
widget_list_option: widget_prop_usable   { current_widget.lst->usable = $1; }
                  | widget_prop_font     { current_widget.lst->font = $1; }
                  | VISIBLEITEMS NUMBER  { current_widget.lst->visible_items = $2; }
                  | SEARCH               { /* WRITE ME */ };


/************************************************************
 * Scroll bar
 ************************************************************/

widget_scrollbar: SCROLLBAR identifier AT POS_SIZE {
    current_widget.scrollbar = dynamic_cast<Widgets::ScrollBar*> (
	current.form->create_widget (Widgets::WIDGET_SCROLLBAR, $2, true));
} widget_scrollbar_options {
    widget_apply_pos_size (current_widget.scrollbar);    
    previous_widget = current_widget.scrollbar;
}

widget_scrollbar_options: widget_scrollbar_option widget_scrollbar_options | /* empty */;
widget_scrollbar_option: widget_prop_usable { current_widget.scrollbar->usable = $1; }
                       | VALUE NUMBER    { current_widget.scrollbar->value = $2; }
		       | MIN_VAL NUMBER  { current_widget.scrollbar->min_value = $2; }
		       | MAX_VAL NUMBER  { current_widget.scrollbar->max_value = $2; }
                       | PAGESIZE NUMBER { current_widget.scrollbar->page_size = $2; };

/************************************************************
 * Slider
 ************************************************************/
widget_slider: SLIDER identifier AT POS_SIZE {
    current_widget.slider = dynamic_cast<Widgets::Slider*> (
        current.form->create_widget (Widgets::WIDGET_SLIDER, $2, true));
} widget_slider_options {
    widget_apply_pos_size (current_widget.slider);
    previous_widget = current_widget.slider;
}

widget_slider_options: widget_slider_option widget_slider_options | /* empty */;
widget_slider_option: widget_prop_usable   { current_widget.slider->usable = $1; }
                    | widget_prop_disabled { current_widget.slider->disabled = $1; }
                    | VERTICAL             { /* WRITE ME */ }
                    | FEEDBACK             { current_widget.slider->feedback = true; }
		    | THUMBID WORD         { current_widget.slider->thumb_id = $2; }
		    | BACKGROUNDID WORD    { current_widget.slider->background_id = $2; }
		    | VALUE NUMBER         { current_widget.slider->value = $2; }
		    | MIN_VAL NUMBER       { current_widget.slider->min_value = $2; }
		    | MAX_VAL NUMBER       { current_widget.slider->max_value = $2; }
                    | PAGESIZE NUMBER      { current_widget.slider->page_size = $2; };

/************************************************************
 * Text Field
 ************************************************************/

widget_field: FIELD identifier AT POS_SIZE {
    current_widget.field = dynamic_cast<Widgets::TextField*> (
	current.form->create_widget (Widgets::WIDGET_TEXT_FIELD, $2, true));
    current_widget.field->underline = false;
    current_widget.field->editable = false;
} widget_field_options {
    widget_apply_pos_size (current_widget.field);
    previous_widget = current_widget.field;
}

widget_field_options: widget_field_option widget_field_options | /* empty */;
widget_field_option: widget_prop_usable   { current_widget.field->usable = $1; }
                   | widget_prop_disabled { current_widget.field->disabled = $1; }
                   | widget_prop_font     { current_widget.field->font = $1; }
                   | EDITABLE             { current_widget.field->editable = true; }
                   | NONEDITABLE          { current_widget.field->editable = false; }
                   | UNDERLINED           { current_widget.field->underline = true; }
                   | SINGLELINE           { current_widget.field->multi_line = false; }
                   | MULTIPLELINES        { current_widget.field->multi_line = true; }
                   | DYNAMICSIZE          { current_widget.field->dynamic_size = true; }
                   | AUTOSHIFT            { current_widget.field->auto_shift = true; }
                   | NUMERIC              { current_widget.field->numeric = true; }
                   | HASSCROLLBAR         { current_widget.field->has_scrollbar = true; }
                   | MAXCHARS NUMBER      { current_widget.field->max_length = $2; }
                   | LEFTALIGN            { current_widget.field->justify_right = false; }
                   | RIGHTALIGN           { current_widget.field->justify_right = true; };

/************************************************************
 * Form
 ************************************************************/

res_form: res_form_start res_form_options BEG res_form_children END { Form::import_end (); };

res_form_start: FORM identifier AT FORM_POS_SIZE {
    previous_widget = 0;
    
    current.form = dynamic_cast<Resources::Form*> (
	manager->create_resource (Resources::RESOURCE_FORM, $2));
    current.form->x = current_pos_size.x->eval (0, 0);
    current.form->y = current_pos_size.y->eval (0, 0);
    current.form->width = current_pos_size.width->eval (0, 0);
    current.form->height = current_pos_size.height->eval (0, 0);
};

res_form_options: res_form_options res_form_option | /* empty */;
res_form_option: FRAME             { current.form->frame = true; }
               | NOFRAME           { current.form->frame = false; }
               | USABLE            { /* Do nothing */ }
               | MODAL             { current.form->modal = true; }
               | SAVEBEHIND        { current.form->savebehind = true; }
               | NOSAVEBEHIND      { current.form->savebehind = false; }
               | HELPID WORD       { current.form->help_id = $2; }
               | MENUID WORD       { current.form->menu_id = $2; }
               | DEFAULTBTNID WORD { current.form->def_button = $2; }
               | localization;

res_form_children: res_form_children res_form_child | /* empty */;
res_form_child: TITLE stringlit { current.form->title = $2; }
              | POPUPLIST identifier WORD { Form::import_popuplist ($2, $3); }
              | widget_button
              | widget_pushbutton
	      | widget_label
              | widget_strigger
	      | widget_ptrigger
              | widget_field
              | widget_checkbox
              | widget_graffiti
	      | widget_table
	      | widget_scrollbar
              | widget_slider
	      | widget_list              
              | widget_bitmap;


/************************************************************
 * Dialog 
 ************************************************************/

res_dialog: ALERT identifier {
    current.dialog = dynamic_cast<Resources::Dialog*> (
	manager->create_resource (Resources::RESOURCE_DIALOG, $2));
}
res_dialog_options BEG res_dialog_children END;

res_dialog_options: res_dialog_options res_dialog_option | /* empty */ ;
res_dialog_option: DEFAULTBUTTON NUMBER { current.dialog->default_button = $2; }
                 | DIALOG_INFORMATION   { current.dialog->dialog_type = Resources::Dialog::TYPE_INFORMATION; }
                 | DIALOG_CONFIRMATION  { current.dialog->dialog_type = Resources::Dialog::TYPE_CONFIRMATION; }
                 | DIALOG_WARNING       { current.dialog->dialog_type = Resources::Dialog::TYPE_WARNING; }
                 | DIALOG_ERROR         { current.dialog->dialog_type = Resources::Dialog::TYPE_ERROR; }
                 | HELPID identifier    { current.dialog->help_id = $2; }
                 | localization;


res_dialog_children: res_dialog_children res_dialog_child | /* empty */;
res_dialog_child: TITLE stringlit   { current.dialog->title = $2; }
                | MESSAGE stringlit { current.dialog->text = $2; }
                | BUTTONS res_dialog_buttons;

res_dialog_buttons: res_dialog_buttons res_dialog_button | /* empty */;
res_dialog_button: stringlit { Dialog::import_button ($1); };


/************************************************************
 * String
 ************************************************************/

res_string: STRING identifier opt_localization stringlit {
    Resource *res = manager->create_resource (Resources::RESOURCE_STRING, $2);
    Resources::String *str = dynamic_cast<Resources::String*> (res);

    str->text = $4;
};

/* TODO: string import from file */


/************************************************************
 * Application meta-resources
 ************************************************************/

res_app_ver: VERSION_TOKEN identifier stringlit { manager->get_application ()->version = $3; }
           | VERSION_TOKEN stringlit            { manager->get_application ()->version = $2; }

res_app_name: APPLICATIONICONNAME identifier opt_localization stringlit {
    manager->get_application ()->iconname = $4; };


/************************************************************
 * String table
 ************************************************************/
res_stringlist: STRINGTABLE identifier opt_localization stringlit {
    current.stringlist = dynamic_cast<Resources::StringList*> (
	manager->create_resource (Resources::RESOURCE_STRINGLIST, $2));

    current.stringlist->prefix = $4;
} res_stringlist_strings { StringList::import_end (); };

res_stringlist_strings: res_stringlist_string res_stringlist_strings | /* empty */ ;
res_stringlist_string: stringlit { StringList::import_string ($1); };


/************************************************************
 * Bitmap
 ************************************************************/

res_bitmap: res_bitmap_type identifier opt_localization stringlit {
    Resource *res = manager->create_resource (Resources::RESOURCE_BITMAP, $2);
    Resources::Bitmap *bmp = dynamic_cast<Resources::Bitmap*> (res);

    bmp->bitmap_type = current_bitmap_type;
    std::string bitmap_uri = create_child_uri ($4);
    if (bitmap_uri != "")
	bmp->load_file (bitmap_uri);
} res_bitmap_options;

res_bitmap_type: BITMAP         { current_bitmap_type = Resources::Bitmap::TYPE_MONO; } 
               | BITMAPGREY     { current_bitmap_type = Resources::Bitmap::TYPE_GREY_4; }
               | BITMAPGREY16   { current_bitmap_type = Resources::Bitmap::TYPE_GREY_16; }
               | BITMAPCOLOR16  { current_bitmap_type = Resources::Bitmap::TYPE_COLOR_16; }
               | BITMAPCOLOR    { current_bitmap_type = Resources::Bitmap::TYPE_COLOR_256; }
               | BITMAPCOLOR16K { current_bitmap_type = Resources::Bitmap::TYPE_COLOR_16K; };

/* These are ignored for now */
res_bitmap_options: res_bitmap_option res_bitmap_options | /* empty */ ;
res_bitmap_option: NOCOMPRESS | COMPRESS | FORCECOMPRESS
                 | NOCOLORTABLE | COLORTABLE
                 | TRANSPARENT NUMBER NUMBER NUMBER
                 | TRANSPARENTINDEX WORD
                 | DENSITY WORD;


/************************************************************
 * Bitmap family
 ************************************************************/

res_bitmapfamily: res_bitmapfamily_def | res_bitmapfamily_special | res_bitmapfamily_ex;
res_bitmapfamily_def: BITMAPFAMILY identifier opt_localization
                      stringlit stringlit stringlit stringlit {
    Resource *res = manager->create_resource (Resources::RESOURCE_BITMAPFAMILY, $2);
    Resources::BitmapFamily *bmpfam = dynamic_cast<Resources::BitmapFamily*> (res);    

    std::string bitmap_uri;
    if ((bitmap_uri = create_child_uri ($4)) != "")
	bmpfam->load_file (Resources::Bitmap::TYPE_MONO, bitmap_uri);
    if ((bitmap_uri = create_child_uri ($5)) != "")
	bmpfam->load_file (Resources::Bitmap::TYPE_GREY_4, bitmap_uri);
    if ((bitmap_uri = create_child_uri ($6)) != "")
	bmpfam->load_file (Resources::Bitmap::TYPE_GREY_16, bitmap_uri);
    if ((bitmap_uri = create_child_uri ($7)) != "")
	bmpfam->load_file (Resources::Bitmap::TYPE_COLOR_256, bitmap_uri);
} res_bitmap_options;

res_bitmapfamily_special: BITMAPFAMILYSPECIAL identifier opt_localization
                          stringlit stringlit stringlit stringlit {
    Resource *res = manager->create_resource (Resources::RESOURCE_BITMAPFAMILY, $2);
    Resources::BitmapFamily *bmpfam = dynamic_cast<Resources::BitmapFamily*> (res);    

    std::string bitmap_uri;
    if ((bitmap_uri = create_child_uri ($4)) != "")
	bmpfam->load_file (Resources::Bitmap::TYPE_MONO, bitmap_uri);
    if ((bitmap_uri = create_child_uri ($5)) != "")
	bmpfam->load_file (Resources::Bitmap::TYPE_GREY_4, bitmap_uri);
    if ((bitmap_uri = create_child_uri ($6)) != "")
	bmpfam->load_file (Resources::Bitmap::TYPE_COLOR_16, bitmap_uri);
    if ((bitmap_uri = create_child_uri ($7)) != "")
	bmpfam->load_file (Resources::Bitmap::TYPE_COLOR_256, bitmap_uri);
} res_bitmap_options;

res_bitmapfamily_ex: BITMAPFAMILYEX | BITMAPFAMILY identifier {
    Resource *res = manager->create_resource (Resources::RESOURCE_BITMAPFAMILY, $2);
    current.bitmap_family = dynamic_cast<Resources::BitmapFamily*> (res);    
} res_bitmap_options BEG res_bitmapfamily_children END;

res_bitmapfamily_children: res_bitmapfamily_child res_bitmapfamily_children | /* Empty */;
res_bitmapfamily_child: BITMAP stringlit BPP NUMBER {
    BitmapFamily::import_bitmap ($2, $4);
} res_bitmap_options;

/************************************************************
 * Menu
 ************************************************************/

res_menu: MENU identifier {
    Resource *res = manager->create_resource (Resources::RESOURCE_MENU, $2);
    current.menu = dynamic_cast<Resources::Menu*> (res);
} opt_localization BEG res_menu_menus END { Menu::import_end (); };

res_menu_menus: res_menu_menu res_menu_menus | /* empty */;
res_menu_menu: PULLDOWN stringlit { Menu::import_menu ($2); }
               BEG res_menu_children END;

res_menu_children: res_menu_children res_menu_child | /* empty */;
res_menu_child: MENUITEM stringlit identifier stringlit { Menu::import_menuitem ($3, $2, $4); }
              | MENUITEM SEPARATOR { Menu::import_separator (); }
              | MENUITEM stringlit identifier { Menu::import_menuitem ($3, $2); };



/************************************************************
 * String literals
 ************************************************************/
stringlit: QUOTESTRING { $$ = g_strdup ($1); }
         | QUOTESTRING BACKSLASH stringlit { $$ = g_strdup_printf ("%s%s", $1, $3); }


/************************************************************
 * Infix algebraic expressions
 ************************************************************/
hexpr: hexpr PLUS hexpr    { $$ = new RCP::Expr::BinFunction (RCP::Expr::Functions::add, $1, $3); }
     | hexpr MINUS hexpr   { $$ = new RCP::Expr::BinFunction (RCP::Expr::Functions::sub, $1, $3); }
     | hexpr TIMES hexpr   { $$ = new RCP::Expr::BinFunction (RCP::Expr::Functions::mul, $1, $3); }
     | hexpr DIV hexpr     { $$ = new RCP::Expr::BinFunction (RCP::Expr::Functions::div, $1, $3); }
     | LBRACE hexpr RBRACE { $$ = $2; }
     | hexpr_atom;

vexpr: vexpr PLUS vexpr     { $$ = new RCP::Expr::BinFunction (RCP::Expr::Functions::add, $1, $3); }
     | vexpr MINUS vexpr    { $$ = new RCP::Expr::BinFunction (RCP::Expr::Functions::sub, $1, $3); }
     | vexpr TIMES vexpr    { $$ = new RCP::Expr::BinFunction (RCP::Expr::Functions::mul, $1, $3); }
     | vexpr DIV vexpr      { $$ = new RCP::Expr::BinFunction (RCP::Expr::Functions::div, $1, $3); }
     | LBRACE vexpr RBRACE  { $$ = $2; }
     | vexpr_atom;

hexpr_atom: NUMLIT
          | CENTER              { $$ = new RCP::Expr::CenterHoriz; }
          | CENTER ATSIGN hexpr { $$ = new RCP::Expr::CenterHoriz ($3); }
          | RIGHT ATSIGN hexpr  { $$ = new RCP::Expr::Right ($3); };
          | PREVLEFT            { $$ = new RCP::Expr::Relative::PrevLeft; }
          | PREVRIGHT           { $$ = new RCP::Expr::Relative::PrevRight; }
          | PREVWIDTH           { $$ = new RCP::Expr::Relative::PrevWidth; }
          | PREVTOP             { $$ = new RCP::Expr::Relative::PrevTop; }
          | PREVBOTTOM          { $$ = new RCP::Expr::Relative::PrevBottom; }
          | PREVHEIGHT          { $$ = new RCP::Expr::Relative::PrevHeight; }

vexpr_atom: NUMLIT
          | CENTER              { $$ = new RCP::Expr::CenterVert; }
          | CENTER ATSIGN vexpr { $$ = new RCP::Expr::CenterVert ($3); }
          | BOTTOM ATSIGN vexpr { $$ = new RCP::Expr::Bottom ($3); };
          | PREVLEFT            { $$ = new RCP::Expr::Relative::PrevLeft; }
          | PREVRIGHT           { $$ = new RCP::Expr::Relative::PrevRight; }
          | PREVWIDTH           { $$ = new RCP::Expr::Relative::PrevWidth; }
          | PREVTOP             { $$ = new RCP::Expr::Relative::PrevTop; }
          | PREVBOTTOM          { $$ = new RCP::Expr::Relative::PrevBottom; }
          | PREVHEIGHT          { $$ = new RCP::Expr::Relative::PrevHeight; }

NUMLIT: NUMBER { $$ = new RCP::Expr::Literal ($1); }

%%

const char *yy_token_name(int token_index) {
  int internal_index = YYTRANSLATE(token_index);
  return yytname[internal_index];
}
