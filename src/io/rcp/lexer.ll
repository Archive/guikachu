/* $Id: lexer.ll,v 1.10 2006/11/04 14:17:59 cactus Exp $ -*- c -*- */

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

%option noyywrap
%option caseless
%option never-interactive

%{
#include "expr.h"
#include "parser.h"
#include <string.h>

#define FOUND(x) { yylval.str = g_strdup(yytext); return (x); }

  int lexer_lineno = 1;  /* %option yylineno creates warnings */
  void count_newlines(char *string) {
    while ((string = strchr(string, '\n'))) {
      string++;
      lexer_lineno++;
    }
  }

%}

/* definitions */
CHAR  [a-zA-Z0-9_]
DIGIT [0-9]

/* contexts */
%s bareword

/* rules start */

%%

autoid return AUTOID;

"(" return LBRACE;
")" return RBRACE;
"@" return ATSIGN;
"\\" return BACKSLASH;

begin return BEG;
end   return END;

helpid       return HELPID;
menuid       return MENUID;
defaultbtnid return DEFAULTBTNID;
version      return VERSION_TOKEN /* VERSION is a macro used by autotools */;
locale       return LOCALE;

applicationiconname return APPLICATIONICONNAME;
alert	            return ALERT;
form                return FORM;
menu                return MENU;
string              return STRING;
stringtable         return STRINGTABLE;
bitmap              return BITMAP;
bitmapfamily	    return BITMAPFAMILY;
categories          return CATEGORIES;

confirmation return DIALOG_CONFIRMATION;
information  return DIALOG_INFORMATION;
warning	     return DIALOG_WARNING;
error	     return DIALOG_ERROR;

button                   return BUTTON;
popuptrigger             return POPUPTRIGGER;
repeatbutton             return REPEATBUTTON;
checkbox                 return CHECKBOX;
selectortrigger          return SELECTORTRIGGER;
label                    return LABEL;
pushbutton               return PUSHBUTTON;
graffitistateindicator   return GRAFFITISTATEINDICATOR;
nograffitistateindicator return NOGRAFFITISTATEINDICATOR;
field                    return FIELD;
list                     return LIST;
formbitmap               return FORMBITMAP;
gadget                   return GADGET;
popuplist                return POPUPLIST;
table                    return TABLE;
scrollbar                return SCROLLBAR;
slider                   return SLIDER;

auto	     return AUTO;
usable       return USABLE;
nonusable    return NONUSABLE;
disabled     return DISABLED;
leftanchor   return LEFTANCHOR;
rightanchor  return RIGHTANCHOR;
frame        return FRAME;
noframe      return NOFRAME;
boldframe    return BOLDFRAME;
leftalign    return LEFTALIGN;
rightalign   return RIGHTALIGN;
autoshift    return AUTOSHIFT;
numeric      return NUMERIC;
hasscrollbar return HASSCROLLBAR;
vertical     return VERTICAL;
feedback     return FEEDBACK;
visibleitems return VISIBLEITEMS;
search       return SEARCH;
rows         return ROWS;
columns      return COLUMNS;
columnwidths return COLUMNWIDTHS;
value	     return VALUE;
min	     return MIN_VAL;
max	     return MAX_VAL;
pagesize     return PAGESIZE;
graphical        return GRAPHICAL;
bitmapid         return BITMAPID;
selectedbitmapid return SELECTEDBITMAPID;
thumbid          return THUMBID;
backgroundid     return BACKGROUNDID;

bitmapgrey          return BITMAPGREY;
bitmapgrey16	    return BITMAPGREY16;
bitmapcolor16	    return BITMAPCOLOR16;
bitmapcolor	    return BITMAPCOLOR;
bitmapcolor16k	    return BITMAPCOLOR16K;
bitmapfamilyspecial return BITMAPFAMILYSPECIAL;
bitmapfamilyex	    return BITMAPFAMILYEX;

compress         return COMPRESS;
nocompress   	 return NOCOMPRESS;
forcecompress 	 return FORCECOMPRESS;
colortable   	 return COLORTABLE;
nocolortable	 return NOCOLORTABLE;
transparent	 return TRANSPARENT;
transparency     return TRANSPARENT; /* some PilRC sample files seem to use
                                      * "TRANSPARENCY" instead of TRANSPARENT */
transparentindex return TRANSPARENTINDEX;
density		 return DENSITY;
bpp		 return BPP;

at            return AT;
buttons       return BUTTONS;
checked       return CHECKED;
defaultbutton return DEFAULTBUTTON;
dynamicsize   return DYNAMICSIZE;
editable      return EDITABLE;
noneditable   return NONEDITABLE;
font          return FONT;
group         return GROUP;
maxchars      return MAXCHARS;
menuitem      return MENUITEM;
message       return MESSAGE;
modal         return MODAL;
multiplelines return MULTIPLELINES;
pulldown      return PULLDOWN;
nosavebehind  return NOSAVEBEHIND;
savebehind    return SAVEBEHIND;
separator     return SEPARATOR;
singleline    return SINGLELINE;
title	      return TITLE;
underlined    return UNDERLINED;

center	   return CENTER;
right      return RIGHT;
bottom     return BOTTOM;
prevleft   return PREVLEFT;
prevright  return PREVRIGHT;
prevtop    return PREVTOP;
prevbottom return PREVBOTTOM;
prevwidth  return PREVWIDTH;
prevheight return PREVHEIGHT;

"+" return PLUS;
"-" return MINUS;
"*" return TIMES;
"/" return DIV;

"/*"([^\*]|\*[^/])*"*/" { count_newlines(yytext); /* C-style comments */ }
"//".* { /* C++-style comments */ }
	
<bareword>{CHAR}+ {
  /* grab a single bareword even if it would otherwise match a reserved word */
  BEGIN (INITIAL);
  FOUND (WORD);
}

id {
  /* ID's can be otherwise reserved words */
  BEGIN (bareword);

  /* ID is a classname used by Guikachu */
  return ID_TOKEN;
}

([0-9]+) {
  yylval.num = atoi (yytext);
  return NUMBER;
}

{CHAR}+  { FOUND (WORD); }

\"[^\"]*\" { 
  /* return the string inside the quotes */
  yytext[yyleng - 1] = '\0';
  yylval.str = g_strdup(yytext + 1); 
  return QUOTESTRING; 
}

\n lexer_lineno++; /* count line numbers for debugging */
.  /* presumably whitespace we can skip */

^[ \t]*#.* { /* Preprocessor macros */ }
