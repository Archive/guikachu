//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/base64.h"

using namespace Guikachu;
using namespace Guikachu::IO::Base64;

namespace
{
    encoded_byte_t decode_value (encoded_char_t encoded_char)
    {
	// encoded_byte_t is a 0..63 value
	// -1: end of stream
	// -2: error
	
        if (encoded_char >= 'A' && encoded_char <= 'Z')
            return encoded_char - 'A';
        
        if (encoded_char >= 'a' && encoded_char <= 'z')
            return encoded_char - 'a' + 26;
        
        if (encoded_char >= '0' && encoded_char <= '9')
            return encoded_char - '0' + 52;
        
        if (encoded_char == '+')
            return 62;
        
        if (encoded_char == '/')
            return 63;
        
        if (encoded_char == '=')
            return -1;
        
        return -2;
    }

    encoded_char_t encode_value (encoded_byte_t encoded_byte)
    {
	// This maps a 0..63 value to [A-Za-z0-9+/]
	// The end of the stream is filled '='
	
        if (encoded_byte < 0)
            return '=';
            
        if (encoded_byte < 26)
            return encoded_byte + 'A';

        if (encoded_byte < 52)
            return encoded_byte + 'a' - 26;
        
        if (encoded_byte < 62)
            return encoded_byte + '0' - 52;
        
        if (encoded_byte == 62)
            return '+';

        if (encoded_byte == 63)
            return '/';

        return '=';
    }

    struct ElementEncoder
    {
    private:
        encoded_byte_t encoded_bytes[4];

    public:
        ElementEncoder (decoded_stream_t orig_data_start, size_t orig_size, size_t idx);
        encoded_string_t write () const;
    };
    
    ElementEncoder::ElementEncoder (decoded_stream_t orig_data_start,
				    size_t orig_size, size_t idx)
    {
        // Encode the first byte
        encoded_bytes[0] = orig_data_start[idx] >> 2;
        encoded_bytes[1] = (orig_data_start[idx] & 0x03) << 4;

        // Only one byte left to encode
        if (orig_size - idx == 1)
        {
            encoded_bytes[2] = encoded_bytes[3] = -1;
            return;
        }

        encoded_bytes[1] |= (orig_data_start[idx + 1] & 0xF0) >> 4;
        encoded_bytes[2] = (orig_data_start[idx + 1] & 0x0F) << 2;

        // Only two bytes left to encode
        if (orig_size - idx == 2)
        {
            encoded_bytes[3] = -1;
            return;
        }

        encoded_bytes[2] |= (orig_data_start[idx + 2] & 0xC0) >> 6;
        encoded_bytes[3] = orig_data_start[idx + 2] & 0x3F;
    }

    encoded_string_t ElementEncoder::write () const
    {
	encoded_char_t ret[5] = {};
        
        for (int i = 0; i < 4; ++i)
            ret[i] = encode_value (encoded_bytes[i]);

        return ret;
    }



    struct ElementDecoder
    {
    private:
        encoded_byte_t encoded_bytes[4];

        decoded_byte_t decode_1 () const { return (encoded_bytes[0] << 2) + (encoded_bytes[1] >> 4); };
        decoded_byte_t decode_2 () const { return ((encoded_bytes[1] << 4) & 0xF0) + ((encoded_bytes[2] & 0x3C) >> 2); };
        decoded_byte_t decode_3 () const { return ((encoded_bytes[2] & 0x03) << 6) + encoded_bytes[3]; };

    public:
	ElementDecoder (const encoded_string_t      &encoded_string,
			encoded_string_t::size_type &start);
        void decode (decoded_stream_t &decoded_data_start, size_t &decoded_size) const;
    };

    ElementDecoder::ElementDecoder (const encoded_string_t      &encoded,
				    encoded_string_t::size_type &start)
    {
        for (int i = 0; i < 4; ++i)
        {
	    // Skip invalid characters (and newlines) in the encoded string
            while (start < encoded.length () && (encoded_bytes[i] = decode_value (encoded[start])) < -1)
                ++start;
            ++start;
        }
    }

    void ElementDecoder::decode (decoded_stream_t &decoded_data_start,
				 size_t           &decoded_size) const
    {
        // Try decoding first byte
        if (encoded_bytes[0] > -1 && encoded_bytes[1] > -1)
        {
            *(decoded_data_start++) = decode_1 ();
            ++decoded_size;
        } else {
            return;
        }
    
        // Try decoding the second byte
        if (encoded_bytes[1] > -1 && encoded_bytes[2] > -1)
        {
            *(decoded_data_start++) = decode_2 ();
            ++decoded_size;
        } else {
            return;
        }
    
        // Try decoding the third byte
        if (encoded_bytes[2] > -1 && encoded_bytes[3] > -1)
        {
            *(decoded_data_start++) = decode_3 ();
            ++decoded_size;
        } else {
            return;
        }
    }


    
    // An 'element' is 4 encoded_chars which are decoded to 3 decoded_bytes
    void decode_element (const encoded_string_t &encoded, encoded_string_t::size_type &start,
                         decoded_stream_t &decoded_data_start, size_t &decoded_size)
    {
	ElementDecoder (encoded, start).decode (decoded_data_start, decoded_size);
    }

    encoded_string_t encode_element (decoded_stream_t orig_data_start, size_t orig_size, size_t idx)
    {
        return ElementEncoder (orig_data_start, orig_size, idx).write ();
    }
}

void IO::Base64::decode (const encoded_string_t &encoded,
			 decoded_stream_t &decoded_data, size_t &decoded_size)
{
    decoded_data = new decoded_byte_t[((encoded.length () + 3) / 4) * 3];
    decoded_size = 0;
    
    decoded_stream_t decoded_data_i = decoded_data;

    for (encoded_string_t::size_type i = 0; i < encoded.length ();
         decode_element (encoded, i, decoded_data_i, decoded_size));
}

encoded_string_t IO::Base64::encode (decoded_stream_t orig_data, size_t orig_size)
{
    encoded_string_t ret;
    ret.reserve (((orig_size + 2) / 3) * 4);

    for (size_t idx = 0; idx < orig_size; idx += 3)
        ret += encode_element (orig_data, orig_size, idx);

    return ret;
}
