//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_IO_TYPES_H
#define GUIKACHU_IO_TYPES_H

#ifdef GUIKACHU_HAVE_GNOMEVFS
#include <libgnomevfsmm/types.h>
#include <libgnomevfsmm/handle.h>
#include <libgnomevfsmm/uri.h>
#endif

#include <glibmm/exception.h>

#include "config.h"

namespace Guikachu
{
    namespace IO
    {
#ifdef GUIKACHU_HAVE_GNOMEVFS
        typedef Glib::RefPtr<Gnome::Vfs::Uri> uri_holder_t;
        typedef Gnome::Vfs::FileSize          filesize_t;

        class Exception: public Gnome::Vfs::exception
        {
        public:
            Exception (): Gnome::Vfs::exception (Gnome::Vfs::ERROR_GENERIC) {};
            Exception (Gnome::Vfs::Result r): Gnome::Vfs::exception (r) {};
        };
#else
        typedef std::string uri_holder_t;
        typedef size_t      filesize_t;
        
        class Exception: public Glib::Exception
        {
            int stored_errno;
        public:
            Exception ();
            Exception (int stored_errno);
            
            virtual Glib::ustring what() const;
        };
#endif
    }
}

#endif /* !GUIKACHU_IO_TYPES_H */
