//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "storage-processor.h"

#include <glib.h> // for g_warning

using namespace Guikachu;

StorageProcessor::~StorageProcessor ()
{
    for (handler_map_t::iterator i = handler_map.begin (); i != handler_map.end (); ++i)
	delete i->second;
}

void StorageProcessor::add_handler (const std::string             &node_name,
				    StorageProcessor::NodeHandler *handler)
{
    handler_map[node_name] = handler;
}

StorageProcessor::NodeHandler* StorageProcessor::get_handler (const StorageNode &node)
{
    handler_map_t::iterator handler_i = handler_map.find (node.name ().c_str ());

    if (handler_i == handler_map.end ())
	return 0;

    return handler_i->second;
}

void StorageProcessor::run (const StorageNode &root)
{
    for (StorageNode node = root.children (); node; ++node)
    {
	NodeHandler *handler = get_handler (node);
	if (handler)
	    (*handler) (node);
	else	
	    g_warning ("Error parsing `%s' node: unexpected element `%s'",
		       root.name ().c_str (), node.name ().c_str ());
    }
}
