//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "rcp-saver.h"
#include "rcp-saver-priv.h"

#include <sstream>

#include "blob-res.h"
#include "dialog-res.h"
#include "menu-res.h"
#include "string-res.h"
#include "stringlist-res.h"
#include "bitmap-res.h"
#include "bitmapfamily-res.h"

#include "rcp-save-form.h" // TODO: change this to a Visitor

#include "config.h" // for VERSION

namespace Guikachu
{
    
    namespace IO
    {
        
        namespace RCP
        {
            class ResourceSaver: public ResourceVisitor
            {
                std::ostream &ostr;
                uri_holder_t  parent_uri;
                
            public:
                ResourceSaver (std::ostream &ostr_, const uri_holder_t &parent_uri_):
                    ostr (ostr_),
                    parent_uri (parent_uri_)
                    {};
                
                void save_app      (Resources::Application *app);
                
                void visit_resource (Resources::Dialog       *res);
                void visit_resource (Resources::String       *res);
                void visit_resource (Resources::StringList   *res);
                void visit_resource (Resources::Menu         *res);
                void visit_resource (Resources::Form         *res);
                void visit_resource (Resources::Blob         *res);
                void visit_resource (Resources::Bitmap       *res);
                void visit_resource (Resources::BitmapFamily *res);
            };
        }
    }
}

using namespace Guikachu;
using namespace Guikachu::IO;

void RCPSaver::save (ResourceManager *manager, const Glib::ustring &uri) throw (Glib::Exception)
{
    std::stringstream stream;

    time_t curr_date = time (0);
    const char* date_str = ctime (&curr_date);
    
    stream << "/*" << std::endl;
    stream << " * This file was created by Guikachu " << VERSION
	   << ". Do not edit it manually." << std::endl;
    stream << " * Creation date: " << date_str; // ctime ()'s return value ends in \n
    stream << " */" << std::endl << std::endl;;
    
    const std::set<Resource*> &resources = manager->get_resources ();

#ifdef GUIKACHU_HAVE_GNOMEVFS
    IO::uri_holder_t orig_uri = Gnome::Vfs::Uri::create (uri);
    IO::uri_holder_t parent_uri = orig_uri->get_parent ();
#else
    IO::uri_holder_t parent_uri = Glib::path_get_dirname (uri);
#endif
    
    RCP::ResourceSaver resource_saver (stream, parent_uri);
    
    // Export blobs before anything else, to allow #includes and whatnot
    for (std::set<Resource*>::const_iterator i = resources.begin ();
         i != resources.end (); i++)
    {
        if ((*i)->get_type () == Resources::RESOURCE_BLOB)
            (*i)->apply_visitor (resource_saver);
    }
    
    // Export per-app data
    resource_saver.save_app (manager->get_application ());
    
    // Export the rest of the resources
    for (std::set<Resource*>::const_iterator i = resources.begin ();
	 i != resources.end (); i++)
    {
        if ((*i)->get_type () != Resources::RESOURCE_BLOB)
            (*i)->apply_visitor (resource_saver);
    }

    // Write buffer to URI
    const std::string stream_str = stream.str ();
    IO::save_uri (uri, (const unsigned char*)stream_str.c_str (), stream_str.length ());
}

std::string RCP::string_to_rcp (const std::string &src)
{
    std::string escaped_string;

    for (std::string::const_iterator i = src.begin (); i != src.end (); ++i)
    {
	if (*i == '\\')
	{
	    // Peek at next character
	    std::string::const_iterator next = i;
	    next++;
	    
	    if (next != src.end ())
	    {
		// Pass double backslashes through
		if (*next == '\\')
		{
		    escaped_string += "\\\\";
		    i++;
		    continue;
		}
		
		// Don't pass single backslashes from before quotes
		if (*next == '"')
		{
		    i++;
		    continue;
		}
		
	    } else {
		
		// This is a trailing single backslash so convert it
		// to double backslash
		escaped_string += "\\\\";
		break;
	    }
	} 
	
	if (*i == '"')
	{
	    escaped_string += "\\\"";
	    continue;
	}
        
	if (*i == '\n')
	{
	    escaped_string += "\\r";
	    continue;
	}        

	escaped_string += *i;
    }

    return escaped_string;
}

void RCP::ResourceSaver::save_app (Resources::Application *app)
{
    if (app->version != "")
	ostr << "VERSION ID 1 \"" << app->version << "\"" << std::endl;
    if (app->iconname != "")
	ostr << "APPLICATIONICONNAME ID 1 \"" << app->iconname << "\"" << std::endl;
    if (app->vendor != "")
	ostr << "APPLICATION ID 1 \"" << app->vendor << "\"" << std::endl;
    ostr << std::endl;
}

void RCP::ResourceSaver::visit_resource (Resources::Blob *res)
{
    // We use contents.get_val() instead of contents to avoid the
    // escaping of quote marks
    
    ostr << "/* Contents of blob '" << res->id << "' */" << std::endl;
    ostr << res->contents.get_val () << std::endl << std::endl;
}


void RCP::ResourceSaver::visit_resource (Resources::Dialog *res)
{
    ostr << "ALERT ID " << res->id << std::endl;

    switch (res->dialog_type)
    {
    case Resources::Dialog::TYPE_INFORMATION:
	ostr << "INFORMATION" << std::endl;
	break;
    case Resources::Dialog::TYPE_CONFIRMATION:
	ostr << "CONFIRMATION" << std::endl;
	break;
    case Resources::Dialog::TYPE_WARNING:
	ostr << "WARNING" << std::endl;
	break;
    case Resources::Dialog::TYPE_ERROR:
	ostr << "ERROR" << std::endl;
	break;
    }
    
    ostr << "DEFAULTBUTTON " << res->default_button << std::endl;
    
    if (res->help_id != "")
	ostr << "  HELPID " << res->help_id << std::endl;

    ostr << "BEGIN" << std::endl;

    ostr << "  TITLE \"" << res->title << "\"" << std::endl
	 << "  MESSAGE \"" << res->text << "\"" << std::endl;

    const std::vector<std::string> &buttons = res->buttons;
    if (buttons.size ())
    {
	ostr << "  BUTTONS ";

	for (std::vector<std::string>::const_iterator i = buttons.begin ();
	     i !=  buttons.end (); i++)
	    ostr << "\"" << string_to_rcp (*i) << "\" ";
	
	ostr << std::endl;
    }
    
    ostr << "END" << std::endl << std::endl;
}

void RCP::ResourceSaver::visit_resource (Resources::Menu *res)
{
    ostr << "MENU ID " << res->id << std::endl;

    ostr << "BEGIN" << std::endl;

    const Resources::Menu::MenuTree &submenus = res->get_submenus ();
    for (Resources::Menu::MenuTree::const_iterator i = submenus.begin ();
	 i != submenus.end (); i++)
    {
	ostr << "  PULLDOWN \"" << i->label << "\"" << std::endl;
	
	ostr << "  BEGIN" << std::endl;
	for (std::vector<Resources::Menu::MenuItem>::const_iterator j = i->items.begin ();
	     j != i->items.end (); j++)
	{
	    if (j->separator)
		ostr << "    MENUITEM SEPARATOR" << std::endl;
	    else
	    {
		ostr << "    MENUITEM \"" << j->label << "\" ID " << j->id;
		if (j->shortcut)
		    ostr << " \"" << j->shortcut << "\"";
		ostr << std::endl;
	    }
	}
	ostr << "  END" << std::endl;
    }
    
    ostr << "END" << std::endl << std::endl;
}

void RCP::ResourceSaver::visit_resource (Resources::String *res)
{
    ostr << "STRING ID " << res->id << " \"" << res->text << "\"" << std::endl
	 << std::endl;
}

void RCP::ResourceSaver::visit_resource (Resources::StringList *res)
{
    ostr << "STRINGTABLE ID " << res->id
	 << " \"" << res->prefix << "\"" << std::endl;

    ostr << " ";
    const std::vector<std::string> &strings = res->strings;
    for (std::vector<std::string>::const_iterator i = strings.begin ();
	 i != strings.end (); i++)
	ostr << " \"" << string_to_rcp (*i) << "\"";
    
    ostr << std::endl << std::endl;
}

void RCP::ResourceSaver::visit_resource (Resources::Form *res)
{
    RCPSaver_funcs::save_res_form (res, ostr);
}
    
void RCP::ResourceSaver::visit_resource (Resources::Bitmap *res)
{
    std::string resource_type;
    switch (res->bitmap_type)
    {
    case Resources::Bitmap::TYPE_MONO:
        resource_type = "BITMAP";
        break;
    case Resources::Bitmap::TYPE_GREY_4:
        resource_type = "BITMAPGREY";
        break;
    case Resources::Bitmap::TYPE_GREY_16:
        resource_type = "BITMAPGREY16";
        break;
    case Resources::Bitmap::TYPE_COLOR_16:
        resource_type = "BITMAPCOLOR16";
        break;
    case Resources::Bitmap::TYPE_COLOR_256:
        resource_type = "BITMAPCOLOR";
        break;
    case Resources::Bitmap::TYPE_COLOR_16K:
	resource_type = "BITMAPCOLOR16K";
	break;
    }

    Glib::ustring bitmap_filename = res->id () + ".bmp";
#ifdef GUIKACHU_HAVE_GNOMEVFS
    res->save_file_bmp (parent_uri->append_file_name (bitmap_filename)->to_string ());
#else
    res->save_file_bmp (Glib::build_filename (parent_uri, bitmap_filename));
#endif
    
    ostr << resource_type << " ID " << res->id << " \"" << bitmap_filename << "\"" << std::endl;
    ostr << std::endl;
}

namespace
{
    void save_bitmapfamily_image (std::ostream &ostr, const IO::uri_holder_t &parent_uri,
                                  Resources::BitmapFamily *res,
                                  Resources::Bitmap::BitmapType type, int bpp)
    {
        if (!res->get_image (type))
            return;

        char *bitmap_filename_str = g_strdup_printf ("%s-%dbpp.bmp", res->id ().c_str (), bpp);
        Glib::ustring bitmap_filename (bitmap_filename_str);
        g_free (bitmap_filename_str);
#ifdef GUIKACHU_HAVE_GNOMEVFS
        res->save_file_bmp (type, parent_uri->append_file_name (bitmap_filename)->to_string ());
#else
        res->save_file_bmp (type, Glib::build_filename (parent_uri, bitmap_filename));
#endif        
            
        ostr << "  BITMAP " << " \"" << bitmap_filename << "\" "
             << "BPP " << bpp << std::endl;
    }
    
} // anonymous namespace

void RCP::ResourceSaver::visit_resource (Resources::BitmapFamily *res)
{

    ostr << "BITMAPFAMILY ID " << res->id << std::endl
         << "COMPRESS" << std::endl
         << "BEGIN" << std::endl;
    
    save_bitmapfamily_image (ostr, parent_uri, res, Resources::Bitmap::TYPE_MONO,      1);
    save_bitmapfamily_image (ostr, parent_uri, res, Resources::Bitmap::TYPE_GREY_4,    2);
    save_bitmapfamily_image (ostr, parent_uri, res, Resources::Bitmap::TYPE_GREY_16,   4);
    save_bitmapfamily_image (ostr, parent_uri, res, Resources::Bitmap::TYPE_COLOR_16,  5);
    save_bitmapfamily_image (ostr, parent_uri, res, Resources::Bitmap::TYPE_COLOR_256, 8);
    save_bitmapfamily_image (ostr, parent_uri, res, Resources::Bitmap::TYPE_COLOR_16K, 16);

    ostr << "END" << std::endl << std::endl;
}
