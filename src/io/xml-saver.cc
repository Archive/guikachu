//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "io/xml-saver.h"

#include "gnome-i18n-cnp.h"

#include <sigc++/object_slot.h>

#include "storage.h"

#include "resource-util.h"

namespace Guikachu
{
    
namespace IO
{

namespace XML
{
    static void save_app      (Resources::Application *app,    StorageNode &root_node);
    static void save_target   (Target                 *target, StorageNode &root_node);
    static void save_resource (Resource               *res,    StorageNode &root_node);
}
    
void XMLSaver::save (ResourceManager *manager, const Glib::ustring &uri) throw (Glib::Exception)
{
    Storage     storage;
    StorageNode root_node = storage.create_root ("guikachu");
    
    // Save per-application data
    XML::save_app (manager->get_application (), root_node);

    // Save target machine data
    XML::save_target (manager->get_target (), root_node);
    
    // Save resources
    std::set<Resource*> resources = manager->get_resources ();
    for (std::set<Resource*>::const_iterator i = resources.begin ();
	 i != resources.end (); i++)
	XML::save_resource (*i, root_node);

    storage.save_uri (uri);
}

    
namespace XML
{

static void save_app (Resources::Application *app, StorageNode &root_node)
{
    StorageNode node = root_node.add_node ("application");

    if (app->iconname != "")
        node.add_node ("iconname", app->iconname);
    if (app->version != "")
        node.add_node ("version",  app->version);
    if (app->vendor != "")
        node.add_node ("vendor",   app->vendor);

    Resources::Application::tag_map_t tags = app->get_tags ();
    if (tags.size ())
    {
        StorageNode tags_node = node.add_node ("tags");

        for (Resources::Application::tag_map_t::const_iterator i = tags.begin ();
             i != tags.end (); ++i)
        {
            StorageNode tag_node = tags_node.add_node ("tag");
            tag_node.set_prop ("name", i->first);
            tag_node.set_prop ("val", i->second);
        }
    }
}

static void save_target (Target *target, StorageNode &root_node)
{
    StorageNode node = root_node.add_node ("target");

    if (target->get_stock_id () != "")
    {
        node.set_prop ("stock_id", target->get_stock_id ());
        return;
    }
    
    StorageNode screen_node = node.add_node ("screen");
    screen_node.set_prop ("width",  target->screen_width);
    screen_node.set_prop ("height", target->screen_height);
    screen_node.set_prop_bool ("color",  target->screen_color);
}

static void save_resource (Resource *res, StorageNode &root_node)
{
    std::string curr_node_name = Resources::type_id_from_type (res->get_type ());
    
    StorageNode node = root_node.add_node (curr_node_name);
    node.set_prop ("id", res->id);

    ResourceSaver saver (node);
    res->apply_visitor (saver);
}

} // namespace XML
} // namespace IO
} // namespace Guikachu
