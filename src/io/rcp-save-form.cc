//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "rcp-save-form.h"
#include "rcp-saver-priv.h"

#include "form-editor/label.h"
#include "form-editor/button.h"
#include "form-editor/pushbutton.h"
#include "form-editor/graffiti.h"
#include "form-editor/selector-trigger.h"
#include "form-editor/checkbox.h"
#include "form-editor/list.h"
#include "form-editor/scrollbar.h"
#include "form-editor/slider.h"
#include "form-editor/text-field.h"
#include "form-editor/table.h"
#include "form-editor/popup-trigger.h"
#include "form-editor/formbitmap.h"
#include "form-editor/gadget.h"

#include <glib.h> // for g_strdup_printf et al.

namespace Guikachu
{    
    namespace IO
    {        
        namespace RCP
        {
            class WidgetSaver: public WidgetVisitor
            {
                std::ostream &ostr;
                
            public:
                explicit WidgetSaver (std::ostream &ostr_) : ostr (ostr_) {};
                
                void visit_widget (Widgets::Label           *widget);
                void visit_widget (Widgets::Button          *widget);
                void visit_widget (Widgets::PushButton      *widget);
                void visit_widget (Widgets::Graffiti        *widget);
                void visit_widget (Widgets::SelectorTrigger *widget);
                void visit_widget (Widgets::Checkbox        *widget);
                void visit_widget (Widgets::List            *widget);
                void visit_widget (Widgets::PopupTrigger    *widget);
                void visit_widget (Widgets::ScrollBar       *widget);
                void visit_widget (Widgets::Slider          *widget);
                void visit_widget (Widgets::TextField       *widget);
                void visit_widget (Widgets::Table           *widget);
                void visit_widget (Widgets::FormBitmap      *widget);
                void visit_widget (Widgets::Gadget          *widget);
            };
        }
    }
}

using namespace Guikachu;
using namespace Guikachu::IO;

namespace
{

    using Guikachu::IO::RCP::operator<<;
                                       
    void save_element_pos (Widget *widget, std::ostream &ostr)
    {
        ostr << " AT (" << widget->x << " " << widget->y << ")";
    } 
    
    template<class T>
    void save_element_pos_size (T *res, std::ostream &ostr)
    {
        ostr << " AT (" << res->x << " " << res->y << " "
             << res->get_width () << " " << res->get_height () << ")"
             << std::endl;
    }

    template<>
    void save_element_pos_size<Resources::Form> (Resources::Form *res, std::ostream &ostr)
    {
        ostr << " AT (" << res->x << " " << res->y << " "
             << res->width << " " << res->height << ")"
             << std::endl;
    }
    
    template<class T>
    void save_element_pos_autosize (T *res, std::ostream &ostr)
    {
        gchar *width_str = 0, *height_str = 0;
        
        if (res->manual_width)
            width_str = g_strdup_printf ("%d", int (res->width));
        else
            width_str = g_strdup ("AUTO");
        
        if (res->manual_height)
            height_str = g_strdup_printf ("%d", int (res->height));
        else
            height_str = g_strdup ("AUTO");
        
        ostr << " AT (" << res->x << " " << res->y
             << " " << width_str << " " << height_str << ")";
        
        g_free (width_str);
        g_free (height_str);
    }
    
    void save_flag (bool value, std::ostream &ostr,
                    const std::string &true_str,
                    const std::string &false_str = "")
    {
        if (value)
            ostr << true_str;
        else
            ostr << false_str;
    }
    
    void save_graphical (Widgets::Graphical *widget, std::ostream &ostr)
    {
        if (widget->bitmap_id != "" || widget->selected_bitmap_id != "")
        {
            ostr << " GRAPHICAL";
            if (widget->bitmap_id != "")
                ostr << " BITMAPID " << widget->bitmap_id;
            if (widget->selected_bitmap_id != "")
                ostr << " SELECTEDBITMAPID " << widget->selected_bitmap_id;
        }    
    }
    
} // Anonymous namespace
    
void RCPSaver_funcs::save_res_form (Resources::Form *res, std::ostream &ostr)
{
    ostr << "FORM ID " << res->id << std::endl;

    save_element_pos_size (res, ostr);
    ostr << "USABLE" << std::endl;

    save_flag (res->modal, ostr,      "MODAL ");
    save_flag (res->frame, ostr,      "FRAME ", "NOFRAME ");
    save_flag (res->savebehind, ostr, "SAVEBEHIND ", "NOSAVEBEHIND ");

    ostr << std::endl;
    
    if (res->help_id != "")
	ostr << "HELPID " << res->help_id << std::endl;

    if (res->menu_id != "")
	ostr << "MENUID " << res->menu_id << std::endl;

    if (res->def_button != "")
	ostr << "DEFAULTBTNID " << res->def_button << std::endl;

    const std::set<Widget*> &widgets = res->get_widgets ();
    RCP::WidgetSaver widget_saver (ostr);
    
    if (widgets.size () || res->title != "")
    {
	ostr << "BEGIN" << std::endl;
	ostr << "  TITLE \"" << res->title << "\"" << std::endl;
	
	for (std::set<Widget*>::const_iterator i = widgets.begin ();
	     i != widgets.end (); i++)
            (*i)->apply_visitor (widget_saver);
	    
	ostr << "END" << std::endl;
    }
    ostr << std::endl;
}

void RCP::WidgetSaver::visit_widget (Widgets::Label *widget)
{
    ostr << "  LABEL \"" << widget->text << "\"" << " ID " << widget->id;

    save_element_pos (widget, ostr);
    
    ostr << " FONT " << widget->font;

    save_flag (widget->usable, ostr, " USABLE", " NONUSABLE");

    ostr << std::endl;
}

void RCP::WidgetSaver::visit_widget (Widgets::Button *widget)
{    
    std::string frame_str, anchor_str;
    std::string type_str;

    if (!widget->repeat)
	type_str = "BUTTON";
    else
	type_str = "REPEATBUTTON";
    
    switch (widget->frame)
    {
    case Widgets::Button::FRAME_NONE:
	frame_str = "NOFRAME";
	break;
    case Widgets::Button::FRAME_SIMPLE:
	frame_str = "FRAME";
	break;
    case Widgets::Button::FRAME_BOLD:
	frame_str = "BOLDFRAME";
	break;
    }

    if (widget->anchor_right)
	anchor_str = "RIGHTANCHOR";
    else
	anchor_str = "LEFTANCHOR";
    
    ostr << "  " << type_str << " \"" << widget->text << "\" ID " << widget->id;
    save_element_pos_autosize (widget, ostr);
    
    ostr << " FONT " << widget->font;
    ostr << " " << frame_str;
    ostr << " " << anchor_str;
    save_flag (widget->usable, ostr,   " USABLE", " NONUSABLE");
    save_flag (widget->disabled, ostr, " DISABLED");
    save_graphical (widget, ostr);
    
    ostr << std::endl;
}

void RCP::WidgetSaver::visit_widget (Widgets::PushButton *widget)
{
    ostr << "  PUSHBUTTON \"" << widget->text << "\" ID " << widget->id;
    save_element_pos_autosize (widget, ostr);

    ostr << " FONT " << widget->font;
    ostr << " GROUP " << widget->group;
    save_flag (widget->usable, ostr,   " USABLE", " NONUSABLE");
    save_flag (widget->disabled, ostr, " DISABLED");
    save_graphical (widget, ostr);
    
    ostr << std::endl;
}

void RCP::WidgetSaver::visit_widget (Widgets::Graffiti *widget)
{
    ostr << "  GRAFFITISTATEINDICATOR";
    save_element_pos (widget, ostr);
    ostr << std::endl;
}

void RCP::WidgetSaver::visit_widget (Widgets::Checkbox *widget)
{
    ostr << "  CHECKBOX \"" << widget->text << "\" ID " << widget->id;
    save_element_pos_autosize (widget, ostr);

    ostr << " FONT " << widget->font;
    ostr << " GROUP " << widget->group;
    save_flag (widget->usable, ostr,       " USABLE", " NONUSABLE");
    save_flag (widget->disabled, ostr,     " DISABLED");
    save_flag (widget->anchor_right, ostr, " RIGHTANCHOR", " LEFTANCHOR");
    save_flag (widget->toggled, ostr,      " CHECKED");
    
    ostr << std::endl;
}

void RCP::WidgetSaver::visit_widget (Widgets::List *widget)
{
    gchar *width_str;

    if (widget->manual_width)
	width_str = g_strdup_printf ("%d", int (widget->width));
    else
	width_str = g_strdup ("AUTO");
    
    ostr << "  LIST";
    const std::vector<std::string> &items = widget->items;
    for (std::vector<std::string>::const_iterator i = items.begin ();
	 i != items.end (); i++)
	ostr << " \"" << string_to_rcp (*i) << "\"";
    
    ostr << " ID " << widget->id;
    ostr << " AT (" << widget->x << " " << widget->y << " " << width_str << " " << " AUTO)";
    ostr << " FONT " << widget->font;
    save_flag (widget->usable, ostr, " USABLE", " NONUSABLE");

    ostr << " VISIBLEITEMS " << widget->visible_items;
    
    ostr << std::endl;

    g_free (width_str);
}

void RCP::WidgetSaver::visit_widget (Widgets::SelectorTrigger *widget)
{
    ostr << "  SELECTORTRIGGER \"" << widget->text << "\" ID " << widget->id;
    save_element_pos_autosize (widget, ostr);

    ostr << " FONT " << widget->font;
    save_flag (widget->usable, ostr,       " USABLE", " NONUSABLE");
    save_flag (widget->disabled, ostr,     " DISABLED");
    save_flag (widget->anchor_right, ostr, " RIGHTANCHOR", " LEFTANCHOR");
    save_graphical (widget, ostr);
    
    ostr << std::endl;
}

void RCP::WidgetSaver::visit_widget (Widgets::PopupTrigger *widget)
{
    ostr << "  POPUPTRIGGER \"" << widget->text << "\" ID " << widget->id;
    save_element_pos_autosize (widget, ostr);

    ostr << " FONT " << widget->font;
    save_flag (widget->usable, ostr,       " USABLE", " NONUSABLE");
    save_flag (widget->disabled, ostr,     " DISABLED");
    save_flag (widget->anchor_right, ostr, " RIGHTANCHOR", " LEFTANCHOR");
    save_graphical (widget, ostr);
    
    ostr << std::endl;

    if (widget->list_id != "")
	ostr << "  POPUPLIST ID " << widget->id << " " << widget->list_id << std::endl;
}

void RCP::WidgetSaver::visit_widget (Widgets::ScrollBar *widget)
{
    ostr << "  SCROLLBAR ID " << widget->id;
    save_element_pos_size (widget, ostr);

    ostr << " VALUE "    << widget->value
	 << " MIN "      << widget->min_value
	 << " MAX "      << widget->max_value
	 << " PAGESIZE " << widget->page_size;
    ostr << std::endl;
}

void RCP::WidgetSaver::visit_widget (Widgets::Slider *widget)
{
    ostr << "  SLIDER ID " << widget->id;
    save_element_pos_size (widget, ostr);

    save_flag (widget->usable, ostr,   " USABLE", " NONUSABLE");
    save_flag (widget->disabled, ostr, " DISABLED");

    if (widget->thumb_id != "")
	ostr << " THUMBID " << widget->thumb_id;
   if (widget->background_id != "")
	ostr << " BACKGROUNDID " << widget->background_id;

    ostr << " VALUE "    << widget->value
	 << " MIN "      << widget->min_value
	 << " MAX "      << widget->max_value
	 << " PAGESIZE " << widget->page_size
	 << std::endl;
}

void RCP::WidgetSaver::visit_widget (Widgets::TextField *widget)
{
    gchar *height_str = 0;

    if (widget->manual_height)
	height_str = g_strdup_printf ("%d", int (widget->height));
    else
	height_str = g_strdup ("AUTO");
    
    ostr << "  FIELD ID " << widget->id;
    ostr << " AT (" << widget->x << " " << widget->y << " "
	 << widget->get_width () << " " << height_str << ")";
    
    g_free (height_str);

    ostr << " FONT " << widget->font;
    save_flag (widget->usable, ostr,        " USABLE", " NONUSABLE");
    save_flag (widget->disabled, ostr,      " DISABLED");
    save_flag (widget->editable, ostr,      " EDITABLE", " NONEDITABLE");
    save_flag (widget->multi_line, ostr,    " MULTIPLELINES", " SINGLELINE");
    save_flag (widget->justify_right, ostr, " RIGHTALIGN", " LEFTALIGN");
    save_flag (widget->underline, ostr,     " UNDERLINED");
    save_flag (widget->dynamic_size, ostr,  " DYNAMICSIZE");
    save_flag (widget->auto_shift, ostr,    " AUTOSHIFT");
    save_flag (widget->numeric, ostr,       " NUMERIC");
    save_flag (widget->has_scrollbar, ostr, " HASSCROLLBAR");

    if (widget->max_length)
	ostr << " MAXCHARS " << widget->max_length;
    else
	ostr << " MAXCHARS 32767";

    ostr << std::endl;
}

void RCP::WidgetSaver::visit_widget (Widgets::Table *widget)
{   
    ostr << "  TABLE ID " << widget->id;
    ostr << " AT (" << widget->x << " " << widget->y
	 << " " << widget->get_width () << " " << widget->get_height ()
	 << ")";
    ostr << " ROWS "   << widget->num_rows;

    const std::vector<int>& columns = widget->column_width;
    ostr << " COLUMNS " << widget->num_columns;
    ostr << " COLUMNWIDTHS ";
    for (int i = 0; i < widget->num_columns; i++)
	ostr << columns[i] << " ";
	
    ostr << std::endl;
}

void RCP::WidgetSaver::visit_widget (Widgets::FormBitmap *widget)
{
    ostr << "  FORMBITMAP";
    save_element_pos (widget, ostr);
    if (widget->bitmap_id != "")
        ostr << " BITMAP " << widget->bitmap_id;
    save_flag (widget->usable, ostr, " USABLE", " NONUSABLE");
	
    ostr << std::endl;
}

void RCP::WidgetSaver::visit_widget (Widgets::Gadget *widget)
{
    ostr << "  GADGET ID " << widget->id;
    save_element_pos_size (widget, ostr);
    save_flag (widget->usable, ostr, " USABLE", " NONUSABLE");
	
    ostr << std::endl;
}
