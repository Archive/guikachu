//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_STORAGE_NODE_HANDLERS_H
#define GUIKACHU_STORAGE_NODE_HANDLERS_H

#include "io/storage-processor.h"
#include "property.h"
#include <vector>

namespace Guikachu
{
    namespace StorageNodeHandlers
    {
	typedef StorageProcessor::NodeHandler HandlerBase;

	class Flag: public HandlerBase
	{
	    Property<bool> &target;
	public:
	    explicit Flag (Property<bool> &target);
	    virtual void operator() (const StorageNode &node);
	};

	class StringContents: public HandlerBase
	{
	    Property<std::string> &target;
	public:
	    explicit StringContents (Property<std::string> &target);
	    virtual void operator() (const StorageNode &node);
	};

	class IntProp: public HandlerBase
	{
	    const std::string  prop_name;
	    Property<int>     &target;
	public:
	    IntProp (const std::string &prop_name, Property<int> &target);
	    virtual void operator() (const StorageNode &node);
	};

	class StringProp: public HandlerBase
	{
	    const std::string      prop_name;
	    Property<std::string> &target;
	public:
	    StringProp (const std::string &prop_name, Property<std::string> &target);
	    virtual void operator() (const StorageNode &node);
	};
	
	class StringListProp: public HandlerBase
	{
	    typedef std::vector<std::string> list_t;

	    list_t             temp_list;
	    const std::string  child_name, prop_name;
	    Property<list_t>  &target;
	public:
	    StringListProp (const std::string &child_name,
			    const std::string &prop_name,
			    Property<list_t>  &target);
	    virtual ~StringListProp ();
	    virtual void operator() (const StorageNode &node);
	};
	
	class StringListContents: public HandlerBase
	{
	    typedef std::vector<std::string> list_t;

	    list_t             temp_list;
	    const std::string  child_name;
	    Property<list_t>  &target;
	public:
	    StringListContents (const std::string &child_name, Property<list_t>  &target);
	    virtual ~StringListContents ();
	    virtual void operator() (const StorageNode &node);
	};
    }
}

#endif /* !GUIKACHU_IO_GUIKACHU_LOAD_H */
