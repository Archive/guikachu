//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_PREFERENCES_H
#define GUIKACHU_PREFERENCES_H

#include <glibmm/ustring.h>
#include <list>
#include <sigc++/signal.h>

namespace Guikachu
{
    namespace Preferences
    {
	void init ();

	namespace Interface
	{
	    extern sigc::signal0<void> recent_files_changed;
	    typedef std::list<Glib::ustring> RecentFiles;
	    
	    RecentFiles get_recent_files ();
	    void add_recent_file (const Glib::ustring &uri);

	    int get_undo_size ();
	}

        namespace MainWin
        {
            bool get_default_grouped ();
        }
        
	namespace FormEditor
	{
	    extern sigc::signal0<void> colors_changed;
	    std::string get_color_fg ();
	    std::string get_color_disabled ();
	    std::string get_color_bg ();
	    std::string get_color_selection ();

	    void set_colors (const std::string &color_fg,
			     const std::string &color_disabled,
			     const std::string &color_bg,
			     const std::string &selection);

	    float get_default_zoom ();
	    void  set_default_zoom (float default_zoom);
	}
    }
}

#endif /* !GUIKACHU_PREFERENCES_H */
