//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "menu-win-treemodel.h"

#include "menu-res-ops.h"

#include <gtkmm/treepath.h>

using namespace Guikachu::GUI;

MenuWindow_Helpers::MenuTreeModel::MenuTreeModel (Resources::Menu *res_):
    Glib::ObjectBase (typeid (MenuTreeModel)),
    Glib::Object (),
    res (res_),
    stamp (1)
{
    update ();

    res->changed.connect (sigc::mem_fun (*this, &MenuTreeModel::update));
}

Glib::RefPtr<MenuWindow_Helpers::MenuTreeModel> MenuWindow_Helpers::MenuTreeModel::create (Resources::Menu *res)
{
    return Glib::RefPtr<MenuTreeModel> (new MenuTreeModel (res));
}

Gtk::TreeModelFlags MenuWindow_Helpers::MenuTreeModel::get_flags_vfunc () const
{
    return Gtk::TreeModelFlags ();
}

int MenuWindow_Helpers::MenuTreeModel::get_n_columns_vfunc () const
{
    return 5;
}

GType MenuWindow_Helpers::MenuTreeModel::get_column_type_vfunc (int index) const
{
    if (index >= get_n_columns_vfunc ())
        return 0;

    switch (index)
    {
    case 0: return G_TYPE_BOOLEAN; // is_submneu
    case 1: return G_TYPE_STRING;  // label
    case 2: return G_TYPE_STRING;  // ID (for items only)
    case 3: return G_TYPE_CHAR;    // shortcut (for items only)
    case 4: return G_TYPE_BOOLEAN; // is_separator
    }

    return 0;
}

namespace
{
    template<typename T>
    void set_gvalue (Glib::ValueBase &target, const T &src)
    {
        Glib::Value<T> value_specific;
        value_specific.init (Glib::Value<T>::value_type ());
        value_specific.set (src);

        target.init (Glib::Value<T>::value_type ());
        target = value_specific;
    }
    
} // anonymous namespace


void MenuWindow_Helpers::MenuTreeModel::get_value_vfunc (const Gtk::TreeModel::iterator& iter,
                                                         int                             column,
                                                         Glib::ValueBase                &value) const
{
    if (!iter_is_valid (iter))
        return;
    
    if (column >= get_n_columns_vfunc ())
        return;
    
    submenu_idx_t submenu_idx;
    item_idx_t    item_idx;
    bool          is_submenu;
    read_iter (iter, submenu_idx, item_idx, is_submenu);

    switch (column)
    {
    case 0:
        set_gvalue<bool> (value, is_submenu);
        break;
    case 1:
        set_gvalue<Glib::ustring> (value, is_submenu ?
                                   submenus[submenu_idx].label : submenus[submenu_idx].items[item_idx].label);
        break;
    case 2:
        set_gvalue<Glib::ustring> (value, is_submenu ?
                                   "" : submenus[submenu_idx].items[item_idx].id);
        break;
    case 3:
        set_gvalue<char> (value, is_submenu ?
                          0 : submenus[submenu_idx].items[item_idx].shortcut);
        break;
    case 4:
        set_gvalue<bool> (value, is_submenu ?
                          false : submenus[submenu_idx].items[item_idx].separator);
        break;
    }
}

void MenuWindow_Helpers::MenuTreeModel::fill_iter (iterator      &iter,
                                                   submenu_idx_t  submenu_idx) const
{
    fill_iter (iter, submenu_idx, submenus[submenu_idx].items.size ());
}

void MenuWindow_Helpers::MenuTreeModel::fill_iter (iterator      &iter,
                                                   submenu_idx_t  submenu_idx,
                                                   item_idx_t     item_idx) const
{
    iter.set_stamp (stamp);
    iter.gobj ()->user_data = GINT_TO_POINTER (submenu_idx);
    iter.gobj ()->user_data2 = GINT_TO_POINTER (item_idx);
}

void MenuWindow_Helpers::MenuTreeModel::read_iter (const iterator &iter,
                                                   submenu_idx_t  &submenu_idx,
                                                   item_idx_t     &item_idx,
                                                   bool           &is_submenu) const
{
    if (!iter_is_valid (iter))
        return;

    submenu_idx = GPOINTER_TO_INT (iter.gobj ()->user_data);
    item_idx =  GPOINTER_TO_INT (iter.gobj ()->user_data2);
    is_submenu = (submenus[submenu_idx].items.size () == item_idx);
}


bool MenuWindow_Helpers::MenuTreeModel::iter_next_vfunc (const iterator &iter, iterator &iter_next) const
{
    iter_next = iterator ();

    if (!iter_is_valid (iter))
        return false;
    
    submenu_idx_t submenu_idx;
    item_idx_t    item_idx;
    bool          is_submenu;
    read_iter (iter, submenu_idx, item_idx, is_submenu);

    if (is_submenu)
    {
        submenu_idx++;
        if (submenu_idx == submenus.size ())
            return false;
        fill_iter (iter_next, submenu_idx);
        
    } else {
        
        item_idx++;
        if (item_idx == submenus[submenu_idx].items.size ())
            return false;
        fill_iter (iter_next, submenu_idx, item_idx);
    }
    
    return true;
}

int MenuWindow_Helpers::MenuTreeModel::iter_n_root_children_vfunc() const
{
    return submenus.size ();
}

bool MenuWindow_Helpers::MenuTreeModel::iter_parent_vfunc (const iterator &iter, iterator &parent) const
{
    parent = iterator ();

    if (!iter_is_valid (iter))
        return false;

    submenu_idx_t submenu_idx;
    item_idx_t    item_idx;
    bool          is_submenu;
    read_iter (iter, submenu_idx, item_idx, is_submenu);

    if (is_submenu)
        return false;

    fill_iter (parent, submenu_idx);
    return true;    
}

int MenuWindow_Helpers::MenuTreeModel::iter_n_children_vfunc (const iterator &iter) const
{
    if (!iter_is_valid (iter))
        return -1;
    
    
    submenu_idx_t submenu_idx;
    item_idx_t    item_idx;
    bool          is_submenu;
    read_iter (iter, submenu_idx, item_idx, is_submenu);

    if (is_submenu)
        return item_idx;
    else
        return 0;
}

bool MenuWindow_Helpers::MenuTreeModel::iter_has_child_vfunc (const iterator &iter) const
{   
    if (!iter_is_valid (iter))
        return false;
    
    return iter_n_children_vfunc (iter);
}

bool MenuWindow_Helpers::MenuTreeModel::iter_children_vfunc (const iterator &iter, iterator &child) const
{
    return iter_nth_child_vfunc (iter, 0, child);
}

bool MenuWindow_Helpers::MenuTreeModel::iter_nth_child_vfunc (const iterator &iter, int n, iterator &child) const
{
    child = iterator();

    if (!iter_is_valid (iter))
        return false;
    
    submenu_idx_t submenu_idx;
    item_idx_t    item_idx;
    bool          is_submenu;
    read_iter (iter, submenu_idx, item_idx, is_submenu);

    if (!is_submenu)
        return false;

    if (submenu_idx_t (n) < submenus[submenu_idx].items.size ())
    {
        fill_iter (child, submenu_idx, n);
        return true;
    }

    return false;
}

Gtk::TreeModel::Path MenuWindow_Helpers::MenuTreeModel::get_path_vfunc (const iterator &iter) const
{
    if (!iter_is_valid (iter))
        return Gtk::TreeModel::Path ();
    
    submenu_idx_t submenu_idx;
    item_idx_t    item_idx;
    bool          is_submenu;
    read_iter (iter, submenu_idx, item_idx, is_submenu);

    Gtk::TreeModel::Path path;

    path.push_back (submenu_idx);
    if (!is_submenu)
        path.push_back (item_idx);
    
    return path;
}

bool MenuWindow_Helpers::MenuTreeModel::get_iter_vfunc (const Path &path, iterator &iter) const
{
    iter = iterator ();

    if (!submenus.size ())
        return false;
    
    if (!path.size () || path.size () > 2)
        return false;

    if (path.size () == 1)
        fill_iter (iter, path[0]);
    else
        fill_iter (iter, path[0], path[1]);

    return true;
}

bool MenuWindow_Helpers::MenuTreeModel::iter_is_valid (const iterator &iter) const
{
    return (iter.get_stamp () == stamp) && Gtk::TreeModel::iter_is_valid (iter);
}

void MenuWindow_Helpers::MenuTreeModel::update ()
{
    invalidate ();

    submenus = res->get_submenus ();

    // Pass 1: Add/remove submenus
    Gtk::TreeModel::Path submenu_path (1), item_path (2);
    for (submenu_idx_t i = submenus.size (); i < submenus_old.size (); ++i)
    {
        submenu_path[0] = submenus.size ();
        row_deleted (submenu_path);
    }
    for (submenu_idx_t i = submenus_old.size (); i < submenus.size (); ++i)
    {
        submenu_path[0] = i;
        row_inserted (submenu_path, get_iter (submenu_path));
    }

    // Pass 2: Add/remove menu items
    for (submenu_idx_t i = 0; i != submenus.size (); ++i)
    {
        submenu_path[0] = i;
        item_path[0] = i;
        
        if (i < submenus_old.size ()) // Add/remove only new/superflous items
        {
            for (item_idx_t j = submenus[i].items.size (); j < submenus_old[i].items.size (); ++j)
            {
                item_path[1] = submenus[i].items.size ();
                row_deleted (item_path);
            }
            for (item_idx_t j = submenus_old[i].items.size (); j < submenus[i].items.size (); ++j)
            {
                item_path[1] = j;
                row_inserted (item_path, get_iter (item_path));
            }
        } else {  // Add all items
            for (item_idx_t j = 0; j != submenus[i].items.size (); ++j)
            {
                item_path[1] = j;
                row_inserted (item_path, get_iter (item_path));
            }            
        }

        for (item_idx_t j = 0; j != submenus[i].items.size (); ++j)
        {
            item_path[1] = j;
            row_changed (item_path, get_iter (item_path));
        }
        
        row_changed (submenu_path, get_iter (submenu_path));
        row_has_child_toggled (submenu_path, get_iter (submenu_path));
    }

    submenus_old = submenus;
}

void MenuWindow_Helpers::MenuTreeModel::invalidate ()
{
    stamp += 2;
}
