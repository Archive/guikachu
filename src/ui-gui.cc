//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <glib/gi18n.h>

#include "ui-gui.h"
#include "session.h"
#include "io/guikachu-io.h"

#include <gtkmm/alignment.h>
#include <gtkmm/label.h>
#include <gtkmm/image.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/main.h>
#include <gtkmm/aboutdialog.h>

#include "guikachu-glade.c"

#include "config.h"
#ifdef GUIKACHU_HAVE_LIBGNOMEUI
#include <libgnome/gnome-url.h>
#endif

#include <list>
#include <iostream>

using namespace Guikachu;

namespace {
#include "pixmaps/inline-pixbufs.h"
}

void UI::flush_events ()
{
    if (Guikachu::Main::initialized ())
	while (Gtk::Main::events_pending ())
	    Gtk::Main::iteration ();
}

namespace {
#ifdef GUIKACHU_HAVE_LIBGNOMEUI
    void about_uri_cb (Gtk::AboutDialog &about, const Glib::ustring &uri)
    {
        gnome_url_show (uri.c_str (), 0);
    }
#endif
    
} // anonymous namespace

void UI::show_about (Gtk::Window *parent)
{
    std::list<Glib::ustring> authors;
    authors.push_back ("Gerg\xC5\x91 \xC3\x89rdi\thttp://cactus.rulez.org/");
    authors.push_back ("Jay Bloodworth");
    authors.push_back ("Christopher Keith Fairbairn");
    authors.push_back ("Tobias Gruetzmacher");
    authors.push_back ("Nathan Kurz");

    std::list<Glib::ustring> documenters;

    static Glib::RefPtr<Gdk::Pixbuf> logo = Gdk::Pixbuf::create_from_inline (-1, guikachu_logo_gdkbuf);
    
    // note to translators: please fill in your names and email addresses
    Glib::ustring translators = _("translator_credits");
    if (translators == "translator_credits")
	translators = "";

    static const char *license =
        "This program is free software; you can redistribute it and/or modify it "
        "under the terms of the GNU General Public License version 2 as published "
        "by the Free Software Foundation.\n"
        "\n"
        "This program is distributed in the hope that it will be useful but WITHOUT "
        "ANY WARRANTY; without even the implied warranty of MERCHANTABILITY "
        "or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public "
        "License for more details.\n"
        "\n"
        "You should have received a copy of the GNU General Public License along "
        "with this program; if not, write to the Free Software Foundation, Inc., "
        "59 Temple Place, Suite 330, Boston, MA 02111-1307 USA";
    
    Gtk::AboutDialog *about = new Gtk::AboutDialog;
#ifdef GUIKACHU_HAVE_LIBGNOMEUI
    about->set_url_hook (sigc::ptr_fun (about_uri_cb));
#endif
    
    about->set_name ("Guikachu");
    about->set_version (VERSION);
    about->set_copyright ("Copyright \xC2\xA9 2001-2007 Gerg\xC5\x91 \xC3\x89rdi");
    about->set_comments (_("PalmOS resource file editor"));
    about->set_website ("http://cactus.rulez.org/projects/guikachu/");
    about->set_website_label (_("Visit the Guikachu website"));

    about->set_authors (authors);
    //about->set_documenters (documenters);
    if (translators != "")
        about->set_translator_credits (translators);

    about->set_logo (logo);
    about->set_license (license);
    about->set_wrap_license (true);

    if (parent)
        about->set_transient_for (*parent);
    about->run ();
    delete about;
}

void UI::show_error (const Glib::ustring &full_message,
		     const Glib::ustring &short_message)
{
    if (Guikachu::Main::initialized ())
    {
	Gtk::MessageDialog dialog (full_message, true, Gtk::MESSAGE_ERROR,
                                   Gtk::BUTTONS_OK, true);
        dialog.run ();
    } else {
	if (short_message != "")
	    std::cerr << _("Guikachu: Error: ") << short_message << std::endl;
    }
}

void UI::show_warning (const Glib::ustring &full_message,
		       const Glib::ustring &short_message)
{
    if (Guikachu::Main::initialized ())
    {
	Gtk::MessageDialog dialog (full_message, true, Gtk::MESSAGE_WARNING,
                                   Gtk::BUTTONS_OK, true);
        dialog.run ();
    } else {
	if (short_message != "")
	    std::cerr << _("Guikachu: Warning: ") << short_message << std::endl;
    }
}

void UI::show_error_io_open (const Glib::ustring &uri, const Glib::ustring &error_msg)
{    
    char *buffer = g_strdup_printf (
	_("<big><b>Unable to open \"%s\"</b></big>\n"
          "%s"),
	UI::visible_filename (uri).c_str () , error_msg.c_str ());

    UI::show_error (buffer, uri + ":" + error_msg);
    g_free (buffer);
}
    
void UI::show_error_io_save (const Glib::ustring &uri, const Glib::ustring &error_msg)
{    
    char *buffer = g_strdup_printf (
	_("<big><b>Unable to save \"%s\"</b></big>\n"
          "%s"),
	UI::visible_filename (uri).c_str (), error_msg.c_str ());

    UI::show_error (buffer, uri + ":" + error_msg);
    g_free (buffer);
}

void UI::show_error_io_load_resource (const Glib::ustring &uri, const std::string &id,
                                      const Glib::ustring &error_msg)
{
    char *buffer;

    if (id == "")
        buffer = g_strdup_printf (
            _("<big><b>Error while loading \"%s\"</b></big>\n"
              "%s"),
            id.c_str (), error_msg.c_str ());
    else
        buffer = g_strdup_printf (
            _("<big><b>Error while loading \"%s\" from \"%s\"</b></big>\n"
              "%s"),
            id.c_str (), UI::visible_filename (uri).c_str (), error_msg.c_str ());

    UI::show_error (buffer, uri + ":" + error_msg);
    g_free (buffer);
}

void UI::show_error_io_save_resource (const Glib::ustring &uri, const std::string &id,
                                      const Glib::ustring &error_msg)
{
    char *buffer;

    if (id == "")
        buffer = g_strdup_printf (
            _("<big><b>Error while saving \"%s\"</b></big>\n"
              "%s"),
            id.c_str (), error_msg.c_str ());
    else
        buffer = g_strdup_printf (
            _("<big><b>Error while saving \"%s\" to \"%s\"</b></big>\n"
              "%s"),
            id.c_str (), UI::visible_filename (uri).c_str (), error_msg.c_str ());

    UI::show_error (buffer, uri + ":" + error_msg);
    g_free (buffer);
}


Gtk::Button * UI::create_stock_button (const Gtk::StockID  &stock_id,
                                       const Glib::ustring &title)
{
    Gtk::Button *button = new Gtk::Button;
    
    Gtk::Alignment *alignment = new Gtk::Alignment (0.5, 0.5, 0, 0);
    Gtk::HBox *hbox = new Gtk::HBox (false, 2);
    hbox->pack_start (*manage (new Gtk::Image (stock_id, Gtk::ICON_SIZE_BUTTON)), Gtk::PACK_SHRINK);
    hbox->pack_start (*manage (new Gtk::Label (title, true)), Gtk::PACK_SHRINK);
    alignment->add (*manage (hbox));

    alignment->show_all ();
    button->add (*manage (alignment));
    return button;
}

Glib::RefPtr<Gnome::Glade::Xml> UI::glade_gui (const Glib::ustring &root)
{
    return Gnome::Glade::Xml::create_from_buffer (guikachu_glade, sizeof guikachu_glade, root);
}
