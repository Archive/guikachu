//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <glib/gi18n.h>

#include "bitmapfamily-res-ops.h"

#include <glib.h> // for g_assert

using namespace Guikachu;
using namespace Guikachu::ResourceOps::BitmapFamilyOps;

namespace {
    Glib::ustring translate_tagged_string (const char *msgid)
    {
        const char *msgval = gettext (msgid);
        if (msgval == msgid)
            msgval = strrchr (msgid, '|') + 1;
        return msgval;
    }
    
    Glib::ustring get_change_label (Resources::Bitmap::BitmapType type)
    {
        const char *msgid = 0;

        switch (type)
        {
        case Resources::Bitmap::TYPE_MONO:
            // Translators: The part before the | shouldn't be
            // included in the translated string
            msgid = N_("MONO|Change monochrome image of %s");
            break;
        case Resources::Bitmap::TYPE_GREY_4:
            msgid = N_("GREY_4|Change greyscale image of %s");
            break;
        case Resources::Bitmap::TYPE_GREY_16:
            msgid = N_("GREY_16|Change greyscale image of %s");
            break;
        case Resources::Bitmap::TYPE_COLOR_16:
            msgid = N_("COLOR_16|Change color image of %s");
            break;
        case Resources::Bitmap::TYPE_COLOR_256:
            msgid = N_("COLOR_256|Change color image of %s");
            break;
        case Resources::Bitmap::TYPE_COLOR_16K:
            msgid = N_("COLOR_16K|Change color image of %s");
            break;
        }
        
        return translate_tagged_string (msgid);
    }

    Glib::ustring get_remove_label (Resources::Bitmap::BitmapType type)
    {
        const char *msgid = 0;

        switch (type)
        {
        case Resources::Bitmap::TYPE_MONO:
            msgid = N_("MONO|Remove monochrome image from %s");
            break;
        case Resources::Bitmap::TYPE_GREY_4:
            msgid = N_("GREY_4|Remove greyscale image from %s");
            break;
        case Resources::Bitmap::TYPE_GREY_16:
            msgid = N_("GREY_16|Remove greyscale image from %s");
            break;
        case Resources::Bitmap::TYPE_COLOR_16:
            msgid = N_("COLOR_16|Remove color image from %s");
            break;
        case Resources::Bitmap::TYPE_COLOR_256:
            msgid = N_("COLOR_256|Remove color image from %s");
            break;
        case Resources::Bitmap::TYPE_COLOR_16K:
            msgid = N_("COLOR_16K|Remove color image from %s");
            break;
        }
        
        return translate_tagged_string (msgid);
    }
    
} // anonymous namespace

BitmapFamilyHolder::BitmapFamilyHolder (Resources::BitmapFamily *res) :
    manager (res->get_manager ()),
    serial (res->get_serial ())
{
}

Resources::BitmapFamily * BitmapFamilyHolder::get_resource () const
{
    Resource *res = manager->get_resource (serial);
    g_assert (res);

    return static_cast<Resources::BitmapFamily*> (res);
}


ImageChangeOp::ImageChangeOp (Resources::BitmapFamily            *res,
                              Resources::Bitmap::BitmapType       type_,
                              const Resources::Bitmap::ImageData &old_image_) :
    BitmapFamilyHolder (res),
    type (type_),
    old_image (old_image_),
    new_image (res->get_image (type))
{
    char *label_str = g_strdup_printf (get_change_label (type).c_str (), res->id ().c_str ());
    op_label = label_str;
    g_free (label_str);
}

void ImageChangeOp::undo ()
{
    Resources::BitmapFamily *res = get_resource ();
    res->set_image (type, old_image);
}

void ImageChangeOp::redo ()
{
    Resources::BitmapFamily *res = get_resource ();
    res->set_image (type, new_image);
}


ImageRemoveOp::ImageRemoveOp (Resources::BitmapFamily       *res,
                              Resources::Bitmap::BitmapType  type_) :
    BitmapFamilyHolder (res),
    type (type_),
    old_image (res->get_image (type))
{
    char *label_str = g_strdup_printf (get_remove_label (type).c_str (), res->id ().c_str ());
    op_label = label_str;
    g_free (label_str);
}

void ImageRemoveOp::undo ()
{
    Resources::BitmapFamily *res = get_resource ();
    res->set_image (type, old_image);
}

void ImageRemoveOp::redo ()
{
    Resources::BitmapFamily *res = get_resource ();
    res->set_image (type, Resources::Bitmap::ImageData ());
}
