//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_CELLRENDERER_ICONTEXT_H
#define GUIKACHU_CELLRENDERER_ICONTEXT_H

#include <gtkmm/cellrendererpixbuf.h>
#include <gtkmm/cellrenderertext.h>

namespace Guikachu
{
    namespace GUI
    {        
        class CellRendererIconText: public Gtk::CellRendererText
        {
            Gtk::CellRendererPixbuf cell_pixbuf;
            
        public:
            CellRendererIconText ();
            
            Glib::PropertyProxy<Glib::RefPtr<Gdk::Pixbuf> > property_pixbuf ();

            Gtk::CellRendererPixbuf & get_pixbuf_cell ();

        protected:
            void get_size_vfunc (Gtk::Widget          &widget,
                                 const Gdk::Rectangle *cell_area,
                                 int                  *x_offset,
                                 int                  *y_offset,
                                 int                  *width,
                                 int                  *height) const;
            
            void render_vfunc (const Glib::RefPtr<Gdk::Drawable> &window,
                               Gtk::Widget                       &widget,
                               const Gdk::Rectangle              &background_area,
                               const Gdk::Rectangle              &cell_area,
                               const Gdk::Rectangle              &expose_area,
                               Gtk::CellRendererState             flags);
            
            Gtk::CellEditable * start_editing_vfunc (GdkEvent               *event,
                                                     Gtk::Widget            &widget,
                                                     const Glib::ustring    &path,
                                                     const Gdk::Rectangle   &background_area,
                                                     const Gdk::Rectangle   &cell_area,
                                                     Gtk::CellRendererState  flags);
        private:
            void editable_cb ();
        };
    }
}

#endif /* !GUIKACHU_CELLRENDERER_ICONTEXT */
