//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_RESOURCE_MANAGER_OPS_H
#define GUIKACHU_RESOURCE_MANAGER_OPS_H

#include "resource-manager.h"
#include "storage-node.h"

namespace Guikachu
{
    namespace ResourceOps
    {
	class RemoveOp: public UndoOp
	{
	    Resource         *resource;
	    ResourceManager  *manager;
	
	    Resources::Type   type;
	    std::string       id;
	    serial_t          serial;
	    StorageNode       node;
	
	    Glib::ustring     label;
	
	public:
	    RemoveOp (Resource *resource);
	    virtual ~RemoveOp () {};
	
	    Glib::ustring get_label () const;
	
	    void undo ();
	    void redo ();
	};

	class CreateOp: public UndoOp
	{
	    ResourceManager *manager;

	    Resources::Type  type;
	    std::string      id;
	    serial_t         serial;

	    Glib::ustring    label;
	
	public:
	    CreateOp (Resource *resource);
	    virtual ~CreateOp () {};
	
	    Glib::ustring get_label () const;
	
	    void undo ();
	    void redo ();	
	};

    } // namespace ResourceOps
} // namespace Guikachu

#endif /* !GUIKACHU_RESOURCE_MANAGER_OPS_H */
