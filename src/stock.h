//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_STOCK_H
#define GUIKACHU_STOCK_H

#include <gtkmm/stockid.h>

namespace Guikachu
{
    namespace UI
    {
        void register_stock ();
    }

    namespace GUI
    {
        namespace Stock
        {
            extern const Gtk::StockID CENTER_HORIZ;
            extern const Gtk::StockID CENTER_VERT;
            extern const Gtk::StockID ALIGN_LEFT;
            extern const Gtk::StockID ALIGN_RIGHT;
            extern const Gtk::StockID ALIGN_TOP;
            extern const Gtk::StockID ALIGN_BOTTOM;
        }
    }
}

#endif /* !GUIKACHU_STOCK_H */
