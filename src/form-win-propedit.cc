//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "form-win.h"

#include <glib/gi18n.h>

#include "form-editor/widget-factory.h"

using namespace Guikachu::GUI;
using namespace Guikachu::Resources;

void FormWindow::edited_widget_changed ()
{
    g_return_if_fail (edited_widget != 0);

    property_window->set_title (Glib::ScopedPtr<char> (
	g_strdup_printf (_("Properties: %s"), edited_widget->id ().c_str ())).get ());
}

void FormWindow::show_property_editor (Widget *widget)
{
    if (edited_widget == widget)
    {
	// Just raise the property editor	
	property_window->show_all ();
	property_window->raise ();
	
	return;
    }

    edited_widget = widget;

    // Update property editor window's title
    edited_widget_changed_conn.disconnect ();
    edited_widget_changed_conn = edited_widget->changed.connect (
	sigc::mem_fun (*this, &FormWindow::edited_widget_changed));
    edited_widget_changed ();

    // Change property editor
    show_property_editor (widget);
    Gtk::Widget *editor = FormEditor::get_widget_editor (widget);
    
    property_window->remove ();
    property_window->add (*editor);
    
    property_window->show_all ();
    property_window->raise ();
}

void FormWindow::show_form_property_editor ()
{
    edited_widget = 0;
    edited_widget_changed_conn.disconnect ();
    
    Gtk::Widget *editor = form_widget->get_editor ();
    
    if (property_window->get_child () != editor)
    {
	property_window->remove ();
	property_window->add (*editor);
    }
    
    property_window->set_title (Glib::ScopedPtr<char> (
	g_strdup_printf (_("Properties: %s"), res->id ().c_str())).get ());
    
    property_window->show_all ();
    property_window->raise ();
}
