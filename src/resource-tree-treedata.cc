//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <glib/gi18n.h>

#include "resource-tree.h"

#include "resource-factory.h"
#include "resource-manager-ops.h"
#include "resource-util-gui.h"

#include "cellrenderer-icontext.h"
#include "cellrenderer-indent.h"

using namespace Guikachu::GUI;
using namespace Guikachu;

ResourceTree::TreeData::TreeData () :
    type (TREEDATA_NULL)
{
}

ResourceTree::TreeData::TreeData (Resources::Application *app_) :
    type (TREEDATA_APP),
    app (app_)
{
}

ResourceTree::TreeData::TreeData (Resources::Type category_) :
    type (TREEDATA_CATEGORY),
    category (category_)
{
}

ResourceTree::TreeData::TreeData (Resource *res_) :
    type (TREEDATA_RESOURCE),
    res (res_)
{
}

Resource * ResourceTree::TreeData::get_resource () const
{
    if (type != TREEDATA_RESOURCE)
        return 0;

    return res;
}

Glib::ustring ResourceTree::TreeData::get_label () const
{
    switch (type)
    {
    case TREEDATA_APP:
        if (app->iconname != "")
            return Glib::ustring (app->iconname);
        else
            return _("Application");
    case TREEDATA_RESOURCE:
        return Glib::ustring (res->id);
    case TREEDATA_CATEGORY:
        switch (category)
        {
        case Resources::RESOURCE_DIALOG:
        case Resources::RESOURCE_FORM:
            return _("Forms and Dialogs");

        case Resources::RESOURCE_STRINGLIST:
        case Resources::RESOURCE_STRING:
            return _("Strings");
            
        case Resources::RESOURCE_MENU:
            return _("Menus");
            
        case Resources::RESOURCE_BITMAPFAMILY:
        case Resources::RESOURCE_BITMAP:
            return _("Bitmaps");
            
        case Resources::RESOURCE_BLOB:
            return _("RCP Fragments");
            
        case Resources::RESOURCE_NONE:
            g_assert_not_reached ();
        }
    case TREEDATA_NULL:
        g_assert_not_reached ();
    }

    g_assert_not_reached ();
    return "";
}

void ResourceTree::TreeData::apply_to_cell (Gtk::CellRenderer *cell) const
{
    CellRendererIconText *cell_icontext = dynamic_cast<CellRendererIconText*> (cell);
    g_return_if_fail (cell_icontext);

    switch (type)
    {
    case TREEDATA_CATEGORY:
        cell_icontext->property_text () = get_label ();    
        cell_icontext->property_weight () = Pango::WEIGHT_BOLD;
        cell_icontext->property_pixbuf () = Glib::RefPtr<Gdk::Pixbuf> ();        
        //cell_icontext->property_editable () = false;
        break;
    case TREEDATA_APP:
        cell_icontext->property_weight_set () = false;
        cell_icontext->property_pixbuf () = Glib::RefPtr<Gdk::Pixbuf> ();
        //cell_icontext->property_editable () = true;

        {
            Glib::ustring app_name = Glib::Markup::escape_text (get_label ());
            Glib::ScopedPtr<char> label (g_strdup_printf (_("<b>%s</b> <small>(double-click to edit)</small>"),
                                                          app_name.c_str ()));
            cell_icontext->property_markup () = label.get ();
        }
        
        break;
    case TREEDATA_RESOURCE:
        cell_icontext->property_text () = get_label ();    
        cell_icontext->property_weight () = Pango::WEIGHT_NORMAL;
        cell_icontext->property_pixbuf () = Resources::get_type_pixbuf (res->get_type ());
        //cell_icontext->property_editable () = true;
        break;
    case TREEDATA_NULL:
        g_assert_not_reached ();
    }
}
