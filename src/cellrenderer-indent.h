// $Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUIKACHU_CELLRENDERER_INDENT_H
#define GUIKACHU_CELLRENDERER_INDENT_H

#include <gtkmm/cellrenderer.h>
#include <gtkmm/treepath.h>
#include <gtkmm/treeview.h>

namespace Guikachu
{
    namespace GUI
    {
        class CellRendererIndentBase: public Gtk::CellRenderer
        {
            int depth;
            
        protected:
            CellRendererIndentBase ();
            virtual ~CellRendererIndentBase () {};

        public:
            virtual Gtk::CellRenderer       * get_real_cell () = 0;
            virtual const Gtk::CellRenderer * get_real_cell () const = 0;

            void set_depth (int depth);
            void set_depth (const Gtk::TreeModel::Path &path);

        protected:
            void real_cell_mode_changed_cb ();
            
            void get_size_vfunc (Gtk::Widget          &widget,
                                 const Gdk::Rectangle *cell_area,
                                 int                  *x_offset,
                                 int                  *y_offset,
                                 int                  *width,
                                 int                  *height) const;
            
            void render_vfunc (const Glib::RefPtr<Gdk::Drawable> &window,
                               Gtk::Widget                       &widget,
                               const Gdk::Rectangle              &background_area,
                               const Gdk::Rectangle              &cell_area,
                               const Gdk::Rectangle              &expose_area,
                               Gtk::CellRendererState             flags);
            
            bool activate_vfunc (GdkEvent               *event,
                                 Gtk::Widget            &widget,
                                 const Glib::ustring    &path,
                                 const Gdk::Rectangle   &background_area,
                                 const Gdk::Rectangle   &cell_area,
                                 Gtk::CellRendererState  flags);            
            
            Gtk::CellEditable * start_editing_vfunc (GdkEvent               *event,
                                                     Gtk::Widget            &widget,
                                                     const Glib::ustring    &path,
                                                     const Gdk::Rectangle   &background_area,
                                                     const Gdk::Rectangle   &cell_area,
                                                     Gtk::CellRendererState  flags);
        };
        
        template<class RealCell>
        class CellRendererIndent: public CellRendererIndentBase
        {
        public:
            typedef RealCell                        real_cell_t;
            typedef CellRendererIndent<real_cell_t> self_t;

        private:
            real_cell_t real_cell;
            Gtk::TreeViewColumn::SlotCellData cell_slot;
            
        public:
            CellRendererIndent ();
            ~CellRendererIndent () {};
        
            Gtk::CellRenderer       * get_real_cell () { return &real_cell; };
            const Gtk::CellRenderer * get_real_cell () const { return &real_cell; };

            static self_t * setup_view (Gtk::TreeView                           &treeview,
                                        const Glib::ustring                     &leftmost_title,
                                        const Gtk::TreeViewColumn::SlotCellData &leftmost_slot);
        private:
            static void cell_cb (CellRenderer *cell, const Gtk::TreeModel::iterator &iter, Gtk::TreeView *treeview);
        };

        /**** Template Implementation **********************************************/

        template<class RealCell>
        CellRendererIndent<RealCell>::CellRendererIndent ()
        {
            real_cell.property_mode ().signal_changed ().connect (
                sigc::mem_fun (*this, &CellRendererIndent::real_cell_mode_changed_cb));
        }

        template<>
        CellRendererIndent<Gtk::CellRendererText>::CellRendererIndent ();
        
        template<class RealCell>
        CellRendererIndent<RealCell> * 
        CellRendererIndent<RealCell>::setup_view (Gtk::TreeView                           &treeview,
                                                  const Glib::ustring                     &title,
                                                  const Gtk::TreeViewColumn::SlotCellData &slot)
        {
            Gtk::CellRenderer *empty_cell = new Gtk::CellRendererText;
            Gtk::TreeView::Column *expander_col = new Gtk::TreeView::Column ("", *manage (empty_cell));
            expander_col->set_visible (false);
            treeview.append_column (*manage (expander_col));
            treeview.set_expander_column (*expander_col);
            
            Gtk::TreeView::Column *column = new Gtk::TreeView::Column (title);
            CellRendererIndent *indent_cell = new CellRendererIndent<RealCell>;
            indent_cell->cell_slot = slot;
            column->pack_start (*manage (indent_cell), true);    
            column->set_cell_data_func (*indent_cell, sigc::bind (sigc::ptr_fun (&CellRendererIndent::cell_cb), &treeview));
            treeview.append_column(*manage (column));
            
            treeview.signal_row_collapsed ().connect (
                sigc::hide (sigc::hide (sigc::mem_fun (treeview, &Gtk::TreeView::expand_all))));
            
            return indent_cell;
        }
        
        template<class RealCell>
        void CellRendererIndent<RealCell>::cell_cb (CellRenderer                   *cell,
                                                    const Gtk::TreeModel::iterator &iter,
                                                    Gtk::TreeView                  *view)
        {    
            CellRendererIndent<RealCell> *cell_indent = dynamic_cast<CellRendererIndent<RealCell>*> (cell);
            g_return_if_fail (cell_indent);
            
            cell_indent->set_depth (view->get_model ()->get_path (iter));
            cell_indent->cell_slot (cell_indent->get_real_cell (), iter);
        }
    }
}
#endif /* !GUIKACHU_CELLRENDERER_INDENT */
