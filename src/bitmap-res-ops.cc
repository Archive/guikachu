//$Id$ -*- c++ -*-

/* Guikachu Copyright (C) 2001-2007 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <glib/gi18n.h>

#include "bitmap-res-ops.h"

#include <glib.h> // for g_assert

using namespace Guikachu;
using namespace Guikachu::ResourceOps::BitmapOps;

ImageChangeOp::ImageChangeOp (Resources::Bitmap                  *bitmap,
                              const Resources::Bitmap::ImageData &old_image_) :
    manager (bitmap->get_manager ()),
    bitmap_serial (bitmap->get_serial ()),
    old_image (old_image_),
    new_image (bitmap->get_image ())
{
    char *label_str = g_strdup_printf (_("Change image in %s"), bitmap->id ().c_str ());
    op_label = label_str;
    g_free (label_str);
}

Resources::Bitmap * ImageChangeOp::get_bitmap () const
{
    Resource *res = manager->get_resource (bitmap_serial);
    g_assert (res);

    return static_cast<Resources::Bitmap*> (res);
}

void ImageChangeOp::undo ()
{
    Resources::Bitmap *res = get_bitmap ();
    res->set_image (old_image);
}

void ImageChangeOp::redo ()
{
    Resources::Bitmap *res = get_bitmap ();
    res->set_image (new_image);
}
