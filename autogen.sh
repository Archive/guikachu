#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="Guikachu"
REQUIRED_AUTOMAKE_VERSION=1.9
REQUIRED_INTLTOOL_VERSION=0.35
ACLOCAL_FLAGS="-I $srcdir/lib/scripts $ACLOCAL_FLAGS"

(test -f $srcdir/configure.in \
  && test -f $srcdir/src/guikachu.glade \
  && test -f $srcdir/src/guikachu2rcp.cc) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level $PKG_NAME directory"
    exit 1
}

which gnome-autogen.sh >/dev/null || {
    echo "You need to install gnome-common from the GNOME CVS"
    exit 1
}

USE_GNOME2_MACROS=1 . gnome-autogen.sh
