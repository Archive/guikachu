Guikachu 1.5.11
===============

Guikachu is a GNOME application for graphical editing of resource
files for PalmOS-based pocket computers. The user interface is
modelled after Glade, the GNOME UI builder. Check out
http://cactus.rulez.org/projects/guikachu/ for up-to-date
information.


Requirements
------------
Guikachu uses the GNOME 2 platform, particularly the GNOMEmm and GTKmm
C++ bindings. It can also be compiled for Windows 2000 and later,
using the MinGW POSIX-like runtime. Please see README-win32.txt for
details.
To actually create the PalmOS resource files, you will also need PilRC
(part of the GNU PalmOS SDK) to compile the .rpc files produced by
Guikachu.


Installation
------------
Untar the Guikachu source tarball somewhere, and start the 'configure'
script:

 $ tar xzvf guikachu-1.5.11.tar.gz
 $ cd guikachu-1.5.11
 $ ./configure
 $ make
 
See the INSTALL file for general help on using the `configure' script.
If everything is just fine, you can install and try it

 $ make install
 $ guikachu

If you start Guikachu and it segfaults when you try to open any
window, it is an indication of guikachu.glade not propertly
installed. Please re-install Guikachu (it will try to check for
required files on startup, but it has no way of detecting the Glade
file being out of date)


Features
--------
 * Uses GNOME-VFS, you can load files from anywhere, e.g. from the
   Web, from an SMB share, from a tarball.
 * Exporting to/importing from PilRC .rcp files
 * Support for non-Palm PilRC targets (like the eBookMan)
 * Support for the following PalmOS resource types:
	- String and string list resources
	- Dialog resources
	- Menu resources
	- Form resources
	- Bitmap and BitmapFamily resources
	- Per-application resources (e.g. version number)
 * WYSIWYG Form Editor, with drag & drop capability, cut & paste, and
   visual resizing
 * Flexible, complete undo support
 * Sample file with sample GNU PalmOS SDK-based application
 * Documentation (a complete user's manual)

 
Reporting bugs
--------------
(and good feature/UI/whatever ideas)
Please use the GNOME Bugzilla (accesible at http://bugzilla.gnome.org)
to file bug reports and feature requests. Detailed instructions are
available on the page. If you find the Bugzilla interface too
complicated, just email directly our mailing list at
guikachu-main@lists.sourceforge.net.


Contact information
-------------------
The Guikachu website is located at http://cactus.rulez.org/projects/guikachu/
The mailing list for both users and contributors is accessable at
http://lists.sourceforge.net/mailman/listinfo/guikachu-main
You can also mail the author directly, at cactus@cactus.rulez.org.


About this release
------------------
Includes bugfixes and new features, mostly suggested by Michal Lisowski:

* Dialog (Alert) editor: fixed a possible crash in the button editor
* Files are now saved with a '.guikachu' extension when nothing is
  specified
* GCC 4.2 compile-time warning fixes
