<?xml version="1.0" encoding="iso-8859-1"?>
<section id="widgets">
  <title>Widget Reference</title>

  <!-- Introduction on widgets -->
  <para>
    A widget is a unit of a user interface, a single element doing a
    single thing, for example, a tappable button, or an editable text
    field. A form contains various widgets, the user interacts with
    them, and the application is notified via <code>events</code>. See
    the PalmOS documentation and the example application included with
    Guikachu for more information on handling widget events.
  </para>
  
  <common-properties>
    <property name="id">
      <title>Resource ID</title>
      <description>
	<para>
	  A unique string ID used for accessing the resource at
	  run-time from the program
	</para>
      </description>
    </property>

    <property name="pos">
      <title>X and Y</title>
      <description>
	<para>
	  Horizontal and vertical position relative to the parent
	  form, in pixels, counted from left to right and top to
	  bottom. Due to the size of the screen of PalmOS devices,
	  both X and Y are integer values between 0 and 159.
	</para>
      </description>
    </property>

    <property name="size">
      <title>Width and height</title>
      <description>
	<para>
	  Horizontal and vertical size, in pixels. Due to the size of
	  the screen of PalmOS devices, both width and height are
	  integer values between 1 and 160. 
	</para>
      </description>
    </property>

    <property name="auto-size">
      <title>Width and height</title>
      <description>
	<para>
	  Horizontal and vertical size, in pixels. Due to the size of
	  the screen of PalmOS devices, both width and height are
	  integer values between 1 and 160. 
	</para>
	<para>
	  You can also use automatic values calculated from the
	  size of the widget's contents.
	</para>
      </description>
    </property>

    <property name="usable">
      <title>Usable</title>
      <description>
	<para>
	  Non-usable widgets are not rendered on the screen and don't
	  receive events. You can set/unset the &quot;usable&quot;
	  property at run-time with the <code>CtlSetUsable</code>
	  function.
	</para>
      </description>
    </property>

    <property name="anchor-right">
      <title>Anchor right</title>
      <description>
	<para>
	  If the widget's size changes at run-time and this flag is
	  set, the new horizontal position is calculated to leave the
	  widget's right edge at place.
	</para>
      </description>
    </property>

    <property name="text-font">
      <title>Text and font</title>
      <description>
	<para>
	  The textual contents of the widget. It will be rendered
	  using the specified font.
	</para>
	<para>
	  Guikachu currently only supports the built-in fonts.
	</para>
      </description>
    </property>
    
    <property name="group">
      <title>Group ID</title>
	<description>
	<para>
	  If a check box or pushbutton belongs to a group, only one
	  per group can be checked by the user. This is 
	  automatically enforced by PalmOS. Note that check boxes and
	  pushbuttons share their groups.
	</para>
	<para>
	  Set the group ID to 0 if you don't want to include it in any
	  group.
	</para>
      </description>
    </property>

    <property name="graphical">
      <title>Bitmap/Selected Bitmap</title>
      <description>
	<para>
	  Select a <link ref="resource-bitmap">Bitmap or Bitmap
	  Group</link> resource to be displayed instead of the textual
	  caption.
	</para>
	<para>
	  Usually, you should specify a separate bitmap for the
	  widget's selected state, since otherwise the normal bitmap
	  is used.
	</para>
      </description>
    </property>
    
  </common-properties>

  <resource name="widget-label">
    <title>Label</title>
    <description>
      <para>
	Labels are static text that can't interact with the user and
	can't be changed by the application at run-time either. For
	non-static labels, use a non-editable
	<link ref="widget-text-field">text field</link>.
      </para>
    </description>
    <properties>
      <property ref="id"/>
      <property ref="usable"/>

      <property ref="text-font"/>
      
      <property ref="pos"/>
    </properties>
    
  </resource>

  <resource name="widget-text-field">
    <title>Text field</title>
    <description>
      <para>
	A text field contains text that is changeable at run-time,
	either by the application or by the user.
      </para>
      <para>
	There are several ways to set/get the currently displayed text
	of a text field, see the PalmOS SDK for details.
      </para>     
    </description>
    <properties>
      <property ref="id"/>
      <property ref="usable"/>

      <property name="font">
	<title>Font</title>
	<description>
	  <para>
	    The font used to display the contents of the text field
	  </para>
	  <para>	    
	    Guikachu currently only supports the three default built-in
	    fonts: #0 is the &quot;normal&quot;, #1 is the &quot;bold&quot; and
	    #2 is the &quot;large&quot; font.
	  </para>
	</description>
      </property>
	
      <property name="editable">
	<title>Editable</title>
	<description>
	  <para>
	    Controls whether the user is able to change the field's
	    contents.
	  </para>
	</description>	  
      </property>
      
      <property name="multi-line">
	<title>Multi-line</title>
	<description>
	  <para>
	    Multi-line text fields automatically wrap input at the end
	    of visible lines. They also accept <key>newline</key> and
	    <key>tab</key> as input.
	  </para>
	</description>	  
      </property>
      
      <property name="numeric">
	<title>Numeric-only</title>
	<description>
	  <para>
	    Set this property to restrict input by the user to numbers
	    only. Since it only controls user input, it makes sense
	    only for <proplink res="widget-text-field" name="editable">editable</proplink> 
	    text fields.
	  </para>
	</description>	  
      </property>

      <property name="length">
	<title>Maximum length</title>
	<description>
	  <para>
	    Maximum allowed length of text entered by the user. Set
	    this to 0 to force no restrictions (actually, there is
	    still a restriction on length due to the PalmOS
	    architecture).
	  </para>
	</description>
      </property>

      <property name="underlined">
	<title>Underlined</title>
	<description>
	  <para>
	    It is usually a good idea to underline
	    <proplink res="widget-text-field"
	    name="editable">editable</proplink>
	    text fields, since otherwise the user has no visual clue
	    about the existence of the field.
	  </para>
	</description>
      </property>
	
      <property name="autoshift">
	<title>Auto-shift</title>
	<description>
	  <para>
	    If you set this property, the graffiti shift state will
	    behave &quot;smartly&quot;, e.g. shift will be turned on
	    for new sentences.
	  </para>
	</description>
      </property>
      
      <property name="dynamic">
	<title>Dynamic size</title>
	<description>
	  <para>
	    If this flag is set, <code>fldHeightChangedEvent</code>s
	    are generated when the number of lines needed to display
	    the contents changes. The application then needs to
	    handle this event by optionally changing the size of the
	    field.
	  </para>
	  <para>
	    This only makes sense for
	    <proplink res="widget-text-field" name="multi-line">multi-line</proplink>
	    text fields.
	  </para>
	</description>
      </property>
      
      <property name="align-right">
	<title>Right-aligned</title>
	<description>
	  <para>
	    Alignment of the text contents.
	  </para>
	  <para>
	    Right-aligned text fields don't accept the <key>Tab</key>
	    character as input.
	  </para>
	</description>
      </property>

      <property name="scrollbar">
	<title>Scrollbar</title>
	<description>
	  <para>
	    Set this to have a scrollbar on the right-hand side of the
	    text field when the contents are larger than the size of
	    the widget.
	  </para>
	  <para>
	    The scrolling itself will be automatically done by the OS,
	    you don't need to do anything on your own.
	  </para>
	</description>
      </property>
      
      <property ref="pos"/>
      <property ref="auto-size"/>
      
    </properties>

  </resource>
  
  <resource name="widget-graffiti">
    <title>Graffiti State Indicator</title>
    <description>
      <para>
	This widget displays an icon showing the current state (like
	upper-case input, or punctuation marks) of the Graffiti input
	system. It is automatically connected to the input system, so
	you don't have to bother implementing it. Just place it on a
	form and you're set.
      </para>
    </description>
    <properties>
      <property ref="id"/>
      <property ref="pos"/>
    </properties>
  </resource>
  
  <resource name="widget-button">
    <title>Button</title>
    <description>
      <para>
	A button is a (rounded) rectangular user interface element
	that emits a specific signal to the application when the user
	taps it with the stylus.
      </para>
    </description>
    <properties>
      <property ref="id"/>
      <property ref="usable"/>      

      <property ref="text-font"/>
      <property ref="graphical"/>

      <property name="frame">
	<title>Frame type</title>
	<description>
	  <para>
	    Buttons can optionally have one of two types of frame
	    around them. It is usually a good idea to give buttons a
	    frame as this helps users distinguishing it from a simple
	    <link ref="widget-label">label</link>
	  </para>
	</description>
      </property>

      <property name="repeating">
	<title>Repeating</title>
	<description>
	  <para>
	    Repeating buttons emit several <code>ButtonPress</code>
	    signals when kept pressed by the user.
	  </para>
	</description>
      </property>

      <property ref="anchor-right"/>
      
      <property ref="pos"/>
      <property ref="auto-size"/>
      
    </properties>

  </resource>


  <resource name="widget-checkbox">
    <title>Check box</title>
    <description>
      <para>
	A check box contains a state icon and a text label. The state
	icon shows whether the widget is &quot;checked&quot;, this
	state can then be handled by the application via the
	<code>CtlGetValue</code> function.
      </para>
    </description>
    <properties>
      <property ref="id"/>
      <property ref="usable"/>     

      <property ref="text-font"/>

      <property name="toggled">
	<title>Toggled</title>
	<description>
	  <para>
	    Initial checked/unchecked state of the widget
	  </para>
	</description>
      </property>

      <property ref="group"/>
      
      <property ref="anchor-right"/>

      <property ref="pos"/>
      <property ref="auto-size"/>
      
    </properties>

  </resource>

  <resource name="widget-pushbutton">
    <title>Pushbutton</title>
    <description>
      <para>
	A pushbutton behaves a lot like a
	<link ref="widget-checkbox">check box</link>, except it has no
	initial state and is rendered as a rectangular
	button.
      </para>
    </description>
    <properties>
      <property ref="id"/>
      <property ref="usable"/>
      
      <property ref="text-font"/>
      <property ref="graphical"/>

      <property ref="group"/>

      <property ref="pos"/>
      <property ref="auto-size"/>
      
    </properties>

  </resource>

  <resource name="widget-selector-trigger">
    <title>Selector trigger</title>
    <description>
      <para>
	Selector triggers are a lot like <link ref="widget-button">buttons</link>,
	the only difference is in the looks. The real difference is in
	intention: selector triggers should be used in cases where
	input is done by tapping the trigger, and then filling in the
	form that pops up.
      </para>
    </description>
    <properties>
      <property ref="id"/>
      <property ref="usable"/>

      <property ref="text-font"/>
      <property ref="graphical"/>
      <property ref="anchor-right"/>
      
      <property ref="pos"/>
      <property ref="auto-size"/>
    </properties>
  </resource>
  
  <resource name="widget-popup-trigger">
    <title>Popup trigger</title>
    <description>
      <para>
	Popup triggers are usually called &quot;drop-down lists&quot;
	or &quot;combo boxes&quot; on other platforms. On tapping, the
	popup trigger displays a list of items and waits for the user
	to select a list item.
      </para>
      <para>
	The <link ref="widget-list">list</link> linked to the trigger is
	usually placed at the same spot as the trigger, with the list
	set to <proplink res="widget-list" name="usable">unusable</proplink> so it isn't
	displayed until the trigger is tapped.
      </para>
    </description>
    <properties>
      <property ref="id"/>
      <property ref="usable"/>

      <property ref="text-font"/>
      <property ref="graphical"/>

      <property name="list-id">
	<title>List ID</title>
	<description>
	  <para>
	    The ID of the <link ref="widget-list">list widget</link> to
	    pop up when the trigger is tapped by the user.
	  </para>
	</description>
      </property>
      
      <property ref="anchor-right"/>
      
      <property ref="pos"/>
      <property ref="auto-size"/>
    </properties>
  </resource>
  
  <resource name="widget-scrollbar">
    <title>Scroll bar</title>
    <description>
      <para>
	Scroll bars are vertical rulers displaying a user-changeable
	value in a given interval. The user can change this value by
	dragging a slider up or down.
      </para>
      <para>
	You can use the
	<code>SclGetScrollBar</code>/<code>SclSetScrollBar</code>
	functions to retrieve/modify the value and state of the
	scroll bar at run-time.
      </para>
    </description>
    <properties>
      <property ref="id"/>
      <property ref="usable"/>

      <property name="min-max">
	<title>Value interval</title>
	<description>
	  <para>
	    Minimal and maximal value of the slider.
	  </para>
	  <para>
	    Due to limitations of the PalmOS platform, both the
	    minimal and the maximal value need to be integers between
	    0 and 32767 (this is enforced by the property editor in
	    Guikachu)
	  </para>
	</description>
      </property>

      <property name="value">
	<title>Initial value</title>
	<description>
	  <para>
	    The initial value of the scroll bar at start-up, within the
	    <proplink res="widget-scrollbar" name="value">valid interval</proplink>.
	  </para>
	</description>
      </property>

      <property name="page-size">
	<title>Page size</title>
	<description>
	  <para>
	    The amount of change when the user taps the scroll bar
	    instead of dragging the slider or tapping one of the
	    arrows at the end of the bar.
	  </para>
	</description>
      </property>
      
      <property ref="pos"/>
      <property ref="auto-size"/>
    </properties>
  </resource>
  
  <resource name="widget-list">
    <title>List</title>
    <description>
      <para>
	This widget is a list of one-line text items. Selecting a list
	item emits a <code>lstSelectEvent</code>. You can then use
	<code>LstGetSelection</code> in the event handler to get the
	currently selected item.
      </para>
      <para>
	Lists are also used by <link ref="widget-popup-trigger">popup
	triggers</link>: the list linked to the trigger is shown when
	the trigger is tapped by the user.
      </para>
    </description>
    <properties>
      <property ref="id"/>
      <property ref="usable"/>

      <property name="visible-items">
	<title>Visible items</title>
	<description>
	  <para>
	    Number of items displayed at a time. There needs to be at
	    least that many items in the list, or undefined things may
	    happen (including the possibility of the PalmOS device
	    freezing).
	  </para>
	  <para>
	    If there are more items than displayed at a time, the user
	    is able to scroll through items.
	  </para>
	</description>
      </property>
      
      <property name="font">
	<title>Font</title>
	<description>
	  <para>
	    The font used to display the list items
	  </para>
	  <para>	    
	    Guikachu currently only supports the three default built-in
	    fonts: #0 is the &quot;normal&quot;, #1 is the &quot;bold&quot; and
	    #2 is the &quot;large&quot; font.
	  </para>
	</description>
      </property>
      
      <property ref="pos"/>
      <property name="width">
	<title>Width</title>
	<description>
	  <para>
	    Horizontal size, in pixels. Due to the size of
	    the screen of PalmOS devices, this is an integer value
	    between 1 and 160.
	  </para>
	  <para>
	    You can also use an automatic value calculated from the
	    width of the first list item.
	  </para>
	  <para>
	    The height of the widget will be automatically calculated
	    from the
	    <proplink res="widget-list" name="visible-items">number of visible
	    items</proplink> and the <proplink res="widget-list"
	    name="font">font</proplink> used.
	  </para>
	</description>
      </property>
    </properties>
  </resource>
  
  <resource name="widget-table">
    <title>Table</title>
    <description>
      <para>
	Use tables to show multi-column data. The PalmOS API makes it
	very easy to bind table cells to database fields and other
	persistent data.
      </para>
      <para>
	Most of the behavior of tables are defined at run-time
	instead of the resource file. Refer to the PalmOS API
	documentation for further information on the related
	library functions.
      </para>
    </description>
    <properties>
      <property ref="id"/>

      <property name="rows">
	<title>Number of rows</title>
	<description>
	  <para>
	    The number of rows visible. The height of the rows will be
	    calculated at run-time based on cell type and content.	   
	  </para>
	  <para>
	    The Form Editor displays rows at the default height of
	    11 pixels.
	  </para>
	</description>
      </property>

      <property name="columns">
	<title>Columns</title>
	<description>
	  <para>
	    Only the number of columns, and each column's width is
	    stored in the resource file. All other aspects of columns
	    (most notably, their content type) is set at run-time.
	  </para>
	</description>
      </property>
      
      <property ref="pos"/>
      
      <property name="size">
	<title>Width and height</title>
	<description>
	  <para>
	    Although PilRC allows you to include height and width
	    information for tables, these values are not used, and an
	    automatic size based on
	    <proplink res="widget-table" name="columns">column width</proplink>
	    and <proplink res="widget-table" name="rows">number of rows</proplink>. Due
	    to this, Guikachu doesn't confuse the user with bogus size
	    settings. 
	  </para>
	</description>
      </property>
    </properties>
  </resource>

  <resource name="widget-formbitmap">
    <title>Bitmap widget</title>
    <description>
      <para>
      </para>
    </description>
    <properties>
      <property ref="id"/>
      <property ref="usable"/>
      <property name="bitmapref">
	<title>Bitmap ID</title>
	<description>
	  <para>
	    ID of the 
	    <link ref="res-bitmap">Bitmap/Bitmap Group</link>
	    resource to display.
	  </para>
	</description>
      </property>
      <property ref="pos"/>
    </properties>
  </resource>
  
  <resource name="widget-gadget">
    <title>Gadget</title>
    <description>
      <para>
	Gadgets are placeholders for widgets implemented by the app
	author. The application is responsible for rendering the
	widget and maintaining any associated state data.
      </para>
    </description>
    <properties>
      <property ref="id"/>
      <property ref="usable"/>      
     
      <property ref="pos"/>
      <property ref="size"/>
      
    </properties>

  </resource>

</section>
