<?xml version="1.0"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="common-properties"/>
  
  <xsl:template match="resource/title">
    <title>
      <xsl:apply-templates/>
    </title>
  </xsl:template>

  <xsl:template match="resource">
    <section id="{@name}">
      <xsl:apply-templates/>
    </section>    
  </xsl:template>

  <xsl:template match="resource/properties">
    <variablelist>
      <xsl:apply-templates/>
    </variablelist>
  </xsl:template>
  
</xsl:stylesheet>
