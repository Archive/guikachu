<?xml version="1.0"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="xml" encoding="iso-8859-1"/>

  <xsl:template match="property/title">
    <term>
      <xsl:apply-templates/>
    </term>
  </xsl:template>
  
  <xsl:template match="property/description">
    <listitem>
      <xsl:apply-templates/>
    </listitem>
  </xsl:template>
  
  <xsl:template match="resource/properties/property">
    <xsl:element name="varlistentry">
      <xsl:attribute name="id">
	<xsl:value-of select="../../@name"/>
	<xsl:text>-</xsl:text>
	<xsl:choose>
	  <xsl:when test="@name">
	    <xsl:value-of select="@name"/>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="@ref"/>
	  </xsl:otherwise>
	</xsl:choose>
      </xsl:attribute>

      <xsl:choose>
	<xsl:when test="@ref">
	  <xsl:apply-templates select="../../../common-properties/property[@name=current()/@ref]"/>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:apply-templates/>
	</xsl:otherwise>
      </xsl:choose>   
    </xsl:element>
    
  </xsl:template>
  
</xsl:stylesheet>
