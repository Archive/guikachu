<?xml version="1.0"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="para">
    <para>
      <xsl:apply-templates/>
    </para>
  </xsl:template>

  <xsl:template match="para/key">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="para/code">
    <emphasis><xsl:apply-templates/></emphasis>
  </xsl:template>

  <xsl:template match="code-snippet">
    <informalexample>
      <programlisting>
	<xsl:apply-templates/>
      </programlisting>
    </informalexample>
  </xsl:template>

  <xsl:template match="weblink">
    <xsl:if test="@uri=''">
      <xsl:message><xsl:text>Missing required attribute uri'</xsl:text></xsl:message>
    </xsl:if>
    
    <ulink url="{@uri}">
      <xsl:apply-templates/>
    </ulink>
  </xsl:template>

  <xsl:template match="link">
    <xsl:if test="@ref=''">
      <xsl:message><xsl:text>Missing required attribute 'ref'</xsl:text></xsl:message>
    </xsl:if>
    
    <link linkend="{@ref}">
      <citetitle>
	<xsl:apply-templates/>
      </citetitle>
    </link>
  </xsl:template>

  <xsl:template match="proplink">
    <link linkend="{@res}-{@name}">
      <citetitle>
	<xsl:apply-templates/>
      </citetitle>
    </link>
  </xsl:template>

  <xsl:template match="img">
    <figure>
      <title>
	<xsl:apply-templates/>
      </title>
      <graphic fileref="pictures/{@ref}"/>
    </figure>
  </xsl:template>

  <xsl:template match="list|itemlist">
    <itemizedlist>
      <xsl:apply-templates/>
    </itemizedlist>
  </xsl:template>

  <xsl:template match="itemlist/item">
    <listitem>
      <para>
	<xsl:apply-templates/>
      </para>
    </listitem>
  </xsl:template>

  <xsl:template match="list/item">
    <listitem>
      <xsl:choose>
	<xsl:when test="title">	 
	  <formalpara>
	    <xsl:apply-templates select="title"/>
	    <xsl:apply-templates select="para[1]"/>
	  </formalpara>
	  <xsl:apply-templates select="para[position() > 1]"/>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:apply-templates select="para"/>
	</xsl:otherwise>
      </xsl:choose>
    </listitem>
  </xsl:template>

  <xsl:template match="item/code-snippet">
    <para>
      <informalexample>
	<programlisting>
	  <xsl:apply-templates/>
	</programlisting>
      </informalexample>
    </para>
  </xsl:template>
  
</xsl:stylesheet>
