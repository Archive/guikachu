<?xml version="1.0"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:exsl="http://exslt.org/common"
  extension-element-prefixes="exsl"
  exclude-result-prefixes="exsl">

  <xsl:template match="/guikachu-doc">   
    <xsl:message>
      <xsl:text>Creating '</xsl:text>
      <xsl:value-of select="@id"/>
      <xsl:text>.docbook'</xsl:text>
    </xsl:message>
    
    <exsl:document href="{@id}.docbook"
      method="xml" encoding="utf-8"
      doctype-public="-//OASIS//DTD DocBook XML V4.1.2//EN"
      doctype-system="http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">

      <article id="{@id}">
	<xsl:apply-templates/>
      </article>
      
    </exsl:document>
  </xsl:template>

  <xsl:template match="title">
    <title><xsl:apply-templates/></title>
  </xsl:template>

  <xsl:template match="author">
    <articleinfo>
      <author>
	<xsl:apply-templates/>
      </author>
    </articleinfo>
  </xsl:template>

  <xsl:template match="author/firstname">
    <firstname>
      <xsl:apply-templates/>
    </firstname>
  </xsl:template>

  <xsl:template match="author/surname">
    <firstname>
      <xsl:apply-templates/>
    </firstname>
  </xsl:template>

  <xsl:template match="section">
    <xsl:choose>
      <xsl:when test="@id">
	<section id="{@id}">
	  <xsl:apply-templates/>
	</section>
      </xsl:when>
      <xsl:otherwise>
	<section>
	  <xsl:apply-templates/>
	</section>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:include href="inline.xsl"/>
  <xsl:include href="property.xsl"/>
  <xsl:include href="resource.xsl"/>
  
</xsl:stylesheet>
