<?xml version="1.0"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  
  <xsl:template match="section|resource" mode="toc">
    <dt>
      <xsl:apply-templates select="." mode="linkto"/>
      <!--
      <xsl:if test="section|resource">
	<dd>
	  <dl>
	    <xsl:apply-templates select="section|resource" mode="toc"/>
	  </dl>
	</dd>
      </xsl:if>
      -->
    </dt>
  </xsl:template>

  <!-- This is needed so we only get one level of sections in the TOC -->
  <xsl:template match="section/section" mode="toc">
    <!--
    <dt>
      <xsl:apply-templates select="." mode="linkto"/>
    </dt>
-->
  </xsl:template>
  
</xsl:stylesheet>
