<?xml version="1.0"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="common-properties"/>
  
  <xsl:template match="resource">
    <div class="resource">
      <a name="res-{@name}"/>
      <xsl:apply-templates/>
    </div>
  </xsl:template>
  
  <xsl:template match="resource/title">
    <h2>
      <xsl:apply-templates/>
    </h2>
  </xsl:template>

  <xsl:template match="resource/properties">
    <div class="property-list">
      <h3><xsl:text>Properties</xsl:text></h3>
      <dl>
	<xsl:apply-templates/>
      </dl>
    </div>
  </xsl:template>
  
</xsl:stylesheet>
