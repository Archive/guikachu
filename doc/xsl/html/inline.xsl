<?xml version="1.0"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="para">
    <xsl:if test="descendant::*|text()">
      <p>
	<xsl:apply-templates/>
      </p>
    </xsl:if>
  </xsl:template>

  <xsl:template match="para/key">
    <cite><xsl:apply-templates/></cite>
  </xsl:template>

  <xsl:template match="para/code">
    <code><xsl:apply-templates/></code>
  </xsl:template>

  <xsl:template match="prod">
    <cite>
      <xsl:apply-templates/>
    </cite>
  </xsl:template>

  <xsl:template match="weblink">
    <a href="{@uri}">
      <xsl:apply-templates/>
    </a>
  </xsl:template>
  
  <xsl:template match="list">
    <dl>
      <xsl:apply-templates/>
    </dl>
  </xsl:template>

  <xsl:template match="itemlist">
    <ul>
      <xsl:apply-templates/>
    </ul>
  </xsl:template>

  <xsl:template match="list/item">
    <dt>
      <xsl:apply-templates select="title"/>
    </dt>
    <dd>
      <xsl:apply-templates select="para|code-snippet"/>
    </dd>
  </xsl:template>

  <xsl:template match="itemlist/item">
    <li>
      <xsl:apply-templates/>
    </li>
  </xsl:template>
  
  <xsl:template match="code-snippet">
    <div class="code-snippet">
      <pre>
	<xsl:apply-templates/>	
      </pre>
    </div>
  </xsl:template>

  <xsl:template match="code-snippet/comment">
    <i class="code-comment"><xsl:apply-templates/></i>
  </xsl:template>

  <xsl:template match="img">
    <xsl:choose>
      <xsl:when test="text()">
	<table>
	  <tr>
	    <td>
	      <img src="pictures/{@ref}" title="{normalize-space(text())}"/>
	    </td>
	  </tr>
	  <tr>
	    <td>
	      <cite><xsl:apply-templates/></cite>
	    </td>
	  </tr>
	</table>
      </xsl:when>
      <xsl:otherwise>
	<img src="pictures/{@ref}"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
</xsl:stylesheet>
