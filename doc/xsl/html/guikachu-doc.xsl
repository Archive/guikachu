<?xml version="1.0"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:exsl="http://exslt.org/common"
  extension-element-prefixes="exsl">

  <xsl:template match="/guikachu-doc">
    <xsl:variable name="uri"><xsl:text>index.html</xsl:text></xsl:variable>
    <xsl:message>
      <xsl:text>Creating '</xsl:text>
      <xsl:value-of select="$uri"/>
      <xsl:text>'</xsl:text>
    </xsl:message>
    
    <exsl:document href="{$uri}"
      method="html" encoding="utf-8"
      indent="yes">
      <html>
	<head>
	  <title><xsl:value-of select="title/text()"/></title>
	  <xsl:apply-templates select="section[1]" mode="header"/>
	</head>
	<body>

	  <xsl:apply-templates select="title"/>
	  <xsl:apply-templates select="author"/>

	  <div id="toc">
	    <a name="toc"/>
	    <h1><xsl:text>Table of Contents</xsl:text></h1>
	    <dl>
	      <xsl:apply-templates select="section" mode="toc"/>
	    </dl>
	  </div>
	  
	  <xsl:apply-templates select="section"/>
	  
	</body>
      </html>
    </exsl:document>
  </xsl:template>

  <xsl:template match="guikachu-doc/title">
    <h1 id="doc-title">
      <xsl:apply-templates/>
    </h1>
  </xsl:template>  

  <xsl:template match="guikachu-doc/author">
    <h2 id="doc-title">
      <xsl:apply-templates/>
    </h2>
  </xsl:template>  

  <xsl:template match="guikachu-doc/section" mode="uri"> <!-- section[1] -->
    <xsl:text>index.html#sec-</xsl:text>
    <xsl:value-of select="@id"/>
  </xsl:template>
  
  <xsl:template match="guikachu-doc/section[preceding-sibling::section]" mode="uri"> <!-- section[n] -->
    <xsl:value-of select="@id"/>
    <xsl:text>.html</xsl:text>    
  </xsl:template>

  <!--
  <xsl:template match="section" mode="id">
    <xsl:choose>
      <xsl:when test="@id">
	<xsl:value-of select="@id"/>
      </xsl:when>
      <xsl:otherwise>
	<xsl:text>foo</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
-->
  
  <xsl:template match="section/section" mode="uri">
    <xsl:variable name="page-uri">
      <xsl:apply-templates select="ancestor::section[position=last()]" mode="uri"/>
    </xsl:variable>
    <xsl:variable name="sec-id">
<!--      <xsl:apply-templates select="." mode="id"/>-->
      <xsl:value-of select="@id"/>
    </xsl:variable>
    <xsl:value-of select="$page-uri"/>
    <xsl:text>#sec-</xsl:text>
    <xsl:value-of select="$sec-id"/>
  </xsl:template>
  
  <xsl:template match="guikachu-doc/section" mode="separate-file">

    <xsl:variable name="uri"><xsl:apply-templates select="." mode="uri"/></xsl:variable>

    <xsl:message>
      <xsl:text>Creating '</xsl:text>
      <xsl:value-of select="$uri"/>
      <xsl:text>'</xsl:text>
    </xsl:message>

    <exsl:document href="{$uri}"
      method="html" encoding="utf-8"
      indent="yes">
      <html>
	<head>
	  <title><xsl:value-of select="title/text()"/></title>
	  <xsl:apply-templates select="." mode="header"/>
	</head>    
	<body>	  	  
	  <xsl:apply-templates select="." mode="page-contents"/>
	</body>
      </html>
    </exsl:document>
  </xsl:template>

  <xsl:template match="/guikachu-doc/section">
    <xsl:apply-templates select="." mode="separate-file"/>
  </xsl:template>

  <xsl:template match="/guikachu-doc/section[1]">
    <xsl:apply-templates select="." mode="page-contents"/>
  </xsl:template>

  <xsl:include href="section.xsl"/>
  <xsl:include href="link.xsl"/>
  <xsl:include href="toc.xsl"/>
  <xsl:include href="inline.xsl"/>
  <xsl:include href="property.xsl"/>
  <xsl:include href="resource.xsl"/>
  
</xsl:stylesheet>
