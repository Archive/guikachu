<?xml version="1.0"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  
  <xsl:template match="section/resource" mode="linkto">
    <xsl:variable name="uri">
      <xsl:apply-templates select="." mode="uri"/>
    </xsl:variable>
    <a href="{$uri}"><xsl:value-of select="title/text()"/></a>    
  </xsl:template>

  <xsl:template match="section/resource" mode="uri">
    <xsl:variable name="parent-uri">
      <xsl:apply-templates select=".." mode="uri"/>
    </xsl:variable>
    <xsl:value-of select="$parent-uri"/>
    <xsl:text>#res-</xsl:text>
    <xsl:value-of select="@name"/>
  </xsl:template>

  <xsl:template match="guikachu-doc/section" mode="linkto">
    <xsl:variable name="uri">
      <xsl:apply-templates select="." mode="uri"/>
    </xsl:variable>
    <a href="{$uri}"><xsl:value-of select="title/text()"/></a>    
  </xsl:template>
  
  <xsl:template match="guikachu-doc/section[1]" mode="linkto">
    <a href="#sec-{@id}"><xsl:value-of select="title/text()"/></a>    
  </xsl:template>
  
  <xsl:template match="section/section" mode="linkto">
    <!--
    <xsl:variable name="parent-uri">
      <xsl:apply-templates select="parent::section" mode="uri"/>
    </xsl:variable>
    <a href="{$parent-uri}#sec-{@id}"><xsl:value-of select="title/text()"/></a>
-->
    <xsl:variable name="uri">
      <xsl:apply-templates select="." mode="uri"/>
    </xsl:variable>
    <a href="{$uri}"><xsl:value-of select="title/text()"/></a>
  </xsl:template>

  <xsl:template match="link">
    <xsl:if test="@ref=''">
      <xsl:message><xsl:text>Missing required attribute 'ref'</xsl:text></xsl:message>
    </xsl:if>

    <xsl:variable name="target"
      select="/descendant::section[@id=current()/@ref] | /descendant::resource[@name=current()/@ref]"/>

    <xsl:variable name="uri">
      <xsl:apply-templates select="$target" mode="uri"/>
    </xsl:variable>
    
    <a href="{$uri}"><xsl:apply-templates/></a>
  </xsl:template>

  <xsl:template match="proplink">
    <xsl:variable name="target"
      select="/descendant::resource[@name=current()/@res]"/>

    <xsl:variable name="res-uri">
      <xsl:apply-templates select="$target" mode="uri"/>
    </xsl:variable>

    <a href="{$res-uri}-{@name}"><xsl:apply-templates/></a>
  </xsl:template>

</xsl:stylesheet>
