<?xml version="1.0"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:exsl="http://exslt.org/common"
  extension-element-prefixes="exsl">

  <xsl:template match="guikachu-doc/section" mode="header">
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="guikachu-doc-html.css"/>

    <!-- TOC -->
    <link rel="toc" href="index.html#toc"/>

    <!-- Prev -->
    <xsl:if test="preceding-sibling::section">
      <xsl:variable name="prev" select="preceding-sibling::section[1]"/>
      <xsl:variable name="prev-uri">
	<xsl:apply-templates select="$prev" mode="uri"/>
      </xsl:variable>
      <link rel="prev" href="{$prev-uri}" title="Previous: {$prev/title/text()}"/>
    </xsl:if>
    
    <!-- Next -->
    <xsl:if test="following-sibling::section">
      <xsl:variable name="next" select="following-sibling::section[1]"/>
      <xsl:variable name="next-uri">
	<xsl:apply-templates select="$next" mode="uri"/>
      </xsl:variable>
      <link rel="next" href="{$next-uri}" title="Next: {$next/title/text()}"/>
    </xsl:if>	  

  </xsl:template>  

  <xsl:template match="guikachu-doc/section" mode="navbar">
    <table width="80%" align="center" border="0" id="navbar">
      <tr>

	<!-- Link to previous section -->
	<td width="40%" align="left" valign="top">
	  <xsl:variable name="prev" select="preceding-sibling::section[1]"/>
	  <xsl:if test="$prev">
	    <xsl:variable name="prev-uri">
	      <xsl:apply-templates select="$prev" mode="uri"/>
	    </xsl:variable>
	    
	    <a href="{$prev-uri}">
	      <xsl:text>Previous: </xsl:text>
	      <xsl:value-of select="$prev/title/text()"/>
	    </a>
	  </xsl:if>
	</td>

	<td width="20%" align="center" valign="top">
	  <a href="index.html#toc"><xsl:text>Index</xsl:text></a>
	</td>
	
	<!-- Link to next section -->
	<td width="40%" align="right" valign="top">
	  <xsl:variable name="next" select="following-sibling::section[1]"/>
	  <xsl:if test="$next">	
	    <xsl:variable name="next-uri">
	      <xsl:apply-templates select="$next" mode="uri"/>
	    </xsl:variable>

	    <a href="{$next-uri}">
	      <xsl:text>Next: </xsl:text>
	      <xsl:value-of select="$next/title/text()"/>
	    </a>
	  </xsl:if>
	</td>
      </tr>
    </table>    
  </xsl:template>
  
  <xsl:template match="guikachu-doc/section" mode="page-contents">	
   <a name="sec-{@id}"/>
    <xsl:apply-templates/>
    
    <hr/>

    <xsl:apply-templates select="." mode="navbar"/>
  </xsl:template>

  <xsl:template match="section/section">
    <a name="sec-{@id}"/>
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="guikachu-doc/section/title">
    <h1 id="section-title">
      <xsl:apply-templates/>
    </h1>
  </xsl:template>

  <xsl:template match="section/section/title">
    <h2>
      <xsl:apply-templates/>
    </h2>
  </xsl:template>

  <xsl:template match="section/section/section/title">
    <h3>
      <xsl:apply-templates/>
    </h3>
  </xsl:template>
  
</xsl:stylesheet>
