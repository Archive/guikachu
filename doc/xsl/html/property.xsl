<?xml version="1.0"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="property/title">
    <dt>
      <xsl:apply-templates/>
    </dt>
  </xsl:template>

  <xsl:template match="property/description">
    <dd>
      <xsl:apply-templates/>
    </dd>
  </xsl:template>

  <xsl:template match="resource/properties/property">
    <xsl:element name="a">
      <xsl:attribute name="name">
	<xsl:text>res-</xsl:text>
	<xsl:value-of select="../../@name"/>
	<xsl:text>-</xsl:text>
	<xsl:choose>
	  <xsl:when test="@name">
	    <xsl:value-of select="@name"/>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="@ref"/>
	  </xsl:otherwise>
	</xsl:choose>
      </xsl:attribute>
    </xsl:element>

    <xsl:choose>
      <xsl:when test="@ref">
	<xsl:apply-templates select="../../../common-properties/property[@name=current()/@ref]"/>
      </xsl:when>
      <xsl:otherwise>
	<xsl:apply-templates/>
      </xsl:otherwise>
    </xsl:choose>   
  </xsl:template>

</xsl:stylesheet>
