<?xml version="1.0" encoding="iso-8859-1"?>
<section id="resource-win">
  <title>Resource Windows</title>
  <para>
    Resources are edited by double-clicking on them in the main
    window. This pops up a new window containing controls to view
    and change the attributes of the resource. With the exception of
    the <link ref="menu-editor">menu editor</link> and the <link
    ref="form-editor">form editor</link> (see their description
    later), these property editors share a common set of controls.
  </para>
  <para>
    A special resource is the one holding application data such as
    icon caption (as shown by the Program Manager on Palm handhelds)
    and the version number. Since it wouldn't make sense to have either
    more or less than one of these, they are not shown in the main window's
    tree view. Instead, you can access it by double-clicking on the
    &quot;root&quot; of the list, the item that shows the icon caption
    (or &quot;Application&quot; by default).
  </para>
  <img ref="main-window-app-res.png">
    Tree item for application-level resources
  </img>

    <para>
    <!-- General resource editor principles-->
    A resource window contains controls that show the current value
    of different aspects, or &quot;properties&quot; of the
    resource. The left-hand side of the window contains the names of
    the properties, and the right-hand side has the property
    controls. Press <key>Alt</key> and the underlined letter from
    the property label to move the input focus to the selected
    property's control.
  </para>
  <para>
    Some resource windows are broken into multiple pages of property
    editors. Click on the tabs at the top of the window to change
    pages.
  </para>
  
  <section>
    <title>Text properties</title>
    
    <para>
      There are three types of textual properties in Guikachu:
      resource ID's, single-line small strings and multi-line, long
      texts.
    </para>
    
    <img ref="prop-string.png">
      String property editor
    </img>
    <para>
      Single-line string properties are edited by simply focusing
      the text entry (by clicking on it, or pressing the
      <key>Alt</key> key and the underlined letter of the left-side
      label) and typing. Changes are instantly applied, which is
      very useful for widgets, since you can see the results in real
      time in the form preview.
    </para>
    
    <img ref="prop-id.png">
      Resource ID editor
    </img>
    <para>
      Although they look just like any other string property,
      resource ID's are a special case because changes are not
      instantly applied. This is done to make validation possible:
      the uniqueness of each ID is automatically enforced by
      Guikachu when you press <key>Enter</key> in a resource ID
      field. If there is another resource with the same ID already
      existing in the current project, a message is shown and the
      previous value is restored.
    </para>
    
    <img ref="prop-text.png">
      Text property editor
    </img>
    <para>
      Certain resources can have long, multi-line string
      properties. These are edited just like single-line strings,
      but your input can include <key>Enter</key> and
      <key>Tab</key>.
    </para>

    <para>
      For user-visible strings (i.e. not ID's), you can use the
      <key>\</key> character to include &quot;escape
      sequences&quot;. The two most important notions are
      <code>\nnn</code> where <code>nnn</code> is an octal number, and
      <code>\xnn</code>, where <code>nn</code> is a hexadecimal
      number. Since Guikachu only accepts 7-bit input, you will need
      to use these escape codes to include characters from the Latin-1
      set. Note that as of 1.2, <code>\r</code> is not yet supported
      by Guikachu.
    </para>
  </section>
  
  <section>
    <title>Numeric properties</title>
    <img ref="prop-spin.png">
      Numeric property editor
    </img>
    <para>
      Numeric properties can be edited either by entering the new
      value directly to the entry field, or by clicking the up/down
      arrows with the mouse. Since some properties only accept a
      certain range of values, the value entered may be changed
      after you set it to a value outside the valid range.
    </para>
  </section>
  
  <section>
    <title>References</title>
    <img ref="prop-ref.png">
      A reference to a string resource
    </img>
    <para>
      Some properties are references to other resources, for example
      the &quot;Help&quot; property of a dialog points to a string
      resource that will be displayed when the user taps the help
      icon. Setting these is done by selecting from the drop-down
      list of available resources of the appropriate type. Changing
      a resource's ID automatically changes every other property
      pointing to it.
    </para>
    <para>
      To quickly access the referenced resource, click the
      <key>Edit</key> button next to the drop-down list. This pops
      up the editor window for the currently selected resource.
    </para>
  </section>
  
  <section>
    <title>Toggles</title>
    <img ref="prop-toggle.png">
      Simple toggle button
    </img>
    <para>
      Properties that are of binary nature (i.e. those that can
      either be set or unset) can be changed by clicking on the
      yes/no toggle button. The button shows the current state of
      the property.
    </para>
    
    <img ref="prop-size.png">
      Compound property editor with manual/automatic switch
    </img>
    <para>
      The size of some widgets can be either manually specified or
      calculated from its contents. Automatic size is the
      default. To enter a manual value, first check the check-box on
      the left of the size entry, and then change the value in the
      entry.
    </para>
    <para>
      The first time you change an automatic value to a manual one,
      the manual value is initialized with the currently calculated
      value.
    </para>
  </section>
  
  <section>
    <title>Selecting from a list</title>
    <img ref="prop-dropdown.png">
      Drop-down list
    </img>
    <para>
      Some properties can only have values from a fixed
      set. Clicking on the current value pops up a list of options.
    </para>
  </section>
  
  <section>
    <title>String lists</title>
      <img ref="prop-stringlist.png">
      A string list editor
    </img>
    <para>
      Some properties, like the buttons of a dialog, contain a list of
      strings. To insert a new item after the currently selected
      string, click on the <key>Add</key> button, then type in the new
      string. To modify an existing item, select it from the list and
      in the new string. The <key>Up</key> and <key>Down</key> buttons
      change the position of the currently selected item.
    </para>
    <para>
      The dialog editor also has a special <key>Default</key>
      button. Press this to mark the currently selected item as the
      default response.
    </para>
  </section>
</section>  
